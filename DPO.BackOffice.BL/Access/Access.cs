﻿using System;
using DPO.BackOffice.DAL;


namespace DPO.BackOffice.BL
{
	public partial class BackOfficeBL
	{
		public tblAccess GetAccessBySubApplication(String pUserName, Int32 pSubApplicationID)
		{
			try
			{
                UpdateTempMethod("GetAccessBySubApplication", "(String pUserName, Int32 pSubApplicationID)", true);
				tblAccessCrud crud = new tblAccessCrud();
				crud.Where("UserName", DAL.General.Operator.Equals, pUserName);
				crud.And("SubApplicationID", DAL.General.Operator.Equals, pSubApplicationID);
				recordFound = false;
				tblAccess access = crud.ReadSingle();
				recordFound = crud.RecordFound;

				return access;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetAccessBySubApplication", "(String pUserName, Int32 pSubApplicationID)", false);
				throw new Exception(errorMessage + "GetAccessBySubApplication-" + Ex.Message);
			}
		}
		public Boolean CheckAccessBySubApplication(String pUserName, Int32 pSubApplicationID)
		{
			try
			{
                UpdateTempMethod("CheckAccessBySubApplication", "(String pUserName, Int32 pSubApplicationID)", true);
				tblAccess access = GetAccessBySubApplication(pUserName, pSubApplicationID);
				if (recordFound) return true;

				return false;
					
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("CheckAccessBySubApplication", "(String pUserName, Int32 pSubApplicationID)", false);
				throw new Exception(errorMessage + "GetAccessBySubApplication-" + Ex.Message);
			}
		}
    }
}
