﻿using System;
using System.Collections.Generic;
using System.Linq;
using DPO.BackOffice.DAL;

namespace DPO.BackOffice.BL
{
	public partial class BackOfficeBL
	{

		public List<tblApplicationLogTypeChange> GetApplicationLogTypeChange()
		{
			try
			{
                UpdateTempMethod("GetApplicationLogTypeChange", "()", true);
				recordFound = false;
				tblApplicationLogTypeChangeCrud crud = new tblApplicationLogTypeChangeCrud();
				List<tblApplicationLogTypeChange> search = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetApplicationLogTypeChange", "()", false);
				throw new Exception("GetApplicationLogTypeChange - " + Ex.Message);
			}
		}
		public Boolean DeleteApplicationLogType(Int32 pApplicationID, Byte pLogTypeID)
		{
			try
			{
                UpdateTempMethod("DeleteApplicationLogType", "(Int32 pApplicationID, Byte pLogTypeID)", true);
				tblApplicationLogTypeCrud crud = new tblApplicationLogTypeCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
				crud.And("LogTypeID", DAL.General.Operator.Equals, pLogTypeID);

				List<tblApplicationLogType> search = crud.ReadMulti().ToList();
				foreach (tblApplicationLogType del in search)
					crud.Delete(del);
				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("DeleteApplicationLogType", "(Int32 pApplicationID, Byte pLogTypeID)", false);
				throw new Exception("DeleteApplicationLogType - " + Ex.Message);
			}
		}
		public Boolean DeleteApplicationLogTypeChange(Int32 pChangeID)
		{
			try
			{
                UpdateTempMethod("DeleteApplicationLogTypeChange", "(Int32 pChangeID)", true);
				tblApplicationLogTypeChangeCrud crud = new tblApplicationLogTypeChangeCrud();
				crud.Where("ChangeID", DAL.General.Operator.Equals, pChangeID);

				List<tblApplicationLogTypeChange> search = crud.ReadMulti().ToList();
				foreach (tblApplicationLogTypeChange del in search)
					crud.Delete(del);
				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("DeleteApplicationLogTypeChange", "(Int32 pChangeID)", false);
				throw new Exception("DeleteApplicationLogTypeChange - " + Ex.Message);
			}
		}
		public Boolean DeleteApplicationLogTypeChange()
		{
			try
			{
                UpdateTempMethod("DeleteApplicationLogTypeChange", "()", true);
				tblApplicationLogTypeChangeCrud crud = new tblApplicationLogTypeChangeCrud();

				List<tblApplicationLogTypeChange> search = crud.ReadMulti().ToList();
				foreach (tblApplicationLogTypeChange del in search)
					crud.Delete(del);
				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("DeleteApplicationLogTypeChange", "()", false);
				throw new Exception("DeleteApplicationLogTypeChange - " + Ex.Message);
			}
		}
		public Boolean DeleteApplicationFolder(Int32 pFolderID)
		{
			try
			{
                UpdateTempMethod("DeleteApplicationFolder", "(Int32 pFolderID)", true);
				tblApplicationFolderCrud crud = new tblApplicationFolderCrud();
				crud.Where("FolderID", DAL.General.Operator.Equals, pFolderID);

				List<tblApplicationFolder> search = crud.ReadMulti().ToList();
				foreach (tblApplicationFolder del in search)
					crud.Delete(del);
				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("DeleteApplicationFolder", "(Int32 pFolderID)", false);
				throw new Exception("DeleteApplicationFolder - " + Ex.Message);
			}
		}
		public Boolean DeleteApplication(Byte pApplicationID)
		{
			try
			{
                UpdateTempMethod("DeleteApplication", "(Byte pApplicationID)", true);
				tblApplicationCrud crud = new tblApplicationCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);

				List<tblApplication> search = crud.ReadMulti().ToList();
				foreach (tblApplication del in search)
					crud.Delete(del);
				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("DeleteApplication", "(Byte pApplicationID)", false);
				throw new Exception("DeleteApplicationByte - " + Ex.Message);
			}
		}
		public String GetApplicationGridData()
		{
			try
			{
                UpdateTempMethod("GetApplicationGridData", "()", true);
				tblApplicationCrud crud = new tblApplicationCrud();
				crud.OrderBy(x => x.vcsApplication);
				List<tblApplication> search = crud.ReadMulti().ToList();
				String divString = "";
				foreach (tblApplication workRec in search)
				{
					divString += workRec.ApplicationID + "═";
					divString += workRec.vcsApplication + "═";
					divString += workRec.SystemID + "═";
					divString += workRec.ApplicationTypeID + "═";
					divString += workRec.ServerID + "═";
					divString += workRec.ApplicationDescription + "║";
				}

				if (divString.Length > 0)
					divString = divString.Substring(0, divString.Length - 1);

				return divString;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetApplicationGridData", "()", false);
				throw new Exception("GetApplicationGridData - " + Ex.Message);
			}
		}
		public Int32 SaveSubApplicationFolder(Int32 pFolderID, Int32 pApplicationID, Int32 pSubApplicationID, String pFolderPath)
		{
			try
			{
                UpdateTempMethod("SaveSubApplicationFolder", "(Int32 pFolderID, Int32 pApplicationID, Int32 pSubApplicationID, String pFolderPath)", true);
				tblApplicationFolderCrud crud = new tblApplicationFolderCrud();
				tblApplicationFolder workRec = new tblApplicationFolder();
				if (pFolderID != 0)
				{
					crud.Where("FolderID", DAL.General.Operator.Equals, pFolderID);
					workRec = crud.ReadSingle();
					workRec.ApplicationID = pApplicationID;
					workRec.SubApplicationID = pSubApplicationID;
					workRec.FolderPath = pFolderPath;
					crud.Update(workRec);
					return pFolderID;
				}
				else
				{
					workRec.ApplicationID = pApplicationID;
					workRec.SubApplicationID = pSubApplicationID;
					workRec.FolderPath = pFolderPath;
					long ID = 0;
					crud.Insert(workRec, out ID);
					return (Int32)ID;
				}
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("SaveSubApplicationFolder", "(Int32 pFolderID, Int32 pApplicationID, Int32 pSubApplicationID, String pFolderPath)", false);
				throw new Exception("SaveSubApplicationFolder - " + Ex.Message);
			}
		}
		public Int32 SaveApplicationFolder(Int32 pFolderID, Int32 pApplicationID, String pFolderPath)
		{
			try
			{
                UpdateTempMethod("SaveApplicationFolder", "(Int32 pFolderID, Int32 pApplicationID, String pFolderPath)", true);
				tblApplicationFolderCrud crud = new tblApplicationFolderCrud();
				tblApplicationFolder workRec = new tblApplicationFolder();
				if (pFolderID != 0)
				{
					crud.Where("FolderID", DAL.General.Operator.Equals, pFolderID);
					workRec = crud.ReadSingle();
					workRec.ApplicationID = pApplicationID;
					workRec.FolderPath = pFolderPath;
					crud.Update(workRec);
					return pFolderID;
				}
				else
				{
					workRec.ApplicationID = pApplicationID;
					workRec.FolderPath = pFolderPath;
					long ID = 0;
					crud.Insert(workRec, out ID);
					return (Int32)ID;
				}
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("SaveApplicationFolder", "(Int32 pFolderID, Int32 pApplicationID, String pFolderPath)", false);
				throw new Exception("SaveApplicationFolder - " + Ex.Message);
			}
		}
		public Int32 SaveApplication(Int32 pApplicationID, Byte pSystemID, String pApplicationName, Byte pApplicationTypeID, Int32 pServerID, String pApplicationDescription)
		{
			try
			{
                UpdateTempMethod("SaveApplication", "(Int32 pApplicationID, Byte pSystemID, String pApplicationName, Byte pApplicationTypeID, Int32 pServerID, String pApplicationDescription)", true);
				tblApplicationCrud crud = new tblApplicationCrud();
				tblApplication workRec = new tblApplication();
				if (pApplicationID != 0)
				{
					crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
					workRec = crud.ReadSingle();
					workRec.SystemID = pSystemID;
					workRec.vcsApplication = pApplicationName;
					workRec.ApplicationTypeID = pApplicationTypeID;
					workRec.ServerID = pServerID;
					workRec.ApplicationDescription = pApplicationDescription;
					crud.Update(workRec);
					return pApplicationID;
				}
				else
				{
					workRec.SystemID = pSystemID;
					workRec.vcsApplication = pApplicationName;
					workRec.ApplicationTypeID = pApplicationTypeID;
					workRec.ServerID = pServerID;
					workRec.ApplicationDescription = pApplicationDescription;
					long ID = 0;
					crud.Insert(workRec, out ID);
					return (Int32)ID;
				}
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("SaveApplication", "(Int32 pApplicationID, Byte pSystemID, String pApplicationName, Byte pApplicationTypeID, Int32 pServerID, String pApplicationDescription)", false);
				throw new Exception("SaveApplication - " + Ex.Message);
			}
		}
		public tblApplicationType GetApplicationType(Int32 pApplicationTypeID)
		{
			try
			{
                UpdateTempMethod("GetApplicationType", "(Int32 pApplicationTypeID)", true);
				recordFound = false;
				tblApplicationTypeCrud crud = new tblApplicationTypeCrud();
				crud.Where("ApplicationTypeID", DAL.General.Operator.Equals, pApplicationTypeID);

				tblApplicationType search = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetApplicationType", "(Int32 pApplicationTypeID)", false);
				throw new Exception("GetApplicationType - " + Ex.Message);
			}
		}
		public String GetApplicationTypeGridData()
		{
			try
			{
                UpdateTempMethod("GetApplicationTypeGridData", "()", true);
				tblApplicationTypeCrud crud = new tblApplicationTypeCrud();
				List<tblApplicationType> search = crud.ReadMulti().ToList();
				String divString = "";
				foreach (tblApplicationType workRec in search)
				{
					divString += workRec.ApplicationTypeID + "═";
					divString += workRec.ApplicationType + "║";
				}

				if (divString.Length > 0)
					divString = divString.Substring(0, divString.Length - 1);

				return divString;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetApplicationTypeGridData", "()", false);
				throw new Exception("GetApplicationTypeGridData - " + Ex.Message);
			}
		}
		public tblSystem GetSystem(short pSystemID)
		{
			try
			{
                UpdateTempMethod("GetSystem", "(short pSystemID)", true);
				recordFound = false;
				tblSystemCrud crud = new tblSystemCrud();
				crud.Where("SystemID", DAL.General.Operator.Equals, pSystemID);

				tblSystem search = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetSystem", "(short pSystemID)", false);
				throw new Exception("GetSystemshort - " + Ex.Message);
			}
		}
		public List<tblSystem> GetSystem()
		{
			try
			{
                UpdateTempMethod("GetSystem", "()", true);
				recordFound = false;
				tblSystemCrud crud = new tblSystemCrud();
				List<tblSystem> search = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetSystem", "()", false);
				throw new Exception("GetSystem - " + Ex.Message);
			}
		}
		public String GetSystemGridData()
		{
			try
			{
                UpdateTempMethod("GetSystemGridData", "()", true);
				tblSystemCrud crud = new tblSystemCrud();
				crud.OrderBy(x => x.vcsSystem);
				List<tblSystem> search = crud.ReadMulti().ToList();
				String divString = "";
				foreach (tblSystem workRec in search)
				{
					divString += workRec.SystemID + "═";
					divString += workRec.vcsSystem + "║";
				}

				if (divString.Length > 0)
					divString = divString.Substring(0, divString.Length - 1);

				return divString;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetSystemGridData", "()", false);
				throw new Exception("GetSystemGridData - " + Ex.Message);
			}
		}
		public tblSystem GetSystemByName(String pSystemName)
		{
			try
			{
                UpdateTempMethod("GetSystemByName", "(String pSystemName)", true);
				recordFound = false;
				tblSystemCrud crud = new tblSystemCrud();
				crud.Where("vcsSystem", DAL.General.Operator.Equals, pSystemName);

				tblSystem search = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetSystemByName", "(String pSystemName)", false);
				throw new Exception("GetSystemByName - " + Ex.Message);
			}
		}




        public tblApplication GetApplicationByName(String pApplicationName)
		{
			try
			{
                UpdateTempMethod("GetApplicationByName", "(String pApplicationName)", true);
				recordFound = false;
				tblApplicationCrud crud = new tblApplicationCrud();
				crud.Where("vcsApplication", DAL.General.Operator.Equals, pApplicationName);

				tblApplication search = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetApplicationByName", "(String pApplicationName)", false);
				throw new Exception("GetApplicationByName - " + Ex.Message);
			}
		}

        
        public List<tblApplication> GetApplicationBySystemID(Byte pSystemID)
		{
			try
			{
                UpdateTempMethod("GetApplicationBySystemID", "(Byte pSystemID)", true);
				recordFound = false;
				tblApplicationCrud crud = new tblApplicationCrud();
				List<tblApplication> search = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetApplicationBySystemID", "(Byte pSystemID)", false);
				throw new Exception("GetApplicationBySystemIDByte - " + Ex.Message);
			}
		}
		public tblApplication GetApplicationByName(Byte pSystemID, String pApplicationName)
		{
			try
			{
                UpdateTempMethod("GetApplicationByName", "(Byte pSystemID, String pApplicationName)", true);
				recordFound = false;
				tblApplicationCrud crud = new tblApplicationCrud();
				crud.Where("SystemID", DAL.General.Operator.Equals, pSystemID);
				crud.And("vcsApplication", DAL.General.Operator.Equals, pApplicationName);

				tblApplication search = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetApplicationByName", "(Byte pSystemID, String pApplicationName)", false);
				throw new Exception("GetApplicationByNameByte - " + Ex.Message);
			}
		}

        

        public List<tblApplication> GetApplication()
		{
			try
			{
                UpdateTempMethod("GetApplication", "()", true);
				recordFound = false;
				tblApplicationCrud crud = new tblApplicationCrud();
				List<tblApplication> search = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetApplication", "()", false);
				throw new Exception("GetApplication - " + Ex.Message);
			}
		}
		public tblApplication GetApplication(Int32 pApplicationID)
		{
			try
			{
                UpdateTempMethod("GetApplication", "(Int32 pApplicationID)", true);
				recordFound = false;
				tblApplicationCrud crud = new tblApplicationCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);

				tblApplication search = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetApplication", "(Int32 pApplicationID)", false);
				throw new Exception("GetApplication - " + Ex.Message);
			}
		}
		public tblApplicationSubApp GetSubApplicationByName(String pApplicationName, String pSubApplicationName)
		{
			try
			{
                UpdateTempMethod("GetSubApplicationByName", "(String pApplicationName, String pSubApplicationName)", true);
				tblApplication app = GetApplicationByName(pApplicationName);

				tblApplicationSubAppCrud crud = new tblApplicationSubAppCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, (Int32)app.ApplicationID);
                crud.And("SubApplication", DAL.General.Operator.Equals, pSubApplicationName);
				tblApplicationSubApp search = crud.ReadSingle();
				if (!crud.RecordFound)
					throw new Exception("Sub application by the name of " + pApplicationName + "." + pSubApplicationName + " was not found in the database. ");

				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetSubApplicationByName", "(String pApplicationName, String pSubApplicationName)", false);
				throw new Exception("GetSubApplicationByName(String, String)-" + Ex.Message);
			}
		}



        public tblApplicationSubApp GetSubApplicationByName(Int32 pApplicationID, String pSubApplicationName)
		{
			try
			{
                UpdateTempMethod("GetSubApplicationByName", "(Int32 pApplicationID, String pSubApplicationName)", true);
				recordFound = false;
				tblApplicationSubAppCrud crud = new tblApplicationSubAppCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
				crud.And("SubApplication", DAL.General.Operator.Equals, pSubApplicationName);

				tblApplicationSubApp search = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetSubApplicationByName", "(Int32 pApplicationID, String pSubApplicationName)", false);
				throw new Exception("GetSubApplicationByName - " + Ex.Message);
			}
		}
		public tblApplicationSubApp GetSubApplicationByName(String pSubApplicationName)
		{
			try
			{
                UpdateTempMethod("GetSubApplicationByName", "(String pSubApplicationName)", true);
				recordFound = false;
				tblApplicationSubAppCrud crud = new tblApplicationSubAppCrud();
				crud.Where("SubApplication", DAL.General.Operator.Equals, pSubApplicationName);

				tblApplicationSubApp search = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetSubApplicationByName", "(String pSubApplicationName)", false);
				throw new Exception("GetSubApplicationByName - " + Ex.Message);
			}
		}
		public tblApplicationSubApp GetSubApplication(Int32 pApplicationID, Int32 pSubApplicationID)
		{
			try
			{
                UpdateTempMethod("GetSubApplication", "(Int32 pApplicationID, Int32 pSubApplicationID)", true);
				recordFound = false;
				tblApplicationSubAppCrud crud = new tblApplicationSubAppCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
				crud.And("SubApplicationID", DAL.General.Operator.Equals, pSubApplicationID);

				tblApplicationSubApp search = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetSubApplication", "(Int32 pApplicationID, Int32 pSubApplicationID)", false);
				throw new Exception("GetSubApplication - " + Ex.Message);
			}
		}
		public List<tblApplicationSubApp> GetSubApplication(Int32 pApplicationID)
		{
			try
			{
                UpdateTempMethod("GetSubApplication", "(Int32 pApplicationID)", true);
				recordFound = false;
				tblApplicationSubAppCrud crud = new tblApplicationSubAppCrud();
				List<tblApplicationSubApp> search = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetSubApplication", "(Int32 pApplicationID)", false);
				throw new Exception("GetSubApplication - " + Ex.Message);
			}
		}
		public List<tblApplicationSubApp> GetSubApplication()
		{
			try
			{
                UpdateTempMethod("GetSubApplication", "()", true);
				recordFound = false;
				tblApplicationSubAppCrud crud = new tblApplicationSubAppCrud();
				List<tblApplicationSubApp> search = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetSubApplication", "()", false);
				throw new Exception("GetSubApplication - " + Ex.Message);
			}
		}
		public tblApplicationSubApp GetSubApplication(String pSystemName, String pApplicationName, String pSubApplicationName)
		{
			try
			{
                UpdateTempMethod("GetSubApplication", "(String pSystemName, String pApplicationName, String pSubApplicationName)", true);
				tblApplicationSubApp subApp = GetSubApplicationByName(pApplicationName, pSubApplicationName);

				return subApp;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetSubApplication", "(String pSystemName, String pApplicationName, String pSubApplicationName)", false);
				throw new Exception(errorMessage + "GetApplicationSubApplication(String, String, String) - " + Ex.Message);
			}
		}
		public tblApplication GetApplication(String pSystemName, String pApplicationName)
		{
			try
			{
                UpdateTempMethod("GetApplication", "(String pSystemName, String pApplicationName)", true);

				tblSystem system = GetSystemByName(pSystemName);
				tblApplication app = GetApplicationByName(Convert.ToByte(system.SystemID), pApplicationName);
				return app;

			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetApplication", "(String pSystemName, String pApplicationName)", false);
				throw Ex;
			}
		}
		public tblApplicationSmppNetwork GetApplicationSmppNetwork(Int32 pNetworkID)
		{
			try
			{
                UpdateTempMethod("GetApplicationSmppNetwork", "(Int32 pNetworkID)", true);
				recordFound = false;
				tblApplicationSmppNetworkCrud crud = new tblApplicationSmppNetworkCrud();
				crud.Where("NetworkID", DAL.General.Operator.Equals, pNetworkID);

				tblApplicationSmppNetwork search = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetApplicationSmppNetwork", "(Int32 pNetworkID)", false);
				throw new Exception("GetApplicationSmppNetwork - " + Ex.Message);
			}
		}

	}
}
