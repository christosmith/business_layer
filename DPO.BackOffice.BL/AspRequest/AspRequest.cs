﻿using System;
using DPO.BackOffice.DAL;

namespace DPO.BackOffice.BL
{
	public partial class BackOfficeBL
    {
        public tblRequestFromAsp GetRequestFromAsp(Int32 pApplicationID, Guid pRequestID)
        {
            try
            {
                UpdateTempMethod("GetRequestFromAsp", "(Int32 pApplicationID, Guid pRequestID)", true);
                recordFound = false;
                tblRequestFromAspCrud crud = new tblRequestFromAspCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
                //crud.And("RequestID", DAL.General.Operator.Equals, pRequestID);

                tblRequestFromAsp search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetRequestFromAsp", "(Int32 pApplicationID, Guid pRequestID)", false);
                throw new Exception("GetRequestFromAsp - " + Ex.Message);
            }
        }
		public Boolean VerifyRequestFromAsp(Int32 pApplication, Guid pRequestID, out String pVerificationMessage)
        {
            try
            {
                UpdateTempMethod("VerifyRequestFromAsp", "(Int32 pApplication, Guid pRequestID, out String pVerificationMessage)", true);
                pVerificationMessage = "";
                //Get the timeout value. The .net page has 60 seconds to respond before it fails.
                //This is to prevent reusing requests
                Int32 timeout = Convert.ToInt32(GetSettingByName("RequestTimeout", pApplication).SettingValue);
                //Get the security level needed to use the .net application
                Int32 securityLevel = Convert.ToInt32(GetSettingByName("SecurityLevel", pApplication).SettingValue);

                //Get the request record inserted by the classic asp application
                tblRequestFromAsp request = GetRequestFromAsp(pApplication, pRequestID);
                if (request == null)
                {
                    pVerificationMessage = "Request was not found.";
                    return false;
                }

                //Check if the time out has expired
                DateTime requestTime = Convert.ToDateTime(request.RequestDateTime).AddMilliseconds(timeout);
                if (requestTime < DateTime.Now)
                {
                    pVerificationMessage = "The request time allowed has expired. Please try again.";
                    return false;
                }

                //Check if the user exists
                String userName = vvoBL.GetVtUserName(request.UserName);
                if (userName == "")
                {
                    pVerificationMessage = "The user was not found.";
                    return false;
                }

                //Check if the user has the applicable security level
                Int32 userSecurityLevel = vvoBL.GetVtUserSecurityLevel(request.UserName);
                if (securityLevel < userSecurityLevel)
                {
                    pVerificationMessage = "You do not have permission to use this application.";
                    return false;
                }

                pVerificationMessage = userName;
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("VerifyRequestFromAsp", "(Int32 pApplication, Guid pRequestID, out String pVerificationMessage)", false);
                throw Ex;
            }
        }
    }
}
