﻿using System;
using System.Collections.Generic;
using System.Linq;
using DPO.BackOffice.DAL;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
namespace DPO.BackOffice.BL

{

	public partial class BackOfficeBL
{
        
        public List<tblTableDelete> GetTabelDelete()
        {
            try
            {
                tblTableDeleteCrud crud = new tblTableDeleteCrud();
                recordFound = false;
                List<tblTableDelete> results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return results;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetTableDelete - " + Ex.Message);
            }
        }
        public void GenerateAuditTriggers()
        {
            List<tblSystem> systems = GetSystem();

            foreach (tblSystem system in systems)
            {
                Byte systemID = (Byte)system.SystemID;
                List<tblTable> tables = GetTable((Byte)system.SystemID);
                tables = tables.FindAll(x => x.DoAudit == true);
                foreach (tblTable table in tables)
                {
                    List<tblColumn> columns = GetColumn((Int32)table.TableID);
                    columns = columns.FindAll(x => x.DoAudit == true);
                    String tableName = table.TableName;
                    Int32 tableID = (Int32)table.TableID;
                    String triggerName = tableName + "InsAudit";
                    StringBuilder trig = new StringBuilder();
                    trig.AppendLine(String.Format("If (Select Count(*) From sys.triggers Where name = '{0}') > 0", triggerName));
                    trig.AppendLine(String.Format("Drop Trigger {0}", triggerName));
                    trig.AppendLine("Go");
                    trig.AppendLine(String.Empty);
                    trig.AppendLine(String.Format("Create Trigger {0} On {1} For Insert", triggerName, tableName));
                    trig.AppendLine("As");
                    trig.Append("Declare ");
                    foreach (tblColumn col in columns)
                    {
                        String varName = "@" + col.ColumnName + " " + col.tblColumnDataType.DataType;
                        if (col.tblColumnDataType.DataType.ToLower().Contains("char"))
                            varName += "(" + col.ColumnLength + ")";

                        varName += ", ";
                        trig.Append(varName);
                    }
                    trig.Append("\n");
                    trig.AppendLine("	    @TableID Int, @AuditHeaderID Int, @ColumnID Int,");
                    trig.AppendLine("		@ApplicationID Int, @SubApplicationID Int, @PrimaryKeyValue Int");
                    trig.AppendLine(String.Empty);

                    trig.AppendLine("Select @TableID = TableID");
                    trig.AppendLine("  From tblTable");
                    trig.AppendLine(String.Format(" Where TableName = '{0}'", tableName));
                    trig.AppendLine(String.Empty);
                    tblTablePrimaryKey pk = GetTablePrimaryKey(tableID)[0];
                    tblTablePrimaryKeyColumn pkCol = GetTablePrimaryKeyColumn((Int32)pk.PrimaryKeyID)[0];
                    String pkColName = pkCol.tblColumn.ColumnName;
                    trig.AppendLine("Select @PrimaryKeyValue = " + pkColName + " from inserted");
                    trig.AppendLine("Select @ApplicationID = ApplicationID From tblApplication Where vcsApplication = 'Database'");
                    trig.AppendLine("Select @SubApplicationID = SubApplicationID From tblApplicationSubApp Where ApplicationID = @ApplicationID And SubApplication = 'Audit Trigger'");
                    trig.AppendLine(String.Empty);
                    trig.AppendLine("If @TableID Is Null");
                    trig.AppendLine("	Begin");
                    trig.AppendLine("		Insert Into tblLog ");
                    trig.AppendLine("		(ApplicationID, SubApplicationID, LogTypeID, LogDateTime, LogMessage)");
                    trig.AppendLine("		Values");
                    trig.AppendLine(String.Format("		(1, 1, 3, GETDATE(), 'The table, {0}, was not set up in tblTable.')", tableName));
                    trig.AppendLine("		Return");
                    trig.AppendLine("	End");
                    trig.AppendLine(String.Empty);
                    String varAssign = "";
                    foreach (tblColumn col in columns)
                        varAssign += String.Format("@{0}={0}, ", col.ColumnName);
                    varAssign = varAssign.Substring(0, varAssign.Length - 2);
                    trig.AppendLine("Select " + varAssign);
                    trig.AppendLine("  From inserted");
                    trig.AppendLine(String.Empty);
                    trig.AppendLine("Insert Into tblAuditHeader");
                    trig.AppendLine("(TableID, AuditDateTime, UserID, ActionID, PrimaryKeyValue)");
                    trig.AppendLine("Values");
                    trig.AppendLine("(@TableID, GETDATE(), @UserID, 1, @PrimaryKeyValue)");
                    trig.AppendLine(String.Empty);
                    trig.AppendLine("Select @AuditHeaderID = IDENT_CURRENT('tblAuditHeader')");
                    trig.AppendLine(String.Empty);
                    foreach (tblColumn col in columns)
                    {
                        trig.AppendLine("Select @ColumnID = ColumnID");
                        trig.AppendLine("  From tblColumn");
                        trig.AppendLine(" Where TableID = @TableID");
                        trig.AppendLine(String.Format("   And ColumnName = '{0}'", col.ColumnName));
                        trig.AppendLine(String.Empty);
                        trig.AppendLine("If @@ROWCOUNT = 0");
                        trig.AppendLine("	Insert Into tblLog ");
                        trig.AppendLine("	(ApplicationID, SubApplicationID, LogTypeID, LogDateTime, LogMessage)");
                        trig.AppendLine("	Values");
                        trig.AppendLine(String.Format("	(@ApplicationID, @SubApplicationID, 3, GETDATE(), 'The column, {0}.{1}, was not set up in tblColumn.')", tableName, col.ColumnName));
                        trig.AppendLine("Else");
                        trig.AppendLine("   Insert Into tblAuditLine");
                        trig.AppendLine("   (HeaderID, ColumnID, ValueBefore, ValueAfter)");
                        trig.AppendLine("   Values");
                        trig.AppendLine(String.Format("   (@AuditHeaderID, @ColumnID, '', @{0})", col.ColumnName));
                        trig.AppendLine(String.Empty);
                    }

                    trig.AppendLine(String.Empty);
                    trig.AppendLine("Go");
                    trig.AppendLine(String.Empty);

                    Console.WriteLine(triggerName);

                    triggerName = tableName + "UpdAudit";
                    trig.AppendLine(String.Format("If (Select Count(*) From sys.triggers Where name = '{0}') > 0", triggerName));
                    trig.AppendLine(String.Format("Drop Trigger {0}", triggerName));
                    trig.AppendLine("Go");
                    trig.AppendLine(String.Empty);
                    trig.AppendLine(String.Format("Create Trigger {0} On {1} For Update", triggerName, tableName));
                    trig.AppendLine("As");
                    trig.Append("Declare ");
                    List<tblTablePrimaryKey> pkKeys = GetTablePrimaryKey(tableID);
                    foreach (tblColumn col in columns)
                    {
                        List<tblTablePrimaryKeyColumn> pkCols = GetTablePrimaryKeyColumn((Int32)pkKeys[0].PrimaryKeyID);
                        List<tblTablePrimaryKeyColumn> pkFInd = pkCols.FindAll(x => x .ColumnID == (Int32)col.ColumnID);
                        if (pkFInd.Count == 0)
                        {
                            String varName = "@" + col.ColumnName + " " + col.tblColumnDataType.DataType;
                            if (col.tblColumnDataType.DataType.ToLower().Contains("char"))
                                varName += "(" + col.ColumnLength + ")";

                            varName += ", ";
                            trig.Append(varName);
                        }
                    }
                    trig.Append("\n");

                    foreach (tblColumn col in columns)
                    {
                        List<tblTablePrimaryKeyColumn> pkCols = GetTablePrimaryKeyColumn((Int32)pkKeys[0].PrimaryKeyID);
                        List<tblTablePrimaryKeyColumn> pkFInd = pkCols.FindAll(x => x.ColumnID == (Int32)col.ColumnID);

                        if (pkFInd.Count == 0)
                        {
                            String varName = "@Old" + col.ColumnName + " " + col.tblColumnDataType.DataType;
                            if (col.tblColumnDataType.DataType.ToLower().Contains("char"))
                                varName += "(" + col.ColumnLength + ")";

                            varName += ", ";
                            trig.Append(" " + varName);
                        }
                    }
                    trig.Append("\n");

                    trig.AppendLine("	    @TableID Int, @AuditHeaderID Int, @ColumnID Int,");
                    trig.AppendLine("		@ApplicationID Int, @SubApplicationID Int, @DataChanged Bit, @PrimaryKeyValue Int");
                    trig.AppendLine(String.Empty);
                    pk = GetTablePrimaryKey(tableID)[0];
                    pkCol = GetTablePrimaryKeyColumn((Int32)pk.PrimaryKeyID)[0];
                    pkColName = pkCol.tblColumn.ColumnName;
                    trig.AppendLine("Select @PrimaryKeyValue = " + pkColName + " from inserted");
                    trig.AppendLine("Select @ApplicationID = ApplicationID From tblApplication Where vcsApplication = 'Database'");
                    trig.AppendLine("Select @SubApplicationID = SubApplicationID From tblApplicationSubApp Where ApplicationID = @ApplicationID And SubApplication = 'Audit Trigger'");
                    trig.AppendLine("Select @DataChanged = 0");
                    trig.AppendLine(String.Empty);
                    trig.AppendLine("Select @TableID = TableID");
                    trig.AppendLine("  From tblTable");
                    trig.AppendLine(String.Format(" Where TableName = '{0}'", tableName));
                    trig.AppendLine(String.Empty);
                    trig.AppendLine("If @TableID Is Null");
                    trig.AppendLine("	Begin");
                    trig.AppendLine("		Insert Into tblLog ");
                    trig.AppendLine("		(ApplicationID, SubApplicationID, LogTypeID, LogDateTime, LogMessage)");
                    trig.AppendLine("		Values");
                    trig.AppendLine(String.Format("		(@ApplicationID, @SubApplicationID, 3, GETDATE(), 'The table, {0}, was not set up in tblTable.')", tableName));
                    trig.AppendLine("		Return");
                    trig.AppendLine("	End");
                    trig.AppendLine(String.Empty);
                    varAssign = "";
                    foreach (tblColumn col in columns)
                    {
                        List<tblTablePrimaryKeyColumn> pkCols = GetTablePrimaryKeyColumn((Int32)pkKeys[0].PrimaryKeyID);
                        List<tblTablePrimaryKeyColumn> pkFInd = pkCols.FindAll(x => x.ColumnID == (Int32)col.ColumnID);
                        if (pkFInd.Count == 0)
                            varAssign += String.Format("@{0}={0}, ", col.ColumnName);
                    }
                    trig.AppendLine(String.Empty);
                    varAssign = varAssign.Substring(0, varAssign.Length - 2);
                    trig.AppendLine("Select " + varAssign);
                    trig.AppendLine("  From inserted");

                    varAssign = "";
                    foreach (tblColumn col in columns)
                    {
                        List<tblTablePrimaryKeyColumn> pkCols = GetTablePrimaryKeyColumn((Int32)pkKeys[0].PrimaryKeyID);
                        List<tblTablePrimaryKeyColumn> pkFInd = pkCols.FindAll(x => x.ColumnID == (Int32)col.ColumnID);

                        if (pkFInd.Count == 0)
                            varAssign += String.Format("@Old{0}={0}, ", col.ColumnName);
                    }
                    trig.AppendLine(String.Empty);
                    varAssign = varAssign.Substring(0, varAssign.Length - 2);
                    trig.AppendLine("Select " + varAssign);
                    trig.AppendLine("  From deleted");
                    trig.AppendLine(String.Empty);
                    trig.AppendLine("Insert Into tblAuditHeader");
                    trig.AppendLine("(TableID, AuditDateTime, UserID, ActionID, PrimaryKeyValue)");
                    trig.AppendLine("Values");
                    trig.AppendLine("(@TableID, GETDATE(), @UserID, 1, @PrimaryKeyValue)");
                    trig.AppendLine(String.Empty);
                    trig.AppendLine("Select @AuditHeaderID = IDENT_CURRENT('tblAuditHeader')");
                    trig.AppendLine(String.Empty);

                    foreach (tblColumn col in columns)
                    {
                        List<tblTablePrimaryKeyColumn> pkCols = GetTablePrimaryKeyColumn((Int32)pkKeys[0].PrimaryKeyID);
                        List<tblTablePrimaryKeyColumn> pkFInd = pkCols.FindAll(x => x.ColumnID == (Int32)col.ColumnID);
                        if (pkFInd.Count == 0)
                        {
                            trig.AppendLine(String.Format("IF @Old{0} <> @{0}", col.ColumnName));
                            trig.AppendLine("	Begin");
                            trig.AppendLine("       Select @DataChanged = 1");
                            trig.AppendLine(String.Empty);
                            trig.AppendLine("		Select @ColumnID = ColumnID");
                            trig.AppendLine("		  From tblColumn");
                            trig.AppendLine("		 Where TableID = @TableID");
                            trig.AppendLine(String.Format("		   And ColumnName = '{0}'", col.ColumnName));
                            trig.AppendLine(String.Empty);
                            trig.AppendLine("       If @@ROWCOUNT = 0");
                            trig.AppendLine("           Insert Into tblLog ");
                            trig.AppendLine("           (ApplicationID, SubApplicationID, LogTypeID, LogDateTime, LogMessage)");
                            trig.AppendLine("           Values");
                            trig.AppendLine(String.Format("         (@ApplicationID, @SubApplicationID, 3, GETDATE(), 'The column, {0}.{1}, was not set up in tblColumn.')", tableName, col.ColumnName));
                            trig.AppendLine("   Else");
                            trig.AppendLine("       Insert Into tblAuditLine");
                            trig.AppendLine("       (HeaderID, ColumnID, ValueBefore, ValueAfter)");
                            trig.AppendLine("       Values");
                            trig.AppendLine(String.Format("      (@AuditHeaderID, @ColumnID, @Old{0}, @{0})", col.ColumnName));
                            trig.AppendLine("	End");
                            trig.AppendLine(String.Empty);
                        }
                    }

                    trig.AppendLine("If @DataChanged = 0");
                    trig.AppendLine("   Delete From tblAuditHeader Where HeaderID = @AuditHeaderID");


                    String fileName = @"C:\VCS\AuditTrigger\" + triggerName + ".sql";
                    if (System.IO.File.Exists(fileName))
                        System.IO.File.Delete(fileName);

                    System.IO.StreamWriter writer = new System.IO.StreamWriter(fileName);
                    writer.Write(trig.ToString());
                    writer.Close();
                    //fInfo = new System.IO.FileInfo(fileName);
                    //commandLine = "-S DESKTOP-EABVF26 -d Trident -E -i " + fInfo.FullName;
                    //process = new System.Diagnostics.Process();
                    //process.StartInfo.FileName = "sqlcmd";
                    //process.StartInfo.Arguments = commandLine;
                    //process.StartInfo.UseShellExecute = false;
                    //process.StartInfo.RedirectStandardOutput = true;
                    //process.StartInfo.RedirectStandardError = true;
                    //process.Start();
                    //error = process.StandardError.ReadToEnd();
                    //output = process.StandardOutput.ReadToEnd();
                    //if (output != "")
                    //{
                    //    Console.ReadKey();
                    //}
                    //process.WaitForExit();
                    //System.IO.File.Delete(fileName);
                    //Console.WriteLine(triggerName);

                    /*triggerName = "sp" + tableName.Replace("tbl", "") + "Delete";
                    trig = new StringBuilder();
                    trig.AppendLine(String.Format("If (Select Count(*) From sys.procedures Where name = '{0}') > 0", triggerName));
                    trig.AppendLine(String.Format("Drop Procedure {0}", triggerName));
                    trig.AppendLine("Go");
                    String procedureName = "Create Procedure " + triggerName + "(";
                    foreach (tblTablePrimaryKey key in pkKeys)
                        procedureName += String.Format("@{0} {1}, ", key.tblColumn.ColumnName, key.tblColumn.tblColumnDataType.DataType);

                    procedureName += "@UserID Int)";
                    trig.AppendLine(procedureName);
                    trig.AppendLine(String.Empty);
                    trig.AppendLine("As");
                    trig.AppendLine(String.Empty);
                    String declare = "Declare ";
                    foreach (tblColumn col in columns)
                    {
                        if (col.ColumnName != "UserID")
                        {
                            List<tblTablePrimaryKey> pKeyFind = pkKeys.FindAll(x => x.ColumnID == col.ColumnID).ToList();
                            if (pKeyFind.Count == 0)
                                declare += String.Format("@{0} {1}", col.ColumnName, col.tblColumnDataType.DataType) + ", ";
                        }
                    }

                    trig.AppendLine(declare);
                    trig.AppendLine("		@TableID Int, @ColumnID Int, @AuditHeaderID Int,");
                    trig.AppendLine("		@ErrorMessage NVarChar(Max), @RecordDeleteCount Int");
                    trig.AppendLine(String.Empty);
                    String select = "Select ";
                    foreach (tblColumn col in columns)
                    {
                        List<tblTablePrimaryKey> pKeyFind = pkKeys.FindAll(x => x.ColumnID == col.ColumnID).ToList();
                        if (pKeyFind.Count == 0)
                            select += String.Format("@{0} = {0},\n", col.ColumnName);
                    }

                    select = select.Substring(0, select.Length - 2);
                    select += String.Format("  From {0}\n", tableName);
                    select += String.Format(" Where {0} = @{0}\n", pkKeys[0].tblColumn.ColumnName);
                    for (Int32 pkLoop = 1; pkLoop < pkKeys.Count; pkLoop++)
                        select += String.Format("   And {0} = @{0}\n", pkKeys[pkLoop].tblColumn.ColumnName);

                    trig.AppendLine(select);
                    trig.AppendLine(String.Empty);
                    trig.AppendLine("Begin Try");
                    trig.AppendLine("	Select @TableID = TableiD");
                    trig.AppendLine("	  From tblTable");
                    trig.AppendLine(String.Format("	 Where TableName = '{0}'", tableName));
                    trig.AppendLine(String.Empty);
                    trig.AppendLine("	Insert Into tblDeleteControl (TableID, UserID) Values (@TableID, @UserID)");
                    select = String.Format("Delete From {0}\n", tableName);
                    select += String.Format(" Where {0} = @{0}\n", pkKeys[0].tblColumn.ColumnName);
                    for (Int32 pkLoop = 1; pkLoop < pkKeys.Count; pkLoop++)
                        select += String.Format("   And {0} = @{0}\n", pkKeys[pkLoop].tblColumn.ColumnName);

                    trig.AppendLine(select);
                    trig.AppendLine("	Select @RecordDeleteCount = @@ROWCOUNT");
                    trig.AppendLine("	Delete From tblDeleteControl Where TableID = @TableID");
                    trig.AppendLine(String.Empty);
                    trig.AppendLine("	if @RecordDeleteCount > 0");
                    trig.AppendLine("		Begin");
                    trig.AppendLine("			Insert Into tblAuditHeader ");
                    trig.AppendLine("			(TableID, AuditDateTime, ActionID, UserID)");
                    trig.AppendLine("			Values");
                    trig.AppendLine("			(@TableID, GetDate(), 3, @UserID)");
                    trig.AppendLine(String.Empty);
                    trig.AppendLine(String.Format("			Select @AuditHeaderID = IDENT_CURRENT('tblAuditHeader')"));
                    trig.AppendLine(String.Empty);
                    foreach (tblColumn col in columns)
                    {
                        trig.AppendLine("			Select @ColumnID = ColumnID");
                        trig.AppendLine("			  From tblColumn");
                        trig.AppendLine("			 Where TableID = @TableID");
                        trig.AppendLine(String.Format("			   And ColumnName = '{0}'", col.ColumnName));
                        trig.AppendLine(String.Empty);
                        trig.AppendLine("			Insert Into tblAuditLine");
                        trig.AppendLine("			(HeaderID, ColumnID, ValueBefore, ValueAfter)");
                        trig.AppendLine("			Values");
                        trig.AppendLine(String.Format("			(@AuditHeaderID, @ColumnID, @{0}, '')", col.ColumnName));
                        trig.AppendLine(String.Empty);
                    }

                    trig.AppendLine("		End");
                    trig.AppendLine("End Try");
                    trig.AppendLine("Begin Catch");
                    trig.AppendLine("	Select @ErrorMessage = ERROR_MESSAGE();");
                    trig.AppendLine("	raiserror(@ErrorMessage, 16, 15);");
                    trig.AppendLine("End Catch");

                    fileName = @"C:\Users\Christo\Code\Trident\Documents\\" + triggerName + ".sql";
                    if (System.IO.File.Exists(fileName))
                        System.IO.File.Delete(fileName);

                    writer = new System.IO.StreamWriter(fileName);
                    writer.Write(trig.ToString());
                    writer.Close();
                    fInfo = new System.IO.FileInfo(fileName);
                    commandLine = "-S DESKTOP-EABVF26 -d Trident -E -i " + fInfo.FullName;
                    process = new System.Diagnostics.Process();
                    process.StartInfo.FileName = "sqlcmd";
                    process.StartInfo.Arguments = commandLine;
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.RedirectStandardError = true;
                    process.Start();
                    error = process.StandardError.ReadToEnd();
                    output = process.StandardOutput.ReadToEnd();
                    if (output != "")
                    {
                        Console.ReadKey();
                    }
                    process.WaitForExit();
                    System.IO.File.Delete(fileName);
                    Console.WriteLine(triggerName);

                    triggerName = tableName + "DelAudit";
                    trig = new StringBuilder();
                    trig.AppendLine(String.Format("If (Select Count(*) From sys.triggers Where name = '{0}') > 0", triggerName));
                    trig.AppendLine(String.Format("Drop Trigger {0}", triggerName));
                    trig.AppendLine("Go");
                    trig.AppendLine(String.Empty);
                    trig.AppendLine(String.Format("Create Trigger {0} On {1} Instead Of Delete", triggerName, tableName));
                    trig.AppendLine("As");
                    declare = "Declare @TableID Int, ";
                    foreach (tblColumn col in columns)
                    {
                        if (col.ColumnName != "UserID")
                        {
                            List<tblTablePrimaryKey> pKeyFind = pkKeys.FindAll(x => x.ColumnID == col.ColumnID).ToList();
                            if (pKeyFind.Count > 0)
                                declare += String.Format("@{0} {1}", col.ColumnName, col.tblColumnDataType.DataType) + ", ";
                        }
                    }
                    declare = declare.Substring(0, declare.Length - 2);
                    trig.AppendLine(declare);
                    trig.AppendLine(String.Empty);
                    trig.AppendLine("Select @TableID = TableID");
                    trig.AppendLine("  From tblTable");
                    trig.AppendLine(String.Format(" Where TableName = '{0}'", tableName));
                    trig.AppendLine(String.Empty);
                    select = "Select ";
                    foreach (tblTablePrimaryKey key in pkKeys)
                        select += String.Format("@{0} = {0},", key.tblColumn.ColumnName);

                    select = select.Substring(0, select.Length - 1) + " From deleted";
                    trig.AppendLine(select);
                    trig.AppendLine(String.Empty);
                    trig.AppendLine("If (Select Count(*) From tblDeleteControl Where TableID = @TableID) = 0");
                    trig.AppendLine("	Begin");
                    trig.AppendLine(String.Format("		raiserror('You cannot delete a record directly. Please use the sp{0}Delete stored procedure', 16, 15);", tableName.Replace("tbl", "")));
                    trig.AppendLine("		Rollback Tran");
                    trig.AppendLine("		return");
                    trig.AppendLine("	End");
                    trig.AppendLine("Else");
                    select = String.Format("Delete From {0} Where ", tableName);
                    select += String.Format("{0} = @{0}", pkKeys[0].tblColumn.ColumnName);
                    for (Int32 pkLoop = 1; pkLoop < pkKeys.Count; pkLoop++)
                        select += String.Format(" And {0} = @{0} ", pkKeys[pkLoop].tblColumn.ColumnName);

                    trig.AppendLine(select);
                    fileName = @"C:\Users\Christo\Code\Trident\Documents\\" + triggerName + ".sql";
                    if (System.IO.File.Exists(fileName))
                        System.IO.File.Delete(fileName);

                    writer = new System.IO.StreamWriter(fileName);
                    writer.Write(trig.ToString());
                    writer.Close();
                    fInfo = new System.IO.FileInfo(fileName);
                    commandLine = "-S DESKTOP-EABVF26 -d Trident -E -i " + fInfo.FullName;
                    process = new System.Diagnostics.Process();
                    process.StartInfo.FileName = "sqlcmd";
                    process.StartInfo.Arguments = commandLine;
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.RedirectStandardError = true;
                    process.Start();
                    error = process.StandardError.ReadToEnd();
                    output = process.StandardOutput.ReadToEnd();
                    if (output != "")
                    {
                        Console.ReadKey();
                    }
                    process.WaitForExit();
                    System.IO.File.Delete(fileName);*/
                    Console.WriteLine(triggerName);
                    //}
                    //}
                }
            }
        }

        public tblTable GetTable(Byte pSystemID, Int32 pTableID)
        {
            try
            {
                UpdateTempMethod("GetTable", "(Byte pSystemID, Int32 pTableID)", true);
                recordFound = false;
                tblTableCrud crud = new tblTableCrud();
                crud.Where("SystemID", DAL.General.Operator.Equals, pSystemID);
                crud.And("TableID", DAL.General.Operator.Equals, pTableID);

                tblTable search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetTable", "(Byte pSystemID, Int32 pTableID)", false);
                throw new Exception("GetTableByte - " + Ex.Message);
            }
        }
        public tblTable GetTable(Int32 pTableID)
        {
            try
            {
                recordFound = false;
                tblTableCrud crud = new tblTableCrud();
                crud.Where("TableID", DAL.General.Operator.Equals, pTableID);
                tblTable search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTableByte - " + Ex.Message);
            }
        }
		public List<tblTable> GetTable(Byte pSystemID)
        {
            try
            {
                UpdateTempMethod("GetTable", "(Byte pSystemID)", true);
                recordFound = false;
                tblTableCrud crud = new tblTableCrud();
                crud.Where("SystemID", DAL.General.Operator.Equals, pSystemID);

                List<tblTable> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetTable", "(Byte pSystemID)", false);
                throw new Exception("GetTableByte - " + Ex.Message);
            }
        }
    public tblTable GetTable(Byte pSystemID, String pTableName)
        {
            try
            {
                UpdateTempMethod("GetTable", "(Byte pSystemID, String pTableName)", true);
                recordFound = false;
                tblTableCrud crud = new tblTableCrud();
                crud.Where("SystemID", DAL.General.Operator.Equals, pSystemID);
                crud.And("TableName", DAL.General.Operator.Equals, pTableName);

                tblTable search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetTable", "(Byte pSystemID, String pTableName)", false);
                throw new Exception("GetTableByte - " + Ex.Message);
            }
        }
    public Int32 InsertTable(Byte pSystemID, String pTableName, Boolean pDoAudit, String pUserName)
        {
            try
            {
                tblTableCrud crud = new tblTableCrud();
                tblTable table = new tblTable();
                table.ChangeDateTime = DateTime.Now;
                table.DoAudit = pDoAudit;
                table.SystemID = pSystemID;
                table.TableName = pTableName;
                table.UserName = pUserName;
                long ID = 0;
                crud.Insert(table, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTableByte - " + Ex.Message);
            }
        }
	public tblColumn GetColumnPerColumnID(Int32 pColumnID)
        {
            try
            {
                UpdateTempMethod("GetColumnPerColumnID", "(Int32 pColumnID)", true);
                recordFound = false;
                tblColumnCrud crud = new tblColumnCrud();
                crud.Where("ColumnID", DAL.General.Operator.Equals, pColumnID);

                tblColumn search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetColumnPerColumnID", "(Int32 pColumnID)", false);
                throw new Exception("GetColumnPerColumnID - " + Ex.Message);
            }
        }
    public tblColumn GetColumn(Int32 pTableID, String pColumnName)
        {
            try
            {
                UpdateTempMethod("GetColumn", "(Int32 pTableID, String pColumnName)", true);
                recordFound = false;
                tblColumnCrud crud = new tblColumnCrud();
                crud.Where("TableID", DAL.General.Operator.Equals, pTableID);
                crud.And("ColumnName", DAL.General.Operator.Equals, pColumnName);

                tblColumn search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetColumn", "(Int32 pTableID, String pColumnName)", false);
                throw new Exception("GetColumn - " + Ex.Message);
            }
        }
    public Boolean DeleteColumn(Int32 pColumnID)
        {
            try
            {
                tblColumnCrud crud = new tblColumnCrud();
                crud.Where("ColumnID", DAL.General.Operator.Equals, pColumnID);

                List<tblColumn> search = crud.ReadMulti().ToList();
                foreach (tblColumn del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteColumn - " + Ex.Message);
            }
        }
    public List<tblColumn> GetColumn(Int32 pTableID)
        {
            try
            {
                UpdateTempMethod("GetColumn", "(Int32 pTableID)", true);
                recordFound = false;
                tblColumnCrud crud = new tblColumnCrud();
                crud.Where("TableID", DAL.General.Operator.Equals, pTableID);

                List<tblColumn> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetColumn", "(Int32 pTableID)", false);
                throw new Exception("GetColumn - " + Ex.Message);
            }
        }
        public Int32 UpdateColumn(Int32 pColumnID, Int32 pDataTypeID, Int32 pColumnLength, Boolean pDoAudit, Int32 pColumnPrecision, Boolean pIsNullable, Int32 pOrdinalNo, Int32 pColumnScale, String pUserName)
        {
            try
            {
                tblColumnCrud crud = new tblColumnCrud();
                tblColumn workRec = new tblColumn();
                crud.Where("ColumnID", DAL.General.Operator.Equals, pColumnID);
                workRec = crud.ReadSingle();
                workRec.DataTypeID = pDataTypeID;
                workRec.ColumnLength = pColumnLength;
                workRec.DoAudit = pDoAudit;
                workRec.ColumnPrecision = pColumnPrecision;
                workRec.IsNullable = pIsNullable;
                workRec.OrdinalNo = pOrdinalNo;
                workRec.ColumnScale = pColumnScale;
                workRec.UserName = pUserName;
                crud.Update(workRec);
                return pColumnID;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateColumn - " + Ex.Message);
            }
        }
    public Int32 InsertColumn(Int32 pTableID, String pColumnName, Int32 pDataTypeID, Int32 pColumnLength, Boolean pDoAudit, Int32 pColumnPrecision, Boolean pIsNullable, Int32 pOrdinalNo, String pUserName, Int32 pColumnScale, String pColumnDefault)
        {
            try
            {
                tblColumnCrud crud = new tblColumnCrud();
                tblColumn col = new tblColumn();
                col.ChangeDateTime = DateTime.Now;
                col.ColumnDefault = pColumnDefault;
                col.ColumnLength = pColumnLength;
                col.ColumnName = pColumnName;
                col.ColumnPrecision = pColumnPrecision;
                col.ColumnScale = pColumnScale;
                col.DataTypeID = pDataTypeID;
                col.DoAudit = pDoAudit;
                col.IsNullable = pIsNullable;
                col.OrdinalNo = pOrdinalNo;
                col.TableID = pTableID;
                col.UserName = pUserName;
                long ID;
                crud.Insert(col, out ID);

                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertColumn - " + Ex.Message);
            }
        }
	public System.Data.SqlDbType GetSqlDbType(Int32 pDataTypeID)
	{
		try
		{
                UpdateTempMethod("GetSqlDbType", "(Int32 pDataTypeID)", true);
				System.Data.SqlDbType dataType = System.Data.SqlDbType.VarChar;

			switch (pDataTypeID)
			{
					case 1: dataType = System.Data.SqlDbType.BigInt; break;
					case 2: dataType = System.Data.SqlDbType.Binary; break;
					case 3: dataType = System.Data.SqlDbType.Bit; break;
					case 4: dataType = System.Data.SqlDbType.Char; break;
					case 5: dataType = System.Data.SqlDbType.Date; break;
					case 6: dataType = System.Data.SqlDbType.DateTime; break;
					case 7: dataType = System.Data.SqlDbType.DateTime2; break;
					case 8: dataType = System.Data.SqlDbType.Decimal; break;
					case 9: dataType = System.Data.SqlDbType.Float; break;
					case 10: dataType = System.Data.SqlDbType.Int; break;
					case 11:  dataType = System.Data.SqlDbType.Money; break;
					case 13: dataType = System.Data.SqlDbType.SmallInt; break;
					case 14: dataType = System.Data.SqlDbType.TinyInt; break;
					case 15: dataType = System.Data.SqlDbType.VarBinary; break;
					case 16: dataType = System.Data.SqlDbType.VarChar; break;
					case 17: dataType = System.Data.SqlDbType.Image; break;
					case 18: dataType = System.Data.SqlDbType.NVarChar; break;
					case 19: dataType = System.Data.SqlDbType.UniqueIdentifier; break;
					case 20: dataType = System.Data.SqlDbType.SmallDateTime; break;
			}
				return dataType;
		}
		catch (Exception Ex)
		{
                UpdateTempMethod("GetSqlDbType", "(Int32 pDataTypeID)", false);
				throw new Exception(errorMessage + "GetSqlDbType-" + Ex.Message);
		}
	}
		public tblColumnDataType GetColumnDataType(Int32 pDataTypeID)
        {
            try
            {
                UpdateTempMethod("GetColumnDataType", "(Int32 pDataTypeID)", true);
                recordFound = false;
                tblColumnDataTypeCrud crud = new tblColumnDataTypeCrud();
                crud.Where("DataTypeID", DAL.General.Operator.Equals, pDataTypeID);

                tblColumnDataType search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetColumnDataType", "(Int32 pDataTypeID)", false);
                throw new Exception("GetColumnDataType - " + Ex.Message);
            }
        }
		public tblColumnDataType GetColumnDataType(String pDataType)
        {
            try
            {
                UpdateTempMethod("GetColumnDataType", "(String pDataType)", true);
                recordFound = false;
                tblColumnDataTypeCrud crud = new tblColumnDataTypeCrud();
                crud.Where("DataType", DAL.General.Operator.Equals, pDataType);

                tblColumnDataType search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetColumnDataType", "(String pDataType)", false);
                throw new Exception("GetColumnDataType - " + Ex.Message);
            }
        }
	public Boolean CollectTableInformation(short pSystemID, String pTableName)
    {

			String tableName = "";
			try
			{
				tblSystem system = GetSystem(pSystemID);
					if (system.SystemID.ToString() != "5")
					{
						Byte systemID = Convert.ToByte(system.SystemID);
						List<Table> tables = new List<Table>();
						SqlConnection openConn = new SqlConnection();
						if (system.ConnectionString != "")
						{
							openConn = new SqlConnection(ConfigurationManager.ConnectionStrings[system.ConnectionString].ConnectionString);
							openConn.Open();
							String comd = "Select Name, object_id From sys.tables where name='" + pTableName + "'";
							SqlCommand dbComd = new SqlCommand(comd, openConn);
							SqlDataReader dataReader = dbComd.ExecuteReader();

							while (dataReader.Read())
							{
								Table table = new Table();
								table.Name = dataReader.GetValue(0).ToString();
								table.ObjectID = Convert.ToInt32(dataReader.GetValue(1).ToString());
								tables.Add(table);
							}
							dataReader.Close();

							foreach (Table table in tables)
							{
								tableName = table.Name;
								Int32 tableID = 0;
								String objectID = table.ObjectID.ToString();
								//Check if table exists and add if not
								tblTable checkTable = GetTable(systemID, table.Name);
								if (!RecordFound)
								{
									tableID = InsertTable(systemID, table.Name, true, "christo741");
									checkTable = GetTable(systemID, table.Name);
								}
								else
									tableID = Convert.ToInt32(checkTable.TableID);

								if (openConn.State == System.Data.ConnectionState.Closed)
									openConn.Open();

								comd = "Select A.name, A.max_length, A.precision, A.is_nullable, B.name, A.column_id, A.Scale, IsNull(object_definition(A.default_object_id), '')\n";
								comd += "  From sys.columns A Inner Join sys.types B On A.system_type_id = B.user_type_id\n";
								comd += " Where object_id = " + objectID;
								dbComd = new SqlCommand(comd, openConn);
								dataReader = dbComd.ExecuteReader();

								List<String> dbColName = new List<String>();
								while (dataReader.Read())
								{
									Int32 columnID = 0;
									String colName = dataReader.GetValue(0).ToString();
									dbColName.Add(colName);
									Int32 colLength = Convert.ToInt32(dataReader.GetValue(1));
									Int32 precision = Convert.ToInt32(dataReader.GetValue(2));
									Boolean isNullable = Convert.ToBoolean(dataReader.GetValue(3));
									String dataType = dataReader.GetValue(4).ToString();
									Int32 ordinalNo = Convert.ToInt32(dataReader.GetValue(5));
									Int32 scale = Convert.ToInt32(dataReader.GetValue(6));
									Int32 dataTypeID = Convert.ToInt32(GetColumnDataType(dataType).DataTypeID);
									String colDefault = dataReader.GetValue(7).ToString();
									tblColumn col = GetColumn(tableID, colName);
									if (!RecordFound)
									{
										columnID = InsertColumn(tableID, colName, dataTypeID, colLength, false, precision, isNullable, ordinalNo, "christo741", scale, colDefault);
									}
									else
									{
										Boolean updateCol = false;
										columnID = Convert.ToInt32(col.ColumnID);
										if (Convert.ToInt32(col.ColumnLength) != colLength) updateCol = true;
										if (Convert.ToInt32(col.ColumnPrecision) != precision) updateCol = true;
										if (Convert.ToBoolean(col.IsNullable) != isNullable) updateCol = true;
										if (Convert.ToInt32(col.DataTypeID) != dataTypeID) updateCol = true;
										if (Convert.ToInt32(col.OrdinalNo) != ordinalNo) updateCol = true;
										if (Convert.ToInt32(col.ColumnScale) != scale) updateCol = true;
										if (updateCol)
											UpdateColumn(columnID, dataTypeID, colLength, Convert.ToBoolean(col.DoAudit), precision, isNullable, ordinalNo, scale, "christo741");
									}
								}
								dataReader.Close();

								List<tblColumn> columns = GetColumn(tableID);
								foreach (tblColumn col in columns)
								{
									List<String> colSeek = dbColName.FindAll(x => x == col.ColumnName).ToList();
									if (colSeek.Count == 0)
									{
										//Christo Smith 30/03/2016
										//Should probably put code here to delete future audit tables
										DeleteColumn(Convert.ToInt32(col.ColumnID));
									}
								}

								#region Collect Primary Key Information
								comd = "Select A.CONSTRAINT_NAME, B.COLUMN_NAME, C.ORDINAL_POSITION\n";
								comd += "  From INFORMATION_SCHEMA.TABLE_CONSTRAINTS A Inner Join INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B On A.CONSTRAINT_NAME = B.CONSTRAINT_NAME And A.TABLE_NAME = B.TABLE_NAME\n";
								comd += " Inner Join INFORMATION_SCHEMA.COLUMNS C On C.COLUMN_NAME = B.COLUMN_NAME And C.TABLE_NAME = B.TABLE_NAME\n";
								comd += " Where A.TABLE_NAME = '" + table.Name + "'\n";
								comd += "   And A.CONSTRAINT_TYPE = 'PRIMARY KEY'\n";
								dbComd.CommandText = comd;
								dataReader = dbComd.ExecuteReader();

								while (dataReader.Read())
								{
									String pkName = dataReader.GetValue(0).ToString();
									String colName = dataReader.GetValue(1).ToString();
									Byte ordinal = Convert.ToByte(dataReader.GetValue(2));
									Int32 primaryKeyID = 0;
									tblTablePrimaryKey primaryKey = GetTablePrimaryKey(systemID, table.Name);
									if (!RecordFound)
										primaryKeyID = InsertTablePrimaryKey(Convert.ToInt32(checkTable.TableID), pkName);
									else
										primaryKeyID = Convert.ToInt32(primaryKey.PrimaryKeyID);

									tblColumn col = GetColumn(Convert.ToInt32(checkTable.TableID), colName);
									tblTablePrimaryKeyColumn pkCol = GetTablePrimaryKeyColumn(primaryKeyID, Convert.ToInt32(col.ColumnID));
									Int32 pkColumnID = 0;
									if (!RecordFound)
										pkColumnID = InsertTablePrimaryKeyColumn(primaryKeyID, Convert.ToInt32(col.ColumnID), ordinal);


								}
								dataReader.Close();
								#endregion
								#region Collect Index Information
								comd = "Select IndexName = ind.name, ColumnName = col.name, ind.is_unique,  ind.type_desc, ind.fill_factor, ic.key_ordinal, ic.is_descending_key\n";
								comd += "  From sys.indexes ind Inner Join sys.index_columns ic ON ind.object_id = ic.object_id and ind.index_id = ic.index_id";
								comd += " Inner Join sys.columns col ON ic.object_id = col.object_id and ic.column_id = col.column_id";
								comd += " Inner Join sys.tables t ON ind.object_id = t.object_id";
								comd += " Where ind.is_primary_key = 0";
								comd += "   And t.name = '" + table.Name + "'";
								comd += "Order By t.name, ind.name, ind.index_id, ic.index_column_id";
								dbComd.CommandText = comd;
								dataReader = dbComd.ExecuteReader();
								while (dataReader.Read())
								{
									String indexName = dataReader.GetValue(0).ToString();
									String columnName = dataReader.GetValue(1).ToString();
									Boolean isUnique = Convert.ToBoolean(Convert.ToInt32(dataReader.GetValue(2)));
									String typeDesc = dataReader.GetValue(3).ToString();
									Boolean isClustred = false;
									if (typeDesc == "CLUSTERED") isClustred = true;
									Byte fillFactor = Convert.ToByte(dataReader.GetValue(4));
									Byte ordinal = Convert.ToByte(dataReader.GetValue(5));
									String direction = "Asc";
									if (dataReader.GetValue(6).ToString() == "1")
										direction = "Desc";

									Int32 indexID = 0;
									tblTableIndex index = GetTableIndex(indexName);
									if (!RecordFound)
										indexID = InsertTableIndex(Convert.ToInt32(checkTable.TableID), indexName, isUnique, isClustred, fillFactor);
									else
										indexID = Convert.ToInt32(index.IndexID);

									tblColumn col = GetColumn(Convert.ToInt32(checkTable.TableID), columnName);
									tblTableIndexColumn indexCol = GetTableIndexColumn(indexID, Convert.ToInt32(col.ColumnID));
									Int32 indexColID = 0;
									if (!RecordFound)
										indexColID = InsertTableIndexColumn(indexID, Convert.ToInt32(col.ColumnID), ordinal, direction);

								}
								dataReader.Close();
								#endregion
							}

							foreach (Table table in tables)
							{
								#region Collect Foreign Key Information
								tblTable checkTable = GetTable(systemID, table.Name);

								comd = "Select obj.name AS FK_NAME, tab1.name AS[table], col1.name AS[column], tab2.name AS[referenced_table], col2.name AS[referenced_column]\n";
								comd += "   From sys.foreign_key_columns fkc Inner Join sys.objects obj On obj.object_id = fkc.constraint_object_id\n";
								comd += "  Inner Join sys.tables tab1 On tab1.object_id = fkc.parent_object_id\n";
								comd += "  Inner Join sys.schemas sch On tab1.schema_id = sch.schema_id\n";
								comd += "  Inner Join sys.columns col1 On col1.column_id = parent_column_id AND col1.object_id = tab1.object_id\n";
								comd += "  Inner Join sys.tables tab2 On tab2.object_id = fkc.referenced_object_id\n";
								comd += "  Inner Join sys.columns col2 On col2.column_id = referenced_column_id AND col2.object_id = tab2.object_id\n";
								comd += "  Where tab1.name = '" + table.Name + "'\n";
								dbComd.CommandText = comd;
								dataReader = dbComd.ExecuteReader();

								while (dataReader.Read())
								{
									String foreignKeyName = dataReader.GetValue(0).ToString();
									String columnName = dataReader.GetValue(2).ToString();
									String refTableName = dataReader.GetValue(3).ToString();
									String refColumnName = dataReader.GetValue(4).ToString();
									Int32 refTableID = Convert.ToInt32(GetTable(systemID, refTableName).TableID);
									tblTableForeignKey foreignKey = GetTableForeignKey(Convert.ToInt32(checkTable.TableID), foreignKeyName);
									Int32 foreignKeyID = 0;
									if (!RecordFound)
										foreignKeyID = InsertTableForeignKey(Convert.ToInt32(checkTable.TableID), refTableID, foreignKeyName);
									else
										foreignKeyID = Convert.ToInt32(foreignKey.ForeignKeyID);

									tblColumn columnID = GetColumn(Convert.ToInt32(checkTable.TableID), columnName);
									tblTableForeignKeyColumn foreignKeyColumn = GetTableForeignKeyColumn(foreignKeyID, Convert.ToInt32(columnID.ColumnID));
									if (!RecordFound)
									{
										tblColumn refColID = GetColumn(refTableID, refColumnName);
										InsertTableForeignKeyColumn(foreignKeyID, Convert.ToInt32(columnID.ColumnID), Convert.ToInt32(refColID.ColumnID));
									}

								}
								dataReader.Close();
							}
							#endregion
						}
					}
				return true;
			}

			catch (Exception Ex)
			{
                UpdateTempMethod("CollectTableInformation", "(short pSystemID, String pTableName)", false);
				throw new Exception(errorMessage + "CollectTableInformation-" + Ex.Message);
			}
		
    }
        public Int32 InsertTableIndex(Int32 pTableID, String pIndexName, Boolean pIsUnique, Boolean pIsClustered, Byte pIndexFillFactor)
        {
            try
            {
                tblTableIndexCrud crud = new tblTableIndexCrud();
                tblTableIndex workRec = new tblTableIndex();
                workRec.IndexFillFactor = pIndexFillFactor;
                workRec.IndexName = pIndexName;
                workRec.IsClustered = pIsClustered;
                workRec.IsUnique = pIsUnique;
                workRec.TableID = pTableID;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;


            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTableIndex - " + Ex.Message);
            }
        }
	public tblTableIndexColumn GetTableIndexColumn(Int32 pIndexID, Int32 pColumnID)
        {
            try
            {
                UpdateTempMethod("GetTableIndexColumn", "(Int32 pIndexID, Int32 pColumnID)", true);
                recordFound = false;
                tblTableIndexColumnCrud crud = new tblTableIndexColumnCrud();
                crud.Where("IndexID", DAL.General.Operator.Equals, pIndexID);
                crud.And("ColumnID", DAL.General.Operator.Equals, pColumnID);

                tblTableIndexColumn search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetTableIndexColumn", "(Int32 pIndexID, Int32 pColumnID)", false);
                throw new Exception("GetTableIndexColumn - " + Ex.Message);
            }
        }
	public Int32 InsertTableIndexColumn(Int32 pIndexID, Int32 pColumnID, Byte pOrdinal, String pSortDirection)
        {
            try
            {
                tblTableIndexColumnCrud crud = new tblTableIndexColumnCrud();
                tblTableIndexColumn workRec = new tblTableIndexColumn();
                workRec.ColumnID = pColumnID;
                workRec.IndexID = pIndexID;
                workRec.Ordinal = pOrdinal;
                workRec.SortDirection = pSortDirection;
                long ID = 0;
                crud.Insert(workRec, out ID);

                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTableIndexColumn - " + Ex.Message);
            }
        }
        public Int32 InsertTablePrimaryKey(Int32 pTableID, String pPKName)
        {
            try
            {
                tblTablePrimaryKeyCrud crud = new tblTablePrimaryKeyCrud();
                tblTablePrimaryKey pk = new tblTablePrimaryKey();
                pk.PKName = pPKName;
                pk.TableID = pTableID;
                long ID = 0;
                crud.Insert(pk, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTablePrimaryKey - " + Ex.Message);
            }
        }
        public Int32 InsertTablePrimaryKeyColumn(Int32 pPrimaryKeyID, Int32 pColumnID, Byte pOrdinal)
        {
            try
            {
                tblTablePrimaryKeyColumnCrud crud = new tblTablePrimaryKeyColumnCrud();
                tblTablePrimaryKeyColumn col = new tblTablePrimaryKeyColumn();
                col.ColumnID = pColumnID;
                col.Ordinal = pOrdinal;
                col.PrimaryKeyID = pPrimaryKeyID;
                long ID = 0;
                crud.Insert(col, out ID);
                return (Int32)ID;
                
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTablePrimaryKeyColumn - " + Ex.Message);
            }
        }

        public tblTablePrimaryKey GetTablePrimaryKeyByID(Int32 pPrimaryKeyID)
        {
            try
            {
                tblTablePrimaryKeyCrud crud = new tblTablePrimaryKeyCrud();
                crud.Where("PrimaryKeyID", DAL.General.Operator.Equals, pPrimaryKeyID);
                recordFound = false;
                tblTablePrimaryKey results = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return results;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetTablePrimaryKey-" + Ex.Message);
            }
        }

        public List<tblTablePrimaryKey> GetTablePrimaryKey(Int32 pTableID)
        {
            try
            {
                tblTable table = GetTable(pTableID);
                tblTablePrimaryKeyCrud crud = new tblTablePrimaryKeyCrud();
                crud.Where("TableID", DAL.General.Operator.Equals, (Int32)table.TableID);
                recordFound = false;
                List<tblTablePrimaryKey> results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                
                return results;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetTablePrimaryKey-" + Ex.Message);
            }
        }

        public tblTablePrimaryKey GetTablePrimaryKey(Byte pSystemID, String pTableName)
		{
			try
			{
                UpdateTempMethod("GetTablePrimaryKey", "(Byte pSystemID, String pTableName)", true);
				tblTable table = GetTable(pSystemID, pTableName);
				tblTablePrimaryKeyCrud crud = new tblTablePrimaryKeyCrud();
				crud.Where("TableID", DAL.General.Operator.Equals, (Int32)table.TableID);
				recordFound = false;
				List<tblTablePrimaryKey> results = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				if (results.Count > 0)
					return results[0];

				return null;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetTablePrimaryKey", "(Byte pSystemID, String pTableName)", false);
				throw new Exception(errorMessage + "GetTablePrimaryKey-" + Ex.Message);
			}
		}

        public List<tblTablePrimaryKeyColumn> GetTablePrimaryKeyColumn(Int32 pPrimaryKeyID)
        {
            try
            {
                tblTablePrimaryKey primaryKey = GetTablePrimaryKeyByID(pPrimaryKeyID);
                recordFound = false;
                tblTablePrimaryKeyColumnCrud crud = new tblTablePrimaryKeyColumnCrud();
                crud.Where("PrimaryKeyID", DAL.General.Operator.Equals, (Int32)primaryKey.PrimaryKeyID);
                List<tblTablePrimaryKeyColumn> results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return results;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetTablePrimaryKeyColumn-" + Ex.Message);
            }
        }


        public List<tblAuditHeader> GetAuditHeader(Int32 pTableID, DateTime pAuditDate)
        {
            try
            {
                tblAuditHeaderCrud crud = new tblAuditHeaderCrud();
                crud.Where("TableID", DAL.General.Operator.Equals, pTableID);
                DateTime startDate = Convert.ToDateTime(pAuditDate.Year.ToString() + "-" + pAuditDate.Month.ToString().PadLeft(2, '0') + "-" + pAuditDate.Day.ToString().PadLeft(2, '0'));
                DateTime endDate = Convert.ToDateTime(pAuditDate.Year.ToString() + "-" + pAuditDate.Month.ToString().PadLeft(2, '0') + "-" + pAuditDate.Day.ToString().PadLeft(2, '0') + " 23:59:59");
                crud.And("AuditDateTime", General.Operator.Between, startDate, endDate);
                recordFound = false;
                List<tblAuditHeader> results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return results;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetAuditHeader -" + Ex.Message);
            }
        }

        public List<tblTablePrimaryKeyColumn> GetTablePrimaryKeyColumn(Byte pSystemID, String pTableName)
		{
			try
			{
                UpdateTempMethod("GetTablePrimaryKeyColumn", "(Byte pSystemID, String pTableName)", true);
				tblTablePrimaryKey primaryKey = GetTablePrimaryKey(pSystemID, pTableName);
				recordFound = false;
				tblTablePrimaryKeyColumnCrud crud = new tblTablePrimaryKeyColumnCrud();
				crud.Where("PrimaryKeyID", DAL.General.Operator.Equals, (Int32)primaryKey.PrimaryKeyID);
				List<tblTablePrimaryKeyColumn> results = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return results;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetTablePrimaryKeyColumn", "(Byte pSystemID, String pTableName)", false);
				throw new Exception(errorMessage + "GetTablePrimaryKeyColumn-" + Ex.Message);
			}
		}
		public tblTablePrimaryKeyColumn GetTablePrimaryKeyColumn(Int32 pPrimaryKeyID, Int32 pColumnID)
        {
            try
            {
                UpdateTempMethod("GetTablePrimaryKeyColumn", "(Int32 pPrimaryKeyID, Int32 pColumnID)", true);
                recordFound = false;
                tblTablePrimaryKeyColumnCrud crud = new tblTablePrimaryKeyColumnCrud();
                crud.Where("PrimaryKeyID", DAL.General.Operator.Equals, pPrimaryKeyID);
                crud.And("ColumnID", DAL.General.Operator.Equals, pColumnID);

                tblTablePrimaryKeyColumn search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetTablePrimaryKeyColumn", "(Int32 pPrimaryKeyID, Int32 pColumnID)", false);
                throw new Exception("GetTablePrimaryKeyColumn - " + Ex.Message);
            }
        }
        public Int32 InsertTableForeignKey(Int32 pTableID, Int32 pReferencedTableID, String pForeignKeyName)
        {
            try
            {
                tblTableForeignKeyCrud crud = new tblTableForeignKeyCrud();
                tblTableForeignKey fk = new tblTableForeignKey();
                fk.ForeignKeyName = pForeignKeyName;
                fk.ReferencedTableID = pReferencedTableID;
                fk.TableID = pTableID;
                long ID = 0;
                crud.Insert(fk, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTableForeignKey - " + Ex.Message);
            }
        }
        public Int32 InsertTableForeignKeyColumn(Int32 pForeignKeyID, Int32 pColumnID, Int32 pReferencedColumnID)
        {
            try
            {
                tblTableForeignKeyColumnCrud crud = new tblTableForeignKeyColumnCrud();
                tblTableForeignKeyColumn workRec = new tblTableForeignKeyColumn();
                workRec.ColumnID = pColumnID;
                workRec.ForeignKeyID = pForeignKeyID;
                workRec.ReferencedColumnID = pReferencedColumnID;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTableForeignKeyColumn - " + Ex.Message);
            }
        }
		public tblTableForeignKey GetTableForeignKey(Int16 pSystemID, String pForeignKeyName)
		{
			try
			{
                UpdateTempMethod("GetTableForeignKey", "(Int16 pSystemID, String pForeignKeyName)", true);
				recordFound = false;
				tblTableForeignKeyCrud search = new tblTableForeignKeyCrud();
				search.Where("ForeignKeyName", DAL.General.Operator.Equals, pForeignKeyName);
				tblTableForeignKey returnKey = new tblTableForeignKey();
				List<tblTableForeignKey> results = search.ReadMulti().ToList();
				foreach (tblTableForeignKey key in results)
				{
					if (Convert.ToInt32(key.tblTable.SystemID) == Convert.ToInt32(pSystemID))
					{
						recordFound = true;
						returnKey = key;
					}
				}

				return returnKey;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetTableForeignKey", "(Int16 pSystemID, String pForeignKeyName)", false);
				throw new Exception(errorMessage + "GetTableForeignKey(Short,String)-" + Ex.Message);
			}
		}
		public List<tblTableForeignKeyColumn> GetTableForeignKeyColumn(Int32 pSystemID, String pForeignKeyName)
		{
			try
			{
                UpdateTempMethod("GetTableForeignKeyColumn", "(Int32 pSystemID, String pForeignKeyName)", true);
				tblTableForeignKey foreignKey = GetTableForeignKey(Convert.ToInt16(pSystemID), pForeignKeyName);
				tblTableForeignKeyColumnCrud search = new tblTableForeignKeyColumnCrud();
				search.Where("ForeignKeyID", DAL.General.Operator.Equals, (Int32)foreignKey.ForeignKeyID);
				recordFound = false;
				List<tblTableForeignKeyColumn> results = search.ReadMulti().ToList();
				recordFound = search.RecordFound;
				return results;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetTableForeignKeyColumn", "(Int32 pSystemID, String pForeignKeyName)", false);
					throw new Exception(errorMessage + "GetTableForeignKeyColumn(Byte, String)-" + Ex.Message);
			}
		}
	public tblTableForeignKeyColumn GetTableForeignKeyColumn(Int32 pForeignKeyID, Int32 pColumnID)
        {
            try
            {
                UpdateTempMethod("GetTableForeignKeyColumn", "(Int32 pForeignKeyID, Int32 pColumnID)", true);
                recordFound = false;
                tblTableForeignKeyColumnCrud crud = new tblTableForeignKeyColumnCrud();
                crud.Where("ForeignKeyID", DAL.General.Operator.Equals, pForeignKeyID);
                crud.And("ColumnID", DAL.General.Operator.Equals, pColumnID);

                tblTableForeignKeyColumn search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetTableForeignKeyColumn", "(Int32 pForeignKeyID, Int32 pColumnID)", false);
                throw new Exception("GetTableForeignKeyColumn - " + Ex.Message);
            }
        }
	public List<tblTableForeignKey> GetTableForeignKey(Byte pSystemID, String pTableName)
	{
			try
			{
                UpdateTempMethod("GetTableForeignKey", "(Byte pSystemID, String pTableName)", true);
				tblTable table = GetTable(pSystemID, pTableName);
				tblTableForeignKeyCrud crud = new tblTableForeignKeyCrud();
				crud.Where("TableID", DAL.General.Operator.Equals, (Int32)table.TableID);
				recordFound = false;
				List<tblTableForeignKey> results = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return results;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetTableForeignKey", "(Byte pSystemID, String pTableName)", false);
				throw new Exception(errorMessage + "GetTableForeignKey(Byte,String)-" + Ex.Message);
			}
	}
	public tblTableForeignKey GetTableForeignKey(Int32 pTableID, String pForeignKeyName)
        {
            try
            {
                UpdateTempMethod("GetTableForeignKey", "(Int32 pTableID, String pForeignKeyName)", true);
                recordFound = false;
                tblTableForeignKeyCrud crud = new tblTableForeignKeyCrud();
                crud.Where("TableID", DAL.General.Operator.Equals, pTableID);
                crud.And("ForeignKeyName", DAL.General.Operator.Equals, pForeignKeyName);

                tblTableForeignKey search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetTableForeignKey", "(Int32 pTableID, String pForeignKeyName)", false);
                throw new Exception("GetTableForeignKey - " + Ex.Message);
            }
        }
		public List<tblTableIndexColumn> GetTableIndexColumn(Byte pSystemID, String pIndexName)
		{
			try
			{
                UpdateTempMethod("GetTableIndexColumn", "(Byte pSystemID, String pIndexName)", true);
				tblTableIndex index = GetTableIndex(pIndexName);
				tblTableIndexColumnCrud crud = new tblTableIndexColumnCrud();
				crud.Where("IndexID", DAL.General.Operator.Equals, (Int32)index.IndexID);
				recordFound = false;
				List<tblTableIndexColumn> results = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return results;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetTableIndexColumn", "(Byte pSystemID, String pIndexName)", false);
				throw new Exception(errorMessage + "GetTableIndexColumn(Byte,String)-" + Ex.Message);
			}
		}
		public List<tblTableIndex> GetTableIndex(Byte pSystemID, String pTableName)
		{
			try
			{
                UpdateTempMethod("GetTableIndex", "(Byte pSystemID, String pTableName)", true);
				tblTable table = GetTable(pSystemID, pTableName);
				tblTableIndexCrud crud = new tblTableIndexCrud();
				crud.Where("TableID", DAL.General.Operator.Equals, (Int32)table.TableID);
				recordFound = false;
				List<tblTableIndex> results = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return results;

			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetTableIndex", "(Byte pSystemID, String pTableName)", false);
				throw new Exception(errorMessage + "GetTableIndex-" + Ex.Message);
			}

		}
		public tblTableIndex GetTableIndex(String pIndexName)
        {
            try
            {
                UpdateTempMethod("GetTableIndex", "(String pIndexName)", true);
                recordFound = false;
                tblTableIndexCrud crud = new tblTableIndexCrud();
                crud.Where("IndexName", DAL.General.Operator.Equals, pIndexName);

                tblTableIndex search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetTableIndex", "(String pIndexName)", false);
                throw new Exception("GetTableIndex - " + Ex.Message);
            }
        }

		public Boolean DeleteTableInformation(Byte pSystemID, String pTableName)
		{
			try
			{
                UpdateTempMethod("DeleteTableInformation", "(Byte pSystemID, String pTableName)", true);
				SqlConnection openConn = new SqlConnection(ConfigurationManager.ConnectionStrings["backOffice_ConnectionString"].ConnectionString);
				openConn.Open();
				String comd = "Delete From tblTablePrimaryKeyColumn\n";
				comd += " Where PrimaryKeyID In(Select PrimaryKeyID From tblTablePrimaryKey Where TableID = (Select TableID From tblTable Where TableName = '" + pTableName + "' And SystemID = " + pSystemID + " And DateDiff(Minute, ChangeDateTime, GetDate()) > 15))";
				SqlCommand dbComd = new SqlCommand(comd, openConn); dbComd.ExecuteNonQuery();
				comd = "Delete From tblTableIndexColumn  Where IndexID In (Select IndexID From tblTableIndex Where TableID In (Select TableID From tblTable Where TableName = '" + pTableName + "' And SystemID = " + pSystemID + " And DateDiff(Minute, ChangeDateTime, GetDate()) > 15))";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();
				comd = "Delete From tblTableForeignKeyColumn Where ForeignKeyID In (Select ForeignKeyID From tblTableForeignKey Where TableID In (Select TableID From tblTable Where TableName = '" + pTableName + "' And SystemID = " + pSystemID + " And DateDiff(Minute, ChangeDateTime, GetDate()) > 15))";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();
				comd = "Delete From tblTableForeignKeyColumn Where ReferencedColumnID In (Select ColumnID From tblColumn Where TableID = (Select TableID From tblTable Where TableName = '" + pTableName + "' And SystemID=" + pSystemID + " And DateDiff(Minute, ChangeDateTime, GetDate()) > 15))";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();
				comd = "Delete From tblColumn Where TableID In (Select TableID From tblTable Where TableName = '" + pTableName + "'  And SystemID = " + pSystemID + " And DateDiff(Minute, ChangeDateTime, GetDate()) > 15)";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();
				comd = "Delete From tblTablePrimaryKey Where TableID In (Select TableID From tblTable Where TableName = '" + pTableName + "' And SystemID = " + pSystemID + " And DateDiff(Minute, ChangeDateTime, GetDate()) > 15)";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();
				comd = "Delete From tblTableIndex Where TableID In (Select TableID From tblTable Where TableName = '" + pTableName + "' And SystemID = " + pSystemID + " And DateDiff(Minute, ChangeDateTime, GetDate()) > 15)";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();
				comd = "Delete From tblTableForeignKey Where TableID In (Select TableID From tblTable Where TableName = '" + pTableName + "' And SystemID = " + pSystemID + " And DateDiff(Minute, ChangeDateTime, GetDate()) > 15)";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();
				comd = "Delete From tblTableForeignKey Where ReferencedTableID In (Select TableID From tblTable Where TableName = '" + pTableName + "' And SystemID = " + pSystemID + " And DateDiff(Minute, ChangeDateTime, GetDate()) > 15)";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();
				comd = "Delete From tblTable Where TableName = '" + pTableName + "' And SystemID = " + pSystemID + " And DateDiff(Minute, ChangeDateTime, GetDate()) > 15";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();

				return true;

			}
			catch (Exception Ex)
			{
                UpdateTempMethod("DeleteTableInformation", "(Byte pSystemID, String pTableName)", false);
				throw new Exception(errorMessage + "DeleteTableInformation-" + Ex.Message);
			}

		}
		public Boolean DeleteTableInformation()
	{
		try
		{
                UpdateTempMethod("DeleteTableInformation", "()", true);
				SqlConnection openConn = new SqlConnection(ConfigurationManager.ConnectionStrings["backOffice_ConnectionString"].ConnectionString);
				openConn.Open();
				String comd = "Delete From tblTablePrimaryKeyColumn";
				SqlCommand dbComd = new SqlCommand(comd, openConn); dbComd.ExecuteNonQuery();
				comd = "Delete From tblTableIndexColumn";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();
				comd = "Delete From tblTableForeignKeyColumn";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();
				comd = "Delete From tblColumn";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();
				comd = "Delete From tblTablePrimaryKey";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();
				comd = "Delete From tblTableIndex";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();
				comd = "Delete From tblTableForeignKey";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();
				comd = "Delete From tblTable";
				dbComd.CommandText = comd; dbComd.ExecuteNonQuery();

				return true;

			}
		catch (Exception Ex)
		{
                UpdateTempMethod("DeleteTableInformation", "()", false);
				throw new Exception(errorMessage + "DeleteTableInformation-" + Ex.Message);
		}

	}
public Boolean CollectTableInformation()
    {
        String tableName = "";
			String logInfo = "";
            try
            {
                List<tblSystem> systems = GetSystem();
                Console.WriteLine("SystemCount=" + systems.Count.ToString());
                foreach (tblSystem system in systems)
                {
                    Console.WriteLine("SystemForEachStart");
                    Console.WriteLine("System=" + system.vcsSystem);
                    if (system.SystemID.ToString() != "5")
                    {
                        Console.WriteLine("SystemID=" + system.SystemID.ToString());
                        Byte systemID = Convert.ToByte(system.SystemID);
                        List<Table> tables = new List<Table>();
                        SqlConnection openConn = new SqlConnection();
                        if (system.ConnectionString != "")
                        {
                            Console.WriteLine("ConnectionString=" + system.ConnectionString);
                            openConn = new SqlConnection(ConfigurationManager.ConnectionStrings[system.ConnectionString].ConnectionString);
                            openConn.Open();
                            String comd = "Select Name, object_id From sys.tables order by name";
                            SqlCommand dbComd = new SqlCommand(comd, openConn);
                            SqlDataReader dataReader = dbComd.ExecuteReader();

                            while (dataReader.Read())
                            {
                                Table table = new Table();
                                table.Name = dataReader.GetValue(0).ToString();
                                table.ObjectID = Convert.ToInt32(dataReader.GetValue(1).ToString());
                                tables.Add(table);
                            }
                            dataReader.Close();
                            Console.WriteLine("TableForEachStart");
                            foreach (Table table in tables)
                            {
                                Console.WriteLine(table.Name);
                                //tblTableIgnore ignore = backBL.GetTableIgnore(systemID, table.Name);
                                //if (!backBL.RecordFound)
                                //{
                                Int32 tableID = 0;
                                String objectID = table.ObjectID.ToString();
                                //Check if table exists and add if not
                                tblTable checkTable = GetTable(systemID, table.Name);
                                if (!RecordFound)
                                {
                                    tableID = InsertTable(systemID, table.Name, false, "christo741");
                                    checkTable = GetTable(systemID, table.Name);
                                }
                                else
                                    tableID = Convert.ToInt32(checkTable.TableID);

                                if (openConn.State == System.Data.ConnectionState.Closed)
                                    openConn.Open();

                                comd = "Select A.name, A.max_length, A.precision, A.is_nullable, B.name, A.column_id, A.Scale, IsNull(object_definition(A.default_object_id), '')\n";
                                comd += "  From sys.columns A Inner Join sys.types B On A.system_type_id = B.user_type_id\n";
                                comd += " Where object_id = " + objectID;
                                dbComd = new SqlCommand(comd, openConn);
                                dataReader = dbComd.ExecuteReader();

                                List<String> dbColName = new List<String>();
                                while (dataReader.Read())
                                {
                                    Int32 columnID = 0;
                                    String colName = dataReader.GetValue(0).ToString();
                                    Console.WriteLine("Column: " + table.Name + "." + colName);
                                    dbColName.Add(colName);
                                    Int32 colLength = Convert.ToInt32(dataReader.GetValue(1));
                                    Int32 precision = Convert.ToInt32(dataReader.GetValue(2));
                                    Boolean isNullable = Convert.ToBoolean(dataReader.GetValue(3));
                                    String dataType = dataReader.GetValue(4).ToString();
                                    Int32 ordinalNo = Convert.ToInt32(dataReader.GetValue(5));
                                    Int32 scale = Convert.ToInt32(dataReader.GetValue(6));
                                    Int32 dataTypeID = Convert.ToInt32(GetColumnDataType(dataType).DataTypeID);
                                    String colDefault = dataReader.GetValue(7).ToString();
                                    tblColumn col = GetColumn(tableID, colName);
                                    if (!RecordFound)
                                    {
                                        columnID = InsertColumn(tableID, colName, dataTypeID, colLength, false, precision, isNullable, ordinalNo, "christo741", scale, colDefault);
                                        Console.WriteLine(colName + " id = " + columnID.ToString());
                                    }
                                    else
                                    {
                                        Boolean updateCol = false;
                                        columnID = Convert.ToInt32(col.ColumnID);
                                        if (Convert.ToInt32(col.ColumnLength) != colLength) updateCol = true;
                                        if (Convert.ToInt32(col.ColumnPrecision) != precision) updateCol = true;
                                        if (Convert.ToBoolean(col.IsNullable) != isNullable) updateCol = true;
                                        if (Convert.ToInt32(col.DataTypeID) != dataTypeID) updateCol = true;
                                        if (Convert.ToInt32(col.OrdinalNo) != ordinalNo) updateCol = true;
                                        if (Convert.ToInt32(col.ColumnScale) != scale) updateCol = true;
                                        if (updateCol)
                                            UpdateColumn(columnID, dataTypeID, colLength, Convert.ToBoolean(col.DoAudit), precision, isNullable, ordinalNo, scale, "christo741");
                                    }
                                }
                                dataReader.Close();

                                List<tblColumn> columns = GetColumn(tableID);
                                foreach (tblColumn col in columns)
                                {
                                    List<String> colSeek = dbColName.FindAll(x => x == col.ColumnName).ToList();
                                    if (colSeek.Count == 0)
                                    {
                                        //Christo Smith 30/03/2016
                                        //Should probably put code here to delete future audit tables
                                        DeleteColumn(Convert.ToInt32(col.ColumnID));
                                    }
                                }

                                #region Collect Primary Key Information
                                comd = "Select A.CONSTRAINT_NAME, B.COLUMN_NAME, C.ORDINAL_POSITION\n";
                                comd += "  From INFORMATION_SCHEMA.TABLE_CONSTRAINTS A Inner Join INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B On A.CONSTRAINT_NAME = B.CONSTRAINT_NAME And A.TABLE_NAME = B.TABLE_NAME\n";
                                comd += " Inner Join INFORMATION_SCHEMA.COLUMNS C On C.COLUMN_NAME = B.COLUMN_NAME And C.TABLE_NAME = B.TABLE_NAME\n";
                                comd += " Where A.TABLE_NAME = '" + table.Name + "'\n";
                                comd += "   And A.CONSTRAINT_TYPE = 'PRIMARY KEY'\n";
                                dbComd.CommandText = comd;
                                dataReader = dbComd.ExecuteReader();

                                while (dataReader.Read())
                                {
                                    String pkName = dataReader.GetValue(0).ToString();
                                    String colName = dataReader.GetValue(1).ToString();
                                    Console.WriteLine("Primary Key: " + table.Name + "." + pkName);
                                    Byte ordinal = Convert.ToByte(dataReader.GetValue(2));
                                    Int32 primaryKeyID = 0;
                                    tblTablePrimaryKey primaryKey = GetTablePrimaryKey(systemID, table.Name);
                                    if (!RecordFound)
                                        primaryKeyID = InsertTablePrimaryKey(Convert.ToInt32(checkTable.TableID), pkName);
                                    else
                                        primaryKeyID = Convert.ToInt32(primaryKey.PrimaryKeyID);

                                    tblColumn col = GetColumn(Convert.ToInt32(checkTable.TableID), colName);
                                    tblTablePrimaryKeyColumn pkCol = GetTablePrimaryKeyColumn(primaryKeyID, Convert.ToInt32(col.ColumnID));
                                    Int32 pkColumnID = 0;
                                    if (!RecordFound)
                                        pkColumnID = InsertTablePrimaryKeyColumn(primaryKeyID, Convert.ToInt32(col.ColumnID), ordinal);


                                }
                                dataReader.Close();
                                #endregion
                                #region Collect Index Information
                                comd = "Select IndexName = ind.name, ColumnName = col.name, ind.is_unique,  ind.type_desc, ind.fill_factor, ic.key_ordinal, ic.is_descending_key\n";
                                comd += "  From sys.indexes ind Inner Join sys.index_columns ic ON ind.object_id = ic.object_id and ind.index_id = ic.index_id";
                                comd += " Inner Join sys.columns col ON ic.object_id = col.object_id and ic.column_id = col.column_id";
                                comd += " Inner Join sys.tables t ON ind.object_id = t.object_id";
                                comd += " Where ind.is_primary_key = 0";
                                comd += "   And t.name = '" + table.Name + "'";
                                comd += "Order By t.name, ind.name, ind.index_id, ic.index_column_id";
                                dbComd.CommandText = comd;
                                dataReader = dbComd.ExecuteReader();
                                while (dataReader.Read())
                                {
                                    String indexName = dataReader.GetValue(0).ToString();
                                    String columnName = dataReader.GetValue(1).ToString();
                                    Console.WriteLine("Index: " + table.Name + "." + indexName);
                                    Boolean isUnique = Convert.ToBoolean(Convert.ToInt32(dataReader.GetValue(2)));
                                    String typeDesc = dataReader.GetValue(3).ToString();
                                    Boolean isClustred = false;
                                    if (typeDesc == "CLUSTERED") isClustred = true;
                                    Byte fillFactor = Convert.ToByte(dataReader.GetValue(4));
                                    Byte ordinal = Convert.ToByte(dataReader.GetValue(5));
                                    String direction = "Asc";
                                    if (dataReader.GetValue(6).ToString() == "1")
                                        direction = "Desc";

                                    Int32 indexID = 0;
                                    tblTableIndex index = GetTableIndex(indexName);
                                    if (!RecordFound)
                                        indexID = InsertTableIndex(Convert.ToInt32(checkTable.TableID), indexName, isUnique, isClustred, fillFactor);
                                    else
                                        indexID = Convert.ToInt32(index.IndexID);

                                    tblColumn col = GetColumn(Convert.ToInt32(checkTable.TableID), columnName);
                                    tblTableIndexColumn indexCol = GetTableIndexColumn(indexID, Convert.ToInt32(col.ColumnID));
                                    Int32 indexColID = 0;
                                    if (!RecordFound)
                                        indexColID = InsertTableIndexColumn(indexID, Convert.ToInt32(col.ColumnID), ordinal, direction);

                                }
                                dataReader.Close();
                                #endregion
                            }

                            logInfo = "ForeignKeyStart";
                            foreach (Table table in tables)
                            {
                                logInfo = "ForEachstart| " + table.Name;
                                #region Collect Foreign Key Information
                                tblTable checkTable = GetTable(systemID, table.Name);
                                logInfo += "| GetTable";
                                comd = "Select obj.name AS FK_NAME, tab1.name AS[table], col1.name AS[column], tab2.name AS[referenced_table], col2.name AS[referenced_column]\n";
                                comd += "   From sys.foreign_key_columns fkc Inner Join sys.objects obj On obj.object_id = fkc.constraint_object_id\n";
                                comd += "  Inner Join sys.tables tab1 On tab1.object_id = fkc.parent_object_id\n";
                                comd += "  Inner Join sys.schemas sch On tab1.schema_id = sch.schema_id\n";
                                comd += "  Inner Join sys.columns col1 On col1.column_id = parent_column_id AND col1.object_id = tab1.object_id\n";
                                comd += "  Inner Join sys.tables tab2 On tab2.object_id = fkc.referenced_object_id\n";
                                comd += "  Inner Join sys.columns col2 On col2.column_id = referenced_column_id AND col2.object_id = tab2.object_id\n";
                                comd += "  Where tab1.name = '" + table.Name + "'\n";
                                dbComd.CommandText = comd;
                                dataReader = dbComd.ExecuteReader();
                                logInfo += "|Query";
                                while (dataReader.Read())
                                {
                                    String foreignKeyName = dataReader.GetValue(0).ToString();
                                    Console.WriteLine("Foreign Key: " + table.Name + "." + foreignKeyName);
                                    logInfo += "|Foreign Key| " + table.Name + "." + foreignKeyName;
                                    String columnName = dataReader.GetValue(2).ToString();
                                    logInfo += "|ColumnCollected";
                                    String refTableName = dataReader.GetValue(3).ToString();
                                    logInfo += "|refTableName";
                                    String refColumnName = dataReader.GetValue(4).ToString();
                                    logInfo += "|refColumnName";
                                    Int32 refTableID = Convert.ToInt32(GetTable(systemID, refTableName).TableID);
                                    logInfo += "|refTableID";
                                    tblTableForeignKey foreignKey = GetTableForeignKey(Convert.ToInt32(checkTable.TableID), foreignKeyName);
                                    logInfo += "|GetTableForeignKey";
                                    Int32 foreignKeyID = 0;
                                    if (!RecordFound)
                                    {
                                        foreignKeyID = InsertTableForeignKey(Convert.ToInt32(checkTable.TableID), refTableID, foreignKeyName);
                                        logInfo += "|ForeignKeyRecordNotFound";
                                    }
                                    else
                                    {
                                        foreignKeyID = Convert.ToInt32(foreignKey.ForeignKeyID);
                                        logInfo += "|ForeignKeyRecordFound";
                                    }

                                    tblColumn columnID = GetColumn(Convert.ToInt32(checkTable.TableID), columnName);
                                    logInfo += "|GetColumn";
                                    tblTableForeignKeyColumn foreignKeyColumn = GetTableForeignKeyColumn(foreignKeyID, Convert.ToInt32(columnID.ColumnID));
                                    logInfo += "| GetTableForeignKeyColumn";
                                    if (!RecordFound)
                                    {
                                        logInfo += "|ForeignKeyColumnNotFound";
                                        tblColumn refColID = GetColumn(refTableID, refColumnName);
                                        logInfo += "| GetColumn";
                                        InsertTableForeignKeyColumn(foreignKeyID, Convert.ToInt32(columnID.ColumnID), Convert.ToInt32(refColID.ColumnID));
                                        logInfo += "| InsertTableForeignKeyColumn";
                                    }

                                }
                                dataReader.Close();
                                logInfo += "|ForeachEnd";
                            }
                            #endregion
                        }
                    }
                }
                return true;
            }

            catch (Exception Ex)
            {
                Console.WriteLine(logInfo);
                throw new Exception(errorMessage + "CollectTableInformation-" + Ex.Message);
            }
    }
public Boolean CollectTableInformation(Byte pSystemID, String pTableName)
		{
			String tableName = "";
			//try
			//{
				Byte systemID = pSystemID;
				tblSystem system = GetSystem(pSystemID);
						List<Table> tables = new List<Table>();
						SqlConnection openConn = new SqlConnection();
				if (system.ConnectionString != "")
				{
					openConn = new SqlConnection(ConfigurationManager.ConnectionStrings[system.ConnectionString].ConnectionString);
					openConn.Open();
					String comd = "Select Name, object_id From sys.tables where name = '" + pTableName + "' order by name";
					SqlCommand dbComd = new SqlCommand(comd, openConn);
					SqlDataReader dataReader = dbComd.ExecuteReader();

					while (dataReader.Read())
					{
						Table table = new Table();
						table.Name = dataReader.GetValue(0).ToString();
						table.ObjectID = Convert.ToInt32(dataReader.GetValue(1).ToString());
						tables.Add(table);
					}
					dataReader.Close();

					foreach (Table table in tables)
					{
						tableName = table.Name;
						if (tableName == "vcs061_recipients")
							Console.WriteLine("1");
						//tblTableIgnore ignore = backBL.GetTableIgnore(systemID, table.Name);
						//if (!backBL.RecordFound)
						//{
						Int32 tableID = 0;
						String objectID = table.ObjectID.ToString();
						//Check if table exists and add if not
						tblTable checkTable = GetTable(systemID, table.Name);
						if (checkTable != null)
							if (DateTime.Now.Subtract(Convert.ToDateTime(checkTable.ChangeDateTime)).Minutes < 15)
								return true;
						if (!RecordFound)
						{
							tableID = InsertTable(systemID, table.Name, true, "christo741");
							checkTable = GetTable(systemID, table.Name);
						}
						else
							tableID = Convert.ToInt32(checkTable.TableID);

						if (openConn.State == System.Data.ConnectionState.Closed)
							openConn.Open();

						//comd = "Select A.name, A.max_length, A.precision, A.is_nullable, B.name, A.column_id, A.Scale, IsNull(object_definition(A.default_object_id), '')\n";
						//comd += "  From sys.columns A Inner Join sys.types B On A.system_type_id = B.user_type_id\n";
						//comd += " Where object_id = " + objectID;

						comd = "Select A.COLUMN_NAME, IsNull(A.CHARACTER_MAXIMUM_LENGTH, 0), ISnull(A.NUMERIC_PRECISION, 0), A.IS_NULLABLE, A.DATA_TYPE, A.ORDINAL_POSITION, IsNull(A.NUMERIC_SCALE, 0), IsNull(A.COLUMN_DEFAULT, '')\n";
						comd += "  From INFORMATION_SCHEMA.COLUMNS A\n";
						comd += " Where TABLE_NAME = '" + table.Name + "'";

						dbComd = new SqlCommand(comd, openConn);
						dataReader = dbComd.ExecuteReader();

						List<String> dbColName = new List<String>();
						while (dataReader.Read())
						{
							Int32 columnID = 0;
							String colName = dataReader.GetValue(0).ToString();
							dbColName.Add(colName);
							Int32 colLength = Convert.ToInt32(dataReader.GetValue(1));
							Int32 precision = Convert.ToInt32(dataReader.GetValue(2));
							Boolean isNullable = true;
							if (dataReader.GetValue(3).ToString() == "NO")
								isNullable = false;
							String dataType = dataReader.GetValue(4).ToString();
							Int32 ordinalNo = Convert.ToInt32(dataReader.GetValue(5));
							Int32 scale = Convert.ToInt32(dataReader.GetValue(6));
							Int32 dataTypeID = Convert.ToInt32(GetColumnDataType(dataType).DataTypeID);
							String colDefault = dataReader.GetValue(7).ToString();
							tblColumn col = GetColumn(tableID, colName);
							if (!RecordFound)
							{
								columnID = InsertColumn(tableID, colName, dataTypeID, colLength, false, precision, isNullable, ordinalNo, "christo741", scale, colDefault);
							}
							else
							{
								Boolean updateCol = false;
								columnID = Convert.ToInt32(col.ColumnID);
								if (Convert.ToInt32(col.ColumnLength) != colLength) updateCol = true;
								if (Convert.ToInt32(col.ColumnPrecision) != precision) updateCol = true;
								if (Convert.ToBoolean(col.IsNullable) != isNullable) updateCol = true;
								if (Convert.ToInt32(col.DataTypeID) != dataTypeID) updateCol = true;
								if (Convert.ToInt32(col.OrdinalNo) != ordinalNo) updateCol = true;
								if (Convert.ToInt32(col.ColumnScale) != scale) updateCol = true;
								if (updateCol)
									UpdateColumn(columnID, dataTypeID, colLength, Convert.ToBoolean(col.DoAudit), precision, isNullable, ordinalNo, scale, "christo741");
							}
						}
						dataReader.Close();

						List<tblColumn> columns = GetColumn(tableID);
						foreach (tblColumn col in columns)
						{
							List<String> colSeek = dbColName.FindAll(x => x == col.ColumnName).ToList();
							if (colSeek.Count == 0)
							{
								//Christo Smith 30/03/2016
								//Should probably put code here to delete future audit tables
								DeleteColumn(Convert.ToInt32(col.ColumnID));
							}
						}

						#region Collect Primary Key Information
						comd = "Select A.CONSTRAINT_NAME, B.COLUMN_NAME, C.ORDINAL_POSITION\n";
						comd += "  From INFORMATION_SCHEMA.TABLE_CONSTRAINTS A Inner Join INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B On A.CONSTRAINT_NAME = B.CONSTRAINT_NAME And A.TABLE_NAME = B.TABLE_NAME\n";
						comd += " Inner Join INFORMATION_SCHEMA.COLUMNS C On C.COLUMN_NAME = B.COLUMN_NAME And C.TABLE_NAME = B.TABLE_NAME\n";
						comd += " Where A.TABLE_NAME = '" + table.Name + "'\n";
						comd += "   And A.CONSTRAINT_TYPE = 'PRIMARY KEY'\n";
						dbComd.CommandText = comd;
						dataReader = dbComd.ExecuteReader();

						while (dataReader.Read())
						{
							String pkName = dataReader.GetValue(0).ToString();
							String colName = dataReader.GetValue(1).ToString();
							Byte ordinal = Convert.ToByte(dataReader.GetValue(2));
							Int32 primaryKeyID = 0;
							tblTablePrimaryKey primaryKey = GetTablePrimaryKey(systemID, table.Name);
							if (!RecordFound)
								primaryKeyID = InsertTablePrimaryKey(Convert.ToInt32(checkTable.TableID), pkName);
							else
								primaryKeyID = Convert.ToInt32(primaryKey.PrimaryKeyID);

							tblColumn col = GetColumn(Convert.ToInt32(checkTable.TableID), colName);
							tblTablePrimaryKeyColumn pkCol = GetTablePrimaryKeyColumn(primaryKeyID, Convert.ToInt32(col.ColumnID));
							Int32 pkColumnID = 0;
							if (!RecordFound)
								pkColumnID = InsertTablePrimaryKeyColumn(primaryKeyID, Convert.ToInt32(col.ColumnID), ordinal);


						}
						dataReader.Close();
						#endregion
						#region Collect Index Information
						comd = "Select IndexName = ind.name, ColumnName = col.name, ind.is_unique,  ind.type_desc, ind.fill_factor, ic.key_ordinal, ic.is_descending_key\n";
						comd += "  From sys.indexes ind Inner Join sys.index_columns ic ON ind.object_id = ic.object_id and ind.index_id = ic.index_id";
						comd += " Inner Join sys.columns col ON ic.object_id = col.object_id and ic.column_id = col.column_id";
						comd += " Inner Join sys.tables t ON ind.object_id = t.object_id";
						comd += " Where ind.is_primary_key = 0";
						comd += "   And t.name = '" + table.Name + "'";
						comd += "Order By t.name, ind.name, ind.index_id, ic.index_column_id";
						dbComd.CommandText = comd;
						dataReader = dbComd.ExecuteReader();
						while (dataReader.Read())
						{
							String indexName = dataReader.GetValue(0).ToString();
							String columnName = dataReader.GetValue(1).ToString();
							Boolean isUnique = Convert.ToBoolean(Convert.ToInt32(dataReader.GetValue(2)));
							String typeDesc = dataReader.GetValue(3).ToString();
							Boolean isClustred = false;
							if (typeDesc == "CLUSTERED") isClustred = true;
							Byte fillFactor = Convert.ToByte(dataReader.GetValue(4));
							Byte ordinal = Convert.ToByte(dataReader.GetValue(5));
							String direction = "Asc";
							if (dataReader.GetValue(6).ToString() == "1")
								direction = "Desc";

							Int32 indexID = 0;
							tblTableIndex index = GetTableIndex(indexName);
							if (!RecordFound)
								indexID = InsertTableIndex(Convert.ToInt32(checkTable.TableID), indexName, isUnique, isClustred, fillFactor);
							else
								indexID = Convert.ToInt32(index.IndexID);

							tblColumn col = GetColumn(Convert.ToInt32(checkTable.TableID), columnName);
							tblTableIndexColumn indexCol = GetTableIndexColumn(indexID, Convert.ToInt32(col.ColumnID));
							Int32 indexColID = 0;
							if (!RecordFound)
								indexColID = InsertTableIndexColumn(indexID, Convert.ToInt32(col.ColumnID), ordinal, direction);

						}
						dataReader.Close();
						#endregion
					}

					foreach (Table table in tables)
					{
						#region Collect Foreign Key Information
						tblTable checkTable = GetTable(systemID, table.Name);

						comd = "Select obj.name AS FK_NAME, tab1.name AS[table], col1.name AS[column], tab2.name AS[referenced_table], col2.name AS[referenced_column]\n";
						comd += "   From sys.foreign_key_columns fkc Inner Join sys.objects obj On obj.object_id = fkc.constraint_object_id\n";
						comd += "  Inner Join sys.tables tab1 On tab1.object_id = fkc.parent_object_id\n";
						comd += "  Inner Join sys.schemas sch On tab1.schema_id = sch.schema_id\n";
						comd += "  Inner Join sys.columns col1 On col1.column_id = parent_column_id AND col1.object_id = tab1.object_id\n";
						comd += "  Inner Join sys.tables tab2 On tab2.object_id = fkc.referenced_object_id\n";
						comd += "  Inner Join sys.columns col2 On col2.column_id = referenced_column_id AND col2.object_id = tab2.object_id\n";
						comd += "  Where tab1.name = '" + table.Name + "'\n";
						dbComd.CommandText = comd;
						dataReader = dbComd.ExecuteReader();

						while (dataReader.Read())
						{
							String foreignKeyName = dataReader.GetValue(0).ToString();
							String columnName = dataReader.GetValue(2).ToString();
							String refTableName = dataReader.GetValue(3).ToString();
							String refColumnName = dataReader.GetValue(4).ToString();
						tblTable refTable = GetTable(systemID, refTableName);
						if (!RecordFound)
						{
							CollectTableInformation(pSystemID, refTableName);
							refTable = GetTable(systemID, refTableName);
							checkTable = GetTable(systemID, table.Name);
						}
							Int32 refTableID = Convert.ToInt32(refTable.TableID);
							tblTableForeignKey foreignKey = GetTableForeignKey(Convert.ToInt32(checkTable.TableID), foreignKeyName);
							Int32 foreignKeyID = 0;
							if (!RecordFound)
								foreignKeyID = InsertTableForeignKey(Convert.ToInt32(checkTable.TableID), refTableID, foreignKeyName);
							else
								foreignKeyID = Convert.ToInt32(foreignKey.ForeignKeyID);

							tblColumn columnID = GetColumn(Convert.ToInt32(checkTable.TableID), columnName);
							tblTableForeignKeyColumn foreignKeyColumn = GetTableForeignKeyColumn(foreignKeyID, Convert.ToInt32(columnID.ColumnID));
							if (!RecordFound)
							{
								tblColumn refColID = GetColumn(refTableID, refColumnName);
								InsertTableForeignKeyColumn(foreignKeyID, Convert.ToInt32(columnID.ColumnID), Convert.ToInt32(refColID.ColumnID));
							}

						}
						dataReader.Close();

					//Get the referenced table information

					comd = "SELECT f.name, OBJECT_NAME(f.parent_object_id) TableName, COL_NAME(fc.parent_object_id, fc.parent_column_id) ColName, t.name, COL_NAME(fc.referenced_object_id, fc.referenced_column_id)\n";
					comd += "  FROM sys.foreign_keys AS f INNER JOIN sys.foreign_key_columns AS fc ON f.OBJECT_ID = fc.constraint_object_id\n";
					comd += " INNER JOIN sys.tables t ON t.OBJECT_ID = fc.referenced_object_id\n";
					comd += "WHERE OBJECT_NAME (f.referenced_object_id) = '" + table.Name + "'\n";
					dbComd.CommandText = comd;
					dataReader = dbComd.ExecuteReader();
					while (dataReader.Read())
					{
						DeleteTableInformation(pSystemID, dataReader.GetValue(1).ToString());
						CollectTableInformation(pSystemID, dataReader.GetValue(1).ToString());
					}
					dataReader.Close();
				}
					#endregion
				}
				return true;
			//}

			//catch (Exception Ex)
			//{
                UpdateTempMethod("CollectTableInformation", "(Byte pSystemID, String pTableName)", false);
			//	throw new Exception(errorMessage + "CollectTableInformation-" + Ex.Message);
			//}
		}
    }
}
