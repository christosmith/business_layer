﻿using System;
using System.Xml;
namespace DPO.BackOffice.BL
{

	public partial class BackOfficeBL
    {
        private Boolean recordFound = false;
        private Boolean excludeRefTable = false;
		private String errorMessage = "";
		private DPO.VirtualVendor.BL.VirtualVendorBL vvoBL = new VirtualVendor.BL.VirtualVendorBL();
		private DPO.VirtualMessage.BL.VirtualMessageBL messBL = new VirtualMessage.BL.VirtualMessageBL();

        public BackOfficeBL()
        {
            String cultureName = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(cultureName);
            ci.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = ci;
        }
        public System.Data.DataTable ExecuteMSQuery(String pQuery)
        {
            MySql.Data.MySqlClient.MySqlConnection openConn = new MySql.Data.MySqlClient.MySqlConnection();
            try
            {
                openConn = new MySql.Data.MySqlClient.MySqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["MySQL_backOffice_ConnectionString"].ConnectionString);
                openConn.Open();
                MySql.Data.MySqlClient.MySqlCommand dbComd = new MySql.Data.MySqlClient.MySqlCommand(pQuery, openConn);
                MySql.Data.MySqlClient.MySqlDataAdapter dataAdapter = new MySql.Data.MySqlClient.MySqlDataAdapter(dbComd);
                System.Data.DataTable table = new System.Data.DataTable();
                dataAdapter.Fill(table);

                return table;

            }
            catch (Exception Ex)
            {
                throw new Exception("ExecuteQuery - " + Ex.Message);
            }
        }

		internal class Table
		{
			public String Name;
			public Int32 ObjectID;
		}

        public enum GatewayServer
        {
            VSPMGW01 = 1,
            VSPMGW02 = 2
        }
        internal enum ProcessUpdate
        {
            WebPageCalled = 1,
            Authorised = 2,
            ThirdPartyCalled = 3,
            VendingID = 4,
            TextMessage = 5,
            ValidationError = 6,
        }

        



        public class MulitChoiceParameter
        {
            public String VendorCode = "";
            public String CustomerVendorID = "";
            public String DataSource = "";
            public String BotswanaCurrency = "";
            public String BusinessUnit = "";
            public String PaymentVendorCode = "";
        }
		public enum eLogType
		{
			Error = 1,
			Warning = 2,
			Information = 3,
			Debug = 4
		}


        public struct structDeployBinaryData
		{
			public Int32 binaryDataTypeID;
			public Int32 folderID;
			public Boolean gac;
			public Byte[] binaryData;
			public String fileName;
			public Decimal fileSize;
			public Int32 databaseID;
		}
		public Boolean ExcludeReferenceTable
        {
            get
            {
                return excludeRefTable;
            }
            set
            {
                excludeRefTable = value;
                
                
            }
        }
        public Boolean RecordFound
        {
            get
            {
                return recordFound;
            }
            set
            {
                recordFound = value;
            }
        }
        
    }


	internal class Index
	{
		internal String TableName = "";
		internal String IndexName = "";
	}
	public enum eLogType
	{
		Error = 1,
		Warning = 2,
		Information = 3,
		Debug = 4

	}

	public enum eDatabases
	{
		VirtualVendor = 1,
		VirtualMessage = 2,
		BackOffice = 3,
		VirtualAccount = 4,
		VirtualUser = 5,
		VirtualVoucher = 6

	}

    
}
