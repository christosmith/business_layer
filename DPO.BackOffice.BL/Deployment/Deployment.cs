﻿using System;
using System.Collections.Generic;
using System.Linq;
using DPO.BackOffice.DAL;

using System.Data.SqlClient;

namespace DPO.BackOffice.BL
{


	public partial class BackOfficeBL
    {
        internal String deployInformation = "";
        public Boolean DeleteDeploySaveByHeaderID(Int32 pDeploySaveHeaderID)
        {
            try
            {
                UpdateTempMethod("DeleteDeploySaveByHeaderID", "(Int32 pDeploySaveHeaderID)", true);
                List<tblDeploySave> saves = GetDeploySave(pDeploySaveHeaderID);
				tblDeploySaveCrud crud = new tblDeploySaveCrud();
				foreach (tblDeploySave save in saves)
					crud.Delete(save);

                return true;
            }

            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteDeploySaveByHeaderID", "(Int32 pDeploySaveHeaderID)", false);
                throw Ex;
            }

        }

        public Boolean DeleteDeploySaveHeader(Int32 pDeploySaveHeaderID)
        {
            try
            {
                UpdateTempMethod("DeleteDeploySaveHeader", "(Int32 pDeploySaveHeaderID)", true);
                tblDeploySaveHeaderCrud crud = new tblDeploySaveHeaderCrud();
                crud.Where("DeploySaveHeaderID", DAL.General.Operator.Equals, pDeploySaveHeaderID);

                List<tblDeploySaveHeader> search = crud.ReadMulti().ToList();
                foreach (tblDeploySaveHeader del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteDeploySaveHeader", "(Int32 pDeploySaveHeaderID)", false);
                throw new Exception("DeleteDeploySaveHeader - " + Ex.Message);
            }
        }

		public Int32 SaveDeploySaveHeader(Int32 pDeploySaveHeaderID, Int32 pDeveloperID, Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("SaveDeploySaveHeader", "(Int32 pDeploySaveHeaderID, Int32 pDeveloperID, Int32 pApplicationID)", true);
                tblDeploySaveHeaderCrud crud = new tblDeploySaveHeaderCrud();
                tblDeploySaveHeader workRec = new tblDeploySaveHeader();
                if (pDeploySaveHeaderID != 0)
                {
                    crud.Where("DeploySaveHeaderID", DAL.General.Operator.Equals, pDeploySaveHeaderID);
                    workRec = crud.ReadSingle();
                    workRec.DeveloperID = pDeveloperID;
                    workRec.ApplicationID = pApplicationID;
                    crud.Update(workRec);
                    return pDeploySaveHeaderID;
                }
                else
                {
                    workRec.DeveloperID = pDeveloperID;
                    workRec.ApplicationID = pApplicationID;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveDeploySaveHeader", "(Int32 pDeploySaveHeaderID, Int32 pDeveloperID, Int32 pApplicationID)", false);
                throw new Exception("SaveDeploySaveHeader - " + Ex.Message);
            }
        }
        public List<tblDeploySave> GetDeploySave(Int32 pDeploySaveHeaderID)
        {
            try
            {
                UpdateTempMethod("GetDeploySave", "(Int32 pDeploySaveHeaderID)", true);
                recordFound = false;
                tblDeploySaveCrud crud = new tblDeploySaveCrud();
                crud.Where("DeploySaveHeaderID", DAL.General.Operator.Equals, pDeploySaveHeaderID);

                List<tblDeploySave> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeploySave", "(Int32 pDeploySaveHeaderID)", false);
                throw new Exception("GetDeploySave - " + Ex.Message);
            }
        }

        public tblDeploySaveHeader GetDeploySaveHeaderByID(Int32 pDeploySaveHeaderID)
        {
            try
            {
                UpdateTempMethod("GetDeploySaveHeaderByID", "(Int32 pDeploySaveHeaderID)", true);
				
                tblDeploySaveHeaderCrud crud = new tblDeploySaveHeaderCrud();
				crud.Where("DeploySaveHeaderID", DAL.General.Operator.Equals, pDeploySaveHeaderID);

				tblDeploySaveHeader workRec = crud.ReadSingle();
				return workRec;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeploySaveHeaderByID", "(Int32 pDeploySaveHeaderID)", false);
                throw new Exception("GetDeploySaveHeaderByID - " + Ex.Message);
            }
        }
        public Int32 SaveDeploySave(Int32 pDeploySaveHeaderID, Int32 pFolderID, Boolean pGACDeployment, String pLocalFileName)
        {
            try
            {
                UpdateTempMethod("SaveDeploySave", "(Int32 pDeploySaveHeaderID, Int32 pFolderID, Boolean pGACDeployment, String pLocalFileName)", true);
                tblDeploySaveCrud crud = new tblDeploySaveCrud();
                tblDeploySave workRec = new tblDeploySave();
                if (pDeploySaveHeaderID != 0)
                {
                    crud.Where("DeploySaveHeaderID", DAL.General.Operator.Equals, pDeploySaveHeaderID);
                    workRec = crud.ReadSingle();
                    workRec.FolderID = pFolderID;
                    workRec.GACDeployment = pGACDeployment;
                    workRec.LocalFileName = pLocalFileName;
                    crud.Update(workRec);
                    return pDeploySaveHeaderID;
                }
                else
                {
                    workRec.FolderID = pFolderID;
                    workRec.GACDeployment = pGACDeployment;
                    workRec.LocalFileName = pLocalFileName;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveDeploySave", "(Int32 pDeploySaveHeaderID, Int32 pFolderID, Boolean pGACDeployment, String pLocalFileName)", false);
                throw new Exception("SaveDeploySave - " + Ex.Message);
            }
        }
        public tblDeployBackup GetDeployBackup(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("GetDeployBackup", "(Int32 pDeployID)", true);
                recordFound = false;
                tblDeployBackupCrud crud = new tblDeployBackupCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                tblDeployBackup search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployBackup", "(Int32 pDeployID)", false);
                throw new Exception("GetDeployBackup - " + Ex.Message);
            }
        }
		public Boolean DeleteDeployNotification(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("DeleteDeployNotification", "(Int32 pDeployID)", true);
                tblDeployNotificationCrud crud = new tblDeployNotificationCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                List<tblDeployNotification> search = crud.ReadMulti().ToList();
                foreach (tblDeployNotification del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteDeployNotification", "(Int32 pDeployID)", false);
                throw new Exception("DeleteDeployNotification - " + Ex.Message);
            }
        }
        public Boolean DeleteDeploymentRequest(Int32 deployID)
        {
            try
            {
                UpdateTempMethod("DeleteDeploymentRequest", "(Int32 deployID)", true);
                DeleteDeployCancel(deployID);
                DeleteDeployError(deployID);
                DeleteDeployBackup(deployID);
                DeleteDeployBinaryData(deployID);
                DeleteDeployHistory(deployID);
                DeleteDeployNote(deployID);
                DeleteDeployError(deployID);
				DeleteDeployNotification(deployID);
				tblDeployCrud crud = new tblDeployCrud();
				crud.Where("DeployID", DAL.General.Operator.Equals, deployID);
				tblDeploy dep = crud.ReadSingle();
				crud.Delete(dep); 

                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteDeploymentRequest", "(Int32 deployID)", false);
                throw new Exception (errorMessage + "DeleteDeploymentRequest - " + Ex.Message);
            }
        }
        public String RollbackApplication(Int32 pDeployID, Int32 pDeveloperID)
        {
            Byte systemID = Convert.ToByte(GetSystemByName("Back Office").SystemID);
            Int32 deployApplicationID = Convert.ToInt32(GetApplicationByName(systemID, "VCS.BackOffice.Deploy").ApplicationID);

            try
            {
                UpdateTempMethod("RollbackApplication", "(Int32 pDeployID, Int32 pDeveloperID)", true);
                tblDeployBackup deployBackup = GetDeployBackup(pDeployID);
                List<tblDeployBackupFile> backupFiles = GetDeployBackupFile(Convert.ToInt32(deployBackup.BackupID));

                foreach (tblDeployBackupFile backupFile in backupFiles)
                {
                    RollbackFile(Convert.ToInt32(backupFile.BackupFileID));
                }

                tblDeploy deploy = GetDeploy(pDeployID);
                deploy.StatusID = 5;
                deploy.ChangeDeveloperID = pDeveloperID;
				tblDeployCrud crud = new tblDeployCrud();
				crud.Update(deploy);
                return "Success";
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("RollbackApplication", "(Int32 pDeployID, Int32 pDeveloperID)", false);
                SaveDeployError(pDeployID, Ex.Message);
                WriteLog(deployApplicationID, "DeployID: " + pDeployID.ToString() + "." + Ex.Message, eLogType.Error);
                SaveDeployStatus(pDeployID, 3, pDeveloperID);
                SendMail(pDeployID, "Failed");
                return Ex.Message;
            }
        }

		public Boolean SaveDeployError(Int32 pDeployID, String pErrorMessage)
		{
			try
			{
                UpdateTempMethod("SaveDeployError", "(Int32 pDeployID, String pErrorMessage)", true);
				tblDeployError error = new tblDeployError();
				error.DeployID = pDeployID;
				error.ErrorMessage = pErrorMessage;
				tblDeployErrorCrud crud = new tblDeployErrorCrud();
				crud.Insert(error);
				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("SaveDeployError", "(Int32 pDeployID, String pErrorMessage)", false);
				throw Ex;
			}
		}
		public Boolean DeleteDeployBackup(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("DeleteDeployBackup", "(Int32 pDeployID)", true);
                tblDeployBackupCrud crud = new tblDeployBackupCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                List<tblDeployBackup> search = crud.ReadMulti().ToList();
                foreach (tblDeployBackup del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteDeployBackup", "(Int32 pDeployID)", false);
                throw new Exception("DeleteDeployBackup - " + Ex.Message);
            }
        }
        public Boolean DeleteDeployNote(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("DeleteDeployNote", "(Int32 pDeployID)", true);
                tblDeployNoteCrud crud = new tblDeployNoteCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                List<tblDeployNote> search = crud.ReadMulti().ToList();
                foreach (tblDeployNote del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteDeployNote", "(Int32 pDeployID)", false);
                throw new Exception("DeleteDeployNote - " + Ex.Message);
            }
        }
        public List<tblDeployHistory> GetDeployHistory(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("GetDeployHistory", "(Int32 pDeployID)", true);
                recordFound = false;
                tblDeployHistoryCrud crud = new tblDeployHistoryCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                List<tblDeployHistory> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployHistory", "(Int32 pDeployID)", false);
                throw new Exception("GetDeployHistory - " + Ex.Message);
            }
        }
        public Boolean DeleteDeployHistory(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("DeleteDeployHistory", "(Int32 pDeployID)", true);
                tblDeployHistoryCrud crud = new tblDeployHistoryCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                List<tblDeployHistory> search = crud.ReadMulti().ToList();
                foreach (tblDeployHistory del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteDeployHistory", "(Int32 pDeployID)", false);
                throw new Exception("DeleteDeployHistory - " + Ex.Message);
            }
        }
        public Boolean DeleteDeployBinaryData(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("DeleteDeployBinaryData", "(Int32 pDeployID)", true);
                tblDeployBinaryDataCrud crud = new tblDeployBinaryDataCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                List<tblDeployBinaryData> search = crud.ReadMulti().ToList();
                foreach (tblDeployBinaryData del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteDeployBinaryData", "(Int32 pDeployID)", false);
                throw new Exception("DeleteDeployBinaryData - " + Ex.Message);
            }
        }
        public Boolean DeleteDeployError(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("DeleteDeployError", "(Int32 pDeployID)", true);
                tblDeployErrorCrud crud = new tblDeployErrorCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                List<tblDeployError> search = crud.ReadMulti().ToList();
                foreach (tblDeployError del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteDeployError", "(Int32 pDeployID)", false);
                throw new Exception("DeleteDeployError - " + Ex.Message);
            }
        }
        public List<tblDeployCancel> GetDeployCancel(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("GetDeployCancel", "(Int32 pDeployID)", true);
                recordFound = false;
                tblDeployCancelCrud crud = new tblDeployCancelCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                List<tblDeployCancel> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployCancel", "(Int32 pDeployID)", false);
                throw new Exception("GetDeployCancel - " + Ex.Message);
            }
        }
        public Boolean DeleteDeployCancel(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("DeleteDeployCancel", "(Int32 pDeployID)", true);
                tblDeployCancelCrud crud = new tblDeployCancelCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                List<tblDeployCancel> search = crud.ReadMulti().ToList();
                foreach (tblDeployCancel del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteDeployCancel", "(Int32 pDeployID)", false);
                throw new Exception("DeleteDeployCancel - " + Ex.Message);
            }
        }
        public List<vDeployBackupFile> GetDeployBackFile(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("GetDeployBackFile", "(Int32 pDeployID)", true);
                recordFound = false;
                vDeployBackupFileCrud crud = new vDeployBackupFileCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                List<vDeployBackupFile> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployBackFile", "(Int32 pDeployID)", false);
                throw new Exception("GetDeployBackFile - " + Ex.Message);
            }
        }
        public String RollbackFile(Int32 pBackupFileID)
        {
            Byte systemID = Convert.ToByte(GetSystemByName("Back Office").SystemID);
            Int32 deployApplicationID = Convert.ToInt32(GetApplicationByName(systemID, "VCS.BackOffice.Deploy").ApplicationID);
            Int32 pDeployID = 0;
            Int32 pDeveloperID = 0;
            try
            {
                UpdateTempMethod("RollbackFile", "(Int32 pBackupFileID)", true);
                #region Deployment
                tblDeployBackupFile backupFile = GetDeployBackupFilePerFileID(pBackupFileID);
                tblDeploy deploy = GetDeploy(Convert.ToInt32(backupFile.tblDeployBackup.DeployID));
                pDeployID = Convert.ToInt32(deploy.DeployID);
                pDeveloperID = Convert.ToInt32(deploy.ChangeDeveloperID);
                String deploymentResult = "";
                //Get the server work folder
                String workFolder = GetSettingByName("ServerWorkFolder", Convert.ToInt32(GetApplicationByName(Convert.ToByte(GetSystemByName("Back Office").SystemID), "VCS.BackOffice.Deploy").ApplicationID)).SettingValue;
                if (!System.IO.Directory.Exists(workFolder))
                {
                    System.IO.Directory.CreateDirectory(workFolder);
                }


                tblApplication application = GetApplication(Convert.ToInt32(deploy.ApplicationID));
                tblServer server = GetServer(Convert.ToInt32(application.ServerID));
                String serverName = server.vcsServer;
                if (serverName == "")
                {
                    return "Application not linked to a server";
                }

                //Stop the windows service if it is one
                #region Service Stop & Uninstall
                Boolean restartService = false;
                if (Convert.ToInt32(deploy.tblApplication.ApplicationTypeID) == 3)
                {
                    System.ServiceProcess.ServiceController service = new System.ServiceProcess.ServiceController(deploy.tblApplication.vcsApplication, serverName);
                    TimeSpan waitForService = TimeSpan.FromMilliseconds(600000);
                    if (service.Status != System.ServiceProcess.ServiceControllerStatus.Stopped)
                    {
                        service.Stop();
                        service.WaitForStatus(System.ServiceProcess.ServiceControllerStatus.Stopped);
                        restartService = true;
                    }

                    String installUtilFolder = GetSettingByName("InstallUtilFolder", Convert.ToInt32(GetApplicationByName(Convert.ToByte(GetSystemByName("Back Office").SystemID), "VCS.BackOffice.Deploy").ApplicationID)).SettingValue;
                    System.Diagnostics.Process installUtil = new System.Diagnostics.Process();
                    String serviceName = deploy.tblApplication.vcsApplication;
                    installUtil.StartInfo.FileName = installUtilFolder + "InstallUtil.exe";
                    installUtil.StartInfo.UseShellExecute = false;
                    installUtil.StartInfo.Arguments = "/u \"" + serviceName + "\"";

                    installUtil.StartInfo.RedirectStandardOutput = true;
                    installUtil.Start();
                    String result = "";
                    using (System.IO.StreamReader procOutPut = installUtil.StandardOutput)
                    {
                        result += procOutPut.ReadToEnd();
                    }

                    if (installUtil.ExitCode != 0)
                    {
                        deploymentResult = "InstallUtil Error (" + installUtil.ExitCode.ToString() + ":" + result;
                        SaveDeployError(pDeployID, deploymentResult);
                        SaveDeployStatus(pDeployID, 3, pDeveloperID);
                        SendMail(pDeployID, "Failed");
                        return deploymentResult;
                    }

                }
                #endregion

                #region Write file to disk
                String fileName = workFolder + "\\" + backupFile.BackupFileName;
                System.IO.FileStream writeStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                writeStream.Write(backupFile.BinaryData, 0, backupFile.BinaryData.Length);
                writeStream.Close();
                #endregion
                Int32 binaryDataTypeID = Convert.ToInt32(backupFile.tblDeployBinaryData.BinaryDataTypeID);
                Int32 folderID = Convert.ToInt32(backupFile.FolderID);
                Boolean gacDeploy = Convert.ToBoolean(backupFile.tblDeployBinaryData.GacDeploy);
                if ( binaryDataTypeID == 1 || binaryDataTypeID == 3)
                {
                    #region File Copy Deployment
                    if (folderID != 0 && !gacDeploy)
                    {
                        //Get the folder name

                        tblApplicationFolder folder = GetApplicationFolderByID(Convert.ToInt32(folderID));
                        String deployFolder = folder.FolderPath;
                        switch (deployFolder.Substring(0, 2).ToUpper())
                        {
                            case "C:": deployFolder = "C$" + deployFolder.Substring(2, deployFolder.Length - 2); break;
                            case "D:": deployFolder = "D$" + deployFolder.Substring(2, deployFolder.Length - 2); break;
                        }
                        deployFolder = @"\\" + serverName + "\\" + deployFolder;

                        if (!System.IO.Directory.Exists(deployFolder))
                        {
                            deploymentResult = "Deployment folder was not found. (" + folder.FolderPath + ")";
                            SaveDeployError(pDeployID, deploymentResult);
                            SaveDeployStatus(pDeployID, 3, pDeveloperID);
                            SendMail(pDeployID, "Failed");
                            return deploymentResult;
                        }

                        String destination = deployFolder;
                        if (destination.Substring(destination.Length - 1) != "\\")
                            destination = destination + "\\";
                        destination += backupFile.BackupFileName;

                        System.IO.File.Copy(fileName, destination, true);

                    }
                    #endregion
                    #region Deploy to Global Assembly Cache
                    if (gacDeploy)
                    {
                        String gacUtilFolder = GetSettingByName("GACUtilFolder", Convert.ToInt32(GetApplicationByName(Convert.ToByte(GetSystemByName("Back Office").SystemID), "VCS.BackOffice.Deploy").ApplicationID)).SettingValue;
                        System.Diagnostics.Process cscBuilder = new System.Diagnostics.Process();
                        String dllName = backupFile.BackupFileName;
                        cscBuilder.StartInfo.FileName = gacUtilFolder + "GacUtil.exe";
                        cscBuilder.StartInfo.UseShellExecute = false;
                        cscBuilder.StartInfo.Arguments = "/u \"" + dllName.Substring(0, dllName.Length - 4) + "\"";

                        cscBuilder.StartInfo.RedirectStandardOutput = true;
                        cscBuilder.Start();
                        String result = "";
                        using (System.IO.StreamReader procOutPut = cscBuilder.StandardOutput)
                        {
                            result += procOutPut.ReadToEnd();
                        }

                        if (cscBuilder.ExitCode != 0)
                        {
                            deploymentResult = "GacUtil Error (" + cscBuilder.ExitCode.ToString() + ":" + result;
                            WriteLog(deployApplicationID, "DeployID: " + pDeployID.ToString() + ". Binary: " + backupFile.BackupFileName + ". " + deploymentResult, eLogType.Error);
                            SaveDeployError(pDeployID, deploymentResult);
                            SaveDeployStatus(pDeployID, 3, pDeveloperID);
                            SendMail(pDeployID, "Failed");
                            return deploymentResult;
                        }


                        cscBuilder = new System.Diagnostics.Process();
                        //cscBuilder.StartInfo.WorkingDirectory = workFolder;
                        cscBuilder.StartInfo.FileName = gacUtilFolder + "GacUtil.exe";
                        cscBuilder.StartInfo.UseShellExecute = false;
                        cscBuilder.StartInfo.Arguments = "/i \"" + fileName + "\"";

                        cscBuilder.StartInfo.RedirectStandardOutput = true;
                        cscBuilder.Start();
                        result = "";
                        using (System.IO.StreamReader procOutPut = cscBuilder.StandardOutput)
                        {
                            result += procOutPut.ReadToEnd();
                        }

                        if (cscBuilder.ExitCode != 0)
                        {
                            deploymentResult = "GacUtil Error (" + cscBuilder.ExitCode.ToString() + "): " + result;
                            WriteLog(deployApplicationID, "DeployID: " + pDeployID.ToString() + ". Binary: " + backupFile.BackupFileName + ". " + deploymentResult, eLogType.Error);
                            SaveDeployError(pDeployID, deploymentResult);
                            SaveDeployStatus(pDeployID, 3, pDeveloperID);
                            SendMail(pDeployID, "Failed");
                            return deploymentResult;
                        }


                    }
                    #endregion
                    
                    #region Apply Script To Database
                    if (binaryDataTypeID == 2)
                    {
                        String connSetting = "";
                        switch (backupFile.tblDeployBinaryData.DatabaseID.ToString())
                        {
                            case "1": connSetting = "backOffice_ConnectionString"; break;
                            case "2": connSetting = "vAccount_ConnectionString"; break;
                            case "3": connSetting = "vMessage_ConnectionString"; break;
                            case "4": connSetting = "vOnline_ConnectionString"; break;
                        }

                        String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings[connSetting].ConnectionString;
                        SqlConnection openConn = new SqlConnection(connectionString);
                        System.IO.StreamReader reader = new System.IO.StreamReader(fileName);
                        String sqlScript = reader.ReadToEnd();
                        reader.Close();
                        openConn.Open();
                        SqlCommand dbComd = new SqlCommand(sqlScript, openConn);
                        dbComd.ExecuteNonQuery();
                        openConn.Close();

                    }
                    #endregion
                    System.IO.File.Delete(fileName);
                }
                if (Convert.ToInt32(deploy.tblApplication.ApplicationTypeID) == 3)
                {
                    String computerName = GetSettingByName("WindowsServiceComputerName", Convert.ToInt32(GetApplicationByName(Convert.ToByte(GetSystemByName("Back Office").SystemID), "VCS.BackOffice.Deploy").ApplicationID)).SettingValue;
                    System.ServiceProcess.ServiceController service = new System.ServiceProcess.ServiceController(deploy.tblApplication.vcsApplication, computerName);
                    TimeSpan waitForService = TimeSpan.FromMilliseconds(600000);
                    if (restartService)
                    {
                        if (service.Status != System.ServiceProcess.ServiceControllerStatus.Running)
                        {
                            service.Start();
                            service.WaitForStatus(System.ServiceProcess.ServiceControllerStatus.Running);
                        }
                    }
                }
                #endregion
                #region Update deployment record
                deploy.StatusID = 5;
                deploy.ChangeDeveloperID = pDeveloperID;
				tblDeployCrud crudDep = new tblDeployCrud();
				crudDep.Update(deploy);
                SendMail(pDeployID, "Rollback");
                #endregion
                return deploymentResult;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("RollbackFile", "(Int32 pBackupFileID)", false);
                SaveDeployError(pDeployID, Ex.Message);
                WriteLog(deployApplicationID, "DeployID: " + pDeployID.ToString() + "." + Ex.Message, eLogType.Error);
                SaveDeployStatus(pDeployID, 3, pDeveloperID);
                SendMail(pDeployID, "Failed");
                return Ex.Message;
            }
        }

		List<tblDeployNotification> GetDeployNotification(Int32 pDeployID)
		{
			try
			{
                UpdateTempMethod("RollbackFile", "(Int32 pBackupFileID)", true);
				tblDeployNotificationCrud crud = new tblDeployNotificationCrud();
				crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);
				recordFound = false;
				List<tblDeployNotification> results = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return results;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("RollbackFile", "(Int32 pBackupFileID)", false);
				throw new Exception(errorMessage + "GetDeployNotification-" + Ex.Message);
			}
		}

		private String GetDeployMailHeader(Int32 pDeployID, Int32 pDeveloperID)
		{
			try
			{
                UpdateTempMethod("RollbackFile", "(Int32 pBackupFileID)", true);
				tblDeploy deploy = GetDeploy(pDeployID);
				String system = deploy.tblApplication.tblSystem.vcsSystem;
				String application = deploy.tblApplication.vcsApplication;
				String developer = deploy.tblDeveloper.Name;
				String server = deploy.tblApplication.tblServer.vcsServer;
				String requestDateTime = Convert.ToDateTime(deploy.StatusDateTime).ToString("yyyy-MM-dd HH:mm:ss");
				String url = GetSettingByName("ActionManagerURL", Convert.ToInt32(GetApplicationByName(Convert.ToByte(GetSystemByName("Back Office").SystemID), "VCS.BackOffice.Deploy").ApplicationID)).SettingValue;
				String status = deploy.tblDeployStatus.DeployStatus;
				String ticketNo = deploy.TicketNo;
				Boolean viewOnly = false;

				if (deploy.StatusID.ToString() == "2" || deploy.StatusID.ToString() == "3" || deploy.StatusID.ToString() == "4")
					viewOnly = true;

				String cancelURL = GetSettingByName("ActionDeclineURL", Convert.ToInt32(GetApplicationByName(Convert.ToByte(GetSystemByName("Back Office").SystemID), "VCS.BackOffice.Deploy").ApplicationID)).SettingValue;
				String deployURL = GetSettingByName("ActionDeployURL", Convert.ToInt32(GetApplicationByName(Convert.ToByte(GetSystemByName("Back Office").SystemID), "VCS.BackOffice.Deploy").ApplicationID)).SettingValue;
				url = url.Replace("{?DeployID}", pDeployID.ToString());
				cancelURL = cancelURL.Replace("{?DeployID}", pDeployID.ToString());
				deployURL = deployURL.Replace("{?DeployID}", pDeployID.ToString());
				cancelURL = cancelURL.Replace("{?DeveloperID}", pDeveloperID.ToString());
				deployURL = deployURL.Replace("{?DeveloperID}", pDeveloperID.ToString());

				String header = "<html>\n";
				header += "<body bgcolor=white>\n";
				//header += "<style>.small{font-size:8pt;font-family:verdana;border-collapse:collapse;border-spacing:0;}\n";
				//header += "<table class=small align=left><tr><td>\n";
				header += "<table class='small' style=\"border-collapse:collapse\">\n";
				header += "<tr><td colspan='2' style='font-weight:bold;font-size:14pt;font-family:verdana;width:500px;border:1px solid #778899;text-align:center;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Deployment Information</td></tr>\n";
				//header += "<tr height=10><td></td></tr>\n";
				header += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>System</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + system + "</td></tr>\n";
				header += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Application</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + application + "</td></tr>\n";
				header += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Server</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + server + "</td></tr>\n";
				header += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Developer</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + developer + "</td></tr>\n";
				header += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Status</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + status + "</td></tr>\n";
				header += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Deploy ID</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + deploy.DeployID.ToString() + "</td></tr>\n";
				header += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Requesting Date & Time</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + requestDateTime + "</td></tr>\n";
				header += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Ticket No</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + ticketNo + "</td></tr>\n";
				header += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Action</td>";
				header += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;color:black;padding:2px 2px 2px 2px'><a href='" + url + "'>View</a>\n";
				if (!viewOnly)
				{
					header += "<a href='" + deployURL + "'>Deploy</a>\n";
					header += "<a href='" + cancelURL + "'>Cancel</a></td></tr>\n";
				}
				else
				{
					header += "</td></tr>\n";
				}
				header += "</table>\n";

				List<tblDeployBinaryData> binaries = GetDeployBinaryData(pDeployID);
				if (binaries.Count > 0)
				{
					header += "<br /><br />";
					header += "<table style=\"border-collapse:collapse\">\n";
					header += "<tr><td colspan='5' style='font-weight:bold;font-size:14pt;font-family:verdana;width:800px;border:1px solid #778899;text-align:center;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Deployment Files</td></tr>\n";
					header += "<tr style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'><th style='border:1px solid #778899'>File Name</th><th style='border:1px solid #778899'>File Type</th><th style='border:1px solid #778899'>Folder</th><th style='border:1px solid #778899'>Gac Deployment</th><th style='border:1px solid #778899'>Database</th></tr>\n";
					foreach (tblDeployBinaryData binary in binaries)
					{
						header += "<tr><td style='vertical-align:top;width:130px;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + binary.BinaryDataFileName + "</td>";
						header += "<td style='vertical-align:top;width:150px;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + binary.tblDeployBinaryDataType.BinaryDataType + "</td>\n";
						String folderPath = binary.tblApplicationFolder == null ? "" : binary.tblApplicationFolder.FolderPath;
						header += "<td style='width:530px;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + folderPath + "</td>\n";
						header += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + binary.GacDeploy + "</td>\n";
						String databaseName = binary.tblDatabase == null ? "" : binary.tblDatabase.DatabaseName;
						header += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + databaseName + "</td></tr>\n";
					}
					header += "</table>\n";
				}


				List<tblDeployNote> notes = GetDeployNote(pDeployID);

				if (notes.Count > 0)
				{
					header += "<br /><br />";
					header += "<table style=\"border-collapse:collapse\">\n";
					header += "<tr><td colspan='3' style='font-weight:bold;font-size:14pt;font-family:verdana;width:800px;border:1px solid #778899;text-align:center;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Deployment Notes</td></tr>\n";
					header += "<tr style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'><th style='border:1px solid #778899'>Developer</th><th style='border:1px solid #778899'>Date & Time</th><th style='border:1px solid #778899'>Note</th></tr>\n";
					foreach (tblDeployNote note in notes)
					{
						header += "<tr><td style='vertical-align:top;width:120px;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + note.tblDeveloper.Name + "</td>";
						header += "<td style='vertical-align:top;width:150px;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + Convert.ToDateTime(note.NoteDateTime).ToString("yyyy-MM-dd HH:mm:ss") + "</td>\n";
						header += "<td style='width:530px;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + note.Note.Replace("\r\n", "<br />") + "</td></tr>\n";
					}
					header += "</table>\n";
				}

				List<tblDeployError> errors = GetDeployErrorByDeployID(pDeployID);

				if (errors.Count > 0)
				{
					header += "<br /><br />";
					header += "<table style=\"border-collapse:collapse\">\n";
					header += "<tr><td colspan='3' style='font-weight:bold;font-size:14pt;font-family:verdana;width:800px;border:1px solid #778899;text-align:center;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Deployment Errors</td></tr>\n";
					header += "<tr style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'><th style='border:1px solid #778899'>Error</th></tr>\n";
					foreach (tblDeployError error in errors)
					{
						header += "<tr><td style='vertical-align:top;width:120px;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + error.ErrorMessage + "</td></tr>";
					}
					header += "</table>\n";
				}
				header += "</body>\n";
				header += "</html>\n";
				return header;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("RollbackFile", "(Int32 pBackupFileID)", false);
				throw Ex;
			}
		}
		private Boolean SendMail(Int32 pDeployID, String pSubject)
		{
			try
			{
                UpdateTempMethod("RollbackFile", "(Int32 pBackupFileID)", true);
				tblDeploy deploy = GetDeploy(pDeployID);

				pSubject = "Deployment - " + deploy.tblApplication.vcsApplication + " - " + pSubject;

				List<tblDeployNotification> notficationList = GetDeployNotification(pDeployID);
				String allreadySend = "";
				foreach (tblDeployNotification noti in notficationList)
				{
					if (!allreadySend.Contains(noti.EMailAddress))
					{
						String mailBody = GetDeployMailHeader(pDeployID, Convert.ToInt32(deploy.DeveloperID));

						String recipient = "";
						recipient = noti.EMailAddress;

						messBL.UseMailBodyAsIs = true;
						messBL.SendMail(pSubject, mailBody, recipient, "VCS.BackOffice.Deploy");
						allreadySend += noti.EMailAddress + ";";
					}
				}
				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("RollbackFile", "(Int32 pBackupFileID)", false);
				throw Ex;
			}
		}

		public String DeployApplication(Int32 pDeployID, Int32 pDeveloperID)
        {
            Byte systemID = Convert.ToByte(GetSystemByName("Back Office").SystemID);
            Int32 deployApplicationID = Convert.ToInt32(GetApplicationByName(systemID, "VCS.BackOffice.Deploy").ApplicationID);

            try
            {
                UpdateTempMethod("DeployApplication", "(Int32 pDeployID, Int32 pDeveloperID)", true);
                deployInformation = "DeployID=" + pDeployID.ToString() + "; DeveloperID=" + pDeveloperID.ToString();
                String deploymentResult = "Success";
                #region Check If Deployment Request is Valid
                tblDeploy deploy = GetDeploy(pDeployID);
                if (deploy.ApplicationID == null)
                {
                    deploymentResult = "Deployment record was not found.";
                    WriteLog(deployApplicationID, "DeployID: " + pDeployID.ToString() + ". " + deploymentResult, eLogType.Error);

                    return deploymentResult;
                }
                if (deploy.StatusID != 1)
                {
                    deploymentResult = "Invalid deployment status (" + deploy.tblDeployStatus.DeployStatus + ")";
                    WriteLog(deployApplicationID, "DeployID: " + pDeployID.ToString() + ". " + deploymentResult, eLogType.Error);
                    return deploymentResult;
                }

                tblDeployer deployer = GetDeployer(Convert.ToInt32(deploy.ApplicationID), pDeveloperID);
                if (deployer == null)
                {
                    deploymentResult = "You are not allowed to deploy this application.";
                    SaveDeployError(pDeployID, deploymentResult);
                    SaveDeployStatus(pDeployID, 3, pDeveloperID);
                    WriteLog(deployApplicationID, "DeployID: " + pDeployID.ToString() + ". " + deploymentResult, eLogType.Error);
                    return deploymentResult;
                }
                #endregion
                #region Backup
                List<tblDeployBinaryData> binaries = GetDeployBinaryData(pDeployID);
                Int32 backupID = SaveDeployBackupHeader(pDeployID);
                tblServer appServer = GetApplicationServer(Convert.ToInt32(deploy.ApplicationID));
                if (appServer == null)
                {
                    return "This application does not have a server linked to it.";
                }
                String folderToBackup = GetApplicationServer(Convert.ToInt32(deploy.ApplicationID)).vcsServer;
                if (folderToBackup == "")
                {
                    WriteLog(deployApplicationID, "DeployID: " + pDeployID.ToString() + ". Application not linked to a server.", eLogType.Error);
                    return "Application not linked to a server";
                }     
                folderToBackup = "\\\\" + folderToBackup + "\\";
                List<tblApplicationFolder> folders =   GetApplicationFolder(Convert.ToInt32(deploy.ApplicationID));
                if (folders.Count == 0)
                {
                    WriteLog(deployApplicationID, "Application not linked to a server. Information: " + deployInformation, eLogType.Error);
                    return "The folder for this application was not set up.";
                }

                Int32 backupCount = 0;
                foreach(tblApplicationFolder folder in folders)
                {
                    String folderPath = folderToBackup + folder.FolderPath.ToLower().Replace("c:", "c$").Replace("d:", "d$");
                    foreach(String fileName in System.IO.Directory.GetFiles(folderPath))
                    {
                        System.IO.FileInfo fInfo = new System.IO.FileInfo(fileName);
                        tblDeployBinaryData deployFile = binaries.Find(x => x.BinaryDataFileName == fInfo.Name);
                        if (deployFile != null)
                        {
                            Byte[] fileBytes = System.IO.File.ReadAllBytes(fileName);
                            System.IO.FileInfo fileInfo = new System.IO.FileInfo(fileName);
                            DateTime fileDateTime = fileInfo.LastWriteTime;
                            Int32 binaryDataID = Convert.ToInt32(deployFile.BinaryDataID);
                            SaveDeployBackupFile(backupID, Convert.ToInt32(folder.FolderID), fileBytes, fileInfo.Name, fileDateTime, fileBytes.GetLength(0).ToString(), binaryDataID);
                            backupCount++;
                        }
                    }
                }

                deployInformation += "; BackupID=" + backupID.ToString() + "; Backup Count=" + backupCount.ToString();
                List<tblDeployBinaryData> gacDeployFiles = binaries.FindAll(x => x.GacDeploy == true).ToList();
                backupCount = 0;
                foreach (tblDeployBinaryData gacFile in gacDeployFiles)
                {
                    tblApplication applicationBack = GetApplicationByName(gacFile.BinaryDataFileName.Substring(0, gacFile.BinaryDataFileName.Length - 4));
                    if (applicationBack == null)
                    {
                        WriteLog(deployApplicationID, "GAC file was not setup as an application. Information: " + deployInformation, eLogType.Error);
                        return "GAC file was not setup as an application.";
                    }

                    List<tblApplicationFolder> appFolders = GetApplicationFolder(Convert.ToInt32(applicationBack.ApplicationID));
                    if (appFolders.Count == 0)
                    {
                        WriteLog(deployApplicationID, "Gac file folder was not set up. Information: " + deployInformation, eLogType.Error);
                        return "Gac file folder was not set up.";
                    }

                    String fileName = appFolders[0].FolderPath + "\\" + gacFile.BinaryDataFileName;
                    Byte[] fileBytes = System.IO.File.ReadAllBytes(fileName);
                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(fileName);
                    DateTime fileDateTime = fileInfo.LastWriteTime;
                    Int32 binaryDataID = Convert.ToInt32(gacFile.BinaryDataID);
                    SaveDeployBackupFile(backupID, Convert.ToInt32(appFolders[0].FolderID), fileBytes, fileInfo.Name, fileDateTime, fileBytes.GetLength(0).ToString(), binaryDataID);
                    backupCount++;

                }
                deployInformation += "; GAC Backup Count=" + backupCount.ToString();
                #endregion

                #region Deployment
                //Get the server work folder
                String workFolder = GetSettingByName("ServerWorkFolder", Convert.ToInt32(GetApplicationByName(Convert.ToByte(GetSystemByName("Back Office").SystemID), "VCS.BackOffice.Deploy").ApplicationID)).SettingValue;
                if (!System.IO.Directory.Exists(workFolder))
                {
                    System.IO.Directory.CreateDirectory(workFolder);
                }


                tblApplication application = GetApplication(Convert.ToInt32(deploy.ApplicationID));
                tblServer server = GetServer(Convert.ToInt32(application.ServerID));
                String serverName = server.vcsServer;
                if (serverName == "")
                {
                    WriteLog(deployApplicationID, "Application not linked to a server. Information: " + deployInformation, eLogType.Error);
                    return "Application not linked to a server";
                }

                //Stop the windows service if it is one
                #region Service Stop & Uninstall
                Boolean restartService = false;
                if (Convert.ToInt32(deploy.tblApplication.ApplicationTypeID) == 3)
                {
                    System.ServiceProcess.ServiceController service = new System.ServiceProcess.ServiceController(deploy.tblApplication.vcsApplication, serverName);

                    //Exception exceptionMessage = ((System.Exception)(service.ServiceName));

                    if (service != null)
                    {
                        TimeSpan waitForService = TimeSpan.FromMilliseconds(600000);
                        if (service.Status != System.ServiceProcess.ServiceControllerStatus.Stopped)
                        {
                            service.Stop();
                            service.WaitForStatus(System.ServiceProcess.ServiceControllerStatus.Stopped);
                            restartService = true;
                            deployInformation += "; Windows Service Stopped.";
                        }
                        else
                        {
                            deployInformation += "; Windows Service Was Not Running.";
                        }
                    }
                    else
                        restartService = true;

                    //System.Configuration.Install.ManagedInstallerClass.InstallHelper(new String[] {"/i", @" C:\Applic\VirtualVendor\WindowsServices\VCS.VirtualVendorTi.FileImportTrigger\VCS.VirtualVendor.FileImportTrigger.exe" });

                    Console.WriteLine(System.Reflection.Assembly.GetExecutingAssembly().Location);

                    //Only do the uninstall when the exe is  being deployed
                    String deployFolder = deploy.tblApplication.ApplicationPath;
                    switch (deployFolder.Substring(0, 2).ToUpper())
                    {
                        case "C:": deployFolder = "C$" + deployFolder.Substring(2, deployFolder.Length - 2); break;
                        case "D:": deployFolder = "D$" + deployFolder.Substring(2, deployFolder.Length - 2); break;
                    }
                    deployFolder = @"\\" + serverName + "\\" + deployFolder;

                    /*String installUtilFolder = GetSettingByName("InstallUtilFolder", Convert.ToInt32(GetApplicationByName(Convert.ToByte(GetSystemByName("Back Office").SystemID), "VCS.BackOffice.Deploy").ApplicationID)).SettingValue;
                    System.Diagnostics.Process installUtil = new System.Diagnostics.Process();

                    String serviceName = deployFolder + "\\" + deploy.tblApplication.vcsApplication;
                    installUtil.StartInfo.FileName = installUtilFolder + "\\InstallUtil.exe";
                    installUtil.StartInfo.UseShellExecute = false;
                    installUtil.StartInfo.Arguments = "/u \"" + serviceName + ".exe\"";

                    installUtil.StartInfo.RedirectStandardOutput = true;
                    installUtil.Start();*/
                    String result = "";

                    System.Diagnostics.Process installUtil = new System.Diagnostics.Process();
                    String serviceName = deployFolder + "\\" + deploy.tblApplication.vcsApplication;
                    
                    using (System.IO.StreamReader procOutPut = installUtil.StandardOutput)
                    {
                        result += procOutPut.ReadToEnd();
                    }

                    if (installUtil.ExitCode != 0)
                    {
                        deploymentResult = "InstallUtil Error (" + installUtil.ExitCode.ToString() + ":" + result;
                        SaveDeployError(pDeployID, deploymentResult);
                        SaveDeployStatus(pDeployID, 3, pDeveloperID);
                        SendMail(pDeployID, "Failed");
                        WriteLog(deployApplicationID, deploymentResult + ". Information: " + deployInformation, eLogType.Error);
                        return deploymentResult;
                    }

                    deployInformation += "; Windows Service Uninstalled.";
                    
                }
                #endregion

                Int32 deployCount = 0;
                Int32 gacCount = 0;
                Int32 scriptCount = 0;
                foreach (tblDeployBinaryData binary in binaries)
                {
                    #region Write file to disk
                    String fileName = workFolder + "\\" + binary.BinaryDataFileName;
                    System.IO.FileStream writeStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                    writeStream.Write(binary.BinaryData, 0, binary.BinaryData.Length);
                    writeStream.Close();
                    #endregion
                    if (binary.BinaryDataTypeID == 1 || binary.BinaryDataTypeID == 3)
                    {
                        #region File Copy Deployment
                        
                        if (binary.FolderID != 0 && !Convert.ToBoolean(binary.GacDeploy))
                        {
                            //Get the folder name
                            
                            tblApplicationFolder folder =  GetApplicationFolderByID(Convert.ToInt32(binary.FolderID));
                            String deployFolder = folder.FolderPath;
                            switch (deployFolder.Substring(0, 2).ToUpper())
                            {
                                case "C:": deployFolder = "C$" + deployFolder.Substring(2, deployFolder.Length-2); break;
                                case "D:": deployFolder = "D$" + deployFolder.Substring(2, deployFolder.Length-2); break;
                            }
                            deployFolder = @"\\" + serverName + "\\" + deployFolder;

                            if (!System.IO.Directory.Exists(deployFolder))
                            {
                                deploymentResult = "Deployment folder was not found. (" + folder.FolderPath + ")";
                                WriteLog(deployApplicationID, "DeployID: " + pDeployID.ToString() + ". Binary: " + binary.BinaryDataFileName + ". " + deploymentResult , eLogType.Error);
                                SaveDeployError(pDeployID, deploymentResult);
                                SaveDeployStatus(pDeployID, 3, pDeveloperID);
                                SendMail(pDeployID, "Failed");
                                return deploymentResult;
                            }

                            String destination = deployFolder;
                            if (destination.Substring(destination.Length - 1) != "\\")
                                destination = destination + "\\";
                            destination += binary.BinaryDataFileName;

                            System.IO.File.Copy(fileName, destination, true);
                            deployCount++;
                        }
                        #endregion
                        #region Deploy to Global Assembly Cache
                        if (Convert.ToBoolean(binary.GacDeploy))
                        {
                            String gacUtilFolder = GetSettingByName("GACUtilFolder", Convert.ToInt32(GetApplicationByName(Convert.ToByte(GetSystemByName("Back Office").SystemID), "VCS.BackOffice.Deploy").ApplicationID)).SettingValue;
                            System.Diagnostics.Process cscBuilder = new System.Diagnostics.Process();
                            String dllName = binary.BinaryDataFileName;
                            cscBuilder.StartInfo.FileName = gacUtilFolder + "GacUtil.exe";
                            cscBuilder.StartInfo.UseShellExecute = false;
                            cscBuilder.StartInfo.Arguments = "/u \"" + dllName.Substring(0, dllName.Length - 4) + "\"";

                            cscBuilder.StartInfo.RedirectStandardOutput = true;
                            cscBuilder.Start();
                            String result = "";
                            using (System.IO.StreamReader procOutPut = cscBuilder.StandardOutput)
                            {
                                result += procOutPut.ReadToEnd();
                            }

                            if (cscBuilder.ExitCode != 0)
                            {
                                deploymentResult = "GacUtil Error (" + cscBuilder.ExitCode.ToString() + ":" + result;
                                WriteLog(deployApplicationID, "DeployID: " + pDeployID.ToString() + ". Binary: " + binary.BinaryDataFileName + ". " + deploymentResult, eLogType.Error);
                                SaveDeployError(pDeployID, deploymentResult);
                                SaveDeployStatus(pDeployID, 3, pDeveloperID);
                                SendMail(pDeployID, "Failed");
                                return deploymentResult;
                            }


                            cscBuilder = new System.Diagnostics.Process();
                            //cscBuilder.StartInfo.WorkingDirectory = workFolder;
                            cscBuilder.StartInfo.FileName = gacUtilFolder + "GacUtil.exe";
                            cscBuilder.StartInfo.UseShellExecute = false;
                            cscBuilder.StartInfo.Arguments = "/i \"" + fileName + "\"";

                            cscBuilder.StartInfo.RedirectStandardOutput = true;
                            cscBuilder.Start();
                            result = "";
                            using (System.IO.StreamReader procOutPut = cscBuilder.StandardOutput)
                            {
                                result += procOutPut.ReadToEnd();
                            }

                            if (cscBuilder.ExitCode != 0)
                            {
                                deploymentResult = "GacUtil Error (" + cscBuilder.ExitCode.ToString() + "): " + result;
                                WriteLog(deployApplicationID, "DeployID: " + pDeployID.ToString() + ". Binary: " + binary.BinaryDataFileName + ". " + deploymentResult, eLogType.Error);
                                SaveDeployError(pDeployID, deploymentResult);
                                SaveDeployStatus(pDeployID, 3, pDeveloperID);
                                SendMail(pDeployID, "Failed");
                                return deploymentResult;
                            }

                            gacCount++;
                        }
                        #endregion
                    }
                    #region Apply Script To Database
                    if (binary.BinaryDataTypeID == 2)
                    {
                        String connSetting = "";
                        switch (binary.DatabaseID.ToString())
                        {
                            case "1": connSetting = "backOffice_ConnectionString"; break;
                            case "2": connSetting = "vAccount_ConnectionString"; break;
                            case "3": connSetting = "vMessage_ConnectionString"; break;
                            case "4": connSetting = "vOnline_ConnectionString"; break;
                        }

                        String connectionString = System.Configuration.ConfigurationManager.ConnectionStrings[connSetting].ConnectionString;
                        SqlConnection openConn = new SqlConnection(connectionString);
                        openConn.Open();
                        
                        //Microsoft.SqlServer.Management.Smo.Server smoServer = new Microsoft.SqlServer.Management.Smo.Server(new Microsoft.SqlServer.Management.Common.ServerConnection(openConn));
                        
                        System.IO.StreamReader reader = new System.IO.StreamReader(fileName);
                        String sqlScript = reader.ReadToEnd();
                        reader.Close();
                        //smoServer.ConnectionContext.ExecuteNonQuery(sqlScript);
                        openConn.Close();
                        scriptCount++;
                        
                    }
                    #endregion
                    System.IO.File.Delete(fileName);
                }
                deployInformation += "; File Copy Count=" + deployCount.ToString() + "; GAC Count=" + gacCount.ToString() + "; Script Count=" + scriptCount.ToString();
                if (Convert.ToInt32(deploy.tblApplication.ApplicationTypeID) == 3)
                {
                    String computerName = GetSettingByName("WindowsServiceComputerName", Convert.ToInt32(GetApplicationByName(Convert.ToByte(GetSystemByName("Back Office").SystemID), "VCS.BackOffice.Deploy").ApplicationID)).SettingValue;
                    System.ServiceProcess.ServiceController service = new System.ServiceProcess.ServiceController(deploy.tblApplication.vcsApplication, computerName);
                    TimeSpan waitForService = TimeSpan.FromMilliseconds(600000);
                    if (restartService)
                    {
                        if (service.Status != System.ServiceProcess.ServiceControllerStatus.Running)
                        {
                            service.Start();
                            service.WaitForStatus(System.ServiceProcess.ServiceControllerStatus.Running);
                            deployInformation += "; Windows Service Started";
                        }
                    }
                }
                #endregion
                #region Update deployment record
                deploy.StatusID = 4;
                deploy.ChangeDeveloperID = pDeveloperID;
				tblDeployCrud crudDep = new tblDeployCrud();
				crudDep.Update(deploy);
                SendMail(pDeployID, "Deployed");
                #endregion

                deployInformation += "; Success";
                WriteLog(deployApplicationID, deployInformation, eLogType.Information);
                return deploymentResult;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeployApplication", "(Int32 pDeployID, Int32 pDeveloperID)", false);
                SaveDeployError(pDeployID, Ex.Message);
                WriteLog(deployApplicationID, deployInformation + "; Error=" + Ex.Message , eLogType.Error);
                SaveDeployStatus(pDeployID, 3, pDeveloperID);
                SendMail(pDeployID, "Failed");
                return Ex.Message;
            }
        }
        public Boolean SaveDeployStatus(Int32 pDeployID, Int32 pStatusID, Int32 pDeveloperID)
        {
            try
            {
                UpdateTempMethod("SaveDeployStatus", "(Int32 pDeployID, Int32 pStatusID, Int32 pDeveloperID)", true);
				tblDeploy deploy = GetDeploy(pDeployID);
				deploy.StatusID = pStatusID;
				deploy.ChangeDeveloperID = pDeveloperID;
				tblDeployCrud crud = new tblDeployCrud();
				crud.Update(deploy);
				return true;
			}
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveDeployStatus", "(Int32 pDeployID, Int32 pStatusID, Int32 pDeveloperID)", false);
                throw new Exception("SaveDeployStatus - " + Ex.Message);
            }
        }
        public List<tblDeployBackupFile> GetDeployBackupFile(Int32 pBackupID)
        {
            try
            {
                UpdateTempMethod("GetDeployBackupFile", "(Int32 pBackupID)", true);
                recordFound = false;
                tblDeployBackupFileCrud crud = new tblDeployBackupFileCrud();
                crud.Where("BackupID", DAL.General.Operator.Equals, pBackupID);

                List<tblDeployBackupFile> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployBackupFile", "(Int32 pBackupID)", false);
                throw new Exception("GetDeployBackupFile - " + Ex.Message);
            }
        }
        public tblDeployBackupFile GetDeployBackupFilePerFileID(Int32 pBackupFileID)
        {
            try
            {
                UpdateTempMethod("GetDeployBackupFilePerFileID", "(Int32 pBackupFileID)", true);
                recordFound = false;
                tblDeployBackupFileCrud crud = new tblDeployBackupFileCrud();
                crud.Where("BackupFileID", DAL.General.Operator.Equals, pBackupFileID);

                tblDeployBackupFile search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployBackupFilePerFileID", "(Int32 pBackupFileID)", false);
                throw new Exception("GetDeployBackupFilePerFileID - " + Ex.Message);
            }
        }
        public vDeployBackupLatest GetDeployBackupLatest(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("GetDeployBackupLatest", "(Int32 pDeployID)", true);
                recordFound = false;
                vDeployBackupLatestCrud crud = new vDeployBackupLatestCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                vDeployBackupLatest search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployBackupLatest", "(Int32 pDeployID)", false);
                throw new Exception("GetDeployBackupLatest - " + Ex.Message);
            }
        }
        public String GetDeployNoteGridData(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("GetDeployNoteGridData", "(Int32 pDeployID)", true);
                List<tblDeployNote> notes = GetDeployNote(pDeployID);
                String divString = "";
                foreach (tblDeployNote note in notes)
                {
                    divString += note.DeployID + "═";
                    divString += note.NoteID + "═";
                    divString += note.DeveloperID + "═";
                    divString += note.tblDeveloper.Name + "═";
                    divString += Convert.ToDateTime(note.NoteDateTime).ToString("yyyy-MM-dd HH:mm:ss") + "═";
                    if (note.Note.Length > 60)
                        divString += note.Note.Substring(0, 60) + "...═";
                    else
                        divString += note.Note + "═";
                    divString += note.Note + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployNoteGridData", "(Int32 pDeployID)", false);
                throw Ex;
            }
        }
        public List<tblDeployNote> GetDeployNote(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("GetDeployNote", "(Int32 pDeployID)", true);
                recordFound = false;
                tblDeployNoteCrud crud = new tblDeployNoteCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                List<tblDeployNote> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployNote", "(Int32 pDeployID)", false);
                throw new Exception("GetDeployNote - " + Ex.Message);
            }
        }
        public String GetDeployRequestGridData()
        {
            try
            {
                UpdateTempMethod("GetDeployRequestGridData", "()", true);
                vDeployRequestCrud crud = new vDeployRequestCrud();
                List<vDeployRequest> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (vDeployRequest workRec in search)
                {
                    divString += workRec.DeployID + "═";
                    divString += workRec.vcsApplication + "═";
                    divString += workRec.Name + "═";
                    divString += workRec.DeployStatus + "═";
                    divString += Convert.ToDateTime(workRec.StatusDateTime).ToString("yyyy-MM-dd HH:mm:ss") + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployRequestGridData", "()", false);
                throw new Exception("GetDeployRequestGridData - " + Ex.Message);
            }
        }
        public Boolean CancelDeploy(Int32 pDeployID, Int32 pCancelReasonID, Int32 pDeveloperID)
        {
            try
            {
                UpdateTempMethod("CancelDeploy", "(Int32 pDeployID, Int32 pCancelReasonID, Int32 pDeveloperID)", true);
                tblDeployCancel cancel = new tblDeployCancel();
                cancel.DeployID = pDeployID;
                cancel.ReasonID = pCancelReasonID;
                cancel.DeveloperID = pDeveloperID;
				tblDeployCancelCrud crud = new tblDeployCancelCrud();
				crud.Insert(cancel);

                tblDeploy deploy = GetDeploy(pDeployID);
                deploy.ChangeDeveloperID = pDeveloperID;
                deploy.StatusID = 2;
                deploy.StatusDateTime = DateTime.Now;
				tblDeployCrud crudDep = new tblDeployCrud();
				crudDep.Update(deploy);

                SendMail(pDeployID, "Cancel");
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("CancelDeploy", "(Int32 pDeployID, Int32 pCancelReasonID, Int32 pDeveloperID)", false);
                throw Ex;
            }
        }
        public Boolean CancelDeploy(Int32 pDeployID, Int32 pCancelReasonID, Int32 pDeveloperID, String pNote)
        {
            try
            {
                UpdateTempMethod("CancelDeploy", "(Int32 pDeployID, Int32 pCancelReasonID, Int32 pDeveloperID, String pNote)", true);
                tblDeployCancel cancel = new tblDeployCancel();
                cancel.DeployID = pDeployID;
                cancel.ReasonID = pCancelReasonID;
                cancel.DeveloperID = pDeveloperID;
				tblDeployCancelCrud crud = new tblDeployCancelCrud();
				crud.Insert(cancel);


				tblDeploy deploy = GetDeploy(pDeployID);
                deploy.ChangeDeveloperID = pDeveloperID;
                deploy.StatusID = 2;
                deploy.StatusDateTime = DateTime.Now;
				tblDeployCrud crudDep = new tblDeployCrud();
				crudDep.Update(deploy);

				tblDeployNote note = new tblDeployNote();
                note.DeployID = pDeployID;
                note.DeveloperID = pDeveloperID;
                note.Note = pNote;
                note.NoteDateTime = DateTime.Now;

				tblDeployNoteCrud crudNote = new tblDeployNoteCrud();
				crudNote.Update(note);


                SendMail(pDeployID, "Cancel");
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("CancelDeploy", "(Int32 pDeployID, Int32 pCancelReasonID, Int32 pDeveloperID, String pNote)", false);
                throw Ex;
            }
        }
        public List<vDeployRequest> GetDeployRequest(Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("GetDeployRequest", "(Int32 pApplicationID)", true);
                recordFound = false;
                vDeployRequestCrud crud = new vDeployRequestCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);

                List<vDeployRequest> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployRequest", "(Int32 pApplicationID)", false);
                throw new Exception("GetDeployRequest - " + Ex.Message);
            }
        }
        public List<vDeployRequest> GetDeployRequest()
        {
            try
            {
                UpdateTempMethod("GetDeployRequest", "()", true);
                recordFound = false;
                vDeployRequestCrud crud = new vDeployRequestCrud();
                List<vDeployRequest> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployRequest", "()", false);
                throw new Exception("GetDeployRequest - " + Ex.Message);
            }
        }
        public List<tblDeployCancelReason> GetDeployCancelReason()
        {
            try
            {
                UpdateTempMethod("GetDeployCancelReason", "()", true);
                recordFound = false;
                tblDeployCancelReasonCrud crud = new tblDeployCancelReasonCrud();
                List<tblDeployCancelReason> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployCancelReason", "()", false);
                throw new Exception("GetDeployCancelReason - " + Ex.Message);
            }
        }
		public Int32 SaveDeploy(Int32 pApplicationID, Int32 pDeveloperID, String pTicketNo, Boolean pStaffCommunication, Int32 pSubApplicationID)
        {
            try
            {
                UpdateTempMethod("SaveDeploy", "(Int32 pApplicationID, Int32 pDeveloperID, String pTicketNo, Boolean pStaffCommunication, Int32 pSubApplicationID)", true);
                tblDeployCrud crud = new tblDeployCrud();
                tblDeploy workRec = new tblDeploy();
                if (pApplicationID != 0)
                {
                    crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
                    workRec = crud.ReadSingle();
                    workRec.DeveloperID = pDeveloperID;
                    workRec.TicketNo = pTicketNo;
                    workRec.StaffCommunication = pStaffCommunication;
                    workRec.SubApplicationID = pSubApplicationID;
                    crud.Update(workRec);
                    return pApplicationID;
                }
                else
                {
                    workRec.DeveloperID = pDeveloperID;
                    workRec.TicketNo = pTicketNo;
                    workRec.StaffCommunication = pStaffCommunication;
                    workRec.SubApplicationID = pSubApplicationID;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveDeploy", "(Int32 pApplicationID, Int32 pDeveloperID, String pTicketNo, Boolean pStaffCommunication, Int32 pSubApplicationID)", false);
                throw new Exception("SaveDeploy - " + Ex.Message);
            }
        }
		public Int32 SaveDeploy(Int32 pApplicationID, Int32 pDeveloperID, String pTicketNo, Boolean pStaffCommunication)
        {
            try
            {
                UpdateTempMethod("SaveDeploy", "(Int32 pApplicationID, Int32 pDeveloperID, String pTicketNo, Boolean pStaffCommunication)", true);
                tblDeployCrud crud = new tblDeployCrud();
                tblDeploy workRec = new tblDeploy();
                if (pApplicationID != 0)
                {
                    crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
                    workRec = crud.ReadSingle();
                    workRec.DeveloperID = pDeveloperID;
                    workRec.TicketNo = pTicketNo;
                    workRec.StaffCommunication = pStaffCommunication;
                    crud.Update(workRec);
                    return pApplicationID;
                }
                else
                {
                    workRec.DeveloperID = pDeveloperID;
                    workRec.TicketNo = pTicketNo;
                    workRec.StaffCommunication = pStaffCommunication;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveDeploy", "(Int32 pApplicationID, Int32 pDeveloperID, String pTicketNo, Boolean pStaffCommunication)", false);
                throw new Exception("SaveDeploy - " + Ex.Message);
            }
        }
        public Int32 SaveDeployBinaryData(Int32 pDeployID, Byte[] pBinaryData, String pFileName, Int32 pBinaryDataTypeID, Decimal pFileSize, Int32 pFolderID, Boolean pGagDeploy, Int32 pDatabaseID)
        {
            try
            {
                UpdateTempMethod("SaveDeployBinaryData", "(Int32 pDeployID, Byte[] pBinaryData, String pFileName, Int32 pBinaryDataTypeID, Decimal pFileSize, Int32 pFolderID, Boolean pGagDeploy, Int32 pDatabaseID)", true);
                tblDeployBinaryDataCrud crud = new tblDeployBinaryDataCrud();
                tblDeployBinaryData workRec = new tblDeployBinaryData();
                if (pDeployID != 0)
                {
                    crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);
                    workRec = crud.ReadSingle();
                    workRec.BinaryData = pBinaryData;
                    workRec.BinaryDataFileName = pFileName;
                    workRec.BinaryDataTypeID = pBinaryDataTypeID;
                    workRec.FileSize = pFileSize;
                    workRec.FolderID = pFolderID;
                    workRec.GacDeploy = pGagDeploy;
                    workRec.DatabaseID = pDatabaseID;
                    crud.Update(workRec);
                    return pDeployID;
                }
                else
                {
                    workRec.BinaryData = pBinaryData;
                    workRec.BinaryDataFileName = pFileName;
                    workRec.BinaryDataTypeID = pBinaryDataTypeID;
                    workRec.FileSize = pFileSize;
                    workRec.FolderID = pFolderID;
                    workRec.GacDeploy = pGagDeploy;
                    workRec.DatabaseID = pDatabaseID;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveDeployBinaryData", "(Int32 pDeployID, Byte[] pBinaryData, String pFileName, Int32 pBinaryDataTypeID, Decimal pFileSize, Int32 pFolderID, Boolean pGagDeploy, Int32 pDatabaseID)", false);
                throw new Exception("SaveDeployBinaryData - " + Ex.Message);
            }
        }
        public Boolean SendDeployRequestMail(Int32 pApplicationID, Int32 pDeveloperID, Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("SendDeployRequestMail", "(Int32 pApplicationID, Int32 pDeveloperID, Int32 pDeployID)", true);
                #region Send mail to person responsible for deployment
                tblApplication application = GetApplication(pApplicationID);
                tblDeveloper developer = GetDeveloper(pDeveloperID);
                String recipients = "";
                List<tblDeployer> deployers = GetDeployer(pApplicationID);
                foreach (tblDeployer deployer in deployers)
                    recipients += deployer.tblDeveloper.EMail + ";";

                if (deployers.Count == 0)
                    throw new Exception("SendDeployRequestMail. Deployer for application with id " + pApplicationID.ToString() + " was not set up");

                SendMail(pDeployID, "Request");
                #endregion

                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SendDeployRequestMail", "(Int32 pApplicationID, Int32 pDeveloperID, Int32 pDeployID)", false);
                throw Ex;
            }
        }
        public Int32 SaveDeployRequestNote(Int32 pDeployID, Int32 pDeveloperID, String pNote)
        {
            try
            {
                UpdateTempMethod("SaveDeployRequestNote", "(Int32 pDeployID, Int32 pDeveloperID, String pNote)", true);
                tblDeployNoteCrud crud = new tblDeployNoteCrud();
                tblDeployNote workRec = new tblDeployNote();
                if (pDeployID != 0)
                {
                    crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);
                    workRec = crud.ReadSingle();
                    workRec.DeveloperID = pDeveloperID;
                    workRec.Note = pNote;
                    crud.Update(workRec);
                    return pDeployID;
                }
                else
                {
                    workRec.DeveloperID = pDeveloperID;
                    workRec.Note = pNote;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveDeployRequestNote", "(Int32 pDeployID, Int32 pDeveloperID, String pNote)", false);
                throw new Exception("SaveDeployRequestNote - " + Ex.Message);
            }
        }
        public Int32 DeployRequest(Int32 pApplicationID, Int32 pDeveloperID, List<structDeployBinaryData> pDeployData, String pNote, Boolean pSendMail)
        {
            try
            {
                UpdateTempMethod("DeployRequest", "(Int32 pApplicationID, Int32 pDeveloperID, List<structDeployBinaryData> pDeployData, String pNote, Boolean pSendMail)", true);
                #region Insert the deployment record
                tblDeploy deploy = new tblDeploy();
                deploy.ApplicationID = pApplicationID;
                deploy.DeveloperID = pDeveloperID;
                deploy.StatusDateTime = DateTime.Now;
                deploy.StatusID = 1;
                deploy.ChangeDeveloperID = pDeveloperID;
				tblDeployCrud crudDeploy = new tblDeployCrud();
				long ID = 0;
				crudDeploy.Insert(deploy, out ID);
				Int32 deployID = (Int32)ID;


                #endregion
                #region Insert the binary data
                foreach (structDeployBinaryData deployData in pDeployData)
                {
                    tblDeployBinaryData binData = new tblDeployBinaryData();
                    binData.BinaryData = deployData.binaryData;
                    binData.BinaryDataFileName = deployData.fileName;
                    binData.BinaryDataTypeID = deployData.binaryDataTypeID;
                    if (deployData.databaseID != 0)
                        binData.DatabaseID = deployData.databaseID;
                    binData.DeployID = deployID;
                    binData.FileSize = deployData.fileSize;
                    if (deployData.folderID != 0)
                        binData.FolderID = deployData.folderID;
                    binData.GacDeploy = deployData.gac;
					tblDeployBinaryDataCrud crudBin = new tblDeployBinaryDataCrud();
					crudBin.Insert(binData);
                }
                #endregion

                if (pNote != "")
                {
                    tblDeployNote note = new tblDeployNote();
                    note.DeployID = deployID;
                    note.DeveloperID = pDeveloperID;
                    note.Note = pNote;
                    note.NoteDateTime = DateTime.Now;
					tblDeployNoteCrud crudNote = new tblDeployNoteCrud();
					crudNote.Insert(note);

                }
                #region Send mail to person responsible for deployment
                if (pSendMail)
                {
                    tblApplication application = GetApplication(pApplicationID);
                    tblDeveloper developer = GetDeveloper(pDeveloperID);
                    String recipients = "";
                    List<tblDeployer> deployers = GetDeployer(pApplicationID);
                    foreach (tblDeployer deployer in deployers)
                        recipients += deployer.tblDeveloper.EMail + ";";

                    SendMail(deployID, "Request");
                }

                #endregion
                return deployID;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeployRequest", "(Int32 pApplicationID, Int32 pDeveloperID, List<structDeployBinaryData> pDeployData, String pNote, Boolean pSendMail)", false);
                throw Ex;
            }
        }
        public List<tblDeploy> GetDeployByApplicationID(Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("GetDeployByApplicationID", "(Int32 pApplicationID)", true);
                recordFound = false;
                tblDeployCrud crud = new tblDeployCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);

                List<tblDeploy> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployByApplicationID", "(Int32 pApplicationID)", false);
                throw new Exception("GetDeployByApplicationID - " + Ex.Message);
            }
        }
        public List<tblDeployError> GetDeployErrorByDeployID(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("GetDeployErrorByDeployID", "(Int32 pDeployID)", true);
                recordFound = false;
                tblDeployErrorCrud crud = new tblDeployErrorCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                List<tblDeployError> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployErrorByDeployID", "(Int32 pDeployID)", false);
                throw new Exception("GetDeployErrorByDeployID - " + Ex.Message);
            }
        }
        public List<tblDeployError> GetDeployError()
        {
            try
            {
                UpdateTempMethod("GetDeployError", "()", true);
                recordFound = false;
                tblDeployErrorCrud crud = new tblDeployErrorCrud();
                List<tblDeployError> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployError", "()", false);
                throw new Exception("GetDeployError - " + Ex.Message);
            }
        }
        public String GetDeployErrorGridData()
        {
            try
            {
                UpdateTempMethod("GetDeployErrorGridData", "()", true);
                tblDeployErrorCrud crud = new tblDeployErrorCrud();
                List<tblDeployError> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (tblDeployError workRec in search)
                {
                    divString += workRec.DeployErrorID + "═";
                    divString += workRec.DeployID + "═";
                    divString += workRec.ErrorMessage + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployErrorGridData", "()", false);
                throw new Exception("GetDeployErrorGridData - " + Ex.Message);
            }
        }
        public String GetDeployErrorGridData(Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("GetDeployErrorGridData", "(Int32 pApplicationID)", true);
                tblDeployErrorCrud crud = new tblDeployErrorCrud();
                List<tblDeployError> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (tblDeployError workRec in search)
                {
                    divString += workRec.DeployErrorID + "═";
                    divString += workRec.DeployID + "═";
                    divString += workRec.ErrorMessage + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployErrorGridData", "(Int32 pApplicationID)", false);
                throw new Exception("GetDeployErrorGridData - " + Ex.Message);
            }
        }
        public List<tblDeployError> GetDeployError(Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("GetDeployError", "(Int32 pApplicationID)", true);
                List<tblDeploy> deploys = GetDeployByApplicationID(pApplicationID);
                List<tblDeployError> errors = new List<tblDeployError>();
                foreach (tblDeploy deploy in deploys)
                {
					tblDeployErrorCrud crudErr = new tblDeployErrorCrud();
					crudErr.Where("DeployID", DAL.General.Operator.Equals, (Int32)deploy.DeployID);
					List<tblDeployError> errorSearch = crudErr.ReadMulti().ToList();
                    if (errorSearch.Count > 0)
                        foreach (tblDeployError thisError in errorSearch)
                            errors.Add(thisError);
                }

                return errors;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployError", "(Int32 pApplicationID)", false);
                throw Ex;
            }
        }
        public tblDeployer GetDeployer(Int32 pApplicationID, Int32 pDeveloperID)
        {
            try
            {
                UpdateTempMethod("GetDeployer", "(Int32 pApplicationID, Int32 pDeveloperID)", true);
                recordFound = false;
                tblDeployerCrud crud = new tblDeployerCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
                crud.And("DeveloperID", DAL.General.Operator.Equals, pDeveloperID);

                tblDeployer search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployer", "(Int32 pApplicationID, Int32 pDeveloperID)", false);
                throw new Exception("GetDeployer - " + Ex.Message);
            }
        }
        public List<tblDeployer> GetDeployer(Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("GetDeployer", "(Int32 pApplicationID)", true);
                recordFound = false;
                tblDeployerCrud crud = new tblDeployerCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);

                List<tblDeployer> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployer", "(Int32 pApplicationID)", false);
                throw new Exception("GetDeployer - " + Ex.Message);
            }
        }
        public String GetDeployerGridData()
        {
            try
            {
                UpdateTempMethod("GetDeployerGridData", "()", true);
                tblDeployerCrud crud = new tblDeployerCrud();
                List<tblDeployer> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (tblDeployer workRec in search)
                {
                    divString += workRec.DeployerID + "═";
                    divString += workRec.ApplicationID + "═";
                    divString += workRec.tblApplication.vcsApplication + "═";
                    divString += workRec.DeveloperID + "═";
                    divString += workRec.tblDeveloper.Name + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployerGridData", "()", false);
                throw new Exception("GetDeployerGridData - " + Ex.Message);
            }
        }
        public tblDeploy GetDeploy(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("GetDeploy", "(Int32 pDeployID)", true);
                recordFound = false;
                tblDeployCrud crud = new tblDeployCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                tblDeploy search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeploy", "(Int32 pDeployID)", false);
                throw new Exception("GetDeploy - " + Ex.Message);
            }
        }
        public String GetDeployByApplicationGridData(Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("GetDeployByApplicationGridData", "(Int32 pApplicationID)", true);
                tblDeployCrud crud = new tblDeployCrud();
                List<tblDeploy> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (tblDeploy workRec in search)
                {
                    divString += workRec.DeployID + "═";
                    divString += workRec.tblApplication.vcsApplication + "═";
                    divString += workRec.tblDeveloper.Name + "═";
                    divString += workRec.tblDeployStatus.DeployStatus + "═";
                    divString += Convert.ToDateTime(workRec.StatusDateTime).ToString("yyyy-MM-dd HH:mm:ss") + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployByApplicationGridData", "(Int32 pApplicationID)", false);
                throw new Exception("GetDeployByApplicationGridData - " + Ex.Message);
            }
        }
        public List<tblDeploy> GetDeployByApplication(Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("GetDeployByApplication", "(Int32 pApplicationID)", true);
                recordFound = false;
                tblDeployCrud crud = new tblDeployCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);

                List<tblDeploy> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployByApplication", "(Int32 pApplicationID)", false);
                throw new Exception("GetDeployByApplication - " + Ex.Message);
            }
        }
        public Boolean SaveDeployCancelReason(String pCancelReason)
        {
            try
            {
                UpdateTempMethod("SaveDeployCancelReason", "(String pCancelReason)", true);
                tblDeployCancelReasonCrud crud = new tblDeployCancelReasonCrud();
                tblDeployCancelReason workRec = new tblDeployCancelReason();
				workRec.Reason = pCancelReason;
				crud.Insert(workRec);
				return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveDeployCancelReason", "(String pCancelReason)", false);
                throw new Exception("SaveDeployCancelReason - " + Ex.Message);
            }
        }
        public Boolean SaveDeployNote(Int32 pDeployID, Int32 pDeveloperID, String pNote)
        {
            try
            {
                UpdateTempMethod("SaveDeployNote", "(Int32 pDeployID, Int32 pDeveloperID, String pNote)", true);
                tblDeployNoteCrud crud = new tblDeployNoteCrud();
                tblDeployNote workRec = new tblDeployNote();
				workRec.DeployID = pDeployID;
				workRec.DeveloperID = pDeveloperID;
				workRec.Note = pNote;
				workRec.NoteDateTime = DateTime.Now;
				crud.Insert(workRec);
				SendMail(pDeployID, "Note Added");
				return true;

            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveDeployNote", "(Int32 pDeployID, Int32 pDeveloperID, String pNote)", false);
                throw new Exception("SaveDeployNote - " + Ex.Message);
            }
        }
        public Int32 SaveDeployBackupHeader(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("SaveDeployBackupHeader", "(Int32 pDeployID)", true);
                tblDeployBackupCrud crud = new tblDeployBackupCrud();
                tblDeployBackup workRec = new tblDeployBackup();
				workRec.BackDateTime = DateTime.Now;
				workRec.DeployID = pDeployID;
				long ID = 0;
				crud.Insert(workRec, out ID);
				return (Int32)ID;

            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveDeployBackupHeader", "(Int32 pDeployID)", false);
                throw new Exception("SaveDeployBackupHeader - " + Ex.Message);
            }
        }
        public Int32 SaveDeployBackupFile(Int32 pBackupID, Int32 pFolderID, Byte[] pFileData, String pFileName, DateTime pFileDateTime, String pFileSize, Int32 pBinaryDataID)
        {
            try
            {
                UpdateTempMethod("SaveDeployBackupFile", "(Int32 pBackupID, Int32 pFolderID, Byte[] pFileData, String pFileName, DateTime pFileDateTime, String pFileSize, Int32 pBinaryDataID)", true);
                tblDeployBackupFileCrud crud = new tblDeployBackupFileCrud();
                tblDeployBackupFile workRec = new tblDeployBackupFile();
                if (pBackupID != 0)
                {
                    crud.Where("BackupID", DAL.General.Operator.Equals, pBackupID);
                    workRec = crud.ReadSingle();
                    workRec.FolderID = pFolderID;
                    workRec.BinaryData = pFileData;
                    workRec.BackupFileName = pFileName;
                    workRec.FileDateTimeStamp = pFileDateTime;
                    workRec.FileSize = pFileSize;
                    workRec.BinaryDataID = pBinaryDataID;
                    crud.Update(workRec);
                    return pBackupID;
                }
                else
                {
                    workRec.FolderID = pFolderID;
                    workRec.BinaryData = pFileData;
                    workRec.BackupFileName = pFileName;
                    workRec.FileDateTimeStamp = pFileDateTime;
                    workRec.FileSize = pFileSize;
                    workRec.BinaryDataID = pBinaryDataID;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveDeployBackupFile", "(Int32 pBackupID, Int32 pFolderID, Byte[] pFileData, String pFileName, DateTime pFileDateTime, String pFileSize, Int32 pBinaryDataID)", false);
                throw new Exception("SaveDeployBackupFile - " + Ex.Message);
            }
        }
        public Boolean SaveDeployer(Int32 pApplicationID, Int32 pDeveloperID)
        {
            try
            {
                UpdateTempMethod("SaveDeployer", "(Int32 pApplicationID, Int32 pDeveloperID)", true);
                tblDeployerCrud crud = new tblDeployerCrud();
                tblDeployer workRec = new tblDeployer();
				workRec.ApplicationID = pApplicationID;
				workRec.DeveloperID = pDeveloperID;
				crud.Insert(workRec);
				return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveDeployer", "(Int32 pApplicationID, Int32 pDeveloperID)", false);
                throw new Exception("SaveDeployer - " + Ex.Message);
            }
        }
        public List<tblDeployer> GetDeployer()
        {
            try
            {
                UpdateTempMethod("GetDeployer", "()", true);
                recordFound = false;
                tblDeployerCrud crud = new tblDeployerCrud();
                List<tblDeployer> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployer", "()", false);
                throw new Exception("GetDeployer - " + Ex.Message);
            }
        }
        public String GetDeployBinaryDataTypeGridData()
        {
            try
            {
                UpdateTempMethod("GetDeployBinaryDataTypeGridData", "()", true);
                tblDeployBinaryDataTypeCrud crud = new tblDeployBinaryDataTypeCrud();
                List<tblDeployBinaryDataType> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (tblDeployBinaryDataType workRec in search)
                {
                    divString += workRec.BinaryDataTypeID + "═";
                    divString += workRec.BinaryDataType + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployBinaryDataTypeGridData", "()", false);
                throw new Exception("GetDeployBinaryDataTypeGridData - " + Ex.Message);
            }
        }
        public List<tblDeployBinaryDataType> GetDeployBinaryDataType()
        {
            try
            {
                UpdateTempMethod("GetDeployBinaryDataType", "()", true);
                recordFound = false;
                tblDeployBinaryDataTypeCrud crud = new tblDeployBinaryDataTypeCrud();
                List<tblDeployBinaryDataType> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployBinaryDataType", "()", false);
                throw new Exception("GetDeployBinaryDataType - " + Ex.Message);
            }
        }
        public String GetDeployBinaryDataGridData(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("GetDeployBinaryDataGridData", "(Int32 pDeployID)", true);
                List<tblDeployBinaryData> binaryDataList = GetDeployBinaryData(pDeployID);
                String divString = "";
                foreach (tblDeployBinaryData binaryData in binaryDataList)
                {
                    divString += binaryData.DeployID + "═";
                    divString += binaryData.BinaryDataID + "═";
                    divString += binaryData.BinaryDataFileName + "═";
                    divString += binaryData.BinaryDataTypeID + "═";
                    divString += binaryData.tblDeployBinaryDataType.BinaryDataType + "═";
                    if (binaryData.FolderID != null)
                    {
                        divString += binaryData.FolderID + "═";
                        divString += binaryData.tblApplicationFolder.FolderPath + "═";
                    }
                    else
                    {
                        divString += "══";
                    }
                    divString += binaryData.GacDeploy.ToString() + "═";
                    if (binaryData.DatabaseID != null)
                    {
                        divString += binaryData.DatabaseID + "═";
                        divString += binaryData.tblDatabase.DatabaseName + "═";
                    }
                    else
                        divString += "══";
                    divString += binaryData.FileSize + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployBinaryDataGridData", "(Int32 pDeployID)", false);
                throw Ex;
            }
        }
        public List<tblDeployBinaryData> GetDeployBinaryData(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("GetDeployBinaryData", "(Int32 pDeployID)", true);
                recordFound = false;
                tblDeployBinaryDataCrud crud = new tblDeployBinaryDataCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                List<tblDeployBinaryData> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeployBinaryData", "(Int32 pDeployID)", false);
                throw new Exception("GetDeployBinaryData - " + Ex.Message);
            }
        }
        public String GetApplicationFolderGridData(Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("GetApplicationFolderGridData", "(Int32 pApplicationID)", true);
                tblApplicationFolderCrud crud = new tblApplicationFolderCrud();
                List<tblApplicationFolder> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (tblApplicationFolder workRec in search)
                {
                    divString += workRec.FolderID + "═";
                    divString += workRec.FolderPath + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationFolderGridData", "(Int32 pApplicationID)", false);
                throw new Exception("GetApplicationFolderGridData - " + Ex.Message);
            }
        }
        public String GetApplicationFolderGridData()
        {
            try
            {
                UpdateTempMethod("GetApplicationFolderGridData", "()", true);
                tblApplicationFolderCrud crud = new tblApplicationFolderCrud();
                List<tblApplicationFolder> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (tblApplicationFolder workRec in search)
                {
                    divString += workRec.ApplicationID + "═";
                    divString += workRec.FolderID + "═";
                    divString += workRec.FolderPath + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationFolderGridData", "()", false);
                throw new Exception("GetApplicationFolderGridData - " + Ex.Message);
            }
        }
        public String GetServerGridData()
        {
            try
            {
                UpdateTempMethod("GetServerGridData", "()", true);
                tblServerCrud crud = new tblServerCrud();
                List<tblServer> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (tblServer workRec in search)
                {
                    divString += workRec.ServerID + "═";
                    divString += workRec.vcsServer + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetServerGridData", "()", false);
                throw new Exception("GetServerGridData - " + Ex.Message);
            }
        }
        public tblServer GetServer(Int32 pServerID)
        {
            try
            {
                UpdateTempMethod("GetServer", "(Int32 pServerID)", true);
                recordFound = false;
                tblServerCrud crud = new tblServerCrud();
                crud.Where("ServerID", DAL.General.Operator.Equals, pServerID);

                tblServer search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetServer", "(Int32 pServerID)", false);
                throw new Exception("GetServer - " + Ex.Message);
            }
        }
        public List<tblServer> GetServer()
        {
            try
            {
                UpdateTempMethod("GetServer", "()", true);
                recordFound = false;
                tblServerCrud crud = new tblServerCrud();
                List<tblServer> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetServer", "()", false);
                throw new Exception("GetServer - " + Ex.Message);
            }
        }
        public tblServer GetApplicationServer(Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("GetApplicationServer", "(Int32 pApplicationID)", true);
                tblApplication application = GetApplication(pApplicationID);
                return application.tblServer;


            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationServer", "(Int32 pApplicationID)", false);
                throw Ex;
            }
        }
		public List<tblApplicationFolder> GetSubApplicationFolder(Int32 pSubApplicationID)
        {
            try
            {
                UpdateTempMethod("GetSubApplicationFolder", "(Int32 pSubApplicationID)", true);
                recordFound = false;
                tblApplicationFolderCrud crud = new tblApplicationFolderCrud();
                crud.Where("SubApplicationID", DAL.General.Operator.Equals, pSubApplicationID);

                List<tblApplicationFolder> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetSubApplicationFolder", "(Int32 pSubApplicationID)", false);
                throw new Exception("GetSubApplicationFolder - " + Ex.Message);
            }
        }
        public List<tblApplicationFolder> GetApplicationFolder(Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("GetApplicationFolder", "(Int32 pApplicationID)", true);
                recordFound = false;
                tblApplicationFolderCrud crud = new tblApplicationFolderCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);

                List<tblApplicationFolder> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationFolder", "(Int32 pApplicationID)", false);
                throw new Exception("GetApplicationFolder - " + Ex.Message);
            }
        }
        public List<tblApplicationFolder> GetApplicationFolder()
        {
            try
            {
                UpdateTempMethod("GetApplicationFolder", "()", true);
                recordFound = false;
                tblApplicationFolderCrud crud = new tblApplicationFolderCrud();
                List<tblApplicationFolder> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationFolder", "()", false);
                throw new Exception("GetApplicationFolder - " + Ex.Message);
            }
        }
        public tblApplicationFolder GetApplicationFolderByID(Int32 pFolderID)
        {
            try
            {
                UpdateTempMethod("GetApplicationFolderByID", "(Int32 pFolderID)", true);
                recordFound = false;
                tblApplicationFolderCrud crud = new tblApplicationFolderCrud();
                crud.Where("FolderID", DAL.General.Operator.Equals, pFolderID);

                tblApplicationFolder search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationFolderByID", "(Int32 pFolderID)", false);
                throw new Exception("GetApplicationFolderByID - " + Ex.Message);
            }
        }
    }
}
