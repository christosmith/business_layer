﻿using System;
using System.Collections.Generic;
using System.Linq;
using DPO.BackOffice.DAL;

namespace DPO.BackOffice.BL
{
	public partial class BackOfficeBL
    {
        public List<tblGacDeploy> GetGacDeploy(Int32 pDeveloperID)
        {
            try
            {
                UpdateTempMethod("GetGacDeploy", "(Int32 pDeveloperID)", true);
                recordFound = false;
                tblGacDeployCrud crud = new tblGacDeployCrud();
                crud.Where("DeveloperID", DAL.General.Operator.Equals, pDeveloperID);

                List<tblGacDeploy> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetGacDeploy", "(Int32 pDeveloperID)", false);
                throw new Exception("GetGacDeploy - " + Ex.Message);
            }
        }
        public tblGacDeploy GetGacDeploy(Int32 pDeveloperID, String pBinaryName, String pBinaryPath)
        {
            try
            {
                UpdateTempMethod("GetGacDeploy", "(Int32 pDeveloperID, String pBinaryName, String pBinaryPath)", true);
                recordFound = false;
                tblGacDeployCrud crud = new tblGacDeployCrud();
                crud.Where("DeveloperID", DAL.General.Operator.Equals, pDeveloperID);
                crud.And("BinaryName", DAL.General.Operator.Equals, pBinaryName);
                crud.And("BinaryPath", DAL.General.Operator.Equals, pBinaryPath);

                tblGacDeploy search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetGacDeploy", "(Int32 pDeveloperID, String pBinaryName, String pBinaryPath)", false);
                throw new Exception("GetGacDeploy - " + Ex.Message);
            }
        }
        public Boolean DeleteGacDeploy(Int32 pDeployID)
        {
            try
            {
                UpdateTempMethod("DeleteGacDeploy", "(Int32 pDeployID)", true);
                tblGacDeployCrud crud = new tblGacDeployCrud();
                crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);

                List<tblGacDeploy> search = crud.ReadMulti().ToList();
                foreach (tblGacDeploy del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteGacDeploy", "(Int32 pDeployID)", false);
                throw new Exception("DeleteGacDeploy - " + Ex.Message);
            }
        }
        public Int32 SaveGacDeploy(Int32 pDeployID, Int32 pDeveloperID, String pBinaryName, String pBinaryPath)
        {
            try
            {
                UpdateTempMethod("SaveGacDeploy", "(Int32 pDeployID, Int32 pDeveloperID, String pBinaryName, String pBinaryPath)", true);
                tblGacDeployCrud crud = new tblGacDeployCrud();
                tblGacDeploy workRec = new tblGacDeploy();
                if (pDeployID != 0)
                {
                    crud.Where("DeployID", DAL.General.Operator.Equals, pDeployID);
                    workRec = crud.ReadSingle();
                    workRec.DeveloperID = pDeveloperID;
                    workRec.BinaryName = pBinaryName;
                    workRec.BinaryPath = pBinaryPath;
                    crud.Update(workRec);
                    return pDeployID;
                }
                else
                {
                    workRec.DeveloperID = pDeveloperID;
                    workRec.BinaryName = pBinaryName;
                    workRec.BinaryPath = pBinaryPath;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveGacDeploy", "(Int32 pDeployID, Int32 pDeveloperID, String pBinaryName, String pBinaryPath)", false);
                throw new Exception("SaveGacDeploy - " + Ex.Message);
            }
        }
        public Int32 SaveGacDeploy(Int32 pDeveloperID, String pBinaryName, String pBinaryPath)
        {
            try
            {
                UpdateTempMethod("SaveGacDeploy", "(Int32 pDeveloperID, String pBinaryName, String pBinaryPath)", true);
				if (GetGacDeploy(pDeveloperID, pBinaryName, pBinaryPath) != null)
				{
					throw new Exception("This binary setup already exists.");
				}
				tblGacDeployCrud crud = new tblGacDeployCrud();
				tblGacDeploy deploy = new tblGacDeploy();
				deploy.BinaryName = pBinaryName;
				deploy.BinaryPath = pBinaryPath;
				deploy.DeveloperID = pDeveloperID;
				long ID;
				crud.Insert(deploy, out ID);
				return (Int32)ID;
			}
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveGacDeploy", "(Int32 pDeveloperID, String pBinaryName, String pBinaryPath)", false);
                throw new Exception("SaveGacDeploy - " + Ex.Message);
            }
        }
    }
}
