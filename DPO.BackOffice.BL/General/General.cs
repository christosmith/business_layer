﻿using System;
using System.Collections.Generic;
using System.Linq;
using DPO.BackOffice.DAL;

namespace DPO.BackOffice.BL
{
    public enum DatabaseEntities
    {
        tblussdmenuitem =0,
        tblussdmenuitemtext = 1,
        tblussdconfirmtext = 2,
        tblussdleadintext = 3,
        tblussdtagname =4,
        tblussdwelcomemessage = 5,
        tblussdhttppost = 6,
        tblussdtransaction = 7,
        tblbusiness = 8,
        tblUssdTransactionProcessInstruction = 9,
        tblUssdProcessInstruction = 10,
        tblUssdVendingMessage = 11,
        tblUssdVendingMessageType = 12,
        tblUssdTransactionVendingMessage = 13
    }

    public partial class BackOfficeBL
    {
 


        public tblDatabase GetDatabase(String pDatabaseName)
        {
            try
            {
                UpdateTempMethod("GetDatabase", "(String pDatabaseName)", true);
                recordFound = false;
                tblDatabaseCrud crud = new tblDatabaseCrud();
                crud.Where("DatabaseName", DAL.General.Operator.Equals, pDatabaseName);

                tblDatabase search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDatabase", "(String pDatabaseName)", false);
                throw new Exception("GetDatabase - " + Ex.Message);
            }
        }
        public List<tblDatabase> GetDatabase()
        {
            try
            {
                UpdateTempMethod("GetDatabase", "()", true);
                recordFound = false;
                tblDatabaseCrud crud = new tblDatabaseCrud();
                List<tblDatabase> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDatabase", "()", false);
                throw new Exception("GetDatabase - " + Ex.Message);
            }
        }
        public String GetDeveloperDeployerGridData()
        {
            try
            {
                UpdateTempMethod("GetDeveloperDeployerGridData", "()", true);
                tblDeveloperCrud crud = new tblDeveloperCrud();
                List<tblDeveloper> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (tblDeveloper workRec in search)
                {
                    divString += workRec.DeveloperID + "═";
                    divString += workRec.Name + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeveloperDeployerGridData", "()", false);
                throw new Exception("GetDeveloperDeployerGridData - " + Ex.Message);
            }
        }
        public List<tblDeveloper> GetDeveloperDeployer()
        {
            try
            {
                UpdateTempMethod("GetDeveloperDeployer", "()", true);
                recordFound = false;
                tblDeveloperCrud crud = new tblDeveloperCrud();
                List<tblDeveloper> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeveloperDeployer", "()", false);
                throw new Exception("GetDeveloperDeployer - " + Ex.Message);
            }
        }
        public tblDeveloper GetDeveloper(Int32 pDeveloperID)
        {
            try
            {
                UpdateTempMethod("GetDeveloper", "(Int32 pDeveloperID)", true);
                recordFound = false;
                tblDeveloperCrud crud = new tblDeveloperCrud();
                crud.Where("DeveloperID", DAL.General.Operator.Equals, pDeveloperID);

                tblDeveloper search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeveloper", "(Int32 pDeveloperID)", false);
                throw new Exception("GetDeveloper - " + Ex.Message);
            }
        }
        public tblDeveloper GetDeveloperByName(String pName)
        {
            try
            {
                UpdateTempMethod("GetDeveloperByName", "(String pName)", true);
                recordFound = false;
                tblDeveloperCrud crud = new tblDeveloperCrud();
                crud.Where("Name", DAL.General.Operator.Equals, pName);

                tblDeveloper search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeveloperByName", "(String pName)", false);
                throw new Exception("GetDeveloperByName - " + Ex.Message);
            }
        }
        public String GetDeveloperGridData()
        {
            try
            {
                UpdateTempMethod("GetDeveloperGridData", "()", true);
                tblDeveloperCrud crud = new tblDeveloperCrud();
                List<tblDeveloper> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (tblDeveloper workRec in search)
                {
                    divString += workRec.DeveloperID + "═";
                    divString += workRec.Name + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeveloperGridData", "()", false);
                throw new Exception("GetDeveloperGridData - " + Ex.Message);
            }
        }
        public List<tblDeveloper> GetDeveloper()
        {
            try
            {
                UpdateTempMethod("GetDeveloper", "()", true);
                recordFound = false;
                tblDeveloperCrud crud = new tblDeveloperCrud();
                List<tblDeveloper> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDeveloper", "()", false);
                throw new Exception("GetDeveloper - " + Ex.Message);
            }
        }
    }
}
