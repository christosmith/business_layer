﻿using System;
using System.Collections.Generic;
using System.Linq;
using DPO.BackOffice.DAL;

namespace DPO.BackOffice.BL
{
	public partial class BackOfficeBL
    {
		public Boolean DeleteIISLog(Int32 pFileID)
        {
            try
            {
                UpdateTempMethod("DeleteIISLog", "(Int32 pFileID)", true);
                tblIISLogCrud crud = new tblIISLogCrud();
                crud.Where("FileID", DAL.General.Operator.Equals, pFileID);

                List<tblIISLog> search = crud.ReadMulti().ToList();
                foreach (tblIISLog del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteIISLog", "(Int32 pFileID)", false);
                throw new Exception("DeleteIISLog - " + Ex.Message);
            }
        }
		public tblIISLogFile GetIISLogFile(Byte pSystemID, String pLogFileName)
        {
            try
            {
                UpdateTempMethod("GetIISLogFile", "(Byte pSystemID, String pLogFileName)", true);
                recordFound = false;
                tblIISLogFileCrud crud = new tblIISLogFileCrud();
                crud.Where("SystemID", DAL.General.Operator.Equals, pSystemID);
                crud.And("LogFileName", DAL.General.Operator.Equals, pLogFileName);

                tblIISLogFile search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetIISLogFile", "(Byte pSystemID, String pLogFileName)", false);
                throw new Exception("GetIISLogFileByte - " + Ex.Message);
            }
        }
		public Int32 InsertIISLogFile(Byte pSystemID, String pLogFileName)
        {
            try
            {
                UpdateTempMethod("InsertIISLogFile", "(Byte pSystemID, String pLogFileName)", true);
                tblIISLogFileCrud crud = new tblIISLogFileCrud();
                tblIISLogFile workRec = new tblIISLogFile();
                if (pSystemID != 0)
                {
                    crud.Where("SystemID", DAL.General.Operator.Equals, pSystemID);
                    workRec = crud.ReadSingle();
                    workRec.LogFileName = pLogFileName;
                    crud.Update(workRec);
                    return pSystemID;
                }
                else
                {
                    workRec.LogFileName = pLogFileName;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("InsertIISLogFile", "(Byte pSystemID, String pLogFileName)", false);
                throw new Exception("InsertIISLogFileByte - " + Ex.Message);
            }
        }
		public Boolean UpdateIISLogFolder(String pFolderName, String pFileName)
        {
            try
            {
                UpdateTempMethod("UpdateIISLogFolder", "(String pFolderName, String pFileName)", true);
                tblIISLogFolderCrud crud = new tblIISLogFolderCrud();
                crud.Where("FolderName", DAL.General.Operator.Equals, pFolderName);
				tblIISLogFolder results = crud.ReadSingle();
				if (crud.RecordFound)
				{
					results.LastCollected = DateTime.Now;
					results.LastFileName = pFileName;
					crud.Update(results);

				}

				return true;

	        }
            catch (Exception Ex)
            {
                UpdateTempMethod("UpdateIISLogFolder", "(String pFolderName, String pFileName)", false);
                throw new Exception("UpdateIISLogFolder - " + Ex.Message);
            }
        }
		public List<tblIISLogFolder> GetIISLogFolder()
        {
            try
            {
                UpdateTempMethod("GetIISLogFolder", "()", true);
                recordFound = false;
                tblIISLogFolderCrud crud = new tblIISLogFolderCrud();
                List<tblIISLogFolder> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetIISLogFolder", "()", false);
                throw new Exception("GetIISLogFolder - " + Ex.Message);
            }
        }
		public Int32 InsertIISLog(Byte pProtocolMethod, Int32 pResourceID, String pQuery, String pClientIPAddress, String pUserAgent, Int32 pProtocolStatus, Int32 pProtocolSubStatus, Int32 pWin32Status, Int32 pTimeTaken, Int32 pFileID)
        {
            try
            {
                UpdateTempMethod("InsertIISLog", "(Byte pProtocolMethod, Int32 pResourceID, String pQuery, String pClientIPAddress, String pUserAgent, Int32 pProtocolStatus, Int32 pProtocolSubStatus, Int32 pWin32Status, Int32 pTimeTaken, Int32 pFileID)", true);
                tblIISLogCrud crud = new tblIISLogCrud();
                tblIISLog workRec = new tblIISLog();
                if (pProtocolMethod != 0)
                {
                    crud.Where("ProtocolMethod", DAL.General.Operator.Equals, pProtocolMethod);
                    workRec = crud.ReadSingle();
                    workRec.ResourceID = pResourceID;
                    workRec.Query = pQuery;
                    workRec.ClientIPAddress = pClientIPAddress;
                    workRec.UserAgent = pUserAgent;
                    workRec.ProtocolStatus = pProtocolStatus;
                    workRec.ProtocolSubStatus = pProtocolSubStatus;
                    workRec.Win32Status = pWin32Status;
                    workRec.TimeTaken = pTimeTaken;
                    workRec.FileID = pFileID;
                    crud.Update(workRec);
                    return pProtocolMethod;
                }
                else
                {
                    workRec.ResourceID = pResourceID;
                    workRec.Query = pQuery;
                    workRec.ClientIPAddress = pClientIPAddress;
                    workRec.UserAgent = pUserAgent;
                    workRec.ProtocolStatus = pProtocolStatus;
                    workRec.ProtocolSubStatus = pProtocolSubStatus;
                    workRec.Win32Status = pWin32Status;
                    workRec.TimeTaken = pTimeTaken;
                    workRec.FileID = pFileID;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("InsertIISLog", "(Byte pProtocolMethod, Int32 pResourceID, String pQuery, String pClientIPAddress, String pUserAgent, Int32 pProtocolStatus, Int32 pProtocolSubStatus, Int32 pWin32Status, Int32 pTimeTaken, Int32 pFileID)", false);
                throw new Exception("InsertIISLogByte - " + Ex.Message);
            }
        }
		public Int32 InsertIISLogResource(String pResource, Byte pSystemID)
        {
            try
            {
                UpdateTempMethod("InsertIISLogResource", "(String pResource, Byte pSystemID)", true);
                tblIISLogResourceCrud crud = new tblIISLogResourceCrud();
                tblIISLogResource workRec = new tblIISLogResource();
				workRec.IISResource = pResource;
				workRec.SystemID = pSystemID;
				long ID = 0;
				crud.Insert(workRec, out ID);
				return (Int32)ID;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("InsertIISLogResource", "(String pResource, Byte pSystemID)", false);
                throw new Exception("InsertIISLogResource - " + Ex.Message);
            }
        }
		public tblIISLogResource GetIISLogResource(String pIISResource)
        {
            try
            {
                UpdateTempMethod("GetIISLogResource", "(String pIISResource)", true);
                recordFound = false;
                tblIISLogResourceCrud crud = new tblIISLogResourceCrud();
                crud.Where("IISResource", DAL.General.Operator.Equals, pIISResource);

                tblIISLogResource search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetIISLogResource", "(String pIISResource)", false);
                throw new Exception("GetIISLogResource - " + Ex.Message);
            }
        }
    }
}
