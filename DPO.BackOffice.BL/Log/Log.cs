﻿using System;
using System.Collections.Generic;
using System.Linq;
using DPO.BackOffice.DAL;

namespace DPO.BackOffice.BL
{
	public partial class BackOfficeBL
    {
        public List<spGetLog> GetLog(Int32 pApplicationID, String pIntervalID, DateTime pStartDate, DateTime pEndDate, String pStartTime, String pEndTime, bool pInformation, bool pWarning, bool pError, bool pDebug, String pLastID, Int32 pSubApplicationID, String pSearchPhrase)
        {
            try
            {
                UpdateTempMethod("GetLog", "(Int32 pApplicationID, String pIntervalID, DateTime pStartDate, DateTime pEndDate, String pStartTime, String pEndTime, bool pInformation, bool pWarning, bool pError, bool pDebug, String pLastID, Int32 pSubApplicationID, String pSearchPhrase)", true);
                //return StoredProcedure.spGetLog(pApplicationID, pIntervalID, pStartDate, pEndDate, pStartTime, pEndTime, pInformation, pWarning, pError, pDebug, pLastID, pSubApplicationID, pSearchPhrase);
                return StoredProcedure.spGetLog(pApplicationID, pIntervalID, pStartDate, pEndDate, pStartTime, pEndTime, pSearchPhrase, pInformation, pWarning, pError, pDebug, pLastID, pSubApplicationID);

            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetLog", "(Int32 pApplicationID, String pIntervalID, DateTime pStartDate, DateTime pEndDate, String pStartTime, String pEndTime, bool pInformation, bool pWarning, bool pError, bool pDebug, String pLastID, Int32 pSubApplicationID, String pSearchPhrase)", false);
                throw Ex;
            }
        }
		public String GetLogGridData(Int32 pApplicationID, String pIntervalID, DateTime pStartDate, DateTime pEndDate, String pStartTime, String pEndTime, bool pInformation, bool pWarning, bool pError, bool pDebug, String pLastID, Int32 pSubApplicationID, String pSearchPhrase)
        {
            try
            {
                UpdateTempMethod("GetLogGridData", "(Int32 pApplicationID, String pIntervalID, DateTime pStartDate, DateTime pEndDate, String pStartTime, String pEndTime, bool pInformation, bool pWarning, bool pError, bool pDebug, String pLastID, Int32 pSubApplicationID, String pSearchPhrase)", true);
                //List<spGetLog> logs = StoredProcedure.spGetLog(pApplicationID, pIntervalID, pStartDate, pEndDate, pStartTime, pEndTime, pInformation, pWarning, pError, pDebug, pLastID, pSubApplicationID, pSearchPhrase);
                List<spGetLog> logs = StoredProcedure.spGetLog(pApplicationID, pIntervalID, pStartDate, pEndDate, pStartTime, pEndTime, pSearchPhrase, pInformation, pWarning, pError, pDebug, pLastID, pSubApplicationID);
                String divString = "";
                Int32 recCounter = 0;
                foreach (spGetLog log in logs)
                {
                    divString += log.LogID + "═";
                    divString += log.ApplicationID + "═";
                    divString += log.LogTypeID + "═";
                    divString += log.LogType + "═";
                    divString += Convert.ToDateTime(log.LogDateTime).ToString("yyyy-MM-dd HH:mm:ss.fff") + "═";
                    if (log.LogMessage.Length > 100)
                        divString += log.LogMessage.Substring(0, 100) + "..." + "═";
                    else
                        divString += log.LogMessage + "═";
                    divString += log.LogMessage + "║";
                    recCounter++;
                    if (recCounter == 1000)
                        break;
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetLogGridData", "(Int32 pApplicationID, String pIntervalID, DateTime pStartDate, DateTime pEndDate, String pStartTime, String pEndTime, bool pInformation, bool pWarning, bool pError, bool pDebug, String pLastID, Int32 pSubApplicationID, String pSearchPhrase)", false);
                throw Ex;
            }
        }
		public Boolean SaveLogSetting(Int32 pApplicationID, Int32 pSubApplicationID, Boolean pInformation, Boolean pWarning, Boolean pError, Boolean pDebug)
		{
			try
			{
                UpdateTempMethod("SaveLogSetting", "(Int32 pApplicationID, Int32 pSubApplicationID, Boolean pInformation, Boolean pWarning, Boolean pError, Boolean pDebug)", true);

				tblApplicationLogTypeCrud crud = new tblApplicationLogTypeCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
				crud.And("SubApplicationID", DAL.General.Operator.Equals, pSubApplicationID);
				crud.And("LogTypeID", DAL.General.Operator.Equals, 3);
				List<tblApplicationLogType> logTypes = crud.ReadMulti().ToList();
				tblApplicationLogType logType = new tblApplicationLogType();
				if (pInformation)
				{
					if (logTypes.Count == 0)
					{
						logType.ApplicationID = pApplicationID;
						logType.SubApplicationID = pSubApplicationID;
						logType.LogTypeID = 3;
						crud.Update(logType);
					}
				}
				else
				{
					if (logTypes.Count != 0)
					{
						crud.Delete(logTypes[0]);
					}
				}


				crud = new tblApplicationLogTypeCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
				crud.And("SubApplicationID", DAL.General.Operator.Equals, pSubApplicationID);
				crud.And("LogTypeID", DAL.General.Operator.Equals, 2);
				logTypes = crud.ReadMulti().ToList();
				logType = new tblApplicationLogType();
				if (pWarning)
				{
					if (logTypes.Count == 0)
					{
						logType.ApplicationID = pApplicationID;
						logType.SubApplicationID = pSubApplicationID;
						logType.LogTypeID = 2;
						crud.Update(logType);
					}
				}
				else
				{
					if (logTypes.Count != 0)
					{
						crud.Delete(logTypes[0]);
					}
				}

				crud = new tblApplicationLogTypeCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
				crud.And("SubApplicationID", DAL.General.Operator.Equals, pSubApplicationID);
				crud.And("LogTypeID", DAL.General.Operator.Equals, 1);
				logTypes = crud.ReadMulti().ToList();
				logType = new tblApplicationLogType();
				if (pError)
				{
					if (logTypes.Count == 0)
					{
						logType.ApplicationID = pApplicationID;
						logType.SubApplicationID = pSubApplicationID;
						logType.LogTypeID = 1;
						crud.Update(logType);
					}
				}
				else
				{
					if (logTypes.Count != 0)
					{
						crud.Delete(logTypes[0]);
					}
				}

				crud = new tblApplicationLogTypeCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
				crud.And("SubApplicationID", DAL.General.Operator.Equals, pSubApplicationID);
				crud.And("LogTypeID", DAL.General.Operator.Equals, 4);
				logTypes = crud.ReadMulti().ToList();
				logType = new tblApplicationLogType();
				if (pDebug)
				{
					if (logTypes.Count == 0)
					{
						logType.ApplicationID = pApplicationID;
						logType.SubApplicationID = pSubApplicationID;
						logType.LogTypeID = 4;
						crud.Update(logType);
					}
				}
				else
				{
					if (logTypes.Count != 0)
					{
						crud.Delete(logTypes[0]);
					}
				}
				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("SaveLogSetting", "(Int32 pApplicationID, Int32 pSubApplicationID, Boolean pInformation, Boolean pWarning, Boolean pError, Boolean pDebug)", false);
				throw new Exception(errorMessage + "SaveLogSetting(Int32, Int32) - " + Ex.Message);
			}
		}
		public Boolean SaveLogSetting(Int32 pApplicationID, Boolean pInformation, Boolean pWarning, Boolean pError, Boolean pDebug)
        {
			try
			{
                UpdateTempMethod("SaveLogSetting", "(Int32 pApplicationID, Boolean pInformation, Boolean pWarning, Boolean pError, Boolean pDebug)", true);

				tblApplicationLogTypeCrud crud = new tblApplicationLogTypeCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
				crud.And("LogTypeID", DAL.General.Operator.Equals, 3);
				List<tblApplicationLogType> logTypes = crud.ReadMulti().ToList();
				tblApplicationLogType logType = new tblApplicationLogType();
				if (pInformation)
				{
					if (logTypes.Count == 0)
					{
						logType.ApplicationID = pApplicationID;
						logType.LogTypeID = 3;
						crud.Update(logType);
					}
				}
				else
				{
					if (logTypes.Count != 0)
					{
						crud.Delete(logTypes[0]);
					}
				}


				crud = new tblApplicationLogTypeCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
				crud.And("LogTypeID", DAL.General.Operator.Equals, 2);
				logTypes = crud.ReadMulti().ToList();
				logType = new tblApplicationLogType();
				if (pWarning)
				{
					if (logTypes.Count == 0)
					{
						logType.ApplicationID = pApplicationID;
						logType.LogTypeID = 2;
						crud.Update(logType);
					}
				}
				else
				{
					if (logTypes.Count != 0)
					{
						crud.Delete(logTypes[0]);
					}
				}

				crud = new tblApplicationLogTypeCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
				crud.And("LogTypeID", DAL.General.Operator.Equals, 1);
				logTypes = crud.ReadMulti().ToList();
				logType = new tblApplicationLogType();
				if (pError)
				{
					if (logTypes.Count == 0)
					{
						logType.ApplicationID = pApplicationID;
						logType.LogTypeID = 1;
						crud.Update(logType);
					}
				}
				else
				{
					if (logTypes.Count != 0)
					{
						crud.Delete(logTypes[0]);
					}
				}

				crud = new tblApplicationLogTypeCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
				crud.And("LogTypeID", DAL.General.Operator.Equals, 4);
				logTypes = crud.ReadMulti().ToList();
				logType = new tblApplicationLogType();
				if (pDebug)
				{
					if (logTypes.Count == 0)
					{
						logType.ApplicationID = pApplicationID;
						logType.LogTypeID = 4;
						crud.Update(logType);
					}
				}
				else
				{
					if (logTypes.Count != 0)
					{
						crud.Delete(logTypes[0]);
					}
				}
				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("SaveLogSetting", "(Int32 pApplicationID, Boolean pInformation, Boolean pWarning, Boolean pError, Boolean pDebug)", false);
				throw new Exception(errorMessage + "SaveLogSetting(Int32, Int32) - " + Ex.Message);
			}
		}
        public Boolean WriteLog(Int32 pApplicationID, Int32 pSubApplicationID, String pLogMessage, eLogType pLogType, String pNotifyEMailAddress)
        {
            try
            {
                UpdateTempMethod("WriteLog", "(Int32 pApplicationID, Int32 pSubApplicationID, String pLogMessage, eLogType pLogType, String pNotifyEMailAddress)", true);
                StoredProcedure.spInsertLog(pApplicationID, Convert.ToByte(pLogType), pLogMessage, pSubApplicationID, false);

                tblApplication application = GetApplication(pApplicationID);
                String system = application.tblSystem.vcsSystem;
                String applicationName = application.vcsApplication;

                String header = "<html>\n";
                header += "<body bgcolor=white>\n";
                header += "<table class='small' style=\"border-collapse:collapse\">\n";
                header += "<tr><td colspan='2' style='font-weight:bold;font-size:14pt;font-family:verdana;width:500px;border:1px solid #778899;text-align:center;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Back Office Log Message</td></tr>\n";
                header += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>System</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + system + "</td></tr>\n";
                header += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Application</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + applicationName + "</td></tr>\n";
                header += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Message</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + pLogMessage + "</td></tr>\n";

                header += "</table>\n";
                header += "</body>\n";
                header += "</html>\n";
                messBL.UseMailBodyAsIs = true;
                messBL.SendMail("Message From Logging System", header, pNotifyEMailAddress, application.vcsApplication);

                return true;

            }
            catch (Exception Ex)
            {
                UpdateTempMethod("WriteLog", "(Int32 pApplicationID, Int32 pSubApplicationID, String pLogMessage, eLogType pLogType, String pNotifyEMailAddress)", false);
                throw Ex;
            }
        }
        public Boolean WriteLog(Int32 pApplicationID, String pLogMessage, eLogType pLogType, String pNotifyEMailAddress)
        {
            try
            {
                UpdateTempMethod("WriteLog", "(Int32 pApplicationID, String pLogMessage, eLogType pLogType, String pNotifyEMailAddress)", true);
                StoredProcedure.spInsertLog(pApplicationID, Convert.ToByte(pLogType), pLogMessage, 0, false);

                tblApplication application = GetApplication(pApplicationID);
                String system = application.tblSystem.vcsSystem;
                String applicationName = application.vcsApplication;

                String header = "<html>\n";
                header += "<body bgcolor=white>\n";
                header += "<table class='small' style=\"border-collapse:collapse\">\n";
                header += "<tr><td colspan='2' style='font-weight:bold;font-size:14pt;font-family:verdana;width:500px;border:1px solid #778899;text-align:center;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Back Office Log Message</td></tr>\n";
                header += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>System</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + system + "</td></tr>\n";
                header += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Application</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + applicationName + "</td></tr>\n";
                header += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Message</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + pLogMessage + "</td></tr>\n";

                header += "</table>\n";
                header += "</body>\n";
                header += "</html>\n";
                messBL.UseMailBodyAsIs = true;
                messBL.SendMail("Message From Logging System", header, pNotifyEMailAddress, application.vcsApplication);

                return true;

            }
            catch (Exception Ex)
            {
                UpdateTempMethod("WriteLog", "(Int32 pApplicationID, String pLogMessage, eLogType pLogType, String pNotifyEMailAddress)", false);
                throw Ex;
            }
        }
        public Int32 WriteLog(Int32 pApplicationID, Int32 pSubApplicationID, String pLogMessage, eLogType pLogType)
        {
            try
            {
                UpdateTempMethod("WriteLog", "(Int32 pApplicationID, Int32 pSubApplicationID, String pLogMessage, eLogType pLogType)", true);
                List<spInsertLog> results = StoredProcedure.spInsertLog(pApplicationID, Convert.ToByte(pLogType), pLogMessage, pSubApplicationID, false);
                return Convert.ToInt32(results[0].LogID);
                //return Convert.ToInt32(backDAL.spInsertLog(pApplicationID, Convert.ToByte(pLogType), pLogMessage, pSubApplicationID, false)[0]);
                
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("WriteLog", "(Int32 pApplicationID, Int32 pSubApplicationID, String pLogMessage, eLogType pLogType)", false);
                throw new Exception(errorMessage + "WriteLog(Int, Int, String, eLogtype) - " + Ex.Message);
            }
        }
        public tblThirdPartyNotification GetThirdPartyNotification(Int32 pApplicationID, String pErrorCode)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyNotification", "(Int32 pApplicationID, String pErrorCode)", true);
                recordFound = false;
                tblThirdPartyNotificationCrud crud = new tblThirdPartyNotificationCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
                crud.And("ErrorCode", DAL.General.Operator.Equals, pErrorCode);

                tblThirdPartyNotification search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyNotification", "(Int32 pApplicationID, String pErrorCode)", false);
                throw new Exception("GetThirdPartyNotification - " + Ex.Message);
            }
        }
        public Int32 InsertThirdPartyNotificationLog(Int32 pNotificationID, String pLogMessage)
        {
            try
            {
                tblThirdPartyNotificationLogCrud crud = new tblThirdPartyNotificationLogCrud();
                tblThirdPartyNotificationLog workRec = new tblThirdPartyNotificationLog();
                workRec.LogMessage = pLogMessage;
                workRec.NotificationID = pNotificationID;
                workRec.LogDateTime = DateTime.Now;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertThirdPartyNotificationLog - " + Ex.Message);
            }
        }
		public Boolean NotifyThirdParty(Int32 pApplicationID, String pErrorCode, String pErrorMessage, Int32 pLogID)
        {
            try
            {
                tblThirdPartyNotification notification = GetThirdPartyNotification(pApplicationID, pErrorCode);
                if (notification != null)
                {
                    String mailBody = notification.MailBody;
                    String mailSubject = notification.MailSubject;
                    String recipient = notification.Recipient;
                    String system = notification.ThirdPartySystem;
                    Int32 notificationID = Convert.ToInt32(notification.NotificationID);

                    String message = "<!DOCTYPE html>\n";
                    message += "<html>\n";
                    message += "<title>\n";
                    message += "</title>\n";
                    message += "<head>\n";
                    message += "<style>\n";
                    message += "                .errors\n";
                    message += "       {\n";
                    message += "           font-family:Verdana;\n";
                    message += "           font-size:8pt;\n";
                    message += "}\n";
                    message += "</style>\n";
                    message += "</head>\n";
                    message += "<body>\n";
                    message += "<table class=\"errors\">\n";
                    message += "<tr>\n";
                    message += "<td>\n";
                    message += "Good Day,\n";
                    message += "</td>\n";
                    message += "</tr><tr><td></td></tr>\n";
                    message += mailBody;
                    message += "<tr><td>\n";
                    message += "</td></tr>\n";
                    message += "</table>\n";

                    message += "<br />\n";
                    message += "<table style='table-layout: fixed;border-collapse:collapse'>\n";
                    message += "<tr>\n";
                    message += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>\n";
                    message += "Log ID\n";
                    message += "</th>\n";
                    message += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>\n";
                    message += "Date & Time\n";
                    message += "</th>\n";
                    message += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>\n";
                    message += "System\n";
                    message += "</th>\n";
                    message += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>\n";
                    message += "Message\n";
                    message += "</th>\n";
                    message += "</tr>\n";
                    message += "<tr>\n";
                    message += "<td style='vertical-align:top;word-break:break-all;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + pLogID.ToString() + "</td>\n";
                    message += "<td style='vertical-align:top;word-break:break-all;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "</td>\n";
                    message += "<td style='vertical-align:top;word-break:break-all;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + system + "</td>\n";
                    message += "<td style='text-align:left;vertical-align:top;word-break:break-all;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + pErrorMessage + "</td>\n";
                    message += "</tr>";
                    message += "</table>";
                    message += "<br />\n";
                    message += "<table class=\"errors\">\n";
                    message += "<tr><td>Virtual Card Services (Pty) Ltd.</td></tr>\n";
                    message += "<tr><td>Phone: (011) 802-4233</td></tr>\n";
                    message += "<tr><td>Fax: (011) 802-4233</td></tr>\n";
                    message += "<tr><td>Email: <a href=mailto:support@vcs.co.za>support@vcs.co.za</a></td></tr>\n";
                    message += "<tr><td>Web: <a href=http//:www.vcs.co.za>http://www.vcs.co.za</a></td></tr>\n";
                    message += "</table>\n";
                    message += "</body>\n";
                    message += "</html>\n";
                    messBL.UseMailBodyAsIs = true;
                    messBL.SendMail(mailSubject, message, recipient, "VCS.BackOffice.BL");
                    InsertThirdPartyNotificationLog(notificationID, pErrorMessage);
                }
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "NotifyThirdParty(Int, String) - " + Ex.Message );
            }
        }
        public Int32 WriteLog(Int32 pApplicationID, String pLogMessage, eLogType pLogType, Boolean pNotifyThirdParty)
        {
            try
            {
                UpdateTempMethod("WriteLog", "(Int32 pApplicationID, String pLogMessage, eLogType pLogType, Boolean pNotifyThirdParty)", true);
                List<spInsertLog> result = StoredProcedure.spInsertLog(pApplicationID, Convert.ToByte(pLogType), pLogMessage, 0, pNotifyThirdParty);
                return Convert.ToInt32(result[0].LogID);
                
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("WriteLog", "(Int32 pApplicationID, String pLogMessage, eLogType pLogType, Boolean pNotifyThirdParty)", false);
                throw Ex;
            }
        }
        public Int32 WriteLog(Int32 pApplicationID, String pLogMessage, eLogType pLogType)
        {
            try
            {
                UpdateTempMethod("WriteLog", "(Int32 pApplicationID, String pLogMessage, eLogType pLogType)", true);
                List<spInsertLog> result = StoredProcedure.spInsertLog(pApplicationID, Convert.ToByte(pLogType), pLogMessage, 0, false);
                return Convert.ToInt32(result[0].LogID);
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("WriteLog", "(Int32 pApplicationID, String pLogMessage, eLogType pLogType)", false);
                throw Ex;
            }
        }
        public tblApplicationLogType GetApplicationLogType(Int32 pApplicationID, Byte pLogTypeID)
        {
            try
            {
                UpdateTempMethod("GetApplicationLogType", "(Int32 pApplicationID, Byte pLogTypeID)", true);
                recordFound = false;
                tblApplicationLogTypeCrud crud = new tblApplicationLogTypeCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
                crud.And("LogTypeID", DAL.General.Operator.Equals, pLogTypeID);

                tblApplicationLogType search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationLogType", "(Int32 pApplicationID, Byte pLogTypeID)", false);
                throw new Exception("GetApplicationLogType - " + Ex.Message);
            }
        }
        public void GetApplicationLogType(Int32 pApplicationID, Int32 pSubApplicationID, out Boolean pInformation, out Boolean pWarning, out Boolean pError, out Boolean pDebug)
        {
            try
            {
                UpdateTempMethod("GetApplicationLogType", "(Int32 pApplicationID, Int32 pSubApplicationID, out Boolean pInformation, out Boolean pWarning, out Boolean pError, out Boolean pDebug)", true);
                pInformation = false;
                pWarning = false;
                pError = false;
                pDebug = false;

				tblApplicationLogTypeCrud crud = new tblApplicationLogTypeCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
				crud.And("SubApplicationID", DAL.General.Operator.Equals, pSubApplicationID);				
                List<tblApplicationLogType> logTypes = crud.ReadMulti().ToList();
                foreach (tblApplicationLogType logType in logTypes)
                {
                    if (logType.LogTypeID == 1)
                        pError = true;
                    if (logType.LogTypeID == 2)
                        pWarning = true;
                    if (logType.LogTypeID == 3)
                        pInformation = true;
                    if (logType.LogTypeID == 4)
                        pDebug = true;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationLogType", "(Int32 pApplicationID, Int32 pSubApplicationID, out Boolean pInformation, out Boolean pWarning, out Boolean pError, out Boolean pDebug)", false);
                throw new Exception(errorMessage + "GetApplicationLogType(Int32, Int32) - " + Ex.Message);
            }
        }
        public void GetApplicationLogType(Int32 pApplicationID, out Boolean pInformation, out Boolean pWarning, out Boolean pError, out Boolean pDebug)
        {
			pInformation = false;
			pWarning = false;
			pError = false;
			pDebug = false;

			tblApplicationLogTypeCrud crud = new tblApplicationLogTypeCrud();
			crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
			List<tblApplicationLogType> logTypes = crud.ReadMulti().ToList();

				foreach (tblApplicationLogType logType in logTypes)
			{
				if (logType.LogTypeID == 1)
					pError = true;
				if (logType.LogTypeID == 2)
					pWarning = true;
				if (logType.LogTypeID == 3)
					pInformation = true;
				if (logType.LogTypeID == 4)
					pDebug = true;
			}

		}
        public List<spGetLogError> GetLogError()
        {
            try
            {
                UpdateTempMethod("GetLogError", "()", true);
                return StoredProcedure.spGetLogError();   
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetLogError", "()", false);
                throw Ex;
            }
        }
        public List<tblApplicationErrorRecipient> GetApplicationErrorRecipient(Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("GetApplicationErrorRecipient", "(Int32 pApplicationID)", true);
                recordFound = false;
                tblApplicationErrorRecipientCrud crud = new tblApplicationErrorRecipientCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);

                List<tblApplicationErrorRecipient> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationErrorRecipient", "(Int32 pApplicationID)", false);
                throw new Exception("GetApplicationErrorRecipient - " + Ex.Message);
            }
        }
		public tblApplicationErrorRecipient GetApplicationErrorRecipientByID(Int32 pRecipientID)
        {
            try
            {
                UpdateTempMethod("GetApplicationErrorRecipientByID", "(Int32 pRecipientID)", true);
                recordFound = false;
                tblApplicationErrorRecipientCrud crud = new tblApplicationErrorRecipientCrud();
                crud.Where("RecipientID", DAL.General.Operator.Equals, pRecipientID);

                tblApplicationErrorRecipient search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationErrorRecipientByID", "(Int32 pRecipientID)", false);
                throw new Exception("GetApplicationErrorRecipientByID - " + Ex.Message);
            }
        }
		public tblApplicationErrorRecipient GetApplicationErrorRecipient(Int32 pApplicationID, Int32 pDeveloperID)
        {
            try
            {
                UpdateTempMethod("GetApplicationErrorRecipient", "(Int32 pApplicationID, Int32 pDeveloperID)", true);
                recordFound = false;
                tblApplicationErrorRecipientCrud crud = new tblApplicationErrorRecipientCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
                crud.And("DeveloperID", DAL.General.Operator.Equals, pDeveloperID);

                tblApplicationErrorRecipient search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationErrorRecipient", "(Int32 pApplicationID, Int32 pDeveloperID)", false);
                throw new Exception("GetApplicationErrorRecipient - " + Ex.Message);
            }
        }
        public List<tblApplicationErrorRecipient> GetApplicationErrorRecipient()
        {
            try
            {
                UpdateTempMethod("GetApplicationErrorRecipient", "()", true);
                recordFound = false;
                tblApplicationErrorRecipientCrud crud = new tblApplicationErrorRecipientCrud();
                List<tblApplicationErrorRecipient> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationErrorRecipient", "()", false);
                throw new Exception("GetApplicationErrorRecipient - " + Ex.Message);
            }
        }
        public List<tblDeveloper> GetApplicationErrorRecipientDeveloper(Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("GetApplicationErrorRecipientDeveloper", "(Int32 pApplicationID)", true);
                List<tblDeveloper> developers = new List<tblDeveloper>();
                List<tblApplicationErrorRecipient> recipients = GetApplicationErrorRecipient(pApplicationID);
                foreach (tblApplicationErrorRecipient recipient in recipients)
                    developers.Add(GetDeveloper(Convert.ToInt32(recipient.DeveloperID)));

                return developers;

            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationErrorRecipientDeveloper", "(Int32 pApplicationID)", false);
                throw Ex;
            }
        }
        public List<tblDeveloper> GetApplicationErrorRecipientDeveloper()
        {
            try
            {
                UpdateTempMethod("GetApplicationErrorRecipientDeveloper", "()", true);
                recordFound = false;
                tblDeveloperCrud crud = new tblDeveloperCrud();
                List<tblDeveloper> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationErrorRecipientDeveloper", "()", false);
                throw new Exception("GetApplicationErrorRecipientDeveloper - " + Ex.Message);
            }
        }
        public String GetApplicationErrorRecipientDeveloperGridData()
        {
            try
            {
                UpdateTempMethod("GetApplicationErrorRecipientDeveloperGridData", "()", true);
                List<tblApplicationErrorRecipient> recipients = GetApplicationErrorRecipient();
                String divString = "";
                foreach (tblApplicationErrorRecipient recipient in recipients)
                {
                    divString += recipient.ApplicationID + "═";
                    tblDeveloper developer = GetDeveloper(Convert.ToInt32(recipient.DeveloperID));
                    divString += developer.DeveloperID.ToString() + "═";
                    divString += developer.Name + "═";
                    divString += developer.EMail + "═";
                    divString += recipient.RecipientID.ToString() + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationErrorRecipientDeveloperGridData", "()", false);
                throw Ex;
            }
        }
        public Int32 SaveApplicationErrorRecipient(Int32 pApplicationID, Int32 pDeveloperID)
        {
            try
            {
                UpdateTempMethod("SaveApplicationErrorRecipient", "(Int32 pApplicationID, Int32 pDeveloperID)", true);
                tblApplicationErrorRecipientCrud crud = new tblApplicationErrorRecipientCrud();
                tblApplicationErrorRecipient workRec = new tblApplicationErrorRecipient();
                workRec.DeveloperID = pDeveloperID;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveApplicationErrorRecipient", "(Int32 pApplicationID, Int32 pDeveloperID)", false);
                throw new Exception("SaveApplicationErrorRecipient - " + Ex.Message);
            }
        }
        public Boolean DeleteApplicationErrorRecipient(Int32 pRecipientID)
        {
            try
            {
                UpdateTempMethod("DeleteApplicationErrorRecipient", "(Int32 pRecipientID)", true);
                tblApplicationErrorRecipientCrud crud = new tblApplicationErrorRecipientCrud();
                crud.Where("RecipientID", DAL.General.Operator.Equals, pRecipientID);

                List<tblApplicationErrorRecipient> search = crud.ReadMulti().ToList();
                foreach (tblApplicationErrorRecipient del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteApplicationErrorRecipient", "(Int32 pRecipientID)", false);
                throw new Exception("DeleteApplicationErrorRecipient - " + Ex.Message);
            }
        }
        public List<spSettingCheck> GetSettingCheck()
        {
            try
            {
                UpdateTempMethod("GetSettingCheck", "()", true);
                return StoredProcedure.spSettingCheck();
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetSettingCheck", "()", false);
                throw Ex;
            }
        }
        public vApplicationLogTypeCount GetApplicationLogTypeCount(Int32 pApplicationID, Int32 pSubApplicationID)
        {
            try
            {
                UpdateTempMethod("GetApplicationLogTypeCount", "(Int32 pApplicationID, Int32 pSubApplicationID)", true);
                recordFound = false;
                vApplicationLogTypeCountCrud crud = new vApplicationLogTypeCountCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
                crud.And("SubApplicationID", DAL.General.Operator.Equals, pSubApplicationID);

                vApplicationLogTypeCount search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationLogTypeCount", "(Int32 pApplicationID, Int32 pSubApplicationID)", false);
                throw new Exception("GetApplicationLogTypeCount - " + Ex.Message);
            }
        }
        public vApplicationLogTypeCount GetApplicationLogTypeCount(Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("GetApplicationLogTypeCount", "(Int32 pApplicationID)", true);
                recordFound = false;
                vApplicationLogTypeCountCrud crud = new vApplicationLogTypeCountCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);

                vApplicationLogTypeCount search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationLogTypeCount", "(Int32 pApplicationID)", false);
                throw new Exception("GetApplicationLogTypeCount - " + Ex.Message);
            }
        }
        public List<vApplicationLogTypeCount> GetApplicationLogTypeCount()
        {
            try
            {
                UpdateTempMethod("GetApplicationLogTypeCount", "()", true);
                recordFound = false;
                vApplicationLogTypeCountCrud crud = new vApplicationLogTypeCountCrud();
                List<vApplicationLogTypeCount> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetApplicationLogTypeCount", "()", false);
                throw new Exception("GetApplicationLogTypeCount - " + Ex.Message);
            }
        }
        public Boolean DeleteLog(Int32 pApplicationID, Int32 pSubApplicationID, Int32 pLogTypeID)
        {
            try
            {
                UpdateTempMethod("DeleteLog", "(Int32 pApplicationID, Int32 pSubApplicationID, Int32 pLogTypeID)", true);
                tblLogCrud crud = new tblLogCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
                crud.And("SubApplicationID", DAL.General.Operator.Equals, pSubApplicationID);
                crud.And("LogTypeID", DAL.General.Operator.Equals, pLogTypeID);

                List<tblLog> search = crud.ReadMulti().ToList();
                foreach (tblLog del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteLog", "(Int32 pApplicationID, Int32 pSubApplicationID, Int32 pLogTypeID)", false);
                throw new Exception("DeleteLog - " + Ex.Message);
            }
        }
        public Boolean DeleteLog(Int32 pApplicationID, Int32 pLogTypeID)
        {
            try
            {
                UpdateTempMethod("DeleteLog", "(Int32 pApplicationID, Int32 pLogTypeID)", true);
                tblLogCrud crud = new tblLogCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
                crud.And("LogTypeID", DAL.General.Operator.Equals, pLogTypeID);

                List<tblLog> search = crud.ReadMulti().ToList();
                foreach (tblLog del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteLog", "(Int32 pApplicationID, Int32 pLogTypeID)", false);
                throw new Exception("DeleteLog - " + Ex.Message);
            }
        }
    }
}
