﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.DAL;

namespace DPO.BackOffice.BL
{
    public partial class BackOfficeBL
    {
        public List<tblProcessMonitor> GetProcessMonitor()
        {
            try
            {
                tblProcessMonitorCrud crud = new tblProcessMonitorCrud();
                crud.Where("Active", DAL.General.Operator.Equals, 1);
                recordFound = false;
                List<tblProcessMonitor> results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return results;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetProcessMonitor - " + Ex.Message);
            }
        }

        public List<tblProcessMonitorCriteria> GetProcessMonitorCriteria(Byte pMonitorID)
        {
            try
            {
                tblProcessMonitorCriteriaCrud crud = new tblProcessMonitorCriteriaCrud();
                crud.Where("MonitorID", DAL.General.Operator.Equals, pMonitorID);
                recordFound = false;
                List<tblProcessMonitorCriteria> criteria = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return criteria;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetProcessMonitorCriteria - " + Ex.Message);
            }
        }

        public List<tblProcessMonitorAlert> GetProcessMonitorAlert(Byte pMonitorID)
        {
            try
            {
                tblProcessMonitorAlertCrud crud = new tblProcessMonitorAlertCrud();
                crud.Where("MonitorID", DAL.General.Operator.Equals, pMonitorID);
                recordFound = false;
                List<tblProcessMonitorAlert> alerts = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return alerts;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetProcessMonitorAlert - " + Ex.Message);
            }
        }
    }
}
