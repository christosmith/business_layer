﻿using System;
using System.Collections.Generic;
using System.Linq;
using DPO.BackOffice.DAL;
namespace DPO.BackOffice.BL
{
	public partial class BackOfficeBL
	{
        public String GetReportDataGridData()
        {
            try
            {
                List<tblReportData> reportDatas = GetReportData();
                String divString = "";
                foreach (tblReportData data in reportDatas)
                {
                    divString += data.ReportDataID.ToString() + "═";
                    divString += data.ReportID.ToString() + "═";
                    divString += data.DataSetID.ToString() + "═";
                    divString += data.tblReportDataSet.DataSetName + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetReportDataGridData - " + Ex.Message);
            }

        }

        public Int32 InsertReportData(Int32 pReportID, Int32 pDatasetID)
        {
            try
            {
                tblReportData data = new tblReportData();
                data.ReportID = pReportID;
                data.DataSetID = pDatasetID;
                tblReportDataCrud crud = new tblReportDataCrud();
                long ID = 0;
                crud.Insert(data, out ID);
                return (Int32)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "InsertReportData - " + Ex.Message);
            }
        }
        public  Int32 InsertReportDataset(Byte pDataSourceTypeID, Byte pSystemID, String pDatasetName, String pDataSource)
        {
            try
            {
                tblReportDataSet dataset = new tblReportDataSet();
                dataset.DataSetName = pDatasetName;
                dataset.DataSource = pDataSource;
                dataset.DataSourceTypeID = pDataSourceTypeID;
                dataset.SystemID = pSystemID;
                tblReportDataSetCrud crud = new tblReportDataSetCrud();
                long ID = 0;
                crud.Insert(dataset, out ID);

                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "InsertReportDataset - " + Ex.Message);
            }
        }

        public Int32 InsertReportParameter(Int32 pReportID, Int32 pDatasetID, Byte pDataSourceTypeID, String pParameterName, String pParameterValue)
        {
            try
            {
                tblReportParameter par = new tblReportParameter();
                tblReportParameterCrud crud = new tblReportParameterCrud();
                par.DataSetID = pDatasetID;
                par.DataSourceTypeID = pDataSourceTypeID;
                par.ParameterName = pParameterName;
                par.ParameterValue = pParameterValue;
                par.ReportID = pReportID;
                long ID = 0;
                crud.Insert(par, out ID);

                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "InsertReportParameter - " + Ex.Message);
            }
        }
        public Int32 InsertReport(Byte pSystemID, String pReportName, String pMailSubject, String pRecipient, String pReportFolder)
        {
            try
            {
                tblReport report = new tblReport();
                report.SystemID = pSystemID;
                report.ReportName = pReportName;
                report.Active = true;
                report.MailRecipient = pRecipient;
                report.MailSubject = pMailSubject;
                report.ReportFolder = pReportFolder;
                long ID = 0;
                tblReportCrud crud = new tblReportCrud();
                crud.Insert(report, out ID);

                return (Int32)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "InsertReport - " + Ex.Message);
            }

        }
		public Int32 InsertReportQueue(Byte pFormatID, Int32 pReportID)
        {
            try
            {
                tblReportQueueCrud crud = new tblReportQueueCrud();
                tblReportQueue workRec = new tblReportQueue();
				workRec.FormatID = pFormatID;
				workRec.ReportID = pReportID;
				long ID = 0;
				crud.Insert(workRec, out ID);

				return (Int32)ID;



            }
            catch (Exception Ex)
            {
                throw new Exception("InsertReportQueue - " + Ex.Message);
            }
        }

		public List<tblReportData> GetReportDataByDataSetID(Int32 pDataSetID)
		{
			try
			{
                tblReportDataCrud crud = new tblReportDataCrud();
                crud.Where("DataSetID", DAL.General.Operator.Equals, pDataSetID);
                recordFound = false;
                List<tblReportData> data = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
				return data;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetReportDataByDataSetID - " + Ex.Message);
			}
		}

		public List<tblReportParameter> GetReportParameter(Int32 pReportID, Byte pDataSetID)
		{
			try
			{
				tblReportParameterCrud crud = new tblReportParameterCrud();
				crud.Where("ReportID", DAL.General.Operator.Equals, pReportID);
                crud.And("DataSetID", DAL.General.Operator.Equals, pDataSetID);
				recordFound = false;
				List<tblReportParameter> results = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound ;
				return results;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetReportParameter -" + Ex.Message);
			}
		}
		public List<tblReportParameterQueue> GetReportParameterQueue(Int32 pQueueID)
		{
			try
			{
				tblReportParameterQueueCrud crud = new tblReportParameterQueueCrud();
				crud.Where("QueueID", DAL.General.Operator.Equals, pQueueID);
				recordFound = false;
				List<tblReportParameterQueue> results = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return results;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetReportParameterQueue - " + Ex.Message);
			}

		}

        public List<tblReportParameter> GetReportParameter()
        {
            try
            {
                tblReportParameterCrud crud = new tblReportParameterCrud();
                return crud.ReadMulti().ToList();
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetReportParameter - " + Ex.Message);
            }
        }
        public String GetReportParameterGridData()
        {
            try
            {
                List<tblReportParameter> pars = GetReportParameter();
                String divString = "";
                foreach (tblReportParameter par in pars)
                {
                    divString += par.ParameterID.ToString() + "═";
                    divString += par.ReportID.ToString() + "═";
                    divString += par.DataSetID.ToString() + "═";
                    divString += par.tblReportDataSet.DataSetName + "═";
                    divString += par.ParameterName + "═";
                    divString += par.tblReportDataSourceType.DataSourceType + "═";
                    divString += par.ParameterValue + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetReportParameterGridData - " + Ex.Message);
            }
        }

        public tblReportParameter GetReportParameter(Int32 pParameterID)
        {
            try
            {
                tblReportParameterCrud crud = new tblReportParameterCrud();
                crud.Where("ParameterID", DAL.General.Operator.Equals, pParameterID);
                recordFound = false;
                tblReportParameter par = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return par;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetReportParameter - " + Ex.Message);
            }
        }
        public Boolean DeleteReportParameter(Int32 pParameterID)
        {
            try
            {
                tblReportParameter par = GetReportParameter(pParameterID);
                tblReportParameterCrud crud = new tblReportParameterCrud();
                crud.Delete(par);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "DeleteReportParameter - " + Ex.Message);
            }
        }
        public Boolean DeleteReportData(Int32 pReportDataID)
        {
            try
            {
                tblReportData reportData = GetReportDataByID(pReportDataID);
                tblReportDataCrud crud = new tblReportDataCrud();
                crud.Delete(reportData);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "DeleteReportData - " + Ex.Message);
            }
        }
        public Boolean DeleteReportDataset(Int32 pDatasetID)
        {
            try
            {
                tblReportDataSet dataset = GetReportDataset(pDatasetID);
                tblReportDataSetCrud crud = new tblReportDataSetCrud();
                crud.Delete(dataset);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "DeleteReportDataset - " + Ex.Message);
            }
        }
        public List<tblReportDataSourceType> GetReportDataSourceType()
        {
            try
            {
                tblReportDataSourceTypeCrud crud = new tblReportDataSourceTypeCrud();
                return crud.ReadMulti().ToList();
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetReportDataSourceType - " + Ex.Message);
            }
        }

        public String GetReportDatasetGridData()
        {
            try
            {
                List<tblReportDataSet> sets = GetReportDataset();
                sets = sets.OrderBy(x => x.DataSetName).ToList();
                String divString = "";
                foreach(tblReportDataSet set in sets)
                {
                    divString += set.DataSetID.ToString() + "═";
                    divString += set.DataSetName + "═";
                    divString += set.tblSystem.vcsSystem + "═";
                    divString += set.tblReportDataSourceType.DataSourceType + "═";
                    divString += set.DataSource + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetReportDatasetGridData - " + Ex.Message);
            }
        }
        public tblReportDataSet GetReportDataset(Int32 pDatasetID)
        {
            try
            {
                tblReportDataSetCrud crud = new tblReportDataSetCrud();
                crud.Where("DataSetID", DAL.General.Operator.Equals, pDatasetID);
                recordFound = false;
                tblReportDataSet set = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return set;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetReportDataset - " + Ex.Message);
            }
        }

        public List<tblReportDataSet> GetReportDataset()
        {
            try
            {
                tblReportDataSetCrud crud = new tblReportDataSetCrud();
                return crud.ReadMulti().ToList();
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetReportDataset - " + Ex.Message);
            }
        }
        public tblReportData GetReportDataByID(Int32 pReportDataID)
        {
            try
            {
                recordFound = false;
                tblReportDataCrud crud = new tblReportDataCrud();
                crud.Where("ReportDataID", DAL.General.Operator.Equals, pReportDataID);
                tblReportData search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetReportData - " + Ex.Message);
            }
        }

        public List<tblReportData> GetReportData()
        {
            try
            {
                recordFound = false;
                tblReportDataCrud crud = new tblReportDataCrud();
                List<tblReportData> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetReportData - " + Ex.Message);
            }
        }

        public List<tblReportData> GetReportData(Int32 pReportID)
        {
            try
            {
                recordFound = false;
                tblReportDataCrud crud = new tblReportDataCrud();
                crud.Where("ReportID", DAL.General.Operator.Equals, pReportID);

                List<tblReportData> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetReportData - " + Ex.Message);
            }
        }

		public List<tblReportDataSet> GetReportDataSetBySystemID(Byte pSystemID)
		{
			try
			{
				tblReportDataSetCrud crud = new tblReportDataSetCrud();
				crud.Where("SystemID", DAL.General.Operator.Equals, pSystemID);
				recordFound = false;
				List<tblReportDataSet> results = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;

				return results;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetReportDataSetBySystemID -" + Ex.Message);
			}
		}
		public List<tblReport> GetReportBySystemID()
		{
			try
			{
				recordFound = false;
				tblReportCrud crud = new tblReportCrud();
				List<tblReport> search = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetReport - " + Ex.Message);
			}
		}

        public String GetReportGridData()
        {
            try
            {
                List<tblReport> reports = GetReport();
                reports = reports.OrderBy(x => x.ReportName).ToList();
                String divData = "";
                foreach(tblReport report in reports)
                {
                    divData += report.ReportID + "═";
                    divData += report.SystemID + "═";
                    divData += report.ReportName + "║";

                }

                if (divData.Length > 0)
                    divData = divData.Substring(0, divData.Length - 1);

                return divData;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetReport - " + Ex.Message);
            }
        }



        public List<tblReport> GetReport()
        {
            try
            {
                UpdateTempMethod("GetReport", "()", true);
                recordFound = false;
                tblReportCrud crud = new tblReportCrud();
                List<tblReport> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetReport", "()", false);
                throw new Exception("GetReport - " + Ex.Message);
            }
        }
		public tblReport GetReport(Int32 pReportID)
        {
            try
            {
                UpdateTempMethod("GetReport", "(Int32 pReportID)", true);
                recordFound = false;
                tblReportCrud crud = new tblReportCrud();
                crud.Where("ReportID", DAL.General.Operator.Equals, pReportID);

                tblReport search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetReport", "(Int32 pReportID)", false);
                throw new Exception("GetReport - " + Ex.Message);
            }
        }
		public Boolean DeleteReportQueue(Int32 pQueueID)
        {
            try
            {
                UpdateTempMethod("DeleteReportQueue", "(Int32 pQueueID)", true);
                tblReportQueueCrud crud = new tblReportQueueCrud();
                crud.Where("QueueID", DAL.General.Operator.Equals, pQueueID);

                List<tblReportQueue> search = crud.ReadMulti().ToList();
                foreach (tblReportQueue del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteReportQueue", "(Int32 pQueueID)", false);
                throw new Exception("DeleteReportQueue - " + Ex.Message);
            }
        }

        public List<tblReportFormat> GetReportFormat()
        {
            try
            {
                tblReportFormatCrud crud = new tblReportFormatCrud();
                return crud.ReadMulti().ToList();
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetReportFormat - " + Ex.Message);
            }
        }
		public List<tblReportQueue> GetReportQueue()
        {
            try
            {
                UpdateTempMethod("GetReportQueue", "()", true);
                recordFound = false;
                tblReportQueueCrud crud = new tblReportQueueCrud();
                List<tblReportQueue> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetReportQueue", "()", false);
                throw new Exception("GetReportQueue - " + Ex.Message);
            }
        }
    }
}
