﻿using System;
using System.Collections.Generic;
using System.Linq;
using DPO.BackOffice.DAL;

namespace DPO.BackOffice.BL
{
	public partial class BackOfficeBL
	{
        public Int32 InsertScheduleOnceOff(String pApplicationName, String pServiceName)
        {
            try
            {
                Int32 scheduleID = InsertSchedule(pApplicationName, false);
                Int32 scheduleTaskID = InsertScheduleTask(scheduleID, 1, 1);
                Int32 applicationID = (Int32)GetApplicationByName(pApplicationName).ApplicationID;
                InsertSceduleTaskApplication(scheduleTaskID, applicationID, pServiceName);
                Byte intervalID = (Byte)GetScheduleInterval("Once Off").IntervalID;
                InsertScheduleRun(scheduleID, intervalID, "", DateTime.Now, DateTime.Now);

                return scheduleID;
                
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertScheduleOnceOff - " + Ex.Message);
            }
        }

        public tblScheduleInterval GetScheduleInterval(String pInterval)
        {
            try
            {
                tblScheduleIntervalCrud crud = new tblScheduleIntervalCrud();
                crud.Where("Interval", DAL.General.Operator.Equals, pInterval);
                recordFound = false;
                tblScheduleInterval interval = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return interval;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetScheduleInterval - " + Ex.Message);
            }
        }

        public tblScheduleInterval GetScheduleInterval(Byte pIntervalID)
        {
            try
            {
                tblScheduleIntervalCrud crud = new tblScheduleIntervalCrud();
                crud.Where("IntervalID", DAL.General.Operator.Equals, pIntervalID);
                recordFound = false;
                tblScheduleInterval interval = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return interval;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetScheduleInterval - " + Ex.Message);
            }
        }
        public Int32 InsertScheduleRun(Int32 pScheduleID, Byte pIntervalID, String pIntervalValue, DateTime pStartDateTime, DateTime pNextRunTime)
        {
            try
            {
                tblScheduleRunCrud crud = new tblScheduleRunCrud();
                tblScheduleRun run = new tblScheduleRun();
                run.IntervalID = pIntervalID;
                run.IntervalValue = pIntervalValue;
                run.NextRunTime = pNextRunTime;
                run.ScheduleID = pScheduleID;
                run.StartDateTime = pStartDateTime;
                long ID = 0;
                crud.Insert(run, out ID);

                return (Int32)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertScheduleRun - " + Ex.Message);
            }
        }
        public Int32 InsertSceduleTaskApplication(Int32 pScheduleTaskID, Int32 pApplicationID, String pParameter)
        {
            try
            {
                tblScheduleTaskApplicationCrud crud = new tblScheduleTaskApplicationCrud();
                tblScheduleTaskApplication app = new tblScheduleTaskApplication();
                app.ApplicationID = pApplicationID;
                app.Parameter = pParameter;
                app.ScheduleTaskID = pScheduleTaskID;
                long ID = 0;
                crud.Insert(app, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertSceduleTaskApplication - " + Ex.Message);
            }
        }
        public Int32 InsertScheduleTask(Int32 pScheduleID, Byte pSchedulTaskTypeID, Byte pOrdinal)
        {
            try
            {
                tblScheduleTaskCrud crud = new tblScheduleTaskCrud();
                tblScheduleTask task = new tblScheduleTask();
                task.Ordinal = pOrdinal;
                task.ScheduleID = pScheduleID;
                task.ScheduleTaskTypeID = pSchedulTaskTypeID;
                long ID = 0;
                crud.Insert(task, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertScheduleTask - " + Ex.Message);
            }
        }
        public Int32 InsertSchedule(String pScheduleDescription, Boolean pSuspend)
        {
            try
            {
                tblScheduleCrud crud = new tblScheduleCrud();
                tblSchedule shed = new tblSchedule();
                shed.ScheduleDescription = pScheduleDescription;
                shed.Suspended = pSuspend;
                long ID = 0;
                crud.Insert(shed, out ID);

                return (Int32)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("ÏnsertSchedule - " + Ex.Message);
            }
        }
		public List<vSchedule> GetScheduleDue()
        {
            try
            {
                recordFound = false;
                vScheduleCrud crud = new vScheduleCrud();
                List<vSchedule> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetScheduleDue - " + Ex.Message);
            }
        }
		public List<tblScheduleRun> GetScheduleRun(Int32 pScheduleTaskID)
        {
            try
            {
                UpdateTempMethod("GetScheduleRun", "(Int32 pScheduleTaskID)", true);
                recordFound = false;
                tblScheduleRunCrud crud = new tblScheduleRunCrud();
                crud.Where("ScheduleTaskID", DAL.General.Operator.Equals, pScheduleTaskID);

                List<tblScheduleRun> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetScheduleRun", "(Int32 pScheduleTaskID)", false);
                throw new Exception("GetScheduleRun - " + Ex.Message);
            }
        }
		public List<tblScheduleTask> GetScheduleTask(Int32 pScheduleID)
        {
            try
            {
                recordFound = false;
                tblScheduleTaskCrud crud = new tblScheduleTaskCrud();
                crud.Where("ScheduleID", DAL.General.Operator.Equals, pScheduleID);

                List<tblScheduleTask> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetScheduleTask - " + Ex.Message);
            }
        }
		public tblScheduleRun GetScheduleRunPerID(Int32 pScheduleID)
        {
            try
            {
                recordFound = false;
                tblScheduleRunCrud crud = new tblScheduleRunCrud();
                crud.Where("ScheduleID", DAL.General.Operator.Equals, pScheduleID);

                tblScheduleRun search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetScheduleRunPerID - " + Ex.Message);
            }
        }
		public Boolean SetNextScheduleRun(Int32 pScheduleID)
		{
			try
			{
				tblScheduleRunCrud crud = new tblScheduleRunCrud();
				tblScheduleRun run =  GetScheduleRunPerID(pScheduleID);
				DateTime startDate = Convert.ToDateTime(run.StartDateTime);
                tblScheduleInterval scheduleInterval = GetScheduleInterval((Byte)run.IntervalID);

				Int32 interval = Convert.ToInt32(run.IntervalID);
				String intervalValue = run.IntervalValue;
				DateTime nextRunDate = Convert.ToDateTime(run.NextRunTime);

                if (scheduleInterval.Interval == "Once Off")
                {
                    DeleteScheduleRecursive(pScheduleID);
                    return true;
                }

                while ((nextRunDate - DateTime.Now).Ticks < 0)
				{

					switch (interval)
					{
						case 1:
							nextRunDate = nextRunDate.AddMinutes(Convert.ToDouble(intervalValue));
							break;
						case 2:
							nextRunDate = nextRunDate.AddHours(Convert.ToDouble(intervalValue));
							break;
						case 3:
							nextRunDate = nextRunDate.AddDays(1);
							break;
						case 4:
							nextRunDate = nextRunDate.AddDays(7);
							break;
						case 5:
							nextRunDate = nextRunDate.AddMonths(1);
							break;
                        case 7:
                            Int32 month = nextRunDate.Month;
                            DateTime dateIncrease = nextRunDate;
                            while (month == dateIncrease.Month)
                                dateIncrease = dateIncrease.AddDays(1);

                            dateIncrease = dateIncrease.AddDays(-1);
                            dateIncrease = new DateTime(dateIncrease.Year, dateIncrease.Month, dateIncrease.Day, 23, 59, 59);
                            nextRunDate = dateIncrease;
                            break;
						case 8:
							nextRunDate = nextRunDate.AddMonths(3);
							break;
					}
				}

				run.NextRunTime = nextRunDate;
				crud.Update(run);


				return true; 
			}
			catch (Exception Ex)
			{
				throw new Exception(errorMessage + "SetNestScheduleRun-" + Ex.Message);
			}
		}

        public Boolean DeleteScheduleRecursive(Int32 pScheduleID)
        {
            try
            {
                tblScheduleRun scheduleRun = GetScheduleRunPerID(pScheduleID);
                DeleteScheduleHistory((Int32)scheduleRun.ScheduleRunID);
                List<tblScheduleTask> scheduleTasks = GetScheduleTask(pScheduleID);
                foreach (tblScheduleTask scheduleTask in scheduleTasks)
                    DeleteScheduleTaskApplication((Int32)scheduleTask.ScheduleTaskID);
                DeleteScheduleTask(pScheduleID);
                DeleteScheduleRun(pScheduleID);
                DeleteSchedule(pScheduleID);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteScheduleRecursive - " + Ex.Message);
            }
        }
		public Int32 InsertScheduleHistory(Int32 pScheduleRunID, Boolean pSuccess, String pTaskMessage)
		{
			try
			{
				tblScheduleHistory hist = new tblScheduleHistory();
				hist.RunDateTime = DateTime.Now;
				hist.ScheduleRunID = pScheduleRunID;
				hist.Success = pSuccess;
				hist.TaskMessage = pTaskMessage;
				tblScheduleHistoryCrud crud = new tblScheduleHistoryCrud();
				long ID = 0;
				crud.Insert(hist, out ID);

				return (Int32)ID;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertScheduleTaskHistory -" + Ex.Message);
			}

		}

        public Boolean DeleteSchedule(Int32 pScheduleID)
        {
            try
            {
                tblScheduleCrud crud = new tblScheduleCrud();
                crud.Delete(GetrSchedule(pScheduleID));
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteSchedule - " + Ex.Message);
            }
        }

        public Boolean DeleteScheduleTaskApplication(Int32 pScheduleTaskID)
        {
            try
            {
                tblScheduleTaskApplicationCrud crud = new tblScheduleTaskApplicationCrud();
                List<tblScheduleTaskApplication> apps = GetScheduleTaskApplication(pScheduleTaskID);
                foreach (tblScheduleTaskApplication app in apps)
                    crud.Delete(app);

                return true;

            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteScheduleTaskApplication - " + Ex.Message);
            }
        }

        public Boolean DeleteScheduleRun(Int32 pScheduleID)
        {
            try
            {
                tblScheduleRunCrud crud = new tblScheduleRunCrud();
                tblScheduleRun run = GetScheduleRunPerID(pScheduleID);
                crud.Delete(run);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteScheduleRun - " + Ex.Message);
            }
        }

        public Boolean DeleteScheduleTask(Int32 pScheduleID)
        {
            try
            {
                tblScheduleTaskCrud crud = new tblScheduleTaskCrud();
                List<tblScheduleTask> tasks = GetScheduleTask(pScheduleID);
                foreach (tblScheduleTask task in tasks)
                    crud.Delete(task);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteScheduleTask - " + Ex.Message);
            }
        }
        public List<tblScheduleTaskApplication> GetScheduleTaskApplication(Int32 pScheduleTaskID)
        {
            try
            {
                tblScheduleTaskApplicationCrud crud = new tblScheduleTaskApplicationCrud();
                crud.Where("ScheduleTaskID", DAL.General.Operator.Equals, pScheduleTaskID);
                recordFound = false;
                List<tblScheduleTaskApplication> apps = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return apps;
                
            }
            catch (Exception Ex)
            {
                throw new Exception("GetScheduleTaskApplication - " + Ex.Message);
            }
        }
        public Boolean DeleteScheduleHistory(Int32 pScheduleRunID)
        {
            try
            {
                List<tblScheduleHistory> histories = GetScheduleHistory(pScheduleRunID);
                tblScheduleHistoryCrud crud = new tblScheduleHistoryCrud();
                foreach (tblScheduleHistory hist in histories)
                    crud.Delete(hist);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteScheduleHistory - " + Ex.Message);
            }
        }
        public List<tblScheduleHistory> GetScheduleHistory(Int32 pScheduleRunID)
        {
            try
            {
                tblScheduleHistoryCrud crud = new tblScheduleHistoryCrud();
                crud.Where("ScheduleRunID", DAL.General.Operator.Equals, pScheduleRunID);
                recordFound = false;
                List<tblScheduleHistory> hist = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return hist;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetScheduleHistory - " + Ex.Message);
            }
        }
        public tblSchedule GetrSchedule(Int32 pScheduleID)
        {
            try
            {
                tblScheduleCrud crud = new tblScheduleCrud();
                crud.Where("ScheduleID", DAL.General.Operator.Equals, pScheduleID);
                recordFound = false;
                tblSchedule sched = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return sched;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetSchedule - " + Ex.Message);
            }
        }
		//public List<tblScheduleTaskApplication> GetScheduleTaskApplication(Int32 pScheduleTaskID)
		//{
		//	try
		//	{
		//		tblScheduleTaskApplicationCrud crud = new tblScheduleTaskApplicationCrud();
		//		recordFound = false;
		//		crud.Where("ScheduleTaskID", DAL.General.Operator.Equals, pScheduleTaskID);
		//		List<tblScheduleTaskApplication> results = crud.ReadMulti().ToList();
		//		recordFound = crud.RecordFound;
		//		return results;
		//	}
		//	catch (Exception Ex)
		//	{
		//		throw new Exception("GetScheduleTaskApplication - " + Ex.Message);
		//	}
		//}
		public List<tblScheduleTaskReport> GetScheduleTaskReport(Int32 pScheduleTaskID)
        {
            try
            {
                UpdateTempMethod("GetScheduleTaskReport", "(Int32 pScheduleTaskID)", true);
                recordFound = false;
                tblScheduleTaskReportCrud crud = new tblScheduleTaskReportCrud();
                crud.Where("ScheduleTaskID", DAL.General.Operator.Equals, pScheduleTaskID);

                List<tblScheduleTaskReport> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetScheduleTaskReport", "(Int32 pScheduleTaskID)", false);
                throw new Exception("GetScheduleTaskReport - " + Ex.Message);
            }
        }
    }
}
