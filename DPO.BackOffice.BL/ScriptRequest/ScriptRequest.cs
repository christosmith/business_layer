﻿using System;
using System.Collections.Generic;
using System.Linq;
using DPO.BackOffice.DAL;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DPO.BackOffice.BL
{
	public partial class BackOfficeBL
    {
        private Boolean SaveScriptRequest(Int32 pRequestID, Boolean pUsed)
        {
            try
            {
                UpdateTempMethod("", "", true);
				tblScriptRequestCrud crud = new tblScriptRequestCrud();
				crud.Where("RequestID", DAL.General.Operator.Equals, pRequestID);
				tblScriptRequest request = crud.ReadSingle();
                request.Used = false;
				crud.Update(request);

                return true;
            }
            catch
            {
                UpdateTempMethod("", "", false);
                return false;
            }
        }
        private Boolean SaveScriptRequest(String pUserName, String pPinNo)
        {
            try
            {
                UpdateTempMethod("", "", true);
                tblScriptRequestUser user = GetScriptRequestUserByName(pUserName);

                tblScriptRequest request = new tblScriptRequest();
                request.DateTimeRequested = DateTime.Now;
                request.MailPin = true;
                request.Pin = pPinNo;
                request.Used = false;
                request.UserID = Convert.ToByte(user.UserId);
				tblScriptRequestCrud crud = new tblScriptRequestCrud();
				crud.Update(request);

                return true;
            }
            catch
            {
                UpdateTempMethod("", "", false);
                return false;
            }
        }
        public tblScriptRequest GetScriptRequestByUserIDAndPin(Byte pUserID, String pPin)
        {
            try
            {
                UpdateTempMethod("GetScriptRequestByUserIDAndPin", "(Byte pUserID, String pPin)", true);
                recordFound = false;
                tblScriptRequestCrud crud = new tblScriptRequestCrud();
                crud.Where("UserID", DAL.General.Operator.Equals, pUserID);
                crud.And("Pin", DAL.General.Operator.Equals, pPin);

                tblScriptRequest search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetScriptRequestByUserIDAndPin", "(Byte pUserID, String pPin)", false);
                throw new Exception("GetScriptRequestByUserIDAndPinByte - " + Ex.Message);
            }
        }
        public List<String> GetDatabaseScript(eDatabases pDatabase, String pObjectType, String pObjectName)
        {
            Int32 applicationID = Convert.ToInt32(GetApplicationByName("ScriptRequestService").ApplicationID);
            try
            {
                UpdateTempMethod("GetDatabaseScript", "(eDatabases pDatabase, String pObjectType, String pObjectName)", true);
                
                List<String> scripts = new List<string>();

                SqlConnection openConn = new SqlConnection();
                switch (pDatabase)
                {
                    case eDatabases.VirtualAccount:
                        openConn.ConnectionString = ConfigurationManager.ConnectionStrings["vaccount_ScriptRequest"].ConnectionString;
                        break;
                    case eDatabases.VirtualVendor:
                        openConn.ConnectionString = ConfigurationManager.ConnectionStrings["vvonline_ScriptRequest"].ConnectionString;
                        break;
                    case eDatabases.BackOffice:
                        openConn.ConnectionString = ConfigurationManager.ConnectionStrings["backOffice_ScriptRequest"].ConnectionString;
                        break;
                    case eDatabases.VirtualMessage:
                        openConn.ConnectionString = ConfigurationManager.ConnectionStrings["vMessage_ScriptRequest"].ConnectionString;
                        break;
                }
                openConn.Open();
                String script;
                #region Script Tables
                String comd = "Select COLUMN_NAME, IS_NULLABLE, DATA_TYPE, IsNull(CHARACTER_MAXIMUM_LENGTH, 0), COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity')\n";
                comd += "  From INFORMATION_SCHEMA.COLUMNS\n";
                comd += " Where TABLE_NAME = '" + pObjectName + "'\n";
                comd += " Order by ORDINAL_POSITION";
                SqlCommand dbComd = new SqlCommand(comd, openConn);
                SqlDataReader dataReader = dbComd.ExecuteReader();

                script = "Create Table [" + pObjectName + "]\n(";
                while (dataReader.Read())
                {

                    String sColumnName = dataReader.GetValue(0).ToString();
                    String sNullable = dataReader.GetValue(1).ToString();
                    String sLength = dataReader.GetValue(3).ToString();
                    String sDataType = dataReader.GetValue(2).ToString();
                    String sNull = "Null";
                    String sIdentity = dataReader.GetValue(4).ToString();
                    if (sNullable == "NO") sNull = "Not Null";

                    if (sLength == "-1")
                        sLength = "Max";
                    String col = "[" + sColumnName + "] " + sDataType + " ";
                    if (sLength != "0")
                        col += "(" + sLength + ") ";

                    if (sIdentity == "1")
                        col += "Identity(1,1)";
                    col += sNull + ",\n";
                    script += col;
                }

                script = script.Substring(0, script.Length - 2) + ")";
                scripts.Add(script);
                //script += "\n\n";

                dataReader.Close();

                return scripts;
                #endregion
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetDatabaseScript", "(eDatabases pDatabase, String pObjectType, String pObjectName)", false);
                String message = errorMessage + "GetDatabaseScript-" + Ex.Message;
                WriteLog(applicationID, message, eLogType.Error);
                List<String> error = new List<string>();
                error.Add("Error: Unable to get script: " + Ex.Message);
                return error;
            }
        }
		public List<String> ScriptRequestGetScript(String pUserName, String pPinNo, eDatabases pDatabase)
        {
            Int32 applicationID = Convert.ToInt32(GetApplicationByName("ScriptRequestService").ApplicationID);
            String StoredProc = "";
            try
            {
                UpdateTempMethod("ScriptRequestGetScript", "(String pUserName, String pPinNo, eDatabases pDatabase)", true);
                List<String> scripts = new List<string>();

                tblScriptRequestUser user = GetScriptRequestUserByName(pUserName);
                if (user == null)
                {
                    WriteLog(applicationID, "ScriptRequestGetScript - User not found: " + pUserName, eLogType.Error);
                    scripts.Add("Error: User was not found.");
                    return scripts;
                }

                if (!Convert.ToBoolean(user.Active))
                {
                    WriteLog(applicationID, "ScriptRequestGetScript - User not active: " + pUserName, eLogType.Error);
                    scripts.Add("Error: User is marked as inactive.");
                    return scripts;
                }

                tblScriptRequest request = GetScriptRequestByUserIDAndPin(Convert.ToByte(user.UserId), pPinNo);
                if (request == null)
                {
                    WriteLog(applicationID, "ScriptRequestGetScript Request was not found: " + pPinNo, eLogType.Error);
                    scripts.Add("Error: Request was not found.");
                    return scripts;
                }

                if (Convert.ToBoolean(request.Used))
                {
                    WriteLog(applicationID, "The pin was already used.", eLogType.Error);
                    scripts.Add("This pin was already used.");
                    return scripts;
                }

                //Int32 applicationID = Convert.ToInt32(GetApplication("Back Office", "ScriptRequestService").ApplicationID);


                Int32 expire = Convert.ToInt32(GetSettingByName("PinExpireSeconds", applicationID).SettingValue);
                DateTime requestedDateTime = Convert.ToDateTime(request.DateTimeRequested);
                requestedDateTime = requestedDateTime.AddSeconds(expire);

                if (requestedDateTime < DateTime.Now)
                {
                    WriteLog(applicationID, "The pin actice time has elapsed", eLogType.Error);
                    scripts.Add("The pin active time has elapsed");
                    return scripts;
                }

                //if (Convert.ToDateTime(request.DateTimeRequested).Subtract(timeToElapse).Ticks < 0)
                //    return "The pin active time has elapsed";

                SqlConnection openConn = new SqlConnection();
                switch (pDatabase)
                {
                    case eDatabases.VirtualAccount:
                        openConn.ConnectionString = ConfigurationManager.ConnectionStrings["vaccount_ScriptRequest"].ConnectionString;
                        break;
                    case eDatabases.VirtualVendor:
                        openConn.ConnectionString = ConfigurationManager.ConnectionStrings["vvonline_ScriptRequest"].ConnectionString;
                        break;
                    case eDatabases.BackOffice:
                        openConn.ConnectionString = ConfigurationManager.ConnectionStrings["backOffice_ScriptRequest"].ConnectionString;
                        break;
                    case eDatabases.VirtualMessage:
                        openConn.ConnectionString = ConfigurationManager.ConnectionStrings["vMessage_ScriptRequest"].ConnectionString;
                        break;
                }
                openConn.Open();
                #region Script Tables

                String comd = "Select TABLE_NAME From INFORMATION_SCHEMA.TABLES\n";
                comd += "  Where TABLE_TYPE  = 'BASE TABLE'\n";
                comd += "   Order By TABLE_NAME\n";
                SqlCommand dbcomd = new SqlCommand(comd, openConn);
                SqlDataReader dataReader = dbcomd.ExecuteReader();

                List<String> tableNames = new List<string>();
                while (dataReader.Read())
                    tableNames.Add(dataReader.GetValue(0).ToString());

                dataReader.Close();

                String script = "";
                foreach (String tableName in tableNames)
                {
                    if (tableName != "dtproperties")
                    {
                        comd = "Select COLUMN_NAME, IS_NULLABLE, DATA_TYPE, IsNull(CHARACTER_MAXIMUM_LENGTH, 0), COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity')\n";
                        comd += "  From INFORMATION_SCHEMA.COLUMNS\n";
                        comd += " Where TABLE_NAME = '" + tableName + "'\n";
                        comd += " Order by ORDINAL_POSITION";
                        dbcomd.CommandText = comd;
                        dataReader = dbcomd.ExecuteReader();

                        script = "Create Table [" + tableName + "]\n(";
                        while (dataReader.Read())
                        {

                            String sColumnName = dataReader.GetValue(0).ToString();
                            String sNullable = dataReader.GetValue(1).ToString();
                            String sLength = dataReader.GetValue(3).ToString();
                            String sDataType = dataReader.GetValue(2).ToString();
                            String sNull = "Null";
                            String sIdentity = dataReader.GetValue(4).ToString();
                            if (sNullable == "NO") sNull = "Not Null";

                            if (sLength == "-1")
                                sLength = "Max";
                            String col = "[" + sColumnName + "] " + sDataType + " ";
                            if (sLength != "0")
                                col += "(" + sLength + ") ";

                            if (sIdentity == "1")
                                col += "Identity(1,1)";
                            col += sNull + ",\n";
                            script += col;
                        }

                        script = script.Substring(0, script.Length - 2) + ")";
                        scripts.Add(script);
                        //script += "\n\n";

                        dataReader.Close();
                    }
                }

                #endregion
                #region Script Views

                comd = "Select name From sysobjects\n";
                comd += " Where type = 'V'\n";
                comd += " Order By crdate ";
                dbcomd.CommandText = comd;
                dataReader = dbcomd.ExecuteReader();

                List<String> views = new List<string>();

                String viewDebug = "";
                while (dataReader.Read())
                {
                    views.Add(dataReader.GetValue(0).ToString());
                    viewDebug += dataReader.GetValue(0).ToString() + ", ";
                }
                dataReader.Close();
                viewDebug = viewDebug.Substring(0, viewDebug.Length - 2);

                String viewText = "";
                foreach (String view in views)
                {
                    comd = "sp_helptext " + view;
                    dbcomd.CommandText = comd;
                    dataReader = dbcomd.ExecuteReader();

                    viewText = "";
                    while (dataReader.Read())
                    {
                        viewText += dataReader.GetValue(0).ToString();
                    }

                    dataReader.Close();

                    scripts.Add(viewText);
                    //script += viewText + "\n\nGo\n\n";
                }
                #endregion
                #region Script Primary Keys
                comd = " Select Distinct CONSTRAINT_NAME\n";
                comd += "   From Information_schema.KEY_COLUMN_USAGE\n";
                comd += "  Where OBJECTPROPERTY(OBJECT_ID(constraint_name), 'IsPrimaryKey') = 1\n";
                comd += "    And TABLE_NAME Not In ('dtproperties')\n";
                dbcomd.CommandText = comd;
                dataReader = dbcomd.ExecuteReader();
                DataTable tblResult = new DataTable();
                List<string> primaryKeys = new List<string>();
                while (dataReader.Read())
                {
                    primaryKeys.Add(dataReader.GetValue(0).ToString());
                }
                dataReader.Close();

                foreach (String primaryKey in primaryKeys)
                {
                    comd = "Select CONSTRAINT_NAME, COLUMN_NAME, TABLE_NAME\n";
                    comd += "   From Information_schema.KEY_COLUMN_USAGE\n";
                    comd += "  Where OBJECTPROPERTY(OBJECT_ID(constraint_name), 'IsPrimaryKey') = 1\n";
                    comd += "    And CONSTRAINT_NAME = '" + primaryKey + "'";
                    dbcomd.CommandText = comd;
                    dataReader = dbcomd.ExecuteReader();
                    tblResult = new DataTable();
                    tblResult.Load(dataReader);
                    dataReader.Close();

                    String sConstraintName = "";
                    String sColumnName = "";
                    String sTableName = "";

                    for (Int32 tableLoop = 0; tableLoop < tblResult.Rows.Count; tableLoop++)
                    {
                        sConstraintName = "[" + tblResult.Rows[tableLoop][0].ToString() + "]";
                        sColumnName += "[" + tblResult.Rows[tableLoop][1].ToString() + "],";

                        sTableName = "[" + tblResult.Rows[tableLoop][2].ToString() + "]";
                    }
                    sColumnName = sColumnName.Substring(0, sColumnName.Length - 1);

                    String sPrimary = "Alter Table " + sTableName + " Add Constraint " + sConstraintName + " Primary Key (" + sColumnName + ")";
                    //script += sPrimary + "\n\n";
                    scripts.Add(sPrimary);
                }
                #endregion
                #region Script Foreign Keys
                comd = "SELECT Distinct FK_NAME = CONVERT(SYSNAME,OBJECT_NAME(F.OBJECT_ID))\n";
                comd += "FROM   SYS.ALL_OBJECTS O1,\n";
                comd += "       SYS.ALL_OBJECTS O2,\n";
                comd += "       SYS.ALL_COLUMNS C1,\n";
                comd += "       SYS.ALL_COLUMNS C2,\n";
                comd += "       SYS.FOREIGN_KEYS F\n";
                comd += "       INNER JOIN SYS.FOREIGN_KEY_COLUMNS K\n";
                comd += "         ON (K.CONSTRAINT_OBJECT_ID = F.OBJECT_ID)\n";
                comd += "       INNER JOIN SYS.INDEXES I\n";
                comd += "         ON (F.REFERENCED_OBJECT_ID = I.OBJECT_ID\n";
                comd += "             AND F.KEY_INDEX_ID = I.INDEX_ID)\n";
                comd += "WHERE  O1.OBJECT_ID = F.REFERENCED_OBJECT_ID\n";
                comd += "       AND O2.OBJECT_ID = F.PARENT_OBJECT_ID\n";
                comd += "       AND C1.OBJECT_ID = F.REFERENCED_OBJECT_ID\n";
                comd += "       AND C2.OBJECT_ID = F.PARENT_OBJECT_ID\n";
                comd += "       AND C1.COLUMN_ID = K.REFERENCED_COLUMN_ID\n";
                comd += "       AND C2.COLUMN_ID = K.PARENT_COLUMN_ID\n";
                dbcomd.CommandText = comd;
                tblResult = new DataTable();
                dataReader = dbcomd.ExecuteReader();
                List<String> fkKeys = new List<string>();
                while (dataReader.Read())
                    fkKeys.Add(dataReader.GetValue(0).ToString());
                dataReader.Close();

                foreach (String fkKey in fkKeys)
                {
                    comd = "SELECT PKTABLE_NAME = CONVERT(SYSNAME,O1.NAME),\n";
                    comd += "       PKCOLUMN_NAME = CONVERT(SYSNAME,C1.NAME),\n";
                    comd += "       FKTABLE_NAME = CONVERT(SYSNAME,O2.NAME),\n";
                    comd += "       FKCOLUMN_NAME = CONVERT(SYSNAME,C2.NAME),\n";
                    comd += "       FK_NAME = CONVERT(SYSNAME,OBJECT_NAME(F.OBJECT_ID))\n";
                    comd += "FROM   SYS.ALL_OBJECTS O1,\n";
                    comd += "       SYS.ALL_OBJECTS O2,\n";
                    comd += "       SYS.ALL_COLUMNS C1,\n";
                    comd += "       SYS.ALL_COLUMNS C2,\n";
                    comd += "       SYS.FOREIGN_KEYS F\n";
                    comd += "       INNER JOIN SYS.FOREIGN_KEY_COLUMNS K\n";
                    comd += "         ON (K.CONSTRAINT_OBJECT_ID = F.OBJECT_ID)\n";
                    comd += "       INNER JOIN SYS.INDEXES I\n";
                    comd += "         ON (F.REFERENCED_OBJECT_ID = I.OBJECT_ID\n";
                    comd += "             AND F.KEY_INDEX_ID = I.INDEX_ID)\n";
                    comd += "WHERE  O1.OBJECT_ID = F.REFERENCED_OBJECT_ID\n";
                    comd += "       AND O2.OBJECT_ID = F.PARENT_OBJECT_ID\n";
                    comd += "       AND C1.OBJECT_ID = F.REFERENCED_OBJECT_ID\n";
                    comd += "       AND C2.OBJECT_ID = F.PARENT_OBJECT_ID\n";
                    comd += "       AND C1.COLUMN_ID = K.REFERENCED_COLUMN_ID\n";
                    comd += "       AND C2.COLUMN_ID = K.PARENT_COLUMN_ID\n";
                    comd += "       And CONVERT(SYSNAME,OBJECT_NAME(F.OBJECT_ID)) = '" + fkKey + "'";
                    dbcomd.CommandText = comd;
                    dataReader = dbcomd.ExecuteReader();

                    String sFKTableName = "";
                    String sFKColumn = "";
                    String sPKTableName = "";
                    String sPKColumn = "";
                    String sFKName = "";

                    while (dataReader.Read())
                    {
                        sFKTableName = "[" + dataReader.GetValue(2).ToString() + "]";
                        sFKColumn += "[" + dataReader.GetValue(3).ToString() + "], ";
                        sPKTableName = "[" + dataReader.GetValue(0).ToString() + "]";
                        sPKColumn += "[" + dataReader.GetValue(1).ToString() + "], ";
                        sFKName = "[" + dataReader.GetValue(4).ToString() + "]";
                    }
                    dataReader.Close();

                    sFKColumn = sFKColumn.Substring(0, sFKColumn.Length - 2);
                    sPKColumn = sPKColumn.Substring(0, sPKColumn.Length - 2);

                    comd = "Alter Table " + sFKTableName + " Add Constraint " + sFKName + " Foreign Key (" + sFKColumn + ") References " + sPKTableName + " (" + sPKColumn + ")";
                    //script += comd + "\n\n";
                    scripts.Add(comd);
                }
                #endregion
                #region Script Defaults
                comd = "Select TABLE_NAME, COLUMN_NAME, COLUMN_DEFAULT\n";
                comd += "  From INFORMATION_SCHEMA.COLUMNS\n";
                comd += " Where IsNull(COLUMN_DEFAULT, '') <> ''\n";
                comd += "   And TABLE_NAME Not In ('dtproperties')\n";
                dbcomd.CommandText = comd;
                dataReader = dbcomd.ExecuteReader();
                tblResult = new DataTable();
                tblResult.Load(dataReader);
                dataReader.Close();
                for (Int32 tableLoop = 0; tableLoop < tblResult.Rows.Count; tableLoop++)
                {
                    String sTableName = "[" + tblResult.Rows[tableLoop][0].ToString() + "]";
                    String sColumnName = tblResult.Rows[tableLoop][1].ToString();
                    String sDefault = tblResult.Rows[tableLoop][2].ToString().Replace("(", "").Replace(")", "");

                    if (sDefault.ToLower() == "getdate")
                        sDefault = "getdate()";

                    if (sDefault.ToLower() == "newid")
                        sDefault = "(newid())";

                    comd = "Alter Table " + sTableName + " Add Constraint [DF_" + sTableName.Replace("[", "").Replace("]", "") + "_" + sColumnName.Replace("[", "").Replace("]", "") + "] Default " + sDefault + " For [" + sColumnName + "]";
                    scripts.Add(comd);
                    //script += comd + "\n\n";

                }
                #endregion
                #region Script Indexes
                comd = "Select distinct ind.name, t.name\n";
                comd += "  From sys.indexes ind Inner Join sys.index_columns ic On ind.object_id = ic.object_id and ind.index_id = ic.index_id \n";
                comd += " Inner Join sys.columns col On ic.object_id = col.object_id and ic.column_id = col.column_id \n";
                comd += " Inner Join sys.tables t ON ind.object_id = t.object_id \n";
                comd += " Where ind.is_primary_key = 0 \n";
                comd += "   And ind.is_unique_constraint = 0 \n";
                comd += "   And t.is_ms_shipped = 0 \n";
                comd += " Order By ind.name\n";
                dbcomd.CommandText = comd;
                dataReader = dbcomd.ExecuteReader();
                List<Index> indexes = new List<Index>();

                while (dataReader.Read())
                {
                    Index thisIndex = new Index();
                    thisIndex.IndexName = dataReader.GetValue(0).ToString();
                    thisIndex.TableName = dataReader.GetValue(1).ToString();
                    indexes.Add(thisIndex);
                }
                dataReader.Close();
                foreach (Index index in indexes)
                {
                    comd = "Select ind.is_unique, ind.name, t.name, col.name\n";
                    comd += "  From sys.indexes ind Inner Join sys.index_columns ic On ind.object_id = ic.object_id and ind.index_id = ic.index_id \n";
                    comd += " Inner Join sys.columns col On ic.object_id = col.object_id and ic.column_id = col.column_id \n";
                    comd += " Inner Join sys.tables t ON ind.object_id = t.object_id \n";
                    comd += " Where ind.is_primary_key = 0 \n";
                    comd += "   And ind.is_unique_constraint = 0 \n";
                    comd += "   And t.is_ms_shipped = 0 \n";
                    comd += "   And ind.name = '" + index.IndexName + "'\n";
                    comd += "   And t.name = '" + index.TableName + "'\n";
                    dbcomd.CommandText = comd;
                    dataReader = dbcomd.ExecuteReader();
                    tblResult = new DataTable();
                    tblResult.Load(dataReader);
                    dataReader.Close();
                    String isUnique = "";
                    String indexName = "";
                    String tableName = "";
                    String fieldName = "";
                    String indexStr = "Create ";
                    for (Int32 tableLoop = 0; tableLoop < tblResult.Rows.Count; tableLoop++)
                    {
                        isUnique = tblResult.Rows[tableLoop][0].ToString();
                        indexName = "[" + tblResult.Rows[tableLoop][1].ToString() + "]";
                        tableName = "[" + tblResult.Rows[tableLoop][2].ToString() + "]";
                        fieldName += "[" + tblResult.Rows[tableLoop][3].ToString() + "],";
                        if (isUnique == "1")
                            indexStr += "Unique ";
                    }

                    fieldName = fieldName.Substring(0, fieldName.Length - 1);
                    if (isUnique == "True")
                        indexStr = "Create Unique Index " + indexName + " On " + tableName + " (" + fieldName + ")";
                    else
                        indexStr = "Create Index " + indexName + " On " + tableName + " (" + fieldName + ")";
                    scripts.Add(indexStr);
                    //script += indexStr + "\n\n";
                }
                #endregion
                #region Script Triggers
                script += "Go\n\n";
                comd = "Select Tab.name As Table_Name, Trig.name As Trigger_Name, Trig.is_disabled, Trig.object_id\n";
                comd += "  From [sys].[triggers] As Trig\n";
                comd += " Inner Join sys.tables as Tab On TRIG.parent_id = TAB.object_id ";
                dbcomd = new SqlCommand(comd, openConn);
                dataReader = dbcomd.ExecuteReader();

                List<String> triggers = new List<String>();
                while (dataReader.Read())
                {
                    triggers.Add(dataReader.GetValue(1).ToString());
                }
                dataReader.Close();

                foreach (String trigger in triggers)
                {
                    comd = "sp_helptext " + trigger;
                    dbcomd.CommandText = comd;
                    dataReader = dbcomd.ExecuteReader();

                    String triggerText = "";
                    while (dataReader.Read())
                    {
                        triggerText += dataReader.GetValue(0).ToString();
                    }
                    //script += triggerText + "\n\n Go\n";
                    scripts.Add(triggerText);
                    dataReader.Close();

                }
                #endregion
                #region Script Stored Procedures
                comd = "Select a.name\n";
                comd += "  From sys.procedures a inner join sys.schemas b on a.schema_id = b.schema_id\n";
                comd += " Where b.name = 'dbo'\n";
                comd += "   And substring(a.name, 1, 2) <> 'pr'\n";
                comd += " Order By a.name\n";

                dbcomd.CommandText = comd;
                dataReader = dbcomd.ExecuteReader();

                List<String> procedures = new List<string>();
                while (dataReader.Read())
                {
                    procedures.Add(dataReader.GetValue(0).ToString());
                }

                dataReader.Close();
                foreach (String procedure in procedures)
                {
                    StoredProc += procedure + ", ";
                    String spText = "";
                    comd = "sp_helptext " + procedure;
                    dbcomd.CommandText = comd;
                    dataReader = dbcomd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        spText += dataReader.GetValue(0).ToString();
                    }

                    dataReader.Close();

                    //script += spText + "\n\nGo\n\n";
                    scripts.Add(spText);
                }
                #endregion
                #region Script Functions
                comd = "Select name From sys.objects Where type_desc Like '%FUNCTION%';\n";
                dbcomd.CommandText = comd;
                dataReader = dbcomd.ExecuteReader();

                List<String> functions = new List<string>();

                while (dataReader.Read())
                {
                    functions.Add(dataReader.GetValue(0).ToString());
                }

                dataReader.Close();

                String functionText = "";
                foreach (String function in functions)
                {
                    comd = "sp_helptext " + function;
                    dbcomd.CommandText = comd;
                    dataReader = dbcomd.ExecuteReader();

                    functionText = "";
                    while (dataReader.Read())
                    {
                        functionText += dataReader.GetValue(0).ToString();
                    }

                    dataReader.Close();

                    //script += functionText + "\n\nGo\n\n";
                    scripts.Add(functionText);
                }

                #endregion
                SaveScriptRequest(Convert.ToInt32(request.RequestID), true);
                return scripts;


            }
            catch (Exception Ex)
            {
                UpdateTempMethod("ScriptRequestGetScript", "(String pUserName, String pPinNo, eDatabases pDatabase)", false);
                if (StoredProc.Length > 2)
                {
                    StoredProc = StoredProc.Substring(0, StoredProc.Length - 2);
                }

                WriteLog(applicationID, "VCS.BackOffice.BL.ScriptRequestGetScript. " + Ex.Message, eLogType.Error);
                List<String> error = new List<string>();
                error.Add("Error: Unable to get script: " + Ex.Message);
                return error;
            }
        }
        public String ScriptRequestGeneratePin(String pUserName, String pPassword, String pDeveloperName)
        {
            try
            {
                UpdateTempMethod("ScriptRequestGeneratePin", "(String pUserName, String pPassword, String pDeveloperName)", true);
                DPO.VirtualVendor.BL.Authentication auth = vvoBL.Authenticate(pUserName, pPassword);
                if (auth.VCSResponseCode != "0000")
                {
                    return auth.VCSResponseMessage;
                }

                tblScriptRequestUser requestUser = GetScriptRequestUserByName(pDeveloperName);
                if (requestUser == null)
                    throw new Exception("Error: The user name was not recognized.");

                if (!Convert.ToBoolean(requestUser.Active))
                    throw new Exception("Error: This user is marked as inactive.");

                String pinNo = "";
                Boolean found = true;
                //Generate number, check if it is on the database. if so then generate again.
                while (found)
                {
                    Random random = new Random();
                    pinNo = random.Next(0, 99999).ToString();
                    if (GetScriptRequestByPinNo(pinNo) == null)
                        found = false;
                }

                if (!SaveScriptRequest(pDeveloperName, pinNo))
                    throw new Exception("Error: Unable to insert script request record.");

                return pinNo;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("ScriptRequestGeneratePin", "(String pUserName, String pPassword, String pDeveloperName)", false);
                throw new Exception("Error: Unable to generate pin. " + Ex.Message);
            }
        }
        public String ScriptRequestGeneratePin(String pUserName)
        {
            try
            {
                UpdateTempMethod("ScriptRequestGeneratePin", "(String pUserName)", true);
                tblScriptRequestUser requestUser = GetScriptRequestUserByName(pUserName);
                if (requestUser == null)
                    return "Error: The user name was not recognized.";

                if (!Convert.ToBoolean(requestUser.Active))
                    return "Error: This user is marked as inactive.";

                String pinNo = "";
                Boolean found = true;
                //Generate number, check if it is on the database. if so then generate again.
                while (found)
                {
                    Random random = new Random();
                    pinNo = random.Next(0, 99999).ToString();
                    if (GetScriptRequestByPinNo(pinNo) == null)
                        found = false;
                }

                if (!SaveScriptRequest(pUserName, pinNo))
                    return "Error: Unable to insert script request record.";

                MailPin(pUserName, pinNo);

                return "Pin Issued. Please check your mail.";
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("ScriptRequestGeneratePin", "(String pUserName)", false);
                return "Error: Unable to generate pin. " + Ex.Message;
            }
        }
        public void MailPin(String pUserName, String pPinNo)
        {

            Int32 applicationID = Convert.ToInt32(GetApplication("Back Office", "ScriptRequestService").ApplicationID);
            tblSetting setting = GetSettingByName("PinExpireSeconds", applicationID);

            List<String> message = new List<string>();
            message.Add("Dear " + pUserName + ",");
            message.Add(String.Empty);
            message.Add("Your script request pin is: <b>" + pPinNo + "</b>");
            message.Add("This pin will expire in " + setting.SettingValue + " seconds.");
            message.Add(String.Empty);
            message.Add("Thank You");

            tblScriptRequestUser user = GetScriptRequestUserByName(pUserName);

            messBL.SendMail("Script Request Pin", message, user.EMail, "VCS.BackOffice.ScriptRequest.WCF");
        }
        public tblScriptRequest GetScriptRequestByPinNo(String pPinNo)
        {
			try
			{
                UpdateTempMethod("GetScriptRequestByPinNo", "(String pPinNo)", true);
				tblScriptRequestCrud crud = new tblScriptRequestCrud();
				crud.Where("Pin", DAL.General.Operator.Equals, pPinNo);
				recordFound = false;
				tblScriptRequest search = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetScriptRequestByPinNo", "(String pPinNo)", false);
				throw Ex;
			}

        }
        public tblScriptRequestUser GetScriptRequestUserByName(String pUserName)
        {
            try
            {
                UpdateTempMethod("GetScriptRequestUserByName", "(String pUserName)", true);
                recordFound = false;
                tblScriptRequestUserCrud crud = new tblScriptRequestUserCrud();
                crud.Where("UserName", DAL.General.Operator.Equals, pUserName);

                tblScriptRequestUser search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetScriptRequestUserByName", "(String pUserName)", false);
                throw new Exception("GetScriptRequestUserByName - " + Ex.Message);
            }
        }
        public List<tblScriptRequestUserDB> GetScriptRequestUserDB(String pUserName)
        {
            tblScriptRequestUser user = GetScriptRequestUserByName(pUserName);

			tblScriptRequestUserDBCrud crud = new tblScriptRequestUserDBCrud();
			crud.Where("UserID", DAL.General.Operator.Equals, (Int32)user.UserId);
			List<tblScriptRequestUserDB> results = crud.ReadMulti().ToList() ;
			return results;
            
        }
        public Boolean SaveScriptRequestUserDB(Byte pUserID, String pLocalDatabase, String pMappedDatabase)
        {
            try
            {
                UpdateTempMethod("SaveScriptRequestUserDB", "(Byte pUserID, String pLocalDatabase, String pMappedDatabase)", true);
                tblScriptRequestUserDBCrud crud = new tblScriptRequestUserDBCrud();
                tblScriptRequestUserDB workRec = new tblScriptRequestUserDB();
                workRec.LocalDatabase = pLocalDatabase;
                crud.Insert(workRec);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveScriptRequestUserDB", "(Byte pUserID, String pLocalDatabase, String pMappedDatabase)", false);
                throw new Exception("SaveScriptRequestUserDBByte - " + Ex.Message);
            }
        }
    }
}
