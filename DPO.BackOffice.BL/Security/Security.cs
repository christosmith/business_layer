﻿using System;
using System.Collections.Generic;
using System.Linq;
using DPO.BackOffice.DAL;

namespace DPO.BackOffice.BL
{
	public partial class BackOfficeBL
	{
		public Boolean SendApplicationSecurityAlert(String pApplicationName)
		{
			try
			{
                UpdateTempMethod("SendApplicationSecurityAlert", "(String pApplicationName)", true);
				tblApplicationSecurityAlert alert = GetApplicationSecurityAlert(pApplicationName);
				DateTime scheduleTime = DateTime.Now;
				Int32 factor = Convert.ToInt32(alert.Frequency);
				switch (Convert.ToInt32(alert.IntervalID))
				{
					case 1: scheduleTime = scheduleTime.AddSeconds(factor); break;
					case 2: scheduleTime = scheduleTime.AddMinutes(factor); break;
					case 3: scheduleTime = scheduleTime.AddHours(factor); break;
				}

				VirtualVendor.DAL.vInvalidReferrersLatestDateLoaded invalid = vvoBL.GetInvalidReferrerLatestDateLoaded();
				DateTime dateLoaded = Convert.ToDateTime(invalid.DateLoaded);

				if (scheduleTime > dateLoaded)
				{
					String mailBody = "<html>\n";
					mailBody += "<body bgcolor=white>\n";
					mailBody += "<table style=\"border-collapse:collapse\">\n";
					mailBody += "<tr><th colspan='9' style='font-weight:bold;font-size:14pt;font-family:verdana;border:1px solid #778899;text-align:center;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Security Alert</th></tr>\n";
					mailBody += "<tr>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Referrer</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Referrer URL</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Valid URL</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Page Requested</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Parameters Received</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>LoginId</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>TID</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>RemoteIPAddress</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>DateLoaded</th>\n";
					mailBody += "</tr>\n";
					//mailBody += "<tr>\n";
					List<DPO.VirtualVendor.DAL.vInvalidReferrers> invalids = vvoBL.GetInvalidReferrer();
					foreach (DPO.VirtualVendor.DAL.vInvalidReferrers inv in invalids)
					{
						mailBody += "<tr>\n";
						mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + inv.Referrer + "</td>\n";
						mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + inv.ReferrerUrl + "</td>\n";
						mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + inv.ValidUrl + "</td>\n";
						mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + inv.PageRequested + "</td>\n";
						mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + inv.ParametersReceived + "</td>\n";
						mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + inv.LoginId + "</td>\n";
						mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + inv.TID + "</td>\n";
						mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + inv.RemoteIPAddress + "</td>\n";
						mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + inv.DateLoaded + "</td>\n";
						mailBody += "</tr>\n";
						vvoBL.UpdateInvalidReferrer(Convert.ToInt32(inv.AutoID), alert.AlertMailAddress);
					}

					//mailBody += "</tr>\n";
					mailBody += "</table>\n";
					mailBody += "</body>\n";
					mailBody += "<html>\n";

					messBL.UseMailBodyAsIs = true;
					messBL.SendMail("Security Alert", mailBody, alert.AlertMailAddress, "VCS.General.Security", "backoffice@vcs.co.za");

				}
				return true;

			}
			catch (Exception Ex)
			{
                UpdateTempMethod("SendApplicationSecurityAlert", "(String pApplicationName)", false);
				throw Ex;
			}
		}
		public Boolean SendBlockedIPSecurityAlert(String pIPAddress, String pPageRequested, String pRemoteIPAddressURL, String pLoginID, String pTID, String pUserAgent, String pCookie, String pApplicationName)
		{
			try
			{
                UpdateTempMethod("SendBlockedIPSecurityAlert", "(String pIPAddress, String pPageRequested, String pRemoteIPAddressURL, String pLoginID, String pTID, String pUserAgent, String pCookie, String pApplicationName)", true);
				DPO.VirtualVendor.DAL.BlockedIPs blocked = vvoBL.GetBlockedIPs(pIPAddress);
				if (Convert.ToInt32(blocked.Attempts) % 1000 == 0)
				{
					String mailBody = "<html>\n";
					mailBody += "<body bgcolor=white>\n";
					mailBody += "<table style=\"border-collapse:collapse\">\n";
					mailBody += "<tr><th colspan='10' style='font-weight:bold;font-size:14pt;font-family:verdana;border:1px solid #778899;text-align:center;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Security Alert</th></tr>\n";
					mailBody += "<tr>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Page Requested</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Remote IP Address URL</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>First Date Blocked</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>First Reason Blocked</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Attempts</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Last Attempt</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Login Id</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>TID</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Remote User Agent</th>\n";
					mailBody += "<th style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>HTTP_cookie</th>\n";
					mailBody += "</tr>\n";
					mailBody += "<tr>\n";
					mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + pPageRequested + "</td>\n";
					mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + pRemoteIPAddressURL + "</td>\n";
					mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + blocked.DateBlocked + "</td>\n";
					mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + blocked.ReasonBlocked + "</td>\n";
					mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + blocked.Attempts + "</td>\n";
					mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + blocked.DateLastTried + "</td>\n";
					mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + pLoginID + "</td>\n";
					mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + pTID + "</td>\n";
					mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + pUserAgent + "</td>\n";
					mailBody += "<td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + pCookie + "</td>\n";

					mailBody += "</tr>\n";
					mailBody += "</table>\n";
					mailBody += "</body>\n";
					mailBody += "<html>\n";

					tblApplicationSecurityAlert alert = GetApplicationSecurityAlert(pApplicationName);

					messBL.SendMail("Security Alert - IP Blocked", mailBody, alert.AlertMailAddress, "VCS.General.Security", "backoffice@vcs.co.za");


				}

				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("SendBlockedIPSecurityAlert", "(String pIPAddress, String pPageRequested, String pRemoteIPAddressURL, String pLoginID, String pTID, String pUserAgent, String pCookie, String pApplicationName)", false);
				String errorMessage = "SendBlockedIPSecurityAlert - " + Ex.Message;
				throw new Exception(errorMessage);
			}
		}
		public Boolean CheckInvalidSecurityPhrase(String pParameter, String pIPAddress, String pApplicationName, String pPageCalled)
		{
			try
			{
                UpdateTempMethod("CheckInvalidSecurityPhrase", "(String pParameter, String pIPAddress, String pApplicationName, String pPageCalled)", true);
				if (pParameter == "")
					return false;

				pParameter = pParameter.ToLower();

				List<tblSecurityInvalidPhrase> phrases = GetSecurityInvalidPhrase();
				foreach (tblSecurityInvalidPhrase phrase in phrases)
				{
					if (pParameter.IndexOf(phrase.Phrase.ToLower().Trim()) != -1)
					{
						if (pIPAddress != "")
						{
							tblApplicationSecurityAlert alert = GetApplicationSecurityAlert(pApplicationName);
							vvoBL.InsertVcsAlert(3, pIPAddress, alert.AlertMailAddress, "B");
							String varIPReasonBlocked = "Code insertion attempt: [" + phrase.Phrase.Trim() + "] found in parameter(s) passed to: " + alert.tblApplication.tblSystem.DomainURL + pPageCalled;
							if (varIPReasonBlocked.Length > 1000)
								varIPReasonBlocked = varIPReasonBlocked.Substring(0, 1000);
							vvoBL.InsertBlockedIP(pIPAddress, varIPReasonBlocked);
						}
						return true;
					}
				}

				List<String> invalidChar = new List<string>();
				invalidChar.Add("$");
				invalidChar.Add("{");
				invalidChar.Add("@");
				invalidChar.Add("<");

				List<String> parameters = pParameter.Split('&').ToList();
				foreach (String parameter in parameters)
				{
					String[] parSplit = parameter.Split('=');
					String firstChar = parSplit[1].Substring(0, 1);
					List<String> findChar = invalidChar.FindAll(x => x == firstChar);
					if (findChar.Count > 0)
					{
						if (parSplit[0].ToLower() != "password" && parSplit[0].ToLower() != "userid")
						{
							if (pIPAddress != "")
							{
								tblApplicationSecurityAlert alert = GetApplicationSecurityAlert(pApplicationName);
								vvoBL.InsertVcsAlert(3, pIPAddress, alert.AlertMailAddress, "B");
								String varIPReasonBlocked = "Code insertion attempt: [" + parSplit[1].Trim() + "] found in parameter(s) passed to: " + alert.tblApplication.tblSystem.DomainURL + pPageCalled;
								if (varIPReasonBlocked.Length > 1000)
									varIPReasonBlocked = varIPReasonBlocked.Substring(0, 1000);
								vvoBL.InsertBlockedIP(pIPAddress, varIPReasonBlocked);
							}
							return true;
						}
					}
				}


				return false;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("CheckInvalidSecurityPhrase", "(String pParameter, String pIPAddress, String pApplicationName, String pPageCalled)", false);
				String errorMessage = "CheckInvalidSecurityPhrase(String) - " + Ex.Message;
				throw new Exception(errorMessage);

			}
		}
		public List<tblSecurityInvalidPhrase> GetSecurityInvalidPhrase()
		{
			try
			{
                UpdateTempMethod("GetSecurityInvalidPhrase", "()", true);
				recordFound = false;
				tblSecurityInvalidPhraseCrud crud = new tblSecurityInvalidPhraseCrud();
				List<tblSecurityInvalidPhrase> search = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetSecurityInvalidPhrase", "()", false);
				throw new Exception("GetSecurityInvalidPhrase - " + Ex.Message);
			}
		}
		public tblApplicationSecurityAlert GetApplicationSecurityAlert(String pApplicationName)
		{
			try
			{
                UpdateTempMethod("GetApplicationSecurityAlert", "(String pApplicationName)", true);
				tblApplicationSecurityAlertCrud crud = new tblApplicationSecurityAlertCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, Convert.ToInt32(GetApplicationByName(pApplicationName).ApplicationID));
				List<tblApplicationSecurityAlert> results = crud.ReadMulti().ToList();
				if (results.Count == 0)
					throw new Exception("Security alert for " + pApplicationName + " was not set up.");

				return results[0];
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetApplicationSecurityAlert", "(String pApplicationName)", false);
				throw Ex;
			}
		}
		public List<tblApplicationReferrer> GetApplicationReferrer(Int32 pApplicationID)
		{
			try
			{
                UpdateTempMethod("GetApplicationReferrer", "(Int32 pApplicationID)", true);
				recordFound = false;
				tblApplicationReferrerCrud crud = new tblApplicationReferrerCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);

				List<tblApplicationReferrer> search = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetApplicationReferrer", "(Int32 pApplicationID)", false);
				throw new Exception("GetApplicationReferrer - " + Ex.Message);
			}
		}
		public Boolean VerifyReferrerURL(Int32 pApplicationID, String pReferrerURL)
		{
			try
			{
                UpdateTempMethod("VerifyReferrerURL", "(Int32 pApplicationID, String pReferrerURL)", true);
				List<tblApplicationReferrer> appReferrers = GetApplicationReferrer(pApplicationID);
				List<tblApplicationReferrer> findReferrer = appReferrers.FindAll(x => x.tblSecurityReferrerAllowed.ReferrerURL == pReferrerURL).ToList();
				if (findReferrer.Count == 0)
				{
					WriteLog(pApplicationID, "Invalid referrer request: " + pReferrerURL, eLogType.Error);
					return false;

				}
				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("VerifyReferrerURL", "(Int32 pApplicationID, String pReferrerURL)", false);
				throw Ex;

			}
		}
	}
}
