﻿using System;
using System.Collections.Generic;
using System.Linq;
using DPO.BackOffice.DAL;

namespace DPO.BackOffice.BL
{
	public partial class BackOfficeBL
	{

		public Boolean UpdateSetting(Int32 pSettingID, String pSettingValue)
        {
            try
            {
                UpdateTempMethod("UpdateSetting", "(Int32 pSettingID, String pSettingValue)", true);
				tblSetting setting = GetSetting(pSettingID);
				setting.SettingValue = pSettingValue;
				tblSettingCrud crud = new tblSettingCrud();
				crud.Update(setting);
				return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("UpdateSetting", "(Int32 pSettingID, String pSettingValue)", false);
                throw new Exception("UpdateSetting - " + Ex.Message);
            }
        }
		public String GetExceptionAllowedGidData()
		{
			try
			{
                UpdateTempMethod("GetExceptionAllowedGidData", "()", true);
				List<tblExceptionAllowed> allows = GetExceptionAllowed();
				String divString = "";
				foreach (tblExceptionAllowed allow in allows)
				{
					divString += allow.ApplicationID.ToString() + "═";
					divString += allow.AllowedID.ToString() + "═";
					divString += allow.ExceptionPhrase + "║";
				}

				if (divString.Length > 0)
					divString = divString.Substring(0, divString.Length - 1);

				return divString;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetExceptionAllowedGidData", "()", false);
				throw new Exception(errorMessage + "GetExceptionGridData - " + Ex.Message);
			}
		}
		public Boolean DeleteExceptionAllowed(Int32 pAllowedID)
        {
            try
            {
                UpdateTempMethod("DeleteExceptionAllowed", "(Int32 pAllowedID)", true);
                tblExceptionAllowedCrud crud = new tblExceptionAllowedCrud();
                crud.Where("AllowedID", DAL.General.Operator.Equals, pAllowedID);

                List<tblExceptionAllowed> search = crud.ReadMulti().ToList();
                foreach (tblExceptionAllowed del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteExceptionAllowed", "(Int32 pAllowedID)", false);
                throw new Exception("DeleteExceptionAllowed - " + Ex.Message);
            }
        }
		public tblExceptionAllowed GetExceptionAllowed(Int32 pAllowedID)
        {
            try
            {
                UpdateTempMethod("GetExceptionAllowed", "(Int32 pAllowedID)", true);
                recordFound = false;
                tblExceptionAllowedCrud crud = new tblExceptionAllowedCrud();
                crud.Where("AllowedID", DAL.General.Operator.Equals, pAllowedID);

                tblExceptionAllowed search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetExceptionAllowed", "(Int32 pAllowedID)", false);
                throw new Exception("GetExceptionAllowed - " + Ex.Message);
            }
        }
		public List<tblExceptionAllowed> GetExceptionAllowed()
        {
            try
            {
                UpdateTempMethod("GetExceptionAllowed", "()", true);
                recordFound = false;
                tblExceptionAllowedCrud crud = new tblExceptionAllowedCrud();
                List<tblExceptionAllowed> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetExceptionAllowed", "()", false);
                throw new Exception("GetExceptionAllowed - " + Ex.Message);
            }
        }
		public Int32 SaveExceptionAllowed(Int32 pApplicationID, String pExceptionPhrase)
        {
            try
            {
                UpdateTempMethod("SaveExceptionAllowed", "(Int32 pApplicationID, String pExceptionPhrase)", true);
                tblExceptionAllowedCrud crud = new tblExceptionAllowedCrud();
                tblExceptionAllowed workRec = new tblExceptionAllowed();
                if (pApplicationID != 0)
                {
                    crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
                    workRec = crud.ReadSingle();
                    workRec.ExceptionPhrase = pExceptionPhrase;
                    crud.Update(workRec);
                    return pApplicationID;
                }
                else
                {
                    workRec.ExceptionPhrase = pExceptionPhrase;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveExceptionAllowed", "(Int32 pApplicationID, String pExceptionPhrase)", false);
                throw new Exception("SaveExceptionAllowed - " + Ex.Message);
            }
        }
		public Int32 SaveSubApplication(Int32 pApplicationID, Int32 pSubApplicationID, String pSubApplicationName)
        {
            try
            {
                UpdateTempMethod("SaveSubApplication", "(Int32 pApplicationID, Int32 pSubApplicationID, String pSubApplicationName)", true);
				tblApplicationSubAppCrud workRec = new tblApplicationSubAppCrud();
				tblApplicationSubApp search = GetSubApplication(pApplicationID, pSubApplicationID);
				if (!recordFound)
                {
					search.ApplicationID = pApplicationID;
					search.SubApplicationID = pSubApplicationID;
					search.SubApplication = pSubApplicationName;
					long ID = 0;
					workRec.Insert(search, out ID);
					return (Int32)ID;
                }
                else
                {
					search.SubApplicationID = pSubApplicationID;
					search.SubApplication = pSubApplicationName;
					workRec.Update(search);
					return pSubApplicationID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveSubApplication", "(Int32 pApplicationID, Int32 pSubApplicationID, String pSubApplicationName)", false);
                throw new Exception("SaveSubApplication - " + Ex.Message);
            }
        }
		public Int32 SaveSubApplication(Int32 pApplicationID, String pSubApplicationName)
        {
            try
            {
                UpdateTempMethod("SaveSubApplication", "(Int32 pApplicationID, String pSubApplicationName)", true);
                tblApplicationSubAppCrud crud = new tblApplicationSubAppCrud();
                tblApplicationSubApp workRec = new tblApplicationSubApp();
				workRec.ApplicationID = pApplicationID;
				workRec.SubApplication = pSubApplicationName;
				long ID = 0;
				crud.Insert(workRec, out ID);
				return (Int32)ID;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveSubApplication", "(Int32 pApplicationID, String pSubApplicationName)", false);
                throw new Exception("SaveSubApplication - " + Ex.Message);
            }
        }
		public String GetSubApplicationGridData()
        {
            try
            {
                UpdateTempMethod("GetSubApplicationGridData", "()", true);
                tblApplicationSubAppCrud crud = new tblApplicationSubAppCrud();
                List<tblApplicationSubApp> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (tblApplicationSubApp workRec in search)
                {
                    divString += workRec.ApplicationID.ToString() + "═";
                    divString += workRec.SubApplicationID.ToString() + "═";
                    divString += workRec.SubApplication + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetSubApplicationGridData", "()", false);
                throw new Exception("GetSubApplicationGridData - " + Ex.Message);
            }
        }
		public Boolean DeleteSetting(Int32 pSettingID)
        {
            try
            {
                UpdateTempMethod("DeleteSetting", "(Int32 pSettingID)", true);
                tblSettingCrud crud = new tblSettingCrud();
                crud.Where("SettingID", DAL.General.Operator.Equals, pSettingID);

                List<tblSetting> search = crud.ReadMulti().ToList();
                foreach (tblSetting del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteSetting", "(Int32 pSettingID)", false);
                throw new Exception("DeleteSetting - " + Ex.Message);
            }
        }
		public Int32 SaveSetting(Int32 pSettingID, Int32 pApplicationID, String pSettingName, String pSettingValue, Boolean pSensitiveInformation, Int32 pSubApplicationID)
        {
            try
            {
                UpdateTempMethod("SaveSetting", "(Int32 pSettingID, Int32 pApplicationID, String pSettingName, String pSettingValue, Boolean pSensitiveInformation, Int32 pSubApplicationID)", true);
                tblSettingCrud crud = new tblSettingCrud();
                tblSetting workRec = new tblSetting();
                if (pSettingID != 0)
                {
                    crud.Where("SettingID", DAL.General.Operator.Equals, pSettingID);
                    workRec = crud.ReadSingle();
                    workRec.ApplicationID = pApplicationID;
                    workRec.SettingName = pSettingName;
                    workRec.SettingValue = pSettingValue;
                    workRec.SensitiveInformation = pSensitiveInformation;
                    workRec.SubApplicationID = pSubApplicationID;
                    crud.Update(workRec);
                    return pSettingID;
                }
                else
                {
                    workRec.ApplicationID = pApplicationID;
                    workRec.SettingName = pSettingName;
                    workRec.SettingValue = pSettingValue;
                    workRec.SensitiveInformation = pSensitiveInformation;
                    workRec.SubApplicationID = pSubApplicationID;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveSetting", "(Int32 pSettingID, Int32 pApplicationID, String pSettingName, String pSettingValue, Boolean pSensitiveInformation, Int32 pSubApplicationID)", false);
                throw new Exception("SaveSetting - " + Ex.Message);
            }
        }
		public Int32 SaveSetting(Int32 pSettingID, Int32 pApplicationID, String pSettingName, String pSettingValue, Boolean pSensitiveInformation)
        {
            try
            {
                UpdateTempMethod("SaveSetting", "(Int32 pSettingID, Int32 pApplicationID, String pSettingName, String pSettingValue, Boolean pSensitiveInformation)", true);
                tblSettingCrud crud = new tblSettingCrud();
                tblSetting workRec = new tblSetting();
                if (pSettingID != 0)
                {
                    crud.Where("SettingID", DAL.General.Operator.Equals, pSettingID);
                    workRec = crud.ReadSingle();
                    workRec.ApplicationID = pApplicationID;
                    workRec.SettingName = pSettingName;
                    workRec.SettingValue = pSettingValue;
                    workRec.SensitiveInformation = pSensitiveInformation;
                    crud.Update(workRec);
                    return pSettingID;
                }
                else
                {
                    workRec.ApplicationID = pApplicationID;
                    workRec.SettingName = pSettingName;
                    workRec.SettingValue = pSettingValue;
                    workRec.SensitiveInformation = pSensitiveInformation;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveSetting", "(Int32 pSettingID, Int32 pApplicationID, String pSettingName, String pSettingValue, Boolean pSensitiveInformation)", false);
                throw new Exception("SaveSetting - " + Ex.Message);
            }
        }
		public String GetSettingGridData()
        {
            try
            {
                UpdateTempMethod("GetSettingGridData", "()", true);
                tblSettingCrud crud = new tblSettingCrud();
                crud.OrderBy(x => x.SettingName);
                List<tblSetting> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (tblSetting workRec in search)
                {
                    divString += workRec.ApplicationID + "═";
                    divString += workRec.SettingName + "═";
                    divString += workRec.SettingValue + "═";
                    divString += workRec.SettingID + "═";
                    divString += workRec.tblApplication.ServerID + "═";
                    divString += workRec.SensitiveInformation + "═";
                    divString += workRec.SubApplicationID + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetSettingGridData", "()", false);
                throw new Exception("GetSettingGridData - " + Ex.Message);
            }
        }
		public String ExportSettings(Int32 pApplicationID)
		{
			try
			{
                UpdateTempMethod("ExportSettings", "(Int32 pApplicationID)", true);
				String export = "Declare @ApplicationID Int\n";
				tblApplication application = GetApplication(pApplicationID);
				export += "If (Select COUNT(*) From tblApplication Where vcsApplication = '" + application.vcsApplication + "') = 0\n";
				export += "    Begin\n";
				export += "        Insert Into tblApplication\n";
				export += "        (ApplicationTypeID, SystemID, vcsApplication, ServerID, ApplicationDescription)\n";
				export += "        Values\n";
				export += "        (" + application.ApplicationTypeID + ", " + application.SystemID + ",'" + application.vcsApplication.Replace("'", "''") + "', " + application.ServerID + ", '" + application.ApplicationDescription.Replace("'", "''") + "')\n\n";
				export += "        Select @ApplicationID = SCOPE_IDENTITY();\n";
				export += "    End\n";
				export += "Else\n";
				export += "    Select @ApplicationID = ApplicationID From tblApplication Where vcsApplication = '" + application.vcsApplication + "'\n\n";

				foreach (tblApplicationSubApp subApp in GetSubApplication(pApplicationID))
				{
					export += "If (Select COUNT(*) From tblApplicationSubApp Where @ApplicationID = @ApplicationID And SubApplication = '" + subApp.SubApplication + "') = 0\n";
					export += "	Insert Into tblApplicationSubApp\n";
					export += "	(ApplicationID, SubApplication)\n";
					export += "	Values\n";
					export += "	(@ApplicationID, '" + subApp.SubApplication + "')\n\n";

				}

				tblSettingCrud setCrud = new tblSettingCrud();
				setCrud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
				List<tblSetting> settings = setCrud.ReadMulti().ToList();
				foreach (tblSetting setting in settings)
				{
					if (setting.SubApplicationID == null)
					{
						export += "If (Select COUNT(*) From tblSetting Where ApplicationID = @ApplicationID And SettingName = '" + setting.SettingName + "') = 0\n";
						export += "    Insert Into tblSetting\n";
						export += "    (ApplicationID, SettingName, SettingValue, SensitiveInformation)\n";
						export += "    Values\n";
						export += "    (@ApplicationID, '" + setting.SettingName + "', '" + setting.SettingValue + "', ";
						if (Convert.ToBoolean(setting.SensitiveInformation))
							export += "1)\n\n";
						else
							export += "0)\n\n";
					}
					else
					{
						export += "If (Select COUNT(*) From tblSetting Where ApplicationID = @ApplicationID And SettingName = '" + setting.SettingName + "') = 0\n";
						export += "    Insert Into tblSetting\n";
						export += "    (ApplicationID, SettingName, SettingValue, SensitiveInformation, SubApplicationID)\n";
						export += "    Values\n";
						export += "    (@ApplicationID, '" + setting.SettingName + "', '" + setting.SettingValue + "', ";
						if (Convert.ToBoolean(setting.SensitiveInformation))
							export += "1, " + Convert.ToInt32(setting.SubApplicationID).ToString() + ")\n\n";
						else
							export += "0, " + Convert.ToInt32(setting.SubApplicationID).ToString() + ")\n\n";
					}
				}


				return export;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("ExportSettings", "(Int32 pApplicationID)", false);
				throw Ex;
			}
		}
		public List<tblSetting> GetSettingByApplicationID(Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("GetSettingByApplicationID", "(Int32 pApplicationID)", true);
                recordFound = false;
                tblSettingCrud crud = new tblSettingCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);

                List<tblSetting> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetSettingByApplicationID", "(Int32 pApplicationID)", false);
                throw new Exception("GetSettingByApplicationID - " + Ex.Message);
            }
        }
		public List<tblSetting> GetSettingByApplicationID(Int32 pApplicationID, Int32 pSubApplicationID)
        {
            try
            {
                UpdateTempMethod("GetSettingByApplicationID", "(Int32 pApplicationID, Int32 pSubApplicationID)", true);
                recordFound = false;
                tblSettingCrud crud = new tblSettingCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
                crud.And("SubApplicationID", DAL.General.Operator.Equals, pSubApplicationID);

                List<tblSetting> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetSettingByApplicationID", "(Int32 pApplicationID, Int32 pSubApplicationID)", false);
                throw new Exception("GetSettingByApplicationID - " + Ex.Message);
            }
        }

		public tblSetting GetSetting(String pSystem, String pApplication, String pSettingName)
		{
			try
			{
				tblSystem system = GetSystemByName(pSystem);
				tblApplication application = GetApplicationByName((Byte)system.SystemID, pApplication);
				tblSettingCrud crud = new tblSettingCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, (Int32)application.ApplicationID);
				crud.And("SettingName", DAL.General.Operator.Equals, pSettingName);
				recordFound = false;
				tblSetting setting = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return setting;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetSetting (string, string, string) -" + Ex.Message);
			}
		}

        


        public tblSetting GetSetting(Int32 pSettingID)
        {
            try
            {
                UpdateTempMethod("GetSetting", "(Int32 pSettingID)", true);
                recordFound = false;
                tblSettingCrud crud = new tblSettingCrud();
                crud.Where("SettingID", DAL.General.Operator.Equals, pSettingID);

                tblSetting search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetSetting", "(Int32 pSettingID)", false);
                throw new Exception("GetSetting - " + Ex.Message);
            }
        }
        public tblSetting GetSettingByName(String pSettingName, Byte pSystemID, String pApplicationName)
        {
            try
            {
                UpdateTempMethod("GetSettingByName", "(String pSettingName, Byte pSystemID, String pApplicationName)", true);
                tblApplication application = GetApplicationByName(pSystemID, pApplicationName);
				tblSettingCrud crud = new tblSettingCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, (Int32)application.ApplicationID);
				crud.And("SettingName", DAL.General.Operator.Equals, pSettingName);
				List<tblSetting> settings = crud.ReadMulti().ToList();
                if (settings.Count > 0)
                    return settings[0];

                if (settings.Count == 0)
                    throw new Exception("BackOfficeBL.GetSettingByName - Setting with the name of " + pSettingName + " was not found.");

                return null;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetSettingByName", "(String pSettingName, Byte pSystemID, String pApplicationName)", false);
                throw Ex;
            }
        }
		public tblSetting GetSettingByName(String pSettingName, Int32 pApplicationID, Int32 pSubApplicationID)
        {
            try
            {
                recordFound = false;
                tblSettingCrud crud = new tblSettingCrud();
                crud.Where("SettingName", DAL.General.Operator.Equals, pSettingName);
                crud.And("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
                crud.And("SubApplicationID", DAL.General.Operator.Equals, pSubApplicationID);

                tblSetting search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetSettingByName - " + Ex.Message);
            }
        }
		public tblSetting GetSettingByName(String pSettingName, Int32 pApplicationID)
        {
            try
            {
                UpdateTempMethod("GetSettingByName", "(String pSettingName, Int32 pApplicationID)", true);
                recordFound = false;
                tblSettingCrud crud = new tblSettingCrud();
                crud.Where("SettingName", DAL.General.Operator.Equals, pSettingName);
                crud.And("ApplicationID", DAL.General.Operator.Equals, pApplicationID);

                tblSetting search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetSettingByName", "(String pSettingName, Int32 pApplicationID)", false);
                throw new Exception("GetSettingByName - " + Ex.Message);
            }
        }
    }
}
