﻿using System;
using DPO.BackOffice.DAL;

namespace DPO.BackOffice.BL
{
	public partial class BackOfficeBL
	{
		public tblTempMethod GetTempMethod(String pMethodName, String pParameter)
		{
			try
			{
				tblTempMethodCrud crud = new tblTempMethodCrud();
				crud.Where("Method", DAL.General.Operator.Equals, pMethodName);
				crud.And("Parameter", DAL.General.Operator.Equals, pParameter);
				recordFound = false;
				tblTempMethod method = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return method;

			}
			catch (Exception Ex)
			{
				throw new Exception("GetTempMethod - " + Ex.Message);
			}
		}
		public Boolean UpdateTempMethod(String pMethodName, String pParameter, Boolean pSuccess)
		{
			try
			{
				return true;
				tblTempMethod meth = GetTempMethod(pMethodName, pParameter);
				if (!recordFound)
					throw new Exception("Method " + pMethodName + " not found!");

				meth.MethodCalled = true;
				meth.CallSuccess = pSuccess;
				tblTempMethodCrud crud = new tblTempMethodCrud();
				crud.Update(meth);

				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("UpdateTempMethod - " + Ex.Message);
			}
		}
	}
}
