﻿using System;
using System.Collections.Generic;
using System.Linq;
using DPO.BackOffice.DAL;
using DPO.VirtualVendor.DAL;

namespace DPO.BackOffice.BL
{
	public partial class BackOfficeBL
    {
       /* public Int32 InsertThirdPartyUssdParameter(Int32 pProductID, String pFunctionName, String pVCSTerminalID)
        {
            try
            {
                tblThirdPartyUssdParameterCrud crud = new tblThirdPartyUssdParameterCrud();
                tblThirdPartyUssdParameter workRec = new tblThirdPartyUssdParameter();

                workRec.ProductID = pProductID;
                workRec.FunctionName = pFunctionName;
                workRec.VCSTerminalID = pVCSTerminalID;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertThirdPartyUssdParameter - " + Ex.Message);
            }
        }*/

        public tblThirdPartyProduct GetThirdPartyProduct(Int32 pThirdPartyID, String pUssdFunction)
        {
            try
            {
                recordFound = false;
                tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
                crud.Where("UssdFunction", DAL.General.Operator.Equals, pUssdFunction);
                crud.And("ThirdPartyID", DAL.General.Operator.Equals, pThirdPartyID);

                tblThirdPartyProduct search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetThirdPartyProduct - " + Ex.Message);
            }
        }

        public Byte InsertThirdPartyProductType(String pThirdPartyProductType)
        {
            try
            {
                tblThirdPartyProductTypeCrud crud = new tblThirdPartyProductTypeCrud();
                tblThirdPartyProductType type = new tblThirdPartyProductType();
                type.ProductType = pThirdPartyProductType;
                long ID = 0;
                crud.Insert(type, out ID);
                return (Byte)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertThirdPartyProductType - " + Ex.Message);
            }
        }
        public Int32 InsertThirdPartyProduct(Int32 pThirdPartyID, Int32 pProductTypeID, String pProduct, String pUssdFunction, Decimal pAmount, Boolean pApplyFormula, String pFormula)
        {
            try
            {
                tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
                tblThirdPartyProduct product = new tblThirdPartyProduct();
                product.Amount = pAmount;
                product.Product = pProduct;
                product.ProductTypeID = Convert.ToByte(pProductTypeID);
                product.ThirdPartyID = pThirdPartyID;
                product.UssdFunction = pUssdFunction;
                product.ApplyFormula = pApplyFormula;
                product.Formula = pFormula;
                long ID = 0;
                crud.Insert(product, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("InsertThirdPartyProduct", "(Int32 pThirdPartyID, Int32 pProductTypeID, String pProduct, String pUssdFunction, Decimal pAmount)", false);
                throw new Exception("InsertThirdPartyProduct - " + Ex.Message);
            }
        }

        public List<tblThirdPartyUssdMessage> GetThirdPartyUssdMessage(Int32 pUssdParameterID)
        {
            try
            {
                tblThirdPartyUssdMessageCrud crud = new tblThirdPartyUssdMessageCrud();
                crud.Where("UssdParameterID", DAL.General.Operator.Equals, pUssdParameterID);
                recordFound = false;
                List<tblThirdPartyUssdMessage> results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return results;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetThirdPartyUssdMessage - " + Ex.Message);
            }

        }
        public tblThirdPartyUssdParameter GetThirdPartyUssdParameterByProductID(Int32 pProductID)
        {
            try
            {
                tblThirdPartyUssdParameterCrud crud = new tblThirdPartyUssdParameterCrud();
                crud.Where("ProductID", DAL.General.Operator.Equals, pProductID);
                recordFound = false;
                tblThirdPartyUssdParameter par = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return par;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetThirdPartyUssdParameter(Int32)-" + Ex.Message);
            }
        }
        public tblThirdPartyUssdMessage GetThirdPartyUssdMessage(Int32 pUssdParameterID, Byte pMessageType)
        {
            try
            {
                tblThirdPartyUssdMessageCrud crud = new tblThirdPartyUssdMessageCrud();
                crud.Where("UssdParameterID", DAL.General.Operator.Equals, pUssdParameterID);
                recordFound = false;
                List<tblThirdPartyUssdMessage> results = crud.ReadMulti().ToList();
                results = results.FindAll(x => x.tblUssdVendingMessage.VendingMeddageTypeID == pMessageType);
                if (results.Count > 0)
                {
                    recordFound = true;
                    return results[0];
                }

                return null;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetThirdPartyUssdMessage - " + Ex.Message); 
            }

        }

        /*public List<tblThirdPartyUssdMessage> GetThirdPartyUssdMessage(Int32 pUssdParameterID)
        {
            try
            {
                tblThirdPartyUssdMessageCrud crud = new tblThirdPartyUssdMessageCrud();
                crud.Where("UssdParameterID", DAL.General.Operator.Equals, pUssdParameterID);
                recordFound = false;
                List<tblThirdPartyUssdMessage> results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return results;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetThirdPartyUssdMessage - " + Ex.Message);
            }

        }*/

        public List<tblThirdPartyUssdInstruction> GetThirdPartyUssdInstruction(Int32 pUssdParameterID, Int32 pInstructionID)
        {
            try
            {
                tblThirdPartyUssdInstructionCrud crud = new tblThirdPartyUssdInstructionCrud();
                crud.Where("UssdParameterID", DAL.General.Operator.Equals, pUssdParameterID);
                crud.And("InstrunctionID", DAL.General.Operator.Equals, pInstructionID);
                recordFound = false;
                List<tblThirdPartyUssdInstruction> result = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetThirdPartyUssdInstruction - " + Ex.Message);
            }
        }

        public List<tblThirdPartyUssdInstruction> GetThirdPartyUssdInstruction(Int32 pUssdParameterID)
        {
            try
            {
                tblThirdPartyUssdInstructionCrud crud = new tblThirdPartyUssdInstructionCrud();
                crud.Where("UssdParameterID", DAL.General.Operator.Equals, pUssdParameterID);
                recordFound = false;
                List<tblThirdPartyUssdInstruction> result = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetThirdPartyUssdInstruction - " + Ex.Message);
            }
        }
		public List<tblThirdPartyUssdParameter> GetThirdPartyUssdParameterByTransactionDescription(String pTransactionDesciption )
		{
			try
			{
				tblThirdPartyUssdParameterCrud crud = new tblThirdPartyUssdParameterCrud();
				crud.Where("TransactionDescription", DAL.General.Operator.Like, "%" + pTransactionDesciption + "%");
				recordFound = false;
				List<tblThirdPartyUssdParameter> result = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return result;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetThirdPartyUssdParameterByTransactionDescription - " + Ex.Message);
			}
		}
		public Boolean UpdateThirdPartyProduct(String pUssdFunction, Decimal pAmount)
        {
            try
            {
                UpdateTempMethod("UpdateThirdPartyProduct", "(String pUssdFunction, Decimal pAmount)", true);
				tblThirdPartyProduct prod = GetThirdPartyProduct(pUssdFunction);
				prod.Amount = pAmount;
				tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
				crud.Update(prod);
				return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("UpdateThirdPartyProduct", "(String pUssdFunction, Decimal pAmount)", false);
                throw new Exception("UpdateThirdPartyProduct - " + Ex.Message);
            }
        }
		public Boolean DoThirdPartyCallback(String pUserID, String pReferenceNumber)
		{
			tblApplicationSubApp subApp = GetSubApplicationByName("SettleCallback");
			Int32 applicationID = Convert.ToInt32(subApp.tblApplication.ApplicationID);
			Int32 subApplicationID = Convert.ToInt32(subApp.SubApplicationID);
			tblThirdPartyVending vending = new tblThirdPartyVending();
			tblThirdPartyVendingCrud crudVending = new tblThirdPartyVendingCrud();
			Boolean updateMatchMessage = true;

			try
			{
                UpdateTempMethod("DoThirdPartyCallback", "(String pUserID, String pReferenceNumber)", true);
				
				Callbacks callback = vvoBL.GetCallbacks(pUserID, pReferenceNumber);
				if (vvoBL.RecordFound)
				{
					Boolean callbackSuccess = true;
					String informationLog = "";
					String mPars = callback.ParameterString;
					String failReason = "Incorrect Paramaters Passed";
					mPars = System.Net.WebUtility.UrlDecode(mPars);
					String[] mPar = mPars.Split('&');

					String userID = mPar[0].Split('=')[1];
					informationLog = "UserID=" + userID;
					String referenceNumber = mPar[1].Split('=')[1];
					informationLog += "| ReferenceNumbe=" + referenceNumber;
					String ussdIndicator = mPar[11].Split('=')[1];
					Decimal transactionAmount = Convert.ToDecimal(mPar[4].Split('=')[1]);
					informationLog += "| Amount=" + transactionAmount.ToString();
					Boolean ussdTransaction = true;
					Boolean alreadyMatched = true;
					String bankResponse = "";

					if (userID.Trim() == "")
					{
						failReason = "UserID was not passed.";
						informationLog += "| " + failReason;
						callbackSuccess = false;
						goto callbackDone;
					}


					if (referenceNumber.Trim() == "")
					{
						failReason = "Reference number was not passed.";
						informationLog += "| " + failReason;
						callbackSuccess = false;
						goto callbackDone;
					}

					if (referenceNumber.Length != 14)
					{
						failReason = "Reference number is not 14 characters long.";
						informationLog += "| " + failReason;
						callbackSuccess = false;
						goto callbackDone;
					}

					if (ussdIndicator != "VCS.VirtualMessage.Ussd")
					{
						failReason = "Not a ussd transaction.";
						informationLog += "| " + failReason;
						callbackSuccess = false;
						ussdTransaction = false;
						goto callbackDone;
					}

					vending = GetThirdPartyVending(userID, referenceNumber);
					if (!RecordFound)
					{
						failReason = "Vending record was not found";
						informationLog += "| " + failReason;
						callbackSuccess = false;
						goto callbackDone;
					}

					if (Convert.ToBoolean(vending.VendingMatched))
					{
						failReason = "Record was already matched On " + Convert.ToDateTime(vending.MatchDateTime).ToString("dd-MM-yyyy HH:mm:ss") + ".";
						informationLog += "| " + failReason;
						alreadyMatched = true;
						callbackSuccess = false;
						goto callbackDone;
					}

					if (Convert.ToDecimal(vending.Amount) != transactionAmount)
					{
						failReason = "Amount discrepancy. VendingAmount=" + transactionAmount + " TransactionAmount=" + vending.Amount.ToString() + ".";
						informationLog += "| " + failReason;
						callbackSuccess = false;
						goto callbackDone;
					}

					Transactions tran = vvoBL.GetTransactions(userID, referenceNumber);
					if (!vvoBL.RecordFound)
					{
						failReason = "Transaction record was not found.";
						informationLog += "| " + failReason;
						callbackSuccess = false;
						goto callbackDone;
					}

					if (tran.Response.Substring(4, 2) != "07")
					{
						bankResponse = tran.Response;
						if (bankResponse.Length > 100)
							bankResponse = bankResponse.Substring(5, 100);
						else
							bankResponse = bankResponse.Substring(5);

						failReason = "Transaction was not approved.";
						informationLog += "| " + failReason;
						callbackSuccess = false;
						goto callbackDone;
					}

					if (tran.Settled == null)
					{
						bankResponse = tran.Response;
						failReason = "Transaction was not settled.";
						informationLog += "| " + failReason;
						callbackSuccess = false;
						goto callbackDone;
					}

					bankResponse = tran.Response;
					informationLog += "| BankResponse=" + bankResponse;
					if (callbackSuccess)
					{
						if (!ThirdPartySettle(userID, referenceNumber, bankResponse, transactionAmount))
						{
							callbackSuccess = false;
							tblThirdPartyVending refundVending = GetThirdPartyVending(userID, referenceNumber);
							if (refundVending.MatchMessage != null)
							{
								updateMatchMessage = false;
								vending.MatchMessage = refundVending.MatchMessage;
							}
						}
						else
							vending = GetThirdPartyVending(userID, referenceNumber);
					}
					callbackDone:;
					WriteLog(applicationID, subApplicationID, informationLog, eLogType.Information);
					if (!callbackSuccess)
					{
						if (vending != null)
						{
							if (!alreadyMatched)
							{
								vending.MatchDateTime = DateTime.Now;
								vending.MatchSuccess = false;
								if (updateMatchMessage)
									vending.MatchMessage = failReason;
								vending.VendingMatched = true;
								crudVending.Update(vending);
							}
						}
					}
					else
					{
						if (ussdTransaction)
						{
							vending.MatchDateTime = DateTime.Now;
							vending.MatchSuccess = true;
							vending.MatchMessage = "Matched";
							vending.VendingMatched = true;
							crudVending.Update(vending);
						}
					}
				}
				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("DoThirdPartyCallback", "(String pUserID, String pReferenceNumber)", false);
				WriteLog(applicationID, subApplicationID, "DoThirdPartyCallback-" + Ex.Message, eLogType.Error);
				throw new Exception(errorMessage + "DoThirdPartyCallback(String, String)-" + Ex.Message);
			}
		}
		public Boolean DoThirdPartyCallback(String pUserID)
		{
			try
			{
                UpdateTempMethod("DoThirdPartyCallback", "(String pUserID)", true);
				List<tblThirdPartyVending> vendings = GetThirdPartyVendingUnmatched(pUserID);
				foreach (tblThirdPartyVending vending in vendings)
					DoThirdPartyCallback(pUserID, vending.ReferenceNumber);

				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("DoThirdPartyCallback", "(String pUserID)", false);
				throw new Exception(errorMessage + "DoThirdPartyCallback(String)-" + Ex.Message);
			}
		}
		public List<tblThirdPartyVending> GetThirdPartyVendingUnmatched(String pUserID)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyVendingUnmatched", "(String pUserID)", true);
                recordFound = false;
                tblThirdPartyVendingCrud crud = new tblThirdPartyVendingCrud();
                crud.Where("UserID", DAL.General.Operator.Equals, pUserID);

                List<tblThirdPartyVending> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyVendingUnmatched", "(String pUserID)", false);
                throw new Exception("GetThirdPartyVendingUnmatched - " + Ex.Message);
            }
        }
		/*public tblThirdPartyUssdParameter GetThirdPartyUssdParameterByProductID(Int32 pProductID)
		{
			try
			{
                tblThirdPartyUssdParameterCrud crud = new tblThirdPartyUssdParameterCrud();
                crud.Where("ProductID", DAL.General.Operator.Equals, pProductID);
                recordFound = false;
                tblThirdPartyUssdParameter par = crud.ReadSingle();
                recordFound = crud.RecordFound;
				return par;
			}
			catch (Exception Ex)
			{
				throw new Exception(errorMessage + "GetThirdPartyUssdParameter(Int32)-" + Ex.Message);
			}
		}*/
		public List<tblThirdPartyUssdParameter> GetThirdPartyUssdParameter()
        {
            try
            {
                UpdateTempMethod("GetThirdPartyUssdParameter", "()", true);
                recordFound = false;
                tblThirdPartyUssdParameterCrud crud = new tblThirdPartyUssdParameterCrud();
                List<tblThirdPartyUssdParameter> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyUssdParameter", "()", false);
                throw new Exception("GetThirdPartyUssdParameter - " + Ex.Message);
            }
        }
		public String GetThirdPartyUssdParameterGridData()
		{
			try
			{
                UpdateTempMethod("GetThirdPartyUssdParameterGridData", "()", true);
				List<tblThirdPartyUssdParameter> pars = GetThirdPartyUssdParameter();
				String divString = "";
				foreach (tblThirdPartyUssdParameter par in pars)
				{
					divString += par.UssdParameterID.ToString() + "═";
					divString += par.tblThirdPartyProduct.ThirdPartyID.ToString() + "═";
					divString += par.ProductID.ToString() + "═";
					divString += par.FunctionName + "═";
					divString += Convert.ToBoolean(par.CheckSmartCard) == true ? "Yes═" : "No═";
					divString += Convert.ToBoolean(par.MultiChoiceAfricaTransaction) == true ? "Yes═" : "No═";
					divString += Convert.ToBoolean(par.VerifyCardNumber) == true ? "Yes═" : "No═";
					divString += Convert.ToBoolean(par.ExtractAmount) == true ? "Yes═" : "No═";
					divString += Convert.ToBoolean(par.DoAuthorisation) == true ? "Yes═" : "No═";
					divString += par.TransactionDescription + "═";
					divString += Convert.ToBoolean(par.GetMultiChoiceCustomerNumber) == true ? "Yes═" : "No═";
					divString += Convert.ToBoolean(par.DoMultiChoicePayment) == true ? "Yes═" : "No═";
					divString += par.MultiChoicePaymentDescription + "═";
					divString += Convert.ToBoolean(par.GetMultiChoiceSmartCardNumber) == true ? "Yes═" : "No═";
					divString += par.VCSTerminalID + "═";
					divString += par.ResponseToCustomer + "═";
					divString += par.ServiceDownMessage + "═";
					divString += Convert.ToBoolean(par.ExtractMeterNumber) == true ? "Yes═" : "No═";
					divString += Convert.ToBoolean(par.ExtractRecipient) == true ? "Yes═" : "No═";
					divString += Convert.ToBoolean(par.IssueDataVoucher) == true ? "Yes═" : "No═";
					divString += Convert.ToBoolean(par.ExtractPin) == true ? "Yes═" : "No═";
					divString += Convert.ToBoolean(par.GetVirtualAccountCard) == true ? "Yes═" : "No═";
					divString += Convert.ToBoolean(par.IssueFromGlocell) == true ? "Yes═" : "No═";
					divString += Convert.ToBoolean(par.ExtractBankResponseCode) == true ? "Yes═" : "No═";
					divString += Convert.ToBoolean(par.CheckVirtualVoucherAvailable) == true ? "Yes║" : "No║";
				}

				if (divString.Length > 0)
					divString = divString.Substring(0, divString.Length - 1);

				return divString;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetThirdPartyUssdParameterGridData", "()", false);
				throw new Exception(errorMessage + "GetThirdPartyUssdParameterGridData-" + Ex.Message);
			}
		}
		public Boolean DeleteThirdPartyUssdParameterByThirdPartyID(Int32 pThirdPartyID)
		{
			try
			{
    //            UpdateTempMethod("DeleteThirdPartyUssdParameterByThirdPartyID", "(Int32 pThirdPartyID)", true);
				//List<tblThirdPartyUssdParameter> pars = GetThirdPartyUssdParameter(pThirdPartyID);
				//tblThirdPartyUssdParameterCrud crud = new tblThirdPartyUssdParameterCrud();
				//foreach (tblThirdPartyUssdParameter par in pars)
				//	crud.Delete(par);


				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("DeleteThirdPartyUssdParameterByThirdPartyID", "(Int32 pThirdPartyID)", false);
				throw new Exception(errorMessage + "DeleteThirdPartyUssdParameterByThirdPartyID-" + Ex.Message);
			}
		}
		public Boolean DeleteThirdPartyUssdParameter(Int32 pUssdParameterID)
        {
            try
            {
                UpdateTempMethod("DeleteThirdPartyUssdParameter", "(Int32 pUssdParameterID)", true);
                tblThirdPartyUssdParameterCrud crud = new tblThirdPartyUssdParameterCrud();
                crud.Where("UssdParameterID", DAL.General.Operator.Equals, pUssdParameterID);

                List<tblThirdPartyUssdParameter> search = crud.ReadMulti().ToList();
                foreach (tblThirdPartyUssdParameter del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteThirdPartyUssdParameter", "(Int32 pUssdParameterID)", false);
                throw new Exception("DeleteThirdPartyUssdParameter - " + Ex.Message);
            }
        }
		public tblThirdPartyUssdParameter GetThirdPartyUssdParameter(String pFunctionName)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyUssdParameter", "(String pFunctionName)", true);
                recordFound = false;
                tblThirdPartyUssdParameterCrud crud = new tblThirdPartyUssdParameterCrud();
                crud.Where("FunctionName", DAL.General.Operator.Equals, pFunctionName);

                tblThirdPartyUssdParameter search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyUssdParameter", "(String pFunctionName)", false);
                throw new Exception("GetThirdPartyUssdParameter - " + Ex.Message);
            }
        }
        public Int32 InsertThirdPartyUssdParameter(Int32 pProductID, String pFunctionName, String pVCSTerminalID)
        {
            try
            {
                tblThirdPartyUssdParameterCrud crud = new tblThirdPartyUssdParameterCrud();
                tblThirdPartyUssdParameter workRec = new tblThirdPartyUssdParameter();

                workRec.ProductID = pProductID;
                workRec.FunctionName = pFunctionName;
                workRec.VCSTerminalID = pVCSTerminalID;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertThirdPartyUssdParameter - " + Ex.Message);
            }
        }
        public Int32 InsertThirdPartyUssdParameter(Int32 pProductID, String pFunctionName, Boolean pCheckSmartCard, Boolean pMultiChoiceAfricaTransaction, Boolean pVerifyCardNumber, Boolean pExtractAmount, Boolean pDoAuthorisation, String pTransactionDescription, Boolean pGetMultiChoiceCustomerNumber, Boolean pDoMultiChoicePayment, String pMutliChoiceProductUserKey, String pMultiChoiceMethodOfPayment, String pMultiChoicePaymentDescription, Boolean pGetMultiChoiceSmartCardNumber, String pVCSTerminalID, String pResponseToCustomer, String pServiceDownMessage, Boolean pExtractMeterNumber, Boolean pExtractRecipient, Boolean pIssueDataVoucher, Boolean pExtractPin, Boolean pGetVirtualAccountCard, Boolean pIssueFromGlocell, Boolean pExtractBankResponseCode, Boolean pCheckVirtualVoucherAvailable, Boolean pIssueFromConcerotel, Boolean pExtractInvoiceNumber, Boolean pIssueFromGloCell, Boolean pIssueFromCarma, Boolean  pIssueFromTswanaGas) 
        {
			try
			{
				tblThirdPartyUssdParameterCrud crud = new tblThirdPartyUssdParameterCrud();
				tblThirdPartyUssdParameter workRec = new tblThirdPartyUssdParameter();

				workRec.ProductID = pProductID;
				workRec.FunctionName = pFunctionName;
				workRec.CheckSmartCard = pCheckSmartCard;
				workRec.MultiChoiceAfricaTransaction = pMultiChoiceAfricaTransaction;
				workRec.VerifyCardNumber = pVerifyCardNumber;
				workRec.ExtractAmount = pExtractAmount;
				workRec.DoAuthorisation = pDoAuthorisation;
				workRec.TransactionDescription = pTransactionDescription;
				workRec.GetMultiChoiceCustomerNumber = pGetMultiChoiceCustomerNumber;
				workRec.DoMultiChoicePayment = pDoMultiChoicePayment;
				workRec.MutliChoiceProductUserKey = pMutliChoiceProductUserKey;
				workRec.MultiChoiceMethodOfPayment = pMultiChoiceMethodOfPayment;
				workRec.MultiChoicePaymentDescription = pMultiChoicePaymentDescription;
				workRec.GetMultiChoiceSmartCardNumber = pGetMultiChoiceSmartCardNumber;
				workRec.VCSTerminalID = pVCSTerminalID;
				workRec.ResponseToCustomer = pResponseToCustomer;
				workRec.ServiceDownMessage = pServiceDownMessage;
				workRec.ExtractMeterNumber = pExtractMeterNumber;
				workRec.ExtractRecipient = pExtractRecipient;
				workRec.IssueDataVoucher = pIssueDataVoucher;
				workRec.ExtractPin = pExtractPin;
				workRec.GetVirtualAccountCard = pGetVirtualAccountCard;
				workRec.IssueFromGlocell = pIssueFromGlocell;
				workRec.ExtractBankResponseCode = pExtractBankResponseCode;
				workRec.CheckVirtualVoucherAvailable = pCheckVirtualVoucherAvailable;
				workRec.IssueFromConcerotel = pIssueFromConcerotel;
				workRec.ExtractInvoiceNumber = pExtractInvoiceNumber;
				workRec.IssueFromGlocell = pIssueFromGloCell;
				workRec.IssueFromCarma = pIssueFromCarma;
				workRec.IssueFromTswanaGas = pIssueFromTswanaGas;
				long ID = 0;
				crud.Insert(workRec, out ID);
				return (Int32)ID;

			}
			catch (Exception Ex)
			{
				throw new Exception("InsertThirdPartyUssdParameter - " + Ex.Message);
			}
        }
		public String GetThirdPartyProductGridData(Int32 pThirdPartyID)
        {
            try
            {
                tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
                List<tblThirdPartyProduct> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (tblThirdPartyProduct workRec in search)
                {
                    divString += workRec.ProductID + "═";
                    divString += workRec.ThirdPartyID + "═";
                    divString += workRec.ProductTypeID + "═";
                    divString += workRec.Product + "═";
                    divString += workRec.UssdFunction + "═";
                    divString += Convert.ToDecimal(workRec.Amount).ToString("N2") + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetThirdPartyProductGridData - " + Ex.Message);
            }
        }
		public String GetThirdPartyProductGridData()
        {
            try
            {
                tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
                List<tblThirdPartyProduct> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (tblThirdPartyProduct workRec in search)
                {
                    divString += workRec.ProductID + "═";
                    divString += workRec.ThirdPartyID + "═";
                    divString += workRec.ProductTypeID + "═";
                    divString += workRec.Product + "═";
                    divString += workRec.UssdFunction + "═";
                    divString +=Convert.ToDecimal(workRec.Amount).ToString("N2") + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetThirdPartyProductGridData - " + Ex.Message);
            }
        }
		public Boolean DeleteThirdParty(Int32 pThirdPartyID)
        {
            try
            {
                tblThirdPartyCrud crud = new tblThirdPartyCrud();
                crud.Where("ThirdPartyID", DAL.General.Operator.Equals, pThirdPartyID);

                List<tblThirdParty> search = crud.ReadMulti().ToList();
                foreach (tblThirdParty del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteThirdParty", "(Int32 pThirdPartyID)", false);
                throw new Exception("DeleteThirdParty - " + Ex.Message);
            }
        }
		public Int32 SaveThirdParty(String pThirdParty)
        {
            try
            {
                UpdateTempMethod("SaveThirdParty", "(String pThirdParty)", true);
                tblThirdPartyCrud crud = new tblThirdPartyCrud();
                tblThirdParty party = new tblThirdParty();
				party.DownCount = 0;
				party.ServiceDown = false;
				party.SuspendThirdParty = false;
				party.ThirdParty = pThirdParty;
				party.UniqueReferenceNo = "0";
				long ID = 0;
				crud.Insert(party, out ID);
				return (Int32)ID;

            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveThirdParty", "(String pThirdParty)", false);
                throw new Exception("SaveThirdParty - " + Ex.Message);
            }
        }
		public Boolean UpdateThirdParty(Int32 pThirdPartyID, String pThirdPartyName)
        {
            try
            {
                UpdateTempMethod("UpdateThirdParty", "(Int32 pThirdPartyID, String pThirdPartyName)", true);
				tblThirdPartyCrud crud = new tblThirdPartyCrud();
				tblThirdParty party = GetThirdParty(pThirdPartyID);
				party.ThirdParty = pThirdPartyName;
				crud.Update(party);
				return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("UpdateThirdParty", "(Int32 pThirdPartyID, String pThirdPartyName)", false);
                throw new Exception("UpdateThirdParty - " + Ex.Message);
            }
        }
		public Boolean DeleteThirdPartyProductType(short pProductTypeID)
        {
            try
            {
                UpdateTempMethod("DeleteThirdPartyProductType", "(short pProductTypeID)", true);
                tblThirdPartyProductTypeCrud crud = new tblThirdPartyProductTypeCrud();
                crud.Where("ProductTypeID", DAL.General.Operator.Equals, pProductTypeID);

                List<tblThirdPartyProductType> search = crud.ReadMulti().ToList();
                foreach (tblThirdPartyProductType del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteThirdPartyProductType", "(short pProductTypeID)", false);
                throw new Exception("DeleteThirdPartyProductTypeshort - " + Ex.Message);
            }
        }
		public Int32 SaveThirdPartyProductType(short pProductTypeID, String pProductType)
        {
            try
            {
                UpdateTempMethod("SaveThirdPartyProductType", "(short pProductTypeID, String pProductType)", true);
				tblThirdPartyProductTypeCrud crud = new tblThirdPartyProductTypeCrud();
				tblThirdPartyProductType productType = GetThirdPartyProductType(pProductTypeID);
				productType.ProductType = pProductType;
				crud.Update(productType);
				return (Int32)productType.ProductTypeID;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveThirdPartyProductType", "(short pProductTypeID, String pProductType)", false);
                throw new Exception("SaveThirdPartyProductTypeshort - " + Ex.Message);
            }
        }
		public Int32 SaveThirdPartyProductType(String pProductType)
        {
            try
            {
                UpdateTempMethod("SaveThirdPartyProductType", "(String pProductType)", true);
                tblThirdPartyProductTypeCrud crud = new tblThirdPartyProductTypeCrud();
                tblThirdPartyProductType workRec = new tblThirdPartyProductType();
				workRec.ProductType = pProductType;
				long ID = 0;
				crud.Insert(workRec, out ID);
				return (Int32)ID;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveThirdPartyProductType", "(String pProductType)", false);
                throw new Exception("SaveThirdPartyProductType - " + Ex.Message);
            }
        }
		public String GetThirdPartyProductTypeGridData()
        {
            try
            {
                UpdateTempMethod("GetThirdPartyProductTypeGridData", "()", true);
                tblThirdPartyProductTypeCrud crud = new tblThirdPartyProductTypeCrud();
                crud.OrderBy(x => x.ProductType);
                List<tblThirdPartyProductType> search = crud.ReadMulti().ToList();
                String divString = "";
                foreach (tblThirdPartyProductType workRec in search)
                {
                    divString += workRec.ProductTypeID.ToString() + "═";
                    divString += workRec.ProductType + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyProductTypeGridData", "()", false);
                throw new Exception("GetThirdPartyProductTypeGridData - " + Ex.Message);
            }
        }



		public tblThirdPartyProductType GetThirdPartyProductType(short pProductTypeID)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyProductType", "(short pProductTypeID)", true);
                recordFound = false;
                tblThirdPartyProductTypeCrud crud = new tblThirdPartyProductTypeCrud();
                crud.Where("ProductTypeID", DAL.General.Operator.Equals, pProductTypeID);

                tblThirdPartyProductType search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyProductType", "(short pProductTypeID)", false);
                throw new Exception("GetThirdPartyProductTypeshort - " + Ex.Message);
            }
        }
		public tblThirdPartyProductType GetThirdPartyProductType(String pProductType)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyProductType", "(String pProductType)", true);
                recordFound = false;
                tblThirdPartyProductTypeCrud crud = new tblThirdPartyProductTypeCrud();
                crud.Where("ProductType", DAL.General.Operator.Equals, pProductType);

                tblThirdPartyProductType search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyProductType", "(String pProductType)", false);
                throw new Exception("GetThirdPartyProductType - " + Ex.Message);
            }
        }
		public Boolean DeleteThirdPartyFunction(Int32 pThirdPartyID)
        {
            try
            {
                UpdateTempMethod("DeleteThirdPartyFunction", "(Int32 pThirdPartyID)", true);
                tblThirdPartyFunctionCrud crud = new tblThirdPartyFunctionCrud();
                crud.Where("ThirdPartyID", DAL.General.Operator.Equals, pThirdPartyID);

                List<tblThirdPartyFunction> search = crud.ReadMulti().ToList();
                foreach (tblThirdPartyFunction del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteThirdPartyFunction", "(Int32 pThirdPartyID)", false);
                throw new Exception("DeleteThirdPartyFunction - " + Ex.Message);
            }
        }

		public Int32 InsertThirdParty(String pThirdPartyName, String pUserID)
		{
			try
			{
				tblThirdParty party = new tblThirdParty();
				party.DownCount = 0;
				party.ServiceDown = false;
				party.SuspendThirdParty = false;
				party.ThirdParty = pThirdPartyName;
				party.UniqueReferenceNo = "0";
				party.UseNormalCallback = true;
				party.UserID = pUserID;
				tblThirdPartyCrud crud = new tblThirdPartyCrud();
				long ID = 0;
				crud.Insert(party, out ID);
				return (Int32)ID;

			}
			catch (Exception Ex)
			{
				throw new Exception("InsertThirdParty - " + Ex.Message);
			}
		}
		public Int32 InsertThirdPartyFunction(Int32 pThirdPartyID, String pThirdPartyProduct, String pUssdFunction)
        {
            try
            {
				tblThirdPartyFunction func = new tblThirdPartyFunction();
				func.ThirdPartyID = pThirdPartyID;
				func.ThirdPartyProduct = pThirdPartyProduct;
				func.UssdFunction = pUssdFunction;
				tblThirdPartyFunctionCrud crud = new tblThirdPartyFunctionCrud();
				long id = 0;
				crud.Insert(func, out id);

				return (Int32)id;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertThirdPartyFunction - " + Ex.Message);
            }
        }
		public Boolean DeleteThirdPartyVending(Int32 pThirdPartyID)
		{
			try
			{
                UpdateTempMethod("DeleteThirdPartyVending", "(Int32 pThirdPartyID)", true);
				tblThirdPartyVendingCrud crud = new tblThirdPartyVendingCrud();
				List<tblThirdPartyVending> vending = GetThirdPartyVending(pThirdPartyID);
				foreach (tblThirdPartyVending ven in vending)
					crud.Delete(ven);

				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("DeleteThirdPartyVending", "(Int32 pThirdPartyID)", false);
				throw new Exception(errorMessage + "DeleteThirdPartyVending-" + Ex.Message);
			}
		}
		public List<tblThirdPartyProductType> GetThirdPartyProductType()
        {
            try
            {
                UpdateTempMethod("GetThirdPartyProductType", "()", true);
                recordFound = false;
                tblThirdPartyProductTypeCrud crud = new tblThirdPartyProductTypeCrud();
                List<tblThirdPartyProductType> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyProductType", "()", false);
                throw new Exception("GetThirdPartyProductType - " + Ex.Message);
            }
        }
		public Boolean DeleteThirdPartyDownReason(short pDownReasonID)
        {
            try
            {
                UpdateTempMethod("DeleteThirdPartyDownReason", "(short pDownReasonID)", true);
                tblThirdPartyDownReasonCrud crud = new tblThirdPartyDownReasonCrud();
                crud.Where("DownReasonID", DAL.General.Operator.Equals, pDownReasonID);

                List<tblThirdPartyDownReason> search = crud.ReadMulti().ToList();
                foreach (tblThirdPartyDownReason del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteThirdPartyDownReason", "(short pDownReasonID)", false);
                throw new Exception("DeleteThirdPartyDownReasonshort - " + Ex.Message);
            }
        }
		public Int32 SaveThirdPartyDownReason(short pDownReasonID, String pDownReason)
        {
            try
            {
                UpdateTempMethod("SaveThirdPartyDownReason", "(short pDownReasonID, String pDownReason)", true);
				tblThirdPartyDownReasonCrud crud = new tblThirdPartyDownReasonCrud();
				tblThirdPartyDownReason reason = GetThirdPartyDownReason(pDownReasonID);
				reason.DownReason = pDownReason;
				crud.Update(reason);
				return (Int32)reason.DownReasonID;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveThirdPartyDownReason", "(short pDownReasonID, String pDownReason)", false);
                throw new Exception("SaveThirdPartyDownReasonshort - " + Ex.Message);
            }
        }
		public Int32 SaveThirdPartyDownReason(String pDownReasonID)
        {
            try
            {
                UpdateTempMethod("SaveThirdPartyDownReason", "(String pDownReasonID)", true);
                tblThirdPartyDownReasonCrud crud = new tblThirdPartyDownReasonCrud();
                tblThirdPartyDownReason workRec = new tblThirdPartyDownReason();
				workRec.DownReason = pDownReasonID;
				long ID = 0;
				crud.Insert(workRec, out ID);
				return (Int32)ID;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SaveThirdPartyDownReason", "(String pDownReasonID)", false);
                throw new Exception("SaveThirdPartyDownReason - " + Ex.Message);
            }
        }
		public tblThirdPartyDownReason GetThirdPartyDownReason(short pDownReasonID)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyDownReason", "(short pDownReasonID)", true);
                recordFound = false;
                tblThirdPartyDownReasonCrud crud = new tblThirdPartyDownReasonCrud();
                crud.Where("DownReasonID", DAL.General.Operator.Equals, pDownReasonID);

                tblThirdPartyDownReason search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyDownReason", "(short pDownReasonID)", false);
                throw new Exception("GetThirdPartyDownReasonshort - " + Ex.Message);
            }
        }
		public List<tblThirdPartyDownReason> GetThirdPartyDownReason()
        {
            try
            {
                UpdateTempMethod("GetThirdPartyDownReason", "()", true);
                recordFound = false;
                tblThirdPartyDownReasonCrud crud = new tblThirdPartyDownReasonCrud();
                List<tblThirdPartyDownReason> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyDownReason", "()", false);
                throw new Exception("GetThirdPartyDownReason - " + Ex.Message);
            }
        }
		public String GetThirdPartyGridData()
		{
			try
			{
                UpdateTempMethod("GetThirdPartyGridData", "()", true);
				List<tblThirdParty> parties = GetThirdParty();
				parties = parties.OrderBy(x => x.ThirdParty).ToList();
				String divString = "";
				foreach (tblThirdParty party in parties)
				{
					divString += party.ThirdPartyID + "═";
					divString += party.ThirdParty + "═";
					String down = "No";
					if (Convert.ToBoolean(party.ServiceDown))
						down = "Yes";
					divString += down + "═";
					String suspended = "No";
					if (Convert.ToBoolean(party.SuspendThirdParty))
						suspended = "Yes";
					divString += suspended + "║";
				}

				if (divString.Length > 0)
					divString = divString.Substring(0, divString.Length - 1);

				return divString;

			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetThirdPartyGridData", "()", false);
				throw new Exception(errorMessage + "GetThirdPartyGridData-" + Ex.Message);
			}
		}
		public Boolean ThirdPartySettle(String pUserID, String pReferenceNumber, String pBankResponse, Decimal pAmount)
        {
            try
            {
                UpdateTempMethod("ThirdPartySettle", "(String pUserID, String pReferenceNumber, String pBankResponse, Decimal pAmount)", true);
				tblThirdPartyVendingCrud crudVending = new tblThirdPartyVendingCrud();
				tblThirdPartyVending vending = GetThirdPartyVending(pUserID, pReferenceNumber);
                Boolean refundSuccess = false;

                if (Convert.ToBoolean(vending.VendingSuccess) == false)
                {
                    String ccXmlUauthString = GetSettingByName("ccXmlAuthUrl", Convert.ToInt32(GetApplicationByName("General Settings").ApplicationID)).SettingValue;
                    String cellNumber = vending.Msisdn;
                    String cellMessage = "P" + pAmount.ToString() + "was refunded for unsuccessful transaction: " + vending.tblThirdPartyFunction.ThirdPartyProduct;
                    String refundResult = vvoBL.DoRefund(pUserID, pReferenceNumber, "Unsuccessful third party vending.", pAmount.ToString(), "", cellNumber, cellMessage, ccXmlUauthString);
                    if (refundResult.Substring(6).Trim().ToLower() != "approved")
                    {
                        vending.MatchMessage = "Refund Failed: " + refundResult;
                        String header = "<html>";
                        header += "<body bgcolor=white>";
                        header += "<table class='small' style='border-collapse:collapse'>";
                        header += "<tr><td colspan='2' style='font-weight:bold;font-size:14pt;font-family:verdana;width:500px;border:1px solid #778899;text-align:center;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Third Party Refund Failed</td></tr>";
                        header += "<tr><td style='vertical-align:top;word-wrap:break-word;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Terminal ID</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + pUserID + "</td></tr>";
                        header += "<tr><td style='vertical-align:top;word-wrap:break-word;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Reference NumberCreated</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + pReferenceNumber + "</td></tr>";
                        header += "<tr><td style='vertical-align:top;word-wrap:break-word;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Amount</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + pAmount.ToString("N2") + "</td></tr>";
                        header += "<tr><td style='vertical-align:top;word-wrap:break-word;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Third Party</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + vending.tblThirdPartyFunction.tblThirdParty.ThirdParty + "</td></tr>";
                        header += "<tr><td style='vertical-align:top;word-wrap:break-word;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Third Party Product</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + vending.tblThirdPartyFunction.ThirdPartyProduct + "</td></tr>";
                        header += "<tr><td style='vertical-align:top;word-wrap:break-word;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Client Cell Phone</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + vending.Msisdn + "</td></tr>";
                        header += "<tr><td style='vertical-align:top;word-wrap:break-word;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Failure Reason</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + refundResult + "</td></tr>";
                        header += "</table>";
                        header += "</body>";
                        header += "</html>";
                        messBL.UseMailBodyAsIs = true;
                        messBL.SendMail("Refund Failed", header, "christo@vcs.co.za;support@vcs.co.za", "VCS.VirtualMessage.Ussd.SettleCallback", "bakoffice@vcs.co.za");
                    }
                    else
                    {
                        vending.ClientRefunded = true;
                        refundSuccess = true;
                    }
                    
                }

                vending.BankResponse = pBankResponse;
                vending.ClientRefunded = refundSuccess;
                vending.VendingMatched = true;
                vending.MatchDateTime = DateTime.Now;
				crudVending.Update(vending);


                if (Convert.ToBoolean(vending.VendingSuccess) == false && refundSuccess == false)
                    return false;

                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("ThirdPartySettle", "(String pUserID, String pReferenceNumber, String pBankResponse, Decimal pAmount)", false);
                throw new Exception(errorMessage + "ThirdPartySettle-" + Ex.Message);
            }
        }
		public List<tblThirdPartyVending> GetThirdPartyVendingByFunctionID(Int32 pThirdPartyFunctionID)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyVendingByFunctionID", "(Int32 pThirdPartyFunctionID)", true);
                recordFound = false;
                tblThirdPartyVendingCrud crud = new tblThirdPartyVendingCrud();
                crud.Where("ThirdPartyFunctionID", DAL.General.Operator.Equals, pThirdPartyFunctionID);

                List<tblThirdPartyVending> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyVendingByFunctionID", "(Int32 pThirdPartyFunctionID)", false);
                throw new Exception("GetThirdPartyVendingByFunctionID - " + Ex.Message);
            }
        }
		public List<tblThirdPartyVending> GetThirdPartyVending(Int32 pThirdPartyID)
		{
			try
			{
                UpdateTempMethod("GetThirdPartyVending", "(Int32 pThirdPartyID)", true);
				List<tblThirdPartyFunction> functions = GetThirdPartyFunction(pThirdPartyID);
				List<tblThirdPartyVending> vendingRecs = new List<tblThirdPartyVending>();
				foreach (tblThirdPartyFunction function in functions)
				{
					List<tblThirdPartyVending> vend = GetThirdPartyVendingByFunctionID(Convert.ToInt32(function.ThirdPartyFunctionID));
					foreach (tblThirdPartyVending ven in vend)
						vendingRecs.Add(ven);
				}

				return vendingRecs;
				
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetThirdPartyVending", "(Int32 pThirdPartyID)", false);
				throw new Exception(errorMessage + "GetThirdPartyVending-" + Ex.Message);
			}
			
		}
		public List<tblThirdPartyVending> GetThirdPartyVending()
        {
            try
            {
                UpdateTempMethod("GetThirdPartyVending", "()", true);
                recordFound = false;
                tblThirdPartyVendingCrud crud = new tblThirdPartyVendingCrud();
                List<tblThirdPartyVending> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyVending", "()", false);
                throw new Exception("GetThirdPartyVending - " + Ex.Message);
            }
        }
        public tblThirdPartyVending GetThirdPartyVending(String pUserID, String pReferenceNumber)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyVending", "(String pUserID, String pReferenceNumber)", true);
                recordFound = false;
                tblThirdPartyVendingCrud crud = new tblThirdPartyVendingCrud();
                crud.Where("UserID", DAL.General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", DAL.General.Operator.Equals, pReferenceNumber);

                tblThirdPartyVending search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyVending", "(String pUserID, String pReferenceNumber)", false);
                throw new Exception("GetThirdPartyVending - " + Ex.Message);
            }
        }
		public Boolean DeleteThirdPartyProductByThirdPartyID(Int32 pThirdPartyID)
		{
			try
			{
                UpdateTempMethod("DeleteThirdPartyProductByThirdPartyID", "(Int32 pThirdPartyID)", true);
				tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
				List<tblThirdPartyProduct> products = GetThirdPartyProduct(pThirdPartyID);
				foreach (tblThirdPartyProduct product in products)
					crud.Delete(product);

				return true;
			}
			catch(Exception Ex)
			{
                UpdateTempMethod("DeleteThirdPartyProductByThirdPartyID", "(Int32 pThirdPartyID)", false);
				throw new Exception(errorMessage + "DeleteThirdPartyProductByThirdPartyID-" + Ex.Message);
			}
		}
		public Boolean DeleteThirdPartyProduct(Int32 pProductID)
        {
            try
            {
                UpdateTempMethod("DeleteThirdPartyProduct", "(Int32 pProductID)", true);
                tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
                crud.Where("ProductID", DAL.General.Operator.Equals, pProductID);

                List<tblThirdPartyProduct> search = crud.ReadMulti().ToList();
                foreach (tblThirdPartyProduct del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("DeleteThirdPartyProduct", "(Int32 pProductID)", false);
                throw new Exception("DeleteThirdPartyProduct - " + Ex.Message);
            }
        }
		public Int32 UpdateThirdPartyProduct(Int32 pProductID, Int32 pThirdPartyID, Int32 pProductTypeID, String pProduct, String pUssdFunction, Decimal pAmount)
        {
            try
            {
                UpdateTempMethod("UpdateThirdPartyProduct", "(Int32 pProductID, Int32 pThirdPartyID, Int32 pProductTypeID, String pProduct, String pUssdFunction, Decimal pAmount)", true);
				tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
				tblThirdPartyProduct product = GetThirdPartyProductByID(Convert.ToByte(pProductID));
				product.Amount = pAmount;
				product.Product = pProduct;
				product.ProductTypeID = Convert.ToByte(pProductTypeID);
				product.ThirdPartyID = pThirdPartyID;
				product.UssdFunction = pUssdFunction;
				crud.Update(product);
				return (Int32)product.ProductID;
			}
            catch (Exception Ex)
            {
                UpdateTempMethod("UpdateThirdPartyProduct", "(Int32 pProductID, Int32 pThirdPartyID, Int32 pProductTypeID, String pProduct, String pUssdFunction, Decimal pAmount)", false);
                throw new Exception("UpdateThirdPartyProduct - " + Ex.Message);
            }
        }
		public Int32 InsertThirdPartyProduct(Int32 pThirdPartyID, Int32 pProductTypeID, String pProduct, String pUssdFunction, Decimal pAmount)
        {
            try
            {
                UpdateTempMethod("InsertThirdPartyProduct", "(Int32 pThirdPartyID, Int32 pProductTypeID, String pProduct, String pUssdFunction, Decimal pAmount)", true);
                tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
				tblThirdPartyProduct product = new tblThirdPartyProduct();
				product.Amount = pAmount;
				product.Product = pProduct;
				product.ProductTypeID = Convert.ToByte(pProductTypeID);
				product.ThirdPartyID = pThirdPartyID;
				product.UssdFunction = pUssdFunction;
				long ID = 0;
				crud.Insert(product, out ID);
				return (Int32)ID;
			}
            catch (Exception Ex)
            {
                UpdateTempMethod("InsertThirdPartyProduct", "(Int32 pThirdPartyID, Int32 pProductTypeID, String pProduct, String pUssdFunction, Decimal pAmount)", false);
                throw new Exception("InsertThirdPartyProduct - " + Ex.Message);
            }
        }
        /*public Int32 InsertThirdPartyProduct(Int32 pThirdPartyID, Int32 pProductTypeID, String pProduct, String pUssdFunction, Decimal pAmount, Boolean pApplyFormula, String pFormula)
        {
            try
            {
                tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
                tblThirdPartyProduct product = new tblThirdPartyProduct();
                product.Amount = pAmount;
                product.Product = pProduct;
                product.ProductTypeID = Convert.ToByte(pProductTypeID);
                product.ThirdPartyID = pThirdPartyID;
                product.UssdFunction = pUssdFunction;
                product.ApplyFormula = pApplyFormula;
                product.Formula = pFormula;
                long ID = 0;
                crud.Insert(product, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("InsertThirdPartyProduct", "(Int32 pThirdPartyID, Int32 pProductTypeID, String pProduct, String pUssdFunction, Decimal pAmount)", false);
                throw new Exception("InsertThirdPartyProduct - " + Ex.Message);
            }
        }*/
        public Int32 InsertThirdPartyDownClient(Int32 pDownHistoryID, String pMsidn, String pNotifyMessage)
        {
            try
            {
                UpdateTempMethod("InsertThirdPartyDownClient", "(Int32 pDownHistoryID, String pMsidn, String pNotifyMessage)", true);
                tblThirdPartyDownClientCrud crud = new tblThirdPartyDownClientCrud();
                tblThirdPartyDownClient workRec = new tblThirdPartyDownClient();
                if (pDownHistoryID != 0)
                {
                    crud.Where("DownHistoryID", DAL.General.Operator.Equals, pDownHistoryID);
                    workRec = crud.ReadSingle();
                    workRec.Msisdn = pMsidn;
                    workRec.NotifyMessage = pNotifyMessage;
                    crud.Update(workRec);
                    return pDownHistoryID;
                }
                else
                {
                    workRec.Msisdn = pMsidn;
                    workRec.NotifyMessage = pNotifyMessage;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("InsertThirdPartyDownClient", "(Int32 pDownHistoryID, String pMsidn, String pNotifyMessage)", false);
                throw new Exception("InsertThirdPartyDownClient - " + Ex.Message);
            }
        }
        public tblThirdParty GetThirdPartyByName(String pThirdParty)
        {
            try
            {
                recordFound = false;
                tblThirdPartyCrud crud = new tblThirdPartyCrud();
                crud.Where("ThirdParty", DAL.General.Operator.Equals, pThirdParty);

                tblThirdParty search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetThirdPartyByName - " + Ex.Message);
            }
        }
        public List<tblThirdParty> GetThirdParty()
        {
            try
            {
                UpdateTempMethod("GetThirdParty", "()", true);
                recordFound = false;
                tblThirdPartyCrud crud = new tblThirdPartyCrud();
                List<tblThirdParty> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdParty", "()", false);
                throw new Exception("GetThirdParty - " + Ex.Message);
            }
        }
        public tblThirdParty GetThirdParty(Int32 pThirdPartyID)
        {
            try
            {
                UpdateTempMethod("GetThirdParty", "(Int32 pThirdPartyID)", true);
                recordFound = false;
                tblThirdPartyCrud crud = new tblThirdPartyCrud();
                crud.Where("ThirdPartyID", DAL.General.Operator.Equals, pThirdPartyID);

                tblThirdParty search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdParty", "(Int32 pThirdPartyID)", false);
                throw new Exception("GetThirdParty - " + Ex.Message);
            }
        }
        public List<tblThirdPartyDownClient> GetThirdPartyDownClient(Int32 pDownHistoryID)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyDownClient", "(Int32 pDownHistoryID)", true);
                recordFound = false;
                tblThirdPartyDownClientCrud crud = new tblThirdPartyDownClientCrud();
                crud.Where("DownHistoryID", DAL.General.Operator.Equals, pDownHistoryID);

                List<tblThirdPartyDownClient> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyDownClient", "(Int32 pDownHistoryID)", false);
                throw new Exception("GetThirdPartyDownClient - " + Ex.Message);
            }
        }
		public tblThirdPartyDownClient GetThirdPartyDownClient(Int32 pDownHistoryID, String pMsisdn)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyDownClient", "(Int32 pDownHistoryID, String pMsisdn)", true);
                recordFound = false;
                tblThirdPartyDownClientCrud crud = new tblThirdPartyDownClientCrud();
                crud.Where("DownHistoryID", DAL.General.Operator.Equals, pDownHistoryID);
                crud.And("Msisdn", DAL.General.Operator.Equals, pMsisdn);

                tblThirdPartyDownClient search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyDownClient", "(Int32 pDownHistoryID, String pMsisdn)", false);
                throw new Exception("GetThirdPartyDownClient - " + Ex.Message);
            }
        }
		public List<tblThirdPartyDownHistory> GetThirdPartyDownHistoryAll(Int32 pThirdPartyID)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyDownHistoryAll", "(Int32 pThirdPartyID)", true);
                recordFound = false;
                tblThirdPartyDownHistoryCrud crud = new tblThirdPartyDownHistoryCrud();
                crud.Where("ThirdPartyID", DAL.General.Operator.Equals, pThirdPartyID);

                List<tblThirdPartyDownHistory> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyDownHistoryAll", "(Int32 pThirdPartyID)", false);
                throw new Exception("GetThirdPartyDownHistoryAll - " + Ex.Message);
            }
        }
        public tblThirdPartyDownHistory GetThirdPartyDownHistory(Int32 pThirdPartyID)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyDownHistory", "(Int32 pThirdPartyID)", true);
                recordFound = false;
                tblThirdPartyDownHistoryCrud crud = new tblThirdPartyDownHistoryCrud();
                crud.Where("ThirdPartyID", DAL.General.Operator.Equals, pThirdPartyID);

                tblThirdPartyDownHistory search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyDownHistory", "(Int32 pThirdPartyID)", false);
                throw new Exception("GetThirdPartyDownHistory - " + Ex.Message);
            }
        }
		public Int32 InsertThirdPartyDownHistory(Int32 pThirdPartyID, Boolean pThirdPartyDown, Boolean pSuspended, Int32 pDownReasonID)
        {
            try
            {
                UpdateTempMethod("InsertThirdPartyDownHistory", "(Int32 pThirdPartyID, Boolean pThirdPartyDown, Boolean pSuspended, Int32 pDownReasonID)", true);
                tblThirdPartyDownHistoryCrud crud = new tblThirdPartyDownHistoryCrud();
                tblThirdPartyDownHistory workRec = new tblThirdPartyDownHistory();
                if (pThirdPartyID != 0)
                {
                    crud.Where("ThirdPartyID", DAL.General.Operator.Equals, pThirdPartyID);
                    workRec = crud.ReadSingle();
                    workRec.ThirdPartyDown = pThirdPartyDown;
                    workRec.Suspended = pSuspended;
                    workRec.DownReasonID = Convert.ToByte(pDownReasonID);
                    crud.Update(workRec);
                    return pThirdPartyID;
                }
                else
                {
                    workRec.ThirdPartyDown = pThirdPartyDown;
                    workRec.Suspended = pSuspended;
                    workRec.DownReasonID = Convert.ToByte(pDownReasonID);
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("InsertThirdPartyDownHistory", "(Int32 pThirdPartyID, Boolean pThirdPartyDown, Boolean pSuspended, Int32 pDownReasonID)", false);
                throw new Exception("InsertThirdPartyDownHistory - " + Ex.Message);
            }
        }
		public Int32 InsertThirdPartyDownHistory(Int32 pThirdPartyID, Boolean pThirdPartyDown, Boolean pSuspended)
        {
            try
            {
                UpdateTempMethod("InsertThirdPartyDownHistory", "(Int32 pThirdPartyID, Boolean pThirdPartyDown, Boolean pSuspended)", true);
                tblThirdPartyDownHistoryCrud crud = new tblThirdPartyDownHistoryCrud();
                tblThirdPartyDownHistory workRec = new tblThirdPartyDownHistory();
                if (pThirdPartyID != 0)
                {
                    crud.Where("ThirdPartyID", DAL.General.Operator.Equals, pThirdPartyID);
                    workRec = crud.ReadSingle();
                    workRec.ThirdPartyDown = pThirdPartyDown;
                    workRec.Suspended = pSuspended;
                    crud.Update(workRec);
                    return pThirdPartyID;
                }
                else
                {
                    workRec.ThirdPartyDown = pThirdPartyDown;
                    workRec.Suspended = pSuspended;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("InsertThirdPartyDownHistory", "(Int32 pThirdPartyID, Boolean pThirdPartyDown, Boolean pSuspended)", false);
                throw new Exception("InsertThirdPartyDownHistory - " + Ex.Message);
            }
        }
        public Boolean UpdateThirdPartyDownCount(Int32 pThirdPartyID, Int32 pDownCount)
        {
            try
            {
                UpdateTempMethod("UpdateThirdPartyDownCount", "(Int32 pThirdPartyID, Int32 pDownCount)", true);
				tblThirdPartyCrud crud = new tblThirdPartyCrud();
				tblThirdParty party = GetThirdParty(pThirdPartyID);
				party.DownCount = Convert.ToInt16(pDownCount);
				crud.Update(party);
				return true;

			}
            catch (Exception Ex)
            {
                UpdateTempMethod("UpdateThirdPartyDownCount", "(Int32 pThirdPartyID, Int32 pDownCount)", false);
                throw new Exception("UpdateThirdPartyDownCount - " + Ex.Message);
            }
        }
		public List<tblThirdPartyFunction> GetThirdPartyFunction(Int32 pThirdPartyID)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyFunction", "(Int32 pThirdPartyID)", true);
                recordFound = false;
                tblThirdPartyFunctionCrud crud = new tblThirdPartyFunctionCrud();
                crud.Where("ThirdPartyID", DAL.General.Operator.Equals, pThirdPartyID);
                crud.ExcludeReferenceTable = excludeRefTable;
                List<tblThirdPartyFunction> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyFunction", "(Int32 pThirdPartyID)", false);
                throw new Exception("GetThirdPartyFunction - " + Ex.Message);
            }
        }

        public tblThirdPartyFunction GetThirdPartyFunction(Int32 pThirdPartyID, String pUssdFunction)
        {
            try
            {
                recordFound = false;
                tblThirdPartyFunctionCrud crud = new tblThirdPartyFunctionCrud();
                crud.Where("UssdFunction", DAL.General.Operator.Equals, pUssdFunction);
                crud.And("ThirdPartyID", DAL.General.Operator.Equals, pThirdPartyID);

                tblThirdPartyFunction search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetThirdPartyFunction - " + Ex.Message);
            }
        }
        public tblThirdPartyFunction GetThirdPartyFunctionByID(Int32 pThirdPartyFunctionID)
        {
            try
            {
                recordFound = false;
                tblThirdPartyFunctionCrud crud = new tblThirdPartyFunctionCrud();
                crud.Where("ThirdPartyFunctionID", DAL.General.Operator.Equals, pThirdPartyFunctionID);

                tblThirdPartyFunction search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetThirdPartyFunction - " + Ex.Message);
            }
        }
        public tblThirdPartyFunction GetThirdPartyFunction(String pUssdFunction)
        {
            try
            {
                recordFound = false;
                tblThirdPartyFunctionCrud crud = new tblThirdPartyFunctionCrud();
                crud.Where("UssdFunction", DAL.General.Operator.Equals, pUssdFunction);

                tblThirdPartyFunction search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetThirdPartyFunction - " + Ex.Message);
            }
        }
        public tblThirdPartyServiceFailure GetThirdPartyServiceFailure(Int32 pServiceFailureID)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyServiceFailure", "(Int32 pServiceFailureID)", true);
                recordFound = false;
                tblThirdPartyServiceFailureCrud crud = new tblThirdPartyServiceFailureCrud();
                crud.Where("ServiceFailureID", DAL.General.Operator.Equals, pServiceFailureID);

                tblThirdPartyServiceFailure search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyServiceFailure", "(Int32 pServiceFailureID)", false);
                throw new Exception("GetThirdPartyServiceFailure - " + Ex.Message);
            }
        }

        public Int32 InsertThirdPartyVending(Int32 pThirdPartyFunctionID, String pMsisdn, String pUserID, String pReferenceNumber, String pVendingMessage, Boolean pVendingSuccess, Decimal pAmount, short pMobileProviderID, String pBankResponse)
        {
			String logInfo = "pThirdPartyFunctionID = " + pThirdPartyFunctionID + " | pMsisdn = " + pMsisdn + " | pUserID = " + pUserID + " | pReferenceNumber = " + pReferenceNumber + " | pVendingMessage = " + pVendingMessage + "pVendingSuccess = " + pVendingSuccess.ToString() + " | pAmount = " + pAmount.ToString() + " | pMobileProviderID = " + pMobileProviderID.ToString() + " | pBankResponse = " + pBankResponse;
            try
            {
				
				tblThirdPartyVending vend = new tblThirdPartyVending();
				vend.Amount = pAmount;

				vend.BankResponse = pBankResponse;
				vend.ClientRefunded = false;
				vend.MobileProviderID = pMobileProviderID;
				vend.Msisdn = pMsisdn;
				vend.ReferenceNumber = pReferenceNumber;
				vend.UserID = pUserID;
				vend.VendingDateTime = DateTime.Now;
				vend.VendingMatched = false;
				vend.VendingMessage = pVendingMessage;
				vend.VendingSuccess = pVendingSuccess;
				vend.ThirdPartyFunctionID = pThirdPartyFunctionID;
				long ID = 0;
				tblThirdPartyVendingCrud crud = new tblThirdPartyVendingCrud();
				crud.Insert(vend, out ID);
				return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertThirdPartyVending - " + Ex.Message + "Loginfo=" + logInfo);
            }
        }
        public Int32 InsertThirdPartyServiceFailure(Int32 pThirdPartyID, Int32 pThirdPartyFunctionID, String pMsisdn, String pUserID, String pReferenceNumber, String pFailureReason)
        {
            try
            {
                UpdateTempMethod("InsertThirdPartyServiceFailure", "(Int32 pThirdPartyID, Int32 pThirdPartyFunctionID, String pMsisdn, String pUserID, String pReferenceNumber, String pFailureReason)", true);
                tblThirdPartyServiceFailureCrud crud = new tblThirdPartyServiceFailureCrud();
                tblThirdPartyServiceFailure workRec = new tblThirdPartyServiceFailure();
                if (pThirdPartyID != 0)
                {
                    crud.Where("ThirdPartyID", DAL.General.Operator.Equals, pThirdPartyID);
                    workRec = crud.ReadSingle();
                    workRec.ThirdPartyFunctionID = pThirdPartyFunctionID;
                    workRec.Msisdn = pMsisdn;
                    workRec.UserID = pUserID;
                    workRec.ReferenceNumber = pReferenceNumber;
                    workRec.FailureReason = pFailureReason;
                    crud.Update(workRec);
                    return pThirdPartyID;
                }
                else
                {
                    workRec.ThirdPartyFunctionID = pThirdPartyFunctionID;
                    workRec.Msisdn = pMsisdn;
                    workRec.UserID = pUserID;
                    workRec.ReferenceNumber = pReferenceNumber;
                    workRec.FailureReason = pFailureReason;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("InsertThirdPartyServiceFailure", "(Int32 pThirdPartyID, Int32 pThirdPartyFunctionID, String pMsisdn, String pUserID, String pReferenceNumber, String pFailureReason)", false);
                throw new Exception("InsertThirdPartyServiceFailure - " + Ex.Message);
            }
        }
		public Boolean ThirdPartySuspendToggle(Int32 pThirdPartyID)
		{
			try
			{
                UpdateTempMethod("ThirdPartySuspendToggle", "(Int32 pThirdPartyID)", true);
				tblThirdPartyCrud crud = new tblThirdPartyCrud();
				tblThirdParty party = GetThirdParty(pThirdPartyID);
				Boolean suspend = !Convert.ToBoolean(party.SuspendThirdParty);
				party.SuspendThirdParty = suspend;
				crud.Update(party);
				return true;
			}
			catch (Exception Ex)
			{
                UpdateTempMethod("ThirdPartySuspendToggle", "(Int32 pThirdPartyID)", false);
				throw new Exception(errorMessage + "ThirdPartySuspendToggle-" + Ex.Message);
			}
		}
        public Boolean SuspendThirdPartyService(Int32 pThirdPartyID, Boolean pSuspend, Boolean pServiceDown)
        {
            try
            {
                UpdateTempMethod("SuspendThirdPartyService", "(Int32 pThirdPartyID, Boolean pSuspend, Boolean pServiceDown)", true);
				tblThirdPartyCrud crud = new tblThirdPartyCrud();
				tblThirdParty thirdParty = GetThirdParty(pThirdPartyID);
				thirdParty.ServiceDown = pServiceDown;
				thirdParty.SuspendThirdParty = pSuspend;
				crud.Update(thirdParty);
                InsertThirdPartyDownHistory(pThirdPartyID, pServiceDown, pSuspend);
                List<tblApplicationErrorRecipient> appRecipients = GetApplicationErrorRecipient(Convert.ToInt32(GetApplication("General", "VCS.General.ThirdParty").ApplicationID));
                foreach (tblApplicationErrorRecipient rec in appRecipients)
                {
                    messBL.SendSms("The " + thirdParty.ThirdParty + " service is not responding and was suspended. You will be notified when it is back up.", rec.tblDeveloper.CellPhoneNumber);
                }
                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("SuspendThirdPartyService", "(Int32 pThirdPartyID, Boolean pSuspend, Boolean pServiceDown)", false);
                throw new Exception(errorMessage + "SuspendThirdPartyService - " + Ex.Message);
            }
        }
        public Boolean ReinStateThirdPartyService(Int32 pThirdPartyID)
        {
            try
            {
                UpdateTempMethod("ReinStateThirdPartyService", "(Int32 pThirdPartyID)", true);
				tblThirdPartyDownHistoryCrud crud = new tblThirdPartyDownHistoryCrud();
				tblThirdPartyDownHistory history = GetThirdPartyDownHistory(pThirdPartyID);
				tblThirdPartyDownClientCrud crudDown = new tblThirdPartyDownClientCrud();

				List<tblThirdPartyDownClient> clients = new List<tblThirdPartyDownClient>();
                if (RecordFound)
                     clients = GetThirdPartyDownClient(Convert.ToInt32(history.DownHistoryID));

                    tblThirdParty thirdParty = GetThirdParty(pThirdPartyID);
                    if (history != null)
                    {
                        history.TimeUp = DateTime.Now;
                        history.Processed = true;
					crud.Update(history);

                        DateTime downTime = Convert.ToDateTime(history.TimeDown);
                        DateTime upTime = DateTime.Now;
                        String duration = Convert.ToString(upTime - downTime);
                        String clientCount = clients.Count.ToString();

                        String message = "<html>";
                        message += "<body bgcolor=white>";
                        message += "<table class='small' style='border-collapse:collapse'>";
                        message += "<tr><td colspan='2' style='font-weight:bold;font-size:14pt;font-family:verdana;width:500px;border:1px solid #778899;text-align:center;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>" + thirdParty.ThirdParty + "</td></tr>";
                        message += "<tr><td style='vertical-align:top;word-wrap:break-word;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Time Down</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + downTime + "</td></tr>";
                        message += "<tr><td style='vertical-align:top;word-wrap:break-word;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Time Up</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + upTime+ "</td></tr>";
                        message += "<tr><td style='vertical-align:top;word-wrap:break-word;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Duration</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + duration.Substring(0, 8) + "</td></tr>";
                        message += "<tr><td style='vertical-align:top;word-wrap:break-word;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Clients Affected</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + clientCount + "</td></tr>";
                        message += "</table>";
                        message += "</body>";
                        message += "</html>";

                        List<tblApplicationErrorRecipient> appRecipients = GetApplicationErrorRecipient(Convert.ToInt32(GetApplication("General", "VCS.General.ThirdParty").ApplicationID));
                        String recipients = "";
                        foreach (tblApplicationErrorRecipient rec in appRecipients)
                        {
                            recipients += rec.tblDeveloper.EMail + ";";
                        }

                        if (recipients.Length > 0)
                            recipients = recipients.Substring(0, recipients.Length - 1);

                        messBL.UseMailBodyAsIs = true;
                        messBL.SendMail("Third Party Service Up (" + thirdParty.ThirdParty + ")", message, recipients, "VCS.General.ThirdPartyServiceCheck", "backoffice@vcs.co.za");

                        foreach (tblApplicationErrorRecipient rec in appRecipients)
                        {
                            messBL.SendSms("Third Party Service (" + thirdParty.ThirdParty + ") up again", rec.tblDeveloper.CellPhoneNumber);
                        }


                        foreach (tblThirdPartyDownClient client in clients)
                        {
                            messBL.SendSms(client.NotifyMessage, client.Msisdn);
                            client.Notified = true;
						crudDown.Update(client);
                        }

                    }
                thirdParty.ServiceDown = false;
                thirdParty.DownCount = 0;
				tblThirdPartyCrud crudParty = new tblThirdPartyCrud();
				crudParty.Update(thirdParty);

                return true;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("ReinStateThirdPartyService", "(Int32 pThirdPartyID)", false);
                throw new Exception(errorMessage + "ReinstateThirdPartyService - " + Ex.Message);
            }
        }
		public List<tblThirdPartyProduct> GetThirdPartyProduct(Int32 pThirdPartyID)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyProduct", "(Int32 pThirdPartyID)", true);
                recordFound = false;
                tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
                crud.Where("ThirdPartyID", DAL.General.Operator.Equals, pThirdPartyID);

                List<tblThirdPartyProduct> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyProduct", "(Int32 pThirdPartyID)", false);
                throw new Exception("GetThirdPartyProduct - " + Ex.Message);
            }
        }
		public tblThirdPartyProduct GetThirdPartyProductByID(Byte pProductID)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyProductByID", "(Byte pProductID)", true);
                recordFound = false;
                tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
                crud.Where("ProductID", DAL.General.Operator.Equals, pProductID);

                tblThirdPartyProduct search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyProductByID", "(Byte pProductID)", false);
                throw new Exception("GetThirdPartyProductByIDByte - " + Ex.Message);
            }
        }
		public tblThirdPartyProduct GetThirdPartyProduct(String pUssdFunction)
        {
            try
            {
                UpdateTempMethod("GetThirdPartyProduct", "(String pUssdFunction)", true);
                recordFound = false;
                tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
                crud.Where("UssdFunction", DAL.General.Operator.Equals, pUssdFunction);

                tblThirdPartyProduct search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyProduct", "(String pUssdFunction)", false);
                throw new Exception("GetThirdPartyProduct - " + Ex.Message);
            }
        }
        /*public tblThirdPartyProduct GetThirdPartyProduct(Int32 pThirdPartyID, String pUssdFunction)
        {
            try
            {
                recordFound = false;
                tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
                crud.Where("UssdFunction", DAL.General.Operator.Equals, pUssdFunction);
                crud.And("ThirdPartyID", DAL.General.Operator.Equals, pThirdPartyID);

                tblThirdPartyProduct search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetThirdPartyProduct - " + Ex.Message);
            }
        }*/
        public Boolean UpdateThirdPartyUssdFunction(String pUssdFunctionName, Boolean pSmartCard, Boolean pMultiChoiceAfricaTransaction, Boolean pVerifyCardNumber, Boolean pExtractAmount, Boolean pAuthorisation, String pTransactionDescription, Boolean pMultiChoiceCustomerNo, Boolean pMultiChoicePayment, String pMultiChoicePaymentDesc, Boolean pGetMulitChoiceSmartCardNumber, String pVcsTerminalID, String pResponseToCustomer, String pServiceDownMessage, Boolean pExtractMeterNo, Boolean pExtractRecipient, Boolean pIssueDataVoucher, Boolean pPin, Boolean pVirtualAccountCard, Boolean pIssueFromGloCell, Boolean pExtractBankResponse, Boolean pCheckVirtualVoucherAvailable)
        {
            try
            {
                UpdateTempMethod("UpdateThirdPartyUssdFunction", "(String pUssdFunctionName, Boolean pSmartCard, Boolean pMultiChoiceAfricaTransaction, Boolean pVerifyCardNumber, Boolean pExtractAmount, Boolean pAuthorisation, String pTransactionDescription, Boolean pMultiChoiceCustomerNo, Boolean pMultiChoicePayment, String pMultiChoicePaymentDesc, Boolean pGetMulitChoiceSmartCardNumber, String pVcsTerminalID, String pResponseToCustomer, String pServiceDownMessage, Boolean pExtractMeterNo, Boolean pExtractRecipient, Boolean pIssueDataVoucher, Boolean pPin, Boolean pVirtualAccountCard, Boolean pIssueFromGloCell, Boolean pExtractBankResponse, Boolean pCheckVirtualVoucherAvailable)", true);
				tblThirdPartyUssdParameterCrud crud = new tblThirdPartyUssdParameterCrud();
				tblThirdPartyUssdParameter par = GetThirdPartyUssdParameter(pUssdFunctionName);
				par.CheckSmartCard = pSmartCard;
				par.CheckVirtualVoucherAvailable = pCheckVirtualVoucherAvailable;
				par.DoAuthorisation = pAuthorisation;
				par.DoMultiChoicePayment = pMultiChoicePayment;
				par.ExtractAmount = pExtractAmount;
				par.ExtractBankResponseCode = pExtractBankResponse;
				par.ExtractMeterNumber = pExtractMeterNo;
				par.ExtractPin = pPin;
				par.ExtractRecipient = pExtractRecipient;
				par.GetMultiChoiceCustomerNumber = pMultiChoiceCustomerNo;
				par.GetMultiChoiceSmartCardNumber = pGetMulitChoiceSmartCardNumber;
				par.GetVirtualAccountCard = pVirtualAccountCard;
				par.IssueDataVoucher = pIssueDataVoucher;
				par.IssueFromGlocell = pIssueFromGloCell;
				par.MultiChoiceAfricaTransaction = pMultiChoiceAfricaTransaction;
				par.MultiChoiceMethodOfPayment = "CASH";
				par.MultiChoicePaymentDescription = pMultiChoicePaymentDesc;
				par.ResponseToCustomer = pResponseToCustomer;
				par.ServiceDownMessage = pServiceDownMessage;
				par.TransactionDescription = pTransactionDescription;
				par.VCSTerminalID = pVcsTerminalID;
				par.VerifyCardNumber = pVerifyCardNumber;
				crud.Update(par);
				return true;
			}
            catch (Exception Ex)
            {
                UpdateTempMethod("UpdateThirdPartyUssdFunction", "(String pUssdFunctionName, Boolean pSmartCard, Boolean pMultiChoiceAfricaTransaction, Boolean pVerifyCardNumber, Boolean pExtractAmount, Boolean pAuthorisation, String pTransactionDescription, Boolean pMultiChoiceCustomerNo, Boolean pMultiChoicePayment, String pMultiChoicePaymentDesc, Boolean pGetMulitChoiceSmartCardNumber, String pVcsTerminalID, String pResponseToCustomer, String pServiceDownMessage, Boolean pExtractMeterNo, Boolean pExtractRecipient, Boolean pIssueDataVoucher, Boolean pPin, Boolean pVirtualAccountCard, Boolean pIssueFromGloCell, Boolean pExtractBankResponse, Boolean pCheckVirtualVoucherAvailable)", false);
                throw new Exception("UpdateThirdPartyUssdFunction - " + Ex.Message);
            }
        }
		public List<tblThirdPartyProduct> GetThirdPartyProduct()
        {
            try
            {
                UpdateTempMethod("GetThirdPartyProduct", "()", true);
                recordFound = false;
                tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
                List<tblThirdPartyProduct> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                UpdateTempMethod("GetThirdPartyProduct", "()", false);
                throw new Exception("GetThirdPartyProduct - " + Ex.Message);
            }
        }

		public String GetThirdParyUniqueReferenceNumber(Int32 pThirdPartyID)
		{
			try
			{
                UpdateTempMethod("GetThirdParyUniqueReferenceNumber", "(Int32 pThirdPartyID)", true);
				tblThirdPartyCrud crud = new tblThirdPartyCrud();
				tblThirdParty thirdParty = GetThirdParty(pThirdPartyID);
				String refNo = thirdParty.UniqueReferenceNo;
				Int64 refNoInt = Convert.ToInt64(refNo) + 1;
				thirdParty.UniqueReferenceNo = refNoInt.ToString();
				crud.Update(thirdParty);
				return refNo;

			}
			catch (Exception Ex)
			{
                UpdateTempMethod("GetThirdParyUniqueReferenceNumber", "(Int32 pThirdPartyID)", false);
				throw new Exception(errorMessage + "GetThirdPartyUniqueReferenceNumber-" + Ex.Message);
			}
		}


		public Boolean UpdateThirdParty(Int32 pThirdPartyID, Boolean pSuspend)
        {
            try
            {
                UpdateTempMethod("UpdateThirdParty", "(Int32 pThirdPartyID, Boolean pSuspend)", true);
				tblThirdPartyCrud crud = new tblThirdPartyCrud();
				tblThirdParty thirdParty = GetThirdParty(pThirdPartyID);
				thirdParty.SuspendThirdParty = pSuspend;
				crud.Update(thirdParty);
				return true;

			}
            catch (Exception Ex)
            {
                UpdateTempMethod("UpdateThirdParty", "(Int32 pThirdPartyID, Boolean pSuspend)", false);
                throw new Exception("UpdateThirdParty - " + Ex.Message);
            }
        }
    }
}
