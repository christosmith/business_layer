﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.DAL;
using System.Web.Services.Protocols;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace DPO.BackOffice.BL
{

    public partial class BackOfficeBL
    {
        //internal BPCWebService.XMLVendFaultResp DeserializeBPCMessage(XmlNode pBPCMessage)
        //{
        //    if (String.IsNullOrEmpty(pBPCMessage.InnerXml))
        //        return null;

        //    XmlSerializer serializer = new XmlSerializer(GetType(BPCWebService.XMLVendFaultResp));

        //}



        internal BotswanaPowerToken.MsgID CreateBPCMessageID()
        {
            try
            {
                Int32 applicationID = Convert.ToInt32(GetApplicationByName("VCS.VirtualMessage.Ussd").ApplicationID);
                BotswanaPowerToken.MsgID messageID = new BotswanaPowerToken.MsgID();
                messageID.dateTime = DateTime.Now.ToString("yyyyMMddhhmmss");
                Int32 uniqueNo = Convert.ToInt32(GetSettingByName("BcpPrepaidPwUniqueNumber", applicationID).SettingValue);
                uniqueNo++;
                messageID.uniqueNumber = uniqueNo.ToString();
                Int32 settingID = Convert.ToInt32(GetSettingByName("BcpPrepaidPwUniqueNumber", applicationID).SettingID);
                SaveSetting(settingID, applicationID, "BcpPrepaidPwUniqueNumber", uniqueNo.ToString(), false);
                return messageID;
            }
            catch (Exception Ex)
            {
                throw new Exception("BPCWebService - " + Ex.Message);
            }
        }



        public String DoBotswanaPowerPayment(String pMeterNumber, Decimal pAmount, Int32 pUssdMsisdnID, GatewayServer pGatewayServer)
        {
            Int32 applicationID = 0;
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                applicationID = Convert.ToInt32(GetApplicationByName("VCS.VirtualMessage.Ussd").ApplicationID);
                BotswanaPowerToken.CreditVendReq creditVendRequest = new BotswanaPowerToken.CreditVendReq();
                BotswanaPowerToken.AuthCred credentials = new BotswanaPowerToken.AuthCred();
                credentials.opName = GetSettingByName("PwBpoOperatorName", applicationID).SettingValue;
                credentials.password = GetSettingByName("PwBpoOperatorPassword", applicationID).SettingValue;
                BotswanaPowerToken.EANDeviceID clientID = new BotswanaPowerToken.EANDeviceID();
                clientID.ean = GetSettingByName("PwBpoClientId", applicationID).SettingValue;
                BotswanaPowerToken.EANDeviceID terminalID = new BotswanaPowerToken.EANDeviceID();
                terminalID.ean = GetSettingByName("PwBpoTerminalId", applicationID).SettingValue;
                creditVendRequest.authCred = credentials;
                creditVendRequest.clientID = clientID;
                creditVendRequest.terminalID = terminalID;
                creditVendRequest.msgID = CreateBPCMessageID();
                BotswanaPowerToken.VendIDMethod vendingMethod = new BotswanaPowerToken.VendIDMethod();
                BotswanaPowerToken.MeterNumber meterNumber = new BotswanaPowerToken.MeterNumber();
                meterNumber.msno = pMeterNumber;
                vendingMethod.meterIdentifier = meterNumber;
                creditVendRequest.idMethod = vendingMethod;

                BotswanaPowerToken.Currency currency = new BotswanaPowerToken.Currency();
                currency.symbol = "P";
                currency.value = pAmount;
                BotswanaPowerToken.PurchaseValueCurrency purchaseValCur = new BotswanaPowerToken.PurchaseValueCurrency();
                purchaseValCur.amt = currency;

                BotswanaPowerToken.PurchaseValue purchaseValue = purchaseValCur;

                BotswanaPowerToken.Cash purchaseType = new BotswanaPowerToken.Cash();
                purchaseType.tenderAmt = purchaseValCur.amt;
                creditVendRequest.payType = purchaseType;
                creditVendRequest.purchaseValue = purchaseValue;
                BotswanaPowerToken.WebService bcpClient = new BotswanaPowerToken.WebService();
                String receiptNumber = "";
                String token = "";
                String siUnit = "";
                String unit = "";
                String unitsBought = "";
                try
                {
                    BotswanaPowerToken.CreditVendResp creditVendResponse = bcpClient.CreditVendRequest(creditVendRequest);
                    receiptNumber = creditVendResponse.creditVendReceipt.receiptNo;
                    BotswanaPowerToken.Tx creditVendTx = creditVendResponse.creditVendReceipt.transactions.tx[0];
                    token = ((DPO.BackOffice.BL.BotswanaPowerToken.STS1Token)((DPO.BackOffice.BL.BotswanaPowerToken.CreditVendTx)creditVendTx).creditTokenIssue.token).stsCipher;
                    siUnit = ((DPO.BackOffice.BL.BotswanaPowerToken.CreditVendTx)creditVendTx).creditTokenIssue.units.siUnit;
                    unit = ((DPO.BackOffice.BL.BotswanaPowerToken.CreditVendTx)creditVendTx).creditTokenIssue.units.value.ToString();
                    unitsBought = unit + " " + siUnit;

                }

                catch (Exception Ex)
                {
                    WriteLog(applicationID, "DoBotswanaPowerPayment - " + Ex.Message, eLogType.Error);
                    if (Ex.Message.Contains("Meter not found"))
                        return "Error: Meter not found.";

                    if (Ex.Message.Contains("blocked"))
                        return "Customer is blocked.";

                    return "Error: Unable to issue token." + Ex.Message;
                }

                String strSmsMessage = "Bought P" + pAmount.ToString("N2") + " electricity for Meter No " + pMeterNumber + ". Token " + token + ". Units " + unitsBought + ". BPC receipt " + receiptNumber + ". For queries call 3953541 Or 16266";
                WriteLog(applicationID, strSmsMessage, eLogType.Information);
                if (pGatewayServer == GatewayServer.VSPMGW01)
                    return strSmsMessage;

                return "Success";
            }
            catch (Exception Ex)
            {
                WriteLog(applicationID, "DoBotswanaPowerPayment Main - " + Ex.Message, eLogType.Error);
                throw new Exception("DoBotswanaPowerPayment - " + Ex.Message);
            }
        }

        /*
        public String DoBotswanaPowerPayment(String pMeterNumber, Decimal pAmount, Int32 pUssdMsisdnID)
        {
            try
            {

                Int32 applicationID = Convert.ToInt32(GetApplicationByName("VCS.VirtualMessage.Ussd").ApplicationID);
                BPCWebService.CreditVendReq creditVendRequest= new BPCWebService.CreditVendReq();
                BPCWebService.AuthCred credentials = new BPCWebService.AuthCred();
                credentials.opName = GetSettingByName("PwBpoOperatorName", applicationID).SettingValue;
                credentials.password = GetSettingByName("PwBpoOperatorPassword", applicationID).SettingValue;
                BPCWebService.EANDeviceID clientID = new BPCWebService.EANDeviceID();
                clientID.ean = GetSettingByName("PwBpoClientId", applicationID).SettingValue;
                BPCWebService.EANDeviceID terminalID = new BPCWebService.EANDeviceID();
                terminalID.ean = GetSettingByName("PwBpoTerminalId", applicationID).SettingValue;
                creditVendRequest.authCred = credentials;
                creditVendRequest.clientID = clientID;
                creditVendRequest.terminalID = terminalID;
                creditVendRequest.msgID = CreateBPCMessageID();
                BPCWebService.VendIDMethod vendingMethod = new BPCWebService.VendIDMethod();
                BPCWebService.MeterNumber meterNumber = new BPCWebService.MeterNumber();
                meterNumber.msno = pMeterNumber;
                vendingMethod.meterIdentifier = meterNumber;
                creditVendRequest.idMethod = vendingMethod;

                BPCWebService.Currency currency = new BPCWebService.Currency();
                currency.symbol = "P";
                currency.value = pAmount;
                BPCWebService.PurchaseValueCurrency purchaseValCur = new BPCWebService.PurchaseValueCurrency();
                purchaseValCur.amt = currency;

                BPCWebService.PurchaseValue purchaseValue = purchaseValCur;

                BPCWebService.Cash purchaseType = new BPCWebService.Cash();
                purchaseType.tenderAmt = purchaseValCur.amt;
                creditVendRequest.payType = purchaseType;
                creditVendRequest.purchaseValue = purchaseValue;
                BPCWebService.XMLVendServiceSoap bcpClient = new BPCWebService.XMLVendServiceSoap();
                String receiptNumber = "";
                String token = "";
                String siUnit = "";
                String unit = "";
                String unitsBought = "";
                try
                {
                    BPCWebService.CreditVendResp creditVendResponse = bcpClient.CreditVendRequest(creditVendRequest);
                    receiptNumber = creditVendResponse.creditVendReceipt.receiptNo;
                    BPCWebService.Tx creditVendTx = creditVendResponse.creditVendReceipt.transactions.tx[0];
                    token = ((DPO.BackOffice.BL.BPCWebService.STS1Token)((DPO.BackOffice.BL.BPCWebService.CreditVendTx)creditVendTx).creditTokenIssue.token).stsCipher;
                    siUnit = ((DPO.BackOffice.BL.BPCWebService.CreditVendTx)creditVendTx).creditTokenIssue.units.siUnit;
                    unit = ((DPO.BackOffice.BL.BPCWebService.CreditVendTx)creditVendTx).creditTokenIssue.units.value.ToString();
                    unitsBought = unit + " " + siUnit;

                }

                catch (Exception Ex)
                {
                    WriteLog(applicationID, "DoBotswanaPowerPayment - " + Ex.Message, eLogType.Error);
                    return "Unable to issue token.";
                }

                UpdateUssdMsisdnBPCData(pUssdMsisdnID, receiptNumber, token, unitsBought);

                ussdInformation.BPCReceiptNumber = receiptNumber;
                ussdInformation.BPCToken = token;
                ussdInformation.BPCUnitsBought = unitsBought;
                return "Success";
            }
            catch (Exception Ex)
            {
                throw new Exception("DoBotswanaPowerPayment - " + Ex.Message);
            }
        }
        */
    }
}
