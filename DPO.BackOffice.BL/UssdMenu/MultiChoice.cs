﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.DAL;


namespace DPO.BackOffice.BL
{

    public partial class BackOfficeBL
    {


 
        public MulitChoiceParameter GetMultiChoiceParameter()
        {
            try
            {
                Int32 applicationID = Convert.ToInt32(GetApplicationByName("VCS.VirtualMessage.Ussd").ApplicationID);
                MulitChoiceParameter mcParam = new MulitChoiceParameter();
                mcParam.VendorCode = GetSettingByName("McaBotswana", applicationID).SettingValue;
                mcParam.CustomerVendorID = GetSettingByName("McaCustomerVendorID", applicationID).SettingValue;
                mcParam.DataSource = GetSettingByName("McaBotswana", applicationID).SettingValue;
                mcParam.BotswanaCurrency = GetSettingByName("McaBotswanaCurrency", applicationID).SettingValue;
                mcParam.BusinessUnit = GetSettingByName("McaBusinessUnit", applicationID).SettingValue;
                mcParam.PaymentVendorCode = GetSettingByName("McaVendorId", applicationID).SettingValue;

                return mcParam;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMultiChoiceParameter - " + Ex.Message);
            }
        }

        
        /*
        public tblussdvalidationmessage ValidateMultiChoiceSmartCardNumber(String pSmartCardNumber, Int32 pUssdMsisdnID)
        {
            try
            {
                

                MultiChoiceWebService.SelfCareServiceClient mcClient = new MultiChoiceWebService.SelfCareServiceClient();
                
                MulitChoiceParameter mcParam = GetMultiChoiceParameter();
                
                MultiChoiceWebService.GetBalanceByDeviceNumberResponse mcResponse = new MultiChoiceWebService.GetBalanceByDeviceNumberResponse();
                try
                {
                    mcResponse = mcClient.GetCustomerDetailsByDeviceNumber(mcParam.DataSource, pSmartCardNumber, mcParam.BotswanaCurrency, mcParam.BusinessUnit, mcParam.CustomerVendorID, "", "", "");

                    Decimal pLastinvoiceAmount = Convert.ToDecimal(mcResponse.accounts[0].lastInvoiceAmount);
                    DateTime pLastInvoiceDate = mcResponse.accounts[0].lastInvoiceDate;
                    String pAccountNumber = mcResponse.accounts[0].number.ToString();
                    
                    String pAccountStatus = mcResponse.accounts[0].status.ToString();
                    
                    String pFirstName = mcResponse.customerDetails.FirstName;
                    String pEMailAddress = mcResponse.customerDetails.emailAddress;
                    String pInitials = mcResponse.customerDetails.initials;
                    String pCustomerNumber = mcResponse.customerDetails.number.ToString();
                    ussdInformation.MCACustomerNo = pCustomerNumber;
                    String pCustomerStatus = mcResponse.customerDetails.status.ToString();
                    String pSurname = mcResponse.customerDetails.surname;
                    String pCellPhoneNo = mcResponse.customerDetails.cellNumber;

                    MultiChoiceWebService.DueAmountAndDate dueDate = new MultiChoiceWebService.DueAmountAndDate();
                    dueDate = mcClient.GetDueAmountandDate(mcParam.DataSource, Convert.ToInt32(mcResponse.customerDetails.number), "", mcParam.BusinessUnit, mcParam.CustomerVendorID, "", "", "");
                    Decimal pTotalBalance = dueDate.dueAmount;
                    DateTime pPaymentDueDate = dueDate.dueDate;

                    UpdateUssdMsisdnMultiChoiceData(pUssdMsisdnID, pLastinvoiceAmount, pLastInvoiceDate, pAccountNumber, pPaymentDueDate, pAccountStatus, pTotalBalance, pFirstName, pEMailAddress, pInitials, pCustomerNumber, pCustomerStatus, pSurname, pCellPhoneNo, pSmartCardNumber);

                    recordFound = false;

                }
                catch (Exception Ex)
                {
                    recordFound = true;
                    String errorMessage = Ex.Message;
                    if (errorMessage.Contains("smartcardNumber not found"))
                    {
                        mess = GetUssdValidationMessage("The smartcard number was not found.");

                        if (!recordFound)
                            WriteLog(Convert.ToInt32(GetApplicationByName("VCS.VirtualMessage.Ussd").ApplicationID), "Error message not set up in tblValidationMessage = " + errorMessage, eLogType.Error);
                    }
                    else
                        throw new Exception("ValidateMultiChoiceSmartCardNumber - " + Ex.Message);
                }

                return mess;

            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateMultiChoiceSmartCardNumber - " + Ex.Message);
            }
        }

        public String DoMultiChoiceReathorisation(String pSmartCardNumber)
        {
            String retString = "";
            try
            {
                MultiChoiceWebService.SelfCareServiceClient mcClient = new MultiChoiceWebService.SelfCareServiceClient();
                MultiChoiceWebService.DigitalReauthReason reason = new MultiChoiceWebService.DigitalReauthReason();
                reason.Reason = MultiChoiceWebService.DigitalReauthReasons.E16;
                Boolean result = false;
                MulitChoiceParameter mcParam = GetMultiChoiceParameter();
                try
                { 
                   result = mcClient.ReAuthorize(mcParam.DataSource, pSmartCardNumber, reason, mcParam.CustomerVendorID, "", "", mcParam.BusinessUnit);
                }
                catch (Exception Ex)
                {
                    if (Ex.Message.Contains("Your DStv account is suspended."))
                    {
                        retString = "Your DStv account is suspended.";
                    }
                    else
                        retString = "MultiChoice re-authorisation exception";
                    return retString;
                }

                if (!result)
                    retString = "MultiChoice re-authorisation exception";

                return "Success";
            }
            catch (Exception Ex)
            {
                throw new Exception("DoMultiChoiceReathorisation - " + Ex.Message);
            }
        }
        public String DoMultiChoicePayment(List<String> pProductUserKeys, String pUserID, Int32 pUssdMsisdnID, Decimal pAmount)
        {
            String logInfo = "";
            try
            {
                tblussdmsisdnmultichoicedata mcData = GetUssdMsisdnMultiChoiceData(pUssdMsisdnID);
                if (!recordFound)
                    throw new Exception("MultiChoice data was not collected for msisdn with id=" + pUssdMsisdnID.ToString());

                logInfo += "Initialising";
                MultiChoiceWebService.SelfCareServiceClient mcClient = new MultiChoiceWebService.SelfCareServiceClient();

                
                logInfo += "|GetParameters";
                MulitChoiceParameter mcParam = GetMultiChoiceParameter();
                MultiChoiceWebService.PaymentProductCollection productCollection = new MultiChoiceWebService.PaymentProductCollection();
                MultiChoiceWebService.SubmitPaymentResponse paymentResponse = new MultiChoiceWebService.SubmitPaymentResponse();
                MultiChoiceWebService.PaymentProduct product = new MultiChoiceWebService.PaymentProduct();
                logInfo += "|AssignProductUserky";
                foreach (String productUserKey in pProductUserKeys)
                {
                    logInfo += "|" + productUserKey;
                    product.ProductUserKey = productUserKey;
                    productCollection.Add(product);
                }

                String retString = "";
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                try
                {
                    logInfo += "|GetMerchantCount";
                    String transactionNumber = vvoBL.GetMerchantCounter(pUserID).ToString();
                    logInfo += "|transactionNumber=" + transactionNumber;
                    UInt32 customerNumber = Convert.ToUInt32(mcData.CustomerNumber);
                    logInfo += "|CustomerNumber=" + customerNumber.ToString();
                    paymentResponse = mcClient.SubmitPayment(mcParam.CustomerVendorID, mcParam.DataSource, mcParam.PaymentVendorCode, transactionNumber, customerNumber, pAmount, 1, mcParam.BotswanaCurrency, "Payment via Virtual Mobile", productCollection, "MOBILE", "", "", "DSTV", null);
                    logInfo += "|PaymentResponse.ErrorMessage=" + paymentResponse.SubmitPayment.ErrorMessage;
                }
                catch (Exception Ex)
                {
                    logInfo += "|MultiChoiceError=" + Ex.Message;
                    retString = "MultiChoice payment exception";
                    return retString;
                }

                
                String receiptNumber = paymentResponse.SubmitPayment.receiptNumber.ToString();
                logInfo += "|ReceiptNumber=" + receiptNumber;
                UpdateUssdMsisdnMultiChoiceData(receiptNumber, pUssdMsisdnID);
                logInfo += "|MultiChoiceDataUpdated";
                retString = receiptNumber;
                return retString;
            }
            catch (Exception Ex)
            {
                throw new Exception("DoMulitChoicePayment - " + Ex.Message + "|Loginfo=" + logInfo);
            }
        }
        */
    }
}
