﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.DAL;
using System.Net;
using System.IO;
using Microsoft.CSharp;

namespace DPO.BackOffice.BL
{
	/*	public class TswanaGasCity
		{
			public Int32 CityID = 0;
			public String CityName = "";
		}

		public class TswanaGasCentre
		{
			public Int32 CentreID = 0;
			public String CentreName = "";
		}

		public class TswanaGasCylinder
		{
			public Int32 CylinderID = 0;
			public String CylinderName = "";
			public Decimal CylinderPrice = 0;
		}*/

	public partial class BackOfficeBL
	{
		public class TswanaGasCity
		{
			public Int32 CityID = 0;
			public String CityName = "";
		}

		public class TswanaGasCentre
		{
			public Int32 CentreID = 0;
			public String CentreName = "";
            public Int32 CylinderID = 0;
            public String CylinderType = "";
            public Decimal CylinderPrice = 0;
		}

		public class TswanaGasCylinder
		{
			public Int32 CylinderID = 0;
			public String CylinderName = "";
		}

        

        public tblTswanaGasEntityCode GetTswanaGasEntityCode(Int32 pEnityTypeID, String pEntityName)
		{
			try
			{
				tblTswanaGasEntityCodeCrud crud = new tblTswanaGasEntityCodeCrud();
				crud.Where("EntityTypeID", DAL.General.Operator.Equals, pEnityTypeID);
				crud.And("EntityName", DAL.General.Operator.Equals, pEntityName);
				recordFound = false;
				tblTswanaGasEntityCode code = crud.ReadSingle();
				recordFound = crud.RecordFound;

				return code;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasEntityCode - " + Ex.Message);
			}
		}



        public tblTswanaGasEntityCode GetTswanaGasEntityCodeByCode(Int32 pEnityTypeID, String pEntityCode)
		{
			try
			{
				tblTswanaGasEntityCodeCrud crud = new tblTswanaGasEntityCodeCrud();
				crud.Where("EntityTypeID", DAL.General.Operator.Equals, pEnityTypeID);
				crud.And("EntityCode", DAL.General.Operator.Equals, pEntityCode);
				recordFound = false;
				tblTswanaGasEntityCode code = crud.ReadSingle();
				recordFound = crud.RecordFound;

				return code;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasEntityCode - " + Ex.Message);
			}
		}

		public Int32 InsertTswanaGasEntityCode(Byte pEntityTypeID, String pEntityName, String pEntityCode)
		{
			try
			{
				tblTswanaGasEntityCode code = new tblTswanaGasEntityCode();
				code.EntityCode = pEntityCode;
				code.EntityName = pEntityName;
				code.EntityTypeID = pEntityTypeID;
				tblTswanaGasEntityCodeCrud crud = new tblTswanaGasEntityCodeCrud();
				long id = 0;
				crud.Insert(code, out id);
				return (Int32)id;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertTswanaGasEntityCode - " + Ex.Message);
			}
		}

        public Boolean TswanaGasUssdTablePopulate()
		{
            String logInfo = "";
			try
			{
				tblThirdParty thirdParty = GetThirdPartyByName("Tswana Gas");
				Int32 thirdPartyID = 0;
				if (!recordFound)
					thirdPartyID = InsertThirdParty("Tswana Gas", "VW99");
				else
					thirdPartyID = (Int32)thirdParty.ThirdPartyID;

                Decimal emtyCylCost = GetTswanaGasEmptyCylinder();

				String transaction = "";
				List<tblTswanaGasCity> cities = GetTswanaGasCity();
                foreach (tblTswanaGasCity city in cities)
                {
                    logInfo += "|City=" + city.CityName;
                    if ((Boolean)city.Active)
                    {
                        logInfo += "|CityIsActive";
                        List<tblTswanaGasCentre> centres = GetTswanaGasCentre((Int32)city.CityID);
                        logInfo += "|CentresRetrieved";
                        foreach (tblTswanaGasCentre centre in centres)
                        {
                            logInfo += "|Centre=" + centre.CentreName;
                            List<tblTswanaGasCylinder> cylinders = GetTswanaGasCylinder();
                            foreach (tblTswanaGasCylinder cylinder in cylinders)
                            {
                                tblTswanaGasCentreCylinder centreCylinder = GetTswanaGasCentreCylinder(Convert.ToInt32(centre.CentreID), Convert.ToInt32(cylinder.CylinderID));
                                #region Pay Now. With Empty
                                logInfo += "|" + cylinder.CylinderName;
                                transaction = city.tblTswanaGasEntityCode.EntityCode + centre.tblTswanaGasEntityCode.EntityCode + cylinder.tblTswanaGasEntityCode.EntityCode + "YY";
                                Int32 functionID = 0;
                                tblThirdPartyFunction function = GetThirdPartyFunction(transaction);
                                if (!recordFound)
                                    functionID = InsertThirdPartyFunction(thirdPartyID, transaction, transaction);
                                else
                                    functionID = (Int32)function.ThirdPartyFunctionID;

                                Int32 productTypeID = (Int32)GetThirdPartyProductType("Gas Sales").ProductTypeID;
                                Int32 productID = 0;
                                tblThirdPartyProduct product = GetThirdPartyProduct(transaction);
                                String productDesc = city.CityName + ";" + centre.CentreName + ";" + cylinder.CylinderName + "; Empty";
                                if (!recordFound)
                                    productID = InsertThirdPartyProduct(thirdPartyID, productTypeID, productDesc, transaction, (Decimal)centreCylinder.CylinderPrice);
                                else
                                {
                                    productID = (Int32)product.ProductID;
                                    if (Convert.ToDecimal(product.Amount) != Convert.ToDecimal(centreCylinder.CylinderPrice))
                                        UpdateThirdPartyProduct(transaction, Convert.ToDecimal(centreCylinder.CylinderPrice));

                                    if (product.Product == product.UssdFunction)
                                    {
                                        product.Product = productDesc;
                                        tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
                                        crud.Update(product);
                                    }
                                }

                                String transactionDescription = "Tswana Gas Sale - " + cylinder.CylinderName;
                                String customerResponse = "Payment of {?amount} received for " + cylinder.CylinderName + ". Order number {?orderNumber}.";
                                String downMessage = "You wanted to purchase " + cylinder.CylinderName + " gas from Tswana Gas on {?DateTime} , but the service was down. This is to notify you the service is back up. Please try again";
                                tblThirdPartyUssdParameter par = GetThirdPartyUssdParameter(transaction);
                                if (!recordFound)
                                    InsertThirdPartyUssdParameter(productID, transaction, false, false, true, false, true, transactionDescription, false, false, "", "", "", false, thirdParty.UserID, customerResponse, downMessage, false, false, false, false, false, false, true, false, false, false, false, false, true);
                                #endregion

                                #region Pay Now. No Empty
                                logInfo += "|" + cylinder.CylinderName;
                                transaction = city.tblTswanaGasEntityCode.EntityCode + centre.tblTswanaGasEntityCode.EntityCode + cylinder.tblTswanaGasEntityCode.EntityCode + "YN";
                                functionID = 0;
                                function = GetThirdPartyFunction(transaction);
                                if (!recordFound)
                                    functionID = InsertThirdPartyFunction(thirdPartyID, transaction, transaction);
                                else
                                    functionID = (Int32)function.ThirdPartyFunctionID;

                                productTypeID = (Int32)GetThirdPartyProductType("Gas Sales").ProductTypeID;
                                productID = 0;
                                product = GetThirdPartyProduct(transaction);
                                productDesc = city.CityName + ";" + centre.CentreName + ";" + cylinder.CylinderName + "; NoEmpty";
                                if (!recordFound)
                                    productID = InsertThirdPartyProduct(thirdPartyID, productTypeID, productDesc, transaction, (Decimal)centreCylinder.CylinderPrice + emtyCylCost);
                                else
                                {
                                    productID = (Int32)product.ProductID;
                                    if (Convert.ToDecimal(product.Amount) != Convert.ToDecimal(centreCylinder.CylinderPrice) + emtyCylCost)
                                        UpdateThirdPartyProduct(transaction, Convert.ToDecimal(centreCylinder.CylinderPrice) + emtyCylCost);

                                    if (product.Product == product.UssdFunction)
                                    {
                                        product.Product = productDesc;
                                        tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
                                        crud.Update(product);
                                    }
                                }

                                transactionDescription = "Tswana Gas Sale - " + cylinder.CylinderName;
                                customerResponse = "Payment of {?amount} received for " + cylinder.CylinderName + ". Order number {?orderNumber}.";
                                downMessage = "You wanted to purchase " + cylinder.CylinderName + " gas from Tswana Gas on {?DateTime} , but the service was down. This is to notify you the service is back up. Please try again";
                                par = GetThirdPartyUssdParameter(transaction);
                                if (!recordFound)
                                    InsertThirdPartyUssdParameter(productID, transaction, false, false, true, false, true, transactionDescription, false, false, "", "", "", false, thirdParty.UserID, customerResponse, downMessage, false, false, false, false, false, false, true, false, false, false, false, false, true);
                                #endregion
                                
                                #region Pay On Delivery. With Empty

                                transaction = city.tblTswanaGasEntityCode.EntityCode + centre.tblTswanaGasEntityCode.EntityCode + cylinder.tblTswanaGasEntityCode.EntityCode + "NY";
                                functionID = 0;
                                function = GetThirdPartyFunction(transaction);
                                if (!recordFound)
                                    functionID = InsertThirdPartyFunction(thirdPartyID, transaction, transaction);
                                else
                                    functionID = (Int32)function.ThirdPartyFunctionID;

                                productTypeID = (Int32)GetThirdPartyProductType("Gas Sales").ProductTypeID;
                                productID = 0;
                                product = GetThirdPartyProduct(transaction);
                                productDesc = city.CityName + ";" + centre.CentreName + ";" + cylinder.CylinderName + "; COD; Empty";
                                if (!recordFound)
                                    productID = InsertThirdPartyProduct(thirdPartyID, productTypeID, productDesc, transaction, (Decimal)centreCylinder.CylinderPrice);
                                else
                                {
                                    productID = (Int32)product.ProductID;
                                    if (Convert.ToDecimal(product.Amount) != Convert.ToDecimal(centreCylinder.CylinderPrice))
                                        UpdateThirdPartyProduct(transaction, Convert.ToDecimal(centreCylinder.CylinderPrice));

                                    if (product.Product == product.UssdFunction)
                                    {
                                        product.Product = productDesc;
                                        tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
                                        crud.Update(product);
                                    }
                                }

                                transactionDescription = "Tswana Gas Sale - " + cylinder.CylinderName;
                                customerResponse = "Payment of {?amount} received for " + cylinder.CylinderName + ". Order number {?orderNumber}.";
                                downMessage = "You wanted to purchase " + cylinder.CylinderName + " gas from Tswana Gas on {?DateTime} , but the service was down. This is to notify you the service is back up. Please try again";
                                par = GetThirdPartyUssdParameter(transaction);
                                Int32 ussdParameterID = 0;
                                if (!recordFound)
                                    ussdParameterID = InsertThirdPartyUssdParameter(productID, transaction, false, false, false, false, false, transactionDescription, false, false, "", "", "", false, thirdParty.UserID, customerResponse, downMessage, false, false, false, false, false, false, false, false, false, false, false, false, true);
                                else
                                    ussdParameterID = Convert.ToInt32(par.UssdParameterID);
                                //Add the 'Delivery Name' Instruction
                                tblUssdInstruction ussdInstruction = GetUssdInstruction("Extract Delivery Name");
                                Int32 ussdInstructionIO = Convert.ToInt32(ussdInstruction.InstructionID);
                                List<tblThirdPartyUssdInstruction> instr = GetThirdPartyUssdInstruction(ussdParameterID, ussdInstructionIO);
                                if (instr.Count == 0)
                                {
                                    InsertThirdPartyUssdInstruction(ussdParameterID, Convert.ToInt16(ussdInstructionIO), "");
                                }



                                #endregion

                                #region Pay On Delivery. No Empty

                                transaction = city.tblTswanaGasEntityCode.EntityCode + centre.tblTswanaGasEntityCode.EntityCode + cylinder.tblTswanaGasEntityCode.EntityCode + "NN";
                                functionID = 0;
                                function = GetThirdPartyFunction(transaction);
                                if (!recordFound)
                                    functionID = InsertThirdPartyFunction(thirdPartyID, transaction, transaction);
                                else
                                    functionID = (Int32)function.ThirdPartyFunctionID;

                                productTypeID = (Int32)GetThirdPartyProductType("Gas Sales").ProductTypeID;
                                productID = 0;
                                product = GetThirdPartyProduct(transaction);
                                productDesc = city.CityName + ";" + centre.CentreName + ";" + cylinder.CylinderName + "; COD; NoEmpty";
                                if (!recordFound)
                                    productID = InsertThirdPartyProduct(thirdPartyID, productTypeID, productDesc, transaction, (Decimal)centreCylinder.CylinderPrice + emtyCylCost);
                                else
                                {
                                    productID = (Int32)product.ProductID;
                                    if (Convert.ToDecimal(product.Amount) != Convert.ToDecimal(centreCylinder.CylinderPrice) + emtyCylCost)
                                        UpdateThirdPartyProduct(transaction, Convert.ToDecimal(centreCylinder.CylinderPrice) + emtyCylCost);

                                    if (product.Product == product.UssdFunction)
                                    {
                                        product.Product = productDesc;
                                        tblThirdPartyProductCrud crud = new tblThirdPartyProductCrud();
                                        crud.Update(product);
                                    }
                                }

                                transactionDescription = "Tswana Gas Sale - " + cylinder.CylinderName;
                                customerResponse = "Payment of {?amount} received for " + cylinder.CylinderName + ". Order number {?orderNumber}.";
                                downMessage = "You wanted to purchase " + cylinder.CylinderName + " gas from Tswana Gas on {?DateTime} , but the service was down. This is to notify you the service is back up. Please try again";
                                par = GetThirdPartyUssdParameter(transaction);
                                if (!recordFound)
                                    InsertThirdPartyUssdParameter(productID, transaction, false, false, false, false, false, transactionDescription, false, false, "", "", "", false, thirdParty.UserID, customerResponse, downMessage, false, false, false, false, false, false, false, false, false, false, false, false, true);
                                else
                                    ussdParameterID = Convert.ToInt32(par.UssdParameterID);

                                //Add the 'Delivery Name' Instruction
                                ussdInstruction = GetUssdInstruction("Extract Delivery Name");
                                ussdInstructionIO = Convert.ToInt32(ussdInstruction.InstructionID);
                                instr = GetThirdPartyUssdInstruction(ussdParameterID, ussdInstructionIO);
                                if (instr.Count == 0)
                                {
                                    InsertThirdPartyUssdInstruction(ussdParameterID, Convert.ToInt16(ussdInstructionIO), "");
                                }
                                #endregion
                            }
                        }
                    }
                }
				
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("TswanaGasUssdTablePopulate - " + Ex.Message + logInfo);
			}
		}

        /* public Boolean TswanaGasDataCollect()
         {
             String infoLog = "";
             try
             {
                 #region Vairable Declaration
                 tblTswanaGasEntityCode code = new tblTswanaGasEntityCode();
                 String entityCode = "";
                 Int32 entityCodeID = 0;
                 Int32 codeCount = 0;
                 #endregion

                 #region Cylinders
                 codeCount = 0;
                 infoLog += "GetTswanaGasCylinderJSON";
                 List<TswanaGasCylinder> cylinders = GetTswanaGasCylinderJSON();
                 foreach (TswanaGasCylinder cylinder in cylinders)
                 {
                     infoLog += "|GetTswanaGasCylinder";
                     tblTswanaGasCylinder dbCylinder = GetTswanaGasCylinder(cylinder.CylinderID);
                     if (!recordFound)
                     {
                         code = GetTswanaGasEntityCode(3, cylinder.CylinderName);
                         if (!recordFound)
                         {
                             entityCode = cylinder.CylinderName.Replace(" ", "").Substring(0, 3).ToUpper();
                             recordFound = true;
                             while (recordFound)
                             {
                                 code = GetTswanaGasEntityCodeByCode(3, entityCode);
                                 if (recordFound)
                                 {
                                     codeCount++;
                                     entityCode = cylinder.CylinderName.Replace(" ", "").Substring(codeCount, 3).ToUpper();
                                 }
                             }

                             entityCodeID = InsertTswanaGasEntityCode(3, cylinder.CylinderName, entityCode);
                         }
                         else
                             entityCodeID = (Int32)code.EntityCodeID;

                         InsertTswanaGasCylinder(cylinder.CylinderID, cylinder.CylinderName, entityCodeID, 19634);
                     }


                     List<tblTswanaGasCylinder> dbCylinders = GetTswanaGasCylinder();
                     foreach (tblTswanaGasCylinder cyl in dbCylinders)
                     {
                         List<TswanaGasCylinder> cylCheck = cylinders.FindAll(x => x.CylinderName == cyl.CylinderName);
                         if (cylCheck.Count == 0)
                             UpdateTswanaGasCylinderActive((Int32)cyl.CylinderID, false, 19634);
                         else
                             if (!(Boolean)cyl.Active)
                             UpdateTswanaGasCylinderActive((Int32)cyl.CylinderID, true, 19634);
                     }
                 }
                 #endregion

                 #region Cities
                 List<TswanaGasCity> cities = GetTswanaGasCityJSON();
                 foreach (TswanaGasCity city in cities)
                 {
                     tblTswanaGasCity dbCity = GetTswanaGasCity(city.CityID);
                     if (!recordFound)
                     {
                         code = GetTswanaGasEntityCode(1, city.CityName);
                         if (!recordFound)
                         {
                             entityCode = city.CityName.Substring(0, 3).ToUpper();
                             recordFound = true;
                             while (recordFound)
                             {
                                 code = GetTswanaGasEntityCodeByCode(1, entityCode);
                                 if (recordFound)
                                 {
                                     codeCount++;
                                     entityCode = city.CityName.Substring(codeCount, 3).ToUpper();
                                 }
                             }

                             entityCodeID = InsertTswanaGasEntityCode(1, city.CityName, entityCode);
                         }
                         else
                             entityCodeID = (Int32)code.EntityCodeID;

                         InsertTswanaGasCity(city.CityID, city.CityName, entityCodeID, 19634);
                     }
                     else
                     {
                         if (dbCity.CityName != city.CityName)
                         {
                             UpdateTswanaGasCity(city.CityID, city.CityName, 19634);
                         }
                     }
                     #endregion

                     #region Distribution Centres
                     codeCount = 0;
                     List<TswanaGasCentre> centres = GetTswanaGasCentreJSON(city.CityID);
                     if (centres.Count == 0)
                     {
                         UpdateTswanaGasCityActive(city.CityID, false, 19634);
                         List<tblTswanaGasCentre> cenAct = GetTswanaGasCentre(city.CityID);
                         foreach (tblTswanaGasCentre cen in cenAct)
                             UpdateTswanaGasCentre((Int32)cen.CentreID, false, 19634);

                     }
                     else
                     {
                         if (dbCity != null)
                         {
                             if (!(Boolean)dbCity.Active)
                             {
                                 UpdateTswanaGasCityActive(city.CityID, true, 19634);
                             }
                         }
                     }

                     infoLog = "Centres Found=" + centres.Count.ToString();
                     foreach (TswanaGasCentre centre in centres)
                     {
                         infoLog += "| WorkingWith=" + centre.CentreName;
                         tblTswanaGasCentre dbCentre = GetTswanaGasCentreByID(centre.CentreID);
                         Int32 centreID = 0;
                         if (!recordFound)
                         {
                             infoLog += "|CentreNotFound";
                             code = GetTswanaGasEntityCode(2, centre.CentreName);
                             if (!recordFound)
                             {
                                 infoLog += "|EntityCodeNotFound";
                                 entityCode = centre.CentreName.Substring(0, 3).ToUpper();
                                 recordFound = true;
                                 while (recordFound)
                                 {
                                     code = GetTswanaGasEntityCodeByCode(2, entityCode);
                                     if (recordFound)
                                     {
                                         infoLog += "|EntityCodeAlreadyThere";
                                         codeCount++;
                                         entityCode = centre.CentreName.Substring(codeCount, 3).ToUpper();
                                     }
                                 }
                                 infoLog += "|InsertEntityCode=" + entityCode;
                                 entityCodeID = InsertTswanaGasEntityCode(2, centre.CentreName, entityCode);
                             }
                             else
                                 entityCodeID = (Int32)code.EntityCodeID;

                             infoLog += "|CityID=" + city.CityID.ToString() + "| CentreID=" + centre.CentreID + "| CentreName=" + centre.CentreName + "| EntityCodeID=" + entityCodeID.ToString();
                             InsertTswanaGasCentre(city.CityID, centre.CentreID, centre.CentreName, entityCodeID, 19634);




                         }
                         else
                         {
                             centreID = Convert.ToInt32(dbCentre.CentreID);
                             if (dbCentre.CentreName != centre.CentreName)
                                 UpdateTswanaGasCentre(centre.CentreID, centre.CentreName, 19634);

                             if ((Int32)dbCentre.CityID != (Int32)city.CityID)
                                 UpdateTswanaGasCentre(centre.CentreID, city.CityID, 19634);
                         }

                         tblTswanaGasCentreCylinder cenCyl = GetTswanaGasCentreCylinder(centre.CentreID, centre.CylinderID);
                         if (!recordFound)
                             InsertTswanaGasCentreCylinder(centre.CentreID, centre.CylinderID, centre.CylinderPrice);
                         else
                             UpdateTswanaGasCentreCylinder(centre.CentreID, centre.CylinderID, centre.CylinderPrice);

                     }
                     if (dbCity != null)
                     {
                         List<tblTswanaGasCentre> dbCentres = GetTswanaGasCentre((Int32)dbCity.CityID);
                         foreach (tblTswanaGasCentre workCentre in dbCentres)
                         {
                             List<TswanaGasCentre> thisCentre = centres.FindAll(x => x.CentreName == workCentre.CentreName);
                             if (thisCentre.Count == 0)
                                 UpdateTswanaGasCentre((Int32)workCentre.CentreID, false, 19634);
                             else
                                 UpdateTswanaGasCentre((Int32)workCentre.CentreID, true, 19634);
                         }
                     }
                 }

                 List<tblTswanaGasCity> dbCities = GetTswanaGasCity();
                 foreach (tblTswanaGasCity workCity in dbCities)
                 {
                     List<TswanaGasCity> findCity = cities.FindAll(x => x.CityName == workCity.CityName);
                     if (findCity.Count == 0)
                         UpdateTswanaGasCityActive((Int32)workCity.CityID, false, 19634);
                     else
                         if (!(Boolean)workCity.Active)
                         UpdateTswanaGasCityActive((Int32)workCity.CityID, true, 19634);
                 }
                 #endregion
                 return true;
             }
             catch (Exception Ex)
             {
                 throw new Exception("TswanaGasDataCollect - " + Ex.Message + "| InfoLog=" + infoLog);
             }
         }*/

        public List<TswanaGasCentre> GetTswanaGasCentreJSON(Int32 pCityID)
        {
            try
            {
                tblSetting appSetting = GetSetting("Back Office", "DPO.BackOffice.Ussd", "CentreUrl");
                String pUrl = appSetting.SettingValue;
                pUrl += "?city=" + pCityID.ToString();
                List<TswanaGasCentre> centres = new List<TswanaGasCentre>();
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(pUrl);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                String result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                if (result.Contains("Please provide City id"))
                    return centres;

                if (!result.Contains("[") && !result.Contains("]"))
                {
                    if (result.Contains("No Distribution Point Available"))
                        return centres;

                    throw new Exception(result);
                }

                result = result.Replace("[", "").Replace("]", "").Replace("\"id\":", "").Replace("\"name\":", "").Replace("\"", "").Replace("cylid:", "").Replace("type:", "").Replace("price:", "");

                String[] centrs = result.Split('}');

                for (Int32 citLoop = 0; citLoop < centrs.GetUpperBound(0); citLoop++)
                {
                    centrs[citLoop] = centrs[citLoop].Replace(",{", "").Replace("{", "");
                }

                for (Int32 citLoop = 0; citLoop < centrs.GetUpperBound(0); citLoop++)
                {
                    if (centrs[citLoop] != "")
                    {
                        String[] workCentre = centrs[citLoop].Split(',');
                        Int32 cityID = Convert.ToInt32(workCentre[0]);
                        String cityName = workCentre[1];
                        Int32 cylinderID = Convert.ToInt32(workCentre[2]);
                        String cylinderType = workCentre[3];
                        Decimal cylinderPrice = Convert.ToDecimal(workCentre[4], System.Globalization.CultureInfo.InvariantCulture);
                        TswanaGasCentre centre = new TswanaGasCentre();
                        centre.CentreID = cityID;
                        centre.CentreName = cityName;
                        centre.CylinderID = cylinderID;
                        centre.CylinderPrice = cylinderPrice;
                        centre.CylinderType = cylinderType;
                        centres.Add(centre);
                    }
                }


                return centres;
            }
            catch (System.Net.WebException webEx)
            {
                if (webEx.Response != null)
                {
                    var resp = new System.IO.StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
                    dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject(resp);
                    String messageFromServer = obj.error.ToString();

                    throw new Exception("Error=" + messageFromServer);
                }
                else
                    throw new Exception("Error=" + webEx.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentre-" + Ex.Message);
            }

        }
        public Boolean TswanaGasDataCollect()
		{
            String infoLog = "";
			try
			{
                #region Vairable Declaration
                tblTswanaGasEntityCode code = new tblTswanaGasEntityCode();
				String entityCode = "";
				Int32 entityCodeID = 0;
				Int32 codeCount = 0;
                #endregion

                #region Cylinders
                codeCount = 0;
                infoLog += "GetTswanaGasCylinderJSON";
                List<TswanaGasCylinder> cylinders = GetTswanaGasCylinderJSON();
                foreach (TswanaGasCylinder cylinder in cylinders)
                {
                    infoLog += "|GetTswanaGasCylinder";
                    tblTswanaGasCylinder dbCylinder = GetTswanaGasCylinder(cylinder.CylinderID);
                    if (!recordFound)
                    {
                        code = GetTswanaGasEntityCode(3, cylinder.CylinderName);
                        if (!recordFound)
                        {
                            entityCode = cylinder.CylinderName.Replace(" ", "").Substring(0, 3).ToUpper();
                            recordFound = true;
                            while (recordFound)
                            {
                                code = GetTswanaGasEntityCodeByCode(3, entityCode);
                                if (recordFound)
                                {
                                    codeCount++;
                                    entityCode = cylinder.CylinderName.Replace(" ", "").Substring(codeCount, 3).ToUpper();
                                }
                            }

                            entityCodeID = InsertTswanaGasEntityCode(3, cylinder.CylinderName, entityCode);
                        }
                        else
                            entityCodeID = (Int32)code.EntityCodeID;

                        InsertTswanaGasCylinder(cylinder.CylinderID, cylinder.CylinderName, entityCodeID, 19634);
                    }


                    List<tblTswanaGasCylinder> dbCylinders = GetTswanaGasCylinder();
                    foreach (tblTswanaGasCylinder cyl in dbCylinders)
                    {
                        List<TswanaGasCylinder> cylCheck = cylinders.FindAll(x => x.CylinderName == cyl.CylinderName);
                        if (cylCheck.Count == 0)
                            UpdateTswanaGasCylinderActive((Int32)cyl.CylinderID, false, 19634);
                        else
                            if (!(Boolean)cyl.Active)
                            UpdateTswanaGasCylinderActive((Int32)cyl.CylinderID, true, 19634);
                    }
                }
                #endregion

                #region Cities
                List<TswanaGasCity> cities = GetTswanaGasCityJSON();
                foreach (TswanaGasCity city in cities)
				{
					tblTswanaGasCity dbCity = GetTswanaGasCity(city.CityID);
					if (!recordFound)
					{
						code = GetTswanaGasEntityCode(1, city.CityName);
						if (!recordFound)
						{
							entityCode = city.CityName.Substring(0, 3).ToUpper();
							recordFound = true;
							while (recordFound)
							{
								code = GetTswanaGasEntityCodeByCode(1, entityCode);
								if (recordFound)
								{
									codeCount++;
									entityCode = city.CityName.Substring(codeCount, 3).ToUpper();
								}
							}

							entityCodeID = InsertTswanaGasEntityCode(1, city.CityName, entityCode);
						}
						else
							entityCodeID = (Int32)code.EntityCodeID;

						InsertTswanaGasCity(city.CityID, city.CityName, entityCodeID, 19634);
					}
					else
					{
						if (dbCity.CityName != city.CityName)
						{
							UpdateTswanaGasCity(city.CityID, city.CityName, 19634);
						}
					}
                    #endregion

                #region Distribution Centres
                    codeCount = 0;
					List<TswanaGasCentre> centres = GetTswanaGasCentreJSON(city.CityID);
                    if (centres.Count == 0)
                    {
                        UpdateTswanaGasCityActive(city.CityID, false, 19634);
                        List<tblTswanaGasCentre> cenAct = GetTswanaGasCentre(city.CityID);
                        foreach (tblTswanaGasCentre cen in cenAct)
                            UpdateTswanaGasCentre((Int32)cen.CentreID, false, 19634);

                    }
                    else
                    {
                        if (dbCity != null)
                        {
                            if (!(Boolean)dbCity.Active)
                            {
                                UpdateTswanaGasCityActive(city.CityID, true, 19634);
                            }
                        }
                    }

                    infoLog = "Centres Found=" + centres.Count.ToString();
                    foreach (TswanaGasCentre centre in centres)
					{
                        infoLog += "| WorkingWith=" + centre.CentreName;
                        tblTswanaGasCentre dbCentre = GetTswanaGasCentreByID(centre.CentreID);
                        Int32 centreID = 0;
						if (!recordFound)
						{
                            infoLog += "|CentreNotFound";
							code = GetTswanaGasEntityCode(2, centre.CentreName);
                            if (!recordFound)
                            {
                                infoLog += "|EntityCodeNotFound";
                                entityCode = centre.CentreName.Substring(0, 3).ToUpper();
                                recordFound = true;
                                while (recordFound)
                                {
                                    code = GetTswanaGasEntityCodeByCode(2, entityCode);
                                    if (recordFound)
                                    {
                                        infoLog += "|EntityCodeAlreadyThere";
                                        codeCount++;
                                        entityCode = centre.CentreName.Substring(codeCount, 3).ToUpper();
                                    }
                                }
                                infoLog += "|InsertEntityCode=" + entityCode;
                                entityCodeID = InsertTswanaGasEntityCode(2, centre.CentreName, entityCode);
                            }
                            else
                                entityCodeID = (Int32)code.EntityCodeID;
                            
                            infoLog += "|CityID=" + city.CityID.ToString() + "| CentreID=" + centre.CentreID + "| CentreName=" + centre.CentreName + "| EntityCodeID=" + entityCodeID.ToString();
							InsertTswanaGasCentre(city.CityID, centre.CentreID, centre.CentreName, entityCodeID, 19634);

                            


                        }
                        else
                        {
                            centreID = Convert.ToInt32(dbCentre.CentreID);
                            if (dbCentre.CentreName != centre.CentreName)
                                UpdateTswanaGasCentre(centre.CentreID, centre.CentreName, 19634);

                            if ((Int32)dbCentre.CityID != (Int32)city.CityID)
                                UpdateTswanaGasCentre(centre.CentreID, city.CityID, 19634);
                        }

                        tblTswanaGasCentreCylinder cenCyl = GetTswanaGasCentreCylinder(centre.CentreID, centre.CylinderID);
                        if (!recordFound)
                            InsertTswanaGasCentreCylinder(centre.CentreID, centre.CylinderID, centre.CylinderPrice);
                        else
                            UpdateTswanaGasCentreCylinder(centre.CentreID, centre.CylinderID, centre.CylinderPrice);

                    }
                    if (dbCity != null)
                    {
                        List<tblTswanaGasCentre> dbCentres = GetTswanaGasCentre((Int32)dbCity.CityID);
                        foreach (tblTswanaGasCentre workCentre in dbCentres)
                        {
                            List<TswanaGasCentre> thisCentre = centres.FindAll(x => x.CentreName == workCentre.CentreName);
                            if (thisCentre.Count == 0)
                                UpdateTswanaGasCentre((Int32)workCentre.CentreID, false, 19634);
                            else
                                UpdateTswanaGasCentre((Int32)workCentre.CentreID, true, 19634);
                        }
                    }
				}

				List<tblTswanaGasCity> dbCities = GetTswanaGasCity();
				foreach (tblTswanaGasCity workCity in dbCities)
				{
					List<TswanaGasCity> findCity = cities.FindAll(x => x.CityName == workCity.CityName);
					if (findCity.Count == 0)
						UpdateTswanaGasCityActive((Int32)workCity.CityID, false, 19634);
                    else
                        if (!(Boolean)workCity.Active)
                            UpdateTswanaGasCityActive((Int32)workCity.CityID, true, 19634);
                }
                #endregion
                return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("TswanaGasDataCollect - " + Ex.Message + "| InfoLog=" + infoLog);
			}
		}




        public String ExportTswanaGasUssd()
		{
			String infoLog = "";
			try
			{

                Decimal emtyCylCost = GetTswanaGasEmptyCylinder();

                Int32 cityCount = 0; Int32 centreCount = 0; Int32 cylinderCount = 0;
				tblThirdParty thirdParty = GetThirdPartyByName("Tswana Gas");
				List<tblUssdGateway> gateways = GetUssdGateway();
				StringBuilder exportReg = new StringBuilder();
				foreach (tblUssdGateway gateway in gateways)
				{
					infoLog = "Gateway-" + gateway.GatewayName;

                    if (gateway.GatewayName == "VgiTruteqUssd") break;
					exportReg.Clear();
					infoLog += "| GetUssdMenuThirdParty| ThirdPartyID=" + thirdParty.ThirdPartyID.ToString() + "| GatewayID=" + gateway.GatewayID.ToString();
					tblUssdMenuThirdParty ussdThirdPartyMenu = GetUssdMenuThirdParty((Int32)thirdParty.ThirdPartyID, (Int32)gateway.GatewayID);
					infoLog += "|GetUssdMenuThirdParty Done";
					String thirdPartyMenu = ussdThirdPartyMenu.MenuDescription;
					String httpPostUrl = ussdThirdPartyMenu.tblUssdHttpPost.Url;

					String transaction = "";
					infoLog += "| GetTswanaGasCity";
					List<tblTswanaGasCity> cities = GetTswanaGasCity();
					cityCount = 0;
                    foreach (tblTswanaGasCity city in cities)
                    {
                        if ((Boolean)city.Active)
                        {
                            cityCount++;
                            transaction = city.tblTswanaGasEntityCode.EntityCode;

                            String registryKey = gateway.RegistryKey + "\\" + thirdPartyMenu + "\\" + cityCount.ToString() + " " + city.CityName;
                            exportReg.AppendLine("[" + registryKey + "]");
                            exportReg.AppendLine("\"MenuItemNr\"=\"" + cityCount.ToString() + "\"");
                            exportReg.AppendLine("\"MenuItemText\"=\"" + city.CityName + "\"");
                            exportReg.AppendLine("\"WelcomeMsg\"=\"Select nearest distribution centre\"");
                            exportReg.AppendLine(String.Empty);

                            infoLog += "| GetTswanaGasCentre (" + city.CityName + ")";
                            List<tblTswanaGasCentre> centres = GetTswanaGasCentre((Int32)city.CityID);
                            centreCount = 0;
                            foreach (tblTswanaGasCentre centre in centres)
                            {
                                if ((Boolean)centre.Active)
                                {
                                    infoLog += "|Centre=" + centre.CentreName;
                                    centreCount++;
                                    transaction += centre.tblTswanaGasEntityCode.EntityCode;

                                    registryKey = gateway.RegistryKey + "\\" + thirdPartyMenu + "\\" + cityCount.ToString() + " " + city.CityName + "\\" + centreCount + " " + centre.CentreName;
                                    exportReg.AppendLine("[" + registryKey + "]");
                                    exportReg.AppendLine("\"MenuItemNr\"=\"" + centreCount.ToString() + "\"");
                                    exportReg.AppendLine("\"MenuItemText\"=\"" + centre.CentreName + "\"");
                                    exportReg.AppendLine("\"WelcomeMsg\"=\"Do you have an empty cilinder to exchange?\"");
                                    exportReg.AppendLine(String.Empty);

                                    registryKey = gateway.RegistryKey + "\\" + thirdPartyMenu + "\\" + cityCount.ToString() + " " + city.CityName + "\\" + centreCount + " " + centre.CentreName + "\\1 EmptyYes";
                                    exportReg.AppendLine("[" + registryKey + "]");
                                    exportReg.AppendLine("\"MenuItemNr\"=\"1\"");
                                    exportReg.AppendLine("\"MenuItemText\"=\"Yes\"");
                                    exportReg.AppendLine("\"WelcomeMsg\"=\"Select Cylinder Size\"");
                                    exportReg.AppendLine(String.Empty);

                                    infoLog += "| GetTswanaGasCylinder";
                                    List<tblTswanaGasCylinder> cylinders = GetTswanaGasCylinder();
                                    cylinderCount = 0;
                                    tblTswanaGasCentreCylinder centreCyl = new tblTswanaGasCentreCylinder();
                                    foreach (tblTswanaGasCylinder cylinder in cylinders)
                                    {
                                        if ((Boolean)cylinder.Active)
                                        {
                                            centreCyl = GetTswanaGasCentreCylinder(Convert.ToInt32(centre.CentreID), Convert.ToInt32(cylinder.CylinderID));
                                            infoLog += "| Cylinder=" + cylinder.CylinderName;
                                            cylinderCount++;
                                            String cylinderPrice = Convert.ToDecimal(centreCyl.CylinderPrice).ToString();
                                            cylinderPrice = cylinderPrice.Substring(0, cylinderPrice.IndexOf("."));
                                            registryKey = gateway.RegistryKey + "\\" + thirdPartyMenu + "\\" + cityCount.ToString() + " " + city.CityName + "\\" + centreCount + " " + centre.CentreName + "\\1 EmptyYes\\" + cylinderCount.ToString() + " " + cylinder.CylinderName;
                                            exportReg.AppendLine("[" + registryKey + "]");
                                            exportReg.AppendLine("\"MenuItemNr\"=\"" + cylinderCount.ToString() + "\"");
                                            exportReg.AppendLine("\"MenuItemText\"=\"" + cylinder.CylinderName + " (P " + cylinderPrice + ")" + "\"");
                                            exportReg.AppendLine("\"WelcomeMsg\"=\"Pay now or pay on delivery\"");
                                            exportReg.AppendLine(String.Empty);

                                            registryKey = gateway.RegistryKey + "\\" + thirdPartyMenu + "\\" + cityCount.ToString() + " " + city.CityName + "\\" + centreCount + " " + centre.CentreName + "\\1 EmptyYes\\" + cylinderCount.ToString() + " " + cylinder.CylinderName + "\\1 Pay Now";
                                            transaction = city.tblTswanaGasEntityCode.EntityCode + centre.tblTswanaGasEntityCode.EntityCode + cylinder.tblTswanaGasEntityCode.EntityCode + "YY";
                                            exportReg.AppendLine("[" + registryKey + "]");
                                            exportReg.AppendLine("\"MenuItemNr\"=\"1\"");
                                            exportReg.AppendLine("\"MenuItemText\"=\"Pay Now\"");
                                            exportReg.AppendLine("\"WelcomeMsg\"=\"Tswana Gas\"");
                                            exportReg.AppendLine("\"Confirm\"=\"Y\"");
                                            exportReg.AppendLine("\"ConfirmText\"=\"Card\"");
                                            exportReg.AppendLine("\"LeadInText\"=\"Enter the bank card number\"");
                                            exportReg.AppendLine("\"Prompt\"=\"Y\"");
                                            exportReg.AppendLine("\"TagName\"=\"CardNumber\"");
                                            exportReg.AppendLine("\"Transaction\"=\"" + transaction + "\"");
                                            exportReg.AppendLine(String.Empty);

                                            registryKey += "\\Expiry";
                                            exportReg.AppendLine("[" + registryKey + "]");
                                            exportReg.AppendLine("\"WelcomeMsg\"=\"Tswana Gas\"");
                                            exportReg.AppendLine("\"Confirm\"=\"Y\"");
                                            exportReg.AppendLine("\"ConfirmText\"=\"Expiry\"");
                                            exportReg.AppendLine("\"LeadInText\"=\"Enter Expiry MMYY\"");
                                            exportReg.AppendLine("\"Prompt\"=\"Y\"");
                                            exportReg.AppendLine("\"TagName\"=\"ExpiryDate\"");
                                            exportReg.AppendLine(String.Empty);
                                            registryKey += "\\Cvv";
                                            exportReg.AppendLine("[" + registryKey + "]");
                                            exportReg.AppendLine("\"WelcomeMsg\"=\"Tswana Gas\"");
                                            exportReg.AppendLine("\"Confirm\"=\"Y\"");
                                            exportReg.AppendLine("\"ConfirmText\"=\"Cvv\"");
                                            exportReg.AppendLine("\"LeadInText\"=\"Cvv (Last 3 digits from signature panel)\"");
                                            exportReg.AppendLine("\"Prompt\"=\"Y\"");
                                            exportReg.AppendLine("\"TagName\"=\"Cvc\"");
                                            exportReg.AppendLine("\"DisableJumpIn\"=\"Y\"");
                                            exportReg.AppendLine("\"HttpPost\"=\"" + httpPostUrl + "\"");
                                            exportReg.AppendLine("\"UseLegacyPosting\"=\"1\"");
                                            exportReg.AppendLine(String.Empty);

                                            transaction = transaction.Substring(0, transaction.Length - 2) + "NY";
                                            registryKey = gateway.RegistryKey + "\\" + thirdPartyMenu + "\\" + cityCount.ToString() + " " + city.CityName + "\\" + centreCount + " " + centre.CentreName + "\\1 EmptyYes\\" + cylinderCount.ToString() + " " + cylinder.CylinderName + "\\2 Pay On Delivery";
                                            exportReg.AppendLine("[" + registryKey + "]");
                                            exportReg.AppendLine("\"WelcomeMsg\"=\"Tswana Gas\"");
                                            exportReg.AppendLine("\"DisableJumpIn\"=\"Y\"");
                                            exportReg.AppendLine("\"HttpPost\"=\"" + httpPostUrl + "\"");
                                            exportReg.AppendLine("\"UseLegacyPosting\"=\"1\"");
                                            exportReg.AppendLine("\"Transaction\"=\"" + transaction + "\"");
                                            exportReg.AppendLine("\"MenuItemNr\"=\"2\"");
                                            exportReg.AppendLine("\"MenuItemText\"=\"Pay on delivery (Credit or Debit Card/Cash\"");
                                            exportReg.AppendLine("\"LeadInText\"=\"Enter your name\"");
                                            exportReg.AppendLine("\"Prompt\"=\"Y\"");
                                            exportReg.AppendLine("\"TagName\"=\"DeliveryName\"");

                                            exportReg.AppendLine(String.Empty);

                                        }
                                    }
                                    registryKey = gateway.RegistryKey + "\\" + thirdPartyMenu + "\\" + cityCount.ToString() + " " + city.CityName + "\\" + centreCount + " " + centre.CentreName + "\\2 EmptyNo";
                                    exportReg.AppendLine("[" + registryKey + "]");
                                    exportReg.AppendLine("\"MenuItemNr\"=\"2\"");
                                    exportReg.AppendLine("\"MenuItemText\"=\"No\"");
                                    exportReg.AppendLine("\"WelcomeMsg\"=\"Select Cylinder Size\"");
                                    exportReg.AppendLine(String.Empty);

                                    infoLog += "| GetTswanaGasCylinder";
                                    cylinderCount = 0;
                                    foreach (tblTswanaGasCylinder cylinder in cylinders)
                                    {
                                        centreCyl = GetTswanaGasCentreCylinder(Convert.ToInt32(centre.CentreID), Convert.ToInt32(cylinder.CylinderID));
                                        if ((Boolean)cylinder.Active)
                                        {
                                            infoLog += "| Cylinder=" + cylinder.CylinderName;
                                            cylinderCount++;
                                            String cylinderPrice = Convert.ToDecimal(centreCyl.CylinderPrice).ToString();
                                            cylinderPrice = cylinderPrice.Substring(0, cylinderPrice.IndexOf("."));
                                            String cylPrice = "(P " + cylinderPrice + " + P " + emtyCylCost.ToString() + " cylinder deposit)";
                                            if (gateway.GatewayID == 3)
                                            {
                                                Decimal cPrice = (Decimal)centreCyl.CylinderPrice + emtyCylCost;
                                                cylPrice = " (P " + Convert.ToString(cPrice).Substring(0, cPrice.ToString().Length - 5) + ")";
                                            }
                                            registryKey = gateway.RegistryKey + "\\" + thirdPartyMenu + "\\" + cityCount.ToString() + " " + city.CityName + "\\" + centreCount + " " + centre.CentreName + "\\2 EmptyNo\\" + cylinderCount.ToString() + " " + cylinder.CylinderName;
                                            exportReg.AppendLine("[" + registryKey + "]");
                                            exportReg.AppendLine("\"MenuItemNr\"=\"" + cylinderCount.ToString() + "\"");
                                            exportReg.AppendLine("\"MenuItemText\"=\"" + cylinder.CylinderName + cylPrice + "\"");
                                            exportReg.AppendLine("\"WelcomeMsg\"=\"Pay now or pay on delivery\"");
                                            exportReg.AppendLine(String.Empty);

                                            registryKey = gateway.RegistryKey + "\\" + thirdPartyMenu + "\\" + cityCount.ToString() + " " + city.CityName + "\\" + centreCount + " " + centre.CentreName + "\\2 EmptyNo\\" + cylinderCount.ToString() + " " + cylinder.CylinderName + "\\1 Pay Now";
                                            transaction = city.tblTswanaGasEntityCode.EntityCode + centre.tblTswanaGasEntityCode.EntityCode + cylinder.tblTswanaGasEntityCode.EntityCode + "YN";
                                            exportReg.AppendLine("[" + registryKey + "]");
                                            exportReg.AppendLine("\"MenuItemNr\"=\"1\"");
                                            exportReg.AppendLine("\"MenuItemText\"=\"Pay Now\"");
                                            exportReg.AppendLine("\"WelcomeMsg\"=\"Tswana Gas\"");
                                            exportReg.AppendLine("\"Confirm\"=\"Y\"");
                                            exportReg.AppendLine("\"ConfirmText\"=\"Card\"");
                                            exportReg.AppendLine("\"LeadInText\"=\"Enter the bank card number\"");
                                            exportReg.AppendLine("\"Prompt\"=\"Y\"");
                                            exportReg.AppendLine("\"TagName\"=\"CardNumber\"");
                                            exportReg.AppendLine("\"Transaction\"=\"" + transaction + "\"");
                                            exportReg.AppendLine(String.Empty);

                                            registryKey += "\\Expiry";
                                            exportReg.AppendLine("[" + registryKey + "]");
                                            exportReg.AppendLine("\"WelcomeMsg\"=\"Tswana Gas\"");
                                            exportReg.AppendLine("\"Confirm\"=\"Y\"");
                                            exportReg.AppendLine("\"ConfirmText\"=\"Expiry\"");
                                            exportReg.AppendLine("\"LeadInText\"=\"Enter Expiry MMYY\"");
                                            exportReg.AppendLine("\"Prompt\"=\"Y\"");
                                            exportReg.AppendLine("\"TagName\"=\"ExpiryDate\"");
                                            exportReg.AppendLine(String.Empty);
                                            registryKey += "\\Cvv";
                                            exportReg.AppendLine("[" + registryKey + "]");
                                            exportReg.AppendLine("\"WelcomeMsg\"=\"Tswana Gas\"");
                                            exportReg.AppendLine("\"Confirm\"=\"Y\"");
                                            exportReg.AppendLine("\"ConfirmText\"=\"Cvv\"");
                                            exportReg.AppendLine("\"LeadInText\"=\"Cvv (Last 3 digits from signature panel)\"");
                                            exportReg.AppendLine("\"Prompt\"=\"Y\"");
                                            exportReg.AppendLine("\"TagName\"=\"Cvc\"");
                                            exportReg.AppendLine("\"DisableJumpIn\"=\"Y\"");
                                            exportReg.AppendLine("\"HttpPost\"=\"" + httpPostUrl + "\"");
                                            exportReg.AppendLine("\"UseLegacyPosting\"=\"1\"");
                                            exportReg.AppendLine(String.Empty);

                                            transaction = transaction.Substring(0, transaction.Length - 2) + "NN";
                                            registryKey = gateway.RegistryKey + "\\" + thirdPartyMenu + "\\" + cityCount.ToString() + " " + city.CityName + "\\" + centreCount + " " + centre.CentreName + "\\2 EmptyNo\\" + cylinderCount.ToString() + " " + cylinder.CylinderName + "\\2 Pay On Delivery";
                                            exportReg.AppendLine("[" + registryKey + "]");
                                            exportReg.AppendLine("\"WelcomeMsg\"=\"Tswana Gas\"");
                                            exportReg.AppendLine("\"DisableJumpIn\"=\"Y\"");
                                            exportReg.AppendLine("\"HttpPost\"=\"" + httpPostUrl + "\"");
                                            exportReg.AppendLine("\"UseLegacyPosting\"=\"1\"");
                                            exportReg.AppendLine("\"Transaction\"=\"" + transaction + "\"");
                                            exportReg.AppendLine("\"MenuItemNr\"=\"2\"");
                                            exportReg.AppendLine("\"MenuItemText\"=\"Pay on delivery (Credit or Debit Card/Cash\"");
                                            exportReg.AppendLine("\"LeadInText\"=\"Enter your name\"");
                                            exportReg.AppendLine("\"Prompt\"=\"Y\"");
                                            exportReg.AppendLine("\"TagName\"=\"DeliveryName\"");

                                            exportReg.AppendLine(String.Empty);

                                        }
                                    }

                                }
                            }
                        }
                    }

                    
					if (exportReg.Length != 0)
					{
                        //if (TswanaGasHasChanged())
                        //{
                            exportReg.Insert(0, "Windows Registry Editor Version 5.00\n\n");
                            //System.IO.StreamWriter writer = new StreamWriter("C:\\VCS\\TswanaGas" + gateway.GatewayName + ".reg");
                            //writer.WriteLine(exportReg.ToString());
                            //writer.Close();
                            UpdateUssdRegistry(exportReg.ToString());
                        //}
					}
					else
						throw new Exception("No keys generated!");
				}

				return exportReg.ToString();

			}
			catch (Exception Ex)
			{
				throw new Exception("ExportTswanaGasUssd-" + Ex.Message + "| InfoLog=" + infoLog);
			}
		}
        public Boolean TswanaGasHasChanged()
        {
            try
            {
                Int32 tableCityID = (Int32)GetTable(4, "tblTswanaGasCity").TableID;
                Int32 tableCentreID = (Int32)GetTable(4, "tblTswanaGasCentre").TableID;
                Int32 tableCylinderID = (Int32)GetTable(4, "tblTswanaGasCylinder").TableID;
                List<tblAuditHeader> header = GetAuditHeader(tableCityID, DateTime.Today);
                if (header.Count != 0) return true;

                header = GetAuditHeader(tableCentreID, DateTime.Today);
                if (header.Count != 0) return true;

                header = GetAuditHeader(tableCylinderID, DateTime.Today);
                if (header.Count != 0) return true;

                return false;

            }
            catch (Exception Ex)
            {
                throw new Exception("TswanaGasHasChanged - " + Ex.Message);
            }
        }
		public Boolean InsertTswanaGasCylinder(Int32 pCylinderID, String pCylinderName, Int32 pEntityCodeID, Int32 pUserID)
		{
			try
			{
				tblTswanaGasCylinder cyl = new tblTswanaGasCylinder();
				cyl.CylinderID = pCylinderID;
				cyl.CylinderName = pCylinderName;
				cyl.EntityCodeID = pEntityCodeID;
                cyl.UserID = pUserID;
                cyl.Active = true;
				tblTswanaGasCylinderCrud crud = new tblTswanaGasCylinderCrud();
				crud.Insert(cyl);
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertTswanaGasCylinder - " + Ex.Message);
			}
		}

        

        public tblTswanaGasCentreCylinder GetTswanaGasCentreCylinder(Int32 pCentreID, Int32 pCylinderID)
        {
            try
            {
                tblTswanaGasCentreCylinderCrud crud = new tblTswanaGasCentreCylinderCrud();
                crud.Where("CentreID", DAL.General.Operator.Equals , pCentreID);
                crud.And("CylinderID", DAL.General.Operator.Equals, pCylinderID);
                recordFound = false;
                tblTswanaGasCentreCylinder cenCyl = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return cenCyl;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentreCylinder - " + Ex.Message);
            }
        }



        public List<tblTswanaGasCentreCylinder> GetTswanaGasCentreCylinder(Int32 pCentreID)
        {
            try
            {
                tblTswanaGasCentreCylinderCrud crud = new tblTswanaGasCentreCylinderCrud();
                crud.Where("CentreID", DAL.General.Operator.Equals, pCentreID);
                recordFound = false;
                List<tblTswanaGasCentreCylinder> cenCyl = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;

                return cenCyl;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentreCylinder - " + Ex.Message);
            }
        }
        public Boolean UpdateTswanaGasCentreCylinder(Int32 pCentreID, Int32 pCylinderID, Decimal pCylinderPrice)
        {
            try
            {
                tblTswanaGasCentreCylinder cenCyl = GetTswanaGasCentreCylinder(pCentreID, pCylinderID);
                cenCyl.CylinderPrice = pCylinderPrice;
                tblTswanaGasCentreCylinderCrud crud = new tblTswanaGasCentreCylinderCrud();
                crud.Update(cenCyl);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateTswanaGasCentreCylinder - " + Ex.Message);
            }
        }

        
        public Int32 InsertTswanaGasCentreCylinder(Int32 pCentreID, Int32 pCylinderID, Decimal pCylinderPrice)
        {
            try
            {
                tblTswanaGasCentreCylinderCrud crud = new tblTswanaGasCentreCylinderCrud();
                tblTswanaGasCentreCylinder cenCyl = new tblTswanaGasCentreCylinder();
                cenCyl.CentreID = pCentreID;
                cenCyl.CylinderID = pCylinderID;
                cenCyl.CylinderPrice = pCylinderPrice;
                long id = 0;
                crud.Insert(cenCyl, out id);

                return (Int16)id;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTswanaGasCentreCylinder - " + Ex.Message);
            }
        }



        public Boolean InsertTswanaGasCentre(Int32 pCityID, Int32 pCentreID, String pCentreName, Int32 pEntityCodeID, Int32 pUserID)
		{
			try
			{
				tblTswanaGasCentre centre = new tblTswanaGasCentre();
				centre.CentreID = pCentreID;
				centre.CentreName = pCentreName;
				centre.CityID = pCityID;
				centre.EntityCodeID = pEntityCodeID;
                centre.UserID = pUserID;
                centre.Active = true;
				tblTswanaGasCentreCrud crud = new tblTswanaGasCentreCrud();
                //long ID = 0;
				crud.Insert(centre);
                return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertTswanaGasCentre -" + Ex.Message);
			}
		}

        
        public Boolean InsertTswanaGasCity(Int32 pCityID, String pCityName, Int32 pEntityCodeID, Int32 pUserID)
		{
			try
			{
				tblTswanaGasCityCrud crud = new tblTswanaGasCityCrud();
				tblTswanaGasCity city = new tblTswanaGasCity();
				city.CityID = pCityID;
				city.CityName = pCityName;
				city.EntityCodeID = pEntityCodeID;
                city.UserID = pUserID;
                city.Active = true;
				crud.Insert(city);

				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertTswanaGasCity -" + Ex.Message);
			}
		}



        public tblTswanaGasCentre GetTswanaGasCentreByCentreID(Int32 pCentreID)
		{
			try
			{
				tblTswanaGasCentreCrud crud = new tblTswanaGasCentreCrud();
				crud.Where("CentreID", DAL.General.Operator.Equals, pCentreID);
				recordFound = false;
				tblTswanaGasCentre result = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return result;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCentre - " + Ex.Message);
			}
		}



        /*public Boolean UpdateTswanaGasCylinderSelectionNo(Int32 pCylinderID, Byte pSelectionNo)
        {
            try
            {
                tbltswanagascylinder centre =  GetTswanaGasCylinderMS(pCylinderID);
                centre.SelectionNo = pSelectionNo;
                tbltswanagascentreCrud crud = new tbltswanagascentreCrud();
                crud.Update(centre);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateTswanaGasCylinderSelectionNo - " + Ex.Message);
            }

        }*/
        
        public Boolean UpdateTswanaGasCentre(Int32 pCentreID, Int32 pCity, Int32 pUserID)
        {
            try
            {
                tblTswanaGasCentre centre = GetTswanaGasCentreByCentreID(pCentreID);
                centre.CityID= pCity;
                centre.UserID = pUserID;
                tblTswanaGasCentreCrud crud = new tblTswanaGasCentreCrud();
                crud.Update(centre);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateTswanaGasCentre - " + Ex.Message);
            }

        }



        public Boolean UpdateTswanaGasCentre(Int32 pCentreID, String pCentreName, Int32 pUserID)
        {
            try
            {
                tblTswanaGasCentre centre = GetTswanaGasCentreByCentreID(pCentreID);
                centre.CentreName = pCentreName;
                centre.UserID = pUserID;
                tblTswanaGasCentreCrud crud = new tblTswanaGasCentreCrud();
                crud.Update(centre);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateTswanaGasCentre - " + Ex.Message);
            }

        }

        

        public Boolean UpdateTswanaGasCentre(Int32 pCentreID, Boolean pActive, Int32 pUserID)
		{
			try
			{
				tblTswanaGasCentre centre = GetTswanaGasCentreByCentreID(pCentreID);
				centre.Active = pActive;
                centre.UserID = pUserID;
				tblTswanaGasCentreCrud crud = new tblTswanaGasCentreCrud();
				crud.Update(centre);
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("UpdateTswanaGasCentre - " + Ex.Message);
			}
		}



        public Boolean UpdateTswanaGasCity(Int32 pCityID, String pCityName, Int32 pUserID)
		{
			try
			{
				tblTswanaGasCity city = GetTswanaGasCity(pCityID);
				if (! recordFound)
				{
					city.CityName = pCityName;
                    city.UserID = pUserID;
					tblTswanaGasCityCrud crud = new tblTswanaGasCityCrud();
					crud.Update(city);
				}

				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("UpdateTswanaGasCity - " + Ex.Message);
			}
		}



        public Boolean UpdateTswanaGasCylinderActive(Int32 pCylinderID, Boolean pActive, Int32 pUserID)
        {
            try
            {
                tblTswanaGasCylinder cyl = GetTswanaGasCylinder(pCylinderID);
                cyl.Active = pActive;
                cyl.UserID = pUserID;
                tblTswanaGasCylinderCrud crud = new tblTswanaGasCylinderCrud();
                crud.Update(cyl);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateTswanaGasCylinder - " + Ex.Message);
            }
        }
        public Boolean UpdateTswanaGasCylinder(Int32 pCylinderID, String pCylinderName, Int32 pUserID)
		{
			try
			{
				tblTswanaGasCylinder cyl = GetTswanaGasCylinder(pCylinderID);
                cyl.CylinderName = pCylinderName;
                cyl.UserID = pUserID;
				tblTswanaGasCylinderCrud crud = new tblTswanaGasCylinderCrud();
				crud.Update(cyl);
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("UpdateTswanaGasCylinder - " + Ex.Message);
			}
		}		

        public Decimal GetTswanaGasEmptyCylinder()
        {
            try
            {
                tblSetting appSetting = GetSetting("Back Office", "DPO.BackOffice.Ussd",  "EmptyCylinderUrl");
                String pUrl = appSetting.SettingValue;
                List<TswanaGasCylinder> cylinders = new List<TswanaGasCylinder>();
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(pUrl);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                String result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                Decimal emptyCylCost = 0;
                if (!Decimal.TryParse(result, out emptyCylCost))
                    throw new Exception("Values retrieved is not numeric (" + result + ")");

                return emptyCylCost;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasEmptyCylinder - " + Ex.Message);
            }
        }
		public List<TswanaGasCylinder> GetTswanaGasCylinderJSON()
		{
			try
			{
				tblSetting appSetting = GetSetting("Back Office", "DPO.BackOffice.Ussd", "CylinderUrl");
				String pUrl = appSetting.SettingValue;
				List<TswanaGasCylinder> cylinders = new List<TswanaGasCylinder>();
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(pUrl);
				httpWebRequest.ContentType = "application/json";
				httpWebRequest.Method = "GET";
				var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
				String result = "";
				using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
				{
					result = streamReader.ReadToEnd();
				}

				if (!result.Contains("[") && !result.Contains("]"))
					throw new Exception(result);


				result = result.Replace("[", "").Replace("]", "").Replace("\"id\":", "").Replace("\"name\":", "").Replace("\"", "").Replace("price:", "");
				String[] cyls = result.Split('}');

				for (Int32 citLoop = 0; citLoop < cyls.GetUpperBound(0); citLoop++)
				{
					cyls[citLoop] = cyls[citLoop].Replace(",{", "").Replace("{", "");
				}

				for (Int32 cylLoop = 0; cylLoop < cyls.GetUpperBound(0); cylLoop++)
				{
					if (cyls[cylLoop] != "")
					{
						String[] workCyl = cyls[cylLoop].Split(',');
						Int32 cylID = Convert.ToInt32(workCyl[0]);
						String cylName= workCyl[1];
						//Decimal cylPrice = Convert.ToDecimal(workCyl[2]);
						TswanaGasCylinder cyl = new TswanaGasCylinder();
						cyl.CylinderID = cylID;
						cyl.CylinderName = cylName;
						//cyl.CylinderPrice = cylPrice;
						cylinders.Add(cyl);
					}
				}
				return cylinders;
			}
			catch (System.Net.WebException webEx)
			{
				if (webEx.Response != null)
				{
					var resp = new System.IO.StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
					dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject(resp);
					String messageFromServer = obj.error.ToString();

					throw new Exception("Error=" + messageFromServer);
				}
				else
					throw new Exception("Error=" + webEx.Message);
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCylinder-" + Ex.Message);
			}

		}

		public Boolean UpdateTswanaGasCityActive(Int32 pCityID, Boolean pActive, Int32 pUserID)
		{
			try
			{
				tblTswanaGasCity city = GetTswanaGasCity(pCityID);
				city.Active = pActive;
                city.UserID = pUserID;
				tblTswanaGasCityCrud crud = new tblTswanaGasCityCrud();
				crud.Update(city);
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("UpdateTswanaGasCityActive - " + Ex.Message);
			}
		}



        public Boolean DeleteTswanaGasCity(Int32 pCityID)
		{
			try
			{
				tblTswanaGasCity city = GetTswanaGasCity(pCityID);
				tblTswanaGasCityCrud crud = new tblTswanaGasCityCrud();
				crud.Delete(city);
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("DeleteTswanaGasCity - " + Ex.Message);
			}


		}
		

		public tblTswanaGasCentre GetTswanaGasCentreByCode(String pCentreCode)
		{
			try
			{
				tblTswanaGasEntityCodeCrud crud = new tblTswanaGasEntityCodeCrud();
				crud.Where("EntityTypeID", DAL.General.Operator.Equals, 2);
				crud.And("EntityCode", DAL.General.Operator.Equals, pCentreCode);
				tblTswanaGasEntityCode code = crud.ReadSingle();
				tblTswanaGasCentreCrud crudCentre = new tblTswanaGasCentreCrud();
				crudCentre.Where("EntityCodeID", DAL.General.Operator.Equals, (Int32)code.EntityCodeID);
				tblTswanaGasCentre centre = crudCentre.ReadSingle();

				return centre;
				
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCentreByCode -" + Ex.Message);
			}
		}

		public tblTswanaGasCylinder GetTswanaGasCylinderByCode(String pCylinderCode)
		{
			try
			{
				tblTswanaGasEntityCodeCrud crud = new tblTswanaGasEntityCodeCrud();
				crud.Where("EntityTypeID", DAL.General.Operator.Equals, 3);
				crud.And("EntityCode", DAL.General.Operator.Equals, pCylinderCode);
				tblTswanaGasEntityCode code = crud.ReadSingle();
				tblTswanaGasCylinderCrud crudCylinder = new tblTswanaGasCylinderCrud();
				crudCylinder.Where("EntityCodeID", DAL.General.Operator.Equals, (Int32)code.EntityCodeID);
				tblTswanaGasCylinder cylinder = crudCylinder.ReadSingle();

				return cylinder;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCylinderByCode - " + Ex.Message);
			}
		}

		public Int32 InsertTswanaGasTransaction(Int32 pCentreID, Int32 pCylinderID, Decimal pCylinderPrice, String pReferenceNumber, String pOrderNumber)
		{
			try
			{
				tblTswanaGasTransaction tran = new tblTswanaGasTransaction();
				tran.CentreID = pCentreID;
				tran.CylinderID = pCylinderID;
				tran.CylinderPrice = pCylinderPrice;
				tran.OrderNumber = pOrderNumber;
				tran.ReferenceNumber = pReferenceNumber;
				tran.TransactionDateTime = DateTime.Now;
				long ID;
				tblTswanaGasTransactionCrud crud = new tblTswanaGasTransactionCrud();
				crud.Insert(tran, out ID);
				return (Int32)ID;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertTswanaGasTransaction - " + Ex.Message);
			}
		}
		public String TswanaGasPlaceOrder(String pMsidn, String pCentreID, String pCylinderID, String pPrice, String pReferenceNo, Boolean pCylinderReplacement, String pDeliveryName)
		{
            String pUrl = "";
			try
			{
				tblSetting appSetting = GetSetting("Virtual Message", "VCS.VirtualMessage.Ussd", "OrderURL");
				pUrl = appSetting.SettingValue;
				pUrl += "?cell=" + pMsidn;
				pUrl += "&distribution_point=" + pCentreID;
				pUrl += "&cylinder_type=" + pCylinderID;
				pUrl += "&price=" + pPrice;
				pUrl += "&transaction_id=" + pReferenceNo;
				DateTime now = DateTime.Now;
				pUrl += "&transaction_date_time=" + now.Year.ToString() + "-" + now.Month.ToString().PadLeft(2, '0') + "-" + now.Day.ToString().PadLeft(2, '0') + " " + now.Hour.ToString().PadLeft(2, '0') + ":" + now.Minute.ToString().PadLeft(2, '0') + ":" + now.Second.ToString().PadLeft(2, '0');
                pUrl += pCylinderReplacement == false ? "&cylinder_replacement=1" : "&cylinder_replacement=2";
                pUrl += "&C_NAME=" + pDeliveryName;
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(pUrl);
				httpWebRequest.ContentType = "application/json";
				httpWebRequest.Method = "GET";
				var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
				String result = "";
				using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
				{
					result = streamReader.ReadToEnd();
				}
				return result ;

			}
			catch (Exception Ex)
			{
				throw new Exception("TswanaGasPlaceOrder -" + Ex.Message );
			}
		}
		public List<TswanaGasCity> GetTswanaGasCityJSON()
		{
			try
			{
                tblSetting appSetting = GetSetting("Back Office", "DPO.BackOffice.Ussd", "CityURL");
                String pUrl = appSetting.SettingValue;
				List<TswanaGasCity> cities = new List<TswanaGasCity>();
				var httpWebRequest = (HttpWebRequest)WebRequest.Create(pUrl);
				httpWebRequest.ContentType = "application/json";
				httpWebRequest.Method = "GET";
				var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
				String result = "";
				using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
				{
					result = streamReader.ReadToEnd();
				}

				result = result.Replace("[", "").Replace("]", "").Replace("\"id\":", "").Replace("\"name\":", "").Replace("\"", "");
				String[] cits = result.Split('}');

				for (Int32 citLoop = 0; citLoop < cits.GetUpperBound(0); citLoop++)
				{
					cits[citLoop] = cits[citLoop].Replace(",{", "").Replace("{", "");
				}

				for (Int32 citLoop = 0; citLoop < cits.GetUpperBound(0); citLoop++)
				{
					if (cits[citLoop] != "")
					{
						String[] workCity = cits[citLoop].Split(',');
						Int32 cityID = Convert.ToInt32(workCity[0]);
						String cityName = workCity[1];
						TswanaGasCity city = new TswanaGasCity();
						city.CityID = cityID;
						city.CityName = cityName;
						cities.Add(city);
					}
				}


				return cities;
			}
			catch (System.Net.WebException webEx)
			{
				if (webEx.Response != null)
				{
					var resp = new System.IO.StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
					dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject(resp);
					String messageFromServer = obj.error.ToString();

					throw new Exception("Error=" + messageFromServer);
				}
				else
					throw new Exception("Error=" + webEx.Message);
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCity-" + Ex.Message);
			}

		}

        


        public List<tblTswanaGasCylinder> GetTswanaGasCylinder()
		{
			try
			{
				tblTswanaGasCylinderCrud crud = new tblTswanaGasCylinderCrud();
				recordFound = false;
				List<tblTswanaGasCylinder> results = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return results;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCylinder- " + Ex.Message);
			}
		}

        public tblTswanaGasCylinder GetTswanaGasCylinder(String pCylinderName)
        {
            try
            {
                tblTswanaGasCylinderCrud crud = new tblTswanaGasCylinderCrud();
                crud.Where("CylinderName", DAL.General.Operator.Equals, pCylinderName);
                recordFound = false;
                tblTswanaGasCylinder cylinder = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return cylinder;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCylinder - " + Ex.Message);
            }
        }

        public tblTswanaGasCylinder GetTswanaGasCylinder(Int32 pCylinder)
		{
			try
			{
				tblTswanaGasCylinderCrud crud = new tblTswanaGasCylinderCrud();
				crud.Where("CylinderID", DAL.General.Operator.Equals, pCylinder);
				recordFound = false;
				tblTswanaGasCylinder cylinder = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return cylinder;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCylinder - " + Ex.Message);
			}
		}




        

        public tblTswanaGasCentre GetTswanaGasCentreByID(Int32 pCentreID)
        {
            try
            {
                tblTswanaGasCentreCrud crud = new tblTswanaGasCentreCrud();
                crud.Where("CentreID", DAL.General.Operator.Equals, pCentreID);
                recordFound = false;
                tblTswanaGasCentre results = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return results;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentre-" + Ex.Message);
            }
        }


        public tblTswanaGasCentre GetTswanaGasCentre(Int32 pCityID, String pCentreName)
        {
            try
            {
                tblTswanaGasCentreCrud crud = new tblTswanaGasCentreCrud();
                crud.Where("CityID", DAL.General.Operator.Equals, pCityID);
                crud.And("CentreName", DAL.General.Operator.Equals, pCentreName);
                recordFound = false;
                tblTswanaGasCentre results = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return results;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentre-" + Ex.Message);
            }
        }
        public List<tblTswanaGasCentre> GetTswanaGasCentre(Int32 pCityID)
		{
			try
			{
				tblTswanaGasCentreCrud crud = new tblTswanaGasCentreCrud();
				crud.Where("CityID", DAL.General.Operator.Equals, pCityID);
				recordFound = false;
				List<tblTswanaGasCentre> results = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return results;
			}
			catch(Exception Ex)
			{
				throw new Exception("GetTswanaGasCentre-" + Ex.Message);
			}
		}

        
        /*public tblTswanaGasCentre GetTswanaGasCentre(Int32 pCentreID)
		{
			try
			{
				tblTswanaGasCentreCrud crud = new tblTswanaGasCentreCrud();
				crud.Where("CentreID", DAL.General.Operator.Equals, pCentreID);
				recordFound = false;
				tblTswanaGasCentre centre = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return centre;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCentre - " + Ex.Message);
			}
		}*/
        public List<tblTswanaGasCity> GetTswanaGasCity()
		{
			try
			{
				tblTswanaGasCityCrud crud = new tblTswanaGasCityCrud();
				recordFound = false;
				List<tblTswanaGasCity> results = crud.ReadMulti().ToList();
                results = results.OrderBy(x => x.CityName).ToList();
				recordFound = crud.RecordFound;

				return results;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCity() -" + Ex.Message);
			}
		}



        public tblTswanaGasCity GetTswanaGasCity(Int32 pCityID)
		{
			try
			{
				tblTswanaGasCityCrud crud = new tblTswanaGasCityCrud();
				crud.Where("CityID", DAL.General.Operator.Equals, pCityID);
				recordFound = false;
				tblTswanaGasCity city = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return city;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCity - " + Ex.Message);
			}
		}

        public tblTswanaGasCity GetTswanaGasCity(String pCityName)
        {
            try
            {
                tblTswanaGasCityCrud crud = new tblTswanaGasCityCrud();
                crud.Where("CityName", DAL.General.Operator.Equals, pCityName);
                recordFound = false;
                tblTswanaGasCity city = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return city;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCity - " + Ex.Message);
            }
        }
    }
}
