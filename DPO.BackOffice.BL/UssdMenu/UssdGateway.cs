﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.DAL;


namespace DPO.BackOffice.BL
{
    public partial class BackOfficeBL
    {
        public String GetUssdGatewayCountGridData()
        {
            try
            {
                List<vUssdGatewayCounnt> recs = GetUssdGatewayCount();
                String divData = "";
                foreach (vUssdGatewayCounnt rec in recs)
                {
                    divData += rec.GatewayID.ToString() + "═";
                    divData += rec.GateWayName.ToString() + "═";
                    divData += rec.TotalTran.ToString() + "═";
                    divData += rec.ValidationErrorCount.ToString() + "═";
                    divData += rec.GatewayErrorCount.ToString() + "║";
                }

                if (divData.Length > 0)
                    divData = divData.Substring(0, divData.Length - 1);

                return divData;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdGatewayCount - " + Ex.Message);
            }
        }
        public List<vUssdGatewayCounnt> GetUssdGatewayCount()
        {
            try
            {
                vUssdGatewayCounntCrud crud = new vUssdGatewayCounntCrud();
                return crud.ReadMulti().ToList();
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdGatewayCount - " + Ex.Message); 
            }
        }
        public tblUssdGatewayError GetUssdGatewayError(String pUssdGatewayError)
        {
            try
            {
                tblUssdGatewayErrorCrud crud = new tblUssdGatewayErrorCrud();
                crud.Where("GatewayError", DAL.General.Operator.Equals, pUssdGatewayError);
                recordFound = false;
                tblUssdGatewayError result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdGatewayError - " + Ex.Message);
            }
        }

        public Byte InsertUssdGatewayError(String pGatewayError)
        {
            try
            {
                tblUssdGatewayErrorCrud crud = new tblUssdGatewayErrorCrud();
                tblUssdGatewayError err = new tblUssdGatewayError();
                err.GatewayError = pGatewayError;
                long ID = 0;
                crud.Insert(err, out ID);
                return (Byte)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "InsertUssdGatewayError - " + Ex.Message);
            }
        }
    }
}
