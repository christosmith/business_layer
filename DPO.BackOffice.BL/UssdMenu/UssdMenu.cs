﻿using System;
using System.Collections.Generic;
using System.Linq;
using DPO.BackOffice.DAL;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;

namespace DPO.BackOffice.BL
{
	public partial class BackOfficeBL
    {
        /*internal Boolean UpdateUssdMenu(Int32 pUssdMenuID, UssdMenuInformationChange pChange, String pNewValue)
        {
            try
            {
                tblUssdMenu menu = GetUssdMenuByID(pUssdMenuID);
                switch (pChange.UssdMenuInformationType)
                {
                    case eUssdMenuInformation.Confirm:
                        menu.Confirm = Convert.ToBoolean(pNewValue);
                        break;
                    case eUssdMenuInformation.ConfirmText:
                        tblUssdConfirmText confirmText = GetUssdConfirmText(pNewValue);
                        if (!recordFound) throw new Exception("Confirm text record not set up for " + pNewValue);
                        menu.ConfirmTextID = Convert.ToInt16(confirmText.ConfirmTextID);
                        break;
                    case eUssdMenuInformation.LeadInText:
                        tblUssdLeadInText leadInText = GetUssdLeadInText(pNewValue);
                        if (!recordFound) throw new Exception("Lead in text record was not set up for " + pNewValue);
                        menu.LeadInTextID = Convert.ToInt16(leadInText.LeadInTextID);
                        break;

                }

                
                tblUssdMenuCrud crud = new tblUssdMenuCrud();
                crud.Update(menu);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdMenu - " + Ex.Message);
            }

        }
        public Boolean UpdateUssdMenu(List<UssdMenuInformationChange> pChanges, List<tblUssdGateway> pGateway, eUssdMenuChangeOptions pChangeOptions)
        {
            try
            {
                List<UssdMenuInformationChange> changes = pChanges.FindAll(x => x.ChangeInformation == true).ToList();
                foreach (UssdMenuInformationChange change in changes)
                {
                    switch (pChangeOptions)
                    {
                        case eUssdMenuChangeOptions.CurrentNode:
                            foreach (tblUssdGateway gateway in pGateway)
                            {
                                switch (change.UssdMenuInformationType)
                                {
                                    case eUssdMenuInformation.Confirm:

                                        break;
                                }
                            }
                            break;
                    }
                }
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdMenu - " + Ex.Message);
            }
        }*/
        public String BuildUssdMenuComplete()
        {
            try
            {
                return "";
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "BuildUssdMenuComplete - " + Ex.Message);
            }
        }





        public tblUssdValidationError GetUssdValidationError(String pValidationError)
        {
            try
            {
                tblUssdValidationErrorCrud crud = new tblUssdValidationErrorCrud();
                crud.Where("ValidationError", DAL.General.Operator.Like, pValidationError);
                recordFound = false;
                tblUssdValidationError result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdValidationError - " + Ex.Message);
            }
        }
		public String UpdateUssdRegistry(String pMessage)
		{
			try
			{
                /*pMessage = "Windows Registry Editor Version 5.00\n";

				pMessage += "[HKEY_LOCAL_MACHINE\\SOFTWARE\\VCS\\VirtualGateway\\VgiTruteqUssd\\UssdMenu\\VirtualMobile\\5 Ap\\5 Tswana Gas\\1 FrancisTown]\n";
				pMessage += "\"MenuItemNr\" = \"1\"\n";
				pMessage += "\"MenuItemText\" = \"FrancisTown\"\n";
				pMessage += "\"WelcomeMsg\" = \"Select Distribution Centre\"\n";

                pMessage += "[HKEY_LOCAL_MACHINE\\SOFTWARE\\VCS\\VirtualGateway\\VgiTruteqUssd\\UssdMenu\\VirtualMobile\\5 Ap\\5 Tswana Gas\\1 FrancisTown]\n";
                pMessage += "\"MenuItemNr\" = \"1\"\n";
                pMessage += "\"MenuItemText\" = \"FrancisTown\"\n";
                pMessage += "\"WelcomeMsg\" = \"Select Distribution Centre\"\n";

                pMessage += "[HKEY_LOCAL_MACHINE\\SOFTWARE\\VCS\\VirtualGateway\\VgiTruteqUssd\\UssdMenu\\VirtualMobile\\5 Ap\\5 Tswana Gas\\1 FrancisTown]\n";
                pMessage += "\"MenuItemNr\" = \"1\"\n";
                pMessage += "\"MenuItemText\" = \"FrancisTown\"\n";
                pMessage += "\"WelcomeMsg\" = \"Select Distribution Centre\"\n";

                pMessage += "[HKEY_LOCAL_MACHINE\\SOFTWARE\\VCS\\VirtualGateway\\VgiTruteqUssd\\UssdMenu\\VirtualMobile\\5 Ap\\5 Tswana Gas\\1 FrancisTown]\n";
                pMessage += "\"MenuItemNr\" = \"1\"\n";
                pMessage += "\"MenuItemText\" = \"FrancisTown\"\n";
                pMessage += "\"WelcomeMsg\" = \"Select Distribution Centre\"\n";

                pMessage += "[HKEY_LOCAL_MACHINE\\SOFTWARE\\VCS\\VirtualGateway\\VgiTruteqUssd\\UssdMenu\\VirtualMobile\\5 Ap\\5 Tswana Gas\\1 FrancisTown]\n";
                pMessage += "\"MenuItemNr\" = \"1\"\n";
                pMessage += "\"MenuItemText\" = \"FrancisTown\"\n";
                pMessage += "\"WelcomeMsg\" = \"Select Distribution Centre\"\n";

                pMessage += "[HKEY_LOCAL_MACHINE\\SOFTWARE\\VCS\\VirtualGateway\\VgiTruteqUssd\\UssdMenu\\VirtualMobile\\5 Ap\\5 Tswana Gas\\1 FrancisTown]\n";
                pMessage += "\"MenuItemNr\" = \"1\"\n";
                pMessage += "\"MenuItemText\" = \"FrancisTown\"\n";
                pMessage += "\"WelcomeMsg\" = \"Select Distribution Centre\"\n";
                pMessage += "[DPO.BackOffice.BL.UpdateUssdRegistry]";*/

                pMessage += "\n[DPO.BackOffice.BL.UpdateUssdRegistry]";
                TcpClient tcpclnt = new TcpClient();
			    //tcpclnt.Connect("127.0.0.1", 8800);
                //tcpclnt.Connect("41.0.46.163", 3094);
                tcpclnt.Connect("192.168.1.183", 3096); 

                Stream stm = tcpclnt.GetStream();
				ASCIIEncoding asen = new ASCIIEncoding();
				byte[] ba = asen.GetBytes(pMessage);
				stm.Write(ba, 0, ba.Length);
				byte[] bb = new byte[ba.Length];
				int k = stm.Read(bb, 0, ba.Length);

                String socketMessage = "";
				for (int i = 0; i < k; i++)
					socketMessage += Convert.ToChar(bb[i]);

				tcpclnt.Close();

                if (socketMessage.Contains("Error"))
                    WriteLog((Int32)GetApplicationByName("VCS.VirtualMessage.Ussd").ApplicationID, "UpdateUssdRegistry - " + socketMessage, eLogType.Error);
                else
                    WriteLog((Int32)GetApplicationByName("VCS.VirtualMessage.Ussd").ApplicationID, "UpdateUssdRegistry - " + socketMessage, eLogType.Information);

                return "";
			}
			catch (Exception Ex)
			{
				throw new Exception("UpdateUssdRegistry -" + Ex.Message);
			}
		}
        public tblUssdGatewayDown GetUssdGatewayDown(Byte pGatewayID, DateTime pDownDateTime)
        {
            try
            {
                tblUssdGatewayDownCrud crud = new tblUssdGatewayDownCrud();
                crud.Where("GatewayID", DAL.General.Operator.Equals, pGatewayID);
                crud.And("DownDateTime", DAL.General.Operator.Equals, pDownDateTime);
                recordFound = false;
                tblUssdGatewayDown result = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return result;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdGatewayDown -" + Ex.Message);
            }
        }

        public Int32 InsertUssdGatewayDown(Byte pGatewayID, DateTime pDownDateTime)
        {
            try
            {
                tblUssdGatewayDown down = new tblUssdGatewayDown();
                down.DownDateTime = pDownDateTime;
                down.GatewayID = pGatewayID;
                tblUssdGatewayDownCrud crud = new tblUssdGatewayDownCrud();
                long ID = 0;
                crud.Insert(down, out ID);
                return (Int32)ID;

                
                
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "InsertUssdGatewayDown -" + Ex.Message);
            }
        }
        #region Selects

		public List<tblUssdGateway> GetUssdGateway()
		{
			try
			{
				tblUssdGatewayCrud crud = new tblUssdGatewayCrud();
				recordFound = false;
				List<tblUssdGateway> results = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return results;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetUssdGateway -" + Ex.Message);
			}
		}
        /// <summary>
        /// Return information from tblUssdGateway. This is to differentiate between gateways like beMobile and Mascom.
        /// </summary>
        /// <param name="pGatewayName">The name of the gateway</param>
        /// <returns></returns>
        public tblUssdGateway GetUssdGateway(String pGatewayName)
        {
            try
            {
                recordFound = false;
                tblUssdGatewayCrud reader = new tblUssdGatewayCrud();
                reader.Where("GatewayName", DAL.General.Operator.Equals, pGatewayName);
                tblUssdGateway gateway = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return gateway;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdGateway -" + Ex.Message);
            }
        }



        /// <summary>
        /// Returns a list of all the ussd menu items. This is as they appear in the registry menu structure
        /// </summary>
        /// <returns></returns>
        public List<tblUssdMenuItem> GetUssdMenuItem()
        {
            try
            {
                recordFound = false;
                tblUssdMenuItemCrud reader = new tblUssdMenuItemCrud();
                List<tblUssdMenuItem> item = reader.ReadMulti().ToList();
                recordFound = reader.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuItem-" + Ex.Message);
            }
        }




        public tblUssdMenuItem GetUssdMenuItem(Int32 pMenuItemID)
        {
            try
            {
                recordFound = false;
                tblUssdMenuItemCrud reader = new tblUssdMenuItemCrud();
                reader.Where("MenuItemID", DAL.General.Operator.Equals, pMenuItemID);
                tblUssdMenuItem item = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuItem-" + Ex.Message);
            }
        }




            /// <summary>
            /// Returns the menu item as it is in the registry. This is mainly for ussd functionality.
            /// </summary>
            /// <param name="pMenuItem">Name of the menu item. Eg 1 Ap (Payment) and MultiChoice</param>
            /// <returns></returns>
            /// 

            public tblUssdMenuItem GetUssdMenuItem(String pMenuItem)
        {
            try
            {
                recordFound = false;
                tblUssdMenuItemCrud reader = new tblUssdMenuItemCrud();
                reader.Where("MenuItem", DAL.General.Operator.Equals, pMenuItem);
                tblUssdMenuItem item = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuItem-" + Ex.Message);
            }
        }
        /// <summary>
        /// Get a record from the tblMenuItemText table. This is the text displayed to the customer during a ussd session.
        /// </summary>
        /// <param name="pMenuItemText">Description of the menu item.</param>
        /// <returns></returns>
        /// 



        public List<tblUssdMenuItemText> GetUssdMenuItemText()
        {
            try
            {
                recordFound = false;
                tblUssdMenuItemTextCrud reader = new tblUssdMenuItemTextCrud();
                List<tblUssdMenuItemText> item = reader.ReadMulti().ToList();
                recordFound = reader.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuItemText - " + Ex.Message);
            }
        }
        public tblUssdMenuItemText GetUssdMenuItemText(String pMenuItemText)
        {
            try
            {
                recordFound = false;
                tblUssdMenuItemTextCrud reader = new tblUssdMenuItemTextCrud();
                reader.Where("MenuItemText", DAL.General.Operator.Equals, pMenuItemText);
                tblUssdMenuItemText item = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return item;
            }
            catch(Exception Ex)
            {
                throw new Exception("GetUssdMenuItemText - " + Ex.Message);
            }
        }

		public tblUssdMenuItemText GetUssdMenuItemText(Int16 pMenuItemID)
		{
			try
			{
				recordFound = false;
				tblUssdMenuItemTextCrud reader = new tblUssdMenuItemTextCrud();
				reader.Where("MenuItemTextID", DAL.General.Operator.Equals, pMenuItemID);
				tblUssdMenuItemText item = reader.ReadSingle();
				recordFound = reader.RecordFound;
				return item;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetUssdMenuItemText - " + Ex.Message);
			}
		}


        /// <summary>
        /// Return a record from tblUssdConfirmText. This is the information shown when the user has entered all information and needs to confirm which was entered.
        /// </summary>
        /// <param name="pConfirmText">The text to display. Eg Card Number = 5454545454545454</param>
        /// <returns></returns>
        public tblUssdConfirmText GetUssdConfirmText(String pConfirmText)
        {
            try
            {
                recordFound = false;
                tblUssdConfirmTextCrud crud = new tblUssdConfirmTextCrud();
                crud.Where("ConfirmText", DAL.General.Operator.Equals, pConfirmText);
                tblUssdConfirmText item = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdConfirmText-" + Ex.Message);
            }
        }

        public tblUssdConfirmText GetUssdConfirmText(Int16 pConfirmTextID)
        {
            try
            {
                recordFound = false;
                tblUssdConfirmTextCrud crud = new tblUssdConfirmTextCrud();
                crud.Where("ConfirmTextID", DAL.General.Operator.Equals, pConfirmTextID);
                tblUssdConfirmText item = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdConfirmText-" + Ex.Message);
            }
        }

        /// <summary>
        /// Returns a list of all the confirmation tex. This is the information shown when the user has entered all information and needs to confirm which was entered.
        /// </summary>
        /// <returns></returns>
        public List<tblUssdConfirmText> GetUssdConfirmText()
        {
            try
            {
                recordFound = false;
                tblUssdConfirmTextCrud crud = new tblUssdConfirmTextCrud();
                List<tblUssdConfirmText> item = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdConfirmText-" + Ex.Message);
            }
        }


        /// <summary>
        /// Select a record from tblUssdTagName. This is the xml tag send to the server .aspx page.
        /// </summary>
        /// <param name="pTagName">The xml tag name.</param>
        /// <returns></returns>
        public tblUssdTagName GetUssdTagName(String pTagName)
        {
            try
            {
                recordFound = false;
                tblUssdTagNameCrud crud = new tblUssdTagNameCrud();
                crud.Where("TagName", DAL.General.Operator.Equals, pTagName);
                tblUssdTagName tagName = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return tagName;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTagName-" + Ex.Message);
            }
        }

        /// <summary>
        /// Select a list of all records from tblUssdTagName. This is the xml tag send to the server .aspx page from the ussd gateway.
        /// </summary>
        /// <returns></returns>
        public List<tblUssdTagName> GetUssdTagName()
        {
            try
            {
                recordFound = false;
                tblUssdTagNameCrud crud = new tblUssdTagNameCrud();
                List<tblUssdTagName> tagName = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return tagName;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTagName-" + Ex.Message);
            }
        }




        public tblUssdTagName GetUssdTagName(Int16 pTagNameID)
        {
            try
            {
                recordFound = false;
                tblUssdTagNameCrud crud = new tblUssdTagNameCrud();
                crud.Where("TagNameID", DAL.General.Operator.Equals, pTagNameID);
                tblUssdTagName tagName = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return tagName;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTagName-" + Ex.Message);
            }
        }





        /// <summary>
        /// Return a record from tblUssdLeadInText. This is the text shown when a user has to select a menu item during a ussd session.
        /// </summary>
        /// <param name="pLeadInText">Text shown on top of a menu selection.</param>
        /// <returns></returns>
        public tblUssdLeadInText GetUssdLeadInText(String pLeadInText)
        {
            try
            {
                recordFound = false;
                tblUssdLeadInTextCrud crud = new tblUssdLeadInTextCrud();
                crud.Where("LeadInText", DAL.General.Operator.Equals, pLeadInText);
                tblUssdLeadInText text = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return text;
            }
            catch(Exception Ex)
            {
                throw new Exception("GetUssdLeadInText-" + Ex.Message);
            }
        }

        /// <summary>
        /// Returns a list of all records in tblUssdLeadInText. This is the heading shown when a user has to select a menu item during a ussd session.
        /// </summary>
        /// <returns></returns>
        public List<tblUssdLeadInText> GetUssdLeadInText()
        {
            try
            {
                recordFound = false;
                tblUssdLeadInTextCrud crud = new tblUssdLeadInTextCrud();
                List<tblUssdLeadInText> text = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return text;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdLeadInText-" + Ex.Message);
            }
        }

        

        public tblUssdLeadInText GetUssdLeadInText(Int16 pLeadInTextID)
        {
            try
            {
                recordFound = false;
                tblUssdLeadInTextCrud crud = new tblUssdLeadInTextCrud();
                crud.Where("LeadInTextID", DAL.General.Operator.Equals, pLeadInTextID);
                tblUssdLeadInText text = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return text;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdLeadInText-" + Ex.Message);
            }
        }



            /// <summary>
            /// Returns a record from tblUssdWelcomeMessage. This is the intro message on each ussd screen.
            /// </summary>
            /// <param name="pWelcomeMessage">The message shown at the to of each ussd screen.</param>
            /// <returns></returns>
            public tblUssdWelcomeMessage GetUssdWelcomeMessage(String pWelcomeMessage)
        {
            try
            {
                recordFound = false;
                tblUssdWelcomeMessageCrud crud = new tblUssdWelcomeMessageCrud();
                crud.Where("WelcomeMessage", DAL.General.Operator.Equals, pWelcomeMessage);
                tblUssdWelcomeMessage message = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return message;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdWelcomeMessage-" + Ex.Message);
            }
        }

        /// <summary>
        /// Returns a list of all records from tblUssdWelcomeMessage. This is the intro message on each ussd screen.
        /// </summary>
        /// <param name="pWelcomeMessage">The message shown at the to of each ussd screen.</param>
        /// <returns></returns>
        public List<tblUssdWelcomeMessage> GetUssdWelcomeMessage()
        {
            try
            {
                recordFound = false;
                tblUssdWelcomeMessageCrud crud = new tblUssdWelcomeMessageCrud();
                List<tblUssdWelcomeMessage> message = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return message;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdWelcomeMessage-" + Ex.Message);
            }
        }




        public tblUssdWelcomeMessage GetUssdWelcomeMessage(Int16 pWelcomeMessageID)
        {
            try
            {
                recordFound = false;
                tblUssdWelcomeMessageCrud crud = new tblUssdWelcomeMessageCrud();
                crud.Where("WelcomeMessageID", DAL.General.Operator.Equals, pWelcomeMessageID);
                tblUssdWelcomeMessage message = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return message;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdWelcomeMessage-" + Ex.Message);
            }
        }


        /// <summary>
        /// Select a record from tblUssdTransaction. This value gets passed to the server pages in order to identify functionality.
        /// </summary>
        /// <param name="pTransaction">The ussd transaction.</param>
        /// <returns></returns>
        public tblUssdTransaction GetUssdTransaction(String pTransaction)
        {
            try
            {
                recordFound = false;
                tblUssdTransactionCrud crud = new tblUssdTransactionCrud();
                crud.Where("UssdTransaction", DAL.General.Operator.Equals, pTransaction);
                tblUssdTransaction tran = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return tran;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransaction-" + Ex.Message);
            }
        }

        /// <summary>
        /// Select a list of alll records from tblUssdTransaction. This value gets passed to the server pages in order to identify functionality.
        /// </summary>
        /// <param name="pTransaction">The ussd transaction.</param>
        /// <returns></returns>
        public List<tblUssdTransaction> GetUssdTransaction()
        {
            try
            {
                recordFound = false;
                tblUssdTransactionCrud crud = new tblUssdTransactionCrud();
                List<tblUssdTransaction> tran = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return tran;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransaction-" + Ex.Message);
            }
        }



        public tblUssdTransaction GetUssdTransaction(Int16 pTransactionID)
        {
            try
            {
                recordFound = false;
                tblUssdTransactionCrud crud = new tblUssdTransactionCrud();
                crud.Where("TransactionID", DAL.General.Operator.Equals, pTransactionID);
                tblUssdTransaction tran = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return tran;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransaction-" + Ex.Message);
            }
        }


        /// <summary>
        /// Returns a record from tblUSsdHttpPost. This is the url where the collected information from the ussd session are post to.
        /// </summary>
        /// <param name="pUrl">The url ro post the information to.</param>
        /// <returns></returns>
        public tblUssdHttpPost GetUssdHttpPost(String pUrl)
        {
            try
            {
                recordFound = false;
                tblUssdHttpPostCrud crud = new tblUssdHttpPostCrud();
                crud.Where("Url", DAL.General.Operator.Equals, pUrl);
                tblUssdHttpPost post = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return post;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdHttpPost-" + Ex.Message);
            }
        }

        /// <summary>
        /// Returns a list of all record from tblUSsdHttpPost. This is the url where the collected information from the ussd session are post to.
        /// </summary>
        /// <param name="pUrl">The url ro post the information to.</param>
        /// <returns></returns>
        public List<tblUssdHttpPost> GetUssdHttpPost()
        {
            try
            {
                recordFound = false;
                tblUssdHttpPostCrud crud = new tblUssdHttpPostCrud();
                List<tblUssdHttpPost> post = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return post;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdHttpPost-" + Ex.Message);
            }
        }



        public tblUssdHttpPost GetUssdHttpPost(Byte pHttpPostID)
        {
            try
            {
                recordFound = false;
                tblUssdHttpPostCrud crud = new tblUssdHttpPostCrud();
                crud.Where("HttpPostID", DAL.General.Operator.Equals, pHttpPostID);
                tblUssdHttpPost post = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return post;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdHttpPost-" + Ex.Message);
            }
        }

        #endregion
        /// <summary>
        /// Returns a record from tblUssdMenu based on the passed registry key.
        /// </summary>
        /// <param name="pRegistryKey">The registry key.</param>
        /// <returns></returns>
        public tblUssdMenu GetUssdMenuPerRegistryKey(String pRegistryKey)
        {
            try
            {
                recordFound = false;
                tblUssdMenuCrud reader = new tblUssdMenuCrud();
                reader.Where("RegistryKey", DAL.General.Operator.Equals, pRegistryKey);
				reader.OrderBy(x => (int)x.GatewayID);
                tblUssdMenu menu = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return menu;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuPerREgistryKey-" + Ex.Message);
            }
        }

        public Boolean CopyUssdMenu(Int32 pThirdPartyID, Byte pUssdGatewayID, String pFromRegistry, String pToRegistry)
        {
            try
            {
                List<tblUssdMenu> sourceMenus = GetUssdMenuByRegistryKey(pFromRegistry, pUssdGatewayID);
                sourceMenus = sourceMenus.OrderBy(x => x.RegistryKey).ToList();
                Int32 parentID = (Int32)GetUssdMenuByRegistryKey(pToRegistry.Substring(0, pToRegistry.LastIndexOf('\\')), pUssdGatewayID, true).UssdMenuID;
                foreach(tblUssdMenu ussdMenu in sourceMenus)
                {
                    tblUssdMenu newMenu = new tblUssdMenu();
                    newMenu.Confirm = ussdMenu.Confirm;
                    newMenu.ConfirmTextID = ussdMenu.ConfirmTextID;
                    newMenu.FunctionID = ussdMenu.FunctionID;
                    newMenu.GatewayID = ussdMenu.GatewayID;
                    newMenu.HttpPostID = ussdMenu.HttpPostID;
                    newMenu.LeadInTextID = ussdMenu.LeadInTextID;
                    newMenu.MenuItemID = ussdMenu.MenuItemID;
                    newMenu.MenuItemNumber = ussdMenu.MenuItemNumber;
                    newMenu.MenuItemTextID = ussdMenu.MenuItemTextID;
                    newMenu.MenuLevelNo = ussdMenu.MenuLevelNo;
                    newMenu.ParentUssdMenuID = parentID;
                    newMenu.Promt = ussdMenu.Promt;
                    newMenu.RegistryKey = ussdMenu.RegistryKey.Replace(pFromRegistry, pToRegistry);
                    newMenu.TagNameID = ussdMenu.TagNameID;
                    newMenu.TransactionID = ussdMenu.TransactionID;
                    newMenu.UssdMenuID = ussdMenu.UssdMenuID;
                    newMenu.WelcomeMessageID = ussdMenu.WelcomeMessageID;
                    tblUssdMenu menu = GetUssdMenuByRegistryKey(newMenu.RegistryKey, pUssdGatewayID, true);
                    if (!recordFound)
                        parentID= InsertUssdMenu(pUssdGatewayID, newMenu.MenuItemID, (Int16)newMenu.MenuItemNumber, newMenu.MenuItemTextID, (Boolean)newMenu.Confirm, newMenu.ConfirmTextID, newMenu.LeadInTextID, (Boolean)newMenu.Promt, newMenu.TagNameID, newMenu.WelcomeMessageID, newMenu.TransactionID, (Byte)newMenu.MenuLevelNo, newMenu.HttpPostID, newMenu.RegistryKey, newMenu.ParentUssdMenuID, newMenu.FunctionID);
                }

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("CopyUssdMenu - " + Ex.Message);
            }
        }
        public List<tblUssdMenu> GetUssdMenuByFunctionID(Int32 pFunctionID, Byte pGatewayID, String pRegistryHint)
        {
            try
            {
                tblUssdMenuCrud crud = new tblUssdMenuCrud();
                crud.Where("FunctionID", DAL.General.Operator.Equals, pFunctionID);
                crud.And("GatewayID", DAL.General.Operator.Equals, pGatewayID);
                crud.And("RegistryKey", DAL.General.Operator.Like, "%" + pRegistryHint + "%");
                recordFound = false;
                crud.ExcludeReferenceTable = excludeRefTable;
                List<tblUssdMenu> result = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return result;

            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdMenuByFunctionID - " + Ex.Message);
            }
        }
        public List<tblUssdMenu> GetUssdMenuByThirdPartyID(Int32 pThirdPartyID, Byte pUssdGatewayID, String pRegistryHint)
        {
            try
            {
                List<tblUssdMenu> ussdMenus = new List<tblUssdMenu>();
                List<tblThirdPartyFunction> thirdPartyFunctions = GetThirdPartyFunction(pThirdPartyID);
                foreach (tblThirdPartyFunction thirdPartyFunction in thirdPartyFunctions)
                {
                    List<tblUssdMenu> menus = new List<tblUssdMenu>();
                    if ((Int32)thirdPartyFunction.ThirdPartyFunctionID == 24)
                        Console.WriteLine("@");

                    menus = GetUssdMenuByFunctionID((Int32)thirdPartyFunction.ThirdPartyFunctionID, pUssdGatewayID, pRegistryHint);
                    foreach (tblUssdMenu menu in menus)
                        ussdMenus.Add(menu);
                }

                if (ussdMenus.Count > 0)
                {
                    tblUssdMenu menu = GetUssdMenuByID((Int32)ussdMenus[0].ParentUssdMenuID);
                    ussdMenus.Insert(0, menu);
                }
                return ussdMenus;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdMenuByThirdPartyID - " + Ex.Message);
            }
        }
        public String BuildussdRegistryKey(Int32 pThirdPartyID, Byte pUssdGatewayID, String pRegistryHint)
        {
            //try
            //{
            String regKey = "Windows Registry Editor Version 5.00\n\n";

            List<tblUssdMenu> menus = new List<tblUssdMenu>();
            if (pRegistryHint == "")
                menus = GetUssdMenuByThirdPartyID(pThirdPartyID, pUssdGatewayID);
            else
                menus = GetUssdMenuByThirdPartyID(pThirdPartyID, pUssdGatewayID, pRegistryHint);

            foreach (tblUssdMenu menu in menus)
            {
                regKey += "[" + menu.RegistryKey + "]\n";
                if (menu.MenuItemNumber != 0)
                    regKey += "\"MenuItemNr\"=" + "\"" + menu.MenuItemNumber.ToString() + "\"\n";
                if (menu.MenuItemTextID != null)
                    regKey += "\"MenuItemText\"=" + "\"" + GetUssdMenuItemText((Int16)menu.MenuItemTextID).MenuItemText + "\"\n";
                regKey += "\"WelcomeMsg\"=" + "\"" + GetUssdWelcomeMessage((Int16)menu.WelcomeMessageID).WelcomeMessage + "\"\n";
                String confirm = (Boolean)menu.Confirm == true ? "\"Y\"\n" : "\"N\"\n";
                regKey += "\"Confirm\"=" + confirm;
                if (menu.ConfirmTextID != null)
                    regKey += "\"ConfirmText\"=" + "\"" + GetUssdConfirmText((Int16)menu.ConfirmTextID).ConfirmText + "\"\n";
                if (menu.LeadInTextID != null)
                    regKey += "\"LeadInText\"=" + "\"" + GetUssdLeadInText((Int16)menu.LeadInTextID).LeadInText + "\"\n";
                String prompt = (Boolean)menu.Confirm == true ? "\"Y\"\n" : "\"N\"\n";
                regKey += "\"Prompt\"=" + confirm;
                if (menu.TagNameID != null)
                    regKey += "\"TagName\"=" + "\"" + GetUssdTagName((Int16)menu.TagNameID).TagName + "\"\n";
                if (menu.TransactionID != null)
                    regKey += "\"Transaction\"=" + "\"" + GetUssdTransaction((Int16)menu.TransactionID).UssdTransaction + "\"\n";

                if (menu.HttpPostID != null)
                {
                    regKey += "\"DisableJumpIn\"=\"Y\"\n";
                    String url = GetUssdHttpPost((Byte)menu.HttpPostID).Url;
                    regKey += "\"HttpPost\"=\"" + url + "\"\n";
                    regKey += "\"UseLegacyPosting\"=\"1\"\n";
                }
                regKey += "\n";
            }

            return regKey;
            //}
            //catch (Exception Ex)
            //{
            //    throw new Exception(errorMessage + "BuildRegistryKey - " + Ex.Message);
            //}


        }



        public tblUssdInstruction GetUssdInstruction(String pInstruction)
        {
            try
            {
                tblUssdInstructionCrud crud = new tblUssdInstructionCrud();
                crud.Where("Instruction", DAL.General.Operator.Equals, pInstruction);
                recordFound = false;
                tblUssdInstruction instr = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return instr;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdInstruction - " + Ex.Message);
            }
        }
        public Boolean DeleteThirdPartyUssdInstruction(Int32 pUssdParameterID)
        {
            try
            {
                List<tblThirdPartyUssdInstruction> instr = GetThirdPartyUssdInstruction(pUssdParameterID);
                tblThirdPartyUssdInstructionCrud crud = new tblThirdPartyUssdInstructionCrud();
                foreach (tblThirdPartyUssdInstruction ins in instr)
                {
                    crud.Delete(ins);
                }

                return true;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdVendingMessage - " + Ex.Message);
            }
        }
        public Boolean InsertThirdPartyUssdInstruction(Int32 pUssdParameterID, Int16 pInstructionID, String pInstructionValue)
        {
            try
            {
                tblThirdPartyUssdInstruction ins = new tblThirdPartyUssdInstruction();
                ins.InstructionValue = pInstructionValue;
                ins.InstrunctionID = pInstructionID;
                ins.UssdParameterID = pUssdParameterID;
                tblThirdPartyUssdInstructionCrud crud = new tblThirdPartyUssdInstructionCrud();
                crud.Insert(ins);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertThirdPartyUssdInstruction - " + Ex.Message);
            }
        }

        public Boolean DeleteThirdPartyUssdMessage(Int32 pUssdParameterID)
        {
            try
            {
                List<tblThirdPartyUssdMessage> messages = GetThirdPartyUssdMessage(pUssdParameterID);
                tblThirdPartyUssdMessageCrud crud = new tblThirdPartyUssdMessageCrud();
                foreach (tblThirdPartyUssdMessage mess in messages)
                    crud.Delete(mess);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteThirdPartyUssdMessage - " + Ex.Message);
            }
        }
        public Boolean InsertThirdPartyUssdMessage(Int32 pUssdParameterID, Int16 pVendingMessageID)
        {
            try
            {
                tblThirdPartyUssdMessage mess = new tblThirdPartyUssdMessage();
                mess.UssdParameterID = pUssdParameterID;
                mess.VendingMessageID = pVendingMessageID;
                tblThirdPartyUssdMessageCrud crud = new tblThirdPartyUssdMessageCrud();
                crud.Insert(mess);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertThirdPartyUssdMessage - " + Ex.Message);
            }
        }



        public Byte InsertUssdValidationError(String pValidationError)
        {
            try
            {
                tblUssdValidationErrorCrud crud = new tblUssdValidationErrorCrud();
                tblUssdValidationError  message = new tblUssdValidationError();
                message.ValidationError = pValidationError;
                long ID = 0;
                crud.Insert(message, out ID);
                return (Byte)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdVendingMessage - " + Ex.Message);
            }
        }

        public Int16 InsertUssdVendingMessage(Byte pVendingMessageTypeID, String pVendingMessage)
        {
            try
            {
                tblUssdVendingMessageCrud crud = new tblUssdVendingMessageCrud();
                tblUssdVendingMessage message = new tblUssdVendingMessage();
                message.VendingMeddageTypeID = pVendingMessageTypeID;
                message.VendingMessage = pVendingMessage;
                long ID = 0;
                crud.Insert(message, out ID);
                return (Int16)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdVendingMessage - " + Ex.Message);
            }
        }


        public tblUssdVendingMessage GetUssdVendingMessage(String pVendingMessage)
        {
            try
            {
                tblUssdVendingMessageCrud crud = new tblUssdVendingMessageCrud();
                crud.Where("VendingMessage", DAL.General.Operator.Equals, pVendingMessage);
                recordFound = false;
                tblUssdVendingMessage mess = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return mess;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdVendingMessage - " + Ex.Message);
            }
        }
        public String BuildussdRegistryKey(Int32 pThirdPartyID, Byte pUssdGatewayID)
        {
            //try
            //{
                String regKey = "Windows Registry Editor Version 5.00\n\n";
                
                List<tblUssdMenu> menus = GetUssdMenuByThirdPartyID(pThirdPartyID, pUssdGatewayID);
                foreach (tblUssdMenu menu in menus)
                {
                    regKey += "[" + menu.RegistryKey + "]\n";
                    if (menu.MenuItemNumber != 0)
                        regKey += "\"MenuItemNr\"=" + "\"" + menu.MenuItemNumber.ToString() + "\"\n";
                    if (menu.MenuItemTextID != null)
                        regKey += "\"MenuItemText\"=" + "\"" + GetUssdMenuItemText((Int16)menu.MenuItemTextID).MenuItemText + "\"\n";
                    regKey += "\"WelcomeMsg\"=" + "\"" + GetUssdWelcomeMessage((Int16)menu.WelcomeMessageID).WelcomeMessage + "\"\n";
                    String confirm= (Boolean)menu.Confirm == true ? "\"Y\"\n" : "\"N\"\n";
                    regKey += "\"Confirm\"=" + confirm;
                    if (menu.ConfirmTextID != null)
                        regKey += "\"ConfirmText\"=" + "\"" + GetUssdConfirmText((Int16)menu.ConfirmTextID).ConfirmText + "\"\n";
                    if (menu.LeadInTextID != null)
                        regKey += "\"LeadInText\"=" + "\"" + GetUssdLeadInText((Int16)menu.LeadInTextID).LeadInText + "\"\n";
                    String prompt = (Boolean)menu.Confirm == true ? "\"Y\"\n": "\"N\"\n";
                    regKey += "\"Prompt\"=" + confirm;
                    if (menu.TagNameID != null)
                        regKey += "\"TagName\"=" + "\"" + GetUssdTagName((Int16)menu.TagNameID).TagName + "\"\n";
                    if (menu.TransactionID != null)
                        regKey += "\"Transaction\"=" + "\"" + GetUssdTransaction((Int16)menu.TransactionID).UssdTransaction + "\"\n";

                    if (menu.HttpPostID != null)
                    {
                        regKey += "\"DisableJumpIn\"=\"Y\"\n";
                    String url = GetUssdHttpPost((Byte)menu.HttpPostID).Url;
                        regKey += "\"HttpPost\"=\"" + url + "\"\n";
                        regKey += "\"UseLegacyPosting\"=\"1\"\n";
                    }
                    regKey += "\n";
                }

                return regKey;
            //}
            //catch (Exception Ex)
            //{
            //    throw new Exception(errorMessage + "BuildRegistryKey - " + Ex.Message);
            //}


        }

        public List<tblUssdMenu> GetUssdMenuByThirdPartyID(Int32 pThirdPartyID)
        {
            try
            {
                List<tblUssdMenu> ussdMenus = new List<tblUssdMenu>();
                List<tblThirdPartyFunction> thirdPartyFunctions = GetThirdPartyFunction(pThirdPartyID);
                foreach (tblThirdPartyFunction thirdPartyFunction in thirdPartyFunctions)
                {
                    List<tblUssdMenu> menus = GetUssdMenuByFunctionID((Int32)thirdPartyFunction.ThirdPartyFunctionID);
                    foreach (tblUssdMenu menu in menus)
                        ussdMenus.Add(menu);
                }

                return ussdMenus;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdMenuByThirdPartyID - " + Ex.Message);
            }
        }

        public List<tblUssdMenu> GetUssdMenuByThirdPartyID(Int32 pThirdPartyID, Byte pUssdGatewayID)
        {
            try
            {
                List<tblUssdMenu> ussdMenus = new List<tblUssdMenu>();
                List<tblThirdPartyFunction> thirdPartyFunctions = GetThirdPartyFunction(pThirdPartyID);
                foreach (tblThirdPartyFunction thirdPartyFunction in thirdPartyFunctions)
                {
                    List<tblUssdMenu> menus = GetUssdMenuByFunctionID((Int32)thirdPartyFunction.ThirdPartyFunctionID, pUssdGatewayID);
                    foreach (tblUssdMenu menu in menus)
                        ussdMenus.Add(menu);
                }

                if (ussdMenus.Count > 0)
                {
                    tblUssdMenu menu = GetUssdMenuByID((Int32)ussdMenus[0].ParentUssdMenuID);
                    ussdMenus.Insert(0, menu);
                }
                return ussdMenus;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdMenuByThirdPartyID - " + Ex.Message);
            }
        }

        public tblUssdMenu GetUssdMenuParent(String pRegistryKey, Byte pGatewayID)
        {
            try
            {
                pRegistryKey = pRegistryKey.Substring(0, pRegistryKey.LastIndexOf("\\"));
                tblUssdMenuCrud crud = new tblUssdMenuCrud();
                crud.Where("RegistryKey", DAL.General.Operator.Equals, pRegistryKey);
                crud.And("GatewayID", DAL.General.Operator.Equals, pGatewayID);
                recordFound = false;
                tblUssdMenu results = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return results;



            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdMenuParent - " + Ex.Message);
            }
        }



        
        public tblUssdMenu GetUssdMenuByRegistryKey(String pRegistryKey, Byte pGatewayID, Boolean pIsEqual)
        {
            try
            {
                tblUssdMenuCrud crud = new tblUssdMenuCrud();
                if (pIsEqual)
                    crud.Where("RegistryKey", DAL.General.Operator.Equals, pRegistryKey);
                else
                    crud.Where("RegistryKey", DAL.General.Operator.Like, "%" + pRegistryKey + "%");
                crud.And("GatewayID", DAL.General.Operator.Equals, pGatewayID);
                recordFound = false;
                crud.ExcludeReferenceTable = excludeRefTable;
                tblUssdMenu results = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return results;

            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdMenuByRegistryKey - " + Ex.Message);
            }
        }
        public List<tblUssdMenu> GetUssdMenuByRegistryKey(String pRegistryKey, Byte pGatewayID)
        {
            try
            {
                tblUssdMenuCrud crud = new tblUssdMenuCrud();
                crud.Where("RegistryKey", DAL.General.Operator.Like, "%" + pRegistryKey + "%");
                crud.And("GatewayID", DAL.General.Operator.Equals, pGatewayID);
                recordFound = false;
                crud.ExcludeReferenceTable = excludeRefTable;
                List<tblUssdMenu> results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                results = results.OrderBy(x => x.MenuLevelNo).ToList();
                return results;

            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdMenuByRegistryKey - " + Ex.Message);
            }
        }

        public List<tblUssdMenu> GetUssdMenuByFunctionID(Int32 pFunctionID, Byte pGatewayID)
        {
            try
            {
                tblUssdMenuCrud crud = new tblUssdMenuCrud();
                crud.Where("FunctionID", DAL.General.Operator.Equals, pFunctionID);
                crud.And("GatewayID", DAL.General.Operator.Equals, pGatewayID);
                recordFound = false;
                crud.ExcludeReferenceTable = excludeRefTable;
                List<tblUssdMenu> result = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return result;

            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdMenuByFunctionID - " + Ex.Message);
            }
        }

        public List<tblUssdMenu> GetUssdMenuByFunctionID(Int32 pFunctionID)
        {
            try
            {
                tblUssdMenuCrud crud = new tblUssdMenuCrud();
                crud.Where("FunctionID", DAL.General.Operator.Equals, pFunctionID);
                recordFound = false;
                List<tblUssdMenu> result = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return result;

            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdMenuByFunctionID - " + Ex.Message);
            }
        }
		public tblUssdMenuThirdParty GetUssdMenuThirdParty(Int32 pThirdPartyID, Int32 pGatewayID)
		{
			try
			{
				tblUssdMenuThirdPartyCrud crud = new tblUssdMenuThirdPartyCrud();
				crud.Where("ThirdPartyID", DAL.General.Operator.Equals, pThirdPartyID);
				crud.And("GatewayID", DAL.General.Operator.Equals, pGatewayID);
				recordFound = false;
				tblUssdMenuThirdParty result = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return result;

			}
			catch (Exception Ex)
			{
				throw new Exception("GetUssdMenuThirdParty -" + Ex.Message);
			}
		}


        /// <summary>
        /// Returns all records from tblUssdMenu.
        /// </summary>
        /// <returns></returns>
        public List<tblUssdMenu> GetUssdMenu()
        {
            try
            {
                recordFound = false;
                tblUssdMenuCrud reader = new tblUssdMenuCrud();
                reader.ExcludeReferenceTable = excludeRefTable;
                Collection<tblUssdMenu> menus = reader.ReadMulti();
                recordFound = reader.RecordFound;
                return menus.ToList();

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenu - " + Ex.Message);
            }
        }

		public tblUssdMenu GetUssdMenuPerID(Int32 pUssdMenuID)
		{
			try
			{
				tblUssdMenuCrud crud = new tblUssdMenuCrud();
				crud.Where("UssdMenuID", DAL.General.Operator.Equals, pUssdMenuID);
				recordFound = false;
				tblUssdMenu menu = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return menu;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetUssdMenuPerID - " + Ex.Message);
			}
		}


        public List<tblUssdMenuFunction> GetUssdMenuFunction()
        {
            try
            {
                tblUssdMenuFunctionCrud crud = new tblUssdMenuFunctionCrud();
                recordFound = false;
                List<tblUssdMenuFunction> result = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuFunction - " + Ex.Message);
            }
        }

        public tblUssdMenuFunction GetUssdMenuFunction(String pFunctionName)
		{
			try
			{
				tblUssdMenuFunctionCrud crud = new tblUssdMenuFunctionCrud();
				crud.Where("FunctionName", DAL.General.Operator.Equals, pFunctionName);
				recordFound = false;
				tblUssdMenuFunction result = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return result;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetUssdMenuFunction - " + Ex.Message);
			}
		}

		public Int32 InsertUssdMenuFunction(String pFuntionName, Int16 pTransactionID)
		{
			try
			{
				tblUssdMenuFunctionCrud crud = new tblUssdMenuFunctionCrud();
				tblUssdMenuFunction function = new tblUssdMenuFunction();
				function.FunctionName = pFuntionName;
				function.TransactionID = pTransactionID;
				long id = 0;
				crud.Insert(function, out id);
				return (Int32)id;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertUssdMenuFunction - " + Ex.Message);
			}
		}

        public List<tblUssdMenu> GetUssdMenu(Int32 pGatewayID, Boolean pIncludeReference)
        {
            try
            {
                recordFound = false;
                tblUssdMenuCrud reader = new tblUssdMenuCrud();
                reader.Where("GatewayID", DAL.General.Operator.Equals, pGatewayID);
                reader.OrderBy(x => (Int32)x.UssdMenuID);
                reader.ExcludeReferenceTable = pIncludeReference;
                Collection<tblUssdMenu> menus = reader.ReadMulti();
                recordFound = reader.RecordFound;
                return menus.ToList();

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenu - " + Ex.Message);
            }
        }


        public List<tblUssdMenu> GetUssdMenu(Int32 pGatewayID)
		{
			try
			{
				recordFound = false;
				tblUssdMenuCrud reader = new tblUssdMenuCrud();
				reader.Where("GatewayID", DAL.General.Operator.Equals, pGatewayID);
				reader.OrderBy(x => (Int32)x.UssdMenuID);
				reader.ExcludeReferenceTable = true;
				Collection<tblUssdMenu> menus = reader.ReadMulti();
				recordFound = reader.RecordFound;
				return menus.ToList();

			}
			catch (Exception Ex)
			{
				throw new Exception("GetUssdMenu - " + Ex.Message);
			}
		}

        public tblUssdMenu GetUssdMenuByID(Int32 pUssdMenuID)
        {
            try
            {
                recordFound = false;
                tblUssdMenuCrud reader = new tblUssdMenuCrud();
                reader.Where("UssdMenuID", DAL.General.Operator.Equals, pUssdMenuID);
                tblUssdMenu menus = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return menus;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuByID - " + Ex.Message);
            }
        }





        public tblUssdMenu GetUssdMenu(String pUssdMenuItem, Int32 pGatewayID)

		{
			try
			{
				tblUssdMenuItem menuItem = GetUssdMenuItem(pUssdMenuItem);

				recordFound = false;
				tblUssdMenuCrud reader = new tblUssdMenuCrud();
				reader.Where("MenuItemID", DAL.General.Operator.Equals, (Int32)menuItem.MenuItemID);
				reader.And("GatewayID", DAL.General.Operator.Equals, pGatewayID);
				reader.ExcludeReferenceTable = excludeRefTable;
				tblUssdMenu menus = reader.ReadSingle();
				recordFound = reader.RecordFound;
				return menus;

			}
			catch (Exception Ex)
			{
				throw new Exception("GetUssdMenu - " + Ex.Message);
			}
		}

        public tblUssdMenu GetUssdMenuByUssdMenuID(Int32 pUssdMenuID)
        {
            try
            {
                recordFound = false;
                tblUssdMenuCrud reader = new tblUssdMenuCrud();
                reader.ExcludeReferenceTable = excludeRefTable;
                reader.Where("UssdMenuID", DAL.General.Operator.Equals, pUssdMenuID);
                tblUssdMenu menus = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return menus;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenu - " + Ex.Message);
            }
        }

        public tblUssdMenu GetUssdMenu(String pRegistryKey)
		{
			try
			{
				recordFound = false;
				tblUssdMenuCrud reader = new tblUssdMenuCrud();
				reader.ExcludeReferenceTable = excludeRefTable;
				reader.Where("REgistryKey", DAL.General.Operator.Equals, pRegistryKey);
                reader.ExcludeReferenceTable = true;
				tblUssdMenu menus = reader.ReadSingle();
				recordFound = reader.RecordFound;
				return menus;

			}
			catch (Exception Ex)
			{
				throw new Exception("GetUssdMenu - " + Ex.Message);
			}
		}



        #region Inserts
        /// <summary>
        /// Insert a record to tblUssdHttpPost.This is the url where the collected information from the ussd session are post to.
        /// </summary>
        /// <param name="pUrl">The url ro post the information to.</param>
        /// <returns></returns>
        public Byte InsertUssdHttpPost(String pUrl)
        {
            try
            {
                tblUssdHttpPost post = new tblUssdHttpPost();
                post.Url = pUrl;
                long ID = 0;
                tblUssdHttpPostCrud crud = new tblUssdHttpPostCrud();
                crud.Insert(post, out ID);
                return (Byte)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdHttpPost-" + Ex.Message);
            }
        }


        /// <summary>
        /// Insert a record to tblUssdMenu. This is a database representation of what is in the registry. Used for ussd transaction.
        /// </summary>
        /// <param name="pGatewayID">The gateway. Mascom, beMobile or Truteq</param>
        /// <param name="pMenuItemID">The menu item as it is setup in the registry.</param>
        /// <param name="pMenuItemNo">The number of the current menu.</param>
        /// <param name="pMenuItemTextID">The menu text displayed to the customer.</param>
        /// <param name="pConfirm">Indicates if the customer should confirm this entered value.</param>
        /// <param name="pConfirmTextID">The confirm text shown to the customer.</param>
        /// <param name="pLeadInTextID">The text shown when the user has to select from a menu.</param>
        /// <param name="pPrompt"></param>
        /// <param name="pTagNameID">The xml tag passed to the server pages.</param>
        /// <param name="pWelcomeMessageID">The top most message shown on the ussd screens.</param>
        /// <param name="pTransactionID">The transaction passed to the server pages. Indicates what functionality to call.</param>
        /// <returns></returns>
        public Int32 InsertUssdMenu(Byte pGatewayID, Int32? pMenuItemID, Int16? pMenuItemNo, Int16? pMenuItemTextID, Boolean? pConfirm, Int16? pConfirmTextID, Int16? pLeadInTextID, Boolean? pPrompt, Int16? pTagNameID, Int16? pWelcomeMessageID, Int16? pTransactionID, Byte pMenuLevelNo, Byte? pHttpPostID, String pRegistryKey, Int32? pParentID, Int32? pFunctionID )
        {
            try
            {
               
                tblUssdMenu menu = new tblUssdMenu();
                menu.Confirm = pConfirm;
                menu.ConfirmTextID = pConfirmTextID;
                menu.GatewayID = pGatewayID;
                menu.LeadInTextID = pLeadInTextID;
                menu.MenuItemID = pMenuItemID;
                menu.MenuItemNumber = pMenuItemNo;
                menu.MenuItemTextID = pMenuItemTextID;
                menu.Promt = pPrompt;
                menu.TagNameID = pTagNameID;
                menu.TransactionID = pTransactionID;
                menu.WelcomeMessageID = pWelcomeMessageID;
                menu.MenuLevelNo = pMenuLevelNo;
                menu.HttpPostID = pHttpPostID;
                menu.RegistryKey = pRegistryKey;
				menu.ParentUssdMenuID = pParentID;
                menu.FunctionID = pFunctionID;
                tblUssdMenuCrud crud = new tblUssdMenuCrud();
                long ID = 0;
                crud.Insert(menu, out ID);

                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdMenu-" + Ex.Message);
            }
        }


        public Boolean UpdateUssdMenuTest(String pRegistryKey, Int16? pWelcomeMessageID, Int16? pMenuItemNumber, Int16? pMenuItemTextID, Boolean pConfirm, Int16? pConfirmTextID)
        {
            try
            {
                tblUssdMenu menu = GetUssdMenu(pRegistryKey);
                if (recordFound)
                {
                    
                }
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdMenu - " + Ex.Message);
            }
        }

        public Boolean UpdateUssdMenu(String pRegistryKey, Int16? pWelcomeMessageID, Int16? pMenuItemNumber, Int16? pMenuItemTextID, Boolean? pConfirm, Int16? pConfirmTextID, Int16? pLeadinTextID, Boolean? pPrompt, Int16? pTagnameID, Int16? pTransactionID, Int32? pMenuItemID, Byte? pHttpPostID, Byte pMenuLevelNumber)
        {
            try
            {
                tblUssdMenu menu = GetUssdMenu(pRegistryKey);
                if (recordFound)
                {
                    menu.WelcomeMessageID = pWelcomeMessageID;
                    menu.MenuItemNumber = pMenuItemNumber;
                    menu.MenuItemTextID = pMenuItemTextID;
                    //if (pConfirm)
                    //{
                        menu.Confirm = pConfirm;
                        menu.ConfirmTextID = pConfirmTextID;
                    //}
                    //else
                    //{
                    //    menu.Confirm = null;
                    //    menu.ConfirmTextID = null;
                   // }
                    menu.LeadInTextID = pLeadinTextID;
                    //if (!pPrompt)
                        menu.Promt = pPrompt;
                    //else
                    //    menu.Promt = true;
                    menu.TagNameID = pTagnameID;
                    menu.TransactionID = pTransactionID;
                    menu.MenuItemID = pMenuItemID;
                    if (pHttpPostID == 0)
                        menu.HttpPostID = null;
                    else
                        menu.HttpPostID = pHttpPostID;
                    menu.MenuLevelNo = pMenuLevelNumber;

                    tblUssdMenuCrud crud = new tblUssdMenuCrud();
                    crud.Update(menu);
                }
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdMenu - " + Ex.Message);
            }
        }


        public Boolean UpdateUssdMenu(String pRegistryKey, Int32 pFunctionID)
        {
            try
            {
                tblUssdMenu menu = GetUssdMenu(pRegistryKey);
                if (recordFound)
                {
                    menu.FunctionID = pFunctionID;
                    tblUssdMenuCrud crud = new tblUssdMenuCrud();
                    crud.Update(menu);
                }
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdMenu - " + Ex.Message);
            }
        }
        /// <summary>
        /// Insert a record to tblUssdTransaction. This value gets passed to the server pages in order to identify functionality.
        /// </summary>
        /// <param name="pTransaction">The ussd transaction.</param>
        /// <returns></returns>
        public Int16 InsertUssdTransaction(String pTransaction)
        {
            try
            {
                tblUssdTransaction ins = new tblUssdTransaction();
                ins.UssdTransaction = pTransaction;
                tblUssdTransactionCrud crud = new tblUssdTransactionCrud();
                long ID = 0;
                crud.Insert(ins, out ID);

                return (Int16)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdTransaction-" + Ex.Message);
            }
        }




        /// <summary>
        /// Insert a record to tblUssdWelcomeMessage. This is the intro message on each ussd screen.
        /// </summary>
        /// <param name="pWelcomeMessage">The message shown at the to of each ussd screen.</param>
        /// <returns></returns>
        public Int16 InsertUssdWelcomeMessage(String pWelcomeMessage)
        {
            try
            {
                tblUssdWelcomeMessage mess = new tblUssdWelcomeMessage();
                mess.WelcomeMessage = pWelcomeMessage;
                tblUssdWelcomeMessageCrud crud = new tblUssdWelcomeMessageCrud();
                long ID = 0;
                crud.Insert(mess, out ID);
                return (Int16)ID;
            }
            catch (Exception Ex)
            {

                throw new Exception("InsertUssdWelcomeMessage-" + Ex.Message);
            }
        }



        /// <summary>
        /// Insert a record to tblUssdTagName. This is the xml tag send to the server .aspx page.
        /// </summary>
        /// <param name="pTagName">The xml tag name.</param>
        /// <returns></returns>
        public Int16 InsertUssdTagName(String pTagName)
        {
            try
            {
                tblUssdTagName ins = new tblUssdTagName();
                ins.TagName = pTagName;
                tblUssdTagNameCrud crud = new tblUssdTagNameCrud();
                long ID = 0;
                crud.Insert(ins, out ID);
                return (Int16)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdTagName - " + Ex.Message);
            }
        }




        public Int16 InsertUssdInstruction(String pInstruction)
        {
            try
            {
                tblUssdInstruction ins = new tblUssdInstruction();
                tblUssdInstructionCrud crud = new tblUssdInstructionCrud();
                ins.Instruction = pInstruction;
                long ID = 0;
                crud.Insert(ins, out ID);

                return (Int16)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdInstruction - " + Ex.Message); 
            }
        }
        /// <summary>
        /// Insert a record to tblUssdLeadInText. This is the text shown when a user has to select a menu item during a ussd session.
        /// </summary>
        /// <param name="pLeadInText">Text shown on top of a menu selection.</param>
        /// <returns></returns>
        public Int16 InsertUssdLeadInText(String pLeadInText)
        {
            try
            {
                tblUssdLeadInText ins = new tblUssdLeadInText();
                ins.LeadInText = pLeadInText;
                tblUssdLeadInTextCrud crud = new tblUssdLeadInTextCrud();
                long ID = 0;
                crud.Insert(ins, out ID);
                return (Int16)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdLeadInText-" + Ex.Message);
            }
        }


        /// <summary>
        /// Insert a record to tblUssdMenuItem as it should appear in the registry. This is mainly for ussd functionality.
        /// </summary>
        /// <param name="pMenuItem">Name of the menu item. Eg 1 Ap (Payment) and MultiChoice</param>
        /// <returns></returns>
        public Int32 InsertUssdMenuItem(String pMenuItem)
        {
            try
            {
                tblUssdMenuItem ins = new tblUssdMenuItem();
                ins.MenuItem = pMenuItem;
                tblUssdMenuItemCrud reader = new tblUssdMenuItemCrud();
                long returnID = 0;
                reader.Insert(ins, out returnID);
                return (Int32)returnID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdMenuItem-" + Ex.Message);
            }
        }



        /// <summary>
        /// Insert a record to tblUssdMenuText. This is the information displayed to the customer during a ussd session.
        /// </summary>
        /// <param name="pMenuItemText">The menu item to show the customer.</param>
        /// <returns></returns>
        public Int16 InsertUssdMenuItemText(String pMenuItemText)
        {
            try
            {
                tblUssdMenuItemText ins = new tblUssdMenuItemText();
                ins.MenuItemText = pMenuItemText;
                tblUssdMenuItemTextCrud crud = new tblUssdMenuItemTextCrud();
                long returnID = 0;
                crud.Insert(ins, out returnID);
                return (Int16)returnID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdMenuItemText-" + Ex.Message);
            }
        }





        /// <summary>
        /// Insert a record to tbUssdConfirmText. This is the information shown when the user has entered all information and needs to confirm which was entered.
        /// </summary>
        /// <param name="pConfirmText"></param>
        /// <returns>The text to display. Eg Card Number = 5454545454545454</returns>
        public Int16 InsertUssdConfirmText(String pConfirmText)
        {
            try
            {
                tblUssdConfirmText ins = new tblUssdConfirmText();
                ins.ConfirmText = pConfirmText;
                tblUssdConfirmTextCrud crud = new tblUssdConfirmTextCrud();
                long ID = 0;
                crud.Insert(ins, out ID);
                return (Int16)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdConfirmText-" + Ex.Message);
            }
        }

        #endregion

        #region Deletes

        #endregion
    }
}
