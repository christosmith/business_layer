﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.DAL;

namespace DPO.BackOffice.BL
{
    public partial class BackOfficeBL
    {
        public tblUssdProcessTrace GetUssdProcessTrace(String pMsisdn, Int32 pProcessID)
        {
            try
            {
                tblUssdProcessTraceCrud crud = new tblUssdProcessTraceCrud();
                crud.Where("Msisdn", DAL.General.Operator.Equals, pMsisdn);
                crud.And("SmsID", DAL.General.Operator.Equals, pProcessID);
                recordFound = false;
                tblUssdProcessTrace result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdProcessTrace(String, Int32) - " + Ex.Message);
            }

        }


        public tblUssdProcessTrace GetUssdProcessTrace(String pMsisdn, Int32 pThirdPartyFunctionID, DateTime pStartDateTime, DateTime pEndDateTime)
        {
            try
            {
                tblUssdProcessTraceCrud crud = new tblUssdProcessTraceCrud();
                crud.Where("Msisdn", DPO.BackOffice.DAL.General.Operator.Equals, pMsisdn);
                crud.And("ThirdPartyFunctionID", DPO.BackOffice.DAL.General.Operator.Equals, pThirdPartyFunctionID);
                crud.And("LogDateTime", DPO.BackOffice.DAL.General.Operator.Equals, pStartDateTime, pEndDateTime);
                recordFound = false;
                tblUssdProcessTrace result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdProcessTrace - " + Ex.Message);
            }
        }

        public tblUssdProcessTrace GetUssdProcessTrace(String pMsisdn, DateTime pLogDateTime)
        {
            try
            {
                tblUssdProcessTraceCrud crud = new tblUssdProcessTraceCrud();
                crud.Where("Msisdn", DAL.General.Operator.Equals, pMsisdn);
                crud.And("LogDateTime", DAL.General.Operator.Equals, pLogDateTime);
                recordFound = false;
                tblUssdProcessTrace result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdProcessTrace - " + Ex.Message);
            }
        }

        public Int32 InsertUssdProcessTrace(String pMsisdn, Byte pGatewayID, DateTime pLogDateTime, Byte pGatewayErrorID, Int32 pSmsID)
        {
            try
            {
                tblUssdProcessTraceCrud crud = new tblUssdProcessTraceCrud();
                tblUssdProcessTrace trace = new tblUssdProcessTrace();
                trace.Authorised = false;
                trace.GatewayID = pGatewayID;
                trace.LogDateTime = pLogDateTime;
                trace.Msisdn = pMsisdn;
                trace.SmsSend = false;
                trace.WebPageCalled = false;
                trace.ThirdPartyCalled = false;
                trace.GatewayInsert = false;
                trace.GatewayErrorID = pGatewayErrorID;
                trace.SmsID = pSmsID;
                long ID = 0;
                crud.Insert(trace, out ID);
                return (Int32)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdProcessTrace - " + Ex.Message);
            }
        }


        public Int32 InsertUssdProcessTrace(String pMsisdn, Byte pGatewayID, Int32 pThirdPartyFunctionID, DateTime pLogDateTime, Boolean pGatewayInsert)
        {
            try
            {
                tblUssdProcessTraceCrud crud = new tblUssdProcessTraceCrud();
                tblUssdProcessTrace trace = new tblUssdProcessTrace();
                trace.Authorised = false;
                trace.GatewayID = pGatewayID;
                trace.LogDateTime = pLogDateTime;
                trace.Msisdn = pMsisdn;
                trace.SmsSend = false;
                trace.WebPageCalled = false;
                trace.ThirdPartyCalled = false;
                trace.ThirdPartyFunctionID = pThirdPartyFunctionID;
                trace.GatewayInsert = pGatewayInsert;
                long ID = 0;
                crud.Insert(trace, out ID);
                return (Int32)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdProcessTrace - " + Ex.Message);
            }
        }

        public List<tblUssdProcessTrace> GetUssdProcessTrace(DateTime pDateTime)
        {
            try
            {
                tblUssdProcessTraceCrud crud = new tblUssdProcessTraceCrud();
                crud.Where("LogDateTime", BackOffice.DAL.General.Operator.GreaterThanOrEqual, pDateTime);
                recordFound = false;
                crud.ExcludeReferenceTable = excludeRefTable;
                List<tblUssdProcessTrace> result = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetUssdProcessTrace(DateTime) - " + Ex.Message);
            }
        }

    }
}
