﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.DAL;

namespace DPO.BackOffice.BL
{
    public partial class BackOfficeBL
    {
        public List<tblProcessRunVerify> GetProcessRunVerify()
        {
            try
            {
				tblProcessRunVerifyCrud crud = new tblProcessRunVerifyCrud();
				crud.Where("DoVerify", General.Operator.Between, 1);
				List<tblProcessRunVerify> results = crud.ReadMulti().ToList();
				return results;

            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public tblProcessRunVerify GetProcessRunVerify(Int32 pApplicationID)
        {
            try
            {
				tblProcessRunVerifyCrud crud = new tblProcessRunVerifyCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, pApplicationID);
				tblProcessRunVerify result = crud.ReadSingle();
				tblProcessRunVerify search = new tblProcessRunVerify();
				return result;

            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public Boolean UpdateProcessRunVerify(String pApplicationName, DateTime pCheckInDateTime, Byte pCheckCounter)
        {
			try
			{
				tblApplication application = GetApplicationByName(pApplicationName);
				tblProcessRunVerifyCrud crud = new tblProcessRunVerifyCrud();
				crud.Where("ApplicationID", DAL.General.Operator.Equals, (Int32)application.ApplicationID);
				tblProcessRunVerify process = crud.ReadSingle();
				if (crud.RecordFound)
				{ 
                    process.CheckInDateTime = pCheckInDateTime;
                    process.CheckCounter = pCheckCounter;
					crud.Update(process);
                }

                return true;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
