﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.DAL;

namespace DPO.BackOffice.BL
{
    public partial class BackOfficeBL
    {
        public tblWCFServiceRestriction GetWCFServiceRestriction(String pUserID)
        {
            try
            {
                tblWCFServiceRestrictionCrud crud = new tblWCFServiceRestrictionCrud();
                crud.Where("UserID", DAL.General.Operator.Equals, pUserID);
                recordFound = false;
                tblWCFServiceRestriction result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetWCFServiceRestriction - " + Ex.Message);
            }
        }
        public List<tblWCFServiceRestrictionApply> GetWCFServiceRestrictionApply(String pUserID)
        {
            try
            {
                tblWCFServiceRestriction restriction = GetWCFServiceRestriction(pUserID);
                if (recordFound == false) return null;

                tblWCFServiceRestrictionApplyCrud crud = new tblWCFServiceRestrictionApplyCrud();
                crud.Where("RestrictionID", DAL.General.Operator.Equals, (Int32)restriction.RestrictionID);
                recordFound = false;
                List<tblWCFServiceRestrictionApply> results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return results;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetWCFServiceRestrictionApply - " + Ex.Message);
            }
        }

        public List<tblWCFServiceRestrictionType> GetWCFServiceRestrictionType()
        {
            try
            {
                tblWCFServiceRestrictionTypeCrud crud = new tblWCFServiceRestrictionTypeCrud();
                recordFound = false;
                List<tblWCFServiceRestrictionType> results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return results;

            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetWCFServiceRestrictionType - " + Ex.Message);
            }
        }
        public Boolean CreateServiceRestriction(String pServiceName, String pUserID)
        {
            try
            {
                Byte wcfServiceID = (Byte)GetWCFService(pServiceName).WCFServiceID;
                tblWCFServiceRestriction restriction = GetWCFServiceRestriction(pUserID);
                Int32 restrictionID = 0;
                if (recordFound)
                    restrictionID = (Int32)restriction.RestrictionID;
                else
                    restrictionID = InsertWCFServiceRestriction(wcfServiceID, pUserID);

                List<tblWCFServiceRestrictionType> types = GetWCFServiceRestrictionType();
                foreach (tblWCFServiceRestrictionType type in types)
                {
                    String value1 = ""; String value2 = "";
                    switch (type.RestrictionType)
                    {
                        case "Maximum WCF Calls":
                            value1 = GetSetting("Virtual Online", "VCSQuery", "RestrictionRows").SettingValue;
                            break;
                        case "Allowed Operational Times":
                            value1 = GetSetting("Virtual Online", "VCSQuery", "RestrictionStartTime").SettingValue;
                            value2 = GetSetting("Virtual Online", "VCSQuery", "RestrictionEndTime").SettingValue;
                            break;

                    }

                    InsertWCFServiceRestrictionApply(restrictionID, (Byte)type.RestrictionTypeID, value1, value2);
                }

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "CreateServiceRestriction - " + Ex.Message);
            }

        }

        public Int32 InsertWCFServiceRestrictionApply(Int32 pRestrictionID, Byte pRestrictionTypeID, String pRestrictionValue1, String pRestrictionValue2)
        {
            try
            {
                tblWCFServiceRestrictionApplyCrud crud = new tblWCFServiceRestrictionApplyCrud();
                tblWCFServiceRestrictionApply apply = new tblWCFServiceRestrictionApply();
                apply.OverrideRestriction = false;
                apply.RestrictionID = pRestrictionID;
                apply.RestrictionTypeID = pRestrictionTypeID;
                apply.RestrictionValue1 = pRestrictionValue1;
                apply.RestrictionValue2 = pRestrictionValue2;
                long Id = 0;
                crud.Insert(apply, out Id);

                return (Int32)Id;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "InsertWCFServiceRestrictionApply - " + Ex.Message); 
            }
        }
        public Int32 InsertWCFServiceRestriction(Byte pWCFServiceID, String pUserID)
        {
            try
            {
                tblWCFServiceRestrictionCrud crud = new tblWCFServiceRestrictionCrud();
                tblWCFServiceRestriction res = new tblWCFServiceRestriction();
                res.UserID = pUserID;
                res.WCFServiceID = pWCFServiceID;
                long ID = 0;
                crud.Insert(res, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "InsertWCFServiceRestriction - " + Ex.Message);
            }
        }
        public tblWCFServiceUsage GetWCFServiceUsage(Int32 pWCFServiceUsageID)
        {
            try
            {
                tblWCFServiceUsageCrud crud = new tblWCFServiceUsageCrud();
                crud.Where("WCFServiceUsageID", DAL.General.Operator.Equals, pWCFServiceUsageID);
                recordFound = false;
                tblWCFServiceUsage result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetWCFServiceUsage - " + Ex.Message);
            }
        }

        public List<tblWCFServiceUsage> GetWCFServiceUsage(String pUserID, DateTime pUsageDate)
        {
            try
            {
                tblWCFServiceUsageCrud crud = new tblWCFServiceUsageCrud();
                crud.Where("UserID", DAL.General.Operator.Equals, pUserID);
                crud.And("UsageDate", DAL.General.Operator.Equals, pUsageDate);
                recordFound = false;
                List<tblWCFServiceUsage> result = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetWCFServiceUsage - " + Ex.Message);
            }
        }
        public tblWCFServiceUsage GetWCFServiceUsage(Int16 pMethodID, String pUserID, DateTime pUsageDate)
        {
            try
            {
                tblWCFServiceUsageCrud crud = new tblWCFServiceUsageCrud();
                crud.Where("MethodID", DAL.General.Operator.Equals, pMethodID);
                crud.And("UserID", DAL.General.Operator.Equals, pUserID);
                crud.And("UsageDate", DAL.General.Operator.Equals, pUsageDate);
                recordFound = false;
                tblWCFServiceUsage result = crud.ReadSingle(); ;
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetWCFServiceUsage - " + Ex.Message);
            }
        }

        public List<tblWCFServiceUsageCount> GetWCFServiceUsageCount(Int32 pWCFServiceUsageID)
        {
            try
            {
                tblWCFServiceUsageCountCrud crud = new tblWCFServiceUsageCountCrud();
                crud.Where("WCFServiceUsageID", DAL.General.Operator.Equals, pWCFServiceUsageID);
                recordFound = false;
                List<tblWCFServiceUsageCount> result = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return result;

            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetWCFServiceUsageCount - " + Ex.Message);
            }
        }

        public tblWCFServiceUsageCount GetWCFServiceUsageCount(Int32 pWCFServiceUsageID, Byte pUsageCountTypeID)
        {
            try
            {
                tblWCFServiceUsageCountCrud crud = new tblWCFServiceUsageCountCrud();
                crud.Where("WCFServiceUsageID", DAL.General.Operator.Equals, pWCFServiceUsageID);
                crud.And("UsageCountTypeID", DAL.General.Operator.Equals, pUsageCountTypeID);
                recordFound = false;
                tblWCFServiceUsageCount result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;

            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetWCFServiceUsageCount - " + Ex.Message);
            }
        }
        
        public Boolean UpdateWCFServiceUsageCount(Int32 pWCFServiceUsageID, Byte pUsageCountTypeID)
        {
            try
            {
                tblWCFServiceUsageCount usageCount = GetWCFServiceUsageCount(pWCFServiceUsageID, pUsageCountTypeID);
                tblWCFServiceUsageCountCrud crud = new tblWCFServiceUsageCountCrud();
                if (!recordFound)
                {
                    usageCount = new tblWCFServiceUsageCount();
                    
                    usageCount.UsageCount = 1;
                    usageCount.UsageCountTypeID = pUsageCountTypeID;
                    usageCount.WCFServiceUsageID = pWCFServiceUsageID;
                    crud.Insert(usageCount);
                }
                else
                {
                    usageCount.UsageCount = usageCount.UsageCount + 1;
                    crud.Update(usageCount);
                }

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "UpdateWCFServiceUsage - " + Ex.Message);
            }
        }
        public Int32 InsertWCFServiceUsage(String pWCFService, String pMethod, String pUserID)
        {
            try
            {
                Int32 wcfServiceUsageID = 0;
                tblWCFService service = GetWCFService(pWCFService);
                tblWCFServiceMethod method = GetWCFServiceMethod((Byte)service.WCFServiceID, pMethod);
                tblWCFServiceUsage usage = GetWCFServiceUsage((Int16)method.MethodID, pUserID, new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
                tblWCFServiceUsageCrud crud = new tblWCFServiceUsageCrud();
                if (!recordFound)
                {
                    usage = new tblWCFServiceUsage();
                    usage.MethodID = (Int16)method.MethodID;
                    usage.UsageDate = DateTime.Now;
                    usage.UserID = pUserID;
                    long ID = 0;
                    crud.Insert(usage, out ID);
                    wcfServiceUsageID = (Int32)ID;
                    UpdateWCFServiceUsageCount(wcfServiceUsageID, 1);
                }
                else
                {
                    wcfServiceUsageID = (Int32)usage.WCFServiceUsageID;
                    UpdateWCFServiceUsageCount(wcfServiceUsageID, 1);
                }

                return wcfServiceUsageID;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "InsertWCFServiceUsage - " + Ex.Message);
            }
        }
        public tblWCFServiceMethod GetWCFServiceMethod(Byte pWCFServiceID, String pMethod)
        {
            try
            {
                tblWCFServiceMethodCrud crud = new tblWCFServiceMethodCrud();
                crud.Where("WCFServiceID", DAL.General.Operator.Equals, pWCFServiceID);
                crud.And("Method", DAL.General.Operator.Equals, pMethod);
                recordFound = false;
                tblWCFServiceMethod result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetWCFServiceMethod -" + Ex.Message); 
            }
        }

        public tblWCFService GetWCFService(String pServiceName)
        {
            try
            {
                tblWCFServiceCrud crud = new tblWCFServiceCrud();
                crud.Where("WCFService", DAL.General.Operator.Equals, pServiceName);
                recordFound = false;
                tblWCFService result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception(errorMessage + "GetWCFService - " + Ex.Message); 
            }
        }
    }
}
