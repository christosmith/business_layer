﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.DAL;
using System.Net.Sockets;
using System.IO;
using System.ServiceProcess;
using System.Diagnostics;
namespace DPO.BackOffice.BL
{
    public partial class BackOfficeBL
    {
        public String GetApplicationProcessInfo(Int32 pApplicationID)
        {
            try
            {
                tblApplication app = GetApplication(pApplicationID);
                String appName = app.vcsApplication;
                List<Process> processes = Process.GetProcesses().ToList();
                processes = processes.FindAll(x => x.ProcessName == appName).ToList();

                DateTime startTime = DateTime.Now;
                String cpuTime = "";
                Int64 memUsage = -1;
                String returnStr = "";
                if (processes.Count == 0)
                {
                    returnStr = "Process is not running.";
                }
                else
                {
                    startTime = processes[0].StartTime;
                    cpuTime = processes[0].TotalProcessorTime.ToString();
                    memUsage = processes[0].WorkingSet64;
                    returnStr = startTime.ToString() + "|" + cpuTime + "|" + memUsage.ToString();

                }
                return returnStr;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetApplicationMemoryUsage - " + Ex.Message);
            }
        }
        public List<tblWindowsServiceRestart> GetWindowsServiceRestart()
        {
            try
            {
                tblWindowsServiceRestartCrud crud = new tblWindowsServiceRestartCrud();
                recordFound = false;
                List<tblWindowsServiceRestart> recs = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;

                return recs;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetWindowsServiceRestart - " + Ex.Message);
            }
        }
        public String RestartWindowsService(Byte pServiceID)
        {
            try
            {
                tblWindowsServiceRestart service = GetWindowsServiceRestart(pServiceID);
                if (!recordFound)
                    throw new Exception("Service with id " + pServiceID.ToString() + " was not found or is not marked for restart.");

                String returnStr = "";
                String message = "";
                if (service.tblWindowsService.tblApplication.ServerID == 3)
                {

                    message = "Restart:" + service.tblWindowsService.WindowsServiceName + "|" + service.DelayRestart.ToString();
                    TcpClient tcpclnt = new TcpClient();
                    tcpclnt.Connect("192.168.1.183", 3097);

                    Stream stm = tcpclnt.GetStream();
                    ASCIIEncoding asen = new ASCIIEncoding();
                    byte[] ba = asen.GetBytes(message);
                    stm.Write(ba, 0, ba.Length);
                    byte[] bb = new byte[ba.Length];
                    int k = stm.Read(bb, 0, ba.Length);

                    String socketMessage = "";
                    for (int i = 0; i < k; i++)
                        socketMessage += Convert.ToChar(bb[i]);

                    tcpclnt.Close();

                    if (socketMessage.Contains("Error"))
                        throw new Exception(socketMessage);

                    returnStr = socketMessage;
                }
                else
                {
                    System.ServiceProcess.ServiceController controller = new ServiceController(service.tblWindowsService.WindowsServiceName);
                    //if (controller.StartType == "Disabled")
                    if ((controller.Status.Equals(ServiceControllerStatus.Running)) || (controller.Status.Equals(ServiceControllerStatus.StartPending)))
                    {
                        controller.Stop();
                    }
                    if (Convert.ToInt16(service.DelayRestart) > 0)
                        System.Threading.Thread.Sleep(Convert.ToInt16(service.DelayRestart));

                    controller.WaitForStatus(ServiceControllerStatus.Stopped);
                    controller.Start();
                    TimeSpan timeSpan = new TimeSpan(0, 0, 45);
                    controller.WaitForStatus(ServiceControllerStatus.Running, timeSpan);
                    returnStr = "Service Restarted";

                }

                UpdateWindowsService(pServiceID);
                return returnStr;
            }
            catch (Exception Ex)
            {
                throw new Exception("RestartWindowsService-" + Ex.Message);
            }
        }

        public Boolean UpdateWindowsService(Byte pServiceID)
        {
            try
            {
                tblWindowsService service = GetWindowsService(pServiceID);
                service.DateTimeStarted = DateTime.Now;
                tblWindowsServiceCrud crud = new tblWindowsServiceCrud();
                crud.Update(service);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UPdateWindowsServiceRestart - " + Ex.Message);
            }
        }
        public tblWindowsServiceRestart GetWindowsServiceRestart(Byte pServiceID)
        {
            try
            {
                tblWindowsServiceRestartCrud crud = new tblWindowsServiceRestartCrud();
                crud.Where("ServiceID", General.Operator.Equals, pServiceID);
                recordFound = false;
                tblWindowsServiceRestart restart = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return restart;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetWindowsServiceRestart - " + Ex.Message);
            }
        }
        public tblWindowsService GetWindowsService(Byte pServiceID)
        {
            try
            {
                tblWindowsServiceCrud crud = new tblWindowsServiceCrud();
                crud.Where("ServiceID", DAL.General.Operator.Equals, pServiceID);
                recordFound = false;
                tblWindowsService service = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return service;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetWindowsService - " + Ex.Message);
            }
        }

        public tblWindowsService GetWindowsService(String pWindowsServiceName)
        {
            try
            {
                tblWindowsServiceCrud crud = new tblWindowsServiceCrud();
                crud.Where("WindowsServiceName", DAL.General.Operator.Equals, pWindowsServiceName);
                recordFound = false;
                tblWindowsService service = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return service;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetWindowsService - " + Ex.Message);
            }
        }
        public List<tblWindowsService> GetWindowsService()
        {
            try
            {
                tblWindowsServiceCrud crud = new tblWindowsServiceCrud();
                return crud.ReadMulti().ToList();
            }
            catch (Exception Ex)
            {
                throw new Exception("GetWindowsService - " + Ex.Message);
            }
        }
    }
}
