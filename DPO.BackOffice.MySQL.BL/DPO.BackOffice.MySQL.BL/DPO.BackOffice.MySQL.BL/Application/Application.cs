﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.MySQL.DAL;


namespace DPO.BackOffice.MySQL.BL
{
    public partial class MySQLBackOfficeBL
    {
        public tblsetting GetSettingMS(String pSystem, String pApplication, String pSubApplication, String pSettingName)
        {
            try
            {
                tblapplicationsubapp subApplication = GetSubApplicationByNameMS(pApplication, pSubApplication);
                tblsettingCrud crud = new tblsettingCrud();
                crud.Where("ApplicationID", MySQL.DAL.General.Operator.Equals, (Int32)subApplication.tblapplication.ApplicationID);
                crud.And("SubApplicationID", MySQL.DAL.General.Operator.Equals, (Int32)subApplication.SubApplicationID);
                crud.And("SettingName", MySQL.DAL.General.Operator.Equals, pSettingName);
                recordFound = false;
                tblsetting setting = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return setting;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetSetting (string, string, string) -" + Ex.Message);
            }
        }
        public tblapplication GetApplicationByNameMS(Byte pSystemID, String pApplicationName)
        {
            try
            {
                recordFound = false;
                tblapplicationCrud crud = new tblapplicationCrud();
                crud.Where("SystemID", MySQL.DAL.General.Operator.Equals, pSystemID);
                crud.And("vcsApplication", MySQL.DAL.General.Operator.Equals, pApplicationName);

                tblapplication search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetApplicationByNameMS - " + Ex.Message);
            }
        }
        public tblapplication GetApplicationByNameMS(String pApplicationName)
        {
            try
            {
                recordFound = false;
                tblapplicationCrud crud = new tblapplicationCrud();
                crud.Where("ApplicationName", MySQL.DAL.General.Operator.Equals, pApplicationName);

                tblapplication search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetApplicationByNameMS - " + Ex.Message);
            }
        }
        public tblsetting GetSettingMS(String pSystem, String pApplication, String pSettingName)
        {
            try
            {
                tblsystem system = GetSystemByNameMS(pSystem);
                tblapplication application = GetApplicationByNameMS((Byte)system.SystemID, pApplication);
                tblsettingCrud crud = new tblsettingCrud();
                crud.Where("ApplicationID", MySQL.DAL.General.Operator.Equals, (Int32)application.ApplicationID);
                crud.And("SettingName", MySQL.DAL.General.Operator.Equals, pSettingName);
                recordFound = false;
                tblsetting setting = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return setting;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetSetting (string, string, string) -" + Ex.Message);
            }
        }

        public tblapplicationsubapp GetSubApplicationByNameMS(String pApplicationName, String pSubApplicationName)
        {
            try
            {
                tblapplication app = GetApplicationByNameMS(pApplicationName);

                tblapplicationsubappCrud crud = new tblapplicationsubappCrud();
                crud.Where("ApplicationID", MySQL.DAL.General.Operator.Equals, (Int32)app.ApplicationID);
                crud.And("SubApplication", MySQL.DAL.General.Operator.Equals, pSubApplicationName);
                tblapplicationsubapp search = crud.ReadSingle();
                if (!crud.RecordFound)
                    throw new Exception("Sub application by the name of " + pApplicationName + "." + pSubApplicationName + " was not found in the database. ");

                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetSubApplicationByNameMS(String, String)-" + Ex.Message);
            }
        }
        public tblsystem GetSystemByNameMS(String pSystemName)
        {
            try
            {
                recordFound = false;
                tblsystemCrud crud = new tblsystemCrud();
                crud.Where("vcsSystem", MySQL.DAL.General.Operator.Equals, pSystemName);

                tblsystem search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetSystemByNameMS - " + Ex.Message);
            }
        }
        public tblapplication GetApplication(String pApplicationName)
        {
            try
            {
                tblapplicationCrud crud = new tblapplicationCrud();
                crud.Where("ApplicationName", General.Operator.Equals, pApplicationName);
                recordFound = false;
                tblapplication app = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return app;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetApplication - " + Ex.Message);
            }
        }

        public tblapplicationsubapp GetSubApplication(Int32 pApplicationID, String pSubApplicationName)
        {
            try
            {
                tblapplicationsubappCrud crud = new tblapplicationsubappCrud();
                crud.Where("SubApplication", General.Operator.Equals, pSubApplicationName);
                recordFound = false;
                tblapplicationsubapp app = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return app;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetSubApplication - " + Ex.Message);
            }
        }

        public Int64 InsertLog(String pLogMessage, eLogType pLogType)
        {
            try

            {
                
                tbllogCrud crud = new tbllogCrud();
                tbllog log = new tbllog();
                log.ApplicationID = applicationID;
                log.SubApplicationID = subAplpicationID;
                log.LogDateTime = DateTime.Now;
                log.LogMessage = pLogMessage;
                log.LogTypeID = Convert.ToByte(pLogType);
                long id;
                crud.Insert(log, out id);

                return id;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertLog - " + Ex.Message);
            }
        }

        public Int64 InsertLog(Int32 pApplicationID, String pLogMessage, eLogType pLogType)
        {
            try

            {

                tbllogCrud crud = new tbllogCrud();
                tbllog log = new tbllog();
                log.ApplicationID = pApplicationID;
                log.LogDateTime = DateTime.Now;
                log.LogMessage = pLogMessage;
                log.LogTypeID = Convert.ToByte(pLogType);
                long id;
                crud.Insert(log, out id);

                return id;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertLog - " + Ex.Message);
            }
        }

        public Int64 InsertLog(Int32 pApplicationID, Int32 pSubApplicationID, String pLogMessage, eLogType pLogType)
        {
            try

            {

                tbllogCrud crud = new tbllogCrud();
                tbllog log = new tbllog();
                log.ApplicationID = pApplicationID;
                log.LogDateTime = DateTime.Now;
                log.LogMessage = pLogMessage;
                log.LogTypeID = Convert.ToByte(pLogType);
                log.SubApplicationID = pSubApplicationID;
                long id;
                crud.Insert(log, out id);

                return id;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertLog - " + Ex.Message);
            }
        }
        public tblsystem GetSystemByName(String pSystemName)
        {
            try
            {
                tblsystemCrud crud = new tblsystemCrud();
                crud.Where("SystemName", General.Operator.Equals, pSystemName);
                recordFound = false;
                tblsystem sys = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return sys;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetSystemByName - " + Ex.Message);
            }
        }
        public tblsetting GetSetting(String pSystem, String pApplication, String pSettingName)
        {
            try
            {
                tblsystem system = GetSystemByName(pSystem);
                tblapplication application = GetApplication(pApplication);
                tblsettingCrud crud = new tblsettingCrud();
                crud.Where("ApplicationID", DAL.General.Operator.Equals, (Int32)application.ApplicationID);
                crud.And("SettingName", DAL.General.Operator.Equals, pSettingName);
                recordFound = false;
                tblsetting setting = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return setting;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetSetting (string, string, string) -" + Ex.Message);
            }
        }

        public Int32 SaveSetting(Int32 pSettingID, Int32 pApplicationID, String pSettingName, String pSettingValue, Boolean pSensitiveInformation)
        {
            try
            {
                tblsettingCrud crud = new tblsettingCrud();
                tblsetting workRec = new tblsetting();
                if (pSettingID != 0)
                {
                    crud.Where("SettingID", DAL.General.Operator.Equals, pSettingID);
                    workRec = crud.ReadSingle();
                    workRec.ApplicationID = pApplicationID;
                    workRec.SettingName = pSettingName;
                    workRec.SettingValue = pSettingValue;
                    workRec.SensitiveInformation = pSensitiveInformation;
                    crud.Update(workRec);
                    return pSettingID;
                }
                else
                {
                    workRec.ApplicationID = pApplicationID;
                    workRec.SettingName = pSettingName;
                    workRec.SettingValue = pSettingValue;
                    workRec.SensitiveInformation = pSensitiveInformation;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("SaveSetting - " + Ex.Message);
            }
        }
        public tblsetting GetSettingByName(String pSettingName, Int32 pApplicationID)
        {
            try
            {
                tblsettingCrud crud = new tblsettingCrud();
                crud.Where("SettingName", General.Operator.Equals, pSettingName);
                crud.And("ApplicationID", General.Operator.Equals, pApplicationID);
                recordFound = false;
                tblsetting set = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return set;
            }
            catch(Exception Ex)
            {
                throw new Exception("GetSettingByName - " + Ex.Message);
            }
        }

        
    }
}
