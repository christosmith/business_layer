﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.MySQL.DAL;
using System.Net;
using System.Xml;
namespace DPO.BackOffice.MySQL.BL
{
    public partial class MySQLBackOfficeBL
    {
        public String GetDataForDiv(DatabaseEntities pEntity)
        {
            try
            {
                String divString = "";
                switch (pEntity)
                {
                    case DatabaseEntities.tblUssdProcessInstruction:
                        List<tblussdprocessinstruction> instructions = GetUssdProcessInstructionMS();
                        divString = "Process Instruction═ProcessInstructionID═Process Instruction╝";
                        foreach (tblussdprocessinstruction ins in instructions)
                        {
                            divString += ins.ProcessInstructionID + "═";
                            divString += ins.ProcessInstruction + "║";
                        }
                        break;
                    case DatabaseEntities.tblUssdVendingMessage:
                        List<tblussdvendingmessage> messages = GetUssdVendingMessageMS();
                        divString = "Vending Message═VendingMessageID═VendingMessageTypeID═Vending Message╝";
                        foreach (tblussdvendingmessage mess in messages)
                        {
                            divString += mess.VendingMessageID + "═";
                            divString += mess.VendingMessageTypeID + "═";
                            divString += mess.VendingMessage + "║";
                        }
                        break;
                    case DatabaseEntities.tblUssdVendingMessageType:
                        List<tblussdvendingmessagetype> types = GetUssdVendingMessageTypeMS();
                        divString = "Vending Message Type═VendingMessageTypeID═Vending Message Type╝";
                        foreach (tblussdvendingmessagetype type in types)
                        {
                            divString += type.VendingMessageTypeID + "═";
                            divString += type.VendingMessageType + "║";
                        }
                        break;
                    /*case DatabaseEntities.tblUssdTransactionProcessInstruction:
                        divString = "Transaction Process Instruction═TranProcessID═UssdTransactionID═Transaction═ProcessInstructionID═Process Instruction╝";
                        List<tblussdtransactionprocessinstruction> tranIns = GetUssdTransactionProcessInstructionMS();
                        foreach (tblussdtransactionprocessinstruction ins in tranIns)
                        {
                            divString += ins.TranProcessID + "═";
                            divString += ins.UssdTransactionID + "═";
                            divString += ins.tblussdtransactiongroup.t .tblussdtransaction.UssdTransaction + "═";
                            divString += ins.ProcessInstructionID + "═";
                            divString += ins.tblussdprocessinstruction.ProcessInstruction + "║";
                        }
                        break;*/
                    case DatabaseEntities.tblUssdTransactionVendingMessage:
                        List<tblussdtransactionvendingmessage> messes = GetUssdTransactionVendingMessageMS();
                        divString = "Transaction Vending Message═UssdTranMessageID═UssdTransactionID═Transaction═VendingMessageID═VendingMessage╝";
                        foreach (tblussdtransactionvendingmessage mess in messes)
                        {
                            divString += mess.UssdTranMessageID + "═";
                            divString += mess.UssdTransactionID + "═";
                            divString += mess.tblussdtransaction.UssdTransaction + "═";
                            divString += mess.tblussdvendingmessage.VendingMessageID + "═";
                            divString += mess.tblussdvendingmessage.VendingMessage + "║";
                        }
                        break;
                    case DatabaseEntities.tblbusiness:
                        List<tblbusiness> businesses = GetBusiness().OrderBy(x => x.Business).ToList();
                        divString = "Business═BusinessID═Business═TerminalID╝";
                        foreach (tblbusiness bus in businesses)
                        {
                            divString += bus.BusinessID + "═";
                            divString += bus.Business + "═";
                            divString += bus.TerminalID + "║";
                        }
                        break;
                    case DatabaseEntities.tblussdmenuitem:
                        List<tblussdmenuitem> menuItems = GetUssdMenuItemMS().OrderBy(x => x.MenuItem).ToList();
                        divString = "Ussd Menu Item═MenuItemID═MenuItem╝";
                        foreach (tblussdmenuitem item in menuItems)
                        {
                            divString += item.MenuItemID + "═";
                            divString += item.MenuItem + "║";
                        }
                        break;
                    case DatabaseEntities.tblussdmenuitemtext:
                        List<tblussdmenuitemtext> itemText = GetUssdMenuItemTextMS().OrderBy(x => x.MenuItemText).ToList();
                        divString = "Ussd Menu Item Text═MenuItemTextID═MenuItemText╝";
                        foreach (tblussdmenuitemtext item in itemText)
                        {
                            if (item.MenuItemText.ToLower().Contains("bots"))
                                Console.WriteLine();

                            divString += item.MenuItemTextID + "═";
                            divString += item.MenuItemText + "║";
                        }
                        break;
                    case DatabaseEntities.tblussdconfirmtext:
                        List<tblussdconfirmtext> confirmText = GetUssdConfirmTextMS().OrderBy(x => x.ConfirmText).ToList();
                        divString = "Ussd Confirm Text═ConfirmTextID═ConfirmText╝";
                        foreach (tblussdconfirmtext item in confirmText)
                        {
                            divString += item.ConfirmTextID + "═";
                            divString += item.ConfirmText + "║";
                        }
                        break;
                    case DatabaseEntities.tblussdleadintext:
                        List<tblussdleadintext> leadInText = GetUssdLeadInTextMS().OrderBy(x => x.LeadInText).ToList();
                        divString = "Ussd Lead In Text═LeadInTextID═LeadInText╝";
                        foreach (tblussdleadintext item in leadInText)
                        {
                            divString += item.LeadInTextID + "═";
                            divString += item.LeadInText + "║";
                        }
                        break;
                    case DatabaseEntities.tblussdtagname:
                        List<tblussdtagname> tagNames = GetUssdTagNameMS().OrderBy(x => x.TagName).ToList();
                        divString = "Ussd Tag Name═TagNameID═TagName╝";
                        foreach (tblussdtagname item in tagNames)
                        {
                            divString += item.TagNameID + "═";
                            divString += item.TagName + "║";
                        }
                        break;
                    case DatabaseEntities.tblussdwelcomemessage:
                        List<tblussdmenuwelcomemessage> welmessages = GetUssdWelcomeMessageMS().OrderBy(x => x.WelcomeMessage).ToList();
                        divString = "Ussd Welcome Message═WelcomeMessageID═WelcomeMessage╝";
                        foreach (tblussdmenuwelcomemessage item in welmessages)
                        {
                            divString += item.WelcomeMessageID + "═";
                            divString += item.WelcomeMessage + "║";
                        }
                        break;
                    case DatabaseEntities.tblussdhttppost:
                        List<tblussdhttppost> posts = GetUssdHttpPostMS();

                        divString = "Ussd Http Post═HttpPostID═Url╝";

                        foreach (tblussdhttppost item in posts)
                        {
                            divString += item.HttpPostID + "═";
                            divString += item.Url + "║";
                        }
                        break;
                    case DatabaseEntities.tblussdtransaction:
                        List<tblussdtransaction> trans = GetUssdTransactionMS().OrderBy(x => x.UssdTransaction).ToList();
                        divString = "Ussd Transaction═UssdTransactionID═UssdTransaction═Transaction Value═Transaction Amount═Formul aAmount═Product Description═Terminal ID╝";
                        foreach (tblussdtransaction item in trans)
                        {
                            divString += item.UssdTransactionID + "═";
                            divString += item.UssdTransaction + "═";
                            divString += item.TransactionValue + "═";
                            divString += item.TransactionAmount + "═";
                            divString += item.FormulaAmount + "═";
                            divString += item.ProductDescription + "═";
                            divString += item.VCSTerminalID + "║";
                        }
                        break;

                }

                return divString;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetDataForDiv - " + Ex.Message);
            }
        }

        public String GetDataForDiv(DatabaseEntities pEntity, Boolean pDescriptionOnly)
        {
            try
            {
                String divString = "";
                switch (pEntity)
                {
                    case DatabaseEntities.tblussdtransaction:
                        List<tblussdtransaction> trans = GetUssdTransactionMS().OrderBy(x => x.UssdTransaction).ToList();
                        divString = "Ussd Transaction═UssdTransactionID═UssdTransaction╝";
                        foreach (tblussdtransaction item in trans)
                        {
                            divString += item.UssdTransactionID + "═";
                            divString += item.UssdTransaction + "║";
                        }
                        break;

                }

                return divString;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetDataForDiv - " + Ex.Message);
            }
        }
        public Int64 InsertLookupEntity(String pServerData)
        {
            try
            {
                String[] data = pServerData.Split('║');
                String dbEntity = data[0];
                String dbValue = data[1];
                Int64 entityID = 0;
                switch (dbEntity)
                {
                    case "Business":
                        String terminalID = data[2];
                        entityID = InsertBusiness(dbValue, terminalID);
                        break;
                    case "Ussd Menu Item":
                        entityID = InsertUssdMenuItemMS(dbValue);
                        break;
                    case "Ussd Menu Item Text":
                        entityID = InsertUssdMenuItemTextMS(dbValue);
                        break;
                    case "Ussd Confirm Text":
                        entityID = InsertUssdConfirmTextMS(dbValue);
                        break;
                    case "Ussd Lead In Text":
                        entityID = InsertUssdLeadInTextMS(dbValue);
                        break;
                    case "Ussd Tag Name":
                        entityID = InsertUssdTagNameMS(dbValue);
                        break;
                    case "Ussd Welcome Message":
                        entityID = InsertUssdWelcomeMessageMS(dbValue);
                        break;
                    case "Ussd Http Post":
                        entityID = InsertUssdHttpPostMS(dbValue);
                        break;
                    case "Ussd Transaction":
                        String transactionValue = data[2];
                        Decimal transactionAmount = Convert.ToDecimal(data[3]);
                        String amountFormula = data[4];
                        String productDescription = data[5];
                        terminalID = data[6];
                        entityID = InsertUssdTransactionMS(dbValue, transactionAmount, transactionValue, amountFormula, productDescription, terminalID);
                        break;
                }

                return entityID;
            }
            catch (Exception Ex)
            {
                throw new Exception("SaveLookupEntity - " + Ex.Message);
            }
        }

        public Boolean UpdateLookupEntity(String pServerData)
        {
            try
            {
                String[] data = pServerData.Split('║');
                String dbEntity = data[0];
                String dbEntityID = data[1];
                String dbValue = data[2];

                Int64 entityID = 0;
                switch (dbEntity)
                {
                    case "Business":
                        String terminalID = data[3];
                        UpdateBusiness(Convert.ToInt16(dbEntityID), dbValue, terminalID);
                        break;
                    case "Ussd Menu Item":
                        UpdateUssdMenuItem(Convert.ToInt16(dbEntityID), dbValue);
                        break;
                    case "Ussd Menu Item Text":
                        UpdateUssdMenuItemText(Convert.ToInt16(dbEntityID), dbValue);
                        break;
                    case "Ussd Confirm Text":
                        UpdateUssdConfirmText(Convert.ToInt16(dbEntityID), dbValue);
                        break;
                    case "Ussd Leadin Text":
                        UpdateUssdLeadInText(Convert.ToInt16(dbEntity), dbValue);
                        break;
                    case "Ussd Tag Name":
                        UpdateUssdTagName(Convert.ToInt16(dbEntityID), dbValue);
                        break;
                    case "Ussd Welcome Message":
                        UpdateUssdWelcomeMessage(Convert.ToInt16(dbEntity), dbValue);
                        break;
                    case "Ussd Http Post":
                        UpdateUssdHttpPost(Convert.ToByte(dbEntity), dbValue);
                        break;
                    case "Ussd Transaction":
                        String transactionValue = data[3];
                        Decimal transactionAmount = Convert.ToDecimal(data[4]);
                        String amountFormula = data[5];
                        UpdateUssdTransaction(Convert.ToInt16(dbEntityID), dbValue, transactionAmount, transactionValue, amountFormula);
                        break;
                }

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateLookupEntity - " + Ex.Message);
            }
        }

        public String Authorisation(String pUserID, String pReferenceNumber, String pDescription, String pAmount, String pCardNumber, String pCardholderName, String pExpiryDate, String pCvc)
        {
            try
            {
                XmlDocument xmlAuthorisationDoc = new XmlDocument();
                XmlElement xmlDocElement;

                //Root Element
                XmlElement xmlAuthorisationDocRootElement = xmlAuthorisationDoc.CreateElement("AuthorisationRequest");
                xmlAuthorisationDoc.AppendChild(xmlAuthorisationDocRootElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("UserId");
                xmlDocElement.InnerText = pUserID;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("Reference");
                xmlDocElement.InnerText = pReferenceNumber;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("Description");
                xmlDocElement.InnerText = pDescription;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("Amount");
                xmlDocElement.InnerText = pAmount;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("CardNumber");
                xmlDocElement.InnerText = pCardNumber;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("CardholderName");
                xmlDocElement.InnerText = pCardholderName;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("ExpiryMonth");
                xmlDocElement.InnerText = pExpiryDate.Substring(0, 2);
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("ExpiryYear");
                xmlDocElement.InnerText = pExpiryDate.Substring(2, 2);
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("CardValidationCode");
                xmlDocElement.InnerText = pCvc;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                System.Net.HttpWebRequest VcsRequest = (HttpWebRequest)HttpWebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetAuthorisationUrl"]);


                VcsRequest.Method = "POST";
                ((HttpWebRequest)VcsRequest).UserAgent = "VcsAuthCode";


                String postData = "xmlmessage=" + System.Web.HttpUtility.UrlEncode(xmlAuthorisationDoc.OuterXml.ToString());
                ASCIIEncoding encoding = new ASCIIEncoding();
                Byte[] byteArray = encoding.GetBytes(postData);

                //byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                VcsRequest.ContentType = "application/x-www-form-urlencoded";
                VcsRequest.ContentLength = byteArray.Length;

                System.IO.Stream dataStream = VcsRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                HttpWebResponse VcsResponse = (HttpWebResponse)VcsRequest.GetResponse();
                System.IO.Stream responseStream = VcsResponse.GetResponseStream();
                System.IO.StreamReader streamIn = new System.IO.StreamReader(responseStream, System.Text.Encoding.UTF8);
                String strCcxmlauthResponse = streamIn.ReadToEnd();
                streamIn.Close();
                return strCcxmlauthResponse;

            }
            catch (Exception Ex)
            {
                throw new Exception("Authorisation - " + Ex.Message);
            }
        }
        public Boolean DeleteLookupEntity(String pServerData)
        {
            try
            {
                String[] data = pServerData.Split('║');
                String dbEntity = data[0];
                String dbEntityID = data[1];

                switch (dbEntity)
                {
                    case "Business":
                        DeleteBusiness(Convert.ToInt16(dbEntityID));
                        break;
                    case "Ussd Menu Item":
                        DeleteUssdMenuItem(Convert.ToInt16(dbEntityID));
                        break;
                    case "Ussd Menu Item Text":
                        DeleteUssdMenuItemText(Convert.ToInt16(dbEntityID));
                        break;
                    case "Ussd Confirm Text":
                        DeleteUssdConfirmText(Convert.ToInt16(dbEntityID));
                        break;
                    case "Ussd Leadin Text":
                        DeleteUssdLeadInText(Convert.ToInt16(dbEntity));
                        break;
                    case "Ussd Tag Name":
                        DeleteUssdTagName(Convert.ToInt16(dbEntityID));
                        break;
                    case "Ussd Welcome Message":
                        DeleteUssdWelcomeMessage(Convert.ToInt16(dbEntity));
                        break;
                    case "Ussd Http Post":
                        DeleteUssdHttpPost(Convert.ToByte(dbEntity));
                        break;
                    case "Ussd Transaction":
                        DeleteUssdTransaction(Convert.ToInt16(dbEntityID));
                        break;
                }

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteLookupEntity - " + Ex.Message);
            }
        }
        }
    }

