﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.MySQL.DAL;
using System.Xml;
using System.Runtime.Caching;

namespace DPO.BackOffice.MySQL.BL
{
    public partial class MySQLBackOfficeBL
    {
        Int32 applicationID = 0;
        Int32 subAplpicationID = 0;
        Boolean recordFound = false;
        Boolean excludeRefTable = false;
        DPO.Messaging.MySQL.BL.MessagingBL messBL = new Messaging.MySQL.BL.MessagingBL();
        public const String GenericMessage = "An unexpected error has occured. The system administrator was notified.";
        ObjectCache cache = MemoryCache.Default;

        UssdInformation ussdInformation = new UssdInformation();

        public Boolean ExcludeReferenceTable
        {
            get
            {
                return excludeRefTable;
            }
            set
            {
                excludeRefTable = value;


            }
        }

        public UssdInformation SetUssdInformation
        {
            set
            {
                ussdInformation = value;
            }
            get

            {
                return ussdInformation;
            }

        }

        public enum eLogType
        {
            Error = 1,
            Information = 2,
            Warning = 3,
            Debug = 4
        }

        public Boolean ExcludeRefTable
        {
            set
            {
                excludeRefTable = value;
            }
        }
        public Boolean RecordFound
        {
            get
            {
                return recordFound;
            }
        }

        public System.Data.DataTable ExecuteMSQuery(String pQuery)
        {
            MySql.Data.MySqlClient.MySqlConnection openConn = new MySql.Data.MySqlClient.MySqlConnection();
            try
            {
                openConn = new MySql.Data.MySqlClient.MySqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["MySQL_backOffice_ConnectionString"].ConnectionString);
                openConn.Open();
                MySql.Data.MySqlClient.MySqlCommand dbComd = new MySql.Data.MySqlClient.MySqlCommand(pQuery, openConn);
                MySql.Data.MySqlClient.MySqlDataAdapter dataAdapter = new MySql.Data.MySqlClient.MySqlDataAdapter(dbComd);
                System.Data.DataTable table = new System.Data.DataTable();
                dataAdapter.Fill(table);

                return table;

            }
            catch (Exception Ex)
            {
                throw new Exception("ExecuteQuery - " + Ex.Message);
            }
        }
        public MySQLBackOfficeBL(String pApplication, String pSubApplication)
        {
            tblapplication app = GetApplication(pApplication);
            if (!recordFound)
                throw new Exception("Application with the name of " + pApplication + " was not found.");

            tblapplicationsubapp subApp = GetSubApplication((Int32)app.ApplicationID, pSubApplication);
            if (!recordFound)
                throw new Exception("Sub application with the name of " + pSubApplication + " was not found.");

            applicationID = (Int32)app.ApplicationID;
            subAplpicationID = (Int32)subApp.SubApplicationID;
        }

        public void AddInteractionToCache(DateTime InteractionDateTime, Int32 transactionID, Int16? GroupID, String SelectionNo, Int32? InteractionID, String ValueSubmitted, Int32? UssdActionID, Int32? InstructionNo, Boolean? AskConfirmation, String SessionId, Int16? InstructionID)
        {
            UssdInteraction ussdInteraction = new UssdInteraction();
            ussdInteraction.InteractionDateTime = InteractionDateTime;
            ussdInteraction.AskConfirmation = AskConfirmation;
            ussdInteraction.GroupID = GroupID;
            ussdInteraction.InstructionNo = InstructionNo;
            ussdInteraction.InteractionID = InteractionID;
            ussdInteraction.SelectionNo = SelectionNo;
            ussdInteraction.SessionID = SessionId;
            ussdInteraction.TransactionID = transactionID;
            ussdInteraction.UssdActionID = UssdActionID;
            ussdInteraction.ValueSubmitted = ValueSubmitted;
            ussdInteraction.InstructionID = InstructionID;



            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddHours(1.0);
            //System.Runtime.Caching.MemoryCache.Default.Add(SessionId, ussdInteraction, cacheItemPolicy);
            cache.Add(SessionId, ussdInteraction, cacheItemPolicy);

        }


        public List<UssdInteraction> ReadInteractionFromCache(String pSessionID)
        {
            //return null;
            List<UssdInteraction> interactions = new List<UssdInteraction>();
            ObjectCache InteractionCache = System.Runtime.Caching.MemoryCache.Default;
            List<string> cacheKeys = InteractionCache.Select(kvp => kvp.Key).ToList();
            UssdInteraction thisInteraction = new UssdInteraction();
            foreach (string cacheKey in cacheKeys)
            {
                if (cacheKey.Contains(pSessionID))
                {
                    thisInteraction = (UssdInteraction)InteractionCache.Get(cacheKey);
                    interactions.Add(thisInteraction);
                }


            }
            //List<String> cacheKeys = InteractionCache.Select( ((x=>x.key  .Select(x=>x.key   kvp => kvp.Key).ToList()

            //List<UssdInteraction> interactions = (List<UssdInteraction>)(System.Collections.IEnumerable)InteractionCache.Get(pSessionID);
            return interactions;
        }
        public MySQLBackOfficeBL()
        {
            
        }
    }

    public class UssdInteraction
    {
        public DateTime InteractionDateTime;
        public Int32 TransactionID;
        public Int16? GroupID;
        public String SelectionNo;
        public Int32? InteractionID;
        public String ValueSubmitted;
        public Int32? UssdActionID;
        public Int32? InstructionNo;
        public Boolean? AskConfirmation;
        public String SessionID;
        public Int16? InstructionID;
    }
    public enum MultiChoiceDetailType
    {
        CellPhone = 1,
        EmailAddress = 2,
        City = 3,
    }

    public enum UssdServiceProvider
    {
        Orange = 0,
        BeMobile = 1,
        Mascom = 2
    }
    public class UssdIncomingInformation
    {
        public String Msisdn;
        public String SessionID;
        public String UssdType;
        public String UssdMessage;
        public DPOResponse Response = new DPOResponse();
    }
    public enum DatabaseEntities
    {
        tblussdmenuitem = 0,
        tblussdmenuitemtext = 1,
        tblussdconfirmtext = 2,
        tblussdleadintext = 3,
        tblussdtagname = 4,
        tblussdwelcomemessage = 5,
        tblussdhttppost = 6,
        tblussdtransaction = 7,
        tblbusiness = 8,
        tblUssdTransactionProcessInstruction = 9,
        tblUssdProcessInstruction = 10,
        tblUssdVendingMessage = 11,
        tblUssdVendingMessageType = 12,
        tblUssdTransactionVendingMessage = 13
    }
    public enum eUssdMenuChangeOptions
    {
        CurrentNode = 0,
        AllNodes = 1,
        AllNodesForThirdParty = 2
    }
    public class UssdMenuInformationChange
    {
        public eUssdMenuInformation UssdMenuInformationType = new eUssdMenuInformation();
        public Boolean ChangeInformation = false;
        public String NewValue = "";

    }
    public class DPOResponse
    {
        public String ResponseCode;
        public String ResponseMessage;
    }

    public enum eUssdMenuInformation
    {
        ConfirmText = 0,
        LeadInText = 1,
        MenuItem = 2,
        MenuItemNumber = 3,
        MenuItemText = 4,
        PostUrl = 5,
        TagName = 6,
        Transaction = 7,
        WelcomeMessage = 8,
        Confirm = 9,
        Prompt = 10,
        Business = 11,
        Gateway = 12,
        MenuLevelNumber = 13,
        ParentUssdID = 14,
        RegistryKey = 15

    }

    public class UssdInformation
    {
        public Int32 UssdMsisdnID;
        public String Msisdn;
        public Int16? UssdTransactionID;
        public Int16 UssdTransactionGroupID;
        public String UssdTransaction;
        public String CallerPage;
        public Int32 VendingID;
        public Int32 SmsID;
        public Byte ValidationErrorID;
        public XmlDocument xmlIncomingDoc = new XmlDocument();
        public String ResponseCode;
        public String ResponseMessage;
        public String MCASmartCard;
        public String BankCardNumber;
        public String MCACustomerNo;
        public String ExpiryDate;
        public String Cvc;
        public String NumberOfMonths;
        public Decimal Amount;
        public String MeterNumber;
        public Int32 ApplicationID;
        public Int32 SubApplicationID;
        public Int32 UssdMsisdnTransactionID;
        public String ReferenceNumber;
        public String VendingMessage;
        public String BankResponse;
        public Boolean AmountFromXML = false;
        public String BPCReceiptNumber;
        public String BPCToken;
        public String BPCUnitsBought;
        public String MCBouquet;
        public String MCAddon;
        public String InvoiceNumber;
        public String PostBoxIDNumber;
        public String PostBoxNumber;
        public String PostBoxLocation;
        public String PostBoxHolderName;
        public String PlotNumber;
        public String ApplicationNumber;
        public String AccountNumber;
        public String SeatNumber;
        public String Surname;
        public String Initials;
        public String PassengerCell;
        public String EmailAddress;
        public DateTime DepartureDate;
        public String IDNumber;
        public String NextOfKinContact;
        public String LogInfo;
    }

    public class MulitChoiceParameter
    {
        public String VendorCode = "";
        public String CustomerVendorID = "";
        public String DataSource = "";
        public String BotswanaCurrency = "";
        public String BusinessUnit = "";
        public String PaymentVendorCode = "";
    }
    internal class ConcerotelData
    {
        public String mobile { get; set; }
        public Int32 timestamp { get; set; }
        public String result { get; set; }
        public String amount { get; set; }
        public String id { get; set; }
        public String hash { get; set; }
    }
    public class CarmaInformation
    {
        public String FromPoint = "";
        public String ToPoint = "";
        public String TicketType = "";
        public String CoachClass = "";
        public String Title = "";
        public String DepartDate = "";
        public String ReturnDate = "";
        public String PassengerMobileNo = "";
        public String PassengerTitle = "";
        public String PassengerSurname = "";
        public String PassengerInitials = "";
        public String PassengerEmail = "";
        public DateTime DepartArrivalDateTime = DateTime.Now;
        public DateTime DepartBoardingDateTime = DateTime.Now;
        public String DepartServiceNr = "";
        public Decimal DepartPrice = 0;
        public DateTime ReturnArrivalDateTime = DateTime.Now;
        public DateTime ReturnBoardingDateTime = DateTime.Now;
        public String ReturnServiceNr = "";
        public Decimal ReturnPrice = 0;
        public String BookingReferenceNo = "";
        public String DepartBarCode = "";
        public String ReturnBarCode = "";
        public String DepartTicketNo = "";
        public String ReturnTicketNo = "";
        public String VCSResponseCode = "0000";
        public String VCSResponseMessage = "Success";
        public String DepartCoachNumber = "0";
        public String DepartCompartmentNumber = "";
        public String ReturnCoachNumber = "0";
        public String ReturnCompartmentNumber = "";
        public String DepartSeatNo = "0";
        public String ReturnSeatNo = "0";
        public String PassengerID = "";
        public String NextOfKinContact = "";
        public Boolean SendTicketByMail = false;
    }
    /*

    internal void UpdateProcessTrace(ProcessUpdate pProcessUpdate)
    {
        try
        {
            tblussdprocesstrace processTrace = GetUssdProcessTraceMS(ussdInformation.UssdMsisdnID, (Int16)ussdInformation.UssdTransactionGroupID, DateTime.Now.AddMinutes(-2), DateTime.Now);
            if (!recordFound)
            {
                Byte gatewayID = 0;
                switch (ussdInformation.CallerPage)
                {
                    case "Mascom": gatewayID = 3; break;
                    case "beMobile": gatewayID = 3; break;
                }


                InsertUssdProcessTraceMS(ussdInformation.UssdMsisdnID, gatewayID, ussdInformation.UssdTransactionGroupID, DateTime.Now, false);
                processTrace = GetUssdProcessTraceMS(ussdInformation.UssdMsisdnID, (Int16)ussdInformation.UssdTransactionGroupID, DateTime.Now.AddMinutes(-2), DateTime.Now);
            }

            switch (pProcessUpdate)
            {
                case BackOfficeBL.ProcessUpdate.WebPageCalled:
                    processTrace.WebPageCalled = true; break;
                case BackOfficeBL.ProcessUpdate.Authorised:
                    processTrace.Authorised = true; break;
                case BackOfficeBL.ProcessUpdate.ThirdPartyCalled:
                    processTrace.ThirdPartyCalled = true; break;
                case BackOfficeBL.ProcessUpdate.VendingID:
                    processTrace.VendingID = ussdInformation.VendingID;
                    break;
                case BackOfficeBL.ProcessUpdate.TextMessage:
                    processTrace.SmsID = ussdInformation.SmsID;
                    processTrace.SmsSend = true;
                    break;
                case BackOfficeBL.ProcessUpdate.ValidationError:
                    processTrace.ValidationErrorID = ussdInformation.ValidationErrorID; break;
            }

            tblussdprocesstraceCrud crud = new tblussdprocesstraceCrud();
            crud.Update(processTrace);
        }
        catch (Exception Ex)
        {
            throw new Exception("UpdateProcessTrace - " + Ex.Message);
        }
    }

    internal void UssdValidationErrorToTrace(String pValidationError)
    {
        try
        {
            tblussdvalidationerror error = GetUssdValidationErrorMS(pValidationError);
            if (!recordFound)
            {
                ussdInformation.ValidationErrorID = InsertUssdValidationErrorMS(pValidationError);
            }
            else
            {
                ussdInformation.ValidationErrorID = Convert.ToByte(error.ValidationErrorID);
            }
            UpdateProcessTrace(ProcessUpdate.ValidationError);

        }
        catch (Exception Ex)
        {
            throw new Exception("GetUssdValidationError - " + Ex.Message);
        }
    }





    public String Replace(String pMessage)
    {
        String mess = pMessage;
        mess = mess.Replace("{?amount}", ussdInformation.Amount.ToString());
        mess = mess.Replace("{?MeterNumber}", ussdInformation.MeterNumber);
        mess = mess.Replace("{?BPCToken}", ussdInformation.BPCToken);
        mess = mess.Replace("{?BPCUnits}", ussdInformation.BPCUnitsBought);
        mess = mess.Replace("{?BPCReceipt}", ussdInformation.BPCReceiptNumber);

        return mess;
    }
    */

 


}
