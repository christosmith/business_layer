﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.MySQL.DAL;


namespace DPO.BackOffice.MySQL.BL
{
    public partial class MySQLBackOfficeBL
    {
        public tblussdvalidationmessage ValidateBPCMeterNo(String pMeterNumber, Decimal pAmount, Int32 pUssdMsisdnID)
        {
            String logInfo = "";
            try
            {
                recordFound = false;
                tblussdvalidationmessage valMess = new tblussdvalidationmessage();
                Int64 meterNo = 0;
                if (!Int64.TryParse(pMeterNumber, out meterNo))
                {
                    logInfo += "MeterNoNumeric";
                    valMess = GetUssdValidationMessage("Meter number must be numeric digits only");
                    return valMess;
                }

                if (pMeterNumber.Length != 11)
                {
                    logInfo += "|MeterNoLength";
                    valMess = GetUssdValidationMessage("Meter number must be eleven digits in length.");
                    return valMess;
                }

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                Int32 applicationID = Convert.ToInt32(GetApplicationByNameMS("DPO.BackOffice.Ussd").ApplicationID);
                BotswanaPowerToken.WebService bpcClient = new BotswanaPowerToken.WebService();
                //bpcClient.t .Timeout = 60000;
                BotswanaPowerToken.AuthCred credentials = new BotswanaPowerToken.AuthCred();
                credentials.opName = GetSettingByName("PwBpoOperatorName", applicationID).SettingValue;
                credentials.password = GetSettingByName("PwBpoOperatorPassword", applicationID).SettingValue;
                BotswanaPowerToken.EANDeviceID clientID = new BotswanaPowerToken.EANDeviceID();
                clientID.ean = GetSettingByName("PwBpoClientId", applicationID).SettingValue;
                BotswanaPowerToken.EANDeviceID terminalID = new BotswanaPowerToken.EANDeviceID();
                terminalID.ean = GetSettingByName("PwBpoTerminalId", applicationID).SettingValue;
                BotswanaPowerToken.MeterNumber meterNumber = new BotswanaPowerToken.MeterNumber();
                meterNumber.msno = pMeterNumber;
                BotswanaPowerToken.Currency currency = new BotswanaPowerToken.Currency();
                currency.symbol = "P";
                currency.value = pAmount;
                BotswanaPowerToken.PurchaseValueCurrency purchaseValCur = new BotswanaPowerToken.PurchaseValueCurrency();
                purchaseValCur.amt = currency;
                BotswanaPowerToken.VendIDMethod vendingMethod = new BotswanaPowerToken.VendIDMethod();
                vendingMethod.meterIdentifier = meterNumber;
                BotswanaPowerToken.ConfirmCustomerReq confirmCustomer = new BotswanaPowerToken.ConfirmCustomerReq();
                confirmCustomer.authCred = credentials;
                confirmCustomer.clientID = clientID;
                confirmCustomer.terminalID = terminalID;
                confirmCustomer.msgID = CreateBPCMessageID();
                confirmCustomer.idMethod = vendingMethod;

                BotswanaPowerToken.ConfirmCustomerResp confirmCustomerResponse = new BotswanaPowerToken.ConfirmCustomerResp();
                try
                {
                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                    confirmCustomerResponse = bpcClient.ConfirmCustomerRequest(confirmCustomer);
                    BotswanaPowerToken.ConfirmCustResult[] confirmCustResult = confirmCustomerResponse.confirmCustResult;
                    BotswanaPowerToken.CustVendDetail vendDetail = confirmCustResult[0].custVendDetail;
                    BotswanaPowerToken.CustVendConfig vendConfig = confirmCustResult[0].custVendConfig;

                    Boolean canVend = vendConfig.canVend;
                    String vendingSymbol = vendConfig.minVendAmt.symbol;
                    Decimal minVendingAmount = Convert.ToDecimal(vendConfig.minVendAmt.value);
                    //Decimal maxVendingAmount = Convert.ToDecimal(vendConfig.maxVendAmt.value);

                    InsertUssdMsisdnBPCData(pUssdMsisdnID, pMeterNumber, minVendingAmount, 0, pAmount, canVend);

                    recordFound = false;
                    if (!vendConfig.canVend)
                    {
                        valMess = GetUssdValidationMessage("Vending from ths meter is not allowed");
                        return valMess;
                    }

                    if (pAmount < minVendingAmount)
                    {
                        valMess = GetUssdValidationMessage("Amount is less than minimum vending amount.");
                        return valMess;
                    }

                    //if (pAmount > maxVendingAmount)
                    //{
                    //    valMess = GetUssdValidationMessage("Amount is more than maximum vending amount.");
                    //    return valMess;
                    //}
                }
                catch (Exception Ex)
                {
                    if (Ex.Message.Contains("Meter Not Found"))
                    {
                        valMess = GetUssdValidationMessage("Meter number was not found.");
                        return valMess;
                    }
                    if (Ex.Message.ToLower().Contains("blocked"))
                    {
                        valMess = GetUssdValidationMessage("Meter number is blocked.");
                        return valMess;
                    }
                    valMess = GetUssdValidationMessage("Botswana power token issue exception.");
                    return valMess;
                }

                return valMess;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateBPCMeterNo - " + Ex.Message);
            }
        }


        public Boolean UpdateUssdMsisdnBPCData(Int32 pUssdMsisdnID, String pMeterNumber, Decimal pMinimumVendingAmount, Decimal pMaximumVendingAMount, Decimal pLastVendingAmount, Boolean pCanVend)
        {
            try
            {
                tblussdmsisdnbpcdataCrud crud = new tblussdmsisdnbpcdataCrud();
                tblussdmsisdnbpcdata bpcData = GetUssdMsisdnBPCData(pUssdMsisdnID);
                bpcData.CanVend = Convert.ToByte(pCanVend);
                bpcData.LastVendingAmount = pLastVendingAmount;
                bpcData.MaxVedingAmount = pMaximumVendingAMount;
                bpcData.MeterNumber = pMeterNumber;
                bpcData.MinVendingAmount = pMaximumVendingAMount;
                bpcData.UssdMsisdnID = pUssdMsisdnID;
                crud.Update(bpcData);

                return true;

            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdMsisdnBPCData - " + Ex.Message);
            }
        }

        public Int32 InsertUssdMsisdnBPCData(Int32 pUssdMsisdnID, String pMeterNumber, Decimal pMinimumVendingAmount, Decimal pMaximumVendingAMount, Decimal pLastVendingAmount, Boolean pCanVend)
        {
            try
            {
                tblussdmsisdnbpcdata bpcData = GetUssdMsisdnBPCData(pUssdMsisdnID);
                if (recordFound)
                {
                    UpdateUssdMsisdnBPCData(pUssdMsisdnID, pMeterNumber, pMinimumVendingAmount, pMaximumVendingAMount, pLastVendingAmount, pCanVend);
                    return 0;
                }

                tblussdmsisdnbpcdataCrud crud = new tblussdmsisdnbpcdataCrud();
                bpcData = new tblussdmsisdnbpcdata();
                bpcData.CanVend = Convert.ToByte(pCanVend);
                bpcData.LastVendingAmount = pLastVendingAmount;
                bpcData.MaxVedingAmount = pMaximumVendingAMount;
                bpcData.MeterNumber = pMeterNumber;
                bpcData.MinVendingAmount = pMaximumVendingAMount;
                bpcData.UssdMsisdnID = pUssdMsisdnID;
                long ID = 0;
                crud.Insert(bpcData, out ID);

                return (Int32)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdMsisdnBPCData - " + Ex.Message);
            }
        }

        internal BotswanaPowerToken.MsgID CreateBPCMessageID()
        {
            try
            {
                Int32 applicationID = Convert.ToInt32(GetApplication("DPO.BackOffice.Ussd").ApplicationID);
                BotswanaPowerToken.MsgID messageID = new BotswanaPowerToken.MsgID();
                messageID.dateTime = DateTime.Now.ToString("yyyyMMddhhmmss");
                Int32 uniqueNo = Convert.ToInt32(GetSettingByName("BcpPrepaidPwUniqueNumber", applicationID).SettingValue);
                uniqueNo++;
                messageID.uniqueNumber = uniqueNo.ToString();
                Int32 settingID = Convert.ToInt32(GetSettingByName("BcpPrepaidPwUniqueNumber", applicationID).SettingID);
                SaveSetting(settingID, applicationID, "BcpPrepaidPwUniqueNumber", uniqueNo.ToString(), false);
                return messageID;
            }
            catch (Exception Ex)
            {
                throw new Exception("BPCWebService - " + Ex.Message);
            }
        }
        public String DoBotswanaPowerPayment(String pMeterNumber, Decimal pAmount, Int32 pUssdMsisdnID)
        {
            Int32 applicationID = 0;
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                applicationID = Convert.ToInt32(GetApplication("DPO.BackOffice.Ussd").ApplicationID);
                BotswanaPowerToken.CreditVendReq creditVendRequest = new BotswanaPowerToken.CreditVendReq();
                BotswanaPowerToken.AuthCred credentials = new BotswanaPowerToken.AuthCred();
                credentials.opName = GetSettingByName("PwBpoOperatorName", applicationID).SettingValue;
                credentials.password = GetSettingByName("PwBpoOperatorPassword", applicationID).SettingValue;
                BotswanaPowerToken.EANDeviceID clientID = new BotswanaPowerToken.EANDeviceID();
                clientID.ean = GetSettingByName("PwBpoClientId", applicationID).SettingValue;
                BotswanaPowerToken.EANDeviceID terminalID = new BotswanaPowerToken.EANDeviceID();
                terminalID.ean = GetSettingByName("PwBpoTerminalId", applicationID).SettingValue;
                creditVendRequest.authCred = credentials;
                creditVendRequest.clientID = clientID;
                creditVendRequest.terminalID = terminalID;
                creditVendRequest.msgID = CreateBPCMessageID();
                BotswanaPowerToken.VendIDMethod vendingMethod = new BotswanaPowerToken.VendIDMethod();
                BotswanaPowerToken.MeterNumber meterNumber = new BotswanaPowerToken.MeterNumber();
                meterNumber.msno = pMeterNumber;
                vendingMethod.meterIdentifier = meterNumber;
                creditVendRequest.idMethod = vendingMethod;

                BotswanaPowerToken.Currency currency = new BotswanaPowerToken.Currency();
                currency.symbol = "P";
                currency.value = pAmount;
                BotswanaPowerToken.PurchaseValueCurrency purchaseValCur = new BotswanaPowerToken.PurchaseValueCurrency();
                purchaseValCur.amt = currency;

                BotswanaPowerToken.PurchaseValue purchaseValue = purchaseValCur;

                BotswanaPowerToken.Cash purchaseType = new BotswanaPowerToken.Cash();
                purchaseType.tenderAmt = purchaseValCur.amt;
                creditVendRequest.payType = purchaseType;
                creditVendRequest.purchaseValue = purchaseValue;
                BotswanaPowerToken.WebService bcpClient = new BotswanaPowerToken.WebService();
                String receiptNumber = "";
                String token = "";
                String siUnit = "";
                String unit = "";
                String unitsBought = "";
                try
                {
                    BotswanaPowerToken.CreditVendResp creditVendResponse = bcpClient.CreditVendRequest(creditVendRequest);
                    receiptNumber = creditVendResponse.creditVendReceipt.receiptNo;
                    BotswanaPowerToken.Tx creditVendTx = creditVendResponse.creditVendReceipt.transactions.tx[0];
                    token = ((DPO.BackOffice.MySQL.BL.BotswanaPowerToken.STS1Token)((DPO.BackOffice.MySQL.BL.BotswanaPowerToken.CreditVendTx)creditVendTx).creditTokenIssue.token).stsCipher;
                    siUnit = ((DPO.BackOffice.MySQL.BL.BotswanaPowerToken.CreditVendTx)creditVendTx).creditTokenIssue.units.siUnit;
                    unit = ((DPO.BackOffice.MySQL.BL.BotswanaPowerToken.CreditVendTx)creditVendTx).creditTokenIssue.units.value.ToString();
                    unitsBought = unit + " " + siUnit;

                }

                catch (Exception Ex)
                {
                    InsertLog(applicationID, "DoBotswanaPowerPayment - " + Ex.Message, eLogType.Error);
                    if (Ex.Message.Contains("Meter Not Found"))
                        return "Error: Meter Not Found.";

                    if (Ex.Message.Contains("blocked"))
                        return "Customer Is Blocked.";

                    return "Error: Unable to issue token." + Ex.Message;
                }

                UpdateUssdMsisdnBPCData(pUssdMsisdnID, receiptNumber, token, unitsBought);

                ussdInformation.BPCReceiptNumber = receiptNumber;
                ussdInformation.BPCToken = token;
                ussdInformation.BPCUnitsBought = unitsBought;


                return "Success";
            }
            catch (Exception Ex)
            {
                InsertLog(applicationID, "DoBotswanaPowerPayment Main - " + Ex.Message, eLogType.Error);
                throw new Exception("DoBotswanaPowerPayment - " + Ex.Message);
            }
        }

        public Boolean UpdateUssdMsisdnBPCData(Int32 pUssdMsisdnID, String pReceiptNumber, String pToken, String pUnit)
        {
            try
            {
                tblussdmsisdnbpcdata bpcData = GetUssdMsisdnBPCData(pUssdMsisdnID);
                if (recordFound)
                {
                    bpcData.ReceiptNumber = pReceiptNumber;
                    bpcData.Token = pToken;
                    bpcData.Units = pUnit;
                    tblussdmsisdnbpcdataCrud crud = new tblussdmsisdnbpcdataCrud();
                    crud.Update(bpcData);
                }
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdMsisdnBPCData - " + Ex.Message);
            }
        }

        public tblussdmsisdnbpcdata GetUssdMsisdnBPCData(Int32 pUssdMsisdnID)
        {
            try
            {
                tblussdmsisdnbpcdataCrud crud = new tblussdmsisdnbpcdataCrud();
                crud.Where("UssdMsisdnID", MySQL.DAL.General.Operator.Equals, pUssdMsisdnID);
                recordFound = false;
                tblussdmsisdnbpcdata bpcData = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return bpcData;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdnBPCData - " + Ex.Message);
            }
        }
    }
}
