﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.MySQL.DAL;
using System.IO;
using System.Net;

namespace DPO.BackOffice.MySQL.BL
{
    public partial class MySQLBackOfficeBL
    {

        public List<tblconcerotelentity> GetConcerotelEntity()
        {
            try
            {
                tblconcerotelentityCrud crud = new tblconcerotelentityCrud();
                recordFound = false;
                List<tblconcerotelentity> res = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetConcerotelEntity - " + Ex.Message);
            }
        }
        public Boolean DoConcerotelPayment()
        {
            try
            {
                String url = GetSetting("Virtual Message", "DPO.BackOffice.Ussd", "ConcerotelUrl").SettingValue;
                String apiKey = GetSetting("Virtual Message", "DPO.BackOffice.Ussd", "ConcerotelApiKey").SettingValue;
                String hash = GetSetting("Virtual Message", "DPO.BackOffice.Ussd", "ConcerotelHash").SettingValue;
                if (ussdInformation.UssdTransaction.ToLower() == "concerotelinvoice")
                    url += "invoice/" + ussdInformation.InvoiceNumber;
                else
                    url += "hotspot/" + ussdInformation.UssdTransaction.Substring(ussdInformation.UssdTransaction.Length - 1, 1);

                url += "?apiKey=" + apiKey;

                String uniqueNo = GetBusinessUniqueReferenceNumber("Concerotel");
                Int32 unixTimestamp = Convert.ToInt32((DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
                String hashString = ussdInformation.Msisdn + unixTimestamp.ToString() + "Approved" + ussdInformation.Amount.ToString() + ".00" + uniqueNo + hash;

                System.Security.Cryptography.MD5 md5String;
                md5String = System.Security.Cryptography.MD5.Create();
                Byte[] inputBytes = Encoding.ASCII.GetBytes(hashString);
                Byte[] hashBytes = md5String.ComputeHash(inputBytes);
                StringBuilder sb = new StringBuilder();
                for (UInt32 i = 0; i < hashBytes.Length; i++)
                    sb.Append(hashBytes[i].ToString("x2"));

                String result =  PostToConcerotel(url, ussdInformation.Msisdn, ussdInformation.Amount.ToString() + ".00", uniqueNo, sb.ToString(), unixTimestamp.ToString());
                if (!result.ToLower().Contains("error="))
                {
                    if (ussdInformation.UssdTransaction.ToLower() == "concerotelinvoice")
                    {
                        result = result.Replace("{", "").Replace("}", "").Replace("\"", "");
                        String[] resultSplit = result.Split(',');
                        String invoiceStatus = resultSplit[0].Split(':')[0];
                        String timeStamp = resultSplit[5].Split(':')[0];
                        String uniqueID = resultSplit[6].Split(':')[0];
                        ussdInformation.ResponseMessage = "Invoice " + ussdInformation.InvoiceNumber + " was succesfully paid.";
                    }
                    else
                    {
                        result = result.Replace("{", "").Replace("}", "").Replace("\"", "");
                        String[] resultSplit = result.Split(',');
                        String voucherOption = resultSplit[0].Split(':')[1];
                        String voucherName = resultSplit[1].Split(':')[1];
                        String voucherPrice = resultSplit[2].Split(':')[1];
                        String voucherNo = resultSplit[5].Split(':')[1] + resultSplit[5].Split(':')[2] + resultSplit[5].Split(':')[3];
                        String timeStamp = resultSplit[6].Split(':')[1];
                        InsertUssdMsidnConcerotelData(ussdInformation.UssdMsisdnTransactionID, voucherOption, voucherName, voucherNo);
                    }
                }
                else
                {
                    if (ussdInformation.UssdTransaction.ToLower() == "concerotelinvoice")
                    {
                        ussdInformation.ResponseMessage = "Concerotel invoice payment exception.";
                        return false;
                    }
                    else
                    {
                        ussdInformation.ResponseMessage = "Concerotel voucher disbursement exception.";
                        return false;
                    }
                }
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DoConcerotelPayment - " + Ex.Message);
            }
        }

        public Int32 InsertUssdMsidnConcerotelData(Int32 pTransactionID, String pVoucherOption, String pVoucherName, String pVoucherNo)
        {
            try
            {
                tblussdmsisdnconceroteldataCrud crud = new tblussdmsisdnconceroteldataCrud();
                tblussdmsisdnconceroteldata data = new tblussdmsisdnconceroteldata();
                data.TransactionID = pTransactionID;
                data.VoucherName = pVoucherName;
                data.VoucherNo = pVoucherNo;
                data.VoucherOption = pVoucherOption;
                long id = 0;
                crud.Insert(data, out id);

                return (Int32)id;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdMsidnConcerotelData - " + Ex.Message);
            }
        }
        public String PostToConcerotel(String pUrl, String pMobileNo, String pAmount, String pUniqueID, String pHash, String pTimeStamp)
        {
            System.Net.WebClient client = new System.Net.WebClient();
            try
            {

                ConcerotelData conData = new ConcerotelData();
                conData.amount = pAmount;
                conData.hash = pHash;
                conData.id = pUniqueID;
                conData.mobile = pMobileNo;
                conData.result = "Approved";
                conData.timestamp = Convert.ToInt32(pTimeStamp);

                System.Web.Script.Serialization.JavaScriptSerializer jSerial = new System.Web.Script.Serialization.JavaScriptSerializer();
                String jsonData = jSerial.Serialize(conData);

                jsonData = "{\"transaction\":" + jsonData + "}";

                string responseJson = string.Empty;


                using (client)
                {
                    client.Headers[System.Net.HttpRequestHeader.Accept] = "application/json";
                    client.Headers[System.Net.HttpRequestHeader.ContentType] = "application/json";

                    byte[] response = client.UploadData(pUrl, Encoding.UTF8.GetBytes(jsonData));

                    responseJson = Encoding.UTF8.GetString(response);
                }

                return responseJson;
            }
            catch (System.Net.WebException webEx)
            {
                if (webEx.Response != null)
                {
                    var resp = new System.IO.StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
                    dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject(resp);
                    String messageFromServer = obj.error.ToString();

                    return "Error=" + messageFromServer;
                }
                else
                    return "Error=" + webEx.Message;
            }
            catch (Exception Ex)
            {
                throw new Exception("PostToConcerotel-" + Ex.Message);
            }
        }
        public String GetConcerotelInvoiceAmount(String pUrl)
        {
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(pUrl);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                String result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                return result;
            }
            catch (System.Net.WebException webEx)
            {
                if (webEx.Response != null)
                {
                    var resp = new System.IO.StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
                    dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject(resp);
                    String messageFromServer = obj.error.ToString();

                    return "Error=" + messageFromServer;
                }
                else
                    return "Error=" + webEx.Message;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetConcerotelInvoiceAmount-" + Ex.Message);
            }

        }
        public Boolean ValidateConcerotelInvoice(String pInvoiceNumber, out String pValidateMessage, Decimal pAmount)
        {
            String logInfo = "";
            try
            {
                pValidateMessage = "";
                String url = GetSetting("Virtual Message", "DPO.BackOffice.Ussd", "ConcerotelUrl").SettingValue;
                String apiKey = GetSetting("Virtual Message", "DPO.BackOffice.Ussd", "ConcerotelApiKey").SettingValue;
                url += "invoice/" + pInvoiceNumber;
                url += "?apiKey=" + apiKey;

                logInfo = "Url=" + url;
                String result = GetConcerotelInvoiceAmount(url);
                logInfo += "|result=" + result;
                if (!result.Contains("Error"))
                {
                    result = result.Replace("{", "").Replace("}", "").Replace("\"", "");
                    logInfo += "|result2=" + result;
                    String[] resultSplit = result.Split(',');
                    String invoiceStatus = resultSplit[0].Split(':')[0];
                    logInfo += "|invoiceStatus=" + invoiceStatus;
                    String invoiceAmount = resultSplit[1].Split(':')[0];
                    logInfo += "|invoiceAmount=" + invoiceAmount;
                    Decimal amount = Convert.ToDecimal(invoiceAmount);
                    if (invoiceStatus.ToLower() == "paid")
                    {
                        ussdInformation.ResponseCode = "37";
                        pValidateMessage = "This invoice is paid in full.";
                        ussdInformation.ResponseMessage = pValidateMessage;
                        return false;
                    }

                    if (amount != pAmount)
                    {
                        ussdInformation.ResponseCode = "38";
                        pValidateMessage = "Amount discrepancy. Invoice amount is " + invoiceAmount.ToString() + ", amount submitted is " + pAmount.ToString();
                        ussdInformation.ResponseMessage = "Amount discrepancy. Amount entered is not equal to invoice amount.";
                        return false;
                    }
                }
                else
                {
                    if (result.ToLower().Contains("invoice does not exist"))
                    {
                        ussdInformation.ResponseCode = "40";
                        pValidateMessage = "Invoice does not exist.";
                    }
                    else
                    {
                        ussdInformation.ResponseCode = "39";
                        pValidateMessage = "Unable to validate invoice. Sorry for the inconvenience.";
                    }
                    return false;
                }

                ussdInformation.ResponseCode = "00";
                return true;

            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateConcerotelInvoice - " + Ex.Message + "|LogInfo=" + logInfo);
            }
        }


    }
}
