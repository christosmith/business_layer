﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.MySQL.DAL;

namespace DPO.BackOffice.MySQL.BL
{
    public partial class MySQLBackOfficeBL
    {

        public List<tblmultichoiceentity> GetMultiChoiceEntity()
        {
            try
            {
                tblmultichoiceentityCrud crud = new tblmultichoiceentityCrud();
                recordFound = false;
                List<tblmultichoiceentity> res = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return res;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetMultiChoiceEntity - " + Ex.Message);
            }
        }
        public Boolean UpdateUssdMsisdnMultiChoiceData(Int32 pUssdMsisdnID, Decimal pLastinvoiceAmount, DateTime pLastInvoiceDate, String pAccountNumber, DateTime pPaymentDueDate, String pAccountStatus, Decimal pTotalBalance, String pFirstName, String pEMailAddress, String pInitials, String pCustomerNumber, String pCustomerStatus, String pSurname, String pCellPhoneNo, String pSmartCardNumber)
        {
            try
            {
                tblussdmsisdnmultichoicedata mcData = GetUssdMsisdnMultiChoiceData(pUssdMsisdnID);
                if (!recordFound)
                {
                    InsertUssdMsisdnMultiChoiceData(pUssdMsisdnID, pLastinvoiceAmount, pLastInvoiceDate, pAccountNumber, pPaymentDueDate, pAccountStatus, pTotalBalance, pFirstName, pEMailAddress, pInitials, pCustomerNumber, pCustomerStatus, pSurname, pCellPhoneNo, pSmartCardNumber);
                    return true;
                }

                mcData.AccountNumber = pAccountNumber;
                mcData.AccountStatus = pAccountStatus;
                mcData.CustomerNumber = pCustomerNumber;
                mcData.CustomerStatus = pCustomerStatus;
                mcData.EMailAddress = pEMailAddress;
                mcData.FirstName = pFirstName;
                mcData.Initials = pInitials;
                mcData.LastinvoiceAmount = pLastinvoiceAmount;
                //if (pLastInvoiceDate.Year !=1)
                //    mcData.LastInvoiceDate = pLastInvoiceDate;
                mcData.PaymentDueDate = pPaymentDueDate;
                mcData.Surname = pSurname;
                mcData.TotalBalance = pTotalBalance;
                mcData.CellPhoneNo = pCellPhoneNo;
                mcData.SmartCardNumber = pSmartCardNumber;
                tblussdmsisdnmultichoicedataCrud crud = new tblussdmsisdnmultichoicedataCrud();
                crud.Update(mcData);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdMsisdnMultiChoiceData - " + Ex.Message);
            }
        }

        public String DoMultiChoiceDetailUpdate(String pSmartCardNumber, MultiChoiceDetailType pDetailType, String pDetail)
        {
            String retString = "";
            try
            {
                MultiChoiceWebService.SelfCareServiceClient mcClient = new MultiChoiceWebService.SelfCareServiceClient();
                MultiChoiceWebService.UpdateContactDetailsRequest updateDetails = new MultiChoiceWebService.UpdateContactDetailsRequest();
                String customerNumber = GetUssdMsisdnMultiChoiceData(pSmartCardNumber).CustomerNumber;

                updateDetails.customerNumber = Convert.ToUInt32(customerNumber);
                if (Convert.ToInt32(pDetailType) == 3)
                {

                }
                else
                {
                    switch (pDetailType)
                    {
                        case MultiChoiceDetailType.CellPhone: updateDetails.cellNumber = pDetail; break;
                        case MultiChoiceDetailType.EmailAddress: updateDetails.emailAddress = pDetail; break;
                    }

                    MulitChoiceParameter mcParam = GetMultiChoiceParameter();
                    try
                    {
                        mcClient.UpdateContactDetails(mcParam.DataSource, updateDetails, mcParam.BusinessUnit, mcParam.CustomerVendorID, "", "", "");
                    }
                    catch (Exception Ex)
                    {
                        retString = "MultiChoice detail update failed.";
                        return retString;
                    }

                }

                return "Success";
            }
            catch (Exception Ex)
            {
                throw new Exception("DoMultiChoiceDetailUpdate - " + Ex.Message);
            }
        }
        public Int32 InsertUssdMsisdnMultiChoiceData(Int32 pUssdMsisdnID, Decimal pLastinvoiceAmount, DateTime pLastInvoiceDate, String pAccountNumber, DateTime pPaymentDueDate, String pAccountStatus, Decimal pTotalBalance, String pFirstName, String pEMailAddress, String pInitials, String pCustomerNumber, String pCustomerStatus, String pSurname, String pCellPhoneNo, String pSmartCardNuber)
        {
            try
            {
                tblussdmsisdnmultichoicedataCrud crud = new tblussdmsisdnmultichoicedataCrud();
                tblussdmsisdnmultichoicedata mcData = new tblussdmsisdnmultichoicedata();
                mcData.AccountNumber = pAccountNumber;
                mcData.AccountStatus = pAccountStatus;
                mcData.CustomerNumber = pCustomerNumber;
                mcData.CustomerStatus = pCustomerStatus;
                mcData.EMailAddress = pEMailAddress;
                mcData.FirstName = pFirstName;
                mcData.Initials = pInitials;
                //mcData.LastinvoiceAmount = pLastinvoiceAmount;
                //mcData.LastInvoiceDate = pLastInvoiceDate;
                mcData.PaymentDueDate = pPaymentDueDate;
                //mcData.Surname = pSurname;
                mcData.TotalBalance = pTotalBalance;
                mcData.UssdMsisdnID = pUssdMsisdnID;
                mcData.CellPhoneNo = pCellPhoneNo;
                mcData.SmartCardNumber = pSmartCardNuber;
                long ID = 0;
                crud.Insert(mcData, out ID);

                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdMsisdnMultiChoiceData - " + Ex.Message);
            }
        }

        public tblussdvalidationmessage ValidateMultiChoiceSmartCardNumber(String pSmartCardNumber, Int32 pUssdMsisdnID)
        {
            try
            {

                tblussdvalidationmessage mess = new tblussdvalidationmessage();
                /*if (pSmartCardNumber.Length != 10 || pSmartCardNumber.Length != 11)
                {
                    mess = GetUssdValidationMessage("The smartcard number entered is incorrect.");
                    recordFound = true;
                }*/

                MultiChoiceWebService.SelfCareServiceClient mcClient = new MultiChoiceWebService.SelfCareServiceClient();

                //(setting.mcaDataSource, mcaSmartCardNumber, setting.mcaBotswanaCurrency, setting.mcaBusinessUnit, setting.mcaCustomerVendorID, "", "")
                MulitChoiceParameter mcParam = GetMultiChoiceParameter();

                MultiChoiceWebService.GetBalanceByDeviceNumberResponse mcResponse = new MultiChoiceWebService.GetBalanceByDeviceNumberResponse();
                try
                {
                    mcResponse = mcClient.GetCustomerDetailsByDeviceNumber(mcParam.DataSource, pSmartCardNumber, mcParam.BotswanaCurrency, mcParam.BusinessUnit, mcParam.CustomerVendorID, "", "", "");

                    Decimal pLastinvoiceAmount = Convert.ToDecimal(mcResponse.accounts[0].lastInvoiceAmount);
                    DateTime pLastInvoiceDate = mcResponse.accounts[0].lastInvoiceDate;
                    String pAccountNumber = mcResponse.accounts[0].number.ToString();

                    String pAccountStatus = mcResponse.accounts[0].status.ToString();

                    String pFirstName = mcResponse.customerDetails.FirstName;
                    String pEMailAddress = mcResponse.customerDetails.emailAddress;
                    String pInitials = mcResponse.customerDetails.initials;
                    String pCustomerNumber = mcResponse.customerDetails.number.ToString();
                    ussdInformation.MCACustomerNo = pCustomerNumber;
                    String pCustomerStatus = mcResponse.customerDetails.status.ToString();
                    String pSurname = mcResponse.customerDetails.surname;
                    String pCellPhoneNo = mcResponse.customerDetails.cellNumber;

                    MultiChoiceWebService.DueAmountAndDate dueDate = new MultiChoiceWebService.DueAmountAndDate();
                    dueDate = mcClient.GetDueAmountandDate(mcParam.DataSource, Convert.ToInt32(mcResponse.customerDetails.number), "", mcParam.BusinessUnit, mcParam.CustomerVendorID, "", "", "");
                    Decimal pTotalBalance = dueDate.dueAmount;
                    DateTime pPaymentDueDate = dueDate.dueDate;

                    UpdateUssdMsisdnMultiChoiceData(pUssdMsisdnID, pLastinvoiceAmount, pLastInvoiceDate, pAccountNumber, pPaymentDueDate, pAccountStatus, pTotalBalance, pFirstName, pEMailAddress, pInitials, pCustomerNumber, pCustomerStatus, pSurname, pCellPhoneNo, pSmartCardNumber);

                    recordFound = false;

                }
                catch (Exception Ex)
                {
                    recordFound = true;
                    String errorMessage = Ex.Message;
                    if (errorMessage.Contains("smartcardNumber not found"))
                    {
                        mess = GetUssdValidationMessage("The smartcard number was not found.");

                        if (!recordFound)
                            InsertLog(Convert.ToInt32(GetApplication("VCS.VirtualMessage.Ussd").ApplicationID), "Error message not set up in tblValidationMessage = " + errorMessage, eLogType.Error);
                    }
                    else
                        throw new Exception("ValidateMultiChoiceSmartCardNumber - " + Ex.Message);
                }

                return mess;

            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateMultiChoiceSmartCardNumber - " + Ex.Message);
            }
        }
        public List<tblussdtransactionaction> GetUssdTransactionAction(Int16 pUssdTransactionGroupID)
        {
            try
            {
                tblussdtransactionactionCrud crud = new tblussdtransactionactionCrud();
                crud.Where("UssdTransactionGroupID", MySQL.DAL.General.Operator.Equals, pUssdTransactionGroupID);
                recordFound = false;
                List<tblussdtransactionaction> tran = crud.ReadMulti().ToList();
                tran = tran.OrderBy(x => x.Ordinal).ToList();
                recordFound = crud.RecordFound;

                return tran;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransactionAction - " + Ex.Message);
            }
        }
        public MulitChoiceParameter GetMultiChoiceParameter()
        {
            try
            {
                Int32 applicationID = Convert.ToInt32(GetApplication("DPO.BackOffice.Ussd").ApplicationID);
                MulitChoiceParameter mcParam = new MulitChoiceParameter();
                mcParam.VendorCode = GetSettingByName("McaBotswana", applicationID).SettingValue;
                mcParam.CustomerVendorID = GetSettingByName("McaCustomerVendorID", applicationID).SettingValue;
                mcParam.DataSource = GetSettingByName("McaBotswana", applicationID).SettingValue;
                mcParam.BotswanaCurrency = GetSettingByName("McaBotswanaCurrency", applicationID).SettingValue;
                mcParam.BusinessUnit = GetSettingByName("McaBusinessUnit", applicationID).SettingValue;
                mcParam.PaymentVendorCode = GetSettingByName("McaVendorId", applicationID).SettingValue;

                return mcParam;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMultiChoiceParameter - " + Ex.Message);
            }
        }
        public String DoMultiChoicePayment(List<String> pProductUserKeys, String pUserID, Int32 pUssdMsisdnID, Decimal pAmount)
        {
            String logInfo = "";
            try
            {
                tblussdmsisdnmultichoicedata mcData = GetUssdMsisdnMultiChoiceData(pUssdMsisdnID);
                if (!recordFound)
                    throw new Exception("MultiChoice data was not collected for msisdn with id=" + pUssdMsisdnID.ToString());

                logInfo += "Initialising";
                MultiChoiceWebService.SelfCareServiceClient mcClient = new MultiChoiceWebService.SelfCareServiceClient();


                logInfo += "|GetParameters";
                MulitChoiceParameter mcParam = GetMultiChoiceParameter();
                MultiChoiceWebService.PaymentProductCollection productCollection = new MultiChoiceWebService.PaymentProductCollection();
                MultiChoiceWebService.SubmitPaymentResponse paymentResponse = new MultiChoiceWebService.SubmitPaymentResponse();
                MultiChoiceWebService.PaymentProduct product = new MultiChoiceWebService.PaymentProduct();
                logInfo += "|AssignProductUserky";
                foreach (String productUserKey in pProductUserKeys)
                {
                    logInfo += "|" + productUserKey;
                    product.ProductUserKey = productUserKey;
                    productCollection.Add(product);
                }

                String retString = "";
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                try
                {
                    logInfo += "|GetMerchantCount";
                    String transactionNumber = GetBusiness("MultiChoice Africa").UniqueReferenceNo.ToString();
                    logInfo += "|transactionNumber=" + transactionNumber;
                    UInt32 customerNumber = Convert.ToUInt32(mcData.CustomerNumber);
                    logInfo += "|CustomerNumber=" + customerNumber.ToString();
                    paymentResponse = mcClient.SubmitPayment(mcParam.CustomerVendorID, mcParam.DataSource, mcParam.PaymentVendorCode, transactionNumber, customerNumber, pAmount, 1, mcParam.BotswanaCurrency, "Payment via Virtual Mobile", productCollection, "MOBILE", "", "", "DSTV", null);
                    logInfo += "|PaymentResponse.ErrorMessage=" + paymentResponse.SubmitPayment.ErrorMessage;
                }
                catch (Exception Ex)
                {
                    logInfo += "|MultiChoiceError=" + Ex.Message;
                    retString = "MultiChoice payment exception";
                    return retString;
                }


                String receiptNumber = paymentResponse.SubmitPayment.receiptNumber.ToString();
                logInfo += "|ReceiptNumber=" + receiptNumber;
                UpdateUssdMsisdnMultiChoiceData(receiptNumber, pUssdMsisdnID);
                logInfo += "|MultiChoiceDataUpdated";
                retString = receiptNumber;
                return retString;
            }
            catch (Exception Ex)
            {
                throw new Exception("DoMulitChoicePayment - " + Ex.Message + "|Loginfo=" + logInfo);
            }
        }
        public Boolean UpdateUssdMsisdnMultiChoiceData(String pReceiptNumber, Int32 pUssdMsisdnID)
        {
            try
            {
                tblussdmsisdnmultichoicedata mcData = GetUssdMsisdnMultiChoiceData(pUssdMsisdnID);
                mcData.ReceiptNumber = pReceiptNumber;
                tblussdmsisdnmultichoicedataCrud crud = new tblussdmsisdnmultichoicedataCrud();
                crud.Update(mcData);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdMsisdnMultiChoiceData - " + Ex.Message);
            }
        }
        public String DoMultiChoiceReathorisation(String pSmartCardNumber)
        {
            String retString = "";
            try
            {
                MultiChoiceWebService.SelfCareServiceClient mcClient = new MultiChoiceWebService.SelfCareServiceClient();
                MultiChoiceWebService.DigitalReauthReason reason = new MultiChoiceWebService.DigitalReauthReason();
                reason.Reason = MultiChoiceWebService.DigitalReauthReasons.E16;
                Boolean result = false;
                MulitChoiceParameter mcParam = GetMultiChoiceParameter();
                try
                {
                    result = mcClient.ReAuthorize(mcParam.DataSource, pSmartCardNumber, reason, mcParam.CustomerVendorID, "", "", mcParam.BusinessUnit);
                }
                catch (Exception Ex)
                {
                    if (Ex.Message.Contains("Your DStv account is suspended."))
                    {
                        retString = "Your DStv account is suspended.";
                    }
                    else
                        retString = "MultiChoice re-authorisation exception";
                    return retString;
                }

                if (!result)
                    retString = "MultiChoice re-authorisation exception";

                return "Success";
            }
            catch (Exception Ex)
            {
                throw new Exception("DoMultiChoiceReathorisation - " + Ex.Message);
            }
        }
        public tblussdmsisdnmultichoicedata GetUssdMsisdnMultiChoiceData(Int32 pUssdMsisdnID)
        {
            try
            {
                tblussdmsisdnmultichoicedataCrud crud = new tblussdmsisdnmultichoicedataCrud();
                crud.Where("UssdMsisdnID", MySQL.DAL.General.Operator.Equals, pUssdMsisdnID);
                recordFound = false;
                tblussdmsisdnmultichoicedata mcData = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return mcData;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdnMultiChoiceData - " + Ex.Message);
            }
        }
        public tblussdmsisdnmultichoicedata GetUssdMsisdnMultiChoiceData(String pSmartCardNumber)
        {
            try
            {
                tblussdmsisdnmultichoicedataCrud crud = new tblussdmsisdnmultichoicedataCrud();
                crud.Where("SmartCardNumber", MySQL.DAL.General.Operator.Equals, pSmartCardNumber);
                recordFound = false;
                tblussdmsisdnmultichoicedata mcData = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return mcData;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdnMultiChoiceData - " + Ex.Message);
            }
        }

    }
}
