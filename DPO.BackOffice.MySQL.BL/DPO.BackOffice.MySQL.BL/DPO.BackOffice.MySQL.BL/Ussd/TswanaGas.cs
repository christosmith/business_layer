﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.MySQL.DAL;
using System.Net;
using System.IO;

namespace DPO.BackOffice.MySQL.BL
{
    public partial class MySQLBackOfficeBL
    {
        public class TswanaGasCity
        {
            public Int32 CityID = 0;
            public String CityName = "";
        }

        public class TswanaGasCentre
        {
            public Int32 CentreID = 0;
            public String CentreName = "";
            public Int32 CylinderID = 0;
            public String CylinderType = "";
            public Decimal CylinderPrice = 0;
        }

        public class TswanaGasCylinder
        {
            public Int32 CylinderID = 0;
            public String CylinderName = "";
        }
        public Boolean UpdateTswanaGasCityActiveMS(Int32 pCityID, Boolean pActive, Int32 pUserID)
        {
            try
            {
                tbltswanagascity city = GetTswanaGasCityMS(pCityID);
                city.Active = pActive;
                tbltswanagascityCrud crud = new tbltswanagascityCrud();
                crud.Update(city);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateTswanaGasCityActiveMS - " + Ex.Message);
            }
        }
        public Boolean TswanaGasUssdTablePopulateGateway()
        {
            String logInfo = "";
            try
            {
            tblussdaction actionAuth = GetUssdAction("Bank Card Authorisation");
            tblussdaction actionMessage = GetUssdAction("Send Text Message");
            tblussdaction actionVending = GetUssdAction("Insert Vending Record");
            tblussdaction actionGasOrder = GetUssdAction("Tswana Gas Order");


            tblbusiness thirdParty = GetBusiness("Tswana Gas");
            Int16 thirdPartyID = 0;
            thirdPartyID = (Int16)thirdParty.BusinessID;

            Decimal emptyCylCost = GetTswanaGasEmptyCylinder();

            tblussdtransactiondescription vendMessage = GetUssdTransactionDescription("Tswana Gas Sale - {?GasCylinder}");
            Int16 tranDescID = Convert.ToInt16(vendMessage.TranDescID);
            tblussdtextmessage textMessage = GetUssdTextMessage("Payment of {?amount} received for {?GasCylinder}. Order number {?orderNumber}.");
            Int16 textMessageID = Convert.ToInt16(textMessage.TextMessageID);

            String transaction = "";
            List<tbltswanagascity> cities = GetTswanaGasCityMS();
            foreach (tbltswanagascity city in cities)
            {
                logInfo += "|City=" + city.CityName;
                Console.WriteLine(city.CityName); Console.ReadKey();
                if ((Boolean)city.Active)
                {
                    logInfo += "|CityIsActive";
                    List<tbltswanagascentre> centres = GetTswanaGasCentreMS((Int32)city.CityID);
                    logInfo += "|CentresRetrieved";
                    foreach (tbltswanagascentre centre in centres)
                    {
                        logInfo += "|Centre=" + centre.CentreName;
                        List<tbltswanagascylinder> cylinders = GetTswanaGasCylinderMS();
                        foreach (tbltswanagascylinder cylinder in cylinders)
                        {

                            tbltswanagascentrecylinder centreCylinder = GetTswanaGasCentreCylinderMS((Int32)centre.CentreID, (Int32)cylinder.CylinderID);

                            #region With Empty. Pay Now
                            String productLookup = "Tswana Gas-" + city.tbltswanagasentitycode.EntityName + ";" + centre.tbltswanagasentitycode.EntityName + ";" + cylinder.tbltswanagasentitycode.EntityName.Replace(" ", "") + ";PayNow;WithEmpty";
                                logInfo += "|" + productLookup;
                            transaction = city.tbltswanagasentitycode.EntityCode + centre.tbltswanagasentitycode.EntityCode + cylinder.tbltswanagasentitycode.EntityCode + "YY";

                            tblussdtransactiongroup tranGroup = GetUssdTransactionGroup(productLookup);
                            Int16 transactionGroupID = 0;

                            if (!recordFound)
                                transactionGroupID = InsertUssdTransactionGroup(productLookup, tranDescID, textMessageID, transaction);
                            else
                                transactionGroupID = Convert.ToInt16(tranGroup.UssdTransactionGroupID);

                            tblbusinesstransaction busTran = GetBusinessTransaction(transactionGroupID);
                            if (!recordFound)
                                InsertBusinessTransaction(thirdPartyID, transactionGroupID);

                                logInfo += "|Getting Actions";
                                List<tblussdtransactionaction> actions = GetUssdTransactionAction(transactionGroupID);
                            Boolean itemExist = false;
                            itemExist = actions.Any(x => x.UssdActionID == actionAuth.UssdActionID);
                            if (!itemExist)
                                InsertUssdTransactionAction(transactionGroupID, (Int16)actionAuth.UssdActionID, 1);
                            itemExist = actions.Any(x => x.UssdActionID == actionGasOrder.UssdActionID);
                            if (!itemExist)
                                InsertUssdTransactionAction(transactionGroupID, (Int16)actionGasOrder.UssdActionID, 2);
                            itemExist = actions.Any(x => x.UssdActionID == actionMessage.UssdActionID);
                            if (!itemExist)
                                InsertUssdTransactionAction(transactionGroupID, (Int16)actionMessage.UssdActionID, 3);
                            itemExist = actions.Any(x => x.UssdActionID == actionVending.UssdActionID);
                            if (!itemExist)
                                InsertUssdTransactionAction(transactionGroupID, (Int16)actionVending.UssdActionID, 4);

                                logInfo += "|Getting Actions - Done";

                                Int16 ussdTransactionID = 0;
                            tblussdtransaction tran = GetUssdTransactionMS(transaction);
                            String productDesc = "Tswana Gas Sale. City Name: " + city.tbltswanagasentitycode.EntityName + "; Centre Name: " + centre.tbltswanagasentitycode.EntityName + "; Cylinder Size: " + cylinder.tbltswanagasentitycode.EntityName + "; Pay Now; With Empty";
                            if (!recordFound)
                                ussdTransactionID = InsertUssdTransactionMS(transaction, (Decimal)centreCylinder.CylinderPrice, "", "", productDesc, thirdParty.TerminalID);
                            else
                            {
                                Boolean doUpdate = false;
                                if ((Decimal)tran.TransactionAmount != (Decimal)centreCylinder.CylinderPrice) doUpdate = true;
                                if (tran.ProductDescription != productDesc) doUpdate = true;
                                if (doUpdate)
                                    UpdateUssdTransaction(Convert.ToInt16(tran.UssdTransactionID), (Decimal)centreCylinder.CylinderPrice, productDesc);

                                ussdTransactionID = Convert.ToInt16(tran.UssdTransactionID);
                            }

                            tblussdtransactiongroupitem groupItem = GetUssdTransactionGroupItem(transactionGroupID, ussdTransactionID);
                            if (!recordFound)
                                InsertUssdTransactionGroupItem(transactionGroupID, ussdTransactionID);

                            Int16 tranProcessID = 0;
                            tblussdtransactionprocessinstruction tranProcess = GetUssdTransactionProcessInstructionMS(transactionGroupID, Convert.ToInt16(GetUssdProcessInstructionMS("Extract Bank Card Number").ProcessInstructionID));
                            if (!recordFound)
                                tranProcessID = InsertUssdTransactionProcessInstructionMS(transactionGroupID, Convert.ToInt16(GetUssdProcessInstructionMS("Extract Bank Card Number").ProcessInstructionID));
                            else
                                tranProcessID = (Int16)tranProcess.TranProcessID;

                                logInfo += "|Getting Instruction Tags";
                            Int16 tagNameID = Convert.ToInt16(GetUssdTagNameMS("CardNumber").TagNameID);
                            Byte validationErrorID = Convert.ToByte(GetUssdValidationErrorMS("Card number missing").ValidationErrorID);
                            GetUssdTransactionProcessInstructionTagMS(tranProcessID, tagNameID);
                            if (!recordFound)
                                InsertUssdTransactionProcessInstructionTagMS(tranProcessID, tagNameID, validationErrorID);
                            tagNameID = Convert.ToInt16(GetUssdTagNameMS("ExpiryDate").TagNameID);
                            validationErrorID = Convert.ToByte(GetUssdValidationErrorMS("Expiry Date missing").ValidationErrorID);
                            GetUssdTransactionProcessInstructionTagMS(tranProcessID, tagNameID);
                            if (!RecordFound)
                                InsertUssdTransactionProcessInstructionTagMS(tranProcessID, tagNameID, validationErrorID);
                            tagNameID = Convert.ToInt16(GetUssdTagNameMS("Cvc").TagNameID);
                            validationErrorID = Convert.ToByte(GetUssdValidationErrorMS("Cvc missing").ValidationErrorID);
                            GetUssdTransactionProcessInstructionTagMS(tranProcessID, tagNameID);
                            if (!RecordFound)
                                InsertUssdTransactionProcessInstructionTagMS(tranProcessID, tagNameID, validationErrorID);

                                logInfo += "|Getting Instruction Tags - Done";
                                #endregion
                                #region Without Empty. Pay Now
                                productLookup = "Tswana Gas-" + city.tbltswanagasentitycode.EntityName + ";" + centre.tbltswanagasentitycode.EntityName + ";" + cylinder.tbltswanagasentitycode.EntityName.Replace(" ", "") + ";PayNow;WithoutEmpty";
                                logInfo += "|" + productLookup;
                                transaction = city.tbltswanagasentitycode.EntityCode + centre.tbltswanagasentitycode.EntityCode + cylinder.tbltswanagasentitycode.EntityCode + "YN";

                            tranGroup = GetUssdTransactionGroup(productLookup);
                            transactionGroupID = 0;

                            if (!recordFound)
                                transactionGroupID = InsertUssdTransactionGroup(productLookup, tranDescID, textMessageID, transaction);
                            else
                                transactionGroupID = Convert.ToInt16(tranGroup.UssdTransactionGroupID);

                                
                            busTran = GetBusinessTransaction(transactionGroupID);
                            if (!recordFound)
                                InsertBusinessTransaction(thirdPartyID, transactionGroupID);

                                logInfo += "| 2 GetAction";
                                actions = GetUssdTransactionAction(transactionGroupID);
                            itemExist = false;
                            itemExist = actions.Any(x => x.UssdActionID == actionAuth.UssdActionID);
                            if (!itemExist)
                                InsertUssdTransactionAction(transactionGroupID, (Int16)actionAuth.UssdActionID, 1);
                            itemExist = actions.Any(x => x.UssdActionID == actionGasOrder.UssdActionID);
                            if (!itemExist)
                                InsertUssdTransactionAction(transactionGroupID, (Int16)actionGasOrder.UssdActionID, 2);
                            itemExist = actions.Any(x => x.UssdActionID == actionMessage.UssdActionID);
                            if (!itemExist)
                                InsertUssdTransactionAction(transactionGroupID, (Int16)actionMessage.UssdActionID, 3);
                            itemExist = actions.Any(x => x.UssdActionID == actionVending.UssdActionID);
                            if (!itemExist)
                                InsertUssdTransactionAction(transactionGroupID, (Int16)actionVending.UssdActionID, 4);
                                logInfo += "| 2 GetAction - Done";
                                ussdTransactionID = 0;
                            tran = GetUssdTransactionMS(transaction);
                            productDesc = "Tswana Gas Sale. City Name: " + city.tbltswanagasentitycode.EntityName + "; Centre Name: " + centre.tbltswanagasentitycode.EntityName + "; Cylinder Size: " + cylinder.tbltswanagasentitycode.EntityName + ";Pay Now;Without Empty; ";                            if (!recordFound)
                                ussdTransactionID = InsertUssdTransactionMS(transaction, (Decimal)centreCylinder.CylinderPrice + emptyCylCost, "", "", productDesc, thirdParty.TerminalID);
                            else
                            {
                                    logInfo += "| 2 cylinder price";
                                    Boolean doUpdate = false;
                                if ((Decimal)tran.TransactionAmount != (Decimal)centreCylinder.CylinderPrice + emptyCylCost) doUpdate = true;
                                if (tran.ProductDescription != productDesc) doUpdate = true;
                                if (doUpdate)
                                    UpdateUssdTransaction(Convert.ToInt16(tran.UssdTransactionID), (Decimal)centreCylinder.CylinderPrice, productDesc);

                                ussdTransactionID = Convert.ToInt16(tran.UssdTransactionID);
                                    logInfo += "| 2 cylinder price - done";
                                }

                            groupItem = GetUssdTransactionGroupItem(transactionGroupID, ussdTransactionID);
                            if (!recordFound)
                                InsertUssdTransactionGroupItem(transactionGroupID, ussdTransactionID);

                            tranProcessID = 0;
                            tranProcess = GetUssdTransactionProcessInstructionMS(transactionGroupID, Convert.ToInt16(GetUssdProcessInstructionMS("Extract Bank Card Number").ProcessInstructionID));
                            if (!recordFound)
                                tranProcessID = InsertUssdTransactionProcessInstructionMS(transactionGroupID, Convert.ToInt16(GetUssdProcessInstructionMS("Extract Bank Card Number").ProcessInstructionID));
                            else
                                tranProcessID = (Int16)tranProcess.TranProcessID;


                            tagNameID = Convert.ToInt16(GetUssdTagNameMS("CardNumber").TagNameID);
                            validationErrorID = Convert.ToByte(GetUssdValidationErrorMS("Card number missing").ValidationErrorID);
                            GetUssdTransactionProcessInstructionTagMS(tranProcessID, tagNameID);
                            if (!recordFound)
                                InsertUssdTransactionProcessInstructionTagMS(tranProcessID, tagNameID, validationErrorID);
                            tagNameID = Convert.ToInt16(GetUssdTagNameMS("ExpiryDate").TagNameID);
                            validationErrorID = Convert.ToByte(GetUssdValidationErrorMS("Expiry Date missing").ValidationErrorID);
                            GetUssdTransactionProcessInstructionTagMS(tranProcessID, tagNameID);
                            if (!RecordFound)
                                InsertUssdTransactionProcessInstructionTagMS(tranProcessID, tagNameID, validationErrorID);
                            tagNameID = Convert.ToInt16(GetUssdTagNameMS("Cvc").TagNameID);
                            validationErrorID = Convert.ToByte(GetUssdValidationErrorMS("Cvc missing").ValidationErrorID);
                            GetUssdTransactionProcessInstructionTagMS(tranProcessID, tagNameID);
                            if (!RecordFound)
                                InsertUssdTransactionProcessInstructionTagMS(tranProcessID, tagNameID, validationErrorID);

                            #endregion
                            #region With Empty.Pay On Delivery
                            productLookup = "Tswana Gas-" + city.tbltswanagasentitycode.EntityName + ";" + centre.tbltswanagasentitycode.EntityName + ";" + cylinder.tbltswanagasentitycode.EntityName.Replace(" ", "") + "PayDelivery;WithEmpty";
                                logInfo += "|" + productLookup;
                                transaction = city.tbltswanagasentitycode.EntityCode + centre.tbltswanagasentitycode.EntityCode + cylinder.tbltswanagasentitycode.EntityCode + "NY";
                                logInfo += "|E";
                                tranGroup = GetUssdTransactionGroup(productLookup);
                            transactionGroupID = 0;
                                logInfo += "|F";
                                if (!recordFound)
                                transactionGroupID = InsertUssdTransactionGroup(productLookup, tranDescID, textMessageID, transaction);
                            else
                                transactionGroupID = Convert.ToInt16(tranGroup.UssdTransactionGroupID);

                                logInfo += "|A";
                                busTran = GetBusinessTransaction(transactionGroupID);
                            if (!recordFound)
                                InsertBusinessTransaction(thirdPartyID, transactionGroupID);

                                logInfo += "|B";
                                actions = GetUssdTransactionAction(transactionGroupID);
                            itemExist = false;
                            itemExist = actions.Any(x => x.UssdActionID == actionGasOrder.UssdActionID);
                            if (!itemExist)
                                InsertUssdTransactionAction(transactionGroupID, (Int16)actionGasOrder.UssdActionID, 1);
                            itemExist = actions.Any(x => x.UssdActionID == actionMessage.UssdActionID);
                            if (!itemExist)
                                InsertUssdTransactionAction(transactionGroupID, (Int16)actionMessage.UssdActionID, 2);
                            itemExist = actions.Any(x => x.UssdActionID == actionVending.UssdActionID);
                            if (!itemExist)
                                InsertUssdTransactionAction(transactionGroupID, (Int16)actionVending.UssdActionID, 3);
                                logInfo += "|C";
                                ussdTransactionID = 0;
                            tran = GetUssdTransactionMS(transaction);
                            productDesc = "Tswana Gas Sale. City Name: " + city.tbltswanagasentitycode.EntityName + "; Centre Name: " + centre.tbltswanagasentitycode.EntityName + "; Cylinder Size: " + cylinder.tbltswanagasentitycode.EntityName + "; Pay On Delivery; With Empty; ";
                            if (!recordFound)
                                ussdTransactionID = InsertUssdTransactionMS(transaction, 0, "", "", productDesc, thirdParty.TerminalID);
                            else
                            {
                                Boolean doUpdate = false;
                                if ((Decimal)tran.TransactionAmount != (Decimal)centreCylinder.CylinderPrice) doUpdate = true;
                                if (tran.ProductDescription != productDesc) doUpdate = true;
                                if (doUpdate)
                                    UpdateUssdTransaction(Convert.ToInt16(tran.UssdTransactionID), (Decimal)centreCylinder.CylinderPrice, productDesc);

                                ussdTransactionID = Convert.ToInt16(tran.UssdTransactionID);
                            }

                                logInfo += "|D";
                                groupItem = GetUssdTransactionGroupItem(transactionGroupID, ussdTransactionID);
                            if (!recordFound)
                                InsertUssdTransactionGroupItem(transactionGroupID, ussdTransactionID);
                            #endregion
                            #region Without Empty. Pay On Delivery
                            productLookup = "Tswana Gas-" + city.tbltswanagasentitycode.EntityName + ";" + centre.tbltswanagasentitycode.EntityName + ";" + cylinder.tbltswanagasentitycode.EntityName.Replace(" ", "") + ";PayDelivery;WithoutEmpty";
                                logInfo += "|" + productLookup;
                                transaction = city.tbltswanagasentitycode.EntityCode + centre.tbltswanagasentitycode.EntityCode + cylinder.tbltswanagasentitycode.EntityCode + "NN";

                            tranGroup = GetUssdTransactionGroup(productLookup);
                            transactionGroupID = 0;

                            if (!recordFound)
                                transactionGroupID = InsertUssdTransactionGroup(productLookup, tranDescID, textMessageID, transaction);
                            else
                                transactionGroupID = Convert.ToInt16(tranGroup.UssdTransactionGroupID);


                            busTran = GetBusinessTransaction(transactionGroupID);
                            if (!recordFound)
                                InsertBusinessTransaction(thirdPartyID, transactionGroupID);

                            actions = GetUssdTransactionAction(transactionGroupID);
                            itemExist = false;
                            itemExist = actions.Any(x => x.UssdActionID == actionGasOrder.UssdActionID);
                            if (!itemExist)
                                InsertUssdTransactionAction(transactionGroupID, (Int16)actionGasOrder.UssdActionID, 1);
                            itemExist = actions.Any(x => x.UssdActionID == actionMessage.UssdActionID);
                            if (!itemExist)
                                InsertUssdTransactionAction(transactionGroupID, (Int16)actionMessage.UssdActionID, 2);
                            itemExist = actions.Any(x => x.UssdActionID == actionVending.UssdActionID);
                            if (!itemExist)
                                InsertUssdTransactionAction(transactionGroupID, (Int16)actionVending.UssdActionID, 3);

                            ussdTransactionID = 0;
                            tran = GetUssdTransactionMS(transaction);
                            productDesc = "Tswana Gas Sale. City Name: " + city.tbltswanagasentitycode.EntityName + "; Centre Name: " + centre.tbltswanagasentitycode.EntityName + "; Cylinder Size: " + cylinder.tbltswanagasentitycode.EntityName + ";Pay On Delivery; Without Empty";
                            if (!recordFound)
                                ussdTransactionID = InsertUssdTransactionMS(transaction, 0, "", "", productDesc, thirdParty.TerminalID);
                            else
                            {
                                Boolean doUpdate = false;
                                if ((Decimal)tran.TransactionAmount != (Decimal)centreCylinder.CylinderPrice) doUpdate = true;
                                if (tran.ProductDescription != productDesc) doUpdate = true;
                                if (doUpdate)
                                    UpdateUssdTransaction(Convert.ToInt16(tran.UssdTransactionID), (Decimal)centreCylinder.CylinderPrice, productDesc);

                                ussdTransactionID = Convert.ToInt16(tran.UssdTransactionID);
                            }

                            groupItem = GetUssdTransactionGroupItem(transactionGroupID, ussdTransactionID);
                            if (!recordFound)
                                InsertUssdTransactionGroupItem(transactionGroupID, ussdTransactionID);
                            #endregion
                        }
                    }
                }
            }

            return true;
            }
            catch (Exception Ex)
            {
                Console.WriteLine(logInfo); Console.ReadKey();
                throw new Exception("TswanaGasUssdTablePopulate - " + Ex.Message + logInfo);
            }
        }

        public tbltswanagascentrecylinder GetTswanaGasCentreCylinder(Int32 pCentreID, Int32 pCylinderID)
        {
            try
            {
                tbltswanagascentrecylinderCrud crud = new tbltswanagascentrecylinderCrud();
                crud.Where("CentreID", DAL.General.Operator.Equals, pCentreID);
                crud.And("CylinderID", DAL.General.Operator.Equals, pCylinderID);
                recordFound = false;
                tbltswanagascentrecylinder cenCyl = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return cenCyl;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentreCylinder - " + Ex.Message);
            }
        }

        public List<tbltswanagascentrecylinder> GetTswanaGasCentreCylinder(Int32 pCentreID)
        {
            try
            {
                tbltswanagascentrecylinderCrud crud = new tbltswanagascentrecylinderCrud();
                crud.Where("CentreID", DAL.General.Operator.Equals, pCentreID);
                recordFound = false;
                List<tbltswanagascentrecylinder> cenCyl = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;

                return cenCyl;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentreCylinder - " + Ex.Message);
            }
        }

        public Boolean TswanaGasUssdTablePopulateMS()
        {
            //try
            //{

            Int16 parentGroupID = 0;
            Int16 groupID = 0;
            String welcomMessage = "";
            Byte selectionNo = 0;

            List<tbltswanagascity> tswanaCities = GetTswanaGasCityMS();
            foreach (tbltswanagascity tswanaCity in tswanaCities)
            {
                DPO.BackOffice.MySQL.DAL.tblussdmenugroup parentGroup = GetUssdMenuGroup("Tswana Gas");
                parentGroupID = Convert.ToInt16(parentGroup.GroupID);
                tblussdmenuitemtext menuItemText = GetUssdMenuItemTextMS(tswanaCity.CityName);
                Int16 menuItemTextID = 0;
                if (!recordFound)
                {
                    menuItemTextID = InsertUssdMenuItemTextMS(tswanaCity.CityName);
                }
                else
                    menuItemTextID = Convert.ToInt16(menuItemText.MenuItemTextID);

                tblussdmenugroupitem getGroupItem = GetUssdMenuGroupItem(Convert.ToInt32(parentGroup.GroupID), menuItemTextID);
                if (!recordFound)
                {
                    selectionNo = Convert.ToByte(tswanaCity.SelectionNo);
                    welcomMessage = "Please select a distribution centre.";
                    Int16 welcomeMessageID = Convert.ToInt16(GetUssdMenuWelcomeMessage(welcomMessage).WelcomeMessageID);

                    groupID = InsertUssdMenuGroup(tswanaCity.CityName, menuItemTextID, welcomeMessageID, selectionNo, parentGroupID, null, null);
                    InsertUssdGroupMenuItem(parentGroupID, menuItemTextID, selectionNo, parentGroupID);

                }
                else
                    groupID = Convert.ToInt16(getGroupItem.GroupID);



                parentGroupID = Convert.ToInt16(GetUssdMenuGroup(tswanaCity.CityName).GroupID);
                List<tbltswanagascentre> tswanaCentres = GetTswanaGasCentreMS(Convert.ToInt32(tswanaCity.CityID));
                foreach (tbltswanagascentre tswanaCentre in tswanaCentres)
                {
                    menuItemText = GetUssdMenuItemTextMS(tswanaCentre.CentreName);
                    menuItemTextID = 0;
                    if (!recordFound)
                    {
                        menuItemTextID = InsertUssdMenuItemTextMS(tswanaCentre.CentreName);
                    }
                    else
                        menuItemTextID = Convert.ToInt16(menuItemText.MenuItemTextID);

                    tblussdmenugroup ussdMenuGroup = GetUssdMenuGroup(tswanaCity.CityName + "-" + tswanaCentre.CentreName);
                    if (recordFound)
                        groupID = Convert.ToInt16(ussdMenuGroup.ParentGroupID);

                    getGroupItem = GetUssdMenuGroupItem(groupID, menuItemTextID);
                    if (!recordFound)
                    {
                        selectionNo = Convert.ToByte(tswanaCentre.SelectionNo);
                        welcomMessage = "Please select a gas cylinder.";
                        Int16 welcomeMessageID = Convert.ToInt16(GetUssdMenuWelcomeMessage(welcomMessage).WelcomeMessageID);

                        groupID = InsertUssdMenuGroup(tswanaCity.CityName + "-" + tswanaCentre.CentreName, menuItemTextID, welcomeMessageID, selectionNo, parentGroupID, null, null);
                        InsertUssdGroupMenuItem(parentGroupID, menuItemTextID, selectionNo, parentGroupID);

                    }

                    parentGroupID = groupID;
                    
                    List<tbltswanagascentrecylinder> tswanaCylinders = GetTswanaGasCentreCylinder(Convert.ToInt32(tswanaCentre.CentreID));
                    foreach (tbltswanagascentrecylinder tswanaCylinder in tswanaCylinders)
                    {
                        menuItemText = GetUssdMenuItemTextMS(tswanaCylinder.tbltswanagascylinder.CylinderName);
                        menuItemTextID = 0;
                        if (!recordFound)
                        {
                            menuItemTextID = InsertUssdMenuItemTextMS(tswanaCylinder.tbltswanagascylinder.CylinderName);
                        }
                        else
                            menuItemTextID = Convert.ToInt16(menuItemText.MenuItemTextID);

                        ussdMenuGroup = GetUssdMenuGroup(tswanaCity.CityName + "-" + tswanaCentre.CentreName + "-" + tswanaCylinder.tbltswanagascylinder.CylinderName);
                        if (recordFound)
                            groupID = Convert.ToInt16(ussdMenuGroup.ParentGroupID);

                        getGroupItem = GetUssdMenuGroupItem(groupID, menuItemTextID);
                        if (!recordFound)
                        {
                            selectionNo = Convert.ToByte(tswanaCylinder.SelectionNo);
                            welcomMessage = "Do you have an empty cylinder?";
                            Int16 welcomeMessageID = Convert.ToInt16(GetUssdMenuWelcomeMessage(welcomMessage).WelcomeMessageID);

                            groupID = InsertUssdMenuGroup(tswanaCity.CityName + "-" + tswanaCentre.CentreName + "-" + tswanaCylinder.tbltswanagascylinder.CylinderName, menuItemTextID, welcomeMessageID, selectionNo, parentGroupID, null, null);
                            InsertUssdGroupMenuItem(parentGroupID, menuItemTextID, selectionNo, parentGroupID);

                            selectionNo = 1;
                            menuItemTextID = Convert.ToInt16(GetUssdMenuItemTextMS("Yes").MenuItemTextID);
                            InsertUssdMenuGroup(tswanaCity.CityName + "-" + tswanaCentre.CentreName + "-" + tswanaCylinder.tbltswanagascylinder.CylinderName + "-EmptyYes", menuItemTextID, 7, selectionNo, groupID, null, null);
                            InsertUssdGroupMenuItem(groupID, menuItemTextID, selectionNo, groupID);

                            selectionNo = 2;
                            menuItemTextID = Convert.ToInt16(GetUssdMenuItemTextMS("No").MenuItemTextID);
                            InsertUssdMenuGroup(tswanaCity.CityName + "-" + tswanaCentre.CentreName + "-" + tswanaCylinder.tbltswanagascylinder.CylinderName + "-EmptyNo", menuItemTextID, 7, selectionNo, groupID, null, null);
                            InsertUssdGroupMenuItem(groupID, menuItemTextID, selectionNo, groupID);
                        }

                    }

                    foreach (tbltswanagascentrecylinder tswanaCylinder in tswanaCylinders)
                    {

                        parentGroupID = Convert.ToInt16(GetUssdMenuGroup(tswanaCity.CityName + "-" + tswanaCentre.CentreName + "-" + tswanaCylinder.tbltswanagascylinder.CylinderName + "-EmptyYes").GroupID);

                        #region Pay Now
                        String ussdTransactionName = tswanaCity.tbltswanagasentitycode.EntityCode + tswanaCentre.tbltswanagasentitycode.EntityCode + tswanaCylinder.tbltswanagascylinder.tbltswanagasentitycode.EntityCode + "EmptyYesNow";
                        String ussdFunction = tswanaCity.tbltswanagasentitycode.EntityCode + tswanaCentre.tbltswanagasentitycode.EntityCode + tswanaCylinder.tbltswanagascylinder.tbltswanagasentitycode.EntityCode + "NY";
                        tblussdtransaction ussdTransaction = GetUssdTransactionMS(ussdTransactionName);
                        Int16 ussdTransactionID = 0;
                        if (!recordFound)
                        {

                            ussdTransactionID = InsertUssdTransactionMS(ussdTransactionName, Convert.ToDecimal(tswanaCylinder.CylinderPrice), "");
                        }
                        else
                            ussdTransactionID = Convert.ToInt16(ussdTransaction.UssdTransactionID);

                        tblussdtransactiongroup ussdTransactionGroup = GetUssdTransactionGroup(ussdTransactionName);
                        Int16 ussdTransactionGroupID = 0;
                        if (!recordFound)
                        {

                            ussdTransactionGroupID = InsertUssdTransactionGroup(ussdTransactionName, 4, 8, ussdFunction);
                        }
                        else
                            ussdTransactionGroupID = Convert.ToInt16(ussdTransactionGroup.UssdTransactionGroupID);

                        List<tblussdtransactiongroupitem> ussdTransactionGroupItem = GetUssdTransactionGroupItem(ussdTransactionGroupID);
                        if (!recordFound)
                        {
                            InsertUssdTransactionGroupItem(ussdTransactionGroupID, ussdTransactionID);
                        }

                        tblbusinesstransaction businessTransaction = GetBusinessTransaction(ussdTransactionGroupID);
                        if (!recordFound)
                        {
                            InsertBusinessTransaction(3, ussdTransactionGroupID);
                        }

                        List<tblussdtransactionaction> ussdTransactionAction = GetUssdTransactionAction(ussdTransactionGroupID);
                        if (!recordFound)
                        {
                            InsertUssdTransactionAction(ussdTransactionGroupID, 1, 1);
                            InsertUssdTransactionAction(ussdTransactionGroupID, 6, 2);
                            InsertUssdTransactionAction(ussdTransactionGroupID, 3, 3);
                        }
                        Int16 instructionGroupID = Convert.ToInt16(GetUssdInstructionGroup("Standard bank card information").InstructionGroupID);
                        menuItemTextID = Convert.ToInt16(GetUssdMenuItemTextMS("Pay Now").MenuItemTextID);
                        welcomMessage = "Pay now or on delivery.";
                        Int16 welcomeMessageID = Convert.ToInt16(GetUssdMenuWelcomeMessage(welcomMessage).WelcomeMessageID);

                        InsertUssdMenuGroup(tswanaCity.CityName + "-" + tswanaCentre.CentreName + "-" + tswanaCylinder.tbltswanagascylinder.CylinderName + "-EmptyYes-PayNow", menuItemTextID, welcomeMessageID, 1, parentGroupID, instructionGroupID, ussdTransactionGroupID);
                        ussdFunction = tswanaCity.tbltswanagasentitycode.EntityCode + tswanaCentre.tbltswanagasentitycode.EntityCode + tswanaCylinder.tbltswanagascylinder.tbltswanagasentitycode.EntityCode + "YY";
                        InsertUssdGroupMenuItem(parentGroupID, menuItemTextID, 1, parentGroupID);
                        #endregion

                        #region Pay On Delivery
                        ussdTransactionName = tswanaCity.tbltswanagasentitycode.EntityCode + tswanaCentre.tbltswanagasentitycode.EntityCode + tswanaCylinder.tbltswanagascylinder.tbltswanagasentitycode.EntityCode + "EmptyYesDelivery";
                        ussdTransaction = GetUssdTransactionMS(ussdTransactionName);
                        ussdTransactionID = 0;
                        if (!recordFound)
                        {

                            ussdTransactionID = InsertUssdTransactionMS(ussdTransactionName, Convert.ToDecimal(tswanaCylinder.CylinderPrice), "");
                        }
                        else
                            ussdTransactionID = Convert.ToInt16(ussdTransaction.UssdTransactionID);

                        ussdTransactionGroup = GetUssdTransactionGroup(ussdTransactionName);
                        ussdTransactionGroupID = 0;
                        if (!recordFound)
                        {

                            ussdTransactionGroupID = InsertUssdTransactionGroup(ussdTransactionName, 4, 7, ussdFunction);
                        }
                        else
                            ussdTransactionGroupID = Convert.ToInt16(ussdTransactionGroup.UssdTransactionGroupID);

                        ussdTransactionGroupItem = GetUssdTransactionGroupItem(ussdTransactionGroupID);
                        if (!recordFound)
                        {
                            InsertUssdTransactionGroupItem(ussdTransactionGroupID, ussdTransactionID);
                        }

                        businessTransaction = GetBusinessTransaction(ussdTransactionGroupID);
                        if (!recordFound)
                        {
                            InsertBusinessTransaction(3, ussdTransactionGroupID);
                        }

                        ussdTransactionAction = GetUssdTransactionAction(ussdTransactionGroupID);
                        if (!recordFound)
                        {
                            InsertUssdTransactionAction(ussdTransactionGroupID, 6, 1);
                            InsertUssdTransactionAction(ussdTransactionGroupID, 3, 2);
                        }

                        menuItemTextID = Convert.ToInt16(GetUssdMenuItemTextMS("Pay On Delivery").MenuItemTextID);
                        InsertUssdMenuGroup(tswanaCity.CityName + "-" + tswanaCentre.CentreName + "-" + tswanaCylinder.tbltswanagascylinder.CylinderName + "-EmptyYes-PayOnDelivery", menuItemTextID, welcomeMessageID, 2, parentGroupID, instructionGroupID, ussdTransactionGroupID);
                        InsertUssdGroupMenuItem(parentGroupID, menuItemTextID, 2, parentGroupID);
                        #endregion

                        parentGroupID = Convert.ToInt16(GetUssdMenuGroup(tswanaCity.CityName + "-" + tswanaCentre.CentreName + "-" + tswanaCylinder.tbltswanagascylinder.CylinderName + "-EmptyNo").GroupID);

                        #region Pay Now - No Empty
                        ussdTransactionName = tswanaCity.tbltswanagasentitycode.EntityCode + tswanaCentre.tbltswanagasentitycode.EntityCode + tswanaCylinder.tbltswanagascylinder.tbltswanagasentitycode.EntityCode + "EmptyNoNow";
                        ussdFunction = tswanaCity.tbltswanagasentitycode.EntityCode + tswanaCentre.tbltswanagasentitycode.EntityCode + tswanaCylinder.tbltswanagascylinder.tbltswanagasentitycode.EntityCode + "NY";
                        ussdTransaction = GetUssdTransactionMS(ussdTransactionName);
                        ussdTransactionID = 0;
                        if (!recordFound)
                        {

                            ussdTransactionID = InsertUssdTransactionMS(ussdTransactionName, Convert.ToDecimal(tswanaCylinder.CylinderPrice), "");
                        }
                        else
                            ussdTransactionID = Convert.ToInt16(ussdTransaction.UssdTransactionID);

                        ussdTransactionGroup = GetUssdTransactionGroup(ussdTransactionName);
                        ussdTransactionGroupID = 0;
                        if (!recordFound)
                        {

                            ussdTransactionGroupID = InsertUssdTransactionGroup(ussdTransactionName, 4, 8, ussdFunction);
                        }
                        else
                            ussdTransactionGroupID = Convert.ToInt16(ussdTransactionGroup.UssdTransactionGroupID);

                        ussdTransactionGroupItem = GetUssdTransactionGroupItem(ussdTransactionGroupID);
                        if (!recordFound)
                        {
                            InsertUssdTransactionGroupItem(ussdTransactionGroupID, ussdTransactionID);
                        }

                        businessTransaction = GetBusinessTransaction(ussdTransactionGroupID);
                        if (!recordFound)
                        {
                            InsertBusinessTransaction(3, ussdTransactionGroupID);
                        }

                        ussdTransactionAction = GetUssdTransactionAction(ussdTransactionGroupID);
                        if (!recordFound)
                        {
                            InsertUssdTransactionAction(ussdTransactionGroupID, 1, 1);
                            InsertUssdTransactionAction(ussdTransactionGroupID, 6, 2);
                            InsertUssdTransactionAction(ussdTransactionGroupID, 3, 3);
                        }
                        instructionGroupID = Convert.ToInt16(GetUssdInstructionGroup("Standard bank card information").InstructionGroupID);
                        menuItemTextID = Convert.ToInt16(GetUssdMenuItemTextMS("Pay Now").MenuItemTextID);
                        welcomMessage = "Pay now or on delivery.";
                        welcomeMessageID = Convert.ToInt16(GetUssdMenuWelcomeMessage(welcomMessage).WelcomeMessageID);

                        InsertUssdMenuGroup(tswanaCity.CityName + "-" + tswanaCentre.CentreName + "-" + tswanaCylinder.tbltswanagascylinder.CylinderName + "-EmptyNo-PayNow", menuItemTextID, welcomeMessageID, 1, parentGroupID, instructionGroupID, ussdTransactionGroupID);
                        InsertUssdGroupMenuItem(parentGroupID, menuItemTextID, 1, parentGroupID);
                        #endregion

                        #region Pay On Delivery
                        ussdTransactionName = tswanaCity.tbltswanagasentitycode.EntityCode + tswanaCentre.tbltswanagasentitycode.EntityCode + tswanaCylinder.tbltswanagascylinder.tbltswanagasentitycode.EntityCode + "EmptyNoDelivery";
                        ussdFunction = tswanaCity.tbltswanagasentitycode.EntityCode + tswanaCentre.tbltswanagasentitycode.EntityCode + tswanaCylinder.tbltswanagascylinder.tbltswanagasentitycode.EntityCode + "NN";
                        ussdTransaction = GetUssdTransactionMS(ussdTransactionName);
                        ussdTransactionID = 0;
                        if (!recordFound)
                        {

                            ussdTransactionID = InsertUssdTransactionMS(ussdTransactionName, Convert.ToDecimal(tswanaCylinder.CylinderPrice), "");
                        }
                        else
                            ussdTransactionID = Convert.ToInt16(ussdTransaction.UssdTransactionID);

                        ussdTransactionGroup = GetUssdTransactionGroup(ussdTransactionName);
                        ussdTransactionGroupID = 0;
                        if (!recordFound)
                        {

                            ussdTransactionGroupID = InsertUssdTransactionGroup(ussdTransactionName, 4, 7, ussdFunction);
                        }
                        else
                            ussdTransactionGroupID = Convert.ToInt16(ussdTransactionGroup.UssdTransactionGroupID);

                        ussdTransactionGroupItem = GetUssdTransactionGroupItem(ussdTransactionGroupID);
                        if (!recordFound)
                        {
                            InsertUssdTransactionGroupItem(ussdTransactionGroupID, ussdTransactionID);
                        }

                        businessTransaction = GetBusinessTransaction(ussdTransactionGroupID);
                        if (!recordFound)
                        {
                            InsertBusinessTransaction(3, ussdTransactionGroupID);
                        }

                        ussdTransactionAction = GetUssdTransactionAction(ussdTransactionGroupID);
                        if (!recordFound)
                        {
                            InsertUssdTransactionAction(ussdTransactionGroupID, 6, 1);
                            InsertUssdTransactionAction(ussdTransactionGroupID, 3, 2);
                        }

                        menuItemTextID = Convert.ToInt16(GetUssdMenuItemTextMS("Pay On Delivery").MenuItemTextID);
                        InsertUssdMenuGroup(tswanaCity.CityName + "-" + tswanaCentre.CentreName + "-" + tswanaCylinder.tbltswanagascylinder.CylinderName + "-EmptyNo-PayOnDelivery", menuItemTextID, welcomeMessageID, 2, parentGroupID, instructionGroupID, ussdTransactionGroupID);
                        InsertUssdGroupMenuItem(parentGroupID, menuItemTextID, 2, parentGroupID);
                        #endregion
                    }
                }
            }

            #region Update The Selection Order
            List<tbltswanagascity> cities = GetTswanaGasCityMS();
            cities = cities.OrderBy(x => x.CityName).ToList();
            selectionNo = 1;
            foreach (tbltswanagascity city in cities)
            {
                UpdateTswanaGasCityMS((Int32)city.CityID, selectionNo);
                selectionNo++;
                List<tbltswanagascentre> centres = GetTswanaGasCentreMS((Int32)city.CityID);
                centres = centres.OrderBy(x => x.CentreName).ToList();
                Byte centreSelectionNo = 1;
                foreach (tbltswanagascentre centre in centres)
                {
                    UpdateTswanaGasCentreSelectionNo((Int32)centre.CentreID, centreSelectionNo);
                    centreSelectionNo++;
                }
            }

            //UpdateTswanaGasCylinderMS
            #endregion
            return true;
            //}
            //catch (Exception Ex)
            //{
            //    throw new Exception("TswanaGasUssdTablePopulateMS - " + Ex.Message);
            // }
        }

        public List<TswanaGasCylinder> GetTswanaGasCylinderJSON()
        {
            try
            {
                tblsetting appSetting = GetSetting("Back Office", "DPO.BackOffice.Ussd", "CylinderUrl");
                String pUrl = appSetting.SettingValue;
                List<TswanaGasCylinder> cylinders = new List<TswanaGasCylinder>();
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(pUrl);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                String result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                if (!result.Contains("[") && !result.Contains("]"))
                    throw new Exception(result);


                result = result.Replace("[", "").Replace("]", "").Replace("\"id\":", "").Replace("\"name\":", "").Replace("\"", "").Replace("price:", "");
                String[] cyls = result.Split('}');

                for (Int32 citLoop = 0; citLoop < cyls.GetUpperBound(0); citLoop++)
                {
                    cyls[citLoop] = cyls[citLoop].Replace(",{", "").Replace("{", "");
                }

                for (Int32 cylLoop = 0; cylLoop < cyls.GetUpperBound(0); cylLoop++)
                {
                    if (cyls[cylLoop] != "")
                    {
                        String[] workCyl = cyls[cylLoop].Split(',');
                        Int32 cylID = Convert.ToInt32(workCyl[0]);
                        String cylName = workCyl[1];
                        //Decimal cylPrice = Convert.ToDecimal(workCyl[2]);
                        TswanaGasCylinder cyl = new TswanaGasCylinder();
                        cyl.CylinderID = cylID;
                        cyl.CylinderName = cylName;
                        //cyl.CylinderPrice = cylPrice;
                        cylinders.Add(cyl);
                    }
                }
                return cylinders;
            }
            catch (System.Net.WebException webEx)
            {
                if (webEx.Response != null)
                {
                    var resp = new System.IO.StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
                    dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject(resp);
                    String messageFromServer = obj.error.ToString();

                    throw new Exception("Error=" + messageFromServer);
                }
                else
                    throw new Exception("Error=" + webEx.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCylinder-" + Ex.Message);
            }

        }
        public Boolean TswanaGasDataCollectMS()
        {
            String infoLog = "";
            //try
            //{
            #region Vairable Declaration
            tbltswanagasentitycode code = new tbltswanagasentitycode();
            String entityCode = "";
            Int32 entityCodeID = 0;
            Int32 codeCount = 0;
            #endregion

            #region Cylinders
            codeCount = 0;
            infoLog += "GetTswanaGasCylinderJSON";
            List<TswanaGasCylinder> cylinders = GetTswanaGasCylinderJSON();
            foreach (TswanaGasCylinder cylinder in cylinders)
            {
                infoLog += "|GetTswanaGasCylinder";
                tbltswanagascylinder dbCylinder = GetTswanaGasCylinderMS(cylinder.CylinderID);
                if (!recordFound)
                {
                    code = GetTswanaGasEntityCodeMS(3, cylinder.CylinderName);
                    if (!recordFound)
                    {
                        entityCode = cylinder.CylinderName.Replace(" ", "").Substring(0, 3).ToUpper();
                        recordFound = true;
                        while (recordFound)
                        {
                            code = GetTswanaGasEntityCodeByCodeMS(3, entityCode);
                            if (recordFound)
                            {
                                codeCount++;
                                entityCode = cylinder.CylinderName.Replace(" ", "").Substring(codeCount, 3).ToUpper();
                            }
                        }

                        entityCodeID = InsertTswanaGasEntityCodeMS(3, cylinder.CylinderName, entityCode);
                    }
                    else
                        entityCodeID = (Int32)code.EntityCodeID;

                    InsertTswanaGasCylinderMS(cylinder.CylinderID, cylinder.CylinderName, entityCodeID);
                }


                List<tbltswanagascylinder> dbCylinders = GetTswanaGasCylinderMS();
                foreach (tbltswanagascylinder cyl in dbCylinders)
                {
                    List<TswanaGasCylinder> cylCheck = cylinders.FindAll(x => x.CylinderName == cyl.CylinderName);
                    if (cylCheck.Count == 0)
                        UpdateTswanaGasCylinderActive((Int32)cyl.CylinderID, false, 19634);
                    else
                        if (!(Boolean)cyl.Active)
                        UpdateTswanaGasCylinderActive((Int32)cyl.CylinderID, true, 19634);
                }
            }
            #endregion

            #region Cities
            List<TswanaGasCity> cities = GetTswanaGasCityJSON();
            foreach (TswanaGasCity city in cities)
            {
                tbltswanagascity dbCity = GetTswanaGasCityMS(city.CityID);
                if (!recordFound)
                {
                    code = GetTswanaGasEntityCodeMS(1, city.CityName);
                    if (!recordFound)
                    {
                        entityCode = city.CityName.Substring(0, 3).ToUpper();
                        recordFound = true;
                        while (recordFound)
                        {
                            code = GetTswanaGasEntityCodeByCodeMS(1, entityCode);
                            if (recordFound)
                            {
                                codeCount++;
                                entityCode = city.CityName.Substring(codeCount, 3).ToUpper();
                            }
                        }

                        entityCodeID = InsertTswanaGasEntityCodeMS(1, city.CityName, entityCode);
                    }
                    else
                        entityCodeID = (Int32)code.EntityCodeID;

                    InsertTswanaGasCityMS(city.CityID, city.CityName, entityCodeID, 19634);
                }
                else
                {
                    if (dbCity.CityName != city.CityName)
                    {
                        UpdateTswanaGasCity(city.CityID, city.CityName, 19634);
                    }
                }
                #endregion

                #region Distribution Centres
                codeCount = 0;
                List<TswanaGasCentre> centres = GetTswanaGasCentreJSON(city.CityID);
                if (centres.Count == 0)
                {
                    UpdateTswanaGasCityActiveMS(city.CityID, false, 19634);
                    List<tbltswanagascentre> cenAct = GetTswanaGasCentreMS(city.CityID);
                    foreach (tbltswanagascentre cen in cenAct)
                        UpdateTswanaGasCentreMS((Int32)cen.CentreID, false, 19634);

                }
                else
                {
                    if (dbCity != null)
                    {
                        if (!(Boolean)dbCity.Active)
                        {
                            UpdateTswanaGasCityActiveMS(city.CityID, true, 19634);
                        }
                    }
                }

                infoLog = "Centres Found=" + centres.Count.ToString();
                foreach (TswanaGasCentre centre in centres)
                {
                    infoLog += "| WorkingWith=" + centre.CentreName;
                    tbltswanagascentre dbCentre = GetTswanaGasCentreByIDMS(centre.CentreID);
                    Int32 centreID = 0;
                    if (!recordFound)
                    {
                        infoLog += "|CentreNotFound";
                        code = GetTswanaGasEntityCodeMS(2, centre.CentreName);
                        if (!recordFound)
                        {
                            infoLog += "|EntityCodeNotFound";
                            entityCode = centre.CentreName.Substring(0, 3).ToUpper();
                            recordFound = true;
                            while (recordFound)
                            {
                                code = GetTswanaGasEntityCodeByCodeMS(2, entityCode);
                                if (recordFound)
                                {
                                    infoLog += "|EntityCodeAlreadyThere";
                                    codeCount++;
                                    entityCode = centre.CentreName.Substring(codeCount, 3).ToUpper();
                                }
                            }
                            infoLog += "|InsertEntityCode=" + entityCode;
                            entityCodeID = InsertTswanaGasEntityCodeMS(2, centre.CentreName, entityCode);
                        }
                        else
                            entityCodeID = (Int32)code.EntityCodeID;

                        infoLog += "|CityID=" + city.CityID.ToString() + "| CentreID=" + centre.CentreID + "| CentreName=" + centre.CentreName + "| EntityCodeID=" + entityCodeID.ToString();
                        InsertTswanaGasCentreMS(city.CityID, centre.CentreID, centre.CentreName, entityCodeID, 19634);




                    }
                    else
                    {
                        centreID = Convert.ToInt32(dbCentre.CentreID);
                        if (dbCentre.CentreName != centre.CentreName)
                            UpdateTswanaGasCentreMS(centre.CentreID, centre.CentreName);

                        if ((Int32)dbCentre.CityID != (Int32)city.CityID)
                            UpdateTswanaGasCentreMS(centre.CentreID, city.CityID);
                    }

                    tbltswanagascentrecylinder cenCyl = GetTswanaGasCentreCylinderMS(centre.CentreID, centre.CylinderID);
                    if (!recordFound)
                        InsertTswanaGasCentreCylinderMS(centre.CentreID, centre.CylinderID, centre.CylinderPrice);
                    else
                        UpdateTswanaGasCentreCylinderMS(centre.CentreID, centre.CylinderID, centre.CylinderPrice);

                }
                if (dbCity != null)
                {
                    List<tbltswanagascentre> dbCentres = GetTswanaGasCentreMS((Int32)dbCity.CityID);
                    foreach (tbltswanagascentre workCentre in dbCentres)
                    {
                        List<TswanaGasCentre> thisCentre = centres.FindAll(x => x.CentreName == workCentre.CentreName);
                        if (thisCentre.Count == 0)
                            UpdateTswanaGasCentreMS((Int32)workCentre.CentreID, false, 19634);
                        else
                            UpdateTswanaGasCentreMS((Int32)workCentre.CentreID, true, 19634);
                    }
                }
            }

            List<tbltswanagascity> dbCities = GetTswanaGasCityMS();
            foreach (tbltswanagascity workCity in dbCities)
            {
                List<TswanaGasCity> findCity = cities.FindAll(x => x.CityName == workCity.CityName);
                if (findCity.Count == 0)
                    UpdateTswanaGasCityActiveMS((Int32)workCity.CityID, false, 19634);
                else
                    if (!(Boolean)workCity.Active)
                    UpdateTswanaGasCityActiveMS((Int32)workCity.CityID, true, 19634);
            }
            #endregion
            return true;
            //}
            //catch (Exception Ex)
            //{
            //    throw new Exception("TswanaGasDataCollect - " + Ex.Message + "| InfoLog=" + infoLog);
            //}
        }
        public List<TswanaGasCity> GetTswanaGasCityJSON()
        {
            try
            {
                tblsetting appSetting = GetSetting("Back Office", "DPO.BackOffice.Ussd", "CityURL");
                String pUrl = appSetting.SettingValue;
                List<TswanaGasCity> cities = new List<TswanaGasCity>();
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(pUrl);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                String result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                result = result.Replace("[", "").Replace("]", "").Replace("\"id\":", "").Replace("\"name\":", "").Replace("\"", "");
                String[] cits = result.Split('}');

                for (Int32 citLoop = 0; citLoop < cits.GetUpperBound(0); citLoop++)
                {
                    cits[citLoop] = cits[citLoop].Replace(",{", "").Replace("{", "");
                }

                for (Int32 citLoop = 0; citLoop < cits.GetUpperBound(0); citLoop++)
                {
                    if (cits[citLoop] != "")
                    {
                        String[] workCity = cits[citLoop].Split(',');
                        Int32 cityID = Convert.ToInt32(workCity[0]);
                        String cityName = workCity[1];
                        TswanaGasCity city = new TswanaGasCity();
                        city.CityID = cityID;
                        city.CityName = cityName;
                        cities.Add(city);
                    }
                }


                return cities;
            }
            catch (System.Net.WebException webEx)
            {
                if (webEx.Response != null)
                {
                    var resp = new System.IO.StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
                    dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject(resp);
                    String messageFromServer = obj.error.ToString();

                    throw new Exception("Error=" + messageFromServer);
                }
                else
                    throw new Exception("Error=" + webEx.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCity-" + Ex.Message);
            }

        }
        public Boolean UpdateTswanaGasCity(Int32 pCityID, String pCityName, Int32 pUserID)
        {
            try
            {
                tbltswanagascity city = GetTswanaGasCityMS(pCityID);
                if (!recordFound)
                {
                    city.CityName = pCityName;
                    tbltswanagascityCrud crud = new tbltswanagascityCrud();
                    crud.Update(city);
                }

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateTswanaGasCity - " + Ex.Message);
            }
        }
        public Boolean UpdateTswanaGasCylinderActive(Int32 pCylinderID, Boolean pActive, Int32 pUserID)
        {
            try
            {
                tbltswanagascylinder cyl = GetTswanaGasCylinderMS(pCylinderID);
                cyl.Active = pActive;
                tbltswanagascylinderCrud crud = new tbltswanagascylinderCrud();
                crud.Update(cyl);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateTswanaGasCylinder - " + Ex.Message);
            }
        }
        public tbltswanagasentitycode GetTswanaGasEntityCodeMS(Int32 pEnityTypeID, String pEntityName)
        {
            try
            {
                tbltswanagasentitycodeCrud crud = new tbltswanagasentitycodeCrud();
                crud.Where("EntityTypeID", MySQL.DAL.General.Operator.Equals, pEnityTypeID);
                crud.And("EntityName", MySQL.DAL.General.Operator.Equals, pEntityName);
                recordFound = false;
                tbltswanagasentitycode code = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return code;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasEntityCodeMS - " + Ex.Message);
            }
        }
        public tbltswanagascentre GetTswanaGasCentreByCentreIDMS(Int32 pCentreID)
        {
            try
            {
                tbltswanagascentreCrud crud = new tbltswanagascentreCrud();
                crud.Where("CentreID", MySQL.DAL.General.Operator.Equals, pCentreID);
                recordFound = false;
                tbltswanagascentre result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentre - " + Ex.Message);
            }
        }
        public Boolean UpdateTswanaGasCentreSelectionNo(Int32 pCentreID, Byte pSelectionNo)
        {
            try
            {
                tbltswanagascentre centre = GetTswanaGasCentreByCentreIDMS(pCentreID);
                centre.SelectionNo = pSelectionNo;
                tbltswanagascentreCrud crud = new tbltswanagascentreCrud();
                crud.Update(centre);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateTswanaGasCentre - " + Ex.Message);
            }

        }

        public tbltswanagascentre GetTswanaGasCentreByIDMS(Int32 pCentreID)
        {
            try
            {
                tbltswanagascentreCrud crud = new tbltswanagascentreCrud();
                crud.Where("CentreID", MySQL.DAL.General.Operator.Equals, pCentreID);
                recordFound = false;
                tbltswanagascentre results = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return results;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentre-" + Ex.Message);
            }
        }

        public Boolean UpdateTswanaGasCentreMS(Int32 pCentreID, Boolean pActive, Int32 pUserID)
        {
            try
            {
                tbltswanagascentre centre = GetTswanaGasCentreByCentreIDMS(pCentreID);
                centre.Active = pActive;
                tbltswanagascentreCrud crud = new tbltswanagascentreCrud();
                crud.Update(centre);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateTswanaGasCentre - " + Ex.Message);
            }
        }
        public Boolean UpdateTswanaGasCentreMS(Int32 pCentreID, Int32 pCity)
        {
            try
            {
                tbltswanagascentre centre = GetTswanaGasCentreByCentreIDMS(pCentreID);
                centre.CityID = pCity;
                tbltswanagascentreCrud crud = new tbltswanagascentreCrud();
                crud.Update(centre);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateTswanaGasCentre - " + Ex.Message);
            }

        }
        public Boolean InsertTswanaGasCityMS(Int32 pCityID, String pCityName, Int32 pEntityCodeID, Int32 pUserID)
        {
            try
            {
                tbltswanagascityCrud crud = new tbltswanagascityCrud();
                tbltswanagascity city = new tbltswanagascity();
                city.CityID = pCityID;
                city.CityName = pCityName;
                city.EntityCodeID = pEntityCodeID;
                city.Active = true;
                crud.Insert(city);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTswanaGasCityMS -" + Ex.Message);
            }
        }
        public Boolean InsertTswanaGasCentreMS(Int32 pCityID, Int32 pCentreID, String pCentreName, Int32 pEntityCodeID, Int32 pUserID)
        {
            try
            {
                tbltswanagascentre centre = new tbltswanagascentre();
                centre.CentreID = pCentreID;
                centre.CentreName = pCentreName;
                centre.CityID = pCityID;
                centre.EntityCodeID = pEntityCodeID;
                centre.Active = true;
                tbltswanagascentreCrud crud = new tbltswanagascentreCrud();
                //long ID = 0;
                crud.Insert(centre);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTswanaGasCentreMS -" + Ex.Message);
            }
        }

        public Boolean UpdateTswanaGasCentreCylinderMS(Int32 pCentreID, Int32 pCylinderID, Decimal pCylinderPrice)
        {
            try
            {
                tbltswanagascentrecylinder cenCyl = GetTswanaGasCentreCylinderMS(pCentreID, pCylinderID);
                cenCyl.CylinderPrice = pCylinderPrice;
                tbltswanagascentrecylinderCrud crud = new tbltswanagascentrecylinderCrud();
                crud.Update(cenCyl);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateTswanaGasCentreCylinderMS - " + Ex.Message);
            }
        }

        public Boolean InsertTswanaGasCylinderMS(Int32 pCylinderID, String pCylinderName, Int32 pEntityCodeID)
        {
            try
            {
                tbltswanagascylinder cyl = new tbltswanagascylinder();
                cyl.CylinderID = pCylinderID;
                cyl.CylinderName = pCylinderName;
                cyl.EntityCodeID = pEntityCodeID;
                cyl.Active = true;
                tbltswanagascylinderCrud crud = new tbltswanagascylinderCrud();
                crud.Insert(cyl);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTswanaGasCylinder - " + Ex.Message);
            }
        }
        public Int32 InsertTswanaGasEntityCodeMS(Byte pEntityTypeID, String pEntityName, String pEntityCode)
        {
            try
            {
                tbltswanagasentitycode code = new tbltswanagasentitycode();
                code.EntityCode = pEntityCode;
                code.EntityName = pEntityName;
                code.EntityTypeID = pEntityTypeID;
                tbltswanagasentitycodeCrud crud = new tbltswanagasentitycodeCrud();
                long id = 0;
                crud.Insert(code, out id);
                return (Int32)id;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTswanaGasEntityCodeMS - " + Ex.Message);
            }
        }
        public Boolean UpdateTswanaGasCentreMS(Int32 pCentreID, String pCentreName)
        {
            try
            {
                tbltswanagascentre centre = GetTswanaGasCentreByCentreIDMS(pCentreID);
                centre.CentreName = pCentreName;
                tbltswanagascentreCrud crud = new tbltswanagascentreCrud();
                crud.Update(centre);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateTswanaGasCentreMS - " + Ex.Message);
            }

        }
        public Int32 InsertTswanaGasCentreCylinderMS(Int32 pCentreID, Int32 pCylinderID, Decimal pCylinderPrice)
        {
            try
            {
                tbltswanagascentrecylinderCrud crud = new tbltswanagascentrecylinderCrud();
                tbltswanagascentrecylinder cenCyl = new tbltswanagascentrecylinder();
                cenCyl.CentreID = pCentreID;
                cenCyl.CylinderID = pCylinderID;
                cenCyl.CylinderPrice = pCylinderPrice;
                long id = 0;
                crud.Insert(cenCyl, out id);

                return (Int16)id;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTswanaGasCentreCylinderMS - " + Ex.Message);
            }
        }
        public tbltswanagasentitycode GetTswanaGasEntityCodeByCodeMS(Int32 pEnityTypeID, String pEntityCode)
        {
            try
            {
                tbltswanagasentitycodeCrud crud = new tbltswanagasentitycodeCrud();
                crud.Where("EntityTypeID", MySQL.DAL.General.Operator.Equals, pEnityTypeID);
                crud.And("EntityCode", MySQL.DAL.General.Operator.Equals, pEntityCode);
                recordFound = false;
                tbltswanagasentitycode code = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return code;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasEntityCode - " + Ex.Message);
            }
        }

        public Decimal GetTswanaGasEmptyCylinder()
        {
            try
            {
                tblsetting appSetting = GetSettingMS("Back Office", "DPO.BackOffice.Ussd", "Tswana Gas Sync", "EmptyCylinderUrl");
                String pUrl = appSetting.SettingValue;
                List<TswanaGasCylinder> cylinders = new List<TswanaGasCylinder>();
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(pUrl);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                String result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                Decimal emptyCylCost = 0;
                if (!Decimal.TryParse(result, out emptyCylCost))
                    throw new Exception("Values retrieved is not numeric (" + result + ")");

                return emptyCylCost;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasEmptyCylinder - " + Ex.Message);
            }
        }
        public List<TswanaGasCentre> GetTswanaGasCentreJSON(Int32 pCityID)
        {
            try
            {
                tblsetting appSetting = GetSettingMS("Back Office", "DPO.BackOffice.Ussd", "Tswana Gas Sync", "CentreUrl");
                String pUrl = appSetting.SettingValue;
                pUrl += "?city=" + pCityID.ToString();
                List<TswanaGasCentre> centres = new List<TswanaGasCentre>();
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(pUrl);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                String result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                if (result.Contains("Please provide City id"))
                    return centres;

                if (!result.Contains("[") && !result.Contains("]"))
                {
                    if (result.Contains("No Distribution Point Available"))
                        return centres;

                    throw new Exception(result);
                }

                result = result.Replace("[", "").Replace("]", "").Replace("\"id\":", "").Replace("\"name\":", "").Replace("\"", "").Replace("cylid:", "").Replace("type:", "").Replace("price:", "");

                String[] centrs = result.Split('}');

                for (Int32 citLoop = 0; citLoop < centrs.GetUpperBound(0); citLoop++)
                {
                    centrs[citLoop] = centrs[citLoop].Replace(",{", "").Replace("{", "");
                }

                for (Int32 citLoop = 0; citLoop < centrs.GetUpperBound(0); citLoop++)
                {
                    if (centrs[citLoop] != "")
                    {
                        String[] workCentre = centrs[citLoop].Split(',');
                        Int32 cityID = Convert.ToInt32(workCentre[0]);
                        String cityName = workCentre[1];
                        Int32 cylinderID = Convert.ToInt32(workCentre[2]);
                        String cylinderType = workCentre[3];
                        Decimal cylinderPrice = Convert.ToDecimal(workCentre[4], System.Globalization.CultureInfo.InvariantCulture);
                        TswanaGasCentre centre = new TswanaGasCentre();
                        centre.CentreID = cityID;
                        centre.CentreName = cityName;
                        centre.CylinderID = cylinderID;
                        centre.CylinderPrice = cylinderPrice;
                        centre.CylinderType = cylinderType;
                        centres.Add(centre);
                    }
                }


                return centres;
            }
            catch (System.Net.WebException webEx)
            {
                if (webEx.Response != null)
                {
                    var resp = new System.IO.StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
                    dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject(resp);
                    String messageFromServer = obj.error.ToString();

                    throw new Exception("Error=" + messageFromServer);
                }
                else
                    throw new Exception("Error=" + webEx.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentre-" + Ex.Message);
            }

        }
        public tbltswanagascentrecylinder GetTswanaGasCentreCylinderMS(Int32 pCentreID, Int32 pCylinderID)
        {
            try
            {
                tbltswanagascentrecylinderCrud crud = new tbltswanagascentrecylinderCrud();
                crud.Where("CentreID", MySQL.DAL.General.Operator.Equals, pCentreID);
                crud.And("CylinderID", MySQL.DAL.General.Operator.Equals, pCylinderID);
                recordFound = false;
                tbltswanagascentrecylinder cenCyl = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return cenCyl;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentreCylinderMS - " + Ex.Message);
            }
        }

        public List<tbltswanagascentrecylinder> GetTswanaGasCentreCylinderMS(Int32 pCentreID)
        {
            try
            {
                tbltswanagascentrecylinderCrud crud = new tbltswanagascentrecylinderCrud();
                crud.Where("CentreID", MySQL.DAL.General.Operator.Equals, pCentreID);
                recordFound = false;
                List<tbltswanagascentrecylinder> cenCyl = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;

                return cenCyl;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentreCylinderMS - " + Ex.Message);
            }
        }
        public Boolean UpdateTswanaGasCityMS(Int32 pCityID, String pCityName, Int32 pUserID)
        {
            try
            {
                tbltswanagascity city = GetTswanaGasCityMS(pCityID);
                if (!recordFound)
                {
                    city.CityName = pCityName;
                    tbltswanagascityCrud crud = new tbltswanagascityCrud();
                    crud.Update(city);
                }

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateTswanaGasCityMS - " + Ex.Message);
            }
        }
        public Boolean UpdateTswanaGasCityMS(Int32 pCityID, Byte pSelectionNo)
        {
            try
            {
                tbltswanagascity city = GetTswanaGasCityMS(pCityID);
                if (!recordFound)
                {
                    city.SelectionNo = pSelectionNo;
                    tbltswanagascityCrud crud = new tbltswanagascityCrud();
                    crud.Update(city);
                }

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateTswanaGasCity - " + Ex.Message);
            }
        }

        public List<tbltswanagascity> GetTswanaGasCityMS()
        {
            try
            {
                tbltswanagascityCrud crud = new tbltswanagascityCrud();
                recordFound = false;
                List<tbltswanagascity> results = crud.ReadMulti().ToList();
                results = results.OrderBy(x => x.CityName).ToList();
                recordFound = crud.RecordFound;

                return results;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCityMS() -" + Ex.Message);
            }
        }
        public tbltswanagascity GetTswanaGasCityMS(Int32 pCityID)
        {
            try
            {
                tbltswanagascityCrud crud = new tbltswanagascityCrud();
                crud.Where("CityID", MySQL.DAL.General.Operator.Equals, pCityID);
                recordFound = false;
                tbltswanagascity city = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return city;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCityMS - " + Ex.Message);
            }
        }

        public tbltswanagascity GetTswanaGasCityMS(String pCity)
        {
            try
            {
                tbltswanagascityCrud crud = new tbltswanagascityCrud();
                crud.Where("CityName", MySQL.DAL.General.Operator.Equals, pCity);
                recordFound = false;
                tbltswanagascity city = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return city;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCityMS - " + Ex.Message);
            }
        }

        public tbltswanagascity GetTswanaGasCityPerCityCode(String pCityCode)
        {
            try
            {
                tbltswanagasentitycode entCode = GetTswanaGasEntityCodeByCodeMS(1, pCityCode);
                tbltswanagascityCrud crud = new tbltswanagascityCrud();
                crud.Where("EntityCodeID", General.Operator.Equals, Convert.ToInt32(entCode.EntityCodeID));
                recordFound = false;
                tbltswanagascity city = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return city;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentre - " + Ex.Message);
            }
        }

        public tbltswanagascentre GetTswanaGasCentreMS(String pCentreCode)
        {
            try
            {
                tbltswanagasentitycode entCode = GetTswanaGasEntityCodeByCodeMS(2, pCentreCode);
                tbltswanagascentreCrud crud = new tbltswanagascentreCrud();
                crud.Where("EntityCodeID", General.Operator.Equals, Convert.ToInt32(entCode.EntityCodeID));
                recordFound = false;
                tbltswanagascentre centre = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return centre;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentre - " + Ex.Message);
            }
        }

        public tbltswanagascentre GetTswanaGasCentreMS(Int32 pCity, Int32 pCentreID)
        {
            try
            {
                tbltswanagascentreCrud crud = new tbltswanagascentreCrud();
                crud.Where("CentreID", MySQL.DAL.General.Operator.Equals, pCentreID);
                crud.And("CityID", MySQL.DAL.General.Operator.Equals, pCity);
                recordFound = false;
                tbltswanagascentre result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentre - " + Ex.Message);
            }
        }

        public tbltswanagascentre GetTswanaGasCentreMS(Int32 pCity, String pCentre)
        {
            try
            {
                tbltswanagascentreCrud crud = new tbltswanagascentreCrud();
                crud.Where("CentreName", MySQL.DAL.General.Operator.Equals, pCentre);
                crud.And("CityID", MySQL.DAL.General.Operator.Equals, pCity);
                recordFound = false;
                tbltswanagascentre result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentre - " + Ex.Message);
            }
        }
        public tbltswanagascylinder GetTswanaGasCylinderMS(Int32 pCylinder)
        {
            try
            {
                tbltswanagascylinderCrud crud = new tbltswanagascylinderCrud();
                crud.Where("CylinderID", MySQL.DAL.General.Operator.Equals, pCylinder);
                recordFound = false;
                tbltswanagascylinder cylinder = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return cylinder;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCylinder - " + Ex.Message);
            }
        }
        public tbltswanagascylinder GetTswanaGasCylinderMS(String pCylinder)
        {
            try
            {
                tbltswanagascylinderCrud crud = new tbltswanagascylinderCrud();
                crud.Where("CylinderName", MySQL.DAL.General.Operator.Equals, pCylinder);
                recordFound = false;
                tbltswanagascylinder cylinder = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return cylinder;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCylinder - " + Ex.Message);
            }
        }
        public List<tbltswanagascylinder> GetTswanaGasCylinderMS()
        {
            try
            {
                tbltswanagascylinderCrud crud = new tbltswanagascylinderCrud();
                recordFound = false;
                List<tbltswanagascylinder> results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return results;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCylinderMS - " + Ex.Message);
            }
        }
        public List<tbltswanagascentre> GetTswanaGasCentreMS(Int32 pCityID)
        {
            try
            {
                tbltswanagascentreCrud crud = new tbltswanagascentreCrud();
                crud.Where("CityID", MySQL.DAL.General.Operator.Equals, pCityID);
                recordFound = false;
                List<tbltswanagascentre> results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return results;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTswanaGasCentreMS -" + Ex.Message);
            }
        }
        public String TswanaGasPlaceOrder(String pMsidn, String pCentreID, String pCylinderID, String pPrice, String pReferenceNo, Boolean pCylinderReplacement, String pDeliveryName)
        {
            String pUrl = "";
            try
            {
                tblsetting appSetting = GetSetting("Back Office", "DPO.BackOffice.Ussd", "OrderURL");
                pUrl = appSetting.SettingValue;
                pUrl += "?cell=" + pMsidn;
                pUrl += "&distribution_point=" + pCentreID;
                pUrl += "&cylinder_type=" + pCylinderID;
                pUrl += "&price=" + pPrice;
                pUrl += "&transaction_id=" + pReferenceNo;
                DateTime now = DateTime.Now;
                pUrl += "&transaction_date_time=" + now.Year.ToString() + "-" + now.Month.ToString().PadLeft(2, '0') + "-" + now.Day.ToString().PadLeft(2, '0') + " " + now.Hour.ToString().PadLeft(2, '0') + ":" + now.Minute.ToString().PadLeft(2, '0') + ":" + now.Second.ToString().PadLeft(2, '0');
                pUrl += pCylinderReplacement == false ? "&cylinder_replacement=1" : "&cylinder_replacement=2";
                pUrl += "&C_NAME=" + pDeliveryName;
                InsertLog(pUrl, eLogType.Information);
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(pUrl);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                String result = "";
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                return result;

            }
            catch (Exception Ex)
            {
                throw new Exception("TswanaGasPlaceOrder -" + Ex.Message);
            }
        }
    }
}
