﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.BackOffice.MySQL.DAL;
using System.Xml;
using System.Collections.ObjectModel;

namespace DPO.BackOffice.MySQL.BL
{
    public partial class MySQLBackOfficeBL
    {
        public Boolean DeleteUssdMenuItem(Int16 pMenuItemID)
        {
            try
            {
                tblussdmenuitem item = GetUssdMenuItemMS(pMenuItemID);
                tblussdmenuitemCrud crud = new tblussdmenuitemCrud();
                crud.Delete(item);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteUssdMenuItem - " + Ex.Message);
            }
        }

        public Boolean UpdateUssdMenuItemText(Int16 pMenuItemTextID, String pMenuItemText)
        {
            try
            {
                tblussdmenuitemtext item = GetUssdMenuItemTextMS(pMenuItemTextID);
                item.MenuItemText = pMenuItemText;
                tblussdmenuitemtextCrud crud = new tblussdmenuitemtextCrud();
                crud.Update(item);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdMenuItemText - " + Ex.Message);
            }
        }

        public Boolean UpdateUssdConfirmText(Int16 pConfirmTextID, String pConfirmText)
        {
            try
            {
                tblussdconfirmtext text = GetUssdConfirmTextMS(pConfirmTextID);
                text.ConfirmText = pConfirmText;
                tblussdconfirmtextCrud crud = new tblussdconfirmtextCrud();
                crud.Update(text);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdConfirmText - " + Ex.Message);
            }
        }

        public Boolean UpdateUssdLeadInText(Int16 pLeadInTextID, String pLeadInText)
        {
            try
            {
                tblussdleadintext text = GetUssdLeadInTextMS(pLeadInTextID);
                text.LeadInText = pLeadInText;
                tblussdleadintextCrud crud = new tblussdleadintextCrud();
                crud.Update(text);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdLeadInText - " + Ex.Message);
            }
        }
        public Boolean DeleteUssdHttpPost(Byte pHttpPostID)
        {
            try
            {
                tblussdhttppost post = GetUssdHttpPostMS(pHttpPostID);
                tblussdhttppostCrud crud = new tblussdhttppostCrud();
                crud.Delete(post);
                return true;

            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteUssdHttpPost - " + Ex.Message);
            }
        }
        public Boolean DeleteUssdTransaction(Int16 pUssdTransactionID)
        {
            try
            {
                tblussdtransaction tran = GetUssdTransactionMS(pUssdTransactionID);
                tblussdtransactionCrud crud = new tblussdtransactionCrud();
                crud.Delete(tran);
                return true;

            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteUssdTransaction - " + Ex.Message);
            }
        }

        public Boolean UpdateUssdTransaction(Int16 pUssdTransactionID, Decimal pTransactionAmount, String pProductDescription)
        {
            try
            {
                tblussdtransaction tran = GetUssdTransactionMS(pUssdTransactionID);
                tran.TransactionAmount = pTransactionAmount;
                tran.ProductDescription = pProductDescription;
                tblussdtransactionCrud crud = new tblussdtransactionCrud();
                crud.Update(tran);
                return true;

            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdTransaction - " + Ex.Message);
            }
        }
        public Boolean UpdateUssdTransaction(Int16 pUssdTransactionID, String pTransaction, Decimal pTransactionAmount, String pTransactionValue, String pFormulaAmount)
        {
            try
            {
                tblussdtransaction tran = GetUssdTransactionMS(pUssdTransactionID);
                tran.FormulaAmount = pFormulaAmount;
                tran.TransactionAmount = pTransactionAmount;
                tran.TransactionValue = pTransactionValue;
                tran.UssdTransaction = pTransaction;
                tblussdtransactionCrud crud = new tblussdtransactionCrud();
                crud.Update(tran);
                return true;

            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdTransaction - " + Ex.Message);
            }
        }
        public Int16 InsertUssdTransactionMS(String pTransaction, Decimal pTransactionAmount, String pTransactionValue, String pFormulaAmount, String pProductDescription, String pTerminalID)
        {
            try
            {
                tblussdtransaction ins = new tblussdtransaction();
                ins.UssdTransaction = pTransaction;
                ins.TransactionAmount = pTransactionAmount;
                ins.TransactionValue = pTransactionValue;
                ins.FormulaAmount = pFormulaAmount;
                ins.ProductDescription = pProductDescription;
                ins.VCSTerminalID = pTerminalID;
                tblussdtransactionCrud crud = new tblussdtransactionCrud();
                long ID = 0;
                crud.Insert(ins, out ID);

                return (Int16)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdTransaction-" + Ex.Message);
            }
        }


        public Int16 InsertUssdTransactionGroup(String pGroupName, Int16? pTranDescID, Int16 pTextMessageID, String pGatewayTransaction)
        {
            try
            {
                tblussdtransactiongroupCrud crud = new tblussdtransactiongroupCrud();
                tblussdtransactiongroup tranGroup = new tblussdtransactiongroup();
                tranGroup.GroupName = pGroupName;
                tranGroup.TextMessageID = pTextMessageID;
                tranGroup.TranDescID = pTranDescID;
                tranGroup.GatewayTransaction = pGatewayTransaction;
                long ID = 0;
                crud.Insert(tranGroup, out ID);

                return (Int16)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdTransactionGroup - " + Ex.Message);
            }
        }

        public Int16 InsertUssdTransactionGroupItem(Int16 pUssdTransactionGroupID, Int16 pUssdTransactionID)
        {
            try
            {
                tblussdtransactiongroupitemCrud crud = new tblussdtransactiongroupitemCrud();
                tblussdtransactiongroupitem tranGroup = new tblussdtransactiongroupitem();
                tranGroup.UssdTransactionGroupID = pUssdTransactionGroupID;
                tranGroup.UssdTransactionID = pUssdTransactionID;
                long ID = 0;
                crud.Insert(tranGroup, out ID);

                return (Int16)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdTransactionGroup - " + Ex.Message);
            }
        }
        public Boolean UpdateUssdHttpPost(Byte pHttpPostID, String pUrl)
        {
            try
            {
                tblussdhttppost post = GetUssdHttpPostMS(pHttpPostID);
                post.Url = pUrl;
                tblussdhttppostCrud crud = new tblussdhttppostCrud();
                crud.Update(post);
                return true;

            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdHttpPost - " + Ex.Message);
            }
        }
        public Byte InsertUssdHttpPostMS(String pUrl)
        {
            try
            {
                tblussdhttppost post = new tblussdhttppost();
                post.Url = pUrl;
                long ID = 0;
                tblussdhttppostCrud crud = new tblussdhttppostCrud();
                crud.Insert(post, out ID);
                return (Byte)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdHttpPostMS-" + Ex.Message);
            }
        }
        public Boolean DeleteUssdLeadInText(Int16 pLeadInTextID)
        {
            try
            {
                tblussdleadintext text = GetUssdLeadInTextMS(pLeadInTextID);
                tblussdleadintextCrud crud = new tblussdleadintextCrud();
                crud.Delete(text);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteUssdLeadInText - " + Ex.Message);
            }
        }
        public Int16 InsertUssdLeadInTextMS(String pLeadInText)
        {
            try
            {
                tblussdleadintext ins = new tblussdleadintext();
                ins.LeadInText = pLeadInText;
                tblussdleadintextCrud crud = new tblussdleadintextCrud();
                long ID = 0;
                crud.Insert(ins, out ID);
                return (Int16)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdLeadInTextMS-" + Ex.Message);
            }
        }
        public Boolean DeleteUssdConfirmText(Int16 pConfirmTextID)
        {
            try
            {
                tblussdconfirmtext text = GetUssdConfirmTextMS(pConfirmTextID);
                tblussdconfirmtextCrud crud = new tblussdconfirmtextCrud();
                crud.Delete(text);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdConfirmText - " + Ex.Message);
            }
        }
        public Int16 InsertUssdConfirmTextMS(String pConfirmText)
        {
            try
            {
                tblussdconfirmtext ins = new tblussdconfirmtext();
                ins.ConfirmText = pConfirmText;
                tblussdconfirmtextCrud crud = new tblussdconfirmtextCrud();
                long ID = 0;
                crud.Insert(ins, out ID);
                return (Int16)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdConfirmTextMS-" + Ex.Message);
            }
        }
        public Boolean DeleteUssdMenuItemText(Int16 pMenuItemTextID)
        {
            try
            {
                tblussdmenuitemtext item = GetUssdMenuItemTextMS(pMenuItemTextID);
                tblussdmenuitemtextCrud crud = new tblussdmenuitemtextCrud();
                crud.Delete(item);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteUssdMenuItemText - " + Ex.Message);
            }
        }
        public Int16 InsertUssdMenuItemTextMS(String pMenuItemText)
        {
            try
            {
                tblussdmenuitemtext ins = new tblussdmenuitemtext();
                ins.MenuItemText = pMenuItemText;
                tblussdmenuitemtextCrud crud = new tblussdmenuitemtextCrud();
                long returnID = 0;
                crud.Insert(ins, out returnID);
                return (Int16)returnID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdMenuItemTextMS-" + Ex.Message);
            }
        }
        public Boolean UpdateUssdMenuItem(Int16 pMenuItemID, String pMenuItem)
        {
            try
            {
                tblussdmenuitem item = GetUssdMenuItemMS(pMenuItemID);
                item.MenuItem = pMenuItem;
                tblussdmenuitemCrud crud = new tblussdmenuitemCrud();
                crud.Update(item);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdMenuItem - " + Ex.Message);
            }
        }
        public Int16 InsertUssdMenuItemMS(String pMenuItem)
        {
            try
            {
                tblussdmenuitem ins = new tblussdmenuitem();
                ins.MenuItem = pMenuItem;
                tblussdmenuitemCrud reader = new tblussdmenuitemCrud();
                long returnID = 0;
                reader.Insert(ins, out returnID);
                return (Int16)returnID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdMenuItemMS-" + Ex.Message);
            }
        }
        public Boolean UpdateUssdWelcomeMessage(Int16 pWelcomeMessageID, String pWelcomeMessage)
        {
            try
            {
                tblussdmenuwelcomemessage mess = GetUssdWelcomeMessageMS(pWelcomeMessageID);
                mess.WelcomeMessage = pWelcomeMessage;
                tblussdmenuwelcomemessageCrud crud = new tblussdmenuwelcomemessageCrud();
                crud.Update(mess);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdWelcomeMessage -" + Ex.Message);
            }
        }
        public Int16 InsertUssdWelcomeMessageMS(String pWelcomeMessage)
        {
            try
            {
                tblussdmenuwelcomemessage mess = new tblussdmenuwelcomemessage();
                mess.WelcomeMessage = pWelcomeMessage;
                tblussdmenuwelcomemessageCrud crud = new tblussdmenuwelcomemessageCrud();
                long ID = 0;
                crud.Insert(mess, out ID);
                return (Int16)ID;
            }
            catch (Exception Ex)
            {

                throw new Exception("InsertUssdWelcomeMessageMS-" + Ex.Message);
            }
        }
        public tblussdmsisdn GetUssdMsisdn(String pMsisdn)
        {
            try
            {
                tblussdmsisdnCrud crud = new tblussdmsisdnCrud();
                crud.Where("Msisdn", MySQL.DAL.General.Operator.Equals, pMsisdn);
                recordFound = false;
                tblussdmsisdn msisdn = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return msisdn;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdn - " + Ex.Message);
            }
        }
        internal String ExtractUssdMsisdn()
        {
            try
            {
                XmlDocument workDoc = ussdInformation.xmlIncomingDoc;
                String strMessageType = "";
                if (workDoc.GetElementsByTagName("MessageType").Count > 0)
                {
                    if (workDoc.GetElementsByTagName("MessageType")[0].ChildNodes.Count > 0)
                        strMessageType = workDoc.GetElementsByTagName("MessageType")[0].ChildNodes[0].InnerText;
                }
                else
                {
                    ussdInformation.ResponseMessage = "Message type tag is missing.";
                    ussdInformation.ResponseCode = "01";
                    return "01";
                }

                if (strMessageType.ToLower() != "ssmiussd")
                {
                    ussdInformation.ResponseMessage = "Message Type not SsmiUssd.";
                    ussdInformation.ResponseCode = "02";
                    return "02";
                }
                else
                {
                    string msisdn = "";
                    if (workDoc.GetElementsByTagName("SourceMsisdn").Count > 0)
                    {
                        if (workDoc.GetElementsByTagName("SourceMsisdn")[0].ChildNodes.Count > 0)
                        {
                            msisdn = workDoc.GetElementsByTagName("SourceMsisdn")[0].ChildNodes[0].Value;
                            ussdInformation.Msisdn = msisdn;
                        }
                        else
                        {
                            ussdInformation.ResponseMessage = "Source Msisdn missing.";
                            ussdInformation.ResponseCode = "04";
                            return "04";
                        }
                    }
                    else
                    {
                        ussdInformation.ResponseMessage = "Source Msisdn tag missing.";
                        ussdInformation.ResponseCode = "03";
                        return "03";
                    }

                    tblussdmsisdn msisdnRec = GetUssdMsisdn(msisdn);
                    Int32 ussdMsisdnID = 0;
                    if (!recordFound)
                        ussdMsisdnID = InsertUssdMsisdn(msisdn);
                    else
                        ussdMsisdnID = Convert.ToInt32(msisdnRec.UssdMsisdnID);

                    ussdInformation.UssdMsisdnID = ussdMsisdnID;

                    string transaction = "";
                    if (workDoc.GetElementsByTagName("Transaction").Count > 0)
                    {
                        if (workDoc.GetElementsByTagName("Transaction")[0].ChildNodes.Count > 0)
                        {
                            transaction = workDoc.GetElementsByTagName("Transaction")[0].ChildNodes[0].Value;
                            ussdInformation.UssdTransaction = transaction;
                        }
                        else
                        {
                            ussdInformation.ResponseMessage = "Transsaction missing.";
                            ussdInformation.ResponseCode = "05";
                            return "05";
                        }
                    }
                    else
                    {
                        ussdInformation.ResponseMessage = "Transaction tag missing.";
                        ussdInformation.ResponseCode = "06";
                        return "06";
                    }


                    tblussdtransactiongroup ussdTransaction = GetUssdTransactionGroupByGatewayTransaction(transaction);
                    if (!recordFound)
                    {
                        ussdInformation.UssdTransactionID = null;
                        ussdInformation.ResponseMessage = "Ussd transaction not set up in tblUssdTransactionGroup.";
                        ussdInformation.ResponseCode = "07";
                        InsertLog("Ussd transaction " + ussdInformation.UssdTransaction + " not set up in the database.", eLogType.Error);
                        //UssdValidationErrorToTrace("Ussd transaction not set up in tblUssdTransaction.");
                        return "07";
                    }

                    ussdInformation.UssdTransactionGroupID = Convert.ToInt16(ussdTransaction.UssdTransactionGroupID);
                }
                ussdInformation.ResponseCode = "00";
                return "00";
            }
            catch (Exception Ex)
            {
                throw new Exception("ExtractUssdMsisdn - " + Ex.Message);
            }
        }
        public Int32 InsertUssdMsisdn(String pMsisdn)
        {
            try
            {
                tblussdmsisdn ins = new tblussdmsisdn();
                ins.Msisdn = pMsisdn;
                long ID = 0;
                tblussdmsisdnCrud crud = new tblussdmsisdnCrud();
                crud.Insert(ins, out ID);

                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdMsisdn - " + Ex.Message);
            }
        }

        public Boolean UpdateUssdTagName(Int16 pTagNameID, String pTagName)
        {
            try
            {
                tblussdtagname tag = GetUssdTagNameMS(pTagNameID);
                tag.TagName = pTagName;
                tblussdtagnameCrud crud = new tblussdtagnameCrud();
                crud.Update(tag);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdTagName - " + Ex.Message);
            }
        }

        public Boolean InsertUssdMenuCardInformationMS(Int32 pUssdMenuID, Byte pGatewayID)
        {
            try
            {
                tblussdmenu currentMenu = GetUssdMenuByIDMS(pUssdMenuID);
                Byte menuLevelNo = Convert.ToByte(currentMenu.MenuLevelNo);
                Int16 welcomeMessageID = Convert.ToInt16(currentMenu.tblussdmenuwelcomemessage.WelcomeMessageID);
                Int32 ussdMenuID = Convert.ToInt32(currentMenu.UssdMenuID);
                String registeryKey = currentMenu.RegistryKey;

                List<tblussdmenu> ussdMenus = GetUssdMenuCardInfoMS();
                foreach (tblussdmenu ussdmenu in ussdMenus)
                {
                    Int16 menuItemID = Convert.ToInt16(ussdmenu.MenuItemID);
                    Int16 confirmTextID = Convert.ToInt16(ussdmenu.ConfirmTextID);
                    Int16 leadInTextID = Convert.ToInt16(ussdmenu.LeadInTextID);
                    Int16 tagNameID = Convert.ToInt16(ussdmenu.TagNameID);
                    Byte? httpPostID = ussdmenu.HttpPostID;

                    /*if (httpPostID != null)
                    {
                        tblussdgateway gateway = GetUssdGatewayMS(pGatewayID);
                        httpPostID = Convert.ToByte(gateway.tblussdhttppost.HttpPostID);
                    }*/

                    registeryKey += "\\" + ussdmenu.tblussdmenuitem.MenuItem;
                    menuLevelNo++;

                    ussdMenuID = InsertUssdMenuMS(pGatewayID, menuItemID, null, null, true, confirmTextID, leadInTextID, true, tagNameID, welcomeMessageID, null, menuLevelNo, httpPostID, registeryKey, ussdMenuID, null);
                }

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdMenuCardInformationMS - " + Ex.Message);
            }
        }
        public Boolean DeleteBusiness(Int16 pBusinessID)
        {
            try
            {
                tblbusiness business = GetBusiness(pBusinessID);
                tblbusinessCrud crud = new tblbusinessCrud();
                crud.Delete(business);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteBusiness - " + Ex.Message);
            }
        }
        public Boolean UpdateBusiness(Int16 pBusinessID, String pBusiness, String pTerminalID)
        {
            try
            {
                tblbusiness business = GetBusiness(pBusinessID);
                business.Business = pBusiness;
                business.TerminalID = pTerminalID;
                tblbusinessCrud crud = new tblbusinessCrud();
                crud.Update(business);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateBusiness - " + Ex.Message);
            }
        }
        public Int16 InsertBusiness(String pBusiness, String pTerminalID)
        {
            try
            {
                tblbusinessCrud crud = new tblbusinessCrud();
                tblbusiness bus = new tblbusiness();
                bus.Business = pBusiness;
                bus.TerminalID = pTerminalID;


                long id = 0;
                crud.Insert(bus, out id);

                return (Int16)id;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertBusinessTransaction - " + Ex.Message);
            }
        }

        public Boolean InsertBusinessTransaction(Int16 pBusinessID, Int16 pUssdTransactionGroupID)
        {
            try
            {
                tblbusinesstransactionCrud crud = new tblbusinesstransactionCrud();
                tblbusinesstransaction busTran = new tblbusinesstransaction();
                busTran.BusinessID = pBusinessID;
                busTran.UssdTransactionGroupID = pUssdTransactionGroupID;
                crud.Insert(busTran);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertBusinessTransaction - " + Ex.Message);
            }
        }
        public List<tblbusiness> GetBusiness()
        {
            try
            {
                tblbusinessCrud crud = new tblbusinessCrud();
                recordFound = false;
                List<tblbusiness> results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return results;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetBusiness - " + Ex.Message);
            }
        }



        public tblbusiness GetBusiness(Int16 pBusinessID)
        {
            try
            {
                tblbusinessCrud crud = new tblbusinessCrud();
                crud.Where("BusinessID", MySQL.DAL.General.Operator.Equals, pBusinessID);
                recordFound = false;
                tblbusiness results = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return results;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetBusiness - " + Ex.Message);
            }
        }

        public List<tblussdinstructiongroup> GetUssdInstructionGroup(Int32 pInstructionGroupID)
        {
            try
            {
                tblussdinstructiongroupCrud crud = new tblussdinstructiongroupCrud();
                crud.Where("InstructionGroupID", MySQL.DAL.General.Operator.Equals, pInstructionGroupID);
                recordFound = false;
                List<tblussdinstructiongroup> res = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;

                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdInstructionGroup - " + Ex.Message);
            }
        }
        public Int32 GetUssdMsisdnInteractionCheckSum(Int32 pTransactionID)
        {
            try
            {
                List<tblussdmsisdninteraction> interactions = GetUssdMsisdnInteraction(pTransactionID);
                Int32 checkSum = 0;
                foreach (tblussdmsisdninteraction interaction in interactions)
                {
                    checkSum += Convert.ToInt32(interaction.SelectionNo);
                }

                return checkSum;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdnInteractionCheckSum - " + Ex.Message);
            }
        }

        public Boolean UpdateUssdMsisdnInteraction(Int32 pInteractionID, Boolean pExcludeFromProcess)
        {
            try
            {
                tblussdmsisdninteraction interaction = GetUssdMsisdnInteractionByInteractionID(pInteractionID);
                interaction.ExcludeFromProcess = pExcludeFromProcess;
                tblussdmsisdninteractionCrud crud = new tblussdmsisdninteractionCrud();
                crud.Update(interaction);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdMsisdnInteraction - " + Ex.Message);
            }
        }

        public List<tblussdmsisdninteraction> GetUssdMsisdnInteraction(Int32 pTransactionID, Boolean pExcludeFromProcess)
        {
            try
            {
                List<tblussdmsisdninteraction> interactions = GetUssdMsisdnInteraction(pTransactionID);
                interactions = interactions.FindAll(x => x.ExcludeFromProcess == false).ToList();
                return interactions;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdnInteractionLast - " + Ex.Message);
            }
        }

        public tblussdmsisdninteraction GetUssdMsisdnInteractionLast(Int32 pTransactionID)
        {
            try
            {
                List<tblussdmsisdninteraction> interactions = GetUssdMsisdnInteraction(pTransactionID);
                interactions = interactions.FindAll(x => x.ExcludeFromProcess == false).ToList();
                interactions = interactions.OrderBy(x => x.InteractionDateTime).Reverse().ToList();
                tblussdmsisdninteraction interaction = interactions[0];
                return interaction;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdnInteractionLast - " + Ex.Message);
            }
        }
        public String BuildUssdMenu(Int32 pGroupID)
        {
            try
            {
                tblussdmenugroup group = GetUssdMenuGroup(pGroupID);
                return BuildUssdMenuGroup(Convert.ToInt32(group.GroupID));
            }
            catch (Exception Ex)
            {
                throw new Exception("BuildUssdMenu - " + Ex.Message);
            }


        }


        public Int32 InsertUssdMsisdnInteraction(Int32 pUssdMsisdnTransactionID, Int32? pGroupID, String pSelectioNo, Int16? pInstructionID, String pValueSubmitted, Int16? pUssdActionID, Boolean pUpdateInteractionValue, Byte? pInstructionNo, Boolean? pAskConfirmation, Boolean? pValidationError, Boolean pExcludeFromProcess)
        {
            try
            {
                Byte? selectionNo = null;
                if (pGroupID != null)
                    selectionNo = Convert.ToByte(pSelectioNo);

                tblussdmsisdninteractionCrud crud = new tblussdmsisdninteractionCrud();
                if (pUpdateInteractionValue)
                {
                    tblussdmsisdninteraction lastInteraction = GetUssdMsisdnInteractionLast(pUssdMsisdnTransactionID);
                    if (recordFound)
                    {
                        if (lastInteraction.InstructionID != null)
                        {
                            tblussdinstruction instruct = GetUssdInstruction((Int16)lastInteraction.InstructionID);
                            if (!Convert.ToBoolean(instruct.StoreInput))
                                pValueSubmitted = instruct.PromptText + " value submitted";
                        }

                        lastInteraction.ValueSubmitted = pValueSubmitted;
                        lastInteraction.InstructionNo = pInstructionNo;
                        lastInteraction.AskConfrimation = pAskConfirmation;
                        if (lastInteraction.InstructionID == null)
                            lastInteraction.SelectionNo = Convert.ToInt32(pSelectioNo);
                        crud.Update(lastInteraction);
                    }
                }
                tblussdmsisdninteraction interaction = new tblussdmsisdninteraction();
                interaction.GroupID = pGroupID;
                interaction.InteractionDateTime = DateTime.Now;
                interaction.TransactionID = pUssdMsisdnTransactionID;
                interaction.SelectionNo = selectionNo;
                interaction.InstructionID = pInstructionID;
                interaction.ValueSubmitted = pValueSubmitted;
                interaction.UssdActionID = pUssdActionID;
                interaction.InstructionNo = pInstructionNo;
                interaction.ValidationError = pValidationError;
                interaction.ExcludeFromProcess = pExcludeFromProcess;


                long ID = 0;
                crud.Insert(interaction, out ID);

                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdMsisdnInteraction - " + Ex.Message);
            }
        }

        public List<tblussdinstructionvalidate> GetUssdInstructionValidate(Int16 pInstructionID)
        {
            try
            {
                tblussdinstructionvalidateCrud crud = new tblussdinstructionvalidateCrud();
                crud.Where("InstructionID", MySQL.DAL.General.Operator.Equals, pInstructionID);
                recordFound = false;
                List<tblussdinstructionvalidate> result = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdInstructionValidate - " + Ex.Message);
            }
        }

        public tblussdinstructiongroup GetUssdInstructionGroup(Int16 pInstructionID)
        {
            try
            {
                tblussdinstructiongroupitem groupItem = GetUssdInstructionGroupItemByInstructionID(pInstructionID);
                return groupItem.tblussdinstructiongroup;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdInstructionGroup - " + Ex.Message);
            }
        }

        public tblussdinstructiongroup GetUssdInstructionGroup(String pInstructionGroupName)
        {
            try
            {
                tblussdinstructiongroupCrud crud = new tblussdinstructiongroupCrud();
                crud.Where("GroupName", MySQL.DAL.General.Operator.Equals, pInstructionGroupName);
                recordFound = false;
                tblussdinstructiongroup group = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return group;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdInstructionGroup - " + Ex.Message);
            }
        }


        public String BuildUssdInstruction(String pInstruction)
        {
            try
            {

                //tblussdinstruction instr = GetUssdInstructionUser(pInstructionGroupID, pTransactionID);
                String returnStr = "<ussd><type>2</type><msg>" + pInstruction;
                returnStr += "\n9 Back</msg></ussd>";

                return returnStr;

            }
            catch (Exception Ex)
            {
                throw new Exception("BuildUssdInstruction - " + Ex.Message);
            }
        }
        public String BuildUssdMenuGroup(Int32 pGroupID)
        {
            try
            {
                tblussdmenugroup group = GetUssdMenuGroup(pGroupID);
                List<tblussdmenugroupitem> groupItems = GetUssdMenuGroupItem(pGroupID);
                String returnStr = "<ussd><type>2</type><msg>" + group.tblussdmenuwelcomemessage.WelcomeMessage;
                foreach (tblussdmenugroupitem groupItem in groupItems)
                {
                    returnStr += "\n" + groupItem.SelectionNo.ToString() + "." + groupItem.tblussdmenuitemtext.MenuItemText;
                }
                returnStr += "\n\n9.Back";
                returnStr += "</msg></ussd>";

                return returnStr;
            }
            catch (Exception Ex)
            {
                throw new Exception("BuildUssdMenuGroup - " + Ex.Message);
            }
        }

        public Boolean DeleteUssdMsisdnInteractionLastRecord(Int16 pTransactionID)
        {
            try
            {
                List<tblussdmsisdninteraction> interactions = GetUssdMsisdnInteraction(pTransactionID);
                Int32 lastID = (Int32)interactions.Max(x => x.InteractionID);
                tblussdmsisdninteractionCrud crud = new tblussdmsisdninteractionCrud();
                crud.Where("InteractionID", MySQL.DAL.General.Operator.Equals, lastID);
                tblussdmsisdninteraction inter = crud.ReadSingle();
                crud.Delete(inter);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteUssdMsisdnInteractionLastRecord - " + Ex.Message);
            }
        }
        public String BuildUssdConfirmationMenu(Int16 pTransactionID)
        {
            String logInfo = "";
            try
            {
                List<tblussdmsisdninteraction> interactions = GetUssdMsisdnInteraction(pTransactionID);
                logInfo += "GotInteractions|";
                String confirmMenu = "<ussd><type>2</type><msg>Please confirm.\n";
                interactions = interactions.FindAll(x => x.InstructionNo != null).ToList();
                logInfo += "InteractionsFound=" + interactions.Count.ToString() + "|";
                List<UssdInteraction> ussdInteractions = ReadInteractionFromCache(interactions[0].tblussdmsisdntransaction.SessionID.ToString());

                foreach (tblussdmsisdninteraction interaction in interactions)
                {
                    String valueSubmitted = interaction.ValueSubmitted;
                    logInfo += "ValueSubmitted=" + valueSubmitted + "|";
                    if (!Convert.ToBoolean(interaction.tblussdinstruction.StoreInput))
                    {
                        logInfo += "StoreInput=Y|";
                        valueSubmitted = ussdInteractions.FindAll(x => x.InstructionID == Convert.ToInt16(interaction.InstructionID))[0].ValueSubmitted;
                        logInfo += "StoreSubmitted=" + valueSubmitted + "|";
                    }

                    confirmMenu += "\n" + interaction.InstructionNo + " " + interaction.tblussdinstruction.PromptText + ": " + valueSubmitted;
                    logInfo += "ConfirmMenu=" + confirmMenu;
                }
                confirmMenu += "\n\n0 Information Correct";
                confirmMenu += "</msg></ussd>";

                return confirmMenu;
            }
            catch (Exception Ex)
            {
                throw new Exception("BuildUssdConfirmationMenu - " + Ex.Message + "|LogInfo=" + logInfo);
            }
        }
        public String BuildUssdMenuGroupMainMenu(Byte pServiceProviderID)
        {
            try
            {
                tblussdmenugroupmainmenu mainMenu = GetUssdMenuGroupMainMenu(pServiceProviderID);
                List<tblussdmenugroupitem> groupItems = GetUssdMenuGroupItem(Convert.ToInt32(mainMenu.GroupID));
                groupItems = groupItems.OrderBy(x => x.SelectionNo).ToList();
                String returnStr = "<ussd><type>2</type><msg>" + mainMenu.tblussdmenugroup.tblussdmenuwelcomemessage.WelcomeMessage;
                foreach (tblussdmenugroupitem groupItem in groupItems)
                {
                    returnStr += "\n" + groupItem.SelectionNo.ToString() + "." + groupItem.tblussdmenuitemtext.MenuItemText;
                }
                returnStr += "</msg></ussd>";

                return returnStr;
            }
            catch (Exception Ex)
            {
                throw new Exception("BuildUssdMenuGroupMainMenu - " + Ex.Message);
            }
        }

        public List<tblussdmenu> GetUssdMenuChildrenMS(Int32 pUssdMenuID)
        {
            try
            {
                recordFound = true;
                List<tblussdmenu> ussdMenuChildren = new List<tblussdmenu>();
                while (recordFound)
                {
                    tblussdmenu menu = new tblussdmenu();
                    menu = GetUssdMenuByParentIDMS(pUssdMenuID);
                    if (recordFound)
                    {
                        ussdMenuChildren.Add(menu);
                        pUssdMenuID = Convert.ToInt32(menu.UssdMenuID);
                    }
                }

                return ussdMenuChildren;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuChildren - " + Ex.Message);
            }
        }

        public Boolean DeleteUssdMenuMS(Int32 pUssdMenuID)
        {
            try
            {
                List<tblussdmenu> ussdMenuChildren = GetUssdMenuChildrenMS(pUssdMenuID);
                tblussdmenuCrud crud = new tblussdmenuCrud();
                foreach (tblussdmenu menu in ussdMenuChildren)
                    crud.Delete(menu);

                tblussdmenu currentMenu = GetUssdMenuByIDMS(pUssdMenuID);
                crud.Delete(currentMenu);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteUssdMenu - " + Ex.Message);
            }
        }
        public Boolean InsertUssdMenuDuplicateMS(Int32 pUssdMenuID, String pMenuItem, Byte pGatewayID, Int16? pMenuItemNumber, String pMenuItemText, String pTransaction, Decimal pTransactionAmount, String pTransactionValue, String pTransactionFormula, String pProductDescription, String pTerminalID)
        {
            try
            {
                tblussdmenu currentMenu = GetUssdMenuByIDMS(pUssdMenuID);
                pUssdMenuID = Convert.ToInt32(currentMenu.ParentUssdMenuID);

                String registryKey = currentMenu.RegistryKey.Substring(0, currentMenu.RegistryKey.LastIndexOf("\\")) + "\\" + pMenuItem;
                List<tblussdmenu> ussdMenuChildren = GetUssdMenuChildrenMS(pUssdMenuID);
                tblussdmenuitem menuItem = GetUssdMenuItemMS(pMenuItem);
                Int16 menuItemID = 0;
                if (!recordFound)
                    menuItemID = InsertUssdMenuItemMS(pMenuItem);
                else
                    menuItemID = Convert.ToInt16(menuItem.MenuItemID);

                tblussdtransaction transaction = GetUssdTransactionMS(pTransaction);
                Int16 transactionID = 0;
                if (!recordFound)
                    transactionID = InsertUssdTransactionMS(pTransaction, pTransactionAmount, pTransactionValue, pTransactionFormula, pProductDescription, pTerminalID);
                else
                    transactionID = Convert.ToInt16(transaction.UssdTransactionID);

                tblussdmenuitemtext menuItemText = GetUssdMenuItemTextMS(pMenuItemText);
                Int16? menuItemTextID = 0;
                if (!recordFound)
                    menuItemTextID = InsertUssdMenuItemTextMS(pMenuItemText);
                else
                    menuItemTextID = Convert.ToInt16(menuItemText.MenuItemTextID);

                Boolean firstRecord = true;
                foreach (tblussdmenu menu in ussdMenuChildren)
                {

                    if (!firstRecord)
                    {
                        menuItemID = Convert.ToInt16(menu.MenuItemID);
                        menuItemTextID = menu.MenuItemTextID;
                        pMenuItemNumber = menu.MenuItemNumber;
                        registryKey += menu.tblussdmenuitem;
                    }


                    Boolean? confirm = menu.Confirm;
                    Int16? confirmTextID = menu.ConfirmTextID;
                    Int16? leadTextInID = menu.LeadInTextID;
                    Boolean? prompt = menu.Promt;
                    Int16? tagNameID = menu.TagNameID;
                    Int16? welcomeMessageID = menu.WelcomeMessageID;
                    Int16? ussdTransactionID = null;
                    if (menu.UssdTransactionGroupID != null)
                        ussdTransactionID = transactionID;

                    Byte? httpPostID = menu.HttpPostID;
                    Byte menuLevelNo = Convert.ToByte(menu.MenuLevelNo);

                    /*if (httpPostID != null)
                    {
                        tblussdgateway gateway = GetUssdGatewayMS(pGatewayID);
                        httpPostID = Convert.ToByte(gateway.tblussdhttppost.HttpPostID);
                    }*/


                    pUssdMenuID = InsertUssdMenuMS(pGatewayID, menuItemID, pMenuItemNumber, menuItemTextID, confirm, confirmTextID, leadTextInID, prompt, tagNameID, welcomeMessageID, ussdTransactionID, menuLevelNo, httpPostID, registryKey, pUssdMenuID, null);
                    firstRecord = false;
                }

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdMenuDuplicateMS - " + Ex.Message);
            }
        }

        public Int32 InsertTempGatewayTest(Int16? pUssdTransactionGroupID, String pTransactionDesc, String pVendingMessage, String pTextMessage, Int16? pValidationMessageID)
        {
            try
            {
                tbltempgatewaytestCrud crud = new tbltempgatewaytestCrud();
                tbltempgatewaytest test = new tbltempgatewaytest();
                test.UssdTransactionGroupID = pUssdTransactionGroupID;
                test.TransactionDesc = pTransactionDesc;
                test.VendingMessage = pVendingMessage;
                test.ValidationMessageID = pValidationMessageID;
                long id;
                crud.Insert(test, out id);

                return (Int32)id;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTempGatewayTest - " + Ex.Message);
            }

        }

        /*
        public Boolean DoConcerotelPayment()
        {
            try
            {
                String url = GetSetting("Virtual Message", "VCS.VirtualMessage.Ussd", "ConcerotelUrl").SettingValue;
                String apiKey = GetSetting("Virtual Message", "VCS.VirtualMessage.Ussd", "ConcerotelApiKey").SettingValue;
                String hash = GetSetting("Virtual Message", "VCS.VirtualMessage.Ussd", "ConcerotelHash").SettingValue;
                if (ussdInformation.UssdTransaction.ToLower() == "concerotelinvoice")
                    url += "invoice/" + ussdInformation.InvoiceNumber;
                else
                    url += "hotspot/" + ussdInformation.UssdTransaction.Substring(ussdInformation.UssdTransaction.Length - 1, 1);

                url += "?apiKey=" + apiKey;

                String uniqueNo = GetBusinessUniqueReferenceNumber("Concerotel");
                Int32 unixTimestamp = Convert.ToInt32((DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);
                String hashString = ussdInformation.Msisdn + unixTimestamp.ToString() + "Approved" + ussdInformation.Amount.ToString() + ".00" + uniqueNo + hash;

                System.Security.Cryptography.MD5 md5String;
                md5String = System.Security.Cryptography.MD5.Create();
                Byte[] inputBytes = Encoding.ASCII.GetBytes(hashString);
                Byte[] hashBytes = md5String.ComputeHash(inputBytes);
                StringBuilder sb = new StringBuilder();
                for (UInt32 i = 0; i < hashBytes.Length; i++)
                    sb.Append(hashBytes[i].ToString("x2"));

                String result = messBL.PostToConcerotel(url, ussdInformation.Msisdn, ussdInformation.Amount.ToString() + ".00", uniqueNo, sb.ToString(), unixTimestamp.ToString());
                if (!result.ToLower().Contains("error="))
                {
                    if (ussdInformation.UssdTransaction.ToLower() == "concerotelinvoice")
                    {
                        result = result.Replace("{", "").Replace("}", "").Replace("\"", "");
                        String[] resultSplit = result.Split(',');
                        String invoiceStatus = resultSplit[0].Split(':')[0];
                        String timeStamp = resultSplit[5].Split(':')[0];
                        String uniqueID = resultSplit[6].Split(':')[0];
                        ussdInformation.ResponseMessage = "Invoice " + ussdInformation.InvoiceNumber + " was succesfully paid.";
                    }
                    else
                    {
                        result = result.Replace("{", "").Replace("}", "").Replace("\"", "");
                        String[] resultSplit = result.Split(',');
                        String voucherOption = resultSplit[0].Split(':')[1];
                        String voucherName = resultSplit[1].Split(':')[1];
                        String voucherPrice = resultSplit[2].Split(':')[1];
                        String voucherNo = resultSplit[5].Split(':')[1] + resultSplit[5].Split(':')[2] + resultSplit[5].Split(':')[3];
                        String timeStamp = resultSplit[6].Split(':')[1];
                        InsertUssdMsidnConcerotelData(ussdInformation.UssdMsisdnTransactionID, voucherOption, voucherName, voucherNo);
                    }
                }
                else
                {
                    if (ussdInformation.UssdTransaction.ToLower() == "concerotelinvoice")
                    {
                        ussdInformation.ResponseMessage = "Concerotel invoice payment exception.";
                        return false;
                    }
                    else
                    {
                        ussdInformation.ResponseMessage = "Concerotel voucher disbursement exception.";
                        return false;
                    }
                }
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DoConcerotelPayment - " + Ex.Message);
            }
        }
        */
        public UssdIncomingInformation ExtractUssdIncomingInformation(String IncomingStream, UssdServiceProvider ServiceProvider)
        {
            UssdIncomingInformation ussdInfo = new UssdIncomingInformation();
            try
            {

                XmlDocument xmlIncomingDoc = new System.Xml.XmlDocument();
                try
                {
                    xmlIncomingDoc.LoadXml(IncomingStream);
                }
                catch (Exception Ex)
                {
                    ussdInfo.Response.ResponseCode = "0011";
                    ussdInfo.Response.ResponseMessage = "Invalid XML Document Format.";
                    return ussdInfo;
                }

                switch (ServiceProvider)
                {
                    case UssdServiceProvider.Orange:
                        String msisdn = "";
                        if (xmlIncomingDoc.GetElementsByTagName("msisdn").Count > 0)
                        {
                            if (xmlIncomingDoc.GetElementsByTagName("msisdn")[0].ChildNodes.Count > 0)
                                msisdn = xmlIncomingDoc.GetElementsByTagName("msisdn")[0].ChildNodes[0].Value;
                        }
                        else
                        {
                            ussdInfo.Response.ResponseCode = "0001";
                            ussdInfo.Response.ResponseMessage = "Msisdn tag missing..";
                            return ussdInfo;
                        }

                        String sessionID = "";
                        if (xmlIncomingDoc.GetElementsByTagName("sessionid").Count > 0)
                        {
                            if (xmlIncomingDoc.GetElementsByTagName("sessionid")[0].ChildNodes.Count > 0)
                                sessionID = xmlIncomingDoc.GetElementsByTagName("sessionid")[0].ChildNodes[0].Value;
                        }
                        else
                        {
                            ussdInfo.Response.ResponseCode = "0001";
                            ussdInfo.Response.ResponseMessage = "Sessdion ID tag missing..";
                            return ussdInfo;
                        }

                        String ussdType = "";
                        if (xmlIncomingDoc.GetElementsByTagName("type").Count > 0)
                        {
                            if (xmlIncomingDoc.GetElementsByTagName("type")[0].ChildNodes.Count > 0)
                                ussdType = xmlIncomingDoc.GetElementsByTagName("type")[0].ChildNodes[0].Value;
                        }
                        else
                        {
                            ussdInfo.Response.ResponseCode = "0002";
                            ussdInfo.Response.ResponseMessage = "Request type tag missing..";
                            return ussdInfo;
                        }

                        String ussdMessage = "";
                        if (xmlIncomingDoc.GetElementsByTagName("msg").Count > 0)
                        {
                            if (xmlIncomingDoc.GetElementsByTagName("msg")[0].ChildNodes.Count > 0)
                                ussdMessage = xmlIncomingDoc.GetElementsByTagName("msg")[0].ChildNodes[0].Value;
                        }
                        else
                        {
                            ussdInfo.Response.ResponseCode = "0003";
                            ussdInfo.Response.ResponseMessage = "Message tag missing..";
                            return ussdInfo;
                        }

                        ussdInfo.Msisdn = msisdn;
                        ussdInfo.Response.ResponseCode = "0000";
                        ussdInfo.Response.ResponseMessage = "Success";
                        ussdInfo.SessionID = sessionID;
                        ussdInfo.UssdMessage = ussdMessage;
                        ussdInfo.UssdType = ussdType;
                        break;


                }
                return ussdInfo;
            }
            catch (Exception Ex)
            {
                ussdInfo.Response.ResponseCode = "9999";
                ussdInfo.Response.ResponseMessage = GenericMessage;
                return ussdInfo;
            }
        }

        public Int16 InsertUssdGroupMenuItem(Int16 pGroupID, Int16 pMenuItemTextID, Byte pSelectionNo, Int16 pParentGroupID)
        {
            try
            {
                tblussdmenugroupitemCrud crud = new tblussdmenugroupitemCrud();
                tblussdmenugroupitem item = new tblussdmenugroupitem();
                item.GroupID = pGroupID;
                item.MenuItemTextID = pMenuItemTextID;
                item.ParentGroupID = pParentGroupID;
                item.SelectionNo = pSelectionNo;
                long ID;
                crud.Insert(item, out ID);
                return (Int16)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdGroupMenuItem - " + Ex.Message);
            }
        }

        public Int16 InsertUssdMenuGroup(String pGroupName, Int16 pMenuItemTextID, Int16 pWelcomeMessageID, Byte pSelectionNo, Int32 pParentGroupID, Int16? pInstructionGroupID, Int16? pUssdTransactionGroupID)
        {
            try
            {
                tblussdmenugroupCrud crud = new tblussdmenugroupCrud();
                tblussdmenugroup group = new tblussdmenugroup();
                group.GroupName = pGroupName;
                group.InstructionGroupID = pInstructionGroupID;
                group.MenuItemTextID = pMenuItemTextID;
                group.ParentGroupID = pParentGroupID;
                group.SelectionNo = pSelectionNo;
                group.UssdTransactionGroupID = pUssdTransactionGroupID;
                group.WelcomeMessageID = pWelcomeMessageID;
                long ID = 0;
                crud.Insert(group, out ID);
                return (Int16)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdGroupMenu - " + Ex.Message);
            }
        }
        public tblussdmenugroupitem GetUssdMenuGroupItem(Int32 pGroupID, Int16 pMenuItemTextID)
        {
            try
            {
                tblussdmenugroupitemCrud crud = new tblussdmenugroupitemCrud();
                crud.Where("GroupID", MySQL.DAL.General.Operator.Equals, pGroupID);
                crud.And("MenuItemTextID", MySQL.DAL.General.Operator.Equals, pMenuItemTextID);
                recordFound = false;
                tblussdmenugroupitem item = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return item;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuGroupItem - " + Ex.Message);
            }
        }
        public List<tblussdmenugroupitem> GetUssdMenuGroupItem(Int32 pGroupID)
        {
            try
            {
                tblussdmenugroupitemCrud crud = new tblussdmenugroupitemCrud();
                crud.Where("GroupID", MySQL.DAL.General.Operator.Equals, pGroupID);
                recordFound = false;
                List<tblussdmenugroupitem> items = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return items;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuGroupItem - " + Ex.Message);
            }
        }

        public tblussdmenugroup GetUssdMenuGroup(Int32 pParentGroupID, Byte pSelectionNo)
        {
            try
            {
                tblussdmenugroupCrud crud = new tblussdmenugroupCrud();
                crud.Where("ParentGroupID", MySQL.DAL.General.Operator.Equals, pParentGroupID);
                crud.And("SelectionNo", MySQL.DAL.General.Operator.Equals, pSelectionNo);
                recordFound = false;
                tblussdmenugroup group = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return group;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuGroup - " + Ex.Message);
            }
        }

        public tblussdmenugroup GetUssdMenuGroup(Int32 pGroupID)
        {
            try
            {
                tblussdmenugroupCrud crud = new tblussdmenugroupCrud();
                crud.Where("GroupID", MySQL.DAL.General.Operator.Equals, pGroupID);
                recordFound = false;
                tblussdmenugroup group = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return group;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuGroup - " + Ex.Message);
            }
        }

        public tblussdmenugroup GetUssdMenuGroup(String pGroupName)
        {
            try
            {
                tblussdmenugroupCrud crud = new tblussdmenugroupCrud();
                crud.Where("GroupName", MySQL.DAL.General.Operator.Equals, pGroupName);
                recordFound = false;
                tblussdmenugroup group = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return group;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuGroup - " + Ex.Message);
            }
        }
        /*public tblUssdMsisdnTransaction GetUssdMsisdnTransaction(Int32 pUssdMsisdnID, Int32 pSessionID)
        {
            try
            {
                tblUssdMsisdnTransactionCrud crud = new tblUssdMsisdnTransactionCrud();
                crud.Where("UssdMsisdnID", DAL.General.Operator.Equals, pUssdMsisdnID);
                crud.And("SessionID", DAL.General.Operator.Equals, pSessionID);
                recordFound = false;
                tblUssdMsisdnTransaction tran = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return tran;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdnTransaction - " + Ex.Message);
            }
        }*/

        public tblussdinstructiongroupitem GetUssdInstructionGroupItemByInstructionID(Int16 pInstructionID)
        {
            try
            {
                tblussdinstructiongroupitemCrud crud = new tblussdinstructiongroupitemCrud();
                crud.Where("InstructionID", MySQL.DAL.General.Operator.Equals, pInstructionID);
                recordFound = false;
                tblussdinstructiongroupitem res = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdInstructionGroupItem - " + Ex.Message);
            }
        }

        public List<tblussdinstructiongroupitem> GetUssdInstructionGroupItem(Int16 pInstructionGroupID)
        {
            try
            {
                tblussdinstructiongroupitemCrud crud = new tblussdinstructiongroupitemCrud();
                crud.Where("InstructionGroupID", MySQL.DAL.General.Operator.Equals, pInstructionGroupID);
                recordFound = false;
                List<tblussdinstructiongroupitem> res = crud.ReadMulti().ToList();
                res = res.OrderBy(x => x.Ordinal).ToList();
                recordFound = crud.RecordFound;
                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdInstructionGroupItem - " + Ex.Message);
            }
        }


        public tblussdinstruction GetUssdInstructionMS(String pInstruction)
        {
            try
            {
                tblussdinstructionCrud crud = new tblussdinstructionCrud();
                crud.Where("Instruction", MySQL.DAL.General.Operator.Equals, pInstruction);
                recordFound = false;
                tblussdinstruction ins = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return ins;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdInstruction - " + Ex.Message);
            }
        }


        public tblussdtransactiongroup GetUssdTransactionGroupByUssdTransactionID(Int32 pUssdTransactionID)
        {
            try
            {
                tblussdtransactiongroupitem item = GetUssdTransactionGroupItem(pUssdTransactionID);
                if (recordFound)
                {
                    tblussdtransactiongroupCrud crud = new tblussdtransactiongroupCrud();
                    crud.Where("UssdTransactionGroupID", MySQL.DAL.General.Operator.Equals, Convert.ToInt16(item.UssdTransactionGroupID));
                    recordFound = false;
                    tblussdtransactiongroup trans = crud.ReadSingle();
                    recordFound = crud.RecordFound;
                    return trans;
                }

                return null;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransaction - " + Ex.Message);
            }
        }

        public tblussdtransactiongroup GetUssdTransactionGroup(Int16 pUssdTransactionGroupID)
        {
            try
            {
                tblussdtransactiongroupCrud crud = new tblussdtransactiongroupCrud();
                crud.Where("UssdTransactionGroupID", MySQL.DAL.General.Operator.Equals, pUssdTransactionGroupID);
                recordFound = false;
                tblussdtransactiongroup trans = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return trans;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransaction - " + Ex.Message);
            }
        }

        public List<tblussdtransactiongroup> GetUssdTransactionGroup()
        {
            try
            {
                tblussdtransactiongroupCrud crud = new tblussdtransactiongroupCrud();
                recordFound = false;
                List<tblussdtransactiongroup> trans = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;

                return trans;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransaction - " + Ex.Message);
            }
        }



        public tblussdtransactiongroup GetUssdTransactionGroup(String pGroupName)
        {
            try
            {
                tblussdtransactiongroupCrud crud = new tblussdtransactiongroupCrud();
                crud.Where("GroupName", MySQL.DAL.General.Operator.Equals, pGroupName);
                recordFound = false;
                tblussdtransactiongroup trans = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return trans;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransaction - " + Ex.Message);
            }
        }

        public tblussdmsisdninteraction GetUssdMsisdnInteractionByInteractionID(Int32 pInteractionID)
        {
            try
            {
                tblussdmsisdninteraction retInteraction = new tblussdmsisdninteraction();
                tblussdmsisdninteractionCrud crud = new tblussdmsisdninteractionCrud();
                crud.Where("InteractionID", MySQL.DAL.General.Operator.Equals, pInteractionID);
                recordFound = false;
                retInteraction = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return retInteraction;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdnInteraction - " + Ex.Message);
            }
        }

        public tblussdmsisdninteraction GetUssdMsisdnInteraction(Int32 pTransactionID, Int16 pInstructionID)
        {
            try
            {
                tblussdmsisdninteraction retInteraction = new tblussdmsisdninteraction();
                tblussdmsisdninteractionCrud crud = new tblussdmsisdninteractionCrud();
                crud.Where("TransactionID", MySQL.DAL.General.Operator.Equals, pTransactionID);
                crud.And("InstructionID", MySQL.DAL.General.Operator.Equals, pInstructionID);
                recordFound = false;
                retInteraction = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return retInteraction;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdnInteraction - " + Ex.Message);
            }
        }

        public Boolean InsertUssdTransactionAction(Int16 pUssdTransactionGroupID, Int16 pUssdActionID, Int16? pOrdinal)
        {
            try
            {
                tblussdtransactionactionCrud crud = new tblussdtransactionactionCrud();
                tblussdtransactionaction act = new tblussdtransactionaction();
                act.UssdActionID = pUssdActionID;
                act.UssdTransactionGroupID = pUssdTransactionGroupID;
                act.Ordinal = pOrdinal;
                crud.Insert(act);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdTransactionAction - " + Ex.Message);
            }
        }

        public tblussdaction GetUssdAction(String pUssdAction)
        {
            try
            {
                tblussdactionCrud crud = new tblussdactionCrud();
                crud.Where("UssdAction", MySQL.DAL.General.Operator.Equals, pUssdAction);
                recordFound = false;
                tblussdaction act = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return act;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdAction -" + Ex.Message);
            }
        }

        public Int32 InsertUssdMenuMS(List<UssdMenuInformationChange> pUssdMenuChanges)
        {
            try
            {
                Byte gatewayID = 0;
                Byte menuLevelNo = 0;
                Int32? parentUssdMenuID = null;
                Int16? menuItemID = null;
                Int16? menuItemNo = null;
                Int16? menuItemTextID = null;
                Boolean? confirm = null;
                Int16? confirmTextID = null;
                Int16? leadInTextID = null;
                Boolean? prompt = null;
                Int16? tagNameID = null;
                Int16? welcomeMessageID = null;
                Int16? transactionID = null;
                Byte? httpPostID = null;
                Int32? functionID = null;
                Byte? menuItemNumber = null;
                String menuItem = "";
                String registryKey = "";
                List<UssdMenuInformationChange> workChanges = pUssdMenuChanges.FindAll(x => x.ChangeInformation == true).ToList();
                foreach (UssdMenuInformationChange change in workChanges)
                {
                    switch (change.UssdMenuInformationType)
                    {
                        case eUssdMenuInformation.Gateway:
                            gatewayID = Convert.ToByte(GetUssdGatewayMS(change.NewValue).GatewayID);
                            break;
                        case eUssdMenuInformation.MenuLevelNumber:
                            menuLevelNo = Convert.ToByte(change.NewValue);
                            break;
                        case eUssdMenuInformation.ParentUssdID:
                            parentUssdMenuID = Convert.ToInt32(change.NewValue);
                            break;
                        case eUssdMenuInformation.MenuItemNumber:
                            menuItemNumber = Convert.ToByte(change.NewValue);
                            break;
                        case eUssdMenuInformation.Confirm:
                            confirm = Convert.ToBoolean(change.NewValue);
                            break;
                        case eUssdMenuInformation.ConfirmText:
                            confirmTextID = Convert.ToInt16(GetUssdConfirmTextMS(change.NewValue).ConfirmTextID);
                            break;
                        case eUssdMenuInformation.LeadInText:
                            leadInTextID = Convert.ToInt16(GetUssdLeadInTextMS(change.NewValue).LeadInTextID);
                            break;
                        case eUssdMenuInformation.MenuItem:
                            menuItemID = Convert.ToInt16(GetUssdMenuItemMS(change.NewValue).MenuItemID);
                            menuItem = change.NewValue;
                            break;
                        case eUssdMenuInformation.MenuItemText:
                            menuItemTextID = Convert.ToInt16(GetUssdMenuItemTextMS(change.NewValue).MenuItemTextID);
                            break;
                        case eUssdMenuInformation.PostUrl:
                            httpPostID = Convert.ToByte(GetUssdHttpPostMS(change.NewValue).HttpPostID);
                            break;
                        case eUssdMenuInformation.Prompt:
                            prompt = Convert.ToBoolean(change.NewValue);
                            break;
                        case eUssdMenuInformation.TagName:
                            tagNameID = Convert.ToInt16(GetUssdTagNameMS(change.NewValue).TagNameID);
                            break;
                        case eUssdMenuInformation.Transaction:
                            transactionID = Convert.ToInt16(GetUssdTransactionMS(change.NewValue).UssdTransactionID);
                            break;
                        case eUssdMenuInformation.WelcomeMessage:
                            welcomeMessageID = Convert.ToInt16(GetUssdMenuWelcomeMessage(change.NewValue).WelcomeMessageID);
                            break;
                        case eUssdMenuInformation.RegistryKey:
                            registryKey = change.NewValue;
                            break;
                    }
                }


                if (parentUssdMenuID == null)
                {
                    switch (gatewayID)
                    {
                        case 1:
                            registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\VCS\VirtualGateway\VgiBeMobVcsUssd1\UssdMenu\BeMobVcs\" + menuItem;
                            break;
                        case 2:
                            registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\VCS\VirtualGateway\VgiBeMobVcsUssd2\UssdMenu\BeMobVcs\" + menuItem;
                            break;
                        case 3:
                            registryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\VCS\VirtualGateway\VgiMascomSmpp\UssdMenu\Mascom\" + menuItem;
                            break;

                    }
                }
                else
                    registryKey = registryKey + "\\" + menuItem;
                return InsertUssdMenuMS(gatewayID, menuItemID, menuItemNumber, menuItemTextID, confirm, confirmTextID, leadInTextID, prompt, tagNameID, welcomeMessageID, transactionID, menuLevelNo, httpPostID, registryKey, parentUssdMenuID, null);
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdMenuMS - " + Ex.Message);
            }
        }

        public Int16 GetOrInsertUssdTagNameMS(String pTagName)
        {
            try
            {
                tblussdtagname tagName = GetUssdTagNameMS(pTagName);
                if (RecordFound)
                    return (Int16)tagName.TagNameID;
                else
                    return InsertUssdTagNameMS(pTagName);
            }
            catch (Exception Ex)
            {
                throw new Exception("GetOrInsertUssdTagNameMS - " + Ex.Message);
            }
        }
        public List<tblussdmenu> GetUssdMenuCardInfoMS()
        {
            try
            {
                List<tblussdmenu> ussdMenus = new List<tblussdmenu>();
                tblussdmenucardinfoCrud crud = new tblussdmenucardinfoCrud();
                List<tblussdmenucardinfo> info = crud.ReadMulti().ToList();
                foreach (tblussdmenucardinfo inf in info)
                    ussdMenus.Add(inf.tblussdmenu);

                return ussdMenus;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuCardInfoMS - " + Ex.Message);
            }
        }

        public String GetBusinessUniqueReferenceNumber(String pBusinessName)
        {
            try
            {
                tblbusiness bus = GetBusiness(pBusinessName);
                String refNo = bus.UniqueReferenceNo.ToString();
                Int32 refNoInt = Convert.ToInt32(refNo) + 1;
                bus.UniqueReferenceNo = refNoInt;
                tblbusinessCrud crud = new tblbusinessCrud();
                crud.Update(bus);
                return refNo;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetThirdPartyUniqueReferenceNumber-" + Ex.Message);
            }
        }

        public tblussdmenugroupmainmenu GetUssdMenuGroupMainMenu(Byte pServiceProviderID)
        {
            try
            {
                tblussdmenugroupmainmenuCrud crud = new tblussdmenugroupmainmenuCrud();
                crud.Where("ServiceProviderID", MySQL.DAL.General.Operator.Equals, pServiceProviderID);
                recordFound = false;
                tblussdmenugroupmainmenu menu = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return menu;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuGroupMainMenu - " + Ex.Message);
            }
        }

        public List<tblussdmsisdninteraction> GetUssdMsisdnInteraction(Int32 pTransactionID)
        {
            try
            {
                tblussdmsisdninteractionCrud crud = new tblussdmsisdninteractionCrud();
                crud.Where("TransactionID", MySQL.DAL.General.Operator.Equals, pTransactionID);
                recordFound = false;
                List<tblussdmsisdninteraction> results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;

                return results;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdnInteraction - " + Ex.Message);
            }
        }
        public tblussdinstruction GetUssdInstructionUser(Int16 pInstructionGroupID, Int32 pTransactionID)
        {
            try
            {
                tblussdinstruction userInstruction = new tblussdinstruction();

                List<tblussdinstructiongroupitem> instructGroups = GetUssdInstructionGroupItem(pInstructionGroupID);
                tblussdmsisdninteraction lastInteraction = GetUssdMsisdnInteractionLast(pTransactionID);
                recordFound = false;
                if (lastInteraction.InstructionID != null)
                {
                    tblussdinstructiongroupitem lastInstr = instructGroups.FindAll(x => x.InstructionID == lastInteraction.InstructionID)[0];
                    instructGroups = instructGroups.FindAll(x => x.Ordinal > lastInstr.Ordinal);
                    if (instructGroups.Count > 0)
                    {
                        userInstruction = instructGroups[0].tblussdinstruction;
                        recordFound = true;
                    }
                }
                else
                {
                    userInstruction = instructGroups[0].tblussdinstruction;
                    recordFound = true;
                }

                return userInstruction;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdInstructionUser - " + Ex.Message);
            }
        }
        public tblbusinesstransaction GetBusinessTransaction(Int16 pUssdTransactionGroupID)
        {
            try
            {
                tblbusinesstransactionCrud crud = new tblbusinesstransactionCrud();
                crud.Where("UssdTransactionGroupID", MySQL.DAL.General.Operator.Equals, pUssdTransactionGroupID);
                recordFound = false;
                tblbusinesstransaction tran = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return tran;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetBusinessTransaction - " + Ex.Message);
            }
        }
        public Boolean DeleteUssdTagName(Int16 pTagNameID)
        {
            try
            {
                tblussdtagname tag = GetUssdTagNameMS(pTagNameID);
                tblussdtagnameCrud crud = new tblussdtagnameCrud();
                crud.Delete(tag);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteUssdTagName - " + Ex.Message);
            }
        }
        public Int16 InsertUssdTagNameMS(String pTagName)
        {
            try
            {
                tblussdtagname ins = new tblussdtagname();
                ins.TagName = pTagName;
                tblussdtagnameCrud crud = new tblussdtagnameCrud();
                long ID = 0;
                crud.Insert(ins, out ID);
                return (Int16)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdTagNameMS - " + Ex.Message);
            }
        }
        public tblussdtransactiongroup GetUssdTransactionGroupByGatewayTransaction(String pTransaction)
        {
            try
            {
                tblussdtransactiongroupCrud crud = new tblussdtransactiongroupCrud();
                crud.Where("GatewayTransaction", MySQL.DAL.General.Operator.Equals, pTransaction);
                recordFound = false;
                tblussdtransactiongroup trans = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return trans;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransaction - " + Ex.Message);
            }
        }
        public Int32 GetOrInsertUssdMsisdn(String pMsisdn)
        {
            try
            {
                tblussdmsisdn msisdn = GetUssdMsisdn(pMsisdn);
                Int32 msisdnID = 0;
                if (!recordFound)
                {
                    msisdnID = InsertUssdMsisdn(pMsisdn);
                }
                else
                    msisdnID = Convert.ToInt32(msisdn.UssdMsisdnID);

                return msisdnID;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdn - " + Ex.Message);
            }
        }
        public tblussdtransactiongroupitem GetUssdTransactionGroupItem(Int32 pUssdTransactionGroupID)
        {
            try
            {
                tblussdtransactiongroupitemCrud crud = new tblussdtransactiongroupitemCrud();
                crud.Where("UssdTransactionGroupID", MySQL.DAL.General.Operator.Equals, pUssdTransactionGroupID);
                recordFound = false;
                tblussdtransactiongroupitem res = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransactionGroupItem - " + Ex.Message);
            }
        }

        public List<tblussdtransactiongroupitem> GetUssdTransactionGroupItem(Int16 pUssdTransactionGroupID)
        {
            try
            {
                tblussdtransactiongroupitemCrud crud = new tblussdtransactiongroupitemCrud();
                crud.Where("UssdTransactionGroupID", MySQL.DAL.General.Operator.Equals, pUssdTransactionGroupID);
                recordFound = false;
                List<tblussdtransactiongroupitem> res = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;

                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransactionGroupItem - " + Ex.Message);
            }
        }

        public tblussdtransactiongroupitem GetUssdTransactionGroupItem(Int16 pUssdTransactionGroupID, Int16 pUssdTransactionID)
        {
            try
            {
                tblussdtransactiongroupitemCrud crud = new tblussdtransactiongroupitemCrud();
                crud.Where("UssdTransactionGroupID", MySQL.DAL.General.Operator.Equals, pUssdTransactionGroupID);
                crud.And("UssdTransactionID", MySQL.DAL.General.Operator.Equals, pUssdTransactionID);
                recordFound = false;
                tblussdtransactiongroupitem res = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransactionGroupItem - " + Ex.Message);
            }
        }

        public tblbusiness GetBusiness(String pBusinessName)
        {
            try
            {
                tblbusinessCrud crud = new tblbusinessCrud();
                crud.Where("Business", General.Operator.Equals, pBusinessName);
                recordFound = false;
                tblbusiness bus = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return bus;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetBusiness - " + Ex.Message);
            }
        }
        public tblbusinesstransaction GetBusinessTransaction(Int32 pUssdTransactionID)
        {
            try
            {
                tblussdtransactiongroupitem groupItem = GetUssdTransactionGroupItem((Int32)pUssdTransactionID);
                if (recordFound)
                {
                    tblbusinesstransactionCrud crud = new tblbusinesstransactionCrud();
                    crud.Where("UssdTransactionGroupID", MySQL.DAL.General.Operator.Equals, Convert.ToInt16(groupItem.UssdTransactionGroupID));
                    recordFound = false;
                    tblbusinesstransaction tran = crud.ReadSingle();
                    recordFound = crud.RecordFound;
                    return tran;
                }

                return null;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetBusinessTransaction - " + Ex.Message);
            }
        }
        public Int32 InsertUssdMsisdnTransaction(Int32 pUssdMsisdnID, Int32? pSessionID, Int16? pUssdTransactionGroupID, Byte? pValidationErrorID)
        {
            try
            {
                tblussdmsisdntransaction tran = new tblussdmsisdntransaction();
                tran.TransactionDate = DateTime.Now;
                tran.UssdMsisdnID = pUssdMsisdnID;
                tran.SessionID = pSessionID;
                tran.UssdTransactionGroupID = pUssdTransactionGroupID;
                tran.ValidationMessageID = pValidationErrorID;
                tblussdmsisdntransactionCrud crud = new tblussdmsisdntransactionCrud();
                long ID = 0;
                crud.Insert(tran, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdMsisdnTransaction - " + Ex.Message);
            }
        }
        public List<tblussdmsisdntransaction> GetUssdMsisdnTransactionByUssdMsisdnID(Int32 pUssdMsisdnID)
        {
            try
            {
                tblussdmsisdntransactionCrud crud = new tblussdmsisdntransactionCrud();
                crud.Where("UssdMsisdnID", MySQL.DAL.General.Operator.Equals, pUssdMsisdnID);
                recordFound = false;
                List<tblussdmsisdntransaction> tran = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;

                return tran;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdnTransactionByUssdMsisdnID - " + Ex.Message);
            }
        }
        public Int32 GetUssdMsisdnTransactionLastSession(Int32 pUssdMsisdnID)
        {
            try
            {
                Int32 retVal = 0;
                List<tblussdmsisdntransaction> trans = GetUssdMsisdnTransactionByUssdMsisdnID(pUssdMsisdnID);
                if (trans.Count == 0)
                    return 1;
                else
                    retVal = trans.Max(x => x.SessionID).Value;

                retVal = retVal + 1;
                return retVal++;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdnTranactionLastSession - " + Ex.Message);
            }
        }
        public tblussdmenu GetUssdMenuByUssdTransactionGroupID(Int16 pUssdTransactionGroupID, Byte pGatewayID)
        {
            try
            {
                tblussdmenuCrud crud = new tblussdmenuCrud();
                crud.Where("UssdTransactionGroupID", MySQL.DAL.General.Operator.Equals, pUssdTransactionGroupID);
                crud.And("GatewayID", MySQL.DAL.General.Operator.Equals, pGatewayID);
                recordFound = false;
                crud.ExcludeReferenceTable = excludeRefTable;
                tblussdmenu results = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return results;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuByRegistryKey - " + Ex.Message);
            }
        }
        public tblussdmenu GetUssdMenuByParentIDMS(Int32 pParentUssdMenuID)
        {
            try
            {
                recordFound = false;
                tblussdmenuCrud reader = new tblussdmenuCrud();
                reader.Where("ParentUssdMenuID", MySQL.DAL.General.Operator.Equals, pParentUssdMenuID);
                tblussdmenu menus = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return menus;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuByIDMS - " + Ex.Message);
            }
        }
        public tblussdmenu GetUssdMenuByIDMS(Int32 pUssdMenuID)
        {
            try
            {
                recordFound = false;
                tblussdmenuCrud reader = new tblussdmenuCrud();
                reader.Where("UssdMenuID", MySQL.DAL.General.Operator.Equals, pUssdMenuID);
                tblussdmenu menus = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return menus;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuByIDMS - " + Ex.Message);
            }
        }
        public List<tblussdmenu> CollectUssdMenu(Int16 pUssdTransactionGroupID, Byte pGatewayID)
        {
            try
            {
                List<tblussdmenu> menuRet = new List<tblussdmenu>();
                List<tblussdmenu> menuRetEntry = new List<tblussdmenu>();

                tblussdmenu menu = GetUssdMenuByUssdTransactionGroupID(pUssdTransactionGroupID, pGatewayID);
                tblussdmenu topMenu = menu;


                Int32 ussdMenuID = Convert.ToInt32(menu.UssdMenuID);
                Int32? pParentID = menu.ParentUssdMenuID;
                while (ussdMenuID != 0)
                {
                    menu = new tblussdmenu();
                    menu = GetUssdMenuByParentIDMS(ussdMenuID);
                    if (!recordFound) break;
                    ussdMenuID = Convert.ToInt32(menu.UssdMenuID);
                    menuRetEntry.Add(menu);
                }

                menuRet.Add(topMenu);

                while (pParentID != null && pParentID != 0)
                {
                    menu = new tblussdmenu();
                    menu = GetUssdMenuByIDMS((Int32)pParentID);
                    pParentID = Convert.ToInt32(menu.ParentUssdMenuID);
                    menuRet.Insert(0, menu);
                }


                foreach (tblussdmenu menuAdd in menuRetEntry)
                    menuRet.Add(menuAdd);
                return menuRet;
            }
            catch (Exception Ex)
            {
                throw new Exception("CollectUssdMenu - " + Ex.Message);
            }
        }
        public Int32 InsertUssdInteraction(Int32 pTransactionID, Int32? pUssdMenuID, String pValueEntered, Int16? pUssdActionID, String pUssdActionResult, Boolean pValidationError)
        {
            try
            {

                tblussdinteraction interaction = new tblussdinteraction();
                interaction.InteractionDateTime = DateTime.Now;
                interaction.TransactionID = pTransactionID;
                interaction.UssdActionID = pUssdActionID;
                interaction.UssdMenuID = pUssdMenuID;
                interaction.ValidationError = pValidationError;
                interaction.ValueEntered = pValueEntered;
                interaction.UssdActionResult = pUssdActionResult;
                tblussdinteractionCrud crud = new tblussdinteractionCrud();
                long ID = 0;
                crud.Insert(interaction, out ID);

                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdInteraction - " + Ex.Message);
            }
        }
        public List<tblussdtransactionprocessinstruction> GetUssdTransactionProcessInstruction(Int32 pUssdTransactionID)
        {
            try
            {
                tblussdtransactionprocessinstructionCrud crud = new tblussdtransactionprocessinstructionCrud();
                crud.Where("UssdTransactionGroupID", MySQL.DAL.General.Operator.Equals, pUssdTransactionID);
                recordFound = false;
                List<tblussdtransactionprocessinstruction> res = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;

                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdProcessTransaction - " + Ex.Message);
            }
        }
        public tblussdtransactionprocessinstructiontag GetUssdTransactionProcessInstructionTagMS(Int16 pTranProcessID, Int16 pTagNameID)
        {
            try
            {
                tblussdtransactionprocessinstructiontagCrud crud = new tblussdtransactionprocessinstructiontagCrud();
                crud.Where("TranProcessID", MySQL.DAL.General.Operator.Equals, pTranProcessID);
                crud.And("TagNameID", MySQL.DAL.General.Operator.Equals, pTagNameID);
                recordFound = false;
                tblussdtransactionprocessinstructiontag res = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransactionProcessInstruction - " + Ex.Message);
            }
        }
        public List<tblussdtransactionprocessinstructiontag> GetUssdTransactionProcessInstructionTagMS(Int16 pTranProcessID)
        {
            try
            {
                tblussdtransactionprocessinstructiontagCrud crud = new tblussdtransactionprocessinstructiontagCrud();
                crud.Where("TranProcessID", MySQL.DAL.General.Operator.Equals, pTranProcessID);
                recordFound = false;
                List<tblussdtransactionprocessinstructiontag> res = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransactionProcessInstruction - " + Ex.Message);
            }
        }
        public tblussdvalidationerror GetUssdValidationErrorMS(Byte pValidationErrorID)
        {
            try
            {
                tblussdvalidationerrorCrud crud = new tblussdvalidationerrorCrud();
                crud.Where("ValidationErrorID", MySQL.DAL.General.Operator.Equals, pValidationErrorID);
                recordFound = false;
                tblussdvalidationerror result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdValidationError - " + Ex.Message);
            }
        }
        public tblussdvalidationerror GetUssdValidationErrorMS(String pValidationError)
        {
            try
            {
                tblussdvalidationerrorCrud crud = new tblussdvalidationerrorCrud();
                crud.Where("ValidationError", MySQL.DAL.General.Operator.Equals, pValidationError);
                recordFound = false;
                tblussdvalidationerror result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdValidationError - " + Ex.Message);
            }
        }
        internal String ExtractFromXML(String pTagName, Byte pValidationErrorID)
        {
            try
            {
                XmlDocument workDoc = ussdInformation.xmlIncomingDoc;
                String returnValue = "";
                if (workDoc.GetElementsByTagName(pTagName).Count > 0)
                {
                    if (workDoc.GetElementsByTagName(pTagName)[0].ChildNodes.Count > 0)
                    {
                        returnValue = workDoc.GetElementsByTagName(pTagName)[0].ChildNodes[0].Value;
                    }
                    else
                    {
                        tblussdvalidationerror error = GetUssdValidationErrorMS(pValidationErrorID);
                        ussdInformation.ResponseMessage = error.ValidationError;
                        ussdInformation.ResponseCode = "09";
                    }
                }
                else
                {
                    tblussdvalidationerror error = GetUssdValidationErrorMS(pValidationErrorID);
                    ussdInformation.ResponseMessage = error.ValidationError;
                    ussdInformation.ResponseCode = "10";
                }

                return returnValue;
            }
            catch (Exception Ex)
            {
                throw new Exception("ExtractFromXML - " + Ex.Message);
            }
        }
        public List<tblussdtagname> GetUssdTagNameMS()
        {
            try
            {
                recordFound = false;
                tblussdtagnameCrud crud = new tblussdtagnameCrud();
                List<tblussdtagname> tagName = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return tagName;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTagNameMS-" + Ex.Message);
            }
        }
        public tblussdtagname GetUssdTagNameMS(Int16 pTagNameID)
        {
            try
            {
                recordFound = false;
                tblussdtagnameCrud crud = new tblussdtagnameCrud();
                crud.Where("TagNameID", MySQL.DAL.General.Operator.Equals, pTagNameID);
                tblussdtagname tagName = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return tagName;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTagNameMS-" + Ex.Message);
            }
        }
        public tblussdtagname GetUssdTagNameMS(String pTagName)
        {
            try
            {
                recordFound = false;
                tblussdtagnameCrud crud = new tblussdtagnameCrud();
                crud.Where("TagName", MySQL.DAL.General.Operator.Equals, pTagName);
                tblussdtagname tagName = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return tagName;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTagNameMS-" + Ex.Message);
            }
        }
        public tblussdvalidationmessage GetUssdValidationMessage(String pMessage)
        {
            try
            {
                tblussdvalidationmessageCrud crud = new tblussdvalidationmessageCrud();
                crud.Where("ValidationMessage", MySQL.DAL.General.Operator.Equals, pMessage);
                recordFound = false;
                tblussdvalidationmessage mess = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return mess;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdValidationMessage - " + Ex.Message);
            }
        }
        public tblussdmsisdntransaction GetUssdMsisdnTransaction(Int32 pTransactionID)
        {
            try
            {
                tblussdmsisdntransactionCrud crud = new tblussdmsisdntransactionCrud();
                crud.Where("TransactionID", MySQL.DAL.General.Operator.Equals, pTransactionID);
                recordFound = false;
                tblussdmsisdntransaction tran = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return tran;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdnTransaction - " + Ex.Message);
            }
        }
        public tblussdmsisdntransaction GetUssdMsisdnTransaction(Int32 pMsisdnID, Int32 pSessionID)
        {
            try
            {
                tblussdmsisdntransactionCrud crud = new tblussdmsisdntransactionCrud();
                crud.Where("UssdMsisdnID", MySQL.DAL.General.Operator.Equals, pMsisdnID);
                crud.And("SessionID", MySQL.DAL.General.Operator.Equals, pSessionID);
                recordFound = false;
                tblussdmsisdntransaction tran = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return tran;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMsisdnTransaction - " + Ex.Message);
            }
        }
        public Boolean UpdateUssdMsisdnTransaction(Int32 pTransactionID, Int16? pUssdTransactionID, Int16? pValidationMessageID, Int16? pInstructionGroupID)
        {
            try
            {
                if (pUssdTransactionID == null && pValidationMessageID == null && pInstructionGroupID == null) return true;
                tblussdmsisdntransaction tran = GetUssdMsisdnTransaction(pTransactionID);

                if (pUssdTransactionID != null)
                    tran.UssdTransactionGroupID = pUssdTransactionID;

                if (pValidationMessageID != null)
                    tran.ValidationMessageID = pValidationMessageID;

                if (pInstructionGroupID != null)
                    tran.InstructionGroupID = pInstructionGroupID;

                tblussdmsisdntransactionCrud crud = new tblussdmsisdntransactionCrud();
                crud.Update(tran);

                return true;

            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdMsisdnTransaction - " + Ex.Message);
            }
        }
        public tblussdtranslate GetUssdTranslate(String pKeyword)
        {
            try
            {
                tblussdtranslateCrud crud = new tblussdtranslateCrud();
                crud.Where("Keyword", MySQL.DAL.General.Operator.Equals, pKeyword);
                recordFound = false;
                tblussdtranslate translate = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return translate;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTranslate - " + Ex.Message);
            }
        }
        public String Translate(String pKeyword)
        {
            try
            {
                String retString = pKeyword;
                List<String> keywords = new List<string>();
                while (retString.Contains("{?"))
                {
                    retString = retString.Substring(retString.IndexOf("{?"));
                    keywords.Add(retString.Substring(0, retString.IndexOf("}") + 1));
                    retString = retString.Substring(retString.IndexOf("}") + 1);
                }

                foreach (String keyword in keywords)
                {
                    tblussdtranslate trans = GetUssdTranslate(keyword);
                    if (!recordFound)
                    {
                        
                        InsertLog(ussdInformation.ApplicationID, ussdInformation.SubApplicationID, "Translation not found for keyword " + pKeyword, eLogType.Error);
                        return pKeyword;
                    }

                    String translation = trans.Translation.Replace("{?msisdn}", "'" + ussdInformation.Msisdn + "'").Replace("{?TransactionID}", ussdInformation.UssdMsisdnTransactionID.ToString());
                    String transWith = "";
                    switch (trans.UssdTranslateTypeID)
                    {
                        case 1:
                            try
                            {
                                System.Data.DataTable table = ExecuteMSQuery(translation);
                                transWith = table.Rows[0][0].ToString();
                            }
                            catch (Exception Ex)
                            {
                                InsertLog(ussdInformation.ApplicationID, ussdInformation.SubApplicationID, "Tranlation Error = " + Ex.Message + " | Keyword = " + keyword + " Query = " + translation, eLogType.Error);
                                return "";
                            }
                            break;
                        case 3:
                            switch (keyword)
                            {
                                case "{?amount}":
                                    transWith = ussdInformation.Amount.ToString();
                                    break;
                                case "{?ReferenceNumber}":
                                    transWith = ussdInformation.ReferenceNumber;
                                    break;

                            }
                            break;
                    }

                    pKeyword = pKeyword.Replace(keyword, transWith);
                }

                return pKeyword;
            }
            catch (Exception Ex)
            {
                throw new Exception("Translate - " + Ex.Message);
            }
        }
        public Decimal GetUssdTransactionAmount(Int16 pTransactionGroupID)
        {
            try
            {
                List<tblussdtransactiongroupitem> tranGroupItems = GetUssdTransactionGroupItem(pTransactionGroupID);
                Decimal sumAmount = 0;
                foreach (tblussdtransactiongroupitem tran in tranGroupItems)
                {
                    if (!String.IsNullOrEmpty(tran.tblussdtransaction.FormulaAmount))
                        if (tran.tblussdtransaction.FormulaAmount == "{?EnteredAnount}")
                        {
                            sumAmount = Convert.ToDecimal(Translate("{?EnteredAmount}"));
                        }
                        else
                        {
                            String formula = Translate(tran.tblussdtransaction.FormulaAmount);
                            Decimal months = Convert.ToDecimal(formula.Trim().Substring(0, formula.IndexOf("*") - 1));
                            Decimal amount = Convert.ToDecimal(formula.Trim().Substring(formula.IndexOf("*") + 1));
                            sumAmount = sumAmount + (months * amount);
                        }
                    else
                    {
                        if (tran.tblussdtransaction.TransactionAmount != null)
                        {
                            sumAmount += Convert.ToDecimal(tran.tblussdtransaction.TransactionAmount);
                        }
                        else
                            sumAmount = 0;
                    }
                }
                return sumAmount;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransactionAmount - " + Ex.Message);
            }
        }

        public UssdInformation ExtractBotswanaRailwaysInformation(List<tblussdtransactionprocessinstruction> instructions, List<tblussdtransactionprocessinstructiontag> instructionTags, List<tblussdmenu> menuChoices)
        {
            try
            {
                #region Extract Surname
                tblussdmenu ussdMenu = new tblussdmenu();
                List<tblussdtransactionprocessinstruction> insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Surname");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Surname");
                    ussdInformation.Surname= ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("Surname").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.Surname, null, "", false);
                }
                #endregion
                #region Extract Initials
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Initials");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Initials");
                    ussdInformation.Initials = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("Initials").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.Initials, null, "", false);
                }
                #endregion
                #region Extract Passenger Cell
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Passenger Cell");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Passenger Cell");
                    ussdInformation.PassengerCell = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("PassengerCell").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.PassengerCell, null, "", false);
                }
                #endregion
                #region Extract Email Address
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Email Address");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Email Address");
                    ussdInformation.EmailAddress = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("Email").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.EmailAddress, null, "", false);
                }
                #endregion
                #region Extract Departure Date
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Departure Date");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Departure Date");
                    String departDate = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    DateTime departureDate = DateTime.Now;
                    if (!DateTime.TryParse(departDate, out departureDate))
                    {
                        ussdInformation.ResponseCode = "35";
                        ussdInformation.ResponseMessage = "The date entered is not valid. (" + departDate + ")";
                        Int16 validationMessageID = Convert.ToInt16(GetUssdValidationMessage("The date entered is not valid.").ValidationMessageID);
                        UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                        InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.DepartureDate.ToString() , null, "", true);
                        return ussdInformation;
                    }
                    ussdInformation.DepartureDate = departureDate;
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("DepartDate").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.DepartureDate.ToString(), null, "", false);
                }
                #endregion
                #region Extract Passenger ID Number
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Passenger ID Number");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Passenger ID Number");
                    ussdInformation.IDNumber = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("PassengerID").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.IDNumber, null, "", false);
                }
                #endregion
                #region Extract Next Of Kin Contact Number
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Next Of Kin Contact Number");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Next Of Kin Contact Number");
                    ussdInformation.NextOfKinContact = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("NextOfKinContact").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.NextOfKinContact, null, "", false);
                }
                #endregion

                return ussdInformation;
            }
            catch (Exception Ex)
            {
                throw new Exception("ExtractBotswanaRailwaysInformation - " + Ex.Message);
            }
        }
        public UssdInformation ProcessUssdRequest(String pCallerPage, XmlDocument pXmlDocument)
        {
            try
            {

                String logInfo = pCallerPage;
                ussdInformation.LogInfo = logInfo;
                
                Int32 applicationID = Convert.ToInt32(GetApplication("DPO.BackOFfice.Ussd").ApplicationID);
                Int32 subApplicationID = Convert.ToInt32(GetSubApplication(applicationID, "Ussd Process Handler").SubApplicationID);
                ussdInformation.ApplicationID = applicationID;
                ussdInformation.SubApplicationID = subApplicationID;
                const String unexpectedError = "An unexpected error has occured. The system administrator was notified. Sorry for the inconvenience.";
                String validationMessage = "";
                Int16? validationMessageID = 0;

                ussdInformation.CallerPage = pCallerPage;
                Byte mobileProviderID = 1;
                Byte gatewayID = 2;
                if (pCallerPage.ToLower() == "mascom")
                {
                    mobileProviderID = 2;
                    gatewayID = 3;
                }
                ussdInformation.xmlIncomingDoc = pXmlDocument;
                if (ExtractUssdMsisdn() != "00")
                {
                    InsertLog(logInfo, eLogType.Information);
                    return ussdInformation;
                }

                //UpdateProcessTrace(ProcessUpdate.WebPageCalled);
                logInfo += "|MsisdnID=" + ussdInformation.UssdMsisdnID.ToString() + "|UssdTransactionID=" + ussdInformation.UssdTransactionID.ToString();
                ussdInformation.LogInfo += logInfo;
                tblbusinesstransaction busTran = GetBusinessTransaction(ussdInformation.UssdTransactionGroupID);
                ussdInformation.UssdMsisdnTransactionID = InsertUssdMsisdnTransaction(ussdInformation.UssdMsisdnID, GetUssdMsisdnTransactionLastSession(ussdInformation.UssdMsisdnID), ussdInformation.UssdTransactionGroupID, null);
                //BuildUssdInteraction

                List<tblussdmenu> menuChoices = CollectUssdMenu(ussdInformation.UssdTransactionGroupID, gatewayID);
                //menuChoices = menuChoices.FindAll(x => x.MenuItemTextID != null);
                foreach (tblussdmenu menu in menuChoices)
                    if (menu.MenuItemTextID != null)
                        InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, menu.UssdMenuID, menu.MenuItemNumber.ToString(), null, "", false);

                List<tblussdtransactionprocessinstruction> instructions = GetUssdTransactionProcessInstruction(ussdInformation.UssdTransactionGroupID);
                List<tblussdtransactionprocessinstructiontag> instructionTags = new List<tblussdtransactionprocessinstructiontag>();
                foreach (tblussdtransactionprocessinstruction instruction in instructions)
                {
                    List<tblussdtransactionprocessinstructiontag> instructionTagsCollect = GetUssdTransactionProcessInstructionTagMS(Convert.ToInt16(instruction.TranProcessID));
                    foreach (tblussdtransactionprocessinstructiontag tag in instructionTagsCollect)
                        instructionTags.Add(tag);
                }

                /*if (instructionTags.Count==0)
                {
                    ussdInformation.ResponseCode = "29";
                    ussdInformation.ResponseMessage = "No insturctions was set up for transaction  (" + ussdInformation.UssdTransaction + ")";
                    validationMessageID = Convert.ToInt16(GetUssdValidationMessage("No instructions was set up for transaction.").ValidationMessageID);
                    UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null,  "", null, "", true);
                    return ussdInformation;
                }*/

                tblussdmenu ussdMenu = new tblussdmenu();
                //Extract MultiChoice SmartCard
                List<tblussdtransactionprocessinstruction> insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract MultiChoice SmartCard Number");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && Convert.ToInt16(x.tblussdtransactionprocessinstruction.ProcessInstructionID) == 5);
                    ussdInformation.MCASmartCard = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    logInfo += "|SmartCardNumber=" + ussdInformation.MCASmartCard;
                    ussdInformation.LogInfo += logInfo;
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("SmartCard").TagNameID));
                    if (ussdInformation.MCASmartCard.Length != 10 && ussdInformation.MCASmartCard.Length != 11)
                    {
                        ussdInformation.ResponseCode = "11";
                        ussdInformation.ResponseMessage = "The smartcard number entered is incorrect. (" + ussdInformation.MCASmartCard + ")";
                        validationMessageID = Convert.ToInt16(GetUssdValidationMessage("The smartcard number entered is incorrect.").ValidationMessageID);
                        UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                        InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.MCASmartCard, null, "", true);
                        InsertLog(logInfo + ussdInformation.ResponseMessage, eLogType.Information);
                        return ussdInformation;
                    }

                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.MCASmartCard, null, "", true);
                }


                #region Extract Bank Card Information
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Bank Card Number");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && Convert.ToInt16(x.tblussdtransactionprocessinstruction.ProcessInstructionID) == 3 && x.tblussdtagname.TagName == "CardNumber");
                    ussdInformation.BankCardNumber = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && Convert.ToInt16(x.tblussdtransactionprocessinstruction.ProcessInstructionID) == 3 && x.tblussdtagname.TagName == "ExpiryDate");
                    ussdInformation.ExpiryDate = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && Convert.ToInt16(x.tblussdtransactionprocessinstruction.ProcessInstructionID) == 3 && x.tblussdtagname.TagName == "Cvc");
                    ussdInformation.Cvc = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("CardNumber").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), "Bank card number entered", null, "", false);
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("ExpiryDate").TagNameID));
                    Int32 iExpiry = 0;
                    if (!Int32.TryParse(ussdInformation.ExpiryDate, out iExpiry))
                    {
                        ussdInformation.ResponseCode = "12";
                        ussdInformation.ResponseMessage = "Expiry Date not numeric";
                        //UssdValidationErrorToTrace("Expiry Date not numeric");
                        validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Expiry Date not numeric").ValidationMessageID);
                        UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                        InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), "Expiry Date Entered", null, "", true);
                        return ussdInformation;
                    }

                    if (ussdInformation.ExpiryDate.Length != 4)
                    {
                        ussdInformation.ResponseCode = "13";
                        ussdInformation.ResponseMessage = "Expiry Date must be four digits MMYY";
                        //UssdValidationErrorToTrace("Expiry Date must be four digits MMYY");
                        validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Expiry Date must be four digits MMYY").ValidationMessageID);
                        UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                        InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), "Expiry Date Entered", null, "", true);

                        return ussdInformation;
                    }

                    Int32 month = Convert.ToInt32(ussdInformation.ExpiryDate.Substring(0, 2));
                    if (month < 1 || month > 12)
                    {
                        ussdInformation.ResponseCode = "13";
                        ussdInformation.ResponseMessage = "Invalid expiry month must be 01-12";
                        //UssdValidationErrorToTrace("Invalid expiry month must be 01-12");
                        validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Invalid expiry month must be 01-12").ValidationMessageID);
                        UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                        InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), "Expiry Date Entered", null, "", true);
                        return ussdInformation;
                    }

                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), "Expiry Date Entered", null, "", false);

                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("Cvc").TagNameID));

                    if (!Int32.TryParse(ussdInformation.Cvc, out iExpiry))
                    {
                        ussdInformation.ResponseCode = "13";
                        ussdInformation.ResponseMessage = "Cvc Not numeric";
                        //UssdValidationErrorToTrace("Cvc Not numeric");
                        validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Cvc Not numeric").ValidationMessageID);
                        UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                        InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), "Cvc Entered", null, "", true);
                        return ussdInformation;
                    }

                    if (ussdInformation.Cvc.Length != 3)
                    {
                        ussdInformation.ResponseCode = "13";
                        ussdInformation.ResponseMessage = "Cvc must be three digits";
                        //UssdValidationErrorToTrace("Cvc must be three digits");
                        validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Expiry Date not numeric").ValidationMessageID);
                        UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                        InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), "Cvc Entered", null, "", true);
                        return ussdInformation;
                    }

                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), "Cvc Entered", null, "", false);
                }
                #endregion
                #region Extract Number of Months
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Number Of Months");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Number Of Months");
                    ussdInformation.NumberOfMonths = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("NoOfMonths").TagNameID));
                    Int32 iExpiry = 0;
                    if (!Int32.TryParse(ussdInformation.ExpiryDate, out iExpiry))
                    {
                        ussdInformation.ResponseCode = "14";
                        ussdInformation.ResponseMessage = "Number of months not numeric";
                        validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Number of months not numeric").ValidationMessageID);
                        UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                        InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.NumberOfMonths, null, "", true);
                        return ussdInformation;
                    }

                    Int32 month = Convert.ToInt32(ussdInformation.ExpiryDate.Substring(2, 2));
                    if (month < 1 || month > 12)
                    {
                        ussdInformation.ResponseCode = "15";
                        ussdInformation.ResponseMessage = "Invalid number of month must be 01-12";
                        validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Invalid number of month must be 01-12").ValidationMessageID);
                        UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                        InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.NumberOfMonths, null, "", true);
                        return ussdInformation;
                    }
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.NumberOfMonths, null, "", false);
                }
                #endregion
                #region Extract Amount
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Amount");
                if (insFound.Count > 0)
                {
                    ussdInformation.AmountFromXML = true;
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Amount");
                    String sAmount = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("Amount").TagNameID));
                    Decimal dAmount = 0;
                    if (!Decimal.TryParse(sAmount, out dAmount))
                    {
                        ussdInformation.ResponseCode = "16";
                        ussdInformation.ResponseMessage = "Amount not numeric";
                        //UssdValidationErrorToTrace("Amount not numeric");
                        validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Amount not numeric").ValidationMessageID);
                        UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                        InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), sAmount, null, "", true);
                        return ussdInformation;
                    }
                    ussdInformation.Amount = dAmount;
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), sAmount, null, "", false);
                }
                #endregion
                #region Extract Meter Number
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Meter Number");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Meter Number");
                    ussdInformation.MeterNumber = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("MeterNumber").TagNameID));
                    Decimal meterNoo = 0;
                    if (!Decimal.TryParse(ussdInformation.MeterNumber, out meterNoo))
                    {
                        ussdInformation.ResponseCode = "17";
                        ussdInformation.ResponseMessage = "Meter number must be numeric digits only";
                        validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Meter Number must be numeric digits only").ValidationMessageID);
                        UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                        InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.MeterNumber, null, "", true);
                        InsertLog(logInfo + " " + ussdInformation.ResponseMessage , eLogType.Information);
                        return ussdInformation;
                    }

                    if (ussdInformation.MeterNumber.Length != 11)
                    {
                        ussdInformation.ResponseCode = "18";
                        ussdInformation.ResponseMessage = "Meter number must be eleven digits in length.";
                        validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Meter number must be eleven digits in length.").ValidationMessageID);
                        UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                        InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.MeterNumber, null, "", true);
                        return ussdInformation;
                    }

                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.MeterNumber, null, "", false);
                }
                #endregion
                #region Extract Invoice Number
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Invoice Number");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Invoice Number");
                    ussdInformation.InvoiceNumber = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("InvoiceNumber").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.InvoiceNumber, null, "", false);
                }
                #endregion
                #region Extract Box Holder ID Number
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Box Holder ID Number");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Box Holder ID Number");
                    ussdInformation.PostBoxIDNumber = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("BoxHolderIdentity").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.PostBoxIDNumber, null, "", false);
                }
                #endregion
                #region Extract Box Holder Name
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Box Holder Name");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Box Holder Name");
                    ussdInformation.PostBoxHolderName = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("BoxHolderName").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.PostBoxHolderName, null, "", false);
                }
                #endregion
                #region Extract Box Number
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Box Number");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Box Number");
                    ussdInformation.PostBoxNumber = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("BoxNumber").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.PostBoxNumber, null, "", false);
                }
                #endregion
                #region Extract Box Location
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Box Location");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Box Location");
                    ussdInformation.PostBoxLocation = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("BoxLocation").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.PostBoxLocation, null, "", false);
                }
                #endregion
                #region Extract Plot Number
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract plot number");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract plot number");
                    ussdInformation.PlotNumber = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("PlotNumber").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.PlotNumber, null, "", false);
                }
                #endregion
                #region Extract Application Number
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Application Number");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Application Number");
                    ussdInformation.ApplicationNumber = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("ApplicationNo").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.ApplicationNumber, null, "", false);
                }
                #endregion
                #region Extract Account Number
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Account Number");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Account Number");
                    ussdInformation.AccountNumber = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("AccountNumber").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.AccountNumber, null, "", false);
                }
                #endregion
                #region Extract Seat Number
                insFound = instructions.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == "Extract Seat Number");
                if (insFound.Count > 0)
                {
                    List<tblussdtransactionprocessinstructiontag> tagName = instructionTags.FindAll(x => x.TranProcessID == Convert.ToInt16(insFound[0].TranProcessID) && x.tblussdtransactionprocessinstruction.tblussdprocessinstruction.ProcessInstruction == "Extract Seat Number");
                    ussdInformation.SeatNumber = ExtractFromXML(tagName[0].tblussdtagname.TagName, Convert.ToByte(tagName[0].ValidationErrorID));
                    ussdMenu = menuChoices.Find(x => x.TagNameID == Convert.ToInt16(GetUssdTagNameMS("SeatNumber").TagNameID));
                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, Convert.ToInt32(ussdMenu.UssdMenuID), ussdInformation.SeatNumber, null, "", false);
                }
                #endregion


                if (busTran.tblbusiness.Business == "Botswana Railways")
                {
                    ussdInformation = ExtractBotswanaRailwaysInformation(instructions, instructionTags, menuChoices);
                    if (ussdInformation.ResponseCode != "00")
                    {
                        return ussdInformation;
                    }

                }

                #region Do Ussd Action
                String transactionDescription = "";
                String vendingMessage = "";
                String textMessage = "";
                //BuildUssdInteraction(ussdInformation.UssdMsisdnID, Convert.ToInt16(busTran.tblussdtransactiongroup.UssdTransactionGroupID));
                if (!ussdInformation.AmountFromXML)
                    ussdInformation.Amount = GetUssdTransactionAmount(Convert.ToInt16(busTran.tblussdtransactiongroup.UssdTransactionGroupID));
                List<tblussdtransactionaction> actions = GetUssdTransactionAction(ussdInformation.UssdTransactionGroupID);
                foreach (tblussdtransactionaction action in actions)
                {
                    switch (action.tblussdaction.UssdAction)
                    {
                        case "Bank Card Authorisation":
                            try
                            {
                                
                                if (busTran == null)
                                {
                                    InsertLog("Business Transaction not set up for transaction " + ussdInformation.UssdTransaction, eLogType.Error);
                                    ussdInformation.ResponseCode = "19";
                                    ussdInformation.ResponseMessage = unexpectedError;
                                    return ussdInformation;
                                }

                                String bankResponse = AuthorisationUssd(busTran.tblbusiness.TerminalID, Translate(action.tblussdtransactiongroup.tblussdtransactiondescription.TransactionDescription));
                                ussdInformation.BankResponse = bankResponse;
                                String bankResult = busTran.tblbusiness.TerminalID + "|" + ussdInformation.ReferenceNumber + "|" + bankResponse;
                                if (!bankResponse.ToLower().Contains("approved"))
                                {
                                    ussdInformation.ResponseCode = "20";
                                    ussdInformation.ResponseMessage = "Response from bank: " + bankResponse;
                                    tblussdvalidationmessage err = GetUssdValidationMessage(bankResponse);
                                    if (recordFound)
                                        validationMessageID = Convert.ToInt16(err.ValidationMessageID);
                                    else
                                        validationMessageID = InsertUssdValidationMessageMS(bankResponse);
                                    UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), bankResponse, true);
                                    return ussdInformation;
                                }
                                

                                /*String bankResponse = "Approved4455 ";
                                String referenceNo = System.DateTime.Now.ToString("yyyyMMddhhssmmm");
                                ussdInformation.ReferenceNumber = referenceNo;
                                transactionDescription = Translate(action.tblussdtransactiongroup.tblussdtransactiondescription.TransactionDescription);
                                ussdInformation.BankResponse = bankResponse;
                                ussdInformation.ReferenceNumber = System.DateTime.Now.ToString("yyyyMMddhhssmmm");*/

                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), bankResponse, false);
                            }
                            catch (Exception Ex)
                            {
                                InsertLog(action.tblussdaction.UssdAction + ": " + Ex.Message, eLogType.Error);
                                validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Bank Card Authorisation Excption").ValidationMessageID);
                                UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "Bank Card Authorisation Exception", true);
                                ussdInformation.ResponseCode = "29";
                                ussdInformation.ResponseMessage = unexpectedError;
                                //InsertTempGatewayTest(ussdInformation.UssdTransactionID, transactionDescription, vendingMessage, textMessage, validationMessageID);
                                InsertLog(logInfo + "Bank Card Authorisation Excption", eLogType.Information);
                                return ussdInformation;
                            }
                            break;
                        case "Check BR Availability":
                            CarmaInformation carmaInfo = new CarmaInformation();
                            carmaInfo.DepartDate = ussdInformation.DepartureDate.ToString();
                            carmaInfo.PassengerMobileNo = ussdInformation.PassengerCell;
                            carmaInfo.PassengerSurname = ussdInformation.Surname;
                            carmaInfo.PassengerEmail = ussdInformation.EmailAddress;
                            carmaInfo.NextOfKinContact = ussdInformation.NextOfKinContact;
                            carmaInfo.PassengerInitials = ussdInformation.Initials;
                            carmaInfo.CoachClass =  Translate("{?CoachClass}");
                            carmaInfo.FromPoint = GetBrPoint(Translate("{?BrFromPoint}")).PointCode;
                            carmaInfo.ToPoint = GetBrPoint(Translate("{?BrToPoint}")).PointCode;
                            carmaInfo.PassengerTitle = Translate("{?BrTitle}");
                            carmaInfo.TicketType = Translate("{?BrTicketType}");

                            carmaInfo = GetBrAvailability(carmaInfo);
                            break;
                        case "MultiChoice Payment":
                            try
                            {
                                List<tblussdtransactiongroupitem> tranGroupItems = GetUssdTransactionGroupItem(Convert.ToInt16(action.tblussdtransactiongroup.UssdTransactionGroupID));
                                List<String> prodUserKeys = new List<string>();
                                foreach (tblussdtransactiongroupitem grpItems in tranGroupItems)
                                {
                                    prodUserKeys.Add(grpItems.tblussdtransaction.TransactionValue);
                                }
                                String mcPayRes = DoMultiChoicePayment(prodUserKeys, busTran.tblbusiness.TerminalID, ussdInformation.UssdMsisdnID, ussdInformation.Amount);

                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "Receipt No.:" + mcPayRes, false);
                            }
                            catch (Exception Ex)
                            {
                                InsertLog(action.tblussdaction.UssdAction + ": " + Ex.Message, eLogType.Error);
                                validationMessageID = Convert.ToInt16(GetUssdValidationMessage("MultiChoice Payment Excption").ValidationMessageID);
                                UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "MultiChoice Payment Excption", true);
                                ussdInformation.ResponseCode = "29";
                                ussdInformation.ResponseMessage = unexpectedError;
                                //InsertTempGatewayTest(ussdInformation.UssdTransactionGroupID, transactionDescription, vendingMessage, textMessage, validationMessageID);
                                return ussdInformation;
                            }
                            break;
                        case "Send Text Message":
                            try
                            {
                                textMessage = Translate(busTran.tblussdtransactiongroup.tblussdtextmessage.TextMessage);
                                Int32 messageID = messBL.InsertTextMessage(ussdInformation.Msisdn, textMessage, "DPO.VirtualMessage.Ussd");
                                ussdInformation.VendingMessage = textMessage;
                                ussdInformation.SmsID = messageID;
                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), textMessage, false);
                            }
                            catch (Exception Ex)
                            {
                                InsertLog(action.tblussdaction.UssdAction + ": " + Ex.Message, eLogType.Error);
                                validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Send Text Message Excption").ValidationMessageID);
                                UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "Send Text Message Excption", true);
                                ussdInformation.ResponseCode = "28";
                                ussdInformation.ResponseMessage = unexpectedError;
                                //InsertTempGatewayTest(ussdInformation.UssdTransactionGroupID, transactionDescription, vendingMessage, textMessage, validationMessageID);
                                return ussdInformation;
                            }
                            break;
                        case "MultiChoice Reauthorisation":
                            String reAuthRes = DoMultiChoiceReathorisation(ussdInformation.MCASmartCard);
                            if (reAuthRes != "Success")
                            {
                                ussdInformation.ResponseCode = "21";
                                ussdInformation.ResponseMessage = "Response from MultiChoice: " + reAuthRes;
                            }
                            break;
                        /*case "MultiChoice Detail Update":
                            MultiChoiceDetailType detailType = MultiChoiceDetailType.CellPhone;

                            break;*/
                        case "Tswana Gas Order":
                            try
                            {
                                String city = Translate("{?TswanaCity}");
                                Int32 cityID = (Int32)GetTswanaGasCityMS(city).CityID;
                                String centre = Translate("{?TswanaCentre}");
                                String centreID = GetTswanaGasCentreMS(cityID, centre).CentreID.ToString();
                                String cylinder = Translate("{?GasCylinder}").Trim();
                                String cylinderID = GetTswanaGasCylinderMS(cylinder).CylinderID.ToString();
                                if (String.IsNullOrEmpty(ussdInformation.ReferenceNumber)) ussdInformation.ReferenceNumber = "";
                                Boolean cylReplace = Convert.ToBoolean(Convert.ToInt32(Translate("{?TswanaEmptyCylinder}")));
                                String orderNo = TswanaGasPlaceOrder(ussdInformation.Msisdn, centreID, cylinderID, ussdInformation.Amount.ToString(), ussdInformation.ReferenceNumber, !cylReplace, "");
                                if (orderNo.ToLower().Contains("error"))
                                {
                                    InsertLog(action.tblussdaction.UssdAction + ": " + orderNo, eLogType.Error);
                                    tblussdvalidationmessage valMess = GetUssdValidationMessage("Tswana Gas order exception");
                                    UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, Convert.ToInt16(valMess.ValidationMessageID), null);
                                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "Tswana Gas order exception", true);
                                    ussdInformation.ResponseCode = "21";
                                    ussdInformation.ResponseMessage = "Response from Tswana Gas: " + orderNo;
                                    //InsertTempGatewayTest(ussdInformation.UssdTransactionGroupID, transactionDescription, vendingMessage, textMessage, validationMessageID);
                                    return ussdInformation;
                                }
                                else
                                {
                                    String orderNumber = orderNo.Replace("\"", "").Replace("{", "").Replace("}", "").Replace("order_id:", "");
                                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), orderNumber, true);
                                }
                            }
                            catch (Exception Ex)
                            {
                                InsertLog(action.tblussdaction.UssdAction + ": " + Ex.Message, eLogType.Error);
                                validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Tswana Gas Order Excption").ValidationMessageID);
                                UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "Tswana Gas Order Excption", true);
                                ussdInformation.ResponseCode = "27";
                                ussdInformation.ResponseMessage = unexpectedError;
                                return ussdInformation;
                            }
                            break;
                        case "Validate BPC Meter Number":
                            tblussdvalidationmessage mess = ValidateBPCMeterNo(ussdInformation.MeterNumber, ussdInformation.Amount, ussdInformation.UssdMsisdnID);
                            validationMessageID = Convert.ToInt16(mess.ValidationMessageID);
                            if (mess.ValidationMessage != "")
                            {
                                InsertLog(action.tblussdaction.UssdAction + ": " + mess.ValidationMessage, eLogType.Error);
                                UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), mess.ValidationMessage, true);
                                ussdInformation.ResponseCode = "29";
                                ussdInformation.ResponseMessage = mess.ValidationMessage;
                                //InsertTempGatewayTest(ussdInformation.UssdTransactionGroupID, transactionDescription, vendingMessage, textMessage, validationMessageID);
                                return ussdInformation;
                            }
                            InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "BPC Card Validated", true);
                            break;
                        case "Issue BPC Electricity Token":
                            try
                            {
                                String meterNo = ussdInformation.MeterNumber;
                                InsertLog(meterNo, eLogType.Debug);
                                String resVending = DoBotswanaPowerPayment(meterNo, ussdInformation.Amount, ussdInformation.UssdMsisdnID);
                                if (resVending != "Success")
                                {
                                    InsertThirdPartyVendingMS((Int16)ussdInformation.UssdTransactionGroupID, ussdInformation.UssdMsisdnID, busTran.tblbusiness.TerminalID, ussdInformation.ReferenceNumber, resVending, false, ussdInformation.Amount, mobileProviderID, ussdInformation.BankResponse);
                                    ussdInformation.ResponseCode = "22";
                                    ussdInformation.ResponseMessage = "Message from Botswana Power:" + resVending;
                                    return ussdInformation;
                                }
                            }
                            catch (Exception Ex)
                            {
                                InsertLog(action.tblussdaction.UssdAction + ": " + Ex.Message, eLogType.Error);
                                validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Issue BPC Electricity Token Excption").ValidationMessageID);
                                UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "Issue BPC Electricity Token Excption", true);
                                ussdInformation.ResponseCode = "26";
                                ussdInformation.ResponseMessage = Ex.Message;
                                //InsertTempGatewayTest(ussdInformation.UssdTransactionGroupID, transactionDescription, vendingMessage, textMessage, validationMessageID);
                                return ussdInformation;
                            }
                            break;
                        case "Validate Concerotel Invoice":
                            try
                            {
                                InsertLog("ussdInformation.InvoiceNumber=" + ussdInformation.InvoiceNumber + "|" + ussdInformation.Amount.ToString(), eLogType.Information);
                                ValidateConcerotelInvoice(ussdInformation.InvoiceNumber, out validationMessage, ussdInformation.Amount);
                                if (validationMessage != "")
                                {
                                    validationMessageID = Convert.ToInt16(GetUssdValidationMessage(ussdInformation.ResponseMessage).ValidationMessageID);
                                    UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), validationMessage, true);
                                    ussdInformation.ResponseCode = "33";
                                    ussdInformation.ResponseMessage = validationMessage;
                                    return ussdInformation;
                                }
                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "Concerotel Invoice Validated", false);
                            }
                            catch (Exception Ex)
                            {
                                InsertLog(ussdInformation.ApplicationID, ussdInformation.SubApplicationID, action.tblussdaction.UssdAction + ": " + Ex.Message, eLogType.Error);
                                validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Validate Concerotel Invoice Exception").ValidationMessageID);
                                UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "Validate Concerotel Invoice Exception", true);
                                ussdInformation.ResponseCode = "25";
                                ussdInformation.ResponseMessage = unexpectedError;
                                //InsertTempGatewayTest(ussdInformation.UssdTransactionGroupID, transactionDescription, vendingMessage, textMessage, validationMessageID);
                                return ussdInformation;

                            }
                            break;
                        case "Get MultiChoice Customer Detail":
                            try
                            {
                                tblussdvalidationmessage valMess = ValidateMultiChoiceSmartCardNumber(ussdInformation.MCASmartCard, ussdInformation.UssdMsisdnID);
                                if (valMess.ValidationMessageID != null)
                                {
                                    UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, Convert.ToInt16(valMess.ValidationMessageID), null);
                                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), valMess.ValidationMessage, true);
                                    ussdInformation.ResponseCode = "23";
                                    ussdInformation.ResponseMessage = valMess.ValidationMessage;
                                    //InsertTempGatewayTest(ussdInformation.UssdTransactionGroupID, transactionDescription, vendingMessage, textMessage, validationMessageID);
                                    return ussdInformation;
                                }

                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "Customer No." + ussdInformation.MCACustomerNo, false);
                            }
                            catch (Exception Ex)
                            {
                                InsertLog(ussdInformation.ApplicationID, ussdInformation.SubApplicationID, action.tblussdaction.UssdAction + ": " + Ex.Message, eLogType.Error);
                                validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Get MultiChoice Customer Detail Exception").ValidationMessageID);
                                UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "Get MultiChoice Customer Detail Exception", true);
                                ussdInformation.ResponseCode = "24";
                                ussdInformation.ResponseMessage = unexpectedError;
                                //InsertTempGatewayTest(ussdInformation.UssdTransactionGroupID, transactionDescription, vendingMessage, textMessage, validationMessageID);
                                return ussdInformation;
                            }
                            break;
                        case "Insert Vending Record":
                            try
                            {
                                Int32 vendingID = InsertThirdPartyVendingMS((Int16)ussdInformation.UssdTransactionGroupID, ussdInformation.UssdMsisdnID, busTran.tblbusiness.TerminalID, ussdInformation.ReferenceNumber, ussdInformation.VendingMessage, true, ussdInformation.Amount, mobileProviderID, ussdInformation.BankResponse);
                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "Customer No." + ussdInformation.MCACustomerNo, false);
                            }
                            catch (Exception Ex)
                            {
                                InsertLog(ussdInformation.ApplicationID, ussdInformation.SubApplicationID, action.tblussdaction.UssdAction + ": " + Ex.Message, eLogType.Error);
                                validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Insert Vending Message Exception").ValidationMessageID);
                                UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "Insert Vending Message Exception", true);
                                ussdInformation.ResponseCode = "24";
                                ussdInformation.ResponseMessage = unexpectedError;
                                //InsertTempGatewayTest(ussdInformation.UssdTransactionGroupID, transactionDescription, vendingMessage, textMessage, validationMessageID);
                                return ussdInformation;
                            }
                            break;
                        case "Do Concerotel Payment":
                            try
                            {
                                Boolean res = DoConcerotelPayment();
                                if (!res)
                                {
                                    validationMessageID = Convert.ToInt16(GetUssdValidationMessage(ussdInformation.ResponseMessage).ValidationMessageID);
                                    UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                                    InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), validationMessage, true);
                                    ussdInformation.ResponseCode = "34";
                                    ussdInformation.ResponseMessage = validationMessage;
                                    //InsertTempGatewayTest(ussdInformation.UssdTransactionGroupID, transactionDescription, vendingMessage, textMessage, validationMessageID);
                                    return ussdInformation;
                                }
                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "Concerotel Payment Successful.", false);
                            }
                            catch (Exception Ex)
                            {
                                InsertLog(ussdInformation.ApplicationID, ussdInformation.SubApplicationID, action.tblussdaction.UssdAction + ": " + Ex.Message, eLogType.Error);
                                validationMessageID = Convert.ToInt16(GetUssdValidationMessage("Validate Concerotel Invoice Exception").ValidationMessageID);
                                UpdateUssdMsisdnTransaction(ussdInformation.UssdMsisdnTransactionID, null, validationMessageID, null);
                                InsertUssdInteraction(ussdInformation.UssdMsisdnTransactionID, null, "", Convert.ToInt16(action.UssdActionID), "Validate Concerotel Invoice Exception", true);
                                ussdInformation.ResponseCode = "26";
                                ussdInformation.ResponseMessage = unexpectedError;
                                //InsertTempGatewayTest(ussdInformation.UssdTransactionGroupID, transactionDescription, vendingMessage, textMessage, validationMessageID);
                                return ussdInformation;
                            }

                            break;
                    }
                    #endregion
                }

                ussdInformation.ResponseCode = "00";
                ussdInformation.ResponseMessage = ussdInformation.VendingMessage;
                //InsertTempGatewayTest(ussdInformation.UssdTransactionGroupID, transactionDescription, vendingMessage, textMessage, null);
                return ussdInformation;
            }
            catch (Exception Ex)
            {
                throw new Exception("ProcessUssdRequest - " + Ex.Message);
            }
        }
        public Int32 InsertThirdPartyVendingMS(Int16 pUssdTransactionGroupID, Int32 pUssdMsisdnID, String pUserID, String pReferenceNumber, String pVendingMessage, Boolean pVendingSuccess, Decimal pAmount, short pMobileProviderID, String pBankResponse)
        {
            //String logInfo = "pThirdPartyFunctionID = " + pThirdPartyFunctionID + " | pMsisdn = " + pMsisdn + " | pUserID = " + pUserID + " | pReferenceNumber = " + pReferenceNumber + " | pVendingMessage = " + pVendingMessage + "pVendingSuccess = " + pVendingSuccess.ToString() + " | pAmount = " + pAmount.ToString() + " | pMobileProviderID = " + pMobileProviderID.ToString() + " | pBankResponse = " + pBankResponse;
            try
            {

                tblussdthirdpartyvending vend = new tblussdthirdpartyvending();
                vend.Amount = pAmount;
                vend.BankResponse = pBankResponse;
                vend.ClientRefunded = false;
                vend.MobileProviderID = pMobileProviderID;
                vend.UssdMsisdnID = pUssdMsisdnID;
                vend.ReferenceNumber = pReferenceNumber;
                vend.UserID = pUserID;
                vend.VendingDateTime = DateTime.Now;
                vend.VendingMatched = false;
                vend.VendingMessage = pVendingMessage;
                vend.VendingSuccess = pVendingSuccess;
                vend.UssdTransactionGroupID = pUssdTransactionGroupID;
                long ID = 0;
                tblussdthirdpartyvendingCrud crud = new tblussdthirdpartyvendingCrud();
                crud.Insert(vend, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertThirdPartyVendingMS - " + Ex.Message);
            }
        }

        public Boolean BuildUssdInteraction(Int32 pUssdMsisdnID, Int16 pUssdTransactionGroupID, Byte pGatewayID)
        {
            try
            {
                List<tblussdmenu> menus = CollectUssdMenu(pUssdTransactionGroupID, pGatewayID);
                Int32 transactionID = InsertUssdMsisdnTransaction(pUssdMsisdnID, GetUssdMsisdnTransactionLastSession(pUssdMsisdnID), pUssdTransactionGroupID, null);
                ussdInformation.UssdMsisdnTransactionID = transactionID;
                foreach (tblussdmenu menu in menus)
                {
                    String valueSubmitted = "";
                    if (menu.tblussdtagname != null)
                    {
                        switch (menu.tblussdtagname.TagName)
                        {
                            case "Amount": valueSubmitted = ussdInformation.Amount.ToString(); break;
                            case "MeterNumber": valueSubmitted = ussdInformation.MeterNumber.ToString(); break;
                            case "SmartCard": valueSubmitted = ussdInformation.MCASmartCard; break;
                            case "CardNumber": valueSubmitted = "Card number entered"; break;
                            case "Cvc": valueSubmitted = "Cvc entered"; break;
                            case "ExpiryDate": valueSubmitted = "Expiry date entered"; break;
                        }
                    }
                    else
                        valueSubmitted = menu.MenuItemNumber.ToString();

                    InsertUssdInteraction(transactionID, Convert.ToInt32(menu.UssdMenuID), valueSubmitted, null, "", false);
                }

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("BuildUssdInteraction - " + Ex.Message);
            }

        }
        /*
        public tblussdvalidationmessage ValidateBankCardNumber(String pBankCardNumber, String pTerminalID)
        {
            try
            {
                tblussdvalidationmessage mess = new tblussdvalidationmessage();
                DPO.VirtualVendor.BL.Validation valRes = vvoBL.ValidateCardNumber(pTerminalID, pBankCardNumber);
                recordFound = false;
                if (valRes.VCSResponseCode != "0000")
                {
                    recordFound = true;
                    mess = GetUssdValidationMessage(valRes.VCSResponseMessage);

                    if (!recordFound)
                    {
                        WriteLog(Convert.ToInt32(GetApplicationByName("VCS.VirtualMessage.Ussd").ApplicationID), "Error message not set up in tblValidationMessage = " + errorMessage, eLogType.Error);
                        mess = GetUssdValidationMessage("An unexpected error has occured. The system administartor was notified.");
                    }

                }

                return mess;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateBankCardNumber - " + Ex.Message);
            }
        }*/


        public Int16 InsertUssdValidationMessageMS(String pValidationMessage)
        {
            try
            {
                tblussdvalidationmessageCrud crud = new tblussdvalidationmessageCrud();
                tblussdvalidationmessage mess = new tblussdvalidationmessage();
                mess.ValidationMessage = pValidationMessage;
                long id = 0;
                crud.Insert(mess, out id);
                return (Int16)id;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdValidationMessageMS - " + Ex.Message);
            }

        }
        public List<tblussdmenuwelcomemessage> GetUssdWelcomeMessageMS()
        {
            try
            {
                recordFound = false;
                tblussdmenuwelcomemessageCrud crud = new tblussdmenuwelcomemessageCrud();
                List<tblussdmenuwelcomemessage> message = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return message;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdWelcomeMessageMS-" + Ex.Message);
            }
        }

        public List<tblussdprocessinstruction> GetUssdProcessInstructionMS()
        {
            try
            {
                tblussdprocessinstructionCrud crud = new tblussdprocessinstructionCrud();
                recordFound = false;
                List<tblussdprocessinstruction> res = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;

                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdProcessTransaction - " + Ex.Message);
            }
        }



        public tblussdprocessinstruction GetUssdProcessInstructionMS(String pProcessInstruction)
        {
            try
            {
                tblussdprocessinstructionCrud crud = new tblussdprocessinstructionCrud();
                crud.Where("ProcessInstruction", MySQL.DAL.General.Operator.Equals, pProcessInstruction);
                recordFound = false;
                tblussdprocessinstruction res = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdProcessTransaction - " + Ex.Message);
            }
        }

        public List<tblussdvendingmessage> GetUssdVendingMessageMS()
        {
            try
            {
                tblussdvendingmessageCrud crud = new tblussdvendingmessageCrud();
                recordFound = false;
                List<tblussdvendingmessage> res = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return res;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdVendingMessage - " + Ex.Message);
            }
        }

        public tblussdvendingmessage GetUssdVendingMessageMS(String pVendingMessage)
        {
            try
            {
                tblussdvendingmessageCrud crud = new tblussdvendingmessageCrud();
                crud.Where("VendingMessage", MySQL.DAL.General.Operator.Equals, pVendingMessage);
                recordFound = false;
                tblussdvendingmessage res = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return res;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdVendingMessage - " + Ex.Message);
            }
        }

        public Int16 InsertUssdTransactionProcessInstructionTagMS(Int16 pTranProcessID, Int16 pTagNameID, Byte pValidationErrorID)
        {
            try
            {
                tblussdtransactionprocessinstructiontagCrud crud = new tblussdtransactionprocessinstructiontagCrud();
                tblussdtransactionprocessinstructiontag ins = new tblussdtransactionprocessinstructiontag();
                ins.TranProcessID = pTranProcessID;
                ins.TagNameID = pTagNameID;
                ins.ValidationErrorID = pValidationErrorID;
                long id = 0;
                crud.Insert(ins, out id);
                return (Int16)id;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdTransactionProcessInstructionTag - " + Ex.Message);
            }
        }
        public Int16 InsertUssdProcessInstruction(String pProcessInstruction)
        {
            try
            {
                tblussdprocessinstructionCrud crud = new tblussdprocessinstructionCrud();
                tblussdprocessinstruction ins = new tblussdprocessinstruction();
                ins.ProcessInstruction = pProcessInstruction;
                Int64 ID = 0;
                crud.Insert(ins, out ID);
                return (Int16)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdProcessInstruction - " + Ex.Message);
            }
        }

        public Int16 InsertUssdTransactionProcessInstructionMS(Int16 pUssdTransactionGroupID, Int16 pProcessInsructionID)
        {
            try
            {
                tblussdtransactionprocessinstructionCrud crud = new tblussdtransactionprocessinstructionCrud();
                tblussdtransactionprocessinstruction ins = new tblussdtransactionprocessinstruction();
                ins.UssdTransactionGroupID = pUssdTransactionGroupID;
                ins.ProcessInstructionID = pProcessInsructionID;
                long ID = 0;
                crud.Insert(ins, out ID);
                return (Int16)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdTransactionProcessInstruction - " + Ex.Message);
            }
        }
        public tblussdtransactionprocessinstruction GetUssdTransactionProcessInstructionMS(Int16 pUssdTransactionID, Int16 pProcessInstructionID)
        {
            try
            {
                tblussdtransactionprocessinstructionCrud crud = new tblussdtransactionprocessinstructionCrud();
                crud.Where("UssdTransactionGroupID", MySQL.DAL.General.Operator.Equals, pUssdTransactionID);
                crud.And("ProcessInstructionID", MySQL.DAL.General.Operator.Equals, pProcessInstructionID);
                recordFound = false;
                tblussdtransactionprocessinstruction res = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransactionProcessInstruction - " + Ex.Message);
            }
        }






        public tblussdtransactionprocessinstruction GetUssdTransactionProcessInstructionMS(Int16 pUssdTransactionID, String pProcessInstruction)
        {
            try
            {
                tblussdtransactionprocessinstructionCrud crud = new tblussdtransactionprocessinstructionCrud();
                crud.Where("UssdTransactionGroupID", MySQL.DAL.General.Operator.Equals, pUssdTransactionID);
                recordFound = false;
                List<tblussdtransactionprocessinstruction> res = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                List<tblussdtransactionprocessinstruction> ins = res.FindAll(x => x.tblussdprocessinstruction.ProcessInstruction == pProcessInstruction);
                if (ins.Count > 0)
                    return ins[0];
                else
                    recordFound = false;

                return null;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransactionProcessInstruction - " + Ex.Message);
            }
        }

        public List<tblussdtransactionprocessinstruction> GetUssdTransactionProcessInstructionMS()
        {
            try
            {
                tblussdtransactionprocessinstructionCrud crud = new tblussdtransactionprocessinstructionCrud();
                recordFound = false;
                List<tblussdtransactionprocessinstruction> res = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransactionProcessInstruction - " + Ex.Message);
            }
        }

        public List<tblussdtransactionvendingmessage> GetUssdTransactionVendingMessageMS()
        {
            try
            {
                tblussdtransactionvendingmessageCrud crud = new tblussdtransactionvendingmessageCrud();
                recordFound = false;
                List<tblussdtransactionvendingmessage> res = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransactionVendingMessage - " + Ex.Message);
            }
        }

        public tblussdtransactionvendingmessage GetUssdTransactionVendingMessageMS(Int16 pUssdTransactionID, String pVendingMessage)
        {
            try
            {
                tblussdtransactionvendingmessageCrud crud = new tblussdtransactionvendingmessageCrud();
                crud.Where("UssdTransactionID", MySQL.DAL.General.Operator.Equals, pUssdTransactionID);
                recordFound = false;
                List<tblussdtransactionvendingmessage> res = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                List<tblussdtransactionvendingmessage> mess = res.FindAll(x => x.tblussdvendingmessage.VendingMessage == pVendingMessage);
                if (mess.Count > 0)
                    return mess[0];
                else
                    recordFound = false;

                return null;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransactionVendingMessage - " + Ex.Message);
            }
        }

        public Byte InsertUssdValidationErrorMS(String pValidationError)
        {
            try
            {
                tblussdvalidationerrorCrud crud = new tblussdvalidationerrorCrud();
                tblussdvalidationerror message = new tblussdvalidationerror();
                message.ValidationError = pValidationError;
                long ID = 0;
                crud.Insert(message, out ID);
                return (Byte)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdVendingMessage - " + Ex.Message);
            }
        }
        public tblussdmenuwelcomemessage GetUssdWelcomeMessageMS(Int16 pWelcomeMessageID)
        {
            try
            {
                recordFound = false;
                tblussdmenuwelcomemessageCrud crud = new tblussdmenuwelcomemessageCrud();
                crud.Where("WelcomeMessageID", MySQL.DAL.General.Operator.Equals, pWelcomeMessageID);
                tblussdmenuwelcomemessage message = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return message;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdWelcomeMessageMS-" + Ex.Message);
            }
        }

        public tblussdgateway GetUssdGatewayMS(String pGatewayName)
        {
            try
            {
                recordFound = false;
                tblussdgatewayCrud reader = new tblussdgatewayCrud();
                reader.Where("GatewayName", MySQL.DAL.General.Operator.Equals, pGatewayName);
                tblussdgateway gateway = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return gateway;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdGateway -" + Ex.Message);
            }
        }
        public List<tblussdmenuitem> GetUssdMenuItemMS()
        {
            try
            {
                recordFound = false;
                tblussdmenuitemCrud reader = new tblussdmenuitemCrud();
                List<tblussdmenuitem> item = reader.ReadMulti().ToList();
                recordFound = reader.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuItemMS-" + Ex.Message);
            }
        }
        public tblussdmenuitem GetUssdMenuItemMS(Int32 pMenuItemID)
        {
            try
            {
                recordFound = false;
                tblussdmenuitemCrud reader = new tblussdmenuitemCrud();
                reader.Where("MenuItemID", MySQL.DAL.General.Operator.Equals, pMenuItemID);
                tblussdmenuitem item = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuItemMS-" + Ex.Message);
            }
        }

        public Int16 GetOrInsertUssdMenuItemMS(String pMenuItem)
        {
            try
            {
                tblussdmenuitem menuItem = GetUssdMenuItemMS(pMenuItem);
                if (recordFound)
                    return (Int16)menuItem.MenuItemID;
                else
                    return InsertUssdMenuItemMS(pMenuItem);

            }
            catch (Exception Ex)
            {
                throw new Exception("GetOrInsertUssdMenuItemMS - " + Ex.Message);
            }
        }
        public Int16 GetOrInsertUssdMenuItemTextMS(String pMenuItemText)
        {
            try
            {
                tblussdmenuitemtext menuItemText = GetUssdMenuItemTextMS(pMenuItemText);
                if (recordFound)
                    return (Int16)menuItemText.MenuItemTextID;
                else
                    return InsertUssdMenuItemTextMS(pMenuItemText);
            }
            catch (Exception Ex)
            {
                throw new Exception("GetOrInsertUssdMenuItemTextMS - " + Ex.Message);
            }
        }
        public tblussdmenuitemtext GetUssdMenuItemTextMS(String pMenuItemText)
        {
            try
            {
                recordFound = false;
                tblussdmenuitemtextCrud reader = new tblussdmenuitemtextCrud();
                reader.Where("MenuItemText", MySQL.DAL.General.Operator.Equals, pMenuItemText);
                tblussdmenuitemtext item = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuItemText - " + Ex.Message);
            }
        }

        /// <summary>
        /// Returns a list of the ussd menu item text as they appear to the user.
        /// </summary>
        /// <returns></returns>
        /// 
        public List<tblussdmenuitemtext> GetUssdMenuItemTextMS()
        {
            try
            {
                recordFound = false;
                tblussdmenuitemtextCrud reader = new tblussdmenuitemtextCrud();
                List<tblussdmenuitemtext> item = reader.ReadMulti().ToList();
                recordFound = reader.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuItemTextMS - " + Ex.Message);
            }
        }
        public tblussdmenuitem GetUssdMenuItemMS(String pMenuItem)
        {
            try
            {
                recordFound = false;
                tblussdmenuitemCrud reader = new tblussdmenuitemCrud();
                reader.Where("MenuItem", MySQL.DAL.General.Operator.Equals, pMenuItem);
                tblussdmenuitem item = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuItemMS-" + Ex.Message);
            }

        }
        public Int16 GetOrInsertUssdConfirmTextMS(String pConfirmText)
        {
            try
            {
                tblussdconfirmtext confirmText = GetUssdConfirmTextMS(pConfirmText);
                if (recordFound)
                    return (Int16)confirmText.ConfirmTextID;
                else
                    return InsertUssdConfirmTextMS(pConfirmText);
            }
            catch (Exception Ex)
            {
                throw new Exception("GetOrInsertUssdConfirmTextMS - " + Ex.Message);
            }
        }
        public List<tblussdconfirmtext> GetUssdConfirmTextMS()
        {
            try
            {
                recordFound = false;
                tblussdconfirmtextCrud crud = new tblussdconfirmtextCrud();
                List<tblussdconfirmtext> item = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdConfirmTextMS-" + Ex.Message);
            }
        }


        public tblussdconfirmtext GetUssdConfirmTextMS(String pConfirmText)
        {
            try
            {
                recordFound = false;
                tblussdconfirmtextCrud crud = new tblussdconfirmtextCrud();
                crud.Where("ConfirmText", MySQL.DAL.General.Operator.Equals, pConfirmText);
                tblussdconfirmtext item = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdConfirmTextMS-" + Ex.Message);
            }
        }

        public tblussdleadintext GetUssdLeadInTextMS(Int16 pLeadInTextID)
        {
            try
            {
                recordFound = false;
                tblussdleadintextCrud crud = new tblussdleadintextCrud();
                crud.Where("LeadInTextID", MySQL.DAL.General.Operator.Equals, pLeadInTextID);
                tblussdleadintext text = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return text;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdLeadInTextMS-" + Ex.Message);
            }
        }

        public Int16 GetOrInsertUssdLeadInTextMS(String pLeadInText)
        {
            try
            {
                tblussdleadintext leadIn = GetUssdLeadInTextMS(pLeadInText);
                if (recordFound)
                    return (Int16)leadIn.LeadInTextID;
                else
                    return InsertUssdLeadInTextMS(pLeadInText);
            }
            catch (Exception Ex)
            {
                throw new Exception("GetOrInsertUssdLeadInTextMS - " + Ex.Message);
            }
        }
        public List<tblussdtransaction> GetUssdTransactionMS()
        {
            try
            {
                recordFound = false;
                tblussdtransactionCrud crud = new tblussdtransactionCrud();
                List<tblussdtransaction> tran = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return tran;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransactionMS-" + Ex.Message);
            }
        }

        public tblussdhttppost GetUssdHttpPostMS(String pPostUrl)
        {
            try
            {
                recordFound = false;
                tblussdhttppostCrud crud = new tblussdhttppostCrud();
                crud.Where("Url", MySQL.DAL.General.Operator.Equals, pPostUrl);
                tblussdhttppost post = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return post;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdHttpPostMS-" + Ex.Message);
            }
        }

        public tblussdhttppost GetUssdHttpPostMS(Byte pHttpPostID)
        {
            try
            {
                recordFound = false;
                tblussdhttppostCrud crud = new tblussdhttppostCrud();
                crud.Where("HttpPostID", MySQL.DAL.General.Operator.Equals, pHttpPostID);
                tblussdhttppost post = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return post;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdHttpPostMS-" + Ex.Message);
            }
        }
        public tblussdmenuthirdparty GetUssdMenuThirdPartyMS(Int32 pBusinessID, Int32 pGatewayID)
        {
            try
            {
                tblussdmenuthirdpartyCrud crud = new tblussdmenuthirdpartyCrud();
                crud.Where("BusinessID", MySQL.DAL.General.Operator.Equals, pBusinessID);
                crud.And("GatewayID", MySQL.DAL.General.Operator.Equals, pGatewayID);
                recordFound = false;
                tblussdmenuthirdparty result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuThirdParty -" + Ex.Message);
            }
        }
        public Int32 InsertUssdMenuMS(Byte pGatewayID, Int16? pMenuItemID, Int16? pMenuItemNo, Int16? pMenuItemTextID, Boolean? pConfirm, Int16? pConfirmTextID, Int16? pLeadInTextID, Boolean? pPrompt, Int16? pTagNameID, Int16? pWelcomeMessageID, Int16? pUssdTransactionGroupID, Byte pMenuLevelNo, Byte? pHttpPostID, String pRegistryKey, Int32? pParentID, Int32? pFunctionID)
        {
            try
            {

                tblussdmenu menu = new tblussdmenu();
                menu.Confirm = pConfirm;
                menu.ConfirmTextID = pConfirmTextID;
                menu.GatewayID = pGatewayID;
                menu.LeadInTextID = pLeadInTextID;
                menu.MenuItemID = pMenuItemID;
                menu.MenuItemNumber = pMenuItemNo;
                menu.MenuItemTextID = pMenuItemTextID;
                menu.Promt = pPrompt;
                menu.TagNameID = pTagNameID;
                menu.UssdTransactionGroupID = pUssdTransactionGroupID;
                menu.WelcomeMessageID = pWelcomeMessageID;
                menu.MenuLevelNo = pMenuLevelNo;
                menu.HttpPostID = pHttpPostID;
                menu.RegistryKey = pRegistryKey;
                menu.ParentUssdMenuID = pParentID;
                menu.FunctionID = pFunctionID;
                menu.Active = true;
                tblussdmenuCrud crud = new tblussdmenuCrud();
                long ID = 0;
                crud.Insert(menu, out ID);

                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdMenu-" + Ex.Message);
            }
        }


        public Boolean UpdateUssdMenuMS(String pRegistryKey, Int16? pWelcomeMessageID, Int16? pMenuItemNumber, Int16? pMenuItemTextID, Boolean? pConfirm, Int16? pConfirmTextID, Int16? pLeadinTextID, Boolean? pPrompt, Int16? pTagnameID, Int16? pTransactionID, Int16? pMenuItemID, Byte? pHttpPostID, Byte pMenuLevelNumber)
        {
            try
            {
                tblussdmenu menu = GetUssdMenuMS(pRegistryKey);
                if (recordFound)
                {
                    menu.WelcomeMessageID = pWelcomeMessageID;
                    menu.MenuItemNumber = pMenuItemNumber;
                    menu.MenuItemTextID = pMenuItemTextID;
                    menu.Confirm = pConfirm;
                    menu.ConfirmTextID = pConfirmTextID;
                    menu.LeadInTextID = pLeadinTextID;
                    menu.Promt = pPrompt;
                    menu.TagNameID = pTagnameID;
                    menu.UssdTransactionGroupID = pTransactionID;
                    menu.MenuItemID = pMenuItemID;
                    if (pHttpPostID == 0)
                        menu.HttpPostID = null;
                    else
                        menu.HttpPostID = pHttpPostID;
                    menu.MenuLevelNo = pMenuLevelNumber;

                    tblussdmenuCrud crud = new tblussdmenuCrud();
                    crud.Update(menu);
                }
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdMenuMS - " + Ex.Message);
            }
        }
        public Int16 InsertUssdTransactionMS(String pTransaction)
        {
            try
            {
                tblussdtransaction ins = new tblussdtransaction();
                ins.UssdTransaction = pTransaction;
                tblussdtransactionCrud crud = new tblussdtransactionCrud();
                long ID = 0;
                crud.Insert(ins, out ID);

                return (Int16)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdTransactionMS-" + Ex.Message);
            }
        }

        public Int16 InsertUssdTransactionMS(String pTransaction, Decimal pTransactionAmount, String pTransactionValue)
        {
            try
            {
                tblussdtransaction ins = new tblussdtransaction();
                ins.UssdTransaction = pTransaction;
                ins.TransactionAmount = pTransactionAmount;
                ins.TransactionValue = pTransactionValue;
                tblussdtransactionCrud crud = new tblussdtransactionCrud();
                long ID = 0;
                crud.Insert(ins, out ID);

                return (Int16)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdTransaction-" + Ex.Message);
            }
        }
        public Int32 InsertUssdProcessTraceMS(Int32 pUssdMsisdnID, Byte pGatewayID, DateTime pLogDateTime, Byte pGatewayErrorID, Int32 pSmsID)
        {
            try
            {
                tblussdprocesstraceCrud crud = new tblussdprocesstraceCrud();
                tblussdprocesstrace trace = new tblussdprocesstrace();
                trace.Authorised = false;
                trace.GatewayID = pGatewayID;
                trace.LogDateTime = pLogDateTime;
                trace.UssdMsisdnID = pUssdMsisdnID;
                trace.SmsSend = false;
                trace.WebPageCalled = false;
                trace.ThirdPartyCalled = false;
                trace.GatewayInsert = false;
                trace.GatewayErrorID = pGatewayErrorID;
                trace.SmsID = pSmsID;
                long ID = 0;
                crud.Insert(trace, out ID);
                return (Int32)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdProcessTrace - " + Ex.Message);
            }
        }

        public Int32 InsertUssdProcessTraceMS(Int32 pUssdMsisdinID, Byte pGatewayID, Int16? pUssdTransactionGroupID, DateTime pLogDateTime, Boolean pGatewayInsert)
        {
            try
            {
                tblussdprocesstraceCrud crud = new tblussdprocesstraceCrud();
                tblussdprocesstrace trace = new tblussdprocesstrace();
                trace.Authorised = false;
                trace.GatewayID = pGatewayID;
                trace.LogDateTime = pLogDateTime;
                trace.UssdMsisdnID = pUssdMsisdinID;
                trace.SmsSend = false;
                trace.WebPageCalled = false;
                trace.ThirdPartyCalled = false;
                trace.UssdTransactionGroupID = pUssdTransactionGroupID;
                trace.GatewayInsert = pGatewayInsert;
                long ID = 0;
                crud.Insert(trace, out ID);
                return (Int32)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdProcessTrace - " + Ex.Message);
            }
        }

        public tblussdprocesstrace GetUssdProcessTraceMS(Int32 pUssdMsisdnID, Int32 pUssdTransactionGroupID, DateTime pStartDateTime, DateTime pEndDateTime)
        {
            try
            {
                tblussdprocesstraceCrud crud = new tblussdprocesstraceCrud();
                crud.Where("UssdMsisdnID", MySQL.DAL.General.Operator.Equals, pUssdMsisdnID);
                crud.And("UssdTransactionGroupID", MySQL.DAL.General.Operator.Equals, pUssdTransactionGroupID);
                crud.And("LogDateTime", MySQL.DAL.General.Operator.Between, pStartDateTime, pEndDateTime);
                recordFound = false;
                tblussdprocesstrace result = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdProcessTrace - " + Ex.Message);
            }
        }

        public Boolean DeleteUssdWelcomeMessage(Int16 pWelcomeMessageID)
        {
            try
            {
                tblussdmenuwelcomemessage mess = GetUssdWelcomeMessageMS(pWelcomeMessageID);
                tblussdmenuwelcomemessageCrud crud = new tblussdmenuwelcomemessageCrud();
                crud.Delete(mess);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateUssdWelcomeMessage -" + Ex.Message);
            }
        }

        public tblussdmenu GetUssdMenuMS(String pRegistryKey)
        {
            try
            {
                recordFound = false;
                tblussdmenuCrud reader = new tblussdmenuCrud();
                reader.ExcludeReferenceTable = excludeRefTable;
                reader.Where("REgistryKey", MySQL.DAL.General.Operator.Equals, pRegistryKey);
                reader.ExcludeReferenceTable = true;
                tblussdmenu menus = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return menus;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuMS - " + Ex.Message);
            }
        }
        public List<tblussdmenu> GetUssdMenuMS()
        {
            try
            {
                recordFound = false;
                tblussdmenuCrud reader = new tblussdmenuCrud();
                reader.ExcludeReferenceTable = excludeRefTable;
                Collection<tblussdmenu> menus = reader.ReadMulti();
                recordFound = reader.RecordFound;
                return menus.ToList();

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuMS - " + Ex.Message);
            }
        }
        public tblussdmenu GetUssdMenuByUssdTransactionGroupID(Byte pGatewayID, Int16 pUssdTransactionGroupID)
        {
            try
            {
                tblussdmenuCrud crud = new tblussdmenuCrud();
                crud.Where("UssdTransactionGroupID", MySQL.DAL.General.Operator.Equals, pUssdTransactionGroupID);
                crud.And("GatewayID", MySQL.DAL.General.Operator.Equals, pGatewayID);
                recordFound = false;
                crud.ExcludeReferenceTable = excludeRefTable;
                tblussdmenu results = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return results;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuByRegistryKey - " + Ex.Message);
            }
        }
        public tblussdinstruction GetUssdInstruction(Int16 pInstructionID)
        {
            try
            {
                tblussdinstructionCrud crud = new tblussdinstructionCrud();
                crud.Where("InstructionID", MySQL.DAL.General.Operator.Equals, pInstructionID);
                recordFound = false;
                tblussdinstruction instr = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return instr;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdInstruction - " + Ex.Message);
            }
        }
        public List<tblussdhttppost> GetUssdHttpPostMS()
        {
            try
            {
                recordFound = false;
                tblussdhttppostCrud crud = new tblussdhttppostCrud();
                List<tblussdhttppost> post = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return post;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdHttpPostMS-" + Ex.Message);
            }
        }
        public tblussdtransaction GetUssdTransactionMS(Int16 pTransactionID)
        {
            try
            {
                recordFound = false;
                tblussdtransactionCrud crud = new tblussdtransactionCrud();
                crud.Where("UssdTransactionID", MySQL.DAL.General.Operator.Equals, pTransactionID);
                tblussdtransaction tran = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return tran;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransactionMS-" + Ex.Message);
            }
        }
        public tblussdtransaction GetUssdTransactionMS(String pTransaction)
        {
            try
            {
                recordFound = false;
                tblussdtransactionCrud crud = new tblussdtransactionCrud();
                crud.Where("UssdTransaction", MySQL.DAL.General.Operator.Equals, pTransaction);
                tblussdtransaction tran = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return tran;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransaction-" + Ex.Message);
            }
        }
        public tblussdleadintext GetUssdLeadInTextMS(String pLeadInText)
        {
            try
            {
                recordFound = false;
                tblussdleadintextCrud crud = new tblussdleadintextCrud();
                crud.Where("LeadInText", MySQL.DAL.General.Operator.Equals, pLeadInText);
                tblussdleadintext text = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return text;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdLeadInTextMS-" + Ex.Message);
            }
        }
        public tblussdconfirmtext GetUssdConfirmTextMS(Int32 pConfirmTextID)
        {
            try
            {
                recordFound = false;
                tblussdconfirmtextCrud crud = new tblussdconfirmtextCrud();
                crud.Where("ConfirmTextID", MySQL.DAL.General.Operator.Equals, pConfirmTextID);
                tblussdconfirmtext item = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdConfirmTextMS-" + Ex.Message);
            }
        }
        public tblussdmenuitemtext GetUssdMenuItemTextMS(Int16 pMenuItemID)
        {
            try
            {
                recordFound = false;
                tblussdmenuitemtextCrud reader = new tblussdmenuitemtextCrud();
                reader.Where("MenuItemTextID", MySQL.DAL.General.Operator.Equals, pMenuItemID);
                tblussdmenuitemtext item = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return item;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdMenuItemText - " + Ex.Message);
            }
        }
        public List<tblussdgateway> GetUssdGatewayMS()
        {
            try
            {
                recordFound = false;
                tblussdgatewayCrud reader = new tblussdgatewayCrud();
                List<tblussdgateway> gateway = reader.ReadMulti().ToList();
                recordFound = reader.RecordFound;
                return gateway;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdGateway -" + Ex.Message);
            }
        }
        public tblussdgateway GetUssdGatewayMS(Byte pGatewayID)
        {
            try
            {
                recordFound = false;
                tblussdgatewayCrud reader = new tblussdgatewayCrud();
                reader.Where("GatewayID", MySQL.DAL.General.Operator.Equals, pGatewayID);
                tblussdgateway gateway = reader.ReadSingle();
                recordFound = reader.RecordFound;
                return gateway;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdGateway -" + Ex.Message);
            }
        }
        public tblussdtextmessage GetUssdTextMessage(String pTextMessage)
        {
            try
            {
                tblussdtextmessageCrud crud = new tblussdtextmessageCrud();
                recordFound = false;
                crud.Where("TextMessage", MySQL.DAL.General.Operator.Equals, pTextMessage);
                tblussdtextmessage mess = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return mess;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTextMessage - " + Ex.Message);
            }
        }
        public tblussdtransactiondescription GetUssdTransactionDescription(String pTransactionDescription)
        {
            try
            {
                tblussdtransactiondescriptionCrud crud = new tblussdtransactiondescriptionCrud();
                crud.Where("TransactionDescription", MySQL.DAL.General.Operator.Equals, pTransactionDescription);
                recordFound = false;
                tblussdtransactiondescription res = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdTransactionDescription - " + Ex.Message);
            }
        }
        public Int16 InsertUssdTransactionVendingMessage(Int16 pUssdTransactionID, Int16 pVendingMessageID)
        {
            try
            {
                tblussdtransactionvendingmessageCrud crud = new tblussdtransactionvendingmessageCrud();
                tblussdtransactionvendingmessage mess = new tblussdtransactionvendingmessage();
                mess.UssdTransactionID = pUssdTransactionID;
                mess.VendingMessageID = pVendingMessageID;
                Int64 id = 0;
                crud.Insert(mess, out id);
                return (Int16)id;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdTransactionVendingMessage - " + Ex.Message);
            }

        }
        public Int16 InsertUssdVendingMessageMS(Byte pVendingMessageTypeID, String pVendingMessage)
        {
            try
            {
                tblussdvendingmessageCrud crud = new tblussdvendingmessageCrud();
                tblussdvendingmessage message = new tblussdvendingmessage();
                message.VendingMessageTypeID = pVendingMessageTypeID;
                message.VendingMessage = pVendingMessage;
                long ID = 0;
                crud.Insert(message, out ID);
                return (Int16)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertUssdVendingMessage - " + Ex.Message);
            }
        }
        public List<tblussdvendingmessagetype> GetUssdVendingMessageTypeMS()
        {
            try
            {
                tblussdvendingmessagetypeCrud crud = new tblussdvendingmessagetypeCrud();
                recordFound = false;
                List<tblussdvendingmessagetype> res = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return res;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdVendingMessageType - " + Ex.Message);
            }

        }

        public Int16 GetOrInsertUssdMenuWelcomeMessage(String pWelcomeMessage)
        {
            try
            {
                tblussdmenuwelcomemessage welcomeMessage = GetUssdMenuWelcomeMessage(pWelcomeMessage);
                if (recordFound)
                    return (Int16)welcomeMessage.WelcomeMessageID;
                else
                    return InsertUssdWelcomeMessageMS(pWelcomeMessage);

            }
            catch (Exception Ex)
            {
                throw new Exception("GetOrInsertUssdMenuWelcomeMessage - " + Ex.Message);
            }
        }
        public tblussdmenuwelcomemessage GetUssdMenuWelcomeMessage(String pWelcomeMessage)
        {
            try
            {
                recordFound = false;
                tblussdmenuwelcomemessageCrud crud = new tblussdmenuwelcomemessageCrud();
                crud.Where("WelcomeMessage", MySQL.DAL.General.Operator.Equals, pWelcomeMessage);
                tblussdmenuwelcomemessage message = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return message;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdWelcomeMessage-" + Ex.Message);
            }
        }
        public List<tblussdleadintext> GetUssdLeadInTextMS()
        {
            try
            {
                recordFound = false;
                tblussdleadintextCrud crud = new tblussdleadintextCrud();
                List<tblussdleadintext> text = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return text;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdLeadInTextMS-" + Ex.Message);
            }
        }

        public String AuthorisationUssd(String pUserID, String pDescription)
        {
            try
            {
                String referenceNo = System.DateTime.Now.ToString("yyyyMMddhhssmmm");
                ussdInformation.ReferenceNumber = referenceNo;
                String authResult = Authorisation(pUserID, referenceNo, pDescription, ussdInformation.Amount.ToString(), ussdInformation.BankCardNumber, ussdInformation.Msisdn, ussdInformation.ExpiryDate, ussdInformation.Cvc);


                XmlDocument docCcXmlAuthResponse = new XmlDocument();
                try
                {
                    docCcXmlAuthResponse.LoadXml(authResult);
                }
                catch (Exception Ex)
                {
                    return "Authorisation response document failed to load";
                }

                String bankResponse = "";
                if (docCcXmlAuthResponse.GetElementsByTagName("Response").Count > 0)
                    if (docCcXmlAuthResponse.GetElementsByTagName("Response")[0].ChildNodes.Count > 0)
                        bankResponse = docCcXmlAuthResponse.GetElementsByTagName("Response")[0].ChildNodes[0].Value;
                    else
                        bankResponse = "Bank response missing";
                else
                    bankResponse = "Bank response tag missing";

                return bankResponse;
            }
            catch (Exception Ex)
            {
                throw new Exception("Authorisation - " + Ex.Message);
            }
        }


    }
}
