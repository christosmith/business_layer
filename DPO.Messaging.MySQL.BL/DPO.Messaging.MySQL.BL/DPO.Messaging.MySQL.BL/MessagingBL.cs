﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.Messaging.MySQL.DAL;
namespace DPO.Messaging.MySQL.BL
{
    public class MessagingBL
    {
        public Int32 InsertTextMessage(String pMobileNo, String pMessage, String pQueuedByApplication)
        {
            try
            {
                tbltextmessageCrud crud = new tbltextmessageCrud();
                tbltextmessage mess = new tbltextmessage();
                mess.Message = pMessage;
                mess.MobileNo = pMobileNo;
                mess.QueuedByApplication = pQueuedByApplication;
                mess.SubmitDateTime = DateTime.Now;
                mess.TextMessageStatusID = 1;
                long id = 0;
                crud.Insert(mess, out id);

                return (Int32)id;
                
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTextMessage - " + Ex.Message);
            }
        }
    }
}
