﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualAccount.DAL;

namespace DPO.VirtualAccount.BL
{
public partial class VirtualAccountBL
    {
        public Cards GetCards(String pCardNumber)
        {
            try
            {
                recordFound = false;
                CardsCrud crud = new CardsCrud();
                crud.Where("CardNumber", General.Operator.Equals, pCardNumber);

                Cards search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCard - " + Ex.Message);
            }
        }
        public Cards GetCards(Decimal pAccountNumber)
        {
            try
            {
                recordFound = false;
                CardsCrud crud = new CardsCrud();
                crud.Where("AccountNumber", General.Operator.Equals, pAccountNumber);

                Cards search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCardDecimal - " + Ex.Message);
            }
        }
    }
}
