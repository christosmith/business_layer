﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualAccount.DAL;

namespace DPO.VirtualAccount.BL
{
public partial class VirtualAccountBL
	{
		public Boolean UpdateMsidnPin(String pMsisdn, String pPin)
		{
			try
			{
				StoredProcedure.vcs_UpdatePin(pMsisdn, pPin, DateTime.Now);
				return true;
			
			}
			catch (Exception Ex)
			{
				throw new Exception("UpdateMsisdnPin-" + Ex.Message);
			}
		}
		public Msisdn GetMsisdn(String pMsisdn)
        {
            try
            {
                recordFound = false;
                MsisdnCrud crud = new MsisdnCrud();
                crud.Where("Msisdn", General.Operator.Equals, pMsisdn);

                Msisdn search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMsisdn - " + Ex.Message);
            }
        }
		public MsisdnCard GetMsisdnCard(String pMsisdn, String pDefaultCard)
        {
            try
            {
                recordFound = false;
                MsisdnCardCrud crud = new MsisdnCardCrud();
                crud.Where("Msisdn", General.Operator.Equals, pMsisdn);
                crud.And("DefaultCard", General.Operator.Equals, pDefaultCard);

                MsisdnCard search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMsisdnCard - " + Ex.Message);
            }
        }
    }
}
