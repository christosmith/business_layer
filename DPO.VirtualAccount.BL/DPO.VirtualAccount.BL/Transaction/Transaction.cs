﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualAccount.DAL;

namespace DPO.VirtualAccount.BL
{
public partial class VirtualAccountBL
    {

		public Transactions GetTransaction(String pCardNumber, String pReference)
		{
			try
			{
				TransactionsCrud crud = new TransactionsCrud();
                crud.Where("Reference", General.Operator.Like, pReference + "%");
                crud.And("CardNumber", General.Operator.Equals, pCardNumber);
				
				recordFound = false;
				Transactions tran = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return tran;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTransaction(String, String) - " + Ex.Message);
			}
		}

		public Transactions GetTransaction(Decimal pTransactionID)
		{
			try
			{
				TransactionsCrud crud = new TransactionsCrud();
				crud.Where("TranID", General.Operator.Equals, pTransactionID);
				List<Transactions> trans = crud.ReadMulti().ToList();

				System.Data.DataTable table = new System.Data.DataTable();

				System.Reflection.PropertyInfo[] Props = typeof(Transactions).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
				foreach (System.Reflection.PropertyInfo prop in Props)
				{
					var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
					table.Columns.Add(prop.Name, type);
				}
				foreach (Transactions item in trans)
				{
					var values = new object[Props.Length];
					for (int i = 0; i < Props.Length; i++)
					{
						//inserting property values to datatable rows
						values[i] = Props[i].GetValue(item, null);
					}
					table.Rows.Add(values);
				}

				return trans[0];
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTransaction-" + Ex.Message);
			}
		}
        public Int32 InsertTransaction(String pCompany, String pDivision, String pBranch, String pCardNumber, String pType, Decimal pAmount, String pReference, String pExpiry, String pBudget, String pCvc, String pResponse, String pUserId, String pDescription, String pPosted, DateTime pPostingDate, String pAddedToBalance)
        {
            try
            {
                TransactionsCrud crud = new TransactionsCrud();
                Transactions workRec = new Transactions();
                workRec.Division = pDivision;
                workRec.Branch = pBranch;
                workRec.CardNumber = pCardNumber;
                workRec.Type = pType;
                workRec.Amount = pAmount;
                workRec.Reference = pReference;
                workRec.Expiry = pExpiry;
                workRec.Budget = pBudget;
                workRec.Cvc = pCvc;
                workRec.Response = pResponse;
                workRec.UserId = pUserId;
                workRec.Description = pDescription;
                workRec.Posted = pPosted;
                workRec.PostingDate = pPostingDate;
                workRec.AddedToBalance = pAddedToBalance;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTransaction - " + Ex.Message);
            }
        }
    }
}
