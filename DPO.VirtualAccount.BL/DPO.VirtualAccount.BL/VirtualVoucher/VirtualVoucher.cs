﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualAccount.DAL;

namespace DPO.VirtualAccount.BL
{
public partial class VirtualAccountBL
    {

		public VoucherVault GetVoucherVaultByPinNo(String pProdCode, String pPinNumber)
        {
            try
            {
                recordFound = false;
                VoucherVaultCrud crud = new VoucherVaultCrud();
                crud.Where("ProdCode", General.Operator.Equals, pProdCode);
                crud.And("PinNumber", General.Operator.Equals, pPinNumber);

                VoucherVault search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetVoucherVaultByPinNo - " + Ex.Message);
            }
        }
		public VoucherVault GetVoucherVault(String pProdCode, String pReference)
        {
            try
            {
                recordFound = false;
                VoucherVaultCrud crud = new VoucherVaultCrud();
                crud.Where("ProdCode", General.Operator.Equals, pProdCode);
                crud.And("Reference", General.Operator.Equals, pReference);

                VoucherVault search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetVoucherVault - " + Ex.Message);
            }
        }
		public	Int32 GetVirtualVoucherAvailableCount(String pProdCode)
		{
			try
			{
				VoucherVaultCrud crud = new VoucherVaultCrud();
				crud.Where("ProdCode", General.Operator.Equals, pProdCode);
				crud.And("SoldYN", General.Operator.Equals, "N");
				List<VoucherVault> results = crud.ReadMulti().ToList();
				return results.Count;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetVirtualVoucherAvailableCount-" + Ex.Message);
			}
		}
        public VoucherTypes GetVoucherTypes(String pProdCode)
        {
            try
            {
                recordFound = false;
                VoucherTypesCrud crud = new VoucherTypesCrud();
                crud.Where("ProdCode", General.Operator.Equals, pProdCode);

                VoucherTypes search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetVoucherType - " + Ex.Message);
            }
        }
		public Int32 InsertVoucherVault(String pProdCode, String pVoucherNo, String pPinNumber, Decimal pAmount)
		{
			try
			{
				VoucherVault vault = new VoucherVault();
				vault.Amount = pAmount;
				vault.ClientCell = "";
				vault.DateLoaded = DateTime.Now;
				vault.DisburseFeeDone = "N";
				vault.DisburseFeeReference = "";
				vault.LoadFeeDone = "N";
				vault.LoadFeeReference = "";
				vault.MerchantCell = "";
				vault.PinNumber = pPinNumber;
				vault.ProdCode = pProdCode;
				vault.Reference = "";
				vault.SoldYN = "N";
				vault.VoucherNo = pVoucherNo;
				vault.YournetUserName = "";
				VoucherVaultCrud crud = new VoucherVaultCrud();
				long ID = 0;
				crud.Insert(vault, out ID);
				return (Int32)ID;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertVoucherVault-" + Ex.Message);
			}
		}
        public List<VoucherTypes> GetVoucherTypesPerNetWork(String pNetworkCode)
        {
            try
            {
                recordFound = false;
                VoucherTypesCrud crud = new VoucherTypesCrud();
                crud.Where("NetworkCode", General.Operator.Equals, pNetworkCode);

                List<VoucherTypes> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetVoucherTypePerNetWork - " + Ex.Message);
            }
        }
        public VoucherMerchants GetVoucherMerchants(String pMerchantId)
        {
            try
            {
                recordFound = false;
                VoucherMerchantsCrud crud = new VoucherMerchantsCrud();
                crud.Where("MerchantId", General.Operator.Equals, pMerchantId);

                VoucherMerchants search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetVoucherMerchant - " + Ex.Message);
            }
        }
    }
}
