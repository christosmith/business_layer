﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualMessage.DAL;


namespace DPO.VirtualMessage.BL
{
public partial class VirtualMessageBL
    {
        public Boolean InsertBulkSMS(List<OutBox> pOutboxList)
        {
            try
            {
				OutBoxCrud crud = new OutBoxCrud();
                foreach (OutBox outbox in pOutboxList)
					crud.Insert(outbox);

                return true;
            }
            catch (Exception Ex)
            {
				throw Ex;
            }
        }
        public Boolean InsertBulkMail(List<SendMsg> pMessageList, List<byte[]> pImageList)
        {
            try
            {
				SendMsgCrud crud = new SendMsgCrud();
                foreach (SendMsg message in pMessageList)
                {
					long newMessageID = 0;
					crud.Insert(message, out newMessageID);
					SendMsgImageCrud imgCrud = new SendMsgImageCrud();
                    foreach (byte[] image in pImageList)
                    {
                        SendMsgImage img = new SendMsgImage();
                        img.Send_ID = (Int32)newMessageID;
                        img.ImageData = image;
						imgCrud.Insert(img);
                    }
                }
                return true;
            }
            catch (Exception Ex)
            {
				throw Ex;
            }
        }
        public Boolean InsertBulkMail(List<SendMsg> pMessageList)
        {
            try
            {
				SendMsgCrud crud = new SendMsgCrud();
				foreach (SendMsg message in pMessageList)
					crud.Insert(message);

                return true;
            }
            catch (Exception Ex)
            {
				throw Ex;
            }
        }

        public Boolean InsertSendMsgArchive(Int32 pSendID)
        {
            try
            {
                SendMsg message = GetSendMsg(pSendID);
                SendMsgArchive arc = new SendMsgArchive();
                arc.Attachment = message.Attachment;
                arc.Attachment_Filename = message.Attachment_Filename;
                arc.Date_Inserted = message.Date_Inserted;
                arc.Date_Sent = message.Date_Sent;
                arc.DepartmentID = message.DepartmentID;
                arc.DueDateTime = message.DueDateTime;
                arc.Error_Message = message.Error_Message;
                arc.HTML_Format = message.HTML_Format;
                arc.HTML_Format = message.HTML_Format;
                arc.Msg = message.Msg;
                arc.Msg_Subject = message.Msg_Subject;
                arc.QueuedByApplication = message.QueuedByApplication;
                arc.Recipient = message.Recipient;
                arc.RecipientID = message.RecipientID;
                arc.Retry_Count = message.Retry_Count;
                arc.Sender = message.Sender;
                arc.Send_ID = message.Send_ID;
                arc.Send_Method = message.Send_Method;
                arc.SystemGenerated = message.SystemGenerated;
                arc.UserID = message.UserID;
                SendMsgArchiveCrud crud = new SendMsgArchiveCrud();
                crud.Insert(arc);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertSendMsgArchive - " + Ex.Message);
            }
        }
		public Int32 InsertSendMsg(String pRecipient, String pSender, String pSubject, String pMailText, String pQueuedByApplication)
		{
			try
			{
				SendMsgCrud crud = new SendMsgCrud();
				SendMsg msg = new SendMsg();
				msg.Date_Inserted = DateTime.Now;
				msg.DueDateTime = DateTime.Now;
				msg.HTML_Format = true;
				msg.Msg = pMailText;
				msg.Msg_Subject = pSubject;
				msg.QueuedByApplication = pQueuedByApplication;
				msg.Recipient = pRecipient;
				msg.Sender = pSender;
				msg.Send_Method = 1;
				msg.Retry_Count = 0;
				msg.SystemGenerated = false;
				long ID = 0;
				crud.Insert(msg, out ID);

				return (Int32)ID;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertSendMsg - " + Ex.Message);
			}
		
		}

		public Int32 InsertSendMsgSignature(Int32 pMessageID, Byte[] pSignature)
		{
			try
			{
				SendMsgSignature sign = new SendMsgSignature();
				sign.MailSignature = pSignature;
				sign.Send_ID = pMessageID;
				long ID = 0;
				SendMsgSignatureCrud crud = new SendMsgSignatureCrud();
				crud.Insert(sign, out ID);

				return (Int32)ID;	
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertSendMsgSignature - " + Ex.Message);
			}
		}
		
		public Int32 InsertSendMsgAttachment(Int32 pMessageID, Byte[] pAttachment, String pAttachmentName)
		{
			try
			{
				SendMsgAttachment attach = new SendMsgAttachment();
				attach.Attachment = pAttachment;
				attach.AttachmentName = pAttachmentName;
				attach.Send_ID = pMessageID;
				long ID = 0;
				SendMsgAttachmentCrud crud = new SendMsgAttachmentCrud();
				crud.Insert(attach, out ID);
				return (Int32)ID;
			}
			catch (Exception Ex)
			{
				throw new Exception("IsnertSendMsgAttachment - " + Ex.Message);
			}

		}
		public Boolean InsertMailMessage(String pRecipient, String pSender, String pSubject, String pMailText, byte[] pSignature, String pQueuedByApplication)
		{
			try
			{
				Int32 messageID = InsertSendMsg(pRecipient, pSender, pSubject, pMailText, pQueuedByApplication);
				InsertSendMsgSignature(messageID, pSignature);

				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertMailMessage - " + Ex.Message);
			}
		}

		public Boolean InsertMailMessage(String pRecipient, String pSender, String pSubject, String pMailText, byte[] pSignature, List<Byte[]> pAttachment, List<String> pAttachmentName, List<Byte[]> pImage, String pQueuedByApplication)
		{
			try
			{
				Int32 messageID = InsertSendMsg(pRecipient, pSender, pSubject, pMailText, pQueuedByApplication);
				InsertSendMsgSignature(messageID, pSignature);
				Int32 attachCount = 0;
				foreach (Byte[] attach in pAttachment)
				{
					String attachName = pAttachmentName[attachCount]; attachCount++;
					InsertSendMsgAttachment(messageID, attach, attachName);
				}

				foreach (Byte[] image in pImage)
					InsertSendMsgImage(messageID, image);

				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertMailMessage - " + Ex.Message);
			}
		}
        public Boolean InsertBulkMail(List<SendMsg> pMessageList, byte[] pSignature, List<byte[]> pImageList)
        {
            try
            {
				SendMsgCrud crud = new SendMsgCrud();
                foreach (SendMsg message in pMessageList)
                {

					long newMessageID = 0;
					crud.Insert(message, out newMessageID);

					SendMsgSignatureCrud signCrud = new SendMsgSignatureCrud();
					SendMsgSignature signature = new SendMsgSignature();
                    signature.MailSignature = pSignature;
                    signature.Send_ID = (Int32)newMessageID;
					signCrud.Insert(signature);

					SendMsgImageCrud imgCrud = new SendMsgImageCrud();
                    foreach (byte[] image in pImageList)
                    {
                        SendMsgImage img = new SendMsgImage();
                        img.Send_ID = (Int32)newMessageID;
                        img.ImageData = image;
						imgCrud.Insert(img);
                    }
                }

                return true;
            }
            catch (Exception Ex)
            {
				throw Ex;
            }
        }
        public Boolean InsertBulkMail(List<SendMsg> pMessageList, byte[] pSignature)
        {
            try
            {
				SendMsgCrud crud = new SendMsgCrud();
                foreach (SendMsg message in pMessageList)
                {
					long newMessageID = 0;
					crud.Insert(message, out newMessageID);

					SendMsgSignatureCrud signCrud = new SendMsgSignatureCrud();
					SendMsgSignature signature = new SendMsgSignature();
                    signature.MailSignature = pSignature;
                    signature.Send_ID = (Int32)newMessageID;
					signCrud.Insert(signature);
                }

                return true;
            }
            catch (Exception Ex)
            {
				throw Ex;
            }
        }
        public Boolean InsertBulkMail(List<SendMsg> pMessageList, List<byte[]> pAttachmentList, List<String> pAttachmentNames, List<byte[]> pImageList)
        {
            try
            {
				SendMsgCrud crud = new SendMsgCrud();
                foreach (SendMsg message in pMessageList)
                {
					long newMessageID = 0;
					crud.Insert(message, out newMessageID);
                    Int32 counter = 0;
					SendMsgAttachmentCrud attCrud = new SendMsgAttachmentCrud();
					foreach (byte[] attachment in pAttachmentList)
                    {
                        SendMsgAttachment attach = new SendMsgAttachment();
                        attach.Attachment = attachment;
                        attach.AttachmentName = pAttachmentNames[counter];
                        attach.Send_ID = (Int32)newMessageID;
                        counter++;
						attCrud.Insert(attach);
                    }

					SendMsgImageCrud imgCrud = new SendMsgImageCrud();
                    foreach (byte[] image in pImageList)
                    {
                        SendMsgImage img = new SendMsgImage();
                        img.Send_ID = (Int32)newMessageID;
                        img.ImageData = image;
						imgCrud.Insert(img);
                    }
                }
                return true;
            }
            catch (Exception Ex)
            {
				throw Ex;
            }
        }
        public Boolean InsertBulkMail(List<SendMsg> pMessageList, List<byte[]> pAttachmentList, List<String> pAttachmentNames)
        {
            try
            {
				SendMsgCrud crud = new SendMsgCrud();
                foreach (SendMsg message in pMessageList)
                {
					long newMessageID = 0;
					crud.Insert(message, out newMessageID);
                    Int32 counter = 0;
					SendMsgAttachmentCrud attCrud = new SendMsgAttachmentCrud();
                    foreach (byte[] attachment in pAttachmentList)
                    {
                        SendMsgAttachment attach = new SendMsgAttachment();
                        attach.Attachment = attachment;
                        attach.AttachmentName = pAttachmentNames[counter];
                        attach.Send_ID = (Int32)newMessageID;
                        counter++;
						attCrud.Insert(attach);
                    }
                }
                return true;
            }
            catch (Exception Ex)
            {
				throw Ex;
            }
        }
        public Boolean InsertBulkMail(List<SendMsg> pMessageList, List<byte[]> pAttachmentList, List<String> pAttachmentNames, byte[] pSignature)
        {
            try
            {
				SendMsgCrud crud = new SendMsgCrud();
                foreach (SendMsg message in pMessageList)
                {
					long newMessageID = 0;
					crud.Insert(message, out newMessageID);
					SendMsgAttachmentCrud attCrud = new SendMsgAttachmentCrud();
					Int32 counter = 0;
					foreach (byte[] attachment in pAttachmentList)
                    {
                        SendMsgAttachment attach = new SendMsgAttachment();
                        attach.Attachment = attachment;
                        attach.AttachmentName = pAttachmentNames[counter];
                        attach.Send_ID = (Int32)newMessageID;
                        counter++;
						attCrud.Insert(attach);
                    }

					SendMsgSignatureCrud signCrud = new SendMsgSignatureCrud();
					SendMsgSignature signature = new SendMsgSignature();
                    signature.MailSignature = pSignature;
                    signature.Send_ID = (Int32)newMessageID;
					signCrud.Insert(signature);
                }
                return true;
            }
            catch (Exception Ex)
            {
				throw Ex;
            }
        }
        public Boolean InsertBulkMail(List<SendMsg> pMessageList, List<byte[]> pAttachmentList, List<String> pAttachmentNames, byte[] pSignature, List<byte[]> pImageList)
        {
            try
            {
				SendMsgCrud crud = new SendMsgCrud();
                foreach (SendMsg message in pMessageList)
                {
					long newMessageID = 0;
					crud.Insert(message, out newMessageID);
                    Int32 counter = 0;
					SendMsgAttachmentCrud attCrud = new SendMsgAttachmentCrud();

					foreach (byte[] attachment in pAttachmentList)
                    {
                        SendMsgAttachment attach = new SendMsgAttachment();
                        attach.Attachment = attachment;
                        attach.AttachmentName = pAttachmentNames[counter];
                        attach.Send_ID = (Int32)newMessageID;
                        counter++;
						attCrud.Insert(attach);
                    }

					SendMsgImageCrud imgCrud = new SendMsgImageCrud();
					foreach (byte[] image in pImageList)
                    {
                        SendMsgImage img = new SendMsgImage();
                        img.Send_ID = (Int32)newMessageID;
                        img.ImageData = image;
						imgCrud.Insert(img);
                    }

					SendMsgSignatureCrud signCrud = new SendMsgSignatureCrud();
					SendMsgSignature signature = new SendMsgSignature();
                    signature.MailSignature = pSignature;
                    signature.Send_ID = (Int32)newMessageID;
					signCrud.Insert(signature);
                }
                return true;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
