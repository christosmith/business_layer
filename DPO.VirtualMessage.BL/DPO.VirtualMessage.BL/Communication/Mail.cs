﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualMessage.DAL;

namespace DPO.VirtualMessage.BL
{
	public partial class VirtualMessageBL
	{
		public Boolean SendMail(String pSubject, String pMessage, String pRecipient, byte[] pSignature, String pSender)
		{
			try
			{
				SendMsgCrud crud = new SendMsgCrud();
				SendMsg message = new SendMsg();
				message.Date_Inserted = DateTime.Now;
				message.DueDateTime = DateTime.Now;
				message.HTML_Format = true;
				message.Msg = message.Msg = MailBody(pMessage, true);
				message.Msg_Subject = pSubject;
				message.Recipient = pRecipient;
				message.SystemGenerated = false;
				message.Send_Method = 1;
				message.Sender = pSender;
				message.Retry_Count = 0;
				long messageID = 0;
				crud.Insert(message, out messageID);

				SendMsgSignatureCrud signCrud = new SendMsgSignatureCrud();
				SendMsgSignature signature = new SendMsgSignature();
				signature.MailSignature = pSignature;
				signature.Send_ID = (Int32)messageID;
				signCrud.Insert(signature);
				return true;
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
		public Boolean SendMail(String pSubject, String pMessage, String pRecipient, List<Byte[]> pAttachmentList, List<String> pAttachmentNames, byte[] pSignature, String pSender)
		{
			try
			{
				SendMsgCrud crud = new SendMsgCrud();
				SendMsg message = new SendMsg();
				message.Date_Inserted = DateTime.Now;
				message.DueDateTime = DateTime.Now;
				message.HTML_Format = true;
				message.Msg = MailBody(pMessage, true);
				message.Msg_Subject = pSubject;
				message.Recipient = pRecipient;
				message.SystemGenerated = false;
				message.Send_Method = 1;
				message.Retry_Count = 0;
				message.Sender = pSender;
				long messageID = 0;
				crud.Insert(message, out messageID);

				Int32 counter = 0;
				SendMsgAttachmentCrud attCrud = new SendMsgAttachmentCrud();
				foreach (byte[] attachment in pAttachmentList)
				{
					SendMsgAttachment attach = new SendMsgAttachment();
					attach.Attachment = attachment;
					attach.AttachmentName = pAttachmentNames[counter];
					attach.Send_ID = (Int32)messageID;
					counter++;
					attCrud.Insert(attach);
				}
				SendMsgSignatureCrud signCrud = new SendMsgSignatureCrud();
				SendMsgSignature signature = new SendMsgSignature();
				signature.MailSignature = pSignature;
				signature.Send_ID = (Int32)messageID;
				signCrud.Insert(signature);
				return true;
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
		public Boolean SendMail(String pSubject, String pMessage, String pRecipient, List<Byte[]> pAttachmentList, List<String> pAttachmentNames)
		{
			String logInfo = "";
			try
			{
				SendMsgCrud crud = new SendMsgCrud();
				SendMsg message = new SendMsg();
				message.Date_Inserted = DateTime.Now;
				message.DueDateTime = DateTime.Now;
				message.HTML_Format = true;
				logInfo += "BuildMailBody";
				message.Msg = MailBody(pMessage, false);
				message.Msg_Subject = pSubject;
				message.Recipient = pRecipient;
				message.SystemGenerated = false;
				message.Send_Method = 1;
				message.Retry_Count = 0;

				long messageID = 0;
				crud.Insert(message, out messageID);

				logInfo += "|SendMsgInserted";
				Int32 counter = 0;
				SendMsgAttachmentCrud attCrud = new SendMsgAttachmentCrud();
				foreach (byte[] attachment in pAttachmentList)
				{
					SendMsgAttachment attach = new SendMsgAttachment();
					attach.Attachment = attachment;
					attach.AttachmentName = pAttachmentNames[counter];
					attach.Send_ID = (Int32)messageID;
					counter++;
					attCrud.Insert(attach, out messageID);
					logInfo += "|AttachmentInserted";
				}
				return true;

			}
			catch (Exception Ex)
			{
				throw new Exception("SendMail - " + Ex.Message + "|" + logInfo);
			}
		}
		public Boolean SendMail(String pSubject, String pMessage, String pRecipient)
		{
			try
			{
				SendMsgCrud crud = new SendMsgCrud();
				SendMsg message = new SendMsg();
				message.Date_Inserted = DateTime.Now;
				message.DueDateTime = DateTime.Now;
				message.HTML_Format = true;
				if (!useMailAsIs)
					message.Msg = MailBody(pMessage, false);
				else
					message.Msg = pMessage;

				message.Msg_Subject = pSubject;
				message.Recipient = pRecipient;
				message.SystemGenerated = false;
				message.Send_Method = 1;
				message.Retry_Count = 0;
				crud.Insert(message);
				return true;

			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
		public Boolean SendMail(String pSubject, String pMessage, String pRecipient, String pQueuedByApplication, String pSender)
		{
			try
			{
				SendMsgCrud crud = new SendMsgCrud();
				SendMsg message = new SendMsg();
				message.Date_Inserted = DateTime.Now;
				message.DueDateTime = DateTime.Now;
				message.HTML_Format = true;
				if (!useMailAsIs)
					message.Msg = MailBody(pMessage, false);
				else
					message.Msg = pMessage;

				message.Msg_Subject = pSubject;
				message.Recipient = pRecipient;
				message.SystemGenerated = false;
				message.Send_Method = 1;
				message.Retry_Count = 0;
				message.QueuedByApplication = pQueuedByApplication;
				message.Sender = pSender;
				crud.Insert(message);
				return true;

			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
		public Int32 SendMail(String pSubject, String pMessage, String pRecipient, String pQueuedByApplication)
		{
			try
			{
				SendMsgCrud crud = new SendMsgCrud();
				SendMsg message = new SendMsg();
				message.Date_Inserted = DateTime.Now;
				message.DueDateTime = DateTime.Now;
				message.HTML_Format = true;
				if (!useMailAsIs)
					message.Msg = MailBody(pMessage, false);
				else
					message.Msg = pMessage;

				message.Msg_Subject = pSubject;
				message.Recipient = pRecipient;
				message.SystemGenerated = false;
				message.Send_Method = 1;
				message.Retry_Count = 0;
				message.QueuedByApplication = pQueuedByApplication;
				long ID = 0;
				crud.Insert(message, out ID);
				return (Int32)ID;
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
		public Boolean SendMail(String pSubject, List<String> pMessage, String pRecipient, String pQueuedByApplication)
		{
			try
			{
				SendMsgCrud crud = new SendMsgCrud();
				SendMsg message = new SendMsg();
				message.Date_Inserted = DateTime.Now;
				message.DueDateTime = DateTime.Now;
				message.HTML_Format = true;
				message.Msg = ConvertMailBody(pMessage, false);
				message.Msg_Subject = pSubject;
				message.Recipient = pRecipient;
				message.SystemGenerated = false;
				message.Send_Method = 1;
				message.Retry_Count = 0;
				message.QueuedByApplication = pQueuedByApplication;
				crud.Insert(message);
				
				return true;

			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
		public Boolean SendMail(String pSubject, List<String> pMessage, String pRecipient)
		{
			try
			{
				SendMsgCrud crud = new SendMsgCrud();
				SendMsg message = new SendMsg();
				message.Date_Inserted = DateTime.Now;
				message.DueDateTime = DateTime.Now;
				message.HTML_Format = true;
				message.Msg = ConvertMailBody(pMessage, false);
				message.Msg_Subject = pSubject;
				message.Recipient = pRecipient;
				message.SystemGenerated = false;
				message.Send_Method = 1;
				message.Retry_Count = 0;

				crud.Insert(message);
				return true;

			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
		public String ConvertMailBody(List<String> pMessage, Boolean pSignature)
		{
			try
			{
				VCSHTMLMailBuilder.MailBuilder mailBuilder = new VCSHTMLMailBuilder.MailBuilder();
				mailBuilder.Signature = pSignature;
				foreach (String line in pMessage)
				{
					mailBuilder.AddMailEntity(VCSHTMLMailBuilder.eMailEntity.Text, line);
				}

				return mailBuilder.HTMLMailString();
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}

		public String MailBody(String mailMessage, Boolean pSignature)
		{
			try
			{
				mailMessage = mailMessage.Replace("\r", "");
				String[] mailMessageLines = mailMessage.Split('\n');

				VCSHTMLMailBuilder.MailBuilder mailBuilder = new VCSHTMLMailBuilder.MailBuilder();
				mailBuilder.Signature = pSignature;
				for (Int32 messageLoop = 0; messageLoop < mailMessageLines.GetLength(0); messageLoop++)
				{
					mailBuilder.AddMailEntity(VCSHTMLMailBuilder.eMailEntity.Text, mailMessageLines[messageLoop]);
				}

				return mailBuilder.HTMLMailString();


			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}

		public String MailBody(String mailMessage, Boolean pSignature, Int32 pImageCount)
		{
			try
			{
				mailMessage = mailMessage.Replace("\r", "");
				String[] mailMessageLines = mailMessage.Split('\n');

				VCSHTMLMailBuilder.MailBuilder mailBuilder = new VCSHTMLMailBuilder.MailBuilder();
				mailBuilder.Signature = pSignature;
				mailBuilder.ImageCount = pImageCount;
				for (Int32 messageLoop = 0; messageLoop < mailMessageLines.GetLength(0); messageLoop++)
				{
					mailBuilder.AddMailEntity(VCSHTMLMailBuilder.eMailEntity.Text, mailMessageLines[messageLoop]);
				}

				return mailBuilder.HTMLMailString();


			}
			catch (Exception Ex)
			{
				throw Ex;			
			}
		}
	}
}
