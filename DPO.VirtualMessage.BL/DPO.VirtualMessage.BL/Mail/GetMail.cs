﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualMessage.DAL;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace DPO.VirtualMessage.BL
{
public partial class VirtualMessageBL
    {
        public Int32 InsertMail(String pMessage, String pSubject, String pRecipient, Int16? pQueuedByID, Byte pMailSenderID, String pUserName, Int32? pSignatureID, Boolean pSuccess)
        {
            try
            {
                tblMail mail = new tblMail();
                mail.MailMessage = pMessage;
                mail.MailSenderID = pMailSenderID;
                mail.MailSubject = pSubject;
                mail.QueuedByID = pQueuedByID;
                mail.Recipient = pRecipient;
                mail.SignatureID = pSignatureID;
                mail.UserName = pUserName;
                mail.DateSend = DateTime.Now;
                mail.SendSuccess = pSuccess;

                long ID = 0;
                tblMailCrud crud = new tblMailCrud();
                crud.Insert(mail, out ID);

                return (Int32)ID; 
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertMail - " + Ex.Message);
            }
        }

        public Int16 InsertMailQueuedBy(String pQueuedByApplication)
        {
            try
            {
                tblMailQueuedBy by = new tblMailQueuedBy();
                by.QueuedByApplication = pQueuedByApplication;
                tblMailQueuedByCrud crud = new tblMailQueuedByCrud();
                long ID = 0;
                crud.Insert(by, out ID);
                return (Int16)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertMailQueuedBy - " + Ex.Message);
            }
        }

        public Byte InsertMailSender(String pMailSender)
        {
            try
            {
                tblMailSender sender = new tblMailSender();
                sender.MailSender = pMailSender;
                long ID = 0;
                tblMailSenderCrud crud = new tblMailSenderCrud();
                crud.Insert(sender);
                return (Byte)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertMailSender - " + Ex.Message);
            }
        }

        public tblMailQueuedBy GetMailQueuedBy(String pQueuedByApplication)
        {
            try
            {
                tblMailQueuedByCrud crud = new tblMailQueuedByCrud();
                crud.Where("QueuedByApplication", General.Operator.Equals, pQueuedByApplication);
                recordFound = false;
                tblMailQueuedBy by = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return by;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetMailQueuedBy - " + Ex.Message);
            }
        }

        public tblMailError GetMailError(String pMailError)
        {
            try
            {
                tblMailErrorCrud crud = new tblMailErrorCrud();
                crud.Where("MailError", General.Operator.Equals, pMailError);
                recordFound = false;
                tblMailError error = crud.ReadSingle();
                recordFound = crud.RecordFound;

                return error; 
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMailError - " + Ex.Message);
            }
        }

        public Byte InsertMailError(String pMailError)
        {
            try
            {
                tblMailErrorCrud crud = new tblMailErrorCrud();
                tblMailError error = new tblMailError();
                error.MailError = pMailError;
                long ID = 0;
                crud.Insert(error, out ID);
                return (Byte)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertMailError - " + Ex.Message);
            }
        }

        public Int32 InsertMailSendError(Int32 pMailID, Byte pMailErrorID)
        {
            try
            {
                tblMailSendErrorCrud crud = new tblMailSendErrorCrud();
                tblMailSendError err = new tblMailSendError();
                err.MailErrorID = pMailErrorID;
                err.MailID = pMailID;
                long ID = 0;
                crud.Insert(err, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertMailSendError - " + Ex.Message);
            }
        }
        public tblMailSender GetMailSender(String pMailSender)
        {
            try
            {
                tblMailSenderCrud crud = new tblMailSenderCrud();
                crud.Where("MailSender", General.Operator.Equals, pMailSender);
                recordFound = false;
                tblMailSender sender = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return sender;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMailSender - " + Ex.Message);
            }
        }
        public List<tblMailLine> GetMailLine(Int32 pMailHeaderID)
        {
            try
            {
                recordFound = false;
                tblMailLineCrud crud = new tblMailLineCrud();
                crud.Where("MailHeaderID", General.Operator.Equals, pMailHeaderID);

                List<tblMailLine> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMailLine - " + Ex.Message);
            }
        }
		public tblMailHeader GetMailHeader(String pMailDescription)
        {
            try
            {
                recordFound = false;
                tblMailHeaderCrud crud = new tblMailHeaderCrud();
                crud.Where("MailDescription", General.Operator.Equals, pMailDescription);

                tblMailHeader search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMailHeader - " + Ex.Message);
            }
        }
		public Int32 InsertSendMsgImage(Int32 pSendID, Byte[] pImageData)
        {
            try
            {
                SendMsgImageCrud crud = new SendMsgImageCrud();
                SendMsgImage workRec = new SendMsgImage();
                workRec.ImageData = pImageData;
				workRec.Send_ID = pSendID;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertSendMsgImage - " + Ex.Message);
            }
        }
		public List<tblMailData> GetMailData(Int32 pMailHeaderID)
        {
            try
            {
                recordFound = false;
                tblMailDataCrud crud = new tblMailDataCrud();
                crud.Where("MailHeaderID", General.Operator.Equals, pMailHeaderID);

                List<tblMailData> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMailData - " + Ex.Message);
            }
        }
        public List<SendMsgRequeuError> GetSendMsgRequeueError()
        {
            try
            {
                recordFound = false;
                SendMsgRequeuErrorCrud crud = new SendMsgRequeuErrorCrud();
                List<SendMsgRequeuError> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetSendMsgRequeueError - " + Ex.Message);
            }
        }
		public SendMsgSignature GetSendMsgSignatureByMessageID(Int32 pMessageID)
		{
			try
			{
				SendMsgSignatureCrud crud = new SendMsgSignatureCrud();
				crud.Where("Send_ID", General.Operator.Equals, pMessageID);
				recordFound = false;
				SendMsgSignature result = crud.ReadSingle();
				recordFound = crud.RecordFound;

				return result;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetSendMsgSignature - " + Ex.Message);
			}
		}
		public SendMsgSignature GetSendMsgSignature(Int32 pSignatureID)
		{
			try
			{
				SendMsgSignatureCrud crud = new SendMsgSignatureCrud();
				crud.Where("SignatureID", General.Operator.Equals, pSignatureID);
				recordFound = false;
				SendMsgSignature result = crud.ReadSingle();
				recordFound = crud.RecordFound;

				return result;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetSendMsgSignature - " + Ex.Message);
			}
		}
		public Boolean DeleteSendMsgSignature(Int32 pSignatureID)
		{
			try
			{
				SendMsgSignature sign = GetSendMsgSignature(pSignatureID);
                if (recordFound)
                {
                    SendMsgSignatureCrud crud = new SendMsgSignatureCrud();
                    crud.Delete(sign);
                }
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("DeleteSendMsgSignature -" + Ex.Message);
			}
		}


		public SendMsgAttachment GetSendMsgAttachmentByID(Int32 pAttachmentID)
		{
			try
			{
				SendMsgAttachmentCrud crud = new SendMsgAttachmentCrud();
				crud.Where("AttachmentID", General.Operator.Equals, pAttachmentID);
				recordFound = false;
				SendMsgAttachment result = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return result;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetSensMsgAttachment -" + Ex.Message);
			}
		}

		public Boolean UpdateSendMsgDateSent(Int32 pMessageID)
		{
			try
			{
				SendMsg msg = GetSendMsg(pMessageID);
				msg.Date_Sent = DateTime.Now;
				SendMsgCrud crud = new SendMsgCrud();
				crud.Update(msg);
				return true;
			}
			catch( Exception Ex)
			{
				throw new Exception("UpdateSendMsgDateSend - " + Ex.Message);
			}
		}

		public List<SendMsgImage> GetSendMsgImageByMessageID(Int32 pMessageID)
		{
			try
			{
				SendMsgImageCrud crud = new SendMsgImageCrud();
				crud.Where("Send_ID", General.Operator.Equals, pMessageID);
				recordFound = false;
				List<SendMsgImage> results = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return results;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetSendMsgImageByMessageID - " + Ex.Message);
			}
		}

        public Boolean DeleteSendMsgProcessing()
        {
            try
            {
                SendMsgProcessingCrud crud = new SendMsgProcessingCrud();
                crud.Where("ProcessDateTime", General.Operator.DateDiff, General.DateDiffInterval.Second, DateTime.Now, 600, General.Operator.LessThan);
                List<SendMsgProcessing> procs = crud.ReadMulti().ToList();
                foreach (SendMsgProcessing proc in procs)
                    crud.Delete(proc);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteSendMsgProcessing - " + Ex.Message);
            }
        }

        public Boolean DeleteSendMsgProcessing(Int32 pSendID)
        {
            try
            {
                SendMsgProcessingCrud crud = new SendMsgProcessingCrud();
                crud.Where("Send_ID", General.Operator.Equals, pSendID);
                SendMsgProcessing procs = crud.ReadSingle();
                if (crud.RecordFound)
                    crud.Delete(procs);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteSendMsgProcessing - " + Ex.Message);
            }
        }
        public Boolean DeleteSendMsg(Int32 pMessageID)
		{
			try
			{
				SendMsg msg = GetSendMsg(pMessageID);
				SendMsgCrud crud = new SendMsgCrud();
				crud.Delete(msg);
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("DeleteSendMsg - " + Ex.Message);
			}
		}
		public SendMsgImage GetSendMsgImage(Int32 pImageID)
		{
			try
			{
				SendMsgImageCrud crud = new SendMsgImageCrud();
				crud.Where("ImageID", General.Operator.Equals, pImageID);
				recordFound = false;
				SendMsgImage result = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return result;

			}
			catch (Exception Ex)
			{
				throw new Exception("GetSendMsgImage - " + Ex.Message);
			}
		}

		public Boolean DeleteSendMsgImage(Int32 pImageID)
		{
			try
			{
				SendMsgImage img = GetSendMsgImage(pImageID);
				SendMsgImageCrud crud = new SendMsgImageCrud();
				crud.Delete(img);
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("DeleteSendMsgImage - " + Ex.Message);
			}
		}
		public Boolean DeleteSendMsgAttachment(Int32 pAttachmentID)
		{
			try
			{
				SendMsgAttachment attach = GetSendMsgAttachmentByID(pAttachmentID);
				SendMsgAttachmentCrud crud = new SendMsgAttachmentCrud();
				crud.Delete(attach);
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("DeleteSendMsgAttachment - " + Ex.Message);
			}
		}
		public List<SendMsgAttachment> GetSendMsgAttachment(Int32 pMessageID)
		{
			try
			{
				SendMsgAttachmentCrud crud = new SendMsgAttachmentCrud();
				crud.Where("Send_ID", General.Operator.Equals, pMessageID);
				recordFound = false;
				List<SendMsgAttachment> results = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return results;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetSensMsgAttachment -" + Ex.Message);
			}
		}

        public Boolean DeleteOutboxProcessing()
        {
            try
            {
                OutBoxProcessingCrud crud = new OutBoxProcessingCrud();
                crud.Where("ProcessingDateTime", General.Operator.DateDiff, General.DateDiffInterval.Second, DateTime.Now, 600, General.Operator.GreaterThan);
                List<OutBoxProcessing> procs = crud.ReadMulti().ToList();
                foreach (OutBoxProcessing proc in procs)
                    crud.Delete(proc);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteOutboxProcessing - " + Ex.Message);
            }
        }
        public Boolean DeleteOutboxProcessing(Int32 pProcessID)
        {
            try
            {
                OutBoxProcessingCrud crud = new OutBoxProcessingCrud();
                crud.Where("Process_ID", General.Operator.Equals, pProcessID);
                OutBoxProcessing proc = crud.ReadSingle();
                crud.Delete(proc);


                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteOutboxProcessing - " + Ex.Message);
            }
        }
        public Boolean InsertOutboxProcessing(Int32 pProcessID)
        {
            try
            {
                OutBoxProcessingCrud crud = new OutBoxProcessingCrud();
                OutBoxProcessing proc = new OutBoxProcessing();
                proc.Process_ID = pProcessID;
                proc.ProcessingDateTime = DateTime.Now;
                crud.Insert(proc);
                return true;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertOutboxProcessing - " + Ex.Message);
            }
        }

        public Boolean UpdateSendMsgError(Int32 pSendID, String pErrorMessage, Int32 pRetryCount)
        {
            try
            {
                SendMsgCrud crud = new SendMsgCrud();
                SendMsg message = GetSendMsg(pSendID);
                message.Error_Message = pErrorMessage;
                message.Retry_Count = pRetryCount;
                crud.Update(message);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateSendMsgError - " + Ex.Message);
            }
        }

        public SendMsgProcessing GetSendMsgProcessing (Int32 pSendID)
        {
            try
            {
                SendMsgProcessingCrud crud = new SendMsgProcessingCrud();
                crud.Where("Send_ID", General.Operator.Equals, pSendID);
                recordFound = false;
                SendMsgProcessing msg = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return msg;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetSendMsgProcessing - " + Ex.Message);
            }
        }
        public Boolean InsertSendMsgProcessing(Int32 pSendID)
        {
            try
            {
                SendMsgProcessing find = GetSendMsgProcessing(pSendID);
                if (!recordFound)
                {
                    SendMsgProcessingCrud crud = new SendMsgProcessingCrud();
                    SendMsgProcessing proc = new SendMsgProcessing();
                    proc.Send_ID = pSendID;
                    proc.ProcessDateTime = DateTime.Now;
                    crud.Insert(proc);
                }
                return true;

            }
            catch (Exception Ex)
            {
                throw new Exception("InsertSendMsgProcessing - " + Ex.Message);
            }
        }
        public List<SendMsg> GetSendMsg(Int16 pNoOfRecords, Int32 pRetryCount)
        {
            try
            {
                List<SendMsg> results = new List<SendMsg>();
                SendMsgCrud crud = new SendMsgCrud();
                crud.Where("DueDateTime", General.Operator.DateDiff, General.DateDiffInterval.Second, DateTime.Now, 0, General.Operator.LessThan);
                crud.And("RetryCount", General.Operator.LessThan, pRetryCount);
                crud.And("Send_ID", General.Operator.NotIn, "(Select Send_ID From SendMsgProcessing)");
                crud.SelectTop = pNoOfRecords;
                recordFound = false;
                results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return results;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetSendMsg - " + Ex.Message);
            }
        }

        public List<SendMsg> GetSendMsg(String pRecipient, String pSubject)
        {
            try
            {
                List<SendMsg> results = new List<SendMsg>();
                SendMsgCrud crud = new SendMsgCrud();
                
                crud.Where("Msg_Subject", General.Operator.Equals, pSubject);
                crud.And("Recipient", General.Operator.Equals, pRecipient);
                recordFound = false;
                results = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return results;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetSendMsg - " + Ex.Message);
            }
        }
        public SendMsg GetSendMsg(Int32 pMessageID)
		{
			try
			{
				SendMsgCrud crud = new SendMsgCrud();
				crud.Where("Send_ID", General.Operator.Equals, pMessageID);
				recordFound = false;
				SendMsg result = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return result;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetSendMsg - " + Ex.Message);
			}
		}
    }
}
