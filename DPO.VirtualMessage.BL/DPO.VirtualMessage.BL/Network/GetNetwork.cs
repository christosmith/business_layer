﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualMessage.DAL;

namespace DPO.VirtualMessage.BL
{
public partial class VirtualMessageBL
    {
        public Boolean DivertNetwork(Byte pNetworkID, String pInterfaceNo, Boolean pDivert)
        {
			OutboxNetworkCrud crud = new OutboxNetworkCrud();
			OutboxNetwork network = GetNetwork(pNetworkID);
            network.DivertInterface = pDivert;
            network.DivertInterfaceNo = pInterfaceNo;
			crud.Insert(network);
            return true;
        }
        public List<String> GetInterfaceNumber()
        {
			try
			{
				List<String> InterfaceNo = new List<string>();
				List<OutboxNetwork> networks = GetNetwork();
				InterfaceNo = networks.Select(x => x.InterfaceNo).Distinct().ToList();
				return InterfaceNo;
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
		public Boolean DeleteNetworkPhoneRange(Int32 pPhoneRangeID)
        {
			try
			{
				OutboxPhoneRangeCrud crud = new OutboxPhoneRangeCrud();
				crud.Where("RangeID", General.Operator.Equals, pPhoneRangeID);
				List<OutboxPhoneRange> ranges = crud.ReadMulti().ToList();
				foreach (OutboxPhoneRange range in ranges)
					crud.Delete(range);

			}
			catch (Exception Ex)
			{
				throw Ex;
			}
			return true;
        }
        public Int32 SaveNetwork(Byte pCountryID, String pNetwork, Boolean pActive, String pInterfaceNo, Byte pNetworkID)
        {
            try
            {
                OutboxNetworkCrud crud = new OutboxNetworkCrud();
                OutboxNetwork workRec = new OutboxNetwork();
                if (pCountryID != 0)
                {
                    crud.Where("CountryID", General.Operator.Equals, pCountryID);
                    workRec = crud.ReadSingle();
                    workRec.Network = pNetwork;
                    workRec.Active = pActive;
                    workRec.InterfaceNo = pInterfaceNo;
                    workRec.NetworkID = pNetworkID;
                    crud.Update(workRec);
                    return pCountryID;
                }
                else
                {
                    workRec.Network = pNetwork;
                    workRec.Active = pActive;
                    workRec.InterfaceNo = pInterfaceNo;
                    workRec.NetworkID = pNetworkID;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("SaveNetworkByte - " + Ex.Message);
            }
        }
        public Int32 SaveNetworkPhoneRange(Byte pNetworkID, String pStartRange, String pEndRange, Int32 pRangeID)
        {
            try
            {
                OutboxPhoneRangeCrud crud = new OutboxPhoneRangeCrud();
                OutboxPhoneRange workRec = new OutboxPhoneRange();
                if (pNetworkID != 0)
                {
                    crud.Where("NetworkID", General.Operator.Equals, pNetworkID);
                    workRec = crud.ReadSingle();
                    workRec.StartRange = pStartRange;
                    workRec.EndRange = pEndRange;
                    workRec.RangeID = pRangeID;
                    crud.Update(workRec);
                    return pNetworkID;
                }
                else
                {
                    workRec.StartRange = pStartRange;
                    workRec.EndRange = pEndRange;
                    workRec.RangeID = pRangeID;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("SaveNetworkPhoneRangeByte - " + Ex.Message);
            }
        }
        public String GetNetworkPhoneRangeGridData(Byte pNetworkID)
        {
			try
			{
				List<OutboxPhoneRange> ranges = GetNetworkPhoneRange(pNetworkID);
				String divString = "";
				foreach (OutboxPhoneRange range in ranges)
				{
					divString += range.RangeID + "═";
					divString += range.NetworkID + "═";
					divString += range.StartRange + "═";
					divString += range.EndRange + "║";
				}

				if (divString.Length > 0)
					divString = divString.Substring(0, divString.Length - 1);

				return divString;
			}
            catch (Exception Ex)
            {
                throw new Exception("GetNetworkPhoneRangeGridDataByte - " + Ex.Message);
            }
        }
        public OutboxPhoneRange GetNetworkPhoneRange(Int32 pRangeID)
        {
            try
            {
                recordFound = false;
                OutboxPhoneRangeCrud crud = new OutboxPhoneRangeCrud();
                crud.Where("RangeID", General.Operator.Equals, pRangeID);

                OutboxPhoneRange search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetNetworkPhoneRange - " + Ex.Message);
            }
        }
        public List<OutboxPhoneRange> GetNetworkPhoneRange(Byte pNetworkID)
        {
            try
            {
                recordFound = false;
                OutboxPhoneRangeCrud crud = new OutboxPhoneRangeCrud();
                crud.Where("NetworkID", General.Operator.Equals, pNetworkID);

                List<OutboxPhoneRange> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetNetworkPhoneRangeByte - " + Ex.Message);
            }
        }
        public List<OutboxCountry> GetNetworkCountry()
        {
            try
            {
                recordFound = false;
                OutboxCountryCrud crud = new OutboxCountryCrud();
                List<OutboxCountry> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetNetworkCountry - " + Ex.Message);
            }
        }
		public OutboxNetwork GetNetwork(Byte pNetworkID)
        {
            try
            {
                recordFound = false;
                OutboxNetworkCrud crud = new OutboxNetworkCrud();
                crud.Where("NetworkID", General.Operator.Equals, pNetworkID);

                OutboxNetwork search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetNetworkByte - " + Ex.Message);
            }
        }
		public List<OutboxNetwork> GetNetwork()
        {
            try
            {
                recordFound = false;
                OutboxNetworkCrud crud = new OutboxNetworkCrud();
                List<OutboxNetwork> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetNetwork - " + Ex.Message);
            }
        }
		public String GetNetworkGridData()
        {
			try
			{
				List<OutboxNetwork> networks = GetNetwork();
            String divString = "";
            foreach (OutboxNetwork network in networks)
            {
                divString += network.NetworkID + "═";
                divString += network.CountryID + "═";
                divString += network.OutboxCountry.Country + "═";
                divString += network.Network + "═";
                divString += network.Active + "═";
                divString += network.InterfaceNo + "═";
                divString += network.DivertInterface + "═";
                divString += network.DivertInterfaceNo + "═";
				divString += network.UseBlastSms.ToString() + "║";
            }

            if (divString.Length > 0)
                divString = divString.Substring(0, divString.Length - 1);

            return divString;
			}
            catch (Exception Ex)
            {
                throw new Exception("GetNetworkGridData - " + Ex.Message);
            }
        }
		public OutboxNetwork GetOutboxNetwork(Byte pNetworkID)
        {
            try
            {
                recordFound = false;
                OutboxNetworkCrud crud = new OutboxNetworkCrud();
                crud.Where("NetworkID", General.Operator.Equals, pNetworkID);

                OutboxNetwork search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetOutboxNetworkByte - " + Ex.Message);
            }
        }
		public Boolean ToggelBlastSms(Byte pNetworkID)
		{
			try
			{
				OutboxNetwork network = GetOutboxNetwork(pNetworkID);
				OutboxNetworkCrud crud = new OutboxNetworkCrud();
				if (RecordFound)
				{
					Boolean useBlastSms = Convert.ToBoolean(network.UseBlastSms);
					useBlastSms = !useBlastSms;
					network.UseBlastSms = useBlastSms;
					crud.Update(network);
				}
				else
					throw new Exception("Network was not found!");
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("ToggleBlastSms-" + Ex.Message);
			}

		}
    }
}
