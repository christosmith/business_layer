﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DPO.VirtualMessage.BL.CarmaRequestSeat {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SeatsResponse", Namespace="http://tempuri.org/")]
    [System.SerializableAttribute()]
    public partial class SeatsResponse : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string STImpIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string STTermField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string INTraceField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string INErrorField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string STErrorField;
        
        private int INSeatsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private DPO.VirtualMessage.BL.CarmaRequestSeat.PassengerReference[] ResponseField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string STImpID {
            get {
                return this.STImpIDField;
            }
            set {
                if ((object.ReferenceEquals(this.STImpIDField, value) != true)) {
                    this.STImpIDField = value;
                    this.RaisePropertyChanged("STImpID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string STTerm {
            get {
                return this.STTermField;
            }
            set {
                if ((object.ReferenceEquals(this.STTermField, value) != true)) {
                    this.STTermField = value;
                    this.RaisePropertyChanged("STTerm");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string INTrace {
            get {
                return this.INTraceField;
            }
            set {
                if ((object.ReferenceEquals(this.INTraceField, value) != true)) {
                    this.INTraceField = value;
                    this.RaisePropertyChanged("INTrace");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string INError {
            get {
                return this.INErrorField;
            }
            set {
                if ((object.ReferenceEquals(this.INErrorField, value) != true)) {
                    this.INErrorField = value;
                    this.RaisePropertyChanged("INError");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string STError {
            get {
                return this.STErrorField;
            }
            set {
                if ((object.ReferenceEquals(this.STErrorField, value) != true)) {
                    this.STErrorField = value;
                    this.RaisePropertyChanged("STError");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=5)]
        public int INSeats {
            get {
                return this.INSeatsField;
            }
            set {
                if ((this.INSeatsField.Equals(value) != true)) {
                    this.INSeatsField = value;
                    this.RaisePropertyChanged("INSeats");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=6)]
        public DPO.VirtualMessage.BL.CarmaRequestSeat.PassengerReference[] Response {
            get {
                return this.ResponseField;
            }
            set {
                if ((object.ReferenceEquals(this.ResponseField, value) != true)) {
                    this.ResponseField = value;
                    this.RaisePropertyChanged("Response");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="PassengerReference", Namespace="http://tempuri.org/")]
    [System.SerializableAttribute()]
    public partial class PassengerReference : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private int INPassNrField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string STReferenceField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public int INPassNr {
            get {
                return this.INPassNrField;
            }
            set {
                if ((this.INPassNrField.Equals(value) != true)) {
                    this.INPassNrField = value;
                    this.RaisePropertyChanged("INPassNr");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false)]
        public string STReference {
            get {
                return this.STReferenceField;
            }
            set {
                if ((object.ReferenceEquals(this.STReferenceField, value) != true)) {
                    this.STReferenceField = value;
                    this.RaisePropertyChanged("STReference");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="CarmaRequestSeat.RequestSeatSoap")]
    public interface RequestSeatSoap {
        
        // CODEGEN: Generating message contract since element name HelloWorldResult from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/HelloWorld", ReplyAction="*")]
        DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldResponse HelloWorld(DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/HelloWorld", ReplyAction="*")]
        System.Threading.Tasks.Task<DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldResponse> HelloWorldAsync(DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldRequest request);
        
        // CODEGEN: Generating message contract since element name STImpID from namespace http://tempuri.org/ is not marked nillable
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Seat", ReplyAction="*")]
        DPO.VirtualMessage.BL.CarmaRequestSeat.SeatResponse Seat(DPO.VirtualMessage.BL.CarmaRequestSeat.SeatRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/Seat", ReplyAction="*")]
        System.Threading.Tasks.Task<DPO.VirtualMessage.BL.CarmaRequestSeat.SeatResponse> SeatAsync(DPO.VirtualMessage.BL.CarmaRequestSeat.SeatRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class HelloWorldRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="HelloWorld", Namespace="http://tempuri.org/", Order=0)]
        public DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldRequestBody Body;
        
        public HelloWorldRequest() {
        }
        
        public HelloWorldRequest(DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute()]
    public partial class HelloWorldRequestBody {
        
        public HelloWorldRequestBody() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class HelloWorldResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="HelloWorldResponse", Namespace="http://tempuri.org/", Order=0)]
        public DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldResponseBody Body;
        
        public HelloWorldResponse() {
        }
        
        public HelloWorldResponse(DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class HelloWorldResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string HelloWorldResult;
        
        public HelloWorldResponseBody() {
        }
        
        public HelloWorldResponseBody(string HelloWorldResult) {
            this.HelloWorldResult = HelloWorldResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class SeatRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="Seat", Namespace="http://tempuri.org/", Order=0)]
        public DPO.VirtualMessage.BL.CarmaRequestSeat.SeatRequestBody Body;
        
        public SeatRequest() {
        }
        
        public SeatRequest(DPO.VirtualMessage.BL.CarmaRequestSeat.SeatRequestBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class SeatRequestBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public string STImpID;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=1)]
        public string STTerm;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=2)]
        public string INSeats;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=3)]
        public string INTrace;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=4)]
        public string STServiceNr;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=5)]
        public string DTDepDate;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=6)]
        public string STTicketType;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=7)]
        public string STCarrier;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=8)]
        public string STBoard;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=9)]
        public string STDest;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=10)]
        public string STClass;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=11)]
        public string STAcc;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=12)]
        public string STRetServiceNr;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=13)]
        public string DTRetDepDate;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=14)]
        public string STRetCarrier;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=15)]
        public string STRetBoard;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=16)]
        public string STRetDest;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=17)]
        public string STRetClass;
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=18)]
        public string STRetAcc;
        
        public SeatRequestBody() {
        }
        
        public SeatRequestBody(
                    string STImpID, 
                    string STTerm, 
                    string INSeats, 
                    string INTrace, 
                    string STServiceNr, 
                    string DTDepDate, 
                    string STTicketType, 
                    string STCarrier, 
                    string STBoard, 
                    string STDest, 
                    string STClass, 
                    string STAcc, 
                    string STRetServiceNr, 
                    string DTRetDepDate, 
                    string STRetCarrier, 
                    string STRetBoard, 
                    string STRetDest, 
                    string STRetClass, 
                    string STRetAcc) {
            this.STImpID = STImpID;
            this.STTerm = STTerm;
            this.INSeats = INSeats;
            this.INTrace = INTrace;
            this.STServiceNr = STServiceNr;
            this.DTDepDate = DTDepDate;
            this.STTicketType = STTicketType;
            this.STCarrier = STCarrier;
            this.STBoard = STBoard;
            this.STDest = STDest;
            this.STClass = STClass;
            this.STAcc = STAcc;
            this.STRetServiceNr = STRetServiceNr;
            this.DTRetDepDate = DTRetDepDate;
            this.STRetCarrier = STRetCarrier;
            this.STRetBoard = STRetBoard;
            this.STRetDest = STRetDest;
            this.STRetClass = STRetClass;
            this.STRetAcc = STRetAcc;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class SeatResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Name="SeatResponse", Namespace="http://tempuri.org/", Order=0)]
        public DPO.VirtualMessage.BL.CarmaRequestSeat.SeatResponseBody Body;
        
        public SeatResponse() {
        }
        
        public SeatResponse(DPO.VirtualMessage.BL.CarmaRequestSeat.SeatResponseBody Body) {
            this.Body = Body;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace="http://tempuri.org/")]
    public partial class SeatResponseBody {
        
        [System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue=false, Order=0)]
        public DPO.VirtualMessage.BL.CarmaRequestSeat.SeatsResponse SeatResult;
        
        public SeatResponseBody() {
        }
        
        public SeatResponseBody(DPO.VirtualMessage.BL.CarmaRequestSeat.SeatsResponse SeatResult) {
            this.SeatResult = SeatResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface RequestSeatSoapChannel : DPO.VirtualMessage.BL.CarmaRequestSeat.RequestSeatSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class RequestSeatSoapClient : System.ServiceModel.ClientBase<DPO.VirtualMessage.BL.CarmaRequestSeat.RequestSeatSoap>, DPO.VirtualMessage.BL.CarmaRequestSeat.RequestSeatSoap {
        
        public RequestSeatSoapClient() {
        }
        
        public RequestSeatSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public RequestSeatSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public RequestSeatSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public RequestSeatSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldResponse DPO.VirtualMessage.BL.CarmaRequestSeat.RequestSeatSoap.HelloWorld(DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldRequest request) {
            return base.Channel.HelloWorld(request);
        }
        
        public string HelloWorld() {
            DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldRequest inValue = new DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldRequest();
            inValue.Body = new DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldRequestBody();
            DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldResponse retVal = ((DPO.VirtualMessage.BL.CarmaRequestSeat.RequestSeatSoap)(this)).HelloWorld(inValue);
            return retVal.Body.HelloWorldResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldResponse> DPO.VirtualMessage.BL.CarmaRequestSeat.RequestSeatSoap.HelloWorldAsync(DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldRequest request) {
            return base.Channel.HelloWorldAsync(request);
        }
        
        public System.Threading.Tasks.Task<DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldResponse> HelloWorldAsync() {
            DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldRequest inValue = new DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldRequest();
            inValue.Body = new DPO.VirtualMessage.BL.CarmaRequestSeat.HelloWorldRequestBody();
            return ((DPO.VirtualMessage.BL.CarmaRequestSeat.RequestSeatSoap)(this)).HelloWorldAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        DPO.VirtualMessage.BL.CarmaRequestSeat.SeatResponse DPO.VirtualMessage.BL.CarmaRequestSeat.RequestSeatSoap.Seat(DPO.VirtualMessage.BL.CarmaRequestSeat.SeatRequest request) {
            return base.Channel.Seat(request);
        }
        
        public DPO.VirtualMessage.BL.CarmaRequestSeat.SeatsResponse Seat(
                    string STImpID, 
                    string STTerm, 
                    string INSeats, 
                    string INTrace, 
                    string STServiceNr, 
                    string DTDepDate, 
                    string STTicketType, 
                    string STCarrier, 
                    string STBoard, 
                    string STDest, 
                    string STClass, 
                    string STAcc, 
                    string STRetServiceNr, 
                    string DTRetDepDate, 
                    string STRetCarrier, 
                    string STRetBoard, 
                    string STRetDest, 
                    string STRetClass, 
                    string STRetAcc) {
            DPO.VirtualMessage.BL.CarmaRequestSeat.SeatRequest inValue = new DPO.VirtualMessage.BL.CarmaRequestSeat.SeatRequest();
            inValue.Body = new DPO.VirtualMessage.BL.CarmaRequestSeat.SeatRequestBody();
            inValue.Body.STImpID = STImpID;
            inValue.Body.STTerm = STTerm;
            inValue.Body.INSeats = INSeats;
            inValue.Body.INTrace = INTrace;
            inValue.Body.STServiceNr = STServiceNr;
            inValue.Body.DTDepDate = DTDepDate;
            inValue.Body.STTicketType = STTicketType;
            inValue.Body.STCarrier = STCarrier;
            inValue.Body.STBoard = STBoard;
            inValue.Body.STDest = STDest;
            inValue.Body.STClass = STClass;
            inValue.Body.STAcc = STAcc;
            inValue.Body.STRetServiceNr = STRetServiceNr;
            inValue.Body.DTRetDepDate = DTRetDepDate;
            inValue.Body.STRetCarrier = STRetCarrier;
            inValue.Body.STRetBoard = STRetBoard;
            inValue.Body.STRetDest = STRetDest;
            inValue.Body.STRetClass = STRetClass;
            inValue.Body.STRetAcc = STRetAcc;
            DPO.VirtualMessage.BL.CarmaRequestSeat.SeatResponse retVal = ((DPO.VirtualMessage.BL.CarmaRequestSeat.RequestSeatSoap)(this)).Seat(inValue);
            return retVal.Body.SeatResult;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<DPO.VirtualMessage.BL.CarmaRequestSeat.SeatResponse> DPO.VirtualMessage.BL.CarmaRequestSeat.RequestSeatSoap.SeatAsync(DPO.VirtualMessage.BL.CarmaRequestSeat.SeatRequest request) {
            return base.Channel.SeatAsync(request);
        }
        
        public System.Threading.Tasks.Task<DPO.VirtualMessage.BL.CarmaRequestSeat.SeatResponse> SeatAsync(
                    string STImpID, 
                    string STTerm, 
                    string INSeats, 
                    string INTrace, 
                    string STServiceNr, 
                    string DTDepDate, 
                    string STTicketType, 
                    string STCarrier, 
                    string STBoard, 
                    string STDest, 
                    string STClass, 
                    string STAcc, 
                    string STRetServiceNr, 
                    string DTRetDepDate, 
                    string STRetCarrier, 
                    string STRetBoard, 
                    string STRetDest, 
                    string STRetClass, 
                    string STRetAcc) {
            DPO.VirtualMessage.BL.CarmaRequestSeat.SeatRequest inValue = new DPO.VirtualMessage.BL.CarmaRequestSeat.SeatRequest();
            inValue.Body = new DPO.VirtualMessage.BL.CarmaRequestSeat.SeatRequestBody();
            inValue.Body.STImpID = STImpID;
            inValue.Body.STTerm = STTerm;
            inValue.Body.INSeats = INSeats;
            inValue.Body.INTrace = INTrace;
            inValue.Body.STServiceNr = STServiceNr;
            inValue.Body.DTDepDate = DTDepDate;
            inValue.Body.STTicketType = STTicketType;
            inValue.Body.STCarrier = STCarrier;
            inValue.Body.STBoard = STBoard;
            inValue.Body.STDest = STDest;
            inValue.Body.STClass = STClass;
            inValue.Body.STAcc = STAcc;
            inValue.Body.STRetServiceNr = STRetServiceNr;
            inValue.Body.DTRetDepDate = DTRetDepDate;
            inValue.Body.STRetCarrier = STRetCarrier;
            inValue.Body.STRetBoard = STRetBoard;
            inValue.Body.STRetDest = STRetDest;
            inValue.Body.STRetClass = STRetClass;
            inValue.Body.STRetAcc = STRetAcc;
            return ((DPO.VirtualMessage.BL.CarmaRequestSeat.RequestSeatSoap)(this)).SeatAsync(inValue);
        }
    }
}
