﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualMessage.DAL;
using System.Data.SqlClient;

namespace DPO.VirtualMessage.BL
{
public partial class VirtualMessageBL
    {
        public List<SmsDeliveryReport> GetSmsDeliveryReport()
        {
            try
            {
                recordFound = false;
                SmsDeliveryReportCrud crud = new SmsDeliveryReportCrud();
                List<SmsDeliveryReport> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetSmsDeliveryReport - " + Ex.Message);
            }
        }
		public Boolean OutboxUpdateDeliveryReport()
        {
            try
            {
                String sCellPhone = "";
                
                String sDeliveryZid = "";
                String sDeliverySubCode = "";
                String sDeliveryDlvrdCode = "";
                String sDeliverySubmitDateTime = "";
                String sDeliveryDoneDate = "";
                String sDeliveryStatus = "";
                String sDeliveryErrCode = "";
                String sSMSMessage = "";

                List<SmsDeliveryReport> reports = GetSmsDeliveryReport();

                foreach (SmsDeliveryReport report in reports)
                {
                    String deliveryReport = report.DeliveryReport;

                    using (System.Xml.XmlReader reader = System.Xml.XmlReader.Create(new System.IO.StringReader(deliveryReport)))
                    {

                        while (reader.Read())
                        {
                            switch (reader.NodeType)
                            {
                                case System.Xml.XmlNodeType.Element:
                                    switch (reader.Name)
                                    {
                                        case "DestinationNumber":
                                            reader.Read();
                                            sCellPhone = reader.Value;
                                            break;
                                        case "SMSMessage":
                                            reader.Read();
                                            String[] sMessage = reader.Value.Split(' ');
                                            sDeliveryZid = sMessage[0].Substring(sMessage[0].IndexOf(":") + 1);
                                            sDeliverySubCode = sMessage[1].Substring(sMessage[1].IndexOf(":") + 1);
                                            sDeliveryDlvrdCode = sMessage[2].Substring(sMessage[2].IndexOf(":") + 1);
                                            sDeliverySubmitDateTime = sMessage[4].Substring(sMessage[4].IndexOf(":") + 1);
                                            sDeliveryDoneDate = sMessage[6].Substring(sMessage[6].IndexOf(":") + 1);
                                            sDeliveryStatus = sMessage[7].Substring(sMessage[7].IndexOf(":") + 1);
                                            sDeliveryErrCode = sMessage[8].Substring(sMessage[8].IndexOf(":") + 1);
                                            sSMSMessage = sMessage[9].Substring(sMessage[9].IndexOf(":") + 1) + " ";
                                            for (Int32 messageLoop = 10; messageLoop < sMessage.GetLength(0); messageLoop++)
                                            {
                                                sSMSMessage += sMessage[messageLoop] + " ";
                                            }

                                            sSMSMessage = sSMSMessage.Substring(0, sSMSMessage.Length - 1);
                                            goto DoneWithReader;
                                            break;

                                    }
                                    break;
                            }

                        }
                    DoneWithReader:
                        reader.Close();

                        //Update the outbox record                 
                        DateTime deliverySubmitDateTime = Convert.ToDateTime("20" + sDeliverySubmitDateTime.Substring(0, 2) + "-" + sDeliverySubmitDateTime.Substring(2, 2) + "-" + sDeliverySubmitDateTime.Substring(4, 2) + " " + sDeliverySubmitDateTime.Substring(6, 2) + ":" + sDeliverySubmitDateTime.Substring(8, 2));
                        DateTime deliveryDoneDateTime = Convert.ToDateTime("20" + sDeliveryDoneDate.Substring(0, 2) + "-" + sDeliveryDoneDate.Substring(2, 2) + "-" + sDeliveryDoneDate.Substring(4, 2) + " " + sDeliveryDoneDate.Substring(6, 2) + ":" + sDeliveryDoneDate.Substring(8, 2));
                        
                        StoredProcedure.spOutboxDeliveryReportUpd(sCellPhone, deliverySubmitDateTime, sSMSMessage, sDeliveryZid, sDeliverySubCode, sDeliveryDlvrdCode, deliveryDoneDateTime, sDeliveryStatus, sDeliveryErrCode, Convert.ToInt32(report.MessageID), Convert.ToInt32(report.DeliveryReportID));


                    }
                }

                return true;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
