﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualMessage.DAL;

namespace DPO.VirtualMessage.BL
{
	public partial class VirtualMessageBL
	{
		public Boolean DeleteOutbox(Int32 pProcess_ID)
		{
			try
			{
				OutBox outbox = GetOutbox(pProcess_ID);
				OutBoxCrud crud = new OutBoxCrud();
				crud.Delete(outbox); 
				return true;
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}

		public String GetInterfaceNumber(String pMobileNo)
		{
			try
			{
				List<spOutboxInterface> outInterface = StoredProcedure.spOutboxInterface(pMobileNo);
				String interfaceNo = outInterface[0].InterfaceNo;
				return interfaceNo;

			}
			catch (Exception Ex)
			{
				throw new Exception("GetInterfaceNumber-" + Ex.Message);
			}
		}
		public Int32 SendSms(String pMessage, String pRecipient, String pContactName, String pUserID, String pQueuedByApplication)
		{
            try
            {
                //List<String> recipients = pRecipient.Split(';').ToList();

                OutBoxCrud crud = new OutBoxCrud();
                //foreach (String recipient in recipients)
                //{
                OutBox outbox = new OutBox();
                outbox.MobileNo = pRecipient;
                outbox.Message = pMessage;
                outbox.ContactName = pContactName;
                outbox.Mode = "000";
                outbox.Monday = "Y";
                outbox.Tuesday = "Y";
                outbox.Wednesday = "Y";
                outbox.Thursday = "Y";
                outbox.Friday = "Y";
                outbox.Saturday = "Y";
                outbox.Sunday = "Y";
                outbox.Processed = "N";
                outbox.UserId = pUserID;
                outbox.DateQueued = DateTime.Now;
                outbox.EndTime = null;
                outbox.DeliverySubmitDateTime = DateTime.Now;
                outbox.QueuedByApplication = pQueuedByApplication;
                long ID = 0;
                crud.Insert(outbox, out ID);

                return (Int32)ID;

                //}
                //return true;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
		}
		public Boolean SendSms(String pMessage, String pRecipient, String pContactName, String pUserID)
		{
			try
			{
				List<String> recipients = pRecipient.Split(';').ToList();
				OutBoxCrud crud = new OutBoxCrud();
				foreach (String recipient in recipients)
				{
					OutBox outbox = new OutBox();
					outbox.MobileNo = recipient;
					outbox.Message = pMessage;
					outbox.ContactName = pContactName;
					outbox.Mode = "000";
					outbox.Monday = "Y";
					outbox.Tuesday = "Y";
					outbox.Wednesday = "Y";
					outbox.Thursday = "Y";
					outbox.Friday = "Y";
					outbox.Saturday = "Y";
					outbox.Sunday = "Y";
					outbox.Processed = "N";
					outbox.UserId = pUserID;
					outbox.EndTime = null; 
					outbox.DateQueued = DateTime.Now;
					outbox.DeliverySubmitDateTime = DateTime.Now;
					crud.Insert(outbox);
				}
				return true;
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
		public Boolean SendSms(String pMessage, String pRecipient)
		{
			try
			{
				List<String> recipients = pRecipient.Split(';').ToList();
				OutBoxCrud crud = new OutBoxCrud();
				foreach (String recipient in recipients)
				{
					OutBox outbox = new OutBox();
					outbox.MobileNo = recipient;
					outbox.Message = pMessage;
					outbox.ContactName = "BackOffice";
					outbox.Mode = "000";
					outbox.Monday = "Y";
					outbox.Tuesday = "Y";
					outbox.Wednesday = "Y";
					outbox.Thursday = "Y";
					outbox.Friday = "Y";
					outbox.Saturday = "Y";
					outbox.Sunday = "Y";
					outbox.Processed = "N";
					outbox.UserId = "vcs061";
					outbox.EndTime = null;
					outbox.DateQueued = DateTime.Now;
					outbox.DeliverySubmitDateTime = DateTime.Now;
					long id = 0;
					crud.Insert(outbox, out id);
				}
				return true;
			}
			catch (Exception Ex)
			{
				throw Ex;
			}

		}
		public String GetOutboxGridData(DateTime pStartDate, DateTime pEndDate, String pStartTime, String pEndTime, Boolean pQueued, Boolean pDelivered, Boolean pDelayed, Boolean pError, String pLastID, String pPhoneNo, Int32 pProcessID)
		{
			List<spGetOutbox> outboxList = GetOutbox(pStartDate, pEndDate, pStartTime, pEndTime, pQueued, pDelivered, pDelayed, pError, pLastID, pPhoneNo, pProcessID);
			String divString = "";
			foreach (spGetOutbox outbox in outboxList)
			{
				divString += outbox.Process_ID + "═";
				divString += outbox.MobileNo + "═";
				divString += outbox.UserID + "═";
				divString += outbox.Department + "═";
				divString += outbox.RecipientID + "═";
				divString += outbox.Reference + "═";
				divString += Convert.ToDateTime(outbox.DateQueued).ToString("yyyy-MM-dd HH:mm") + "═";
				if (outbox.DateProcessed != null)
					divString += Convert.ToDateTime(outbox.DateProcessed).ToString("yyyy-MM-dd HH:mm") + "═";
				else
					divString += "═";
				if (outbox.DelayDelivery != null)
					divString += Convert.ToDateTime(outbox.DelayDelivery).ToString("yyyy-MM-dd HH:mm") + "═";
				else
					divString += "═";
				divString += outbox.DelayCount + "═";
				divString += outbox.InterfaceNo + "═";
				divString += outbox.Processed + "═";
				divString += outbox.ErrorCount + "═";
				divString += outbox.ErrorMessage + "═";
				divString += outbox.Network + "║";
			}

			if (divString.Length > 0)
				divString = divString.Substring(0, divString.Length - 1);

			return divString;
		}

		public Boolean RequeueSms(Int32 pProcessID)
		{
			try
			{
				OutBoxCrud crud = new OutBoxCrud();
				OutBox outbox = GetOutbox(pProcessID);
				outbox.Processed = "N";
				outbox.DateSent = null;
				outbox.EndTime = null;
				crud.Update(outbox);


				OutboxErrorsCrud crudErr = new OutboxErrorsCrud();
				crudErr.Where("Process_ID", General.Operator.Equals, pProcessID);
				OutboxErrors err = crudErr.ReadSingle();
				crudErr.Delete(err);
				return true;
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}

		public Boolean DeleteOutboxError(Int32 pProcessID)
		{
			try
			{
				OutboxErrorsCrud crudErr = new OutboxErrorsCrud();
				crudErr.Where("Process_ID", General.Operator.Equals, pProcessID);
				OutboxErrors err = crudErr.ReadSingle();
				crudErr.Delete(err);
				return true;
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
        public List<OutBox> GetOutbox(String pMobileNo, String pMessage)
        {
            try
            {
                OutBoxCrud crud = new OutBoxCrud();
                crud.Where("Message", General.Operator.Like, "%" + pMessage + "%");
                crud.And("MobileNo", General.Operator.Equals, pMobileNo );
                crud.And("Processed", General.Operator.Equals, "N");
                recordFound = false;
                List<OutBox> result = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;

                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetOutBox - " + Ex.Message);
            }
        }
		public OutBox GetOutbox(Int32 pProcessID)
		{
			try
			{
				OutBoxCrud crud = new OutBoxCrud();
				crud.Where("Process_ID", General.Operator.Equals, pProcessID);
				OutBox outbox = crud.ReadSingle();
				return outbox;
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
		public List<spGetOutbox> GetOutbox(DateTime pStartDate, DateTime pEndDate, String pStartTime, String pEndTime, Boolean pQueued, Boolean pDelivered, Boolean pDelayed, Boolean pError, String pLastID, String pPhoneNo, Int32 pProcessID)
		{
			return StoredProcedure.spGetOutbox(pStartDate, pEndDate, pStartTime, pEndTime, pQueued, pDelivered, pDelayed, pError, pLastID, pPhoneNo, pProcessID);
		}
		public String GetOutBoxQueuedGridData()
		{
			List<OutBox> messages = GetOutBoxQueued();
			String divString = "";
			foreach (OutBox message in messages)
			{
				divString += message.Process_ID + "═";
				divString += message.MobileNo + "═";
				divString += Convert.ToDateTime(message.DateQueued).ToString("yyyy-MM-dd HH:mm") + "═";
				divString += message.DelayCount + "═";
				if (Convert.ToDateTime(message.DelayDelivery).Year != 1)
					divString += Convert.ToDateTime(message.DelayDelivery).ToString("yyyy-MM-dd HH:mm") + "║";
				else
					divString += "║";
			}

			if (divString.Length > 0)
				divString = divString.Substring(0, divString.Length - 1);

			return divString;
		}
		public List<OutBox> GetOutBoxQueued()
		{
			OutBoxCrud crud = new OutBoxCrud();
			crud.Where("Processed", General.Operator.Equals, "N");
			List<OutBox> results = crud.ReadMulti().ToList();
			return results;
		}
	}
}
