﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualMessage.DAL;

namespace DPO.VirtualMessage.BL
{
	public partial class VirtualMessageBL
	{
		public Users GetUsers(String pUserID)
		{
			try
			{
				recordFound = false;
				UsersCrud crud = new UsersCrud();
				crud.Where("UserID", General.Operator.Equals, pUserID);

				Users search = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetUser - " + Ex.Message);
			}
		}
	}
}
