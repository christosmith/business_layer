﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualMessage.DAL;


namespace DPO.VirtualMessage.BL
{

	public partial class VirtualMessageBL
	{

		public tblBrUssdSelection GetBrUssdSelection(String pSelectionName)
		{
			try
			{
				tblBrUssdSelectionCrud crud = new tblBrUssdSelectionCrud();
				crud.Where("SelectionName", General.Operator.Equals, pSelectionName);
				recordFound = false;
				tblBrUssdSelection result = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return result;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetBrUssdSelection - " + Ex.Message);
			}
		}
		public Boolean UpdateBrTransactionRecon()
		{
			try
			{
				List<tblBrTransaction> transactions = GetBrTransactionForRecon();
				tblBrTransactionCrud crud = new tblBrTransactionCrud();
				foreach (tblBrTransaction tran in transactions)
				{
					tran.SendForRecon = true;
					crud.Update(tran);
				}

				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("UpdateBrTransactionRecon-" + Ex.Message);
			}
		}
		public List<tblBrTransaction> GetBrTransactionForRecon()
		{
			try
			{
				recordFound = false;
				tblBrTransactionCrud crud = new tblBrTransactionCrud();
				List<tblBrTransaction> search = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetBrTransactionForRecon - " + Ex.Message);
			}
		}

		public CarmaInformation GetBrSeat(CarmaInformation pCarmaInfo)
		{
			try
			{
				String account = "SI";
				if (pCarmaInfo.CoachClass == "First")
					if (pCarmaInfo.PassengerTitle == "Mr")
						account = "MA";
					else
						account = "FE";
				CarmaRequestSeat.RequestSeatSoapClient clientRequestSeat = new CarmaRequestSeat.RequestSeatSoapClient();
				CarmaRequestSeat.SeatsResponse requestSeat = clientRequestSeat.Seat("FNB", "INTERNET", "1", "0", pCarmaInfo.DepartServiceNr, pCarmaInfo.DepartDate, pCarmaInfo.TicketType, "BW", pCarmaInfo.FromPoint, pCarmaInfo.ToPoint, pCarmaInfo.CoachClass, account, pCarmaInfo.ReturnServiceNr, pCarmaInfo.ReturnDate, "BW", pCarmaInfo.ToPoint, pCarmaInfo.FromPoint, pCarmaInfo.CoachClass, account);
				if (requestSeat.INError != "0")
				{
					pCarmaInfo.VCSResponseCode = "9999";
					pCarmaInfo.VCSResponseMessage = requestSeat.STError;
				}
				else
				{
					pCarmaInfo.BookingReferenceNo = requestSeat.INTrace;
				}

				return pCarmaInfo;


			}
			catch (Exception Ex)
			{
				throw new Exception("GetBrSeat-" + Ex.Message);
			}
		}
		public tblBrTransaction GetBrTransaction(String pBookingReferenceNo)
		{
			try
			{
				recordFound = false;
				tblBrTransactionCrud crud = new tblBrTransactionCrud();
				crud.Where("BookingReferenceNo", General.Operator.Equals, pBookingReferenceNo);

				tblBrTransaction search = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetBrTransaction - " + Ex.Message);
			}
		}
		public tblBrTransaction GetBrTransaction(Int32 pBrTransactionID)
		{
			try
			{
				recordFound = false;
				tblBrTransactionCrud crud = new tblBrTransactionCrud();
				crud.Where("BrTransactionID", General.Operator.Equals, pBrTransactionID);

				tblBrTransaction search = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetBrTransaction - " + Ex.Message);
			}
		}
		public Byte[] GetBarCodeImage(String pEncodingValue)
		{
			try
			{
                
				BarcodeLib.Barcode barCode = new BarcodeLib.Barcode();
				barCode.Alignment = BarcodeLib.AlignmentPositions.CENTER;
				BarcodeLib.TYPE barcodeType = BarcodeLib.TYPE.CODE128B;
				barCode.IncludeLabel = false;
				System.Drawing.Image barcodeImage = barCode.Encode(barcodeType, pEncodingValue, System.Drawing.Color.Black, System.Drawing.Color.White, 250, 80);
				return barCode.GetImageData(BarcodeLib.SaveTypes.PNG);
			}
			catch (Exception Ex)
			{
				throw new Exception("GetBarCodeImage-" + Ex.Message);
			}
		}
		public Boolean SendBrTicket(Int32 pBrTransactionID)
		{
			try
			{

				tblBrTransaction tran = GetBrTransaction(pBrTransactionID);

				if (tran.PassengerEmail.Trim() == "")
					return true;
				/*VirtualMessageWCFDev.iVMessClient vMessClient = new VirtualMessageWCFDev.iVMessClient();
				VirtualMessageWCFDev.MailInfomration par = new VirtualMessageWCFDev.MailInfomration();
				List<VirtualMessageWCFDev.MailInfomration> pars = new List<VirtualMessageWCFDev.MailInfomration>();*/


				VirtualMessageWCF.iVMessClient vMessClient = new VirtualMessageWCF.iVMessClient();
				VirtualMessageWCF.MailInfomration par = new VirtualMessageWCF.MailInfomration();
				List<VirtualMessageWCF.MailInfomration> pars = new List<VirtualMessageWCF.MailInfomration>();

				par.ParameterName = "{?BrTransactionID}";
				par.ParameterValue = pBrTransactionID.ToString();
				pars.Add(par);

				vMessClient.SendMail("Botswana Railway Receipt", pars.ToArray(), tran.PassengerEmail);
				String[] departBarCodes = tran.DepartBarCode.Split(',');
				String departCode = "";
				for (Int32 codeLoop = 0; codeLoop < departBarCodes.Length; codeLoop++)
				{
					Int32 code = Convert.ToInt32(departBarCodes[codeLoop].Trim());
					departCode += (char)code;
				}


				par = new VirtualMessageWCF.MailInfomration();
				pars = new List<VirtualMessageWCF.MailInfomration>();

				/*par = new VirtualMessageWCFDev.MailInfomration();
				pars = new List<VirtualMessageWCFDev.MailInfomration>();*/

				par.ParameterName = "{?BrTransactionID}";
				par.ParameterValue = pBrTransactionID.ToString();
				pars.Add(par);
				List<VirtualMessageWCF.MailImage> mailImages = new List<VirtualMessageWCF.MailImage>();
				VirtualMessageWCF.MailImage mailImage = new VirtualMessageWCF.MailImage();

				/*List<VirtualMessageWCFDev.MailImage> mailImages = new List<VirtualMessageWCFDev.MailImage>();
				VirtualMessageWCFDev.MailImage mailImage = new VirtualMessageWCFDev.MailImage();*/

				mailImage.MailImageData = GetBarCodeImage(departCode);
				mailImage.MailImageName = "[BarCode]";
				mailImages.Add(mailImage);

				VirtualMessageWCF.WCFResponse response = vMessClient.SendMailWithImage("Botswana Railway Ticket - Depart", pars.ToArray(), mailImages.ToArray(), tran.PassengerEmail);
				//VirtualMessageWCFDev.WCFResponse response = vMessClient.SendMailWithImage("Botswana Railway Ticket - Depart", pars.ToArray(), mailImages.ToArray(), tran.PassengerEmail);

				if (tran.ReturnBarCode != "")
				{
					response = vMessClient.SendMailWithImage("Botswana Railway Ticket - Return", pars.ToArray(), mailImages.ToArray(), tran.PassengerEmail);
				}

				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("SendBrTicket-" + Ex.Message);
			}
		}
		public Int32 InsertBrServiceError(Int16 pUssdSelectionID, String pCarmaMethod, String pCarmaMessage)
		{
			try
			{
				tblBrServiceErrorCrud crud = new tblBrServiceErrorCrud();
				tblBrServiceError workRec = new tblBrServiceError();
				workRec.CarmaMethod = pCarmaMethod;
				workRec.CarmaMessage = pCarmaMessage;
				workRec.UssdSelectionID = pUssdSelectionID;
				workRec.CallDateTime = DateTime.Now;
				long ID = 0;
				crud.Insert(workRec, out ID);
				return (Int32)ID;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertBrServiceErrorInt16 - " + Ex.Message);
			}
		}
		public Boolean DeleteBrUssdSelection()
		{
			try
			{
				tblBrUssdSelectionCrud crud = new tblBrUssdSelectionCrud();

				List<tblBrUssdSelection> search = crud.ReadMulti().ToList();
				foreach (tblBrUssdSelection del in search)
					crud.Delete(del);
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("DeleteBrUssdSelection - " + Ex.Message);
			}
		}
		public Int32 InsertBrUssdSelection(String pSelectionName, Int32 pDepartPointID, Int32 pArrivalPointID, Int32 pTicketTypeID, Int32 pClassID, Int32 pTitleID, Boolean pEmail)
		{
			try
			{
				tblBrUssdSelectionCrud crud = new tblBrUssdSelectionCrud();
				tblBrUssdSelection workRec = new tblBrUssdSelection();
				workRec.DepartPointID = (Byte)pDepartPointID;
				workRec.ArrivalPointID = (Byte)pArrivalPointID;
				workRec.TicketTypeID = (Byte)pTicketTypeID;
				workRec.ClassID = (Byte)pClassID;
				workRec.TitleID = (Byte)pTitleID;
				workRec.EmailIndicator = pEmail;
				workRec.SelectionName = pSelectionName;
				long ID = 0;
				crud.Insert(workRec, out ID);
				return (Int32)ID;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertBrUssdSelection - " + Ex.Message);
			}
		}
		public Int32 InsertBrTransaction(Int32 pUssdSelectionID, String pMobileNo, String pSurname, String pInitials, String pEMail, DateTime pDepartArrivalDateTime, DateTime pDepartBoardDateTime, String pDepartServiceNo, Decimal pDepartPrice, DateTime pReturnArrivalDateTime, DateTime pReturnBoardDateTime, String pReturnServiceNr, Decimal pReturnPrice, String pBookingReferenceNo, String pDepartBarCode, String pReturnBarCode, String pDepartTicketNo, String pReturnTicketNo, String pDepartCoachNo, String pDepartCompartmentNo, String pReturnCoachNo, String pReturnCompartmentNo, String pPassengerID, String pNextOfKinContact, String pDepartSeatNo, String pReturnSeatNo, String pVCSReferenceNumber)
		{
			try
			{
				tblBrTransactionCrud crud = new tblBrTransactionCrud();
				tblBrTransaction workRec = new tblBrTransaction();
				workRec.SelectionID = (Int16)pUssdSelectionID;
				workRec.PassengerMobileNo = pMobileNo;
				workRec.PassengerSurname = pSurname;
				workRec.PassengerInitials = pInitials;
				workRec.PassengerEmail = pEMail;
				workRec.DepartArrivalDateTime = pDepartArrivalDateTime;
				workRec.DepartBoardingDateTime = pDepartBoardDateTime;
				workRec.DepartServiceNr = pDepartServiceNo;
				workRec.DepartPrice = pDepartPrice;
				workRec.ReturnArrivalDateTime = pReturnArrivalDateTime;
				workRec.ReturnBoardingDateTime = pReturnBoardDateTime;
				workRec.ReturnServiceNr = pReturnServiceNr;
				workRec.ReturnPrice = pReturnPrice;
				workRec.BookingReferenceNo = pBookingReferenceNo;
				workRec.DepartBarCode = pDepartBarCode;
				workRec.ReturnBarCode = pReturnBarCode;
				workRec.DepartTicketNo = pDepartTicketNo;
				workRec.ReturnTicketNo = pReturnTicketNo;
				workRec.DepartCoachNo = pDepartCoachNo;
				workRec.DepartCompartmentNo = pDepartCompartmentNo;
				workRec.ReturnCoachNo = pReturnCoachNo;
				workRec.ReturnCompartmentNo = pReturnCompartmentNo;
				workRec.PassengerIDNo = pPassengerID;
				workRec.NextOfKinPhoneNo = pNextOfKinContact;
				workRec.DepartSeatNo = pDepartSeatNo;
				workRec.ReturnSeatNo = pReturnSeatNo;
				workRec.VCSReferenceNumber = pVCSReferenceNumber;
				workRec.TransactionDateTime = DateTime.Now;
				workRec.SendForRecon = false;
				long ID = 0;
				crud.Insert(workRec, out ID);
				return (Int32)ID;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertBrTransaction - " + Ex.Message);
			}
		}
		public CarmaInformation PayBrTicket(CarmaInformation pCarmaInfo)
		{
			try
			{

				CarmaPayment.PaymentSoapClient clientPayment = new BL.CarmaPayment.PaymentSoapClient();
				CarmaPayment.Passenger passenger = new BL.CarmaPayment.Passenger();
				CarmaPayment.Passenger[] passengers = new BL.CarmaPayment.Passenger[1];

				passenger.INPassNr = 1;
				passenger.STCellNr = pCarmaInfo.PassengerMobileNo;
				passenger.STDiscount = "J";
				passenger.STEMail = pCarmaInfo.PassengerEmail;
				passenger.STInfant = "N";
				passenger.STInitials = pCarmaInfo.PassengerInitials;
				passenger.STPassport = pCarmaInfo.PassengerID;
				passenger.STPassType = "ADULT";
				passenger.STSurname = pCarmaInfo.PassengerSurname;
				passenger.STTitle = pCarmaInfo.PassengerTitle;


				passengers[0] = passenger;
				CarmaPayment.PayResponse payment = clientPayment.Pay("FNB", "INTERNET", pCarmaInfo.BookingReferenceNo, 1, "BW", passengers, pCarmaInfo.NextOfKinContact, "VCS0000000", pCarmaInfo.PassengerMobileNo);
				if (payment.INError != "0")
				{
					pCarmaInfo.VCSResponseCode = "9999";
					pCarmaInfo.VCSResponseMessage = payment.STError;
				}
				else
				{
					pCarmaInfo.DepartBarCode = payment.Response[0].STBarcode;
					pCarmaInfo.DepartTicketNo = payment.Response[0].STTicketnr;
					pCarmaInfo.DepartCoachNumber = payment.Response[0].STCoach.Trim();
					pCarmaInfo.DepartCompartmentNumber = payment.Response[0].STCompartment.Trim();
					pCarmaInfo.DepartSeatNo = payment.Response[0].STSeatnr.Trim();
					if (pCarmaInfo.TicketType.ToLower() == "return")
					{
						pCarmaInfo.ReturnBarCode = payment.Response[1].STBarcode;
						pCarmaInfo.ReturnTicketNo = payment.Response[1].STTicketnr;
						pCarmaInfo.ReturnCoachNumber = payment.Response[1].STCoach.Trim();
						pCarmaInfo.ReturnCompartmentNumber = payment.Response[1].STCompartment.Trim();
						pCarmaInfo.ReturnSeatNo = payment.Response[1].STSeatnr.Trim();
					}
				}

				return pCarmaInfo;
			}
			catch (Exception Ex)
			{
				throw new Exception("PayBrTicket-" + Ex.Message);
			}
		}
		public CarmaInformation GetBrAvailability(CarmaInformation pCarmaInfo)
		{
			try
			{

				System.IO.File.Delete("C:\\VCS\\BrAvailabilit.txt");
				System.IO.StreamWriter writer = new System.IO.StreamWriter("C:\\VCS\\BrAvailabilit.txt", true);
				writer.WriteLine("A");
				writer.Close();
				String log = "A";
				String account = "SI";
				if (pCarmaInfo.CoachClass == "First")
					if (pCarmaInfo.PassengerTitle == "Mr")
						account = "MA";
					else
						account = "FE";

				CarmaAvailability.AvailabilitySoapClient clientAvailability = new BL.CarmaAvailability.AvailabilitySoapClient();
				CarmaAvailability.AvailabilityResponse availabilityResponse = clientAvailability.GetAvail("FNB", "INTERNET", "1", "T", pCarmaInfo.FromPoint, pCarmaInfo.ToPoint, pCarmaInfo.CoachClass, pCarmaInfo.TicketType, account, pCarmaInfo.DepartDate, "BW", "BWP", pCarmaInfo.ReturnDate);
				writer = new System.IO.StreamWriter("C:\\VCS\\BrAvailabilit.txt", true);
				writer.WriteLine("B");
				writer.Close();
				if (availabilityResponse.INError != "0")
				{
					writer = new System.IO.StreamWriter("C:\\VCS\\BrAvailabilit.txt", true);
					writer.WriteLine("C");
					writer.Close();
					pCarmaInfo.VCSResponseCode = "9999";
					pCarmaInfo.VCSResponseMessage = availabilityResponse.STError;
				}
				else
				{
					writer = new System.IO.StreamWriter("C:\\VCS\\BrAvailabilit.txt", true);
					writer.WriteLine("D");
					writer.Close();
					pCarmaInfo.DepartArrivalDateTime = availabilityResponse.Response[0].DTArrival;
					pCarmaInfo.DepartBoardingDateTime = availabilityResponse.Response[0].DTBoard;
					pCarmaInfo.DepartPrice = availabilityResponse.Response[0].CRPrice;
					pCarmaInfo.DepartServiceNr = availabilityResponse.Response[0].STServiceNr;
					if (pCarmaInfo.TicketType.ToLower() == "return")
					{
						writer = new System.IO.StreamWriter("C:\\VCS\\BrAvailabilit.txt", true);
						writer.WriteLine(DateTime.Now.ToString() + "\tE");
						writer.Close();
						writer = new System.IO.StreamWriter("C:\\VCS\\BrAvailabilit.txt", true);
						writer.WriteLine("Count\t" + availabilityResponse.Response.Count().ToString());
						writer.Close();

						if (availabilityResponse.Response.Count() == 1)
						{
							writer = new System.IO.StreamWriter("C:\\VCS\\BrAvailabilit.txt", true);
							writer.WriteLine(DateTime.Now.ToString() + "\tNo return tickect data");
							writer.Close();
							pCarmaInfo.VCSResponseCode = "1009";
							pCarmaInfo.VCSResponseMessage = "No return ticket data received from Carma";
							return pCarmaInfo;
						}

						pCarmaInfo.ReturnArrivalDateTime = availabilityResponse.Response[1].DTArrival;
						pCarmaInfo.ReturnBoardingDateTime = availabilityResponse.Response[1].DTBoard;
						pCarmaInfo.ReturnPrice = availabilityResponse.Response[1].CRPrice;
						pCarmaInfo.ReturnServiceNr = availabilityResponse.Response[1].STServiceNr;
					}
				}

				writer = new System.IO.StreamWriter("C:\\VCS\\BrAvailabilit.txt");
				writer.WriteLine("F");
				writer.Close();
				pCarmaInfo.VCSResponseMessage += "|" + log;
				return pCarmaInfo;

			}
			catch (Exception Ex)
			{
				throw new Exception("GetBrAvailability-" + Ex.Message);
			}
		}
		public List<tblBrTitle> GetBrTitle()
		{
			try
			{
				recordFound = false;
				tblBrTitleCrud crud = new tblBrTitleCrud();
				List<tblBrTitle> search = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetBrTitle - " + Ex.Message);
			}
		}
		public List<tblBrClass> GetBrClass()
		{
			try
			{
				recordFound = false;
				tblBrClassCrud crud = new tblBrClassCrud();
				List<tblBrClass> search = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetBrClass - " + Ex.Message);
			}
		}
		public List<tblBrTicketType> GetBrTicketType()
		{
			try
			{
				recordFound = false;
				tblBrTicketTypeCrud crud = new tblBrTicketTypeCrud();
				List<tblBrTicketType> search = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetBrTicketType - " + Ex.Message);
			}
		}
		public List<tblBrPoint> GetBrPoint()
		{
			try
			{
				recordFound = false;
				tblBrPointCrud crud = new tblBrPointCrud();
				List<tblBrPoint> search = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetBrPoint - " + Ex.Message);
			}
		}
		public tblBrPoint GetBrPoint(Int32 pPointID)
		{
			try
			{
				recordFound = false;
				tblBrPointCrud crud = new tblBrPointCrud();
				crud.Where("PointID", General.Operator.Equals, pPointID);

				tblBrPoint search = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return search;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetBrPoint - " + Ex.Message);
			}
		}

	}
}
