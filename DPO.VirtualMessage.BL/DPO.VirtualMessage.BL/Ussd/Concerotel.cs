﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualMessage.DAL;
using System.Net;
using System.IO;


namespace DPO.VirtualMessage.BL
{

	public partial class VirtualMessageBL
	{

		public String PostToConcerotel(String pUrl, String pMobileNo, String pAmount, String pUniqueID, String pHash, String pTimeStamp)
		{
			System.Net.WebClient client = new System.Net.WebClient();
			try
			{

				ConcerotelData conData = new ConcerotelData();
				conData.amount = pAmount;
				conData.hash = pHash;
				conData.id = pUniqueID;
				conData.mobile = pMobileNo;
				conData.result = "Approved";
				conData.timestamp = Convert.ToInt32(pTimeStamp);

				System.Web.Script.Serialization.JavaScriptSerializer jSerial = new System.Web.Script.Serialization.JavaScriptSerializer();
				String jsonData = jSerial.Serialize(conData);

				jsonData = "{\"transaction\":" + jsonData + "}";

				string responseJson = string.Empty;


				using (client)
				{
					client.Headers[System.Net.HttpRequestHeader.Accept] = "application/json";
					client.Headers[System.Net.HttpRequestHeader.ContentType] = "application/json";

					byte[] response = client.UploadData(pUrl, Encoding.UTF8.GetBytes(jsonData));

					responseJson = Encoding.UTF8.GetString(response);
				}

				return responseJson;
			}
			catch (System.Net.WebException webEx)
			{
				if (webEx.Response != null)
				{
					var resp = new System.IO.StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
					dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject(resp);
					String messageFromServer = obj.error.ToString();

					return "Error=" + messageFromServer;
				}
				else
					return "Error=" + webEx.Message;
			}
			catch (Exception Ex)
			{
				throw new Exception("PostToConcerotel-" + Ex.Message);
			}
		}

	}
}
