﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualMessage.DAL;
using System.Net;
using System.IO;

namespace DPO.VirtualMessage.BL
{
	public class TswanaGasCity
	{
		public Int32 CityID = 0;
		public String CityName = "";
	}

	public class TswanaGasCentre
	{
		public Int32 CentreID = 0;
		public String CentreName = "";
	}

	public class TswanaGasCylinder
	{
		public Int32 CylinderID = 0;
		public String CylinderName = "";
		public Decimal CylinderPrice = 0;
	}

	public partial class VirtualMessageBL
	{
		public Boolean TswanaGasSync()
		{
			try
			{
				List<TswanaGasCity> cities = GetTswanaGasCity();
				foreach (TswanaGasCity city in cities)
				{
					tblTswanaGasCity dbCity = GetTswanaGasCity(city.CityID);
					if (!recordFound)
					{
						InsertTswanaGasCity(city.CityID, city.CityName);
					}
					else
					{
						if (dbCity.CityName != city.CityName)
							UpdateTswanaGasCity(city.CityID, city.CityName);
					}

					List<TswanaGasCentre> centres = GetTswanaGasCentreJSON(city.CityID);
					foreach (TswanaGasCentre centre in centres)
					{
						tblTswanaGasCentre dbCentre = GetTswanaGasCentre(centre.CentreID);
						if (!recordFound)
							InsertTswanaGasCentre(city.CityID, centre.CentreID, centre.CentreName);

					}
				}

				List<TswanaGasCylinder> cylinders = GetTswanaGasCylinder();
				foreach (TswanaGasCylinder cylinder in cylinders)
				{
					tblTswanaGasCylinder dbCylinder = GetTswanaGasCylinder(cylinder.CylinderID);
					if (!recordFound)
						InsertTswanaGasCylinder(cylinder.CylinderID, cylinder.CylinderName,  cylinder.CylinderPrice);
						
				}

				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("TwanaGasSync - " + Ex.Message);
			}
		}
		
		public Boolean InsertTswanaGasCylinder(Int32 pCylinderID, String pCylinderName, Decimal pCylinderPrice)
		{
			try
			{
				tblTswanaGasCylinder cyl = new tblTswanaGasCylinder();
				cyl.CylinderID = pCylinderID;
				cyl.CylinderName = pCylinderName;
				cyl.CylinderPrice = pCylinderPrice;
				tblTswanaGasCylinderCrud crud = new tblTswanaGasCylinderCrud();
				crud.Insert(cyl);
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertTswanaGasCylinder - " + Ex.Message);
			}
		}
		public Boolean InsertTswanaGasCentre(Int32 pCityID, Int32 pCentreID, String pCentreName)
		{
			try
			{
				tblTswanaGasCentre centre = new tblTswanaGasCentre();
				centre.CentreID = pCentreID;
				centre.CentreName = pCentreName;
				centre.CityID = pCityID;
				tblTswanaGasCentreCrud crud = new tblTswanaGasCentreCrud();
				crud.Insert(centre);
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertTswanaGasCentre -" + Ex.Message);
			}
		}
		public Boolean InsertTswanaGasCity(Int32 pCityID, String pCityName)
		{
			try
			{
				tblTswanaGasCityCrud crud = new tblTswanaGasCityCrud();
				tblTswanaGasCity city = new tblTswanaGasCity();
				city.CityID = pCityID;
				city.CityName = pCityName;
				crud.Insert(city);
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertTswanaGasCity -" + Ex.Message);
			}
		}

		public Boolean UpdateTswanaGasCity(Int32 pCityID, String pCityName)
		{
			try
			{
				tblTswanaGasCity city = GetTswanaGasCity(pCityID);
				if (!recordFound)
				{
					city.CityName = pCityName;
					tblTswanaGasCityCrud crud = new tblTswanaGasCityCrud();
					crud.Update(city);
				}

				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("UpdateTswanaGasCity - " + Ex.Message);
			}
		}
		public List<TswanaGasCylinder> GetTswanaGasCylinder()
		{
			try
			{
                BackOfficeWCF.iBackOfficeClient backClient = new BackOfficeWCF.iBackOfficeClient();
                BackOfficeWCF.ApplicationSetting appSetting = backClient.GetApplicationSetting("Virtual Message", "VCS.VirtualMessage.Ussd", "CylinderUrl");
				String pUrl = appSetting.SettingValue;
				List<TswanaGasCylinder> cylinders = new List<TswanaGasCylinder>();
				var httpWebRequest = (HttpWebRequest)WebRequest.Create(pUrl);
				httpWebRequest.ContentType = "application/json";
				httpWebRequest.Method = "GET";
				var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
				String result = "";
				using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
				{
					result = streamReader.ReadToEnd();
				}

				if (!result.Contains("[") && !result.Contains("]"))
					throw new Exception(result);


				result = result.Replace("[", "").Replace("]", "").Replace("\"id\":", "").Replace("\"name\":", "").Replace("\"", "").Replace("price:", "");
				String[] cyls = result.Split('}');

				for (Int32 citLoop = 0; citLoop < cyls.GetUpperBound(0); citLoop++)
				{
					cyls[citLoop] = cyls[citLoop].Replace(",{", "").Replace("{", "");
				}

				for (Int32 cylLoop = 0; cylLoop < cyls.GetUpperBound(0); cylLoop++)
				{
					if (cyls[cylLoop] != "")
					{
						String[] workCyl = cyls[cylLoop].Split(',');
						Int32 cylID = Convert.ToInt32(workCyl[0]);
						String cylName= workCyl[1];
						Decimal cylPrice = Convert.ToDecimal(workCyl[2]);
						TswanaGasCylinder cyl = new TswanaGasCylinder();
						cyl.CylinderID = cylID;
						cyl.CylinderName = cylName;
						cyl.CylinderPrice = cylPrice;
						cylinders.Add(cyl);
					}
				}
				return cylinders;
			}
			catch (System.Net.WebException webEx)
			{
				if (webEx.Response != null)
				{
					var resp = new System.IO.StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
					dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject(resp);
					String messageFromServer = obj.error.ToString();

					throw new Exception("Error=" + messageFromServer);
				}
				else
					throw new Exception("Error=" + webEx.Message);
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCylinder-" + Ex.Message);
			}

		}
		public List<TswanaGasCentre> GetTswanaGasCentreJSON(Int32 pCityID)
		{
			try
			{
				BackOfficeWCF.iBackOfficeClient backClient = new BackOfficeWCF.iBackOfficeClient();
				BackOfficeWCF.ApplicationSetting appSetting = backClient.GetApplicationSetting("Virtual Message", "VCS.VirtualMessage.Ussd", "CentreUrl");
				String pUrl = appSetting.SettingValue;
				pUrl += "?city=" + pCityID.ToString();
				List<TswanaGasCentre> centres = new List<TswanaGasCentre>();
				var httpWebRequest = (HttpWebRequest)WebRequest.Create(pUrl);
				httpWebRequest.ContentType = "application/json";
				httpWebRequest.Method = "GET";
				var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
				String result = "";
				using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
				{
					result = streamReader.ReadToEnd();
				}

				if (result.Contains("Please provide City id"))
					return centres;

				if (!result.Contains("[") && !result.Contains("]"))
					throw new Exception(result);
				

				result = result.Replace("[", "").Replace("]", "").Replace("\"id\":", "").Replace("\"name\":", "").Replace("\"", "");
				String[] centrs = result.Split('}');

				for (Int32 citLoop = 0; citLoop < centrs.GetUpperBound(0); citLoop++)
				{
					centrs[citLoop] = centrs[citLoop].Replace(",{", "").Replace("{", "");
				}

				for (Int32 citLoop = 0; citLoop < centrs.GetUpperBound(0); citLoop++)
				{
					if (centrs[citLoop] != "")
					{
						String[] workCentre = centrs[citLoop].Split(',');
						Int32 cityID = Convert.ToInt32(workCentre[0]);
						String cityName = workCentre[1];
						TswanaGasCentre centre = new TswanaGasCentre();
						centre.CentreID = cityID;
						centre.CentreName = cityName;
						centres.Add(centre);
					}
				}


				return centres;
			}
			catch (System.Net.WebException webEx)
			{
				if (webEx.Response != null)
				{
					var resp = new System.IO.StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
					dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject(resp);
					String messageFromServer = obj.error.ToString();

					throw new Exception("Error=" + messageFromServer);
				}
				else
					throw new Exception("Error=" + webEx.Message);
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCentre-" + Ex.Message);
			}

		}
		public List<TswanaGasCity> GetTswanaGasCity()
		{
			try
			{
				BackOfficeWCF.iBackOfficeClient backClient = new BackOfficeWCF.iBackOfficeClient();
				BackOfficeWCF.ApplicationSetting appSetting = backClient.GetApplicationSetting("Virtual Message", "VCS.VirtualMessage.Ussd", "CityURL");
				String pUrl = appSetting.SettingValue;
				List<TswanaGasCity> cities = new List<TswanaGasCity>();
				var httpWebRequest = (HttpWebRequest)WebRequest.Create(pUrl);
				httpWebRequest.ContentType = "application/json";
				httpWebRequest.Method = "GET";
				var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
				String result = "";
				using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
				{
					result = streamReader.ReadToEnd();
				}

				result = result.Replace("[", "").Replace("]", "").Replace("\"id\":", "").Replace("\"name\":", "").Replace("\"", "");
				String[] cits = result.Split('}');

				for (Int32 citLoop = 0; citLoop < cits.GetUpperBound(0); citLoop++)
				{
					cits[citLoop] = cits[citLoop].Replace(",{", "").Replace("{", "");
				}

				for (Int32 citLoop = 0; citLoop < cits.GetUpperBound(0); citLoop++)
				{
					if (cits[citLoop] != "")
					{
						String[] workCity = cits[citLoop].Split(',');
						Int32 cityID = Convert.ToInt32(workCity[0]);
						String cityName = workCity[1];
						TswanaGasCity city = new TswanaGasCity();
						city.CityID = cityID;
						city.CityName = cityName;
						cities.Add(city);
					}
				}


				return cities;
			}
			catch (System.Net.WebException webEx)
			{
				if (webEx.Response != null)
				{
					var resp = new System.IO.StreamReader(webEx.Response.GetResponseStream()).ReadToEnd();
					dynamic obj = Newtonsoft.Json.JsonConvert.DeserializeObject(resp);
					String messageFromServer = obj.error.ToString();

					throw new Exception("Error=" + messageFromServer);
				}
				else
					throw new Exception("Error=" + webEx.Message);
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCity-" + Ex.Message);
			}

		}
		public tblTswanaGasCylinder GetTswanaGasCylinder(Int32 pCylinder)
		{
			try
			{
				tblTswanaGasCylinderCrud crud = new tblTswanaGasCylinderCrud();
				crud.Where("CylinderID", General.Operator.Equals, pCylinder);
				recordFound = false;
				tblTswanaGasCylinder cylinder = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return cylinder;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCylinder - " + Ex.Message);
			}
		}
		public tblTswanaGasCentre GetTswanaGasCentre(Int32 pCentreID)
		{
			try
			{
				tblTswanaGasCentreCrud crud = new tblTswanaGasCentreCrud();
				crud.Where("CentreID", General.Operator.Equals, pCentreID);
				recordFound = false;
				tblTswanaGasCentre centre = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return centre;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCentre - " + Ex.Message);
			}
		}
		public tblTswanaGasCity GetTswanaGasCity(Int32 pCityID)
		{
			try
			{
				tblTswanaGasCityCrud crud = new tblTswanaGasCityCrud();
				crud.Where("CityID", General.Operator.Equals, pCityID);
				recordFound = false;
				tblTswanaGasCity city = crud.ReadSingle();
				recordFound = crud.RecordFound;
				return city;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetTswanaGasCity - " + Ex.Message);
			}
		}
	}
}
