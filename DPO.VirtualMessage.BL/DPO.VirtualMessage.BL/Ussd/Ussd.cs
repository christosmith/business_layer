﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualMessage.DAL;
using System.Net;
using System.IO;

namespace DPO.VirtualMessage.BL
{

public partial class VirtualMessageBL
    {

		public Boolean InsertUssdConnection(String pSourceMsisdn, DateTime pPresented, String pNetwork)
		{
			try
			{
				UssdConnectionCrud crud = new UssdConnectionCrud();
				UssdConnection conn = new UssdConnection();
				conn.Network = pNetwork;
				conn.Presented = DateTime.Now;
				conn.SourceMsisdn = pSourceMsisdn;
				crud.Insert(conn);
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("InsertUssdConnection - " + Ex.Message);
			}
		}
        public tblMultiChoiceEntity GetMultiChoiceEntity(String pEntityName)
        {
            try
            {
                recordFound = false;
                tblMultiChoiceEntityCrud crud = new tblMultiChoiceEntityCrud();
                crud.Where("EntityName", General.Operator.Equals, pEntityName);

                tblMultiChoiceEntity search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMultiChoiceEntity - " + Ex.Message);
            }
        }
        public Boolean DeleteUssdRequeue(Int32 pQueueID)
        {
            try
            {
                tblUssdRequeueCrud crud = new tblUssdRequeueCrud();
                crud.Where("QueueID", General.Operator.Equals, pQueueID);

                List<tblUssdRequeue> search = crud.ReadMulti().ToList();
                foreach (tblUssdRequeue del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteUssdRequeue - " + Ex.Message);
            }
        }
        public tblUssdRequeue GetLastUssdRequeue(String pMsisdn)
        {
            try
            {
                recordFound = false;
                tblUssdRequeueCrud crud = new tblUssdRequeueCrud();
                crud.Where("Msisdn", General.Operator.Equals, pMsisdn);

                tblUssdRequeue search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetLastUssdRequeue - " + Ex.Message);
            }
        }
        public Boolean ArchiveUssdRequeue(tblUssdRequeue pRequeue)
        {
            try
            {
                SaveUssdRequeueArchive(pRequeue);
                DeleteUssdRequeue(Convert.ToInt32(pRequeue.QueueID));

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("ArchiveUssdRequeue - " + Ex.Message);
            }
        }
		public Int32 SaveUssdRequeueArchive(tblUssdRequeue pRequeue)
		{
			try
			{
				tblUssdRequeueArchiveCrud crud = new tblUssdRequeueArchiveCrud();
				tblUssdRequeueArchive archive = new tblUssdRequeueArchive();
				archive.CustomerNumber = pRequeue.CustomerNumber;
				archive.DateQueued = pRequeue.DateQueued;
				archive.EntityID = pRequeue.EntityID;
				archive.MethodOfPayment = pRequeue.MethodOfPayment;
				archive.Msisdn = pRequeue.Msisdn;
				archive.MultiChoiceMessage = pRequeue.MultiChoiceMessage;
				archive.PaymentDescription = pRequeue.PaymentDescription;
				archive.ProductCode = pRequeue.ProductCode;
				archive.QueueID = pRequeue.QueueID;
				archive.ReferenceNumber = pRequeue.ReferenceNumber;
				archive.RequeueAttempt = pRequeue.RequeueAttempt;
				archive.SmartCardNumber = pRequeue.SmartCardNumber;
				archive.TransactionAmount = pRequeue.TransactionAmount;
				archive.TransactionNumber = pRequeue.TransactionNumber;
				archive.TranSuccessFull = pRequeue.TranSuccessFull;
				archive.ResponseToCustomer = pRequeue.ResponseToCustomer;
				archive.ResultID = Convert.ToByte(pRequeue.ResultID);
				long id = 0;
				crud.Insert(archive, out id);
				return (Int32)id;
			}
			catch (Exception Ex)
			{
				throw new Exception("SaveUssdRequeueArchive - " + Ex.Message);
			}
		}
        public tblUssdRequeue GetUssdRequeue(Int32 pQueueID)
        {
            try
            {
                recordFound = false;
                tblUssdRequeueCrud crud = new tblUssdRequeueCrud();
                crud.Where("QueueID", General.Operator.Equals, pQueueID);

                tblUssdRequeue search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdRequeue - " + Ex.Message);
            }
        }
        public List<tblUssdRequeue> GetUssdRequeue()
        {
            try
            {
                recordFound = false;
                tblUssdRequeueCrud crud = new tblUssdRequeueCrud();
                List<tblUssdRequeue> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdRequeue - " + Ex.Message);
            }
        }
        public tblUssdRequeue GetUssdRequeue(String pMsisdn, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                tblUssdRequeueCrud crud = new tblUssdRequeueCrud();
                crud.Where("Msisdn", General.Operator.Equals, pMsisdn);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                tblUssdRequeue search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetUssdRequeue - " + Ex.Message);
            }
        }
       public Int32 SaveUssdRequeue(Int32 pQueueID, Int32 pEntityID, String pMsisdn, String pSmartCardNumber, String pCustomerNumber, Int32 pTransactionNumber, String pPaymentDescription, String pProductCode, String pMethodOfPayment, Decimal pTransactionAmount, Boolean pTransactionSuccesful, String pMultiChoiceMessage, String pReferenceNumber, String pResponseToCustomer, Int16 pRequeueAttempt, Int16 pResultID)
        {
            try
            {
                tblUssdRequeueCrud crud = new tblUssdRequeueCrud();
                tblUssdRequeue workRec = new tblUssdRequeue();
                if (pQueueID != 0)
                {
                    crud.Where("QueueID", General.Operator.Equals, pQueueID);
                    workRec = crud.ReadSingle();
                    workRec.EntityID = pEntityID;
                    workRec.Msisdn = pMsisdn;
                    workRec.SmartCardNumber = pSmartCardNumber;
                    workRec.CustomerNumber = pCustomerNumber;
                    workRec.TransactionNumber = pTransactionNumber;
                    workRec.PaymentDescription = pPaymentDescription;
                    workRec.ProductCode = pProductCode;
                    workRec.MethodOfPayment = pMethodOfPayment;
                    workRec.TransactionAmount = pTransactionAmount;
                    workRec.TranSuccessFull = pTransactionSuccesful;
                    workRec.MultiChoiceMessage = pMultiChoiceMessage;
                    workRec.ReferenceNumber = pReferenceNumber;
                    workRec.ResponseToCustomer = pResponseToCustomer;
                    workRec.RequeueAttempt = pRequeueAttempt;
                    workRec.ResultID = (Byte)pResultID;
                    crud.Update(workRec);
                    return pQueueID;
                }
                else
                {
                    workRec.EntityID = pEntityID;
                    workRec.Msisdn = pMsisdn;
                    workRec.SmartCardNumber = pSmartCardNumber;
                    workRec.CustomerNumber = pCustomerNumber;
                    workRec.TransactionNumber = pTransactionNumber;
                    workRec.PaymentDescription = pPaymentDescription;
                    workRec.ProductCode = pProductCode;
                    workRec.MethodOfPayment = pMethodOfPayment;
                    workRec.TransactionAmount = pTransactionAmount;
                    workRec.TranSuccessFull = pTransactionSuccesful;
                    workRec.MultiChoiceMessage = pMultiChoiceMessage;
                    workRec.ReferenceNumber = pReferenceNumber;
                    workRec.ResponseToCustomer = pResponseToCustomer;
                    workRec.RequeueAttempt = pRequeueAttempt;
                    workRec.ResultID = (Byte)pResultID;
                    long ID = 0;
                    crud.Insert(workRec, out ID);
                    return (Int32)ID;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("SaveUssdRequeue - " + Ex.Message);
            }
        }
        public Int32 SaveElectricityPurchases(Decimal pAmount, String pAuthResponse, String pMeterNo, String pMsgID, String pReferenceNumber, String pCardholderName)
        {
            try
            {
                ElectricityPurchasesCrud crud = new ElectricityPurchasesCrud();
                ElectricityPurchases workRec = new ElectricityPurchases();
                workRec.AuthResponse = pAuthResponse;
                workRec.MeterNo = pMeterNo;
                workRec.MsgId = pMsgID;
                workRec.ReferenceNumber = pReferenceNumber;
                workRec.SourceMsisdn = pCardholderName;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("SaveElectricityPurchasesDecimal - " + Ex.Message);
            }
        }
        public Int32 SaveMultiChoicePayment(String pMsisdn, long pTransactionId, String pSmartCardToPay, Decimal pAmount, String pTransactionType, String pBankResponse)
        {
            try
            {
                MultiChoicePaymentsCrud crud = new MultiChoicePaymentsCrud();
                MultiChoicePayments workRec = new MultiChoicePayments();
                workRec.TransactionId = pTransactionId;
                workRec.SmartCardToPay = pSmartCardToPay;
                workRec.Amount = pAmount;
                workRec.TransactionType = pTransactionType;
                workRec.BankResponse = pBankResponse;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("SaveMultiChoicePayment - " + Ex.Message);
            }
        }
		public tblBofinetPayment GetBofinetPayment(Int32 pPaymentID)
        {
            try
            {
                recordFound = false;
                tblBofinetPaymentCrud crud = new tblBofinetPaymentCrud();
                crud.Where("PaymentID", General.Operator.Equals, pPaymentID);

                tblBofinetPayment search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetBofinetPayment - " + Ex.Message);
            }
        }
		public tblBofinetPayment GetBofinetPayment(String pUserID, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                tblBofinetPaymentCrud crud = new tblBofinetPaymentCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                tblBofinetPayment search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetBofinetPayment - " + Ex.Message);
            }
        }
		public Boolean UpdateBofinetPayment(Int32 pPaymentID, String pVoucherProdCode, String pVoucherNo, String pVaReferenceNumber)
        {
            try
            {
                tblBofinetPayment payment = GetBofinetPayment(pPaymentID);
				payment.VoucherProdCode = pVoucherProdCode;
				payment.VoucherNo = pVoucherNo;
				payment.VaReferenceNumber = pVaReferenceNumber;
                tblBofinetPaymentCrud crud = new tblBofinetPaymentCrud();
                crud.Update(payment);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateBofinetPayment - " + Ex.Message);
            }
        }
        public Int32 SaveBofinetPayment(String pMsisdn, String pReferenceNumber, Decimal pAmount, String pTransactionType, String pBankResponse, String pRecipient, String pUserID)
        {
            try
            {
                tblBofinetPaymentCrud crud = new tblBofinetPaymentCrud();
                tblBofinetPayment workRec = new tblBofinetPayment();
                workRec.ReferenceNumber = pReferenceNumber;
                workRec.Amount = pAmount;
                workRec.TransactionType = pTransactionType;
                workRec.BankResponse = pBankResponse;
                workRec.Recipient = pRecipient;
                workRec.UserID = pUserID;
				workRec.Presented = DateTime.Now;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("SaveBofinetPayment - " + Ex.Message);
            }
        }
    }
}
