﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPO.VirtualMessage.BL
{
    public partial class VirtualMessageBL
    {
		private Boolean useMailAsIs = false;
		private Boolean recordFound = false;

        public String ErrorMessage
        {
            get
            {
                return "";
            }
        }
		public Boolean RecordFound 
		{
			get
			{
				return recordFound;
			}
		}
		public Boolean UseMailBodyAsIs
		{
			set
			{
				useMailAsIs = value;
			}
		}


        
		public class VCSResponse
		{
			public String ResponseCode = "";
			public String ResponseMessage = "";
			public String ReturnData = "";
		}

		public class CarmaInformation
		{
			public String FromPoint = "";
			public String ToPoint = "";
			public String TicketType = "";
			public String CoachClass = "";
			public String Title = "";
			public String DepartDate = "";
			public String ReturnDate = "";
			public String PassengerMobileNo = "";
			public String PassengerTitle = "";
			public String PassengerSurname = "";
			public String PassengerInitials = "";
			public String PassengerEmail = "";
			public DateTime DepartArrivalDateTime = DateTime.Now;
			public DateTime DepartBoardingDateTime = DateTime.Now;
			public String DepartServiceNr = "";
			public Decimal DepartPrice = 0;
			public DateTime ReturnArrivalDateTime = DateTime.Now;
			public DateTime ReturnBoardingDateTime = DateTime.Now;
			public String ReturnServiceNr = "";
			public Decimal ReturnPrice = 0;
			public String BookingReferenceNo = "";
			public String DepartBarCode = "";
			public String ReturnBarCode = "";
			public String DepartTicketNo = "";
			public String ReturnTicketNo = "";
			public String VCSResponseCode = "0000";
			public String VCSResponseMessage = "Success";
			public String DepartCoachNumber = "0";
			public String DepartCompartmentNumber = "";
			public String ReturnCoachNumber = "0";
			public String ReturnCompartmentNumber = "";
			public String DepartSeatNo = "0";
			public String ReturnSeatNo = "0";
			public String PassengerID = "";
			public String NextOfKinContact = "";
			public Boolean SendTicketByMail = false;
		}


	}
}
