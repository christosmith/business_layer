﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DPO.VirtualVendor.DAL;

namespace DPO.VirtualVendor.BL
{
public partial class VirtualVendorBL
    {
        public WSTranDetails GetWsTranDetail(String pUserID, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                WSTranDetailsCrud crud = new WSTranDetailsCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                WSTranDetails search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetWsTranDetail - " + Ex.Message);
            }
        }
public Boolean SaveWsTranDetail(ccFormParameters pParam, String pAuthToken, String pBudgetPeriod, String pCustomerID, String pVTUSerID)
        {
            try
            {
                
                WSTranDetails token = new WSTranDetails();
                token.UserID = pParam.UserID;
                token.Amount = pParam.Amount;
                token.MerchantToken = pAuthToken;
                token.BudgetPeriod = pBudgetPeriod;
                token.CustomerID = pCustomerID;
                token.RecurReference = pParam.RecurReference;
                token.MobileEnvironment = pParam.MobileEnvironment;
                token.CardExpiryMm = pParam.CardExpiryMM;
                token.CardExpiryYy = pParam.CardExpiryYY;
                token.CVV = pParam.CCV;
                token.Hash =  pParam.HashValue;
                token.DescrOfGoods = pParam.DescriptionOfGoods;
                token.Presented = System.DateTime.Now;
                token.ReferenceNumber = pParam.ReferenceNumber;
                token.vtUserID = pVTUSerID;
                token.ApprovedURL = pParam.ApprovedURL;
                token.BudgetAllowed = pParam.BudgetAllowed;
                token.CancelURL = pParam.CancelUrl;
                token.CardholderEmail = pParam.CardholderEmail;
                token.CardholderName = pParam.CardholderName;
                token.CellPhoneMessage = pParam.CellMessage;
                token.CellPhoneNumber = pParam.CellNumber;
                token.Currency = pParam.Currency;
                token.DeclinedURL = pParam.DeclinedURL;
                token.DelaySettlement = pParam.DelayedSettlement;
                if (pParam.LastAcceptanceDate.Year != 1900)
                    token.LastAcceptanceDate = pParam.LastAcceptanceDate;
                token.m_1 = pParam.m_1;
                token.m_10 = pParam.m_10;
                token.m_2 = pParam.m_2;
                token.m_3 = pParam.m_3;
                token.m_4 = pParam.m_4;
                token.m_5 = pParam.m_5;
                token.m_6 = pParam.m_6;
                token.m_7 = pParam.m_7;
                token.m_8 = pParam.m_8;
                token.m_9 = pParam.m_9;
                token.MysteryDiscount = pParam.MysteryDiscount;
                token.NextOccurDate = pParam.NextOccurDate;
                token.OccurAmount = pParam.OccurAmount;
                token.OccurCount = pParam.OccurCount;
                token.OccurEmail = pParam.OccurEmailAddress;
                token.OccurFrequency = pParam.OccurFrequency;
                token.OccurUpdate = pParam.OccurUpdate;
                token.PaymentMethod = pParam.PaymentMethod;
                token.Recap = pParam.Recap;
                token.RecapLine1 = pParam.RecapLine1;
                token.RecapLine2 = pParam.RecapLine2;
                token.RecapLine3 = pParam.RecapLine3;
                token.TCConfirm = pParam.TCConfirm;
                token.TCUrl = pParam.TCUrl;
                token.URLsProvided = pParam.URLsProvided;
                token.UserID = pParam.UserID;
                token.MerchMd5String = pParam.MerchMd5String;
				WSTranDetailsCrud crud = new WSTranDetailsCrud();
				crud.Insert(token);
                
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("SaveWsTranDetails - " + Ex.Message);
            }
        }
    }
}
