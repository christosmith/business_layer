﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;
using System.Xml;
using System.Net;

namespace DPO.VirtualVendor.BL
{

    public partial class VirtualVendorBL
	{
		public VCSResponse ccXmlVoucher(String pMerchantID, String pReference, String pProductCode, String pQuantity, String pClientCellNumber, String pClientEmailAddress, String pInstruction, String pInstructionUrl, String pReferUrl)
        {
            VCSResponse result = new VCSResponse();
            try
            {
                XmlDocument xmlVoucherDoc = new XmlDocument();
                XmlElement xmlDocElement;

                //Root Element
                XmlElement xmlAuthorisationDocRootElement = xmlVoucherDoc.CreateElement("AuthorisationRequest");
                xmlVoucherDoc.AppendChild(xmlAuthorisationDocRootElement);

                xmlDocElement = xmlVoucherDoc.CreateElement("MerchantId");
                xmlDocElement.InnerText = pMerchantID;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlVoucherDoc.CreateElement("Reference");
                xmlDocElement.InnerText = pReference;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlVoucherDoc.CreateElement("ProdCode");
                xmlDocElement.InnerText = pProductCode;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlVoucherDoc.CreateElement("Quantity");
                xmlDocElement.InnerText = pQuantity;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlVoucherDoc.CreateElement("ClientCellNumber");
                xmlDocElement.InnerText = pClientCellNumber;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlVoucherDoc.CreateElement("ClientEmailAddress");
                xmlDocElement.InnerText = pClientEmailAddress;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlVoucherDoc.CreateElement("Instruction");
                xmlDocElement.InnerText = pInstruction;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlVoucherDoc.CreateElement("InstructionUrl");
                xmlDocElement.InnerText = pInstructionUrl;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                //System.Net.HttpWebRequest VcsRequest = (HttpWebRequest)HttpWebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetAuthorisationUrl"]);
                System.Net.HttpWebRequest VcsRequest = (HttpWebRequest)HttpWebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["vOnlineUrl"].ToString() + "/vvonline/ccxmlvoucher.asp");

                VcsRequest.Method = "POST";
                ((HttpWebRequest)VcsRequest).UserAgent = "VcsAuthCode";
				VcsRequest.Referer = pReferUrl;


                String postData = "xmlmessage=" + System.Web.HttpUtility.UrlEncode(xmlVoucherDoc.OuterXml.ToString());
                ASCIIEncoding encoding = new ASCIIEncoding();
                Byte[] byteArray = encoding.GetBytes(postData);

                //byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                VcsRequest.ContentType = "application/x-www-form-urlencoded";
                VcsRequest.ContentLength = byteArray.Length;

                System.IO.Stream dataStream = VcsRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                HttpWebResponse VcsResponse = (HttpWebResponse)VcsRequest.GetResponse();
                System.IO.Stream responseStream = VcsResponse.GetResponseStream();
                System.IO.StreamReader streamIn = new System.IO.StreamReader(responseStream, System.Text.Encoding.UTF8);
                String voucherResponse = streamIn.ReadToEnd();
                streamIn.Close();

                XmlDocument docCcXmlVoucherResponse = new XmlDocument();
                docCcXmlVoucherResponse.LoadXml(voucherResponse);
                String responseResult = "";
                if (docCcXmlVoucherResponse.GetElementsByTagName("Response").Count > 0 )
                {
                    if (docCcXmlVoucherResponse.GetElementsByTagName("Response")[0].ChildNodes.Count > 0)
                    {
                        responseResult = docCcXmlVoucherResponse.GetElementsByTagName("Response")[0].ChildNodes[0].Value;
                    }
                    else
                    {
                        result.VCSResponseCode = "FAILURE";
                        result.VCSResponseMessage = "Voucher response missing";
                        return result;
                    }
                }
                else
                {
                    result.VCSResponseCode = "FAILURE";
                    result.VCSResponseMessage = "Voucher response tag missing";
                    return result;
                }

                String voucherMessage = "";
                if (docCcXmlVoucherResponse.GetElementsByTagName("ResponseMessage").Count > 0 )
                {
                    if (docCcXmlVoucherResponse.GetElementsByTagName("ResponseMessage")[0].ChildNodes.Count > 0)
                    {
                        voucherMessage= docCcXmlVoucherResponse.GetElementsByTagName("ResponseMessage")[0].ChildNodes[0].Value;
                    }
                    else
                    {
                        result.VCSResponseCode = "FAILURE";
                        result.VCSResponseMessage = "Voucher response message missing";
                        return result;
                    }
                }
                else
                {
                    result.VCSResponseCode = "FAILURE";
                    result.VCSResponseMessage = "Voucher response message tag missing";
                    return result;
                }

                if (responseResult == "FAILURE")
                    result.VCSResponseCode = "0001";
                else
                    result.VCSResponseCode = "0000";

                result.VCSResponseMessage = voucherMessage;
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("ccXmlVoucher-" + Ex.Message);
            }
        }
		public VCSResponse vaXmlCredit(String pReference, String pCardNumber, String pAmount, String pDescription, String pOpenField1, String pOpenField2, String pOpenField3, String pOpenField4, String pOpenField5)
        {
            VCSResponse result = new VCSResponse();
            try
            {
                XmlDocument xmlRequestDoc = new XmlDocument();
                XmlDeclaration xmlRequestDocDeclaration = xmlRequestDoc.CreateXmlDeclaration("1.0", "utf-8", "no");
                xmlRequestDoc.AppendChild(xmlRequestDocDeclaration);
                XmlElement xmlRequestDocRootElement = xmlRequestDoc.CreateElement("CreditRequest");
                xmlRequestDoc.AppendChild(xmlRequestDocRootElement);
                XmlElement xmlDocElement;
                xmlDocElement = xmlRequestDoc.CreateElement("Reference");
                xmlDocElement.InnerText = pReference;
                xmlRequestDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlRequestDoc.CreateElement("CardNumber");
                xmlDocElement.InnerText =pCardNumber;
                xmlRequestDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlRequestDoc.CreateElement("Amount");
                //xmlDocElement.InnerText = (Convert.ToDecimal(pAmount) * -1).ToString();
                xmlDocElement.InnerText = pAmount;
                xmlRequestDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlRequestDoc.CreateElement("Description");
                xmlDocElement.InnerText = pDescription;
                xmlRequestDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlRequestDoc.CreateElement("Uncleared");
                xmlDocElement.InnerText = "N";
                xmlRequestDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlRequestDoc.CreateElement("ClearDate");
                xmlDocElement.InnerText = System.DateTime.Now.ToString();
                xmlRequestDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlRequestDoc.CreateElement("VaLoginID");
                xmlDocElement.InnerText = "vasettlecallback";
                xmlRequestDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlRequestDoc.CreateElement("VaLoginPwd");
                xmlDocElement.InnerText = "m1VaCallback3110";
                xmlRequestDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlRequestDoc.CreateElement("TranCode");
                xmlDocElement.InnerText = "108";
                xmlRequestDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlRequestDoc.CreateElement("TranOpenField1");
                xmlDocElement.InnerText = pOpenField1;
                xmlRequestDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlRequestDoc.CreateElement("TranOpenField2");
                xmlDocElement.InnerText = pOpenField2;
                xmlRequestDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlRequestDoc.CreateElement("TranOpenField3");
                xmlDocElement.InnerText = pOpenField3;
                xmlRequestDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlRequestDoc.CreateElement("TranOpenField4");
                xmlDocElement.InnerText = pOpenField4;
                xmlRequestDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlRequestDoc.CreateElement("TranOpenField5");
                xmlDocElement.InnerText = pOpenField5;
                xmlRequestDocRootElement.AppendChild(xmlDocElement);

                //Post to the vaxmlCredit page
                System.Net.HttpWebRequest VcsRequest = (HttpWebRequest)HttpWebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["vAccountUrl"].ToString() + "/h2h/vaxmlcredit.asp");

                VcsRequest.Method = "POST";
                ((HttpWebRequest)VcsRequest).UserAgent = "VCS.VirtualVendor.BL";


                String postData = "xmlmessage=" + System.Web.HttpUtility.UrlEncode(xmlRequestDoc.OuterXml.ToString());
                ASCIIEncoding encoding = new ASCIIEncoding();
                Byte[] byteArray = encoding.GetBytes(postData);

                //byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                VcsRequest.ContentType = "application/x-www-form-urlencoded";
                VcsRequest.ContentLength = byteArray.Length;

                System.IO.Stream dataStream = VcsRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                HttpWebResponse VcsResponse = (HttpWebResponse)VcsRequest.GetResponse();
                System.IO.Stream responseStream = VcsResponse.GetResponseStream();
                System.IO.StreamReader streamIn = new System.IO.StreamReader(responseStream, System.Text.Encoding.UTF8);
                String strXmlCreditWebResponse = streamIn.ReadToEnd();
                streamIn.Close();

                String creditResponse = "";
                XmlDocument xmlCreditResponse = new XmlDocument();
                xmlCreditResponse.LoadXml(strXmlCreditWebResponse);

                if (xmlCreditResponse.GetElementsByTagName("Response").Count > 0)
                {
                    if (xmlCreditResponse.GetElementsByTagName("Response")[0].ChildNodes.Count > 0)
                    {
                        creditResponse = xmlCreditResponse.GetElementsByTagName("Response")[0].ChildNodes[0].Value;
                        if (creditResponse != "Successful")
                        {
                            if (!creditResponse.Contains("Reference"))
                            {
                                creditResponse += " Card Number: " + pCardNumber;
                            }
                        }
                    }
                    else
                    {
                        result.VCSResponseCode = "0011";
                        result.VCSResponseMessage = "Response Missing";
                        return result;
                    }
                }
                else
                {
                    result.VCSResponseCode = "0011";
                    result.VCSResponseMessage = "Response Tag Missing";
                    return result;
                }

                if (creditResponse != "Successful")
                {
                    result.VCSResponseCode = "0011";
                    result.VCSResponseMessage = creditResponse;
                    return result;
                }

                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("VaXmlCredit-" + Ex.Message);
            }
        }
		public String Authorisation(String pUserID, String pReferenceNumber,  String pDescription, String pAmount, String pCardNumber, String pCardholderName, String pExpiryDate, String pCvc)
        {
            try
            {
                XmlDocument xmlAuthorisationDoc = new XmlDocument();
                XmlElement xmlDocElement;

                //Root Element
                XmlElement xmlAuthorisationDocRootElement = xmlAuthorisationDoc.CreateElement("AuthorisationRequest");
                xmlAuthorisationDoc.AppendChild(xmlAuthorisationDocRootElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("UserId");
                xmlDocElement.InnerText = pUserID;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("Reference");
                xmlDocElement.InnerText = pReferenceNumber;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("Description");
                xmlDocElement.InnerText = pDescription;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("Amount");
                xmlDocElement.InnerText = pAmount;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("CardNumber");
                xmlDocElement.InnerText = pCardNumber;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("CardholderName");
                xmlDocElement.InnerText = pCardholderName;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("ExpiryMonth");
                xmlDocElement.InnerText = pExpiryDate.Substring(0, 2);
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("ExpiryYear");
                xmlDocElement.InnerText = pExpiryDate.Substring(2, 2);
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                xmlDocElement = xmlAuthorisationDoc.CreateElement("CardValidationCode");
                xmlDocElement.InnerText = pCvc;
                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                System.Net.HttpWebRequest VcsRequest = (HttpWebRequest)HttpWebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["GetAuthorisationUrl"]);

                
                VcsRequest.Method = "POST";
                ((HttpWebRequest)VcsRequest).UserAgent = "VcsAuthCode";


                String postData = "xmlmessage=" + System.Web.HttpUtility.UrlEncode(xmlAuthorisationDoc.OuterXml.ToString());
                ASCIIEncoding encoding = new ASCIIEncoding();
                Byte[] byteArray = encoding.GetBytes(postData);

                //byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                VcsRequest.ContentType = "application/x-www-form-urlencoded";
                VcsRequest.ContentLength = byteArray.Length;

                System.IO.Stream dataStream = VcsRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                HttpWebResponse VcsResponse = (HttpWebResponse)VcsRequest.GetResponse();
                System.IO.Stream responseStream = VcsResponse.GetResponseStream();
                System.IO.StreamReader streamIn = new System.IO.StreamReader(responseStream, System.Text.Encoding.UTF8);
                String strCcxmlauthResponse = streamIn.ReadToEnd();
					streamIn.Close();
                return strCcxmlauthResponse;

            }
            catch (Exception Ex)
            {
                throw new Exception("Authorisation - " + Ex.Message);
            }
        }
		public VCSResponse Authorisation(TransactionsDue pTranDue, String pAuthUrl)
        {
            VCSResponse response = new VCSResponse();
            try
            {
                
                String tranType = pTranDue.TranType == "" ? "auth200" : pTranDue.TranType;

                AuthTranType transactionType;

                switch (tranType.ToLower())
                {
                    case "refund": transactionType = AuthTranType.Refund; break;
                    case "settleoriginal": transactionType = AuthTranType.SettleOriginal; break;
                    case "refundoriginal": transactionType = AuthTranType.RefundOriginal; break;
                    case "settle220": transactionType = AuthTranType.Settle220; break;
                    case "auth100": transactionType = AuthTranType.Auth100; break;
                    case "auth200": transactionType = AuthTranType.Auth200; break;
                    default:
                        response.VCSResponseCode = "0002";
                        response.VCSResponseMessage = tranType + " was not recognized as a valid authorization transaction type.";
                        return response;
                        break;
                }

                String userID = pTranDue.UserId;
                MerchantCurrency merchantCurrency = GetMerchantCurrency(userID)[0];
                String descOfGoods = pTranDue.DescrOfGoods;
                String amount = pTranDue.Amount.ToString();
                String cardNumber = pTranDue.CardNumber;
                String cardholderName = pTranDue.CardholderName;
                String cardholderEmail = "";
                if (transactionType == AuthTranType.Auth200 || transactionType == AuthTranType.Settle220)
                    cardholderEmail = pTranDue.CardholderEmail;

                Validation validateMail = ValidateEmailAddress(cardholderEmail);
                if (validateMail.VCSResponseCode != "0000")
                    cardholderEmail = "";

                String expiryDateMM = pTranDue.ExpiryMm;
                String expiryDateYY = pTranDue.ExpiryYy;

                if (expiryDateMM.Trim() == "" || expiryDateYY.Trim() == "")
                {
                    expiryDateMM = DateTime.Now.Month.ToString().PadLeft(2, '0');
                    expiryDateYY = DateTime.Now.Year.ToString().Substring(2, 2);
                }
                String cvc = pTranDue.Cvc;
                String budgerPeriod = pTranDue.BudgetPeriod;
                String currency = merchantCurrency.NumericCurrencyCode;
                String cardPresent = "N";
                String manualAuthCode = pTranDue.AuthCode;
                String authCode = pTranDue.AuthCode;
                String eci = "";
                String tranTypeECI = "";

                if (Convert.ToInt32(pTranDue.RecurrenceId) > 0)
                {
                    eci = "3";
                    tranTypeECI = "3";
                }
                else
                {
                    eci = "3";
                    tranTypeECI = "3";
                }
                String referenceNumber = pTranDue.ReferenceNumber;
                referenceNumber = StripSpecialCharacters(referenceNumber);
                descOfGoods = StripSpecialCharacters(descOfGoods);
                cardholderName = StripSpecialCharacters(cardholderName);
                cardholderEmail = StripSpecialCharacters(cardholderEmail);

                XmlDocument xmlAuthorisationDoc = new XmlDocument();
                XmlElement xmlDocElement;

                if (transactionType == AuthTranType.Refund)
                {
                    Transactions transaction = GetTransactions(userID, referenceNumber);
                    if (transaction != null)
                    {
                        DateTime delaySettlementDate = Convert.ToDateTime("1900-01-01");
                        StoredProcedure.vcs_StoreTransaction(userID, referenceNumber, cardNumber, "", cvc, cardholderName, expiryDateMM, expiryDateYY, Convert.ToDecimal(amount), budgerPeriod, descOfGoods, "VCSA07000000APPROVED", DateTime.Now, DateTime.Now, cardholderEmail, "", delaySettlementDate, "00", "00000", DateTime.Now.ToString("MMdd"), "", "710", "", "", "VCSA", "0200", "0200", "", "", "", "", "", "", "", "", "", "", "", 0, "", "", "", "", "", 0, "", "", "", "", "", "", "", "", "", "", "", "");
                        transaction = GetTransactions(userID, referenceNumber);
                        transaction.FeesReferenceNumber = "";
                        transaction.CheckRefundAmount = "";
						TransactionsCrud crud = new TransactionsCrud();
						crud.Update(transaction);
                    }
                    transactionType = AuthTranType.RefundOriginal;
                }


                String authUrl = pAuthUrl + "/vvonline/ccxmlauth.asp";
                if (transactionType == AuthTranType.SettleOriginal)
                    authUrl = pAuthUrl + "/vvonline/ccxmlsettle.asp";

                //Root Element

                
                switch (transactionType)
                {
                    case AuthTranType.SettleOriginal:
                        XmlElement xmlSettlementDocRootElement = xmlAuthorisationDoc.CreateElement("SettlementRequest");
                        xmlAuthorisationDoc.AppendChild(xmlSettlementDocRootElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("UserId");
                        xmlDocElement.InnerText = userID;
                        xmlSettlementDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("Reference");
                        xmlDocElement.InnerText = referenceNumber;
                        xmlSettlementDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("SettlementDate");
                        xmlDocElement.InnerText = DateTime.Now.ToString("yyyy/MM/dd");
                        xmlSettlementDocRootElement.AppendChild(xmlDocElement);
                        break;
                    case AuthTranType.RefundOriginal:
                        XmlElement xmlRefundDocRootElement = xmlAuthorisationDoc.CreateElement("RefundRequest");
                        xmlAuthorisationDoc.AppendChild(xmlRefundDocRootElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("UserId");
                        xmlDocElement.InnerText = userID;
                        xmlRefundDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("Reference");
                        xmlDocElement.InnerText = referenceNumber;
                        xmlRefundDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("Description");
                        xmlDocElement.InnerText = descOfGoods;
                        xmlRefundDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("Amount");
                        xmlDocElement.InnerText = amount;
                        xmlRefundDocRootElement.AppendChild(xmlDocElement);
                        break;
                    default:
                        XmlElement xmlAuthorisationDocRootElement = xmlAuthorisationDoc.CreateElement("AuthorisationRequest");
                        xmlAuthorisationDoc.AppendChild(xmlAuthorisationDocRootElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("UserId");
                        xmlDocElement.InnerText = userID;
                        xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("Reference");
                        xmlDocElement.InnerText = referenceNumber;
                        xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("Description");
                        xmlDocElement.InnerText = descOfGoods;
                        xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("Amount");
                        xmlDocElement.InnerText = amount;
                        xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("CardNumber");
                        xmlDocElement.InnerText = cardNumber;
                        xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("CardholderName");
                        xmlDocElement.InnerText = cardholderName;
                        xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("CardholderEmail");
                        xmlDocElement.InnerText = cardholderEmail;
                        xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("ExpiryMonth");
                        xmlDocElement.InnerText = expiryDateMM;
                        xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("ExpiryYear");
                        xmlDocElement.InnerText = expiryDateYY;
                        xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("CardValidationCode");
                        xmlDocElement.InnerText = cvc;
                        xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("BudgetPeriod");
                        xmlDocElement.InnerText = budgerPeriod;
                        xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("Currency");
                        xmlDocElement.InnerText = currency;
                        xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("CardPresent");
                        xmlDocElement.InnerText = cardPresent;
                        xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);


                        if (transactionType == AuthTranType.Settle220)
                        {
                            if (authCode == "")
                            {
                                xmlDocElement = xmlAuthorisationDoc.CreateElement("SettleOnly");
                                xmlDocElement.InnerText = "Y";
                                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);
                            }
                            else
                            {
                                xmlDocElement = xmlAuthorisationDoc.CreateElement("ManualAuthCode");
                                xmlDocElement.InnerText = authCode;
                                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);
                            }
                        }

                        if (transactionType == AuthTranType.Auth100)
                        {
                            xmlDocElement = xmlAuthorisationDoc.CreateElement("DelaySettlement");
                            xmlDocElement.InnerText = "Y";
                            xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);
                        }

                        xmlDocElement = xmlAuthorisationDoc.CreateElement("TransactionType");
                        xmlDocElement.InnerText = tranTypeECI;
                        xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);

                        if (pTranDue.m_pars.Trim() != "")
                        {
                            List<mPars> pars = SplitMPars(pTranDue.m_pars);
                            foreach (mPars par in pars)
                            {
                                xmlDocElement = xmlAuthorisationDoc.CreateElement(par.mParName);
                                xmlDocElement.InnerText = par.mParValue;
                                xmlAuthorisationDocRootElement.AppendChild(xmlDocElement);
                            }
                        }
                        break;
                }

                System.Net.HttpWebRequest VcsRequest = (HttpWebRequest)HttpWebRequest.Create(authUrl);

                VcsRequest.Method = "POST";
                ((HttpWebRequest)VcsRequest).UserAgent = "VCS.VirtualVendor.BulkAuth";


                String postData = "xmlmessage=" + System.Web.HttpUtility.UrlEncode(xmlAuthorisationDoc.OuterXml.ToString());
                ASCIIEncoding encoding = new ASCIIEncoding();
                Byte[] byteArray = encoding.GetBytes(postData);

                //byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                VcsRequest.ContentType = "application/x-www-form-urlencoded";
                VcsRequest.ContentLength = byteArray.Length;

                System.IO.Stream dataStream = VcsRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                HttpWebResponse VcsResponse = (HttpWebResponse)VcsRequest.GetResponse();
                System.IO.Stream responseStream = VcsResponse.GetResponseStream();
                System.IO.StreamReader streamIn = new System.IO.StreamReader(responseStream, System.Text.Encoding.UTF8);
                String strCcxmlauthResponse = streamIn.ReadToEnd();
                streamIn.Close();

                response.VCSResponseCode = "0000";
                response.VCSResponseMessage = strCcxmlauthResponse;
                    return response;
            }
            catch (Exception Ex)
            {
                response.VCSResponseCode = "9999";
                response.VCSResponseMessage = Ex.Message;
                return response;
            }
        }
    }
}
