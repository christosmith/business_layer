﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;
using System.Net;
using System.Data.SqlClient;
using System.Configuration;

namespace DPO.VirtualVendor.BL
{
public partial class VirtualVendorBL
	{
		public CallbacksMerchantRetryCount GetCallbacksMerchantRetryCount(String pUserID)
        {
            try
            {
                recordFound = false;
                CallbacksMerchantRetryCountCrud crud = new CallbacksMerchantRetryCountCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);

                CallbacksMerchantRetryCount search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCallbacksMerchantRetryCount - " + Ex.Message);
            }
        }
		public Boolean DeleteCallbacksInvalidNotification(String pUserID)
        {
            try
            {
                CallbacksInvalidNotification search = GetCallbacksInvalidNotification(pUserID);
                CallbacksInvalidNotificationCrud crud = new CallbacksInvalidNotificationCrud();
                crud.Delete(search);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteCallbacksInvalidNotification - " + Ex.Message);
            }
        }

        public List<Callbacks> GetCallbackFailed(String pUserID)
        {
            try
            {
                CallbacksCrud crud = new CallbacksCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("Delivered", General.Operator.Equals, "N");
                crud.And("ErrorYN", General.Operator.Equals, "Y");
                crud.And("DeliverAttempts", General.Operator.GreaterThanOrEqual, 5);
                recordFound = false;
                List<Callbacks> callbacks = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return callbacks;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetCallbackFailed - " + Ex.Message);
            }
        }

        public Boolean CallbackRequeue(Decimal pAutoID)
        {
            try
            {
                CallbacksCrud crud = new CallbacksCrud();
                Callbacks callback =    GetCallbacks(pAutoID);
                callback.DeliverAttempts = "0";
                callback.Delivered = "N";
                callback.DeliveryDate = null;
                callback.ErrorDescription = "";
                callback.ErrorEmailAddress = "";
                callback.ErrorEmailDate = null;
                callback.ErrorEmailsDone = "N";
                callback.ErrorEmailSent = "N";
                callback.ErrorYN = "N";
                callback.ReportError = false;
                crud.Update(callback);

                return true;
                
            }
            catch (Exception Ex)
            {
                throw new Exception("CallbackRequeue - " + Ex.Message);
            }
        }
        public List<VCSResponse> CallbackRequeue(String pUserID)
        {
            List<VCSResponse> response = new List<VCSResponse>();
            try
            {
                
                List<Callbacks> Callbacks = GetCallbackFailed(pUserID);
                VCSResponse res = new VCSResponse();
                if (Callbacks.Count == 0)
                {
                    res = new VCSResponse();
                    res.VCSResponseCode = "00";
                    res.VCSResponseMessage = "Success";
                    res.ReturnValue = "Nothing to do";
                    response.Add(res);
                    return response;
                }
                foreach (Callbacks call in Callbacks)
                {
                    res = new VCSResponse();
                    CallbackRequeue(Convert.ToDecimal(call.AutoId));
                    res.VCSResponseCode = "00";
                    res.VCSResponseMessage = "Success";
                    res.ReturnValue = call.AutoId.ToString();
                    response.Add(res);
                }

                

                return response;
            }
            catch (Exception Ex)
            {
                throw new Exception("CallbackRequeue - " + Ex.Message);
            }
        }
        public List<CallbacksInvalidNotification> GetCallbacksInvalidNotification()
        {
            try
            {
                recordFound = false;
                CallbacksInvalidNotificationCrud crud = new CallbacksInvalidNotificationCrud();
                List<CallbacksInvalidNotification> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCallbacksInvalidNotification - " + Ex.Message);
            }
        }
        public CallbacksInvalidNotification GetCallbacksInvalidNotification(String pUserID)
        {
            try
            {
                recordFound = false;
                CallbacksInvalidNotificationCrud crud = new CallbacksInvalidNotificationCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);

                CallbacksInvalidNotification search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCallbacksInvalidNotification - " + Ex.Message);
            }
        }
        public Boolean DeleteCallbacksInvalid(String pUserID)
        {
            try
            {
                CallbacksInvalid invalid = GetCallbackInvalid(pUserID);
                CallbacksInvalidCrud crud = new CallbacksInvalidCrud();
                crud.Delete(invalid);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteCallbacksInvalid - " + Ex.Message);
            }
        }
        public Boolean DeleteCallbacksInvalid(Int32 pInvalidID)
        {
            try
            {
                CallbacksInvalidCrud crud = new CallbacksInvalidCrud();
                crud.Where("InvalidID", General.Operator.Equals, pInvalidID);

                List<CallbacksInvalid> search = crud.ReadMulti().ToList();
                foreach (CallbacksInvalid del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteCallbacksInvalid - " + Ex.Message);
            }
        }
        public Boolean UpdateCallbacksInvalidNotification(String pUserID)
        {
            try
            {
             
                CallbacksInvalidNotification notification = GetCallbacksInvalidNotification(pUserID);
				CallbacksInvalidNotificationCrud crud = new CallbacksInvalidNotificationCrud();
                if (notification == null)
                {

                    notification = new CallbacksInvalidNotification();
                    notification.LastNotification = DateTime.Now;
                    notification.NotificationCount = 1;
                    notification.UserID = pUserID;
					crud.Insert(notification);
                }
                else
                {
                    notification.LastNotification = DateTime.Now;
                    notification.NotificationCount = Convert.ToInt32(notification.NotificationCount) + 1;
                    notification.UserID = pUserID;
					crud.Update(notification);
                }

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateCallbacksInvalidNotification - " + Ex.Message);
            }
        }
        public Boolean UpdateCallbackInvalid(Int32 pInvalid)
        {
            try
            {
                CallbacksInvalid invalid = GetCallbackInvalid(pInvalid);
                invalid.NotificationSent = DateTime.Now;
				CallbacksInvalidCrud crud = new CallbacksInvalidCrud();
				crud.Update(invalid);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateCallbackInvalid - " + Ex.Message);
            }
        }
        public List<spGetCallbackInvalid> GetCallbackInvalid()
        {
            try
            {
                return StoredProcedure.spGetCallbackInvalid();
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCallbackInvalid - " + Ex.Message);
            }
        }
		public void SendCallbacksInvalidMail()
        {
            
            Boolean bAttachResponse  = false;
            byte[] fileBytes = new byte[0];
            Boolean accepted = false;
            SqlConnection openConn = new SqlConnection(ConfigurationManager.ConnectionStrings["vMessage_ConnectionString"].ConnectionString);
            openConn.Open();
            SqlCommand dbComd = new SqlCommand("", openConn);

            List<spGetCallbackInvalid> invalids = StoredProcedure.spGetCallbackInvalid();

            foreach (spGetCallbackInvalid invalid in invalids)
            {
                String userID = invalid.UserID;
                String url = invalid.Url;
                String errorMessage = invalid.ErrorMessage;
                String date1 = Convert.ToDateTime(invalid.Date1).ToString("yyyy-MM-dd");
                String date2 = Convert.ToDateTime(invalid.Date2).ToString("yyyy-MM-dd");
                String date3 = Convert.ToDateTime(invalid.Date3).ToString("yyyy-MM-dd");
                String date4 = Convert.ToDateTime(invalid.Date4).ToString("yyyy-MM-dd");
                String date5 = Convert.ToDateTime(invalid.Date5).ToString("yyyy-MM-dd");
                String date6 = Convert.ToDateTime(invalid.Date6).ToString("yyyy-MM-dd");
                String date7 = Convert.ToDateTime(invalid.Date7).ToString("yyyy-MM-dd");
                String day1 = invalid.Day1.ToString();
                String day2 = invalid.Day2.ToString();
                String day3 = invalid.Day3.ToString();
                String day4 = invalid.Day4.ToString();
                String day5 = invalid.Day5.ToString();
                String day6 = invalid.Day6.ToString();
                String day7 = invalid.Day7.ToString();
                Int32 invalidID = Convert.ToInt32(invalid.InvalidID);

                String mailBody = "<html>\n";
                mailBody += "<body bgcolor=white style='font-size:8pt;font-family:verdana'>\n";
               

                bAttachResponse = false; accepted = false;
                switch (invalid.ServerStatus)
                {
                    case "Internal Server Error":
                    case "Unable to connect":
                        mailBody += "<br/>\n";
                        mailBody += "<br/>\n";
                        mailBody += "Good Day,\n";
                        mailBody += "<br/>\n";
                        mailBody += "<br/>\n";
                        mailBody += "The callbacks process is unable to contact the url you have specified in order to deliver your callback detail.\n";
                        mailBody += "<br/>\n";
                        mailBody += "Please correct the error to ensure delivery of your callback.\n";
                        mailBody += "<br/>\n";
                        mailBody += "<br/>\n";
                        break;
                    case "Found":
                    case "OK":
                        bAttachResponse = true;
                        if (invalid.ServerStatus == "OK")
                        {
                            mailBody += "<br/>\n";
                            mailBody += "<br/>\n";
                            mailBody += "Good Day,\n";
                            mailBody += "<br/>\n";
                            mailBody += "<br/>\n";
                            mailBody += "The callbacks process expects the following response when doing a " + invalid.Method.ToLower() + " to your url: <b>&lt;CallBackResponse&gt;Accepted&lt;/CallBackResponse&gt;</b>\n";
                            mailBody += "<br/>\n";
                            mailBody += "The attachment called \"Callback Response.txt\" shows the response from your url.\n";
                            mailBody += "<br/>\n";
                            mailBody += "Please change your response to the expected response.\n";
                            mailBody += "<br/>\n";
                            mailBody += "<br/>\n";
                        }
                        if (invalid.ServerStatus.ToLower() == "found")
                        {
                            mailBody += "<br/>\n";
                            mailBody += "<br/>\n";
                            mailBody += "Good Day,\n";
                            mailBody += "<br/>\n";
                            mailBody += "<br/>\n";
                            mailBody += "The callback url you have provided redirects to another url. This is not allowed by the callback process.\n";
                            mailBody += "<br/>\n";
                            mailBody += "Please change to url to not redirect.\n";


                            mailBody += "<br/>\n";
                            mailBody += "<br/>\n";
                        }

                        break;
                    default:
                        mailBody += "<br/>\n";
                        mailBody += "<br/>\n";
                        mailBody += "Good Day,\n";
                        mailBody += "<br/>\n";
                        mailBody += "<br/>\n";
                        mailBody += "We are unable to deliver your callback to the url you have specified.\n";
                        mailBody += "<br/>\n";
                        mailBody += "Please correct the error to ensure delivery of your callback.\n";
                        mailBody += "<br/>\n";
                        mailBody += "<br/>\n";
                        break;
                }

                mailBody += "<table class='small' style=\"border-collapse:collapse\">\n";
                mailBody += "<tr><td colspan='2' style='font-weight:bold;font-size:14pt;font-family:verdana;width:500px;border:1px solid #778899;text-align:center;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Invalid Callback Response</td></tr>\n";
                mailBody += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>User ID</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + userID + "</td></tr>\n";
                mailBody += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Callback Url</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + url + "</td></tr>\n";
                mailBody += "<tr><td style='font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Error Message</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + errorMessage + "</td></tr>\n";
                mailBody += "</table>\n";
                mailBody += "<br/>";
                mailBody += "<table class='small' style=\"border-collapse:collapse\">\n";
                mailBody += "<tr><td colspan='2' style='font-weight:bold;font-size:14pt;font-family:verdana;width:250px;border:1px solid #778899;text-align:center;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>Number of calls</td></tr>\n";
                mailBody += "<tr><td style='width:120px;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>" + date1 + "</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px;text-align:right'>" + day1 + "</td></tr>\n";
                mailBody += "<tr><td style='width:120px;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>" + date2 + "</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px;text-align:right'>" + day2 + "</td></tr>\n";
                mailBody += "<tr><td style='width:120px;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>" + date3 + "</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px;text-align:right'>" + day3 + "</td></tr>\n";
                mailBody += "<tr><td style='width:120px;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>" + date4 + "</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px;text-align:right'>" + day4 + "</td></tr>\n";
                mailBody += "<tr><td style='width:120px;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>" + date5 + "</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px;text-align:right'>" + day5 + "</td></tr>\n";
                mailBody += "<tr><td style='width:120px;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>" + date6 + "</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px;text-align:right'>" + day6 + "</td></tr>\n";
                mailBody += "<tr><td style='width:120px;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>" + date7 + "</td><td style='font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px;text-align:right'>" + day7 + "</td></tr>\n";
                mailBody += "</table>\n";

                if (!accepted)
                {


                    List<String> addresses = new List<string>();
                    List<vContactDetail> contactDetails = GetContactDetail(invalid.UserID, 7, 1);
                    foreach (vContactDetail contactDetail in contactDetails)
                        addresses.Add(contactDetail.ContactDetail);

                    if (contactDetails.Count == 0)
                    {
                        contactDetails = GetContactDetail(invalid.UserID, 1, 1);
                        foreach (vContactDetail contactDetail in contactDetails)
                            addresses.Add(contactDetail.ContactDetail);
                    }

                    for (Int32 addressCount = 0; addressCount < addresses.Count; addressCount++)
                    {
                        String comd = "Insert Into SendMsg\n";
                        comd += "(Recipient, Send_Method, Msg_Subject, Msg, Date_Inserted, Retry_Count, HTML_Format, DueDateTime, SystemGenerated, QueuedByApplication)\n";
                        comd += "Values\n";
                        comd += "('" + addresses[addressCount] + "', 1, 'Invalid Callback Response', '" + mailBody.Replace("'", "''") + "', GetDate(), 0, 1, GetDate(), 1, 'VCS.VirtualVendor.CallbacksInvalidRemimder')";
                        dbComd.CommandText = comd;
                        dbComd.ExecuteNonQuery();
					}
                    CallbacksInvalid currRec = GetCallbackInvalid(invalidID);
                    currRec.NotificationSent = DateTime.Now;
					CallbacksInvalidCrud crud = new CallbacksInvalidCrud();
					crud.Update(currRec);

					CallbacksInvalidNotificationCrud crudNotify = new CallbacksInvalidNotificationCrud();
					crudNotify.Where("UserID", General.Operator.Equals, invalid.UserID);
					List<CallbacksInvalidNotification> notifySearch = crudNotify.ReadMulti().ToList();

                    if (notifySearch.Count == 0)
                    {
                        notifySearch[0].LastNotification = DateTime.Now;
                        notifySearch[0].NotificationCount = 1;
						crudNotify.Insert(notifySearch[0]);
                    }
                    else
                    {
                        CallbacksInvalidNotification thisNotification = notifySearch[0];
                        thisNotification.LastNotification = DateTime.Now;
                        thisNotification.NotificationCount = Convert.ToInt32(thisNotification.NotificationCount) + 1;
						crudNotify.Update(notifySearch[0]);
                    }


                }
                else
                {
					CallbacksInvalid inv = GetCallbackInvalid(invalidID);
					CallbacksInvalidCrud invCrud = new CallbacksInvalidCrud();
					invCrud.Delete(inv);
                }
            }

            #region Report to support staff
            String htmlString = "<table width='1000' style='table-layout: fixed;border-collapse:collapse'>";
            htmlString += "<tr>";
            htmlString += "<th style='width=100;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>";
            htmlString += "UserID";
            htmlString += "</th>";
            htmlString += "<th style='width:300;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>";
            htmlString += "Callback Url";
            htmlString += "</th>";
            htmlString += "<th style='width:150;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>";
            htmlString += "Server Status";
            htmlString += "</th>";
            htmlString += "<th style='width:300;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>";
            htmlString += "Error Message";
            htmlString += "</th>";
            htmlString += "<th style='width:80;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>";
            htmlString += "Callbacks Count";
            htmlString += "</th>";
            htmlString += "<th style='width:80;font-weight:bold;font-size:8pt;font-family:verdana;border:1px solid #778899;background-color:#E8FDFB;color:black;padding:2px 2px 2px 2px'>";
            htmlString += "Notification Count";
            htmlString += "</th>";
            htmlString += "</tr>";
            foreach (spGetCallbackInvalid invalidSupport in invalids)
            {
                htmlString += "<tr>";
                htmlString += "<td style='vertical-align:top;word-break:break-all;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + invalidSupport.UserID + "</td>";
                htmlString += "<td style='vertical-align:top;word-break:break-all;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + invalidSupport.Url + "</td>";
                htmlString += "<td style='vertical-align:top;word-break:break-all;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + invalidSupport.ServerStatus + "</td>";
                htmlString += "<td style='vertical-align:top;word-break:break-all;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + invalidSupport.ErrorMessage + "</td>";
                htmlString += "<td style='text-align:right;vertical-align:top;word-break:break-all;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + invalidSupport.Total.ToString() + "</td>";

				CallbacksInvalidNotificationCrud notCrud = new CallbacksInvalidNotificationCrud();
				notCrud.Where("UserID", General.Operator.Equals, invalidSupport.UserID);
				List<CallbacksInvalidNotification> notifies = notCrud.ReadMulti().ToList();
                htmlString += "<td style='text-align:right;vertical-align:top;word-break:break-all;font-size:8pt;font-family:verdana;border:1px solid #778899;padding:2px 2px 2px 2px'>" + notifies[0].NotificationCount.ToString() + "</td>";
                htmlString += "</tr>";
            }
            htmlString += "</table>";
            htmlString += "</body>";
            htmlString += "</html>";

            String mailComd = "";
            mailComd = "Insert Into SendMsg\n";
            mailComd += "(Recipient, Send_Method, Msg_Subject, Msg, Date_Inserted, Retry_Count, HTML_Format, DueDateTime, SystemGenerated, QueuedByApplication)\n";
            mailComd += "Values\n";
            mailComd += "('christo@vcs.co.za', 1, 'Invalid Callback Response List', '" + htmlString.Replace("'", "''") + "', GetDate(), 0, 1, GetDate(), 1, 'VCS.VirtualVendor.CallbacksInvalidRemimder')";
            dbComd.CommandText = mailComd;
            dbComd.ExecuteNonQuery();


            #endregion
        }
        public CallbacksInvalid GetCallbackInvalid(String pUserID)
        {
            try
            {
                recordFound = false;
                CallbacksInvalidCrud crud = new CallbacksInvalidCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);

                CallbacksInvalid search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCallbackInvalid - " + Ex.Message);
            }
        }
        public CallbacksInvalid GetCallbackInvalid(Int32 pInvalidID)
        {
            try
            {
                recordFound = false;
                CallbacksInvalidCrud crud = new CallbacksInvalidCrud();
                crud.Where("InvalidID", General.Operator.Equals, pInvalidID);

                CallbacksInvalid search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCallbackInvalid - " + Ex.Message);
            }
        }
        public List<vCallbackUserDue> GetCallbackUserDue()
        {
            try
            {
                recordFound = false;
                vCallbackUserDueCrud crud = new vCallbackUserDueCrud();
                List<vCallbackUserDue> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCallbackUserDue - " + Ex.Message);
            }
        }
        public List<vCallbackTerminalDue> GetCallbackTerminalDue(String pUserID)
        {
            try
            {
                recordFound = false;
                vCallbackTerminalDueCrud crud = new vCallbackTerminalDueCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);

                List<vCallbackTerminalDue> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCallbackTerminalDue - " + Ex.Message);
            }
        }
        public Int32 InsertCallbackLog(DateTime pDateLoaded, String pDeliveryResponseText, String pDeliveryServerStatus, String pReferenceNumber, String pUserID)
        {
            try
            {
				if (pDeliveryResponseText.Length > 1000)
					pDeliveryResponseText = pDeliveryResponseText.Substring(0, 1000);

				CallbacksLogCrud crud = new CallbacksLogCrud();
                CallbacksLog workRec = new CallbacksLog();
                workRec.DeliveryResponseText = pDeliveryResponseText;
                workRec.DeliveryServerStatus = pDeliveryServerStatus;
                workRec.ReferenceNumber = pReferenceNumber;
                workRec.UserId = pUserID;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertCallbackLogDateTime - " + Ex.Message);
            }
        }
        public List<CallbacksLog> GetCallbacksLog(String pUserID, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                CallbacksLogCrud crud = new CallbacksLogCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                List<CallbacksLog> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCallback - " + Ex.Message);
            }
        }
        public Callbacks GetCallbacks(String pUserID, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                CallbacksCrud crud = new CallbacksCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                Callbacks search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCallback - " + Ex.Message);
            }
        }
        public Callbacks GetCallbacks(Decimal pAutoID)
        {
            try
            {
                recordFound = false;
                CallbacksCrud crud = new CallbacksCrud();
                crud.Where("AutoID", General.Operator.Equals, pAutoID);

                Callbacks search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCallbackDecimal - " + Ex.Message);
            }
        }
        public Boolean UpdateCallback(Decimal pAutoID, String pDelivered, String pServerStatus, String pErrorYN, String pErrorDescription, String pErrorEmailAddress, Int32 pDeliveryAttempts, Boolean pErrorReport)
        {
            try
            {
                Callbacks callback = GetCallbacks(pAutoID);
                callback.Delivered = pDelivered;
                callback.DeliveryServerStatus = pServerStatus;
                callback.ErrorYN = pErrorYN;
                callback.ErrorDescription = pErrorDescription;
                callback.ErrorEmailAddress = pErrorEmailAddress;
                callback.DeliverAttempts = pDeliveryAttempts.ToString();
                callback.ReportError = pErrorReport;
                CallbacksCrud crud = new CallbacksCrud();
                crud.Update(callback);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateCallbackDecimal - " + Ex.Message);
            }
        }
    }
}
