﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;

namespace DPO.VirtualVendor.BL
{
public partial class VirtualVendorBL
    {
        public List<vSettleCallbackReport> GetSettleCallbackReport()
        {
            try
            {
                recordFound = false;
                vSettleCallbackReportCrud crud = new vSettleCallbackReportCrud();
                List<vSettleCallbackReport> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetSettleCallbackReport - " + Ex.Message);
            }
        }
        public tblVaSettleCallback GetVaSettleCallback(String pUserID, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                tblVaSettleCallbackCrud crud = new tblVaSettleCallbackCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);
                tblVaSettleCallback search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetVaSettleCallback - " + Ex.Message);
            }
        }
        public Int32 SaveVaSettleCallback(String pUserID, String pReferenceNumber, String pSplitInformation, Int32 pSplitCount)
        {
            try
            {
                tblVaSettleCallbackCrud crud = new tblVaSettleCallbackCrud();
                tblVaSettleCallback workRec = new tblVaSettleCallback();
                workRec.ReferenceNumber = pReferenceNumber;
                workRec.SplitInformation = pSplitInformation;
                workRec.SplitCount = pSplitCount;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("SaveVaSettleCallback - " + Ex.Message);
            }
        }

		public Int32 SaveVaSettleCallback(String pUserID, String pReferenceNumber, String pSplitInformation, Int32 pSplitCount, Boolean pVaVmTransaction)
		{
			try
			{
				tblVaSettleCallbackCrud crud = new tblVaSettleCallbackCrud();
				tblVaSettleCallback workRec = new tblVaSettleCallback();
				workRec.ReferenceNumber = pReferenceNumber;
				workRec.SplitInformation = pSplitInformation;
				workRec.SplitCount = pSplitCount;
				workRec.VaVmTransaction = pVaVmTransaction;
				workRec.DateLoaded = DateTime.Now;
				workRec.NotifySent = "N";
				workRec.UserID = pUserID;
				long ID = 0;
				crud.Insert(workRec, out ID);
				return (Int32)ID;
			}
			catch (Exception Ex)
			{
				throw new Exception("SaveVaSettleCallback - " + Ex.Message);
			}
		}
		public Int32 SaveVaSettleCallbackVaInfo(Int32 pSettleCallbackID, String pCardNumber, String pVAAmount, String pVADescription, String pVAResponse, String pVAReference, Int32 pSplitNo, Int32 pAccountNumber, String pCardHolderName)
        {
            try
            {
                tblVaSettleCallbackVaInfoCrud crud = new tblVaSettleCallbackVaInfoCrud();
                tblVaSettleCallbackVaInfo workRec = new tblVaSettleCallbackVaInfo();
                workRec.CardNumber = pCardNumber;
                workRec.VAAmount = pVAAmount;
                workRec.VADescription = pVADescription;
                workRec.VAResponse = pVAResponse;
                workRec.VAReference = pVAReference;
                workRec.SplitNo = pSplitNo;
                workRec.AccountNumber = pAccountNumber;
                workRec.CardHolderName = pCardHolderName;
				workRec.SettleCallbackID = pSettleCallbackID;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("SaveVaSettleCallbackVaInfo - " + Ex.Message);
            }
        }
    }
}
