﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;


namespace DPO.VirtualVendor.BL
{
public partial class VirtualVendorBL
    {
		
		public Boolean InsertVoucherRequest(String pMerchantID, String pReference, String pProdCode, String pQuantity, String pClientCellNumber, String pClientEmailAddress, String pInstruction, String pInstructionUrl)
        {
            try
            {
                VoucherRequestsCrud crud = new VoucherRequestsCrud();
                VoucherRequests workRec = new VoucherRequests();
                workRec.Reference = pReference;
                workRec.ProdCode = pProdCode;
                workRec.Quantity = pQuantity;
                workRec.ClientCellNumber = pClientCellNumber;
                workRec.ClientEmailAddress = pClientEmailAddress;
                workRec.Instruction = pInstruction;
                crud.Insert(workRec);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertVoucherRequest - " + Ex.Message);
            }
        }
		public VoucherRequests GetVoucherRequests(String pMerchantID, String pReference)
        {
            try
            {
                recordFound = false;
                VoucherRequestsCrud crud = new VoucherRequestsCrud();
                crud.Where("MerchantID", General.Operator.Equals, pMerchantID);
                crud.And("Reference", General.Operator.Equals, pReference);

                VoucherRequests search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetVoucherRequest - " + Ex.Message);
            }
        }
		public Boolean UpdateVoucherRequest(String pMerchantID, String pReference, String pRequest, String pResponse, String pResponsedTo, String pResponseMessage)
        {
            try
            {
                VoucherRequests request = GetVoucherRequests(pMerchantID,pReference);
				request.pRequest = pRequest;
				request.pRespondedTo = pResponsedTo;
				request.pResponse = pResponse;
				request.pResponseMessage = pResponseMessage;
				request.ResponseDateTime = DateTime.Now;
                VoucherRequestsCrud crud = new VoucherRequestsCrud();
                crud.Update(request);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateVoucherRequest - " + Ex.Message);
            }
        }
        public vInvalidReferrersLatestDateLoaded GetInvalidReferrerLatestDateLoaded()
        {
            try
            {
                recordFound = false;
                vInvalidReferrersLatestDateLoadedCrud crud = new vInvalidReferrersLatestDateLoadedCrud();

                vInvalidReferrersLatestDateLoaded search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetInvalidReferrerLatestDateLoaded - " + Ex.Message);
            }
        }
        public List<vInvalidReferrers> GetInvalidReferrer()
        {
            try
            {
                recordFound = false;
                vInvalidReferrersCrud crud = new vInvalidReferrersCrud();
                List<vInvalidReferrers> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetInvalidReferrer - " + Ex.Message);
            }
        }
        public InvalidReferrers GetInvalidReferrers(Int32 pAutoID)
        {
            try
            {
                recordFound = false;
                InvalidReferrersCrud crud = new InvalidReferrersCrud();
                crud.Where("AutoID", General.Operator.Equals, pAutoID);

                InvalidReferrers search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetInvalidReferrer - " + Ex.Message);
            }
        }
        public Int32 InsertVcsAlert(Int32 pTypeCode, String pAlertValue, String pNotifyEmail, String pOccurenceAction)
        {
            try
            {
                VcsAlertsCrud crud = new VcsAlertsCrud();
                VcsAlerts workRec = new VcsAlerts();
                workRec.AlertValue = pAlertValue;
                workRec.NotifyEmail = pNotifyEmail;
                workRec.OccurenceAction = pOccurenceAction;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertVcsAlert - " + Ex.Message);
            }
        }
        public Boolean UpdateInvalidReferrer(Int32 pAutoID, String pEmailAddress)
        {
            try
            {
                InvalidReferrers referrer = GetInvalidReferrers(pAutoID);
                referrer.NotificationSent = "Y";
                referrer.NotificationEmailAddress = pEmailAddress;
                referrer.NotificationDate = DateTime.Now;
                InvalidReferrersCrud crud = new InvalidReferrersCrud();
                crud.Update(referrer);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateInvalidReferrer - " + Ex.Message);
            }
        }
        public Int32 InsertBlockedIP(String pIP, String pBlockReason)
        {
            try
            {
                BlockedIPs block = new BlockedIPs();
                block.IP = pIP;
                block.DateBlocked = DateTime.Now;
                block.ReasonBlocked = pBlockReason;
                block.Attempts = 1;
                block.DateBlocked = DateTime.Now;
                block.DateLastTried = DateTime.Now;

                BlockedIPsCrud crud = new BlockedIPsCrud();
				long ID = 0;
				crud.Insert(block, out ID);
				return (Int32)ID;
            }
            catch (Exception Ex)
            {
                String errorMessage = "InsertBlockedIP(String,String) - " + Ex.Message;
                throw new Exception(errorMessage);
            }
        }
        public Boolean UpdateBlockedIP(String pIpAddress)
        {
            try
            {
                BlockedIPs block = GetBlockedIPs(pIpAddress);
                block.Attempts = Convert.ToInt32(block.Attempts) + 1;
                block.DateLastTried = DateTime.Now;
                BlockedIPsCrud crud = new BlockedIPsCrud();
				crud.Insert(block);

                return true;
            }
            catch (Exception Ex)
            {
                String errorMessage = "UpdateBlockedIP(String) - " + Ex.Message;
                throw new Exception(errorMessage);
            }
        }
        public BlockedIPs GetBlockedIPs(String pIP)
        {
            try
            {
                recordFound = false;
                BlockedIPsCrud crud = new BlockedIPsCrud();
                crud.Where("IP", General.Operator.Equals, pIP);

                BlockedIPs search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetBlockedIP - " + Ex.Message);
            }
        }
		public List<mPars> SplitMPars(String pMPars)
		{
			try
			{
				List<mPars> returnPars = new List<mPars>();
				List<String> pars = pMPars.Split('~').ToList();

				foreach (String par in pars)
				{
					if (par != "")
					{
						if (par.Contains("="))
						{
							String[] parString = par.Split('=');
							String name = parString[0];
							String value = parString[1];
							mPars thisPar = new mPars();
							thisPar.mParName = name;
							thisPar.mParValue = value;
							returnPars.Add(thisPar);
						}
					}
				}

				return returnPars;
			}
			catch (Exception Ex)
			{
				throw new Exception("SplitMPars-" + Ex.Message);
			}
		}
    }
}
