﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DPO.VirtualVendor.BL
{
	public partial class VirtualVendorBL
	{
        internal String StripDoubleQuotes(String pMessage)
        {
            Int32 quotePos = pMessage.IndexOf("\"");
           while (quotePos!= -1)
           {
               if (quotePos == 0)
                   pMessage = pMessage.Substring(1);
               else
                   if (quotePos == pMessage.Length -1)
                       pMessage = pMessage.Substring(0, pMessage.Length -1);
                   else
                       pMessage = pMessage.Substring(0, pMessage.IndexOf("\"")) + pMessage.Substring(pMessage.IndexOf("\"") +1 ); 

               quotePos = pMessage.IndexOf("\"");
           }

           return pMessage;
        }

        internal String StripComma(String pMessage)
        {
            Int32 quotePos = pMessage.IndexOf(",");
            while (quotePos != -1)
            {
                if (quotePos == 0)
                    pMessage = pMessage.Substring(1);
                else
                    if (quotePos == pMessage.Length - 1)
                        pMessage = pMessage.Substring(0, pMessage.Length - 1);
                    else
                        pMessage = pMessage.Substring(0, pMessage.IndexOf(",")) + pMessage.Substring(pMessage.IndexOf(",") + 1);

                quotePos = pMessage.IndexOf(",");
            }

            return pMessage;
        }

        internal String StripNewline(String pMessage)
        {
            Int32 quotePos = pMessage.IndexOf("\n");
            while (quotePos != -1)
            {
                if (quotePos == 0)
                    pMessage = pMessage.Substring(1);
                else
                    if (quotePos == pMessage.Length - 1)
                        pMessage = pMessage.Substring(0, pMessage.Length - 1);
                    else
                        pMessage = pMessage.Substring(0, pMessage.IndexOf("\n")) + pMessage.Substring(pMessage.IndexOf("\n") + 1);

                quotePos = pMessage.IndexOf("\n");
            }

            return pMessage;
        }

        internal String StripHash(String pMessage)
        {
            Int32 quotePos = pMessage.IndexOf("#");
            while (quotePos != -1)
            {
                if (quotePos == 0)
                    pMessage = pMessage.Substring(1);
                else
                    if (quotePos == pMessage.Length - 1)
                        pMessage = pMessage.Substring(0, pMessage.Length - 1);
                    else
                        pMessage = pMessage.Substring(0, pMessage.IndexOf("#")) + pMessage.Substring(pMessage.IndexOf("#") + 1);

                quotePos = pMessage.IndexOf("#");
            }

            return pMessage;
        }


        internal String StripAmpersand(String pMessage)
        {
            Int32 quotePos = pMessage.IndexOf("&");
            while (quotePos != -1)
            {
                if (quotePos == 0)
                    pMessage = pMessage.Substring(1);
                else
                    if (quotePos == pMessage.Length - 1)
                        pMessage = pMessage.Substring(0, pMessage.Length - 1);
                    else
                        pMessage = pMessage.Substring(0, pMessage.IndexOf("&")) + pMessage.Substring(pMessage.IndexOf("&") + 1);

                quotePos = pMessage.IndexOf("&");
            }

            return pMessage;
        }

        public String Apos(String pStringIn)
        {
            return pStringIn.Replace("'", "''");
        }

        public String StripCurrency(String pStringIn)
        {
            if (pStringIn == null)
                return "";
            if (pStringIn == "")
                return "";

            pStringIn = StripDoubleQuotes(pStringIn);
            pStringIn = StripComma(pStringIn);
            pStringIn = StripNewline(pStringIn);
            pStringIn = StripAmpersand (pStringIn);
            pStringIn = StripHash(pStringIn);
            pStringIn = Apos(pStringIn);

            return pStringIn;
        }
        public String StripSpecialCharacters(String pStringIn)
        {
            String returnString = pStringIn;
            returnString = StripDoubleQuotes(pStringIn);
            returnString = StripComma(returnString);
            returnString = StripNewline(returnString);
            returnString = StripHash(returnString);
            returnString = StripAmpersand(returnString);
            returnString = Apos(returnString);
            return returnString;
        }
    }
}
