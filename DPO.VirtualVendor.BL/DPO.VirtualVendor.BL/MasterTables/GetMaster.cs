﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;

namespace DPO.VirtualVendor.BL
{
public partial class VirtualVendorBL
    {
        
        public List<Bins> GetBins(String pUserID)
        {
            try
            {
                recordFound = false;
                BinsCrud crud = new BinsCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);

                List<Bins> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetBin - " + Ex.Message);
            }
        }
        public tblCardType GetCardType(Int32 pCardTypeID)
        {
            try
            {
                recordFound = false;
                tblCardTypeCrud crud = new tblCardTypeCrud();
                crud.Where("CardTypeID", General.Operator.Equals, pCardTypeID);

                tblCardType search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCardType - " + Ex.Message);
            }
        }
        public List<tblCardType> GetCardType()
        {
            try
            {
                recordFound = false;
                tblCardTypeCrud crud = new tblCardTypeCrud();
                List<tblCardType> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCardType - " + Ex.Message);
            }
        }
        public tblInterface GetInterface(String pInterfaceNo)
        {
            try
            {
                recordFound = false;
                tblInterfaceCrud crud = new tblInterfaceCrud();
                crud.Where("InterfaceNo", General.Operator.Equals, pInterfaceNo);

                tblInterface search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetInterface - " + Ex.Message);
            }
        }
    }
}
