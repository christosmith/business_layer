﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;

namespace DPO.VirtualVendor.BL
{
public partial class VirtualVendorBL
    {
        public Acquirer GetAcquirerByID(String pID)
        {
            try
            {
                recordFound = false;
                AcquirerCrud crud = new AcquirerCrud();
                crud.Where("ID", General.Operator.Equals, pID);

                Acquirer search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetAcquirerByID - " + Ex.Message);
            }
        }
        public AcquirerMerchant GetAcquirerMerchantByID(String pUserID, String pAcquirer)
        {
            try
            {
                recordFound = false;
                AcquirerMerchantCrud crud = new AcquirerMerchantCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("Acquirer", General.Operator.Equals, pAcquirer);

                AcquirerMerchant search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetAcquirerMerchantByID - " + Ex.Message);
            }
        }
    }
}
