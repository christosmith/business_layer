﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;

namespace DPO.VirtualVendor.BL
{
public partial class VirtualVendorBL
    {
        public List<MerchantsFTPUploads> GetMerchantsFTPUploads()
        {
            try
            {
                recordFound = false;
                MerchantsFTPUploadsCrud crud = new MerchantsFTPUploadsCrud();
                List<MerchantsFTPUploads> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMerchantsFTPUpload - " + Ex.Message);
            }
        }
    }
}
