﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;
using System.Security.Cryptography;

namespace DPO.VirtualVendor.BL
{
   
public partial class VirtualVendorBL
    {
		public List<Merchants> GetMerchantPerEmailAddress(String pEmailAddress)
		{
			try
			{
				MerchantsCrud crud = new MerchantsCrud();
				crud.Where("Email", General.Operator.Like, "%" + pEmailAddress + "%");
				crud.Or("InvoiceEmail", General.Operator.Like, pEmailAddress);
				crud.Or("CallbackEmailAddress", General.Operator.Like, pEmailAddress);
				crud.Or("OwnerEmailAddress", General.Operator.Like, pEmailAddress);
				crud.Or("TechEmailAddress", General.Operator.Like, pEmailAddress);
				crud.Or("ReceiptEmailAddress", General.Operator.Like, pEmailAddress);
				recordFound = false;
				List<Merchants> mer = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return mer;

			}
			catch (Exception Ex)
			{
				throw new Exception("GetMerchantPerEmailAddress - " + Ex.Message);
			}
		}
        public List<MerchantCurrency> GetMerchantCurrency(String pUserID)
        {
            try
            {
                recordFound = false;
                MerchantCurrencyCrud crud = new MerchantCurrencyCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);

                List<MerchantCurrency> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMerchantCurrency - " + Ex.Message);
            }
        }
		private ccFormParameters SetCCFormParametersNull(ccFormParameters pParam)
        {
            try
            {
                if (pParam.TerminalID == null) pParam.TerminalID = "";
                if (pParam.ReferenceNumber == null) pParam.ReferenceNumber = "";
                if (pParam.DescriptionOfGoods == null) pParam.DescriptionOfGoods = "";
                if (pParam.Amount == null) pParam.Amount = 0;
                if (pParam.Currency == null) pParam.Currency = "";
                if (pParam.OccurCount == null) pParam.OccurCount = "";
                if (pParam.OccurFrequency == null) pParam.OccurFrequency = "";
                if (pParam.CellNumber == null) pParam.CellNumber = "";
                if (pParam.CellMessage == null) pParam.CellMessage = "";
                if (pParam.CancelUrl == null) pParam.CancelUrl = "";
                if (pParam.OccurEmailAddress == null) pParam.OccurEmailAddress = "";
                if (pParam.DelayedSettlement == null) pParam.DelayedSettlement = "";
                if (pParam.OccurAmount == null) pParam.OccurAmount = 0;
                if (pParam.PaymentMethod == null) pParam.PaymentMethod = "";
                if (pParam.CardType == null) pParam.CardType = "";
                if (pParam.CardholderName == null) pParam.CardholderName = "";
                if (pParam.CardholderEmail == null) pParam.CardholderEmail = "";
                if (pParam.NextOccurDate == null) pParam.NextOccurDate = "";
                if (pParam.BasketEntry == null) pParam.BasketEntry = "";
                if (pParam.VTInvoice == null) pParam.VTInvoice = "";
                if (pParam.CCPay == null) pParam.CCPay = "";
                if (pParam.CCReservation == null) pParam.CCReservation = "";
                if (pParam.OSCommerce == null) pParam.OSCommerce = "";
                if (pParam.OSApprovedURL == null) pParam.OSApprovedURL = "";
                if (pParam.OSDeclinedURL == null) pParam.OSDeclinedURL = "";
                if (pParam.ApprovedURL == null) pParam.ApprovedURL = "";
                if (pParam.DeclinedURL == null) pParam.DeclinedURL = "";
                if (pParam.Hash == null) pParam.Hash = "";
                if (pParam.BudgetAllowed == null) pParam.BudgetAllowed = "";
                if (pParam.Recap == null) pParam.Recap = "";
                if (pParam.RecapLine1 == null) pParam.RecapLine1 = "";
                if (pParam.RecapLine2 == null) pParam.RecapLine2 = "";
                if (pParam.RecapLine3 == null) pParam.RecapLine3 = "";
                if (pParam.TCUrl == null) pParam.TCUrl = "";
                if (pParam.TCConfirm == null) pParam.TCConfirm = "";
                if (pParam.LastAcceptanceDate == null) pParam.LastAcceptanceDate = Convert.ToDateTime("1900-01-01");
                if (pParam.MysteryDiscount == null) pParam.MysteryDiscount = "";
                if (pParam.MerchantMD5String == null) pParam.MerchantMD5String = "";
                if (pParam.OccurUpdate == null) pParam.OccurUpdate = "";
                if (pParam.URLsProvided == null) pParam.URLsProvided = "";
                if (pParam.m_1 == null) pParam.m_1 = "";
                if (pParam.m_2 == null) pParam.m_2 = "";
                if (pParam.m_3 == null) pParam.m_3 = "";
                if (pParam.m_4 == null) pParam.m_4 = "";
                if (pParam.m_5 == null) pParam.m_5 = "";
                if (pParam.m_6 == null) pParam.m_6 = "";
                if (pParam.m_7 == null) pParam.m_7 = "";
                if (pParam.m_8 == null) pParam.m_8 = "";
                if (pParam.m_9 == null) pParam.m_9 = "";
                if (pParam.m_10 == null) pParam.m_10 = "";


                return pParam;
            }
            catch (Exception Ex)
            {
                throw new Exception("SetCCFormParametersNull - " + Ex.Message);
            }
        }
        public String CalculateMD5(String pInput, String pMD5Url)
        {
            try
            {

                System.Net.WebClient client = new System.Net.WebClient();
                System.IO.Stream data = client.OpenRead(pMD5Url + "?hash=" + pInput);
                System.IO.StreamReader reader = new System.IO.StreamReader(data);
                String s = reader.ReadToEnd();
                data.Close();
                reader.Close();

                return s;
            }
            catch (Exception Ex)
            {
                throw new Exception("CalculateMD5(String) - " + Ex.Message);
            }
        }
        public String SetMerchantMd5String(ccFormParameters pParam)
        {
            try
            {
                String md5String = "";
                pParam = SetCCFormParametersNull(pParam);
                if (pParam.UserID != "") md5String += pParam.UserID;
                if (pParam.ReferenceNumber != "") md5String += pParam.ReferenceNumber;
                if (pParam.DescriptionOfGoods != "") md5String += pParam.DescriptionOfGoods;
                if (pParam.Amount != 0) md5String += pParam.Amount;
                if (pParam.Currency != "") md5String += pParam.Currency;
                if (pParam.OccurCount != "0") md5String += pParam.OccurCount;
                if (pParam.OccurFrequency != "") md5String += pParam.OccurFrequency;
                if (pParam.CellNumber != "") md5String += pParam.CellNumber;
                if (pParam.CellMessage != "") md5String += pParam.CellMessage;
                if (pParam.CancelUrl != "") md5String += pParam.CancelUrl;
                if (pParam.OccurEmailAddress != "") md5String += pParam.OccurEmailAddress;
                if (pParam.DelayedSettlement != "") md5String += pParam.DelayedSettlement;
                if (pParam.OccurAmount != 0) md5String += pParam.OccurAmount;
                if (pParam.NextOccurDate != "") md5String += pParam.NextOccurDate;
                if (pParam.BudgetAllowed != "") md5String += pParam.BudgetAllowed;
                if (pParam.CardholderEmail != "") md5String += pParam.CardholderEmail;
                if (pParam.m_1 != "") md5String += pParam.m_1;
                if (pParam.m_2 != "") md5String += pParam.m_2;
                if (pParam.m_3 != "") md5String += pParam.m_3;
                if (pParam.m_4 != "") md5String += pParam.m_4;
                if (pParam.m_5 != "") md5String += pParam.m_5;
                if (pParam.m_6 != "") md5String += pParam.m_6;
                if (pParam.m_7 != "") md5String += pParam.m_7;
                if (pParam.m_8 != "") md5String += pParam.m_8;
                if (pParam.m_9 != "") md5String += pParam.m_9;
                if (pParam.m_10 != "") md5String += pParam.m_10;
                if (pParam.CustomerID != "") md5String += pParam.CustomerID;
                if (pParam.MerchantToken != "") md5String += pParam.MerchantToken;
                if (pParam.RecurReference != "") md5String += pParam.RecurReference;
                return md5String;
            }
            catch (Exception Ex)
            {
                throw new Exception("SetMerchantMD5String - " + Ex.Message);
            }
        }
        public List<CountryCodes> GetCountryCodes()
        {
            try
            {
                recordFound = false;
                CountryCodesCrud crud = new CountryCodesCrud();
                List<CountryCodes> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCountryCode - " + Ex.Message);
            }
        }
        public List<MerchantCurrency> GetMerchantCurrency(String pUserID, String pAlphaCurrencyCode)
        {
            try
            {
                recordFound = false;
                MerchantCurrencyCrud crud = new MerchantCurrencyCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("AlphaCurrencyCode", General.Operator.Equals, pAlphaCurrencyCode);

                List<MerchantCurrency> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMerchantCurrency - " + Ex.Message);
            }
        }
        public Int32 GetMerchantCounter(String pUserID)
        {
            try
            {
				MerchantCountsCrud crud = new MerchantCountsCrud();
				crud.Where("UserID", General.Operator.Equals, pUserID);
				MerchantCounts count = crud.ReadSingle();
				Int32 merchantCounter = (Int32)count.MultiChoiceAfrica;
                count.MultiChoiceAfrica = Convert.ToInt32(count.MultiChoiceAfrica) + 1;
				crud.Update(count);

                return merchantCounter;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public String GetMerchantCountryGridData()
        {
            try
            {
                List<vMerchantCountries> countries = GetMerchantCountry();
                String divString = "";
                foreach (vMerchantCountries country in countries)
                {
                    divString += country.Code + "═";
                    divString += country.Description + "║";
                }

                if (divString.Length > 0)
                    divString = divString.Substring(0, divString.Length - 1);

                return divString;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMerchantCountryGridData - " + Ex.Message);
            }
        }
        public CountryCodes GetMerchantCountry(String pCode)
        {
            try
            {
                recordFound = false;
                CountryCodesCrud crud = new CountryCodesCrud();
                crud.Where("Code", General.Operator.Equals, pCode);

                CountryCodes search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMerchantCountry - " + Ex.Message);
            }
        }
        public List<vMerchantCountries> GetMerchantCountry()
        {
            try
            {
                recordFound = false;
                vMerchantCountriesCrud crud = new vMerchantCountriesCrud();
                List<vMerchantCountries> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMerchantCountry - " + Ex.Message);
            }
        }
        public String GetAcquirerGridData()
        {
			try
			{
				List<Acquirer> acquirers = GetAcquirer();
				String divString = "";
				foreach (Acquirer acquirer in acquirers)
				{
					divString += acquirer.Id + "═";
					divString += acquirer.Name + "║";
				}

				if (divString.Length > 0)
					divString = divString.Substring(0, divString.Length - 1);

				return divString;
			}
            catch (Exception Ex)
            {
                throw new Exception("GetAcquirerGridData - " + Ex.Message);
            }
        }
        public List<Acquirer> GetAcquirer()
        {
            try
            {
                recordFound = false;
                AcquirerCrud crud = new AcquirerCrud();
                List<Acquirer> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetAcquirer - " + Ex.Message);
            }
        }
        public List<String> GetMerchantBankAll()
        {
            try
            {
				List<Merchants> merchants = GetMerchants();
                List<String> result = merchants.Select(x => x.Acquirer).Distinct().ToList();
                return result;

            }
            catch (Exception Ex)
            {
				throw new Exception("GetMerchantBankAll -" + Ex.Message);
            }

        }
        public MerchantStatus GetMerchantStatus(String pID)
        {
            try
            {
                recordFound = false;
                MerchantStatusCrud crud = new MerchantStatusCrud();
                crud.Where("ID", General.Operator.Equals, pID);

                MerchantStatus search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMerchantStatus - " + Ex.Message);
            }
        }
        public List<MerchantStatus> GetMerchantStatus()
        {
            try
            {
                recordFound = false;
                MerchantStatusCrud crud = new MerchantStatusCrud();
                List<MerchantStatus> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMerchantStatus - " + Ex.Message);
            }
        }
        public Merchants GetMerchants(String pUserID)
        {
            try
            {
                recordFound = false;
                MerchantsCrud crud = new MerchantsCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);

                Merchants search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMerchant - " + Ex.Message);
            }
        }
        public List<Merchants> GetMerchants()
        {
            try
            {
                recordFound = false;
                MerchantsCrud crud = new MerchantsCrud();
                List<Merchants> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMerchant - " + Ex.Message);
            }
        }
    }
}
