﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;

namespace DPO.VirtualVendor.BL
{
public partial class VirtualVendorBL
    {
        public List<tblContactSavedSelection> GetSavedSelection()
        {
            try
            {
                recordFound = false;
                tblContactSavedSelectionCrud crud = new tblContactSavedSelectionCrud();
                List<tblContactSavedSelection> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetSavedSelection - " + Ex.Message);
            }
        }
        public List<tblContactSavedSelection> GetSavedSelection(Int32 pUserID)
        {
            try
            {
                recordFound = false;
                tblContactSavedSelectionCrud crud = new tblContactSavedSelectionCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);

                List<tblContactSavedSelection> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetSavedSelection - " + Ex.Message);
            }
        }
        public List<vContactDetail> GetContactDetailPerGroup(String pContactGroup, String pUserID)
        {
            try
            {
                recordFound = false;
                vContactDetailCrud crud = new vContactDetailCrud();
                crud.Where("ContactGroup", General.Operator.Equals, pContactGroup);
                crud.And("UserID", General.Operator.Equals, pUserID);

                List<vContactDetail> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactDetailPerGroup - " + Ex.Message);
            }
        }
        public List<vContactDetail> GetContactDetailPerGroup(String pContactGroup)
        {
            try
            {
                recordFound = false;
                vContactDetailCrud crud = new vContactDetailCrud();
                crud.Where("ContactGroup", General.Operator.Equals, pContactGroup);

                List<vContactDetail> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactDetailPerGroup - " + Ex.Message);
            }
        }
        public List<vContactDetail> GetContactDetailPerGroup(Int32 pContactGroupID)
        {
            try
            {
                recordFound = false;
                vContactDetailCrud crud = new vContactDetailCrud();
                crud.Where("ContactGroupID", General.Operator.Equals, pContactGroupID);

                List<vContactDetail> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactDetailPerGroup - " + Ex.Message);
            }
        }
        public List<vContactDetail> GetContactDetailPerGroup(Int32 pContactGroupID, Int32 pContactTypeID)
        {
            try
            {
                recordFound = false;
                vContactDetailCrud crud = new vContactDetailCrud();
                crud.Where("ContactGroupID", General.Operator.Equals, pContactGroupID);
                crud.And("ContactTypeID", General.Operator.Equals, pContactTypeID);

                List<vContactDetail> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactDetailPerGroup - " + Ex.Message);
            }
        }

		public List<tblContactDetail> GetContactDetailPerEmailAddress(String pEmailAddress)
		{
			try
			{
				tblContactDetailCrud crud = new tblContactDetailCrud();
				crud.Where("ContactDetail", General.Operator.Like, "%" + pEmailAddress + "%");
				recordFound = false;
				List<tblContactDetail> contact = crud.ReadMulti().ToList();
				recordFound = crud.RecordFound;
				return contact;
			}
			catch (Exception Ex)
			{
				throw new Exception("GetContactDetailPerEmailAddress - " + Ex.Message);
			}
		}
        public List<tblContactDetail> GetContactDetailPerContactPersonID(Int32 pContactPersonID)
        {
            try
            {
                recordFound = false;
                tblContactDetailCrud crud = new tblContactDetailCrud();
                crud.Where("ContactPersonID", General.Operator.Equals, pContactPersonID);

                List<tblContactDetail> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactDetailPerContactPersonID - " + Ex.Message);
            }
        }
        public List<tblContactPersonGroup> GetPersonContactGroup(Int32? pContactPersonID)
        {
            try
            {
                recordFound = false;
                tblContactPersonGroupCrud crud = new tblContactPersonGroupCrud();
                crud.Where("ContactPersonID", General.Operator.Equals, (Int32)pContactPersonID);

                List<tblContactPersonGroup> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetPersonContactGroup? - " + Ex.Message);
            }
        }
        public tblContactGroupMerchant GetMerchantContactGroup(String pUserID, Int32 pContactGroupID)
        {
            try
            {
                recordFound = false;
                tblContactGroupMerchantCrud crud = new tblContactGroupMerchantCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ContactGroupID", General.Operator.Equals, pContactGroupID);

                tblContactGroupMerchant search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMerchantContactGroup - " + Ex.Message);
            }
        }
        public List<tblContactGroupMerchant> GetMerchantContactGroup(String pUserID)
        {
            try
            {
                recordFound = false;
                tblContactGroupMerchantCrud crud = new tblContactGroupMerchantCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);

                List<tblContactGroupMerchant> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMerchantContactGroup - " + Ex.Message);
            }
        }
        public List<tblMerchantCardType> GetMerchantCardType()
        {
            try
            {
                recordFound = false;
                tblMerchantCardTypeCrud crud = new tblMerchantCardTypeCrud();
                List<tblMerchantCardType> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMerchantCardType - " + Ex.Message);
            }
        }
        public List<tblContactPersonGroup> GetContactPersonGroup()
        {
            try
            {
                recordFound = false;
                tblContactPersonGroupCrud crud = new tblContactPersonGroupCrud();
                List<tblContactPersonGroup> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactPersonGroup - " + Ex.Message);
            }
        }
        public tblContactGroup GetContactGroupPerName(String pContactGroup)
        {
            try
            {
                recordFound = false;
                tblContactGroupCrud crud = new tblContactGroupCrud();
                crud.Where("ContactGroup", General.Operator.Equals, pContactGroup);

                tblContactGroup search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactGroupPerName - " + Ex.Message);
            }
        }
        public List<tblContactGroup> GetContactGroup()
        {
            try
            {
                recordFound = false;
                tblContactGroupCrud crud = new tblContactGroupCrud();
                List<tblContactGroup> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactGroup - " + Ex.Message);
            }
        }
        public tblContactGroup GetContactGroup(Int32 pContactGroupID)
        {
            try
            {
                recordFound = false;
                tblContactGroupCrud crud = new tblContactGroupCrud();
                crud.Where("ContactGroupID", General.Operator.Equals, pContactGroupID);

                tblContactGroup search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactGroup - " + Ex.Message);
            }
        }
        public List<tblContactGroupMerchant> GetContactGroup(String pUserID)
        {
            try
            {
                recordFound = false;
                tblContactGroupMerchantCrud crud = new tblContactGroupMerchantCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);

                List<tblContactGroupMerchant> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactGroup - " + Ex.Message);
            }
        }
        public List<tblContactPerson> GetContactPersonPerTerminalID(String _userID)
        {
            try
            {
                recordFound = false;
                tblContactPersonCrud crud = new tblContactPersonCrud();
                crud.Where("userID", General.Operator.Equals, _userID);

                List<tblContactPerson> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactPersonPerTerminalID - " + Ex.Message);
            }
        }
        public List<tblContactType> GetContactType()
        {
            try
            {
                recordFound = false;
                tblContactTypeCrud crud = new tblContactTypeCrud();
                List<tblContactType> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactType - " + Ex.Message);
            }
        }
        public List<tblContactPerson> GetContactPerson()
        {
            try
            {
                recordFound = false;
                tblContactPersonCrud crud = new tblContactPersonCrud();
                List<tblContactPerson> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactPerson - " + Ex.Message);
            }
        }
        public List<tblContactDetail> GetContactDetail()
        {
            try
            {
                recordFound = false;
                tblContactDetailCrud crud = new tblContactDetailCrud();
                List<tblContactDetail> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactDetail - " + Ex.Message);
            }
        }
        public List<vContactDetail> GetContactDetail(String pUserID, Int32 pContactGroupID, Int32 pContactTypeID)
        {
            try
            {
                recordFound = false;
                vContactDetailCrud crud = new vContactDetailCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ContactGroupID", General.Operator.Equals, pContactGroupID);
                crud.And("ContactTypeID", General.Operator.Equals, pContactTypeID);

                List<vContactDetail> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactDetail - " + Ex.Message);
            }
        }
        public List<vContactDetail> GetContactDetailPerTerminalIDPerGroup(String pUserID, String pContactGroup)
        {
            try
            {
                recordFound = false;
                vContactDetailCrud crud = new vContactDetailCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ContactGroup", General.Operator.Equals, pContactGroup);

                List<vContactDetail> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactDetailPerTerminalIDPerGroup - " + Ex.Message);
            }
        }
        public List<tblContactDetail> GetContactDetailPerTerminalID(String _userID)
        {
            try
            {
				tblContactPersonCrud crud = new tblContactPersonCrud();
				crud.Where("UserID", General.Operator.Equals, _userID);
				List<tblContactPerson> contactPersons = crud.ReadMulti().ToList();
                List<tblContactDetail> contactDetail = new List<tblContactDetail>();

                foreach (tblContactPerson _contactPerson in contactPersons)
                {
					tblContactDetailCrud detCrud = new tblContactDetailCrud();
					detCrud.Where("ContactPersonID", General.Operator.Equals, (Int32)_contactPerson.ContactPersonID);
					List<tblContactDetail> _contactDetailList = detCrud.ReadMulti().ToList();

                    for (Int32 contactLoop =0; contactLoop < _contactDetailList.Count; contactLoop++)
                        contactDetail.Add((tblContactDetail)_contactDetailList[contactLoop]);
                }

                return contactDetail;
            }
            catch(Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
