﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;

namespace DPO.VirtualVendor.BL
{
public partial class VirtualVendorBL
    {
        public String GetMailTemplateGridData(String pUserName)
        {
			try
			{
				List<tblContactMailTemplate> templates = GetMailTemplate(pUserName);
				String divString = "";
				foreach (tblContactMailTemplate template in templates)
				{
					divString += template.TemplateID + "═";
					divString += template.TemplateName + "║";
				}

				if (divString.Length > 0)
					divString = divString.Substring(0, divString.Length - 1);

				return divString;
			}
            catch (Exception Ex)
            {
                throw new Exception("GetMailTemplateGridData - " + Ex.Message);
            }
        }
        public tblContactMailTemplate GetMailTemplateByID(Int32 pTemplateID)
        {
            try
            {
                recordFound = false;
                tblContactMailTemplateCrud crud = new tblContactMailTemplateCrud();
                crud.Where("TemplateID", General.Operator.Equals, pTemplateID);

                tblContactMailTemplate search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMailTemplateByID - " + Ex.Message);
            }
        }


        public tblContactMailTemplate GetContactMailTemplate(Int32 pTemplateID)
        {
            try
            {
                tblContactMailTemplate templ = new tblContactMailTemplate();
                tblContactMailTemplateCrud crud = new tblContactMailTemplateCrud();
                crud.Where("TemplateID", General.Operator.Equals, pTemplateID);
                recordFound = false;
                templ = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return templ;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactMailTemplate - " + Ex.Message);
            }
        }
        public Boolean DeleteMailTemplate(Int32 pMailTemplateID)
        {
            try
            {
                tblContactMailTemplate templ = GetContactMailTemplate(pMailTemplateID);
                tblContactMailTemplateCrud crud = new tblContactMailTemplateCrud();
                crud.Delete(templ);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteMailTemplate - " + Ex.Message);
            }
        }
        public List<tblContactMailTemplate> GetMailTemplate()
        {
            try
            {
                recordFound = false;
                tblContactMailTemplateCrud crud = new tblContactMailTemplateCrud();
                List<tblContactMailTemplate> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMailTemplate - " + Ex.Message);
            }
        }
        public List<tblContactMailTemplate> GetMailTemplate(String pUserName)
        {
            vtUsers user = GetVTUser(pUserName);
			tblContactMailTemplateCrud crud = new tblContactMailTemplateCrud();
			crud.Where("UserID", General.Operator.Equals, (Int32)user.AutoId);
			List<tblContactMailTemplate> search = crud.ReadMulti().ToList();
			return search;
        }
        public tblContactMailTemplate GetMailTemplate(Int32 pTemplateID)
        {
            try
            {
                recordFound = false;
                tblContactMailTemplateCrud crud = new tblContactMailTemplateCrud();
                crud.Where("TemplateID", General.Operator.Equals, pTemplateID);

                tblContactMailTemplate search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMailTemplate - " + Ex.Message);
            }
        }
    }
}
