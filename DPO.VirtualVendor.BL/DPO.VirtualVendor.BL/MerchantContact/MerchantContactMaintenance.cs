﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;


namespace DPO.VirtualVendor.BL
{
public partial class VirtualVendorBL
    {
        /// <summary>
        /// Saves the selection made on the merchant messaging page.
        /// </summary>
        /// <param name="psavedSelection"></param>
        /// <returns></returns>
		public Int32 SaveMerchantContactSelection(tblContactSavedSelection psavedSelection)
		{
			try
			{
				tblContactSavedSelectionCrud crud = new tblContactSavedSelectionCrud();
				tblContactSavedSelection rec = new tblContactSavedSelection();
				rec.Bank = psavedSelection.Bank;
				rec.CardType = psavedSelection.CardType;
				rec.Channel = psavedSelection.Channel;
				rec.ContactGroup = psavedSelection.ContactGroup;
				rec.Country = psavedSelection.Country;
				rec.MailSubject = psavedSelection.MailSubject;
				rec.MerchantStatus = psavedSelection.MerchantStatus;
				rec.SelectionDescription = psavedSelection.SelectionDescription;
				rec.SelectionID = psavedSelection.SelectionID;
				rec.SignatureID = psavedSelection.SignatureID;
				rec.TemplateID = psavedSelection.TemplateID;
				rec.UserID = psavedSelection.UserID;
				long id = 0;
				crud.Insert(rec, out id);
				return (Int32)id;
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
        public Boolean spContactRecurring(Int32 pVTUserID)
        {
            try
            {
                StoredProcedure.spContactRecurring(pVTUserID);
                
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("spContactRecurring - " + Ex.Message);
            }
        }
        public Boolean SaveMerchantContactGroup(tblContactGroupMerchant pContactGroupMerchant)
        {
            try
            {
				tblContactGroupMerchantCrud crud = new tblContactGroupMerchantCrud();
				tblContactGroupMerchant rec = new tblContactGroupMerchant();
				rec.ContactGroupID = pContactGroupMerchant.ContactGroupID;
				rec.GroupMerchantID = pContactGroupMerchant.GroupMerchantID;
				rec.UpdateDateTime = pContactGroupMerchant.UpdateDateTime;
				rec.UpdateUserID = pContactGroupMerchant.UpdateUserID;
				rec.UserID = pContactGroupMerchant.UserID;
				crud.Insert(rec);
				return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("SaveMerchantContactGroup - " + Ex.Message);
            }
        }
        public Boolean DeleteContactGroup(Int32 pContactGroupID)
        {
            try
            {
                tblContactGroupCrud crud = new tblContactGroupCrud();
                crud.Where("ContactGroupID", General.Operator.Equals, pContactGroupID);

                List<tblContactGroup> search = crud.ReadMulti().ToList();
                foreach (tblContactGroup del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteContactGroup - " + Ex.Message);
            }
        }
        public Boolean DeleteContactType(Int32 pContactTypeID)
        {
            try
            {
                tblContactTypeCrud crud = new tblContactTypeCrud();
                crud.Where("ContactTypeID", General.Operator.Equals, pContactTypeID);

                List<tblContactType> search = crud.ReadMulti().ToList();
                foreach (tblContactType del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteContactType - " + Ex.Message);
            }
        }
        public Boolean DeleteContactPerson(Int32 pContactPersonID)
        {
            try
            {
                #region Delete Contact Detail
                List<tblContactDetail> contactDetails = GetContactDetailPerContactPersonID(pContactPersonID);
                if (contactDetails == null)
                {
                    return false;
                }

                foreach (tblContactDetail contactDetail in contactDetails)
                {
                    if (!DeleteContactDetail(Convert.ToInt32(contactDetail.ContactDetailID)))
                    {
                        return false;
                    }
                }
                #endregion

                #region Delete Contact Groups
                List<tblContactPersonGroup> contactGroups = GetPersonContactGroup(pContactPersonID);
                if (contactGroups == null)
                {
                    return false;
                }

                foreach (tblContactPersonGroup contactGroup in contactGroups)
                {
                    if (!DeleteContactPersonGroup(contactGroup.PersonGroupID))
                    {
                        return false;
                    }
                }
				#endregion

				tblContactPersonCrud crud = new tblContactPersonCrud();
				crud.Where("ContactPersonID", General.Operator.Equals, pContactPersonID);
				tblContactPerson person = crud.ReadSingle();
				crud.Delete(person);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteContactPerson - " + Ex.Message);
            }

        }
        public Boolean SaveContactGroup(tblContactGroup pContactGroup)
        {
            try
            {
				throw new Exception("Code this!");
            }
            catch (Exception Ex)
            {
                throw new Exception("SaveContactGrouptblContactGroup - " + Ex.Message);
            }
        }
        public Boolean SaveContactType(tblContactType pContactType)
        {
            try
            {
				throw new Exception("Code this!");
			}
            catch (Exception Ex)
            {
                throw new Exception("SaveContactTypetblContactType - " + Ex.Message);
            }
        }
        public Boolean SaveContactPerson(tblContactPerson pContactPerson, List<tblContactPersonGroup> pContactPersonGroup)
        {
            try
            {
                Int32 iContactPersonID = 0;
                if (pContactPerson.ContactName != null)
                    iContactPersonID = SaveContactPerson(pContactPerson);


                if (iContactPersonID == -1)
                {
                    return false;
                }

                //Delete any existing contact groups and insert the new list
                Int32 iContactPersonIDDelete = 0;
                if (pContactPersonGroup.Count > 0)
                {
                    tblContactPersonGroup workGroup = (tblContactPersonGroup)pContactPersonGroup[0];
                    iContactPersonIDDelete = Convert.ToInt32(workGroup.ContactPersonID);

                    List<tblContactPersonGroup> groupDeletes = GetPersonContactGroup(iContactPersonIDDelete);
                    foreach (tblContactPersonGroup groupDelete in groupDeletes)
                    {
                        if (!DeleteContactPersonGroup(groupDelete.PersonGroupID))
                        {
                            return false;
                        }
                    }
                }
                foreach (tblContactPersonGroup group in pContactPersonGroup)
                {
                    if (iContactPersonID == 0) iContactPersonID = iContactPersonIDDelete;
                    group.ContactPersonID = iContactPersonID;
                    if (SaveContactPersonGroup(group) == -1)
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (Exception Ex)
            {
				throw Ex;

            }

        }
        public Boolean DeleteContactPersonGroup(int? pPersonGroupID)
        {
            try
            {
                tblContactPersonGroupCrud crud = new tblContactPersonGroupCrud();
                crud.Where("PersonGroupID", General.Operator.Equals, (Int32)pPersonGroupID);

                List<tblContactPersonGroup> search = crud.ReadMulti().ToList();
                foreach (tblContactPersonGroup del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteContactPersonGroupint? - " + Ex.Message);
            }
        }
        public Int32 SaveContactPersonGroup(tblContactPersonGroup pContactPersonGroup)
        {
            try
            {
				tblContactPersonGroupCrud crud = new tblContactPersonGroupCrud();
				long ID = 0;
				crud.Insert(pContactPersonGroup, out ID);
				return (Int32)ID;
            }
            catch (Exception Ex)
            {
				throw new Exception(Ex.Message);
            }
        }
		public Int32 SaveContactPerson(tblContactPerson pContactPerson)
		{
			try
			{
				tblContactPersonCrud crud = new tblContactPersonCrud();
				long ID;
				crud.Insert(pContactPerson, out ID);
				return (Int32)ID;
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
        public Boolean DeleteContactDetail(Int32 pContactDetailID)
        {
            try
            {
                tblContactDetailCrud crud = new tblContactDetailCrud();
                crud.Where("ContactDetailID", General.Operator.Equals, pContactDetailID);

                List<tblContactDetail> search = crud.ReadMulti().ToList();
                foreach (tblContactDetail del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteContactDetail - " + Ex.Message);
            }
        }
        public Boolean SaveContactDetail(tblContactDetail pContactDetail)
        {
            try
            {
				tblContactDetailCrud crud = new tblContactDetailCrud();
				crud.Insert(pContactDetail);

                return true;
            }
            catch (Exception Ex)
            {
				throw Ex;
            }
        }
        public Int32 SaveMailTemplate(String pUserName, String pTemplate, String pTemplateName, String pMailSubject)
        {
            try
            {
                tblContactMailTemplateCrud crud = new tblContactMailTemplateCrud();
                tblContactMailTemplate workRec = new tblContactMailTemplate();
                workRec.Template = pTemplate;
                workRec.TemplateName = pTemplateName;
                workRec.MailSubject = pMailSubject;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("SaveMailTemplate - " + Ex.Message);
            }
        }
        public Int32 SaveMailTemplate(String pUserName, String pTemplate, String pTemplateName, String pTemplateSubject, Int32 pMailTemplateID)
        {
			try
			{ 
				tblContactMailTemplate mailTemplate = GetMailTemplate(pMailTemplateID);
				mailTemplate.Template = pTemplate;
				mailTemplate.TemplateName = pTemplateName;
				mailTemplate.MailSubject = pTemplateSubject;
				tblContactMailTemplateCrud crud = new tblContactMailTemplateCrud();
				long ID = 0;

				crud.Insert(mailTemplate, out ID);

				return (Int32)ID;
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
    }
}
