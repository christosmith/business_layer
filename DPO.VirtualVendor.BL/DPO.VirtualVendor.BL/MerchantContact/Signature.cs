﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;

namespace DPO.VirtualVendor.BL
{
public partial class VirtualVendorBL
    {
        public Int32 InsertContactSignature(Int32 pUserID, String pSignatureDescription, String pSender, Byte[] pMailSignature)
        {
            try
            {
                tblContactSignature sign = new tblContactSignature();
                sign.MailSignature = pMailSignature;
                sign.Sender = pSender;
                sign.SignatureDescription = pSignatureDescription;
                sign.UserID = pUserID;
                tblContactSignatureCrud crud = new tblContactSignatureCrud();
                long ID = 0;
                crud.Insert(sign, out ID);

                return (Int32)ID;


            }
            catch (Exception Ex)
            {
                throw new Exception("InsertContactSignature - " + Ex.Message);
            }
        }
        public Boolean DeleteContactSignature(Int32 pSignatureID)
        {
            try
            {
                tblContactSignature sign = GetContactSignature(pSignatureID);
                tblContactSignatureCrud crud = new tblContactSignatureCrud();
                crud.Delete(sign);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteContactSignature - " + Ex.Message);
            }
        }
        public tblContactSignature GetContactSignature(Int32 pSignatureID)
        {
            try
            {
                tblContactSignatureCrud crud = new tblContactSignatureCrud();
                crud.Where("SignatureID", General.Operator.Equals, pSignatureID);
                recordFound = false;
                tblContactSignature sign = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return sign;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactSignature - " + Ex.Message);
            }
        }
        public List<tblContactSignature> GetContactSignature()
        {
            try
            {
                recordFound = false;
                tblContactSignatureCrud crud = new tblContactSignatureCrud();
                List<tblContactSignature> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetContactSignature - " + Ex.Message);
            }
        }
        public tblContactSignature GetSignature(Int32 pSignatureID)
        {
            try
            {
                recordFound = false;
                tblContactSignatureCrud crud = new tblContactSignatureCrud();
                crud.Where("SignatureID", General.Operator.Equals, pSignatureID);

                tblContactSignature search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetSignature - " + Ex.Message);
            }
        }
        public tblContactSignature GetSignature(String pUserName)
        {
            vtUsers user = GetVTUser(pUserName);
			tblContactSignatureCrud crud = new tblContactSignatureCrud();
			crud.Where("UserID", General.Operator.Equals, (Int32)user.AutoId);
			List<tblContactSignature> results = crud.ReadMulti().ToList();
            if (results.Count > 0)
                return results[0];
            else
                return null;
        }
        public String GetSignaturesGridData(String pUserName)
        {
			try
			{
				List<tblContactSignature> signatures = GetSignatures(pUserName);
				String divString = "";
				foreach (tblContactSignature signature in signatures)
				{
					divString += signature.SignatureID + "═";
					divString += signature.SignatureDescription + "║";
				}

				if (divString.Length > 0)
					divString = divString.Substring(0, divString.Length - 1);
				return divString;
			}
            catch (Exception Ex)
            {
                throw new Exception("GetSignaturesGridData - " + Ex.Message);
            }
        }
        public List<tblContactSignature> GetSignatures(String pUserName)
        {
			try
			{
				vtUsers user = GetVTUser(pUserName);
				List<tblContactSignature> search = new List<tblContactSignature>();
				tblContactSignatureCrud crud = new tblContactSignatureCrud();
				crud.Where("UserID", General.Operator.Equals, (Int32)user.AutoId);
				search = crud.ReadMulti().ToList();
				return search;
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
    }
}
