﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using System.Net;
using System.Xml;

namespace DPO.VirtualVendor.BL
{

public partial class VirtualVendorBL
	{
		public List<tblMobiCredRefundReason> GetMobiCredRefundReason()
        {
            try
            {
                recordFound = false;
                tblMobiCredRefundReasonCrud crud = new tblMobiCredRefundReasonCrud();
                List<tblMobiCredRefundReason> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMobiCredRefundReason - " + Ex.Message);
            }
        }
		public String GetMobiCredRefundReasonGridData()
		{
			try
			{
				List<tblMobiCredRefundReason> reasons = GetMobiCredRefundReason();
				String divString = "";
				foreach (tblMobiCredRefundReason reason in reasons)
				{
					divString += reason.RefundReasonID + "═";
					divString += reason.RefundReasonCode + "═";
					divString += reason.RefundReason + "║";
				}

				if (divString.Length > 0)
					divString = divString.Substring(0, divString.Length - 1);

				return divString;

			}
            catch (Exception Ex)
            {
                throw new Exception("GetMobiCredRefundReasonGridData - " + Ex.Message);
            }
        }
		public tblMobiCredResponse GetMobiCredResponse(String pResponseCode)
        {
            try
            {
                recordFound = false;
                tblMobiCredResponseCrud crud = new tblMobiCredResponseCrud();
                crud.Where("ResponseCode", General.Operator.Equals, pResponseCode);

                tblMobiCredResponse search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMobiCredResponse - " + Ex.Message);
            }
        }
		public tblMobiCredTransactionType GetMobiCredTransactionType(String pTransactionType)
        {
            try
            {
                recordFound = false;
                tblMobiCredTransactionTypeCrud crud = new tblMobiCredTransactionTypeCrud();
                crud.Where("TransactionType", General.Operator.Equals, pTransactionType);

                tblMobiCredTransactionType search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMobiCredTransactionType - " + Ex.Message);
            }
        }
		public Int32 InsertMobiCredTransaction(Byte pTransactionTypeID, String pAuthentication, String pMCReference, Int32 pRequestID, Byte pResponseCodeID, String pUserID, String pReferenceNumber)
        {
            try
            {
                tblMobiCredTransactionCrud crud = new tblMobiCredTransactionCrud();
                tblMobiCredTransaction workRec = new tblMobiCredTransaction();
                workRec.Authentication = pAuthentication;
                workRec.MCReference = pMCReference;
                workRec.RequestID = pRequestID;
                workRec.ResponseCodeID = pResponseCodeID;
                workRec.UserID = pUserID;
                workRec.ReferenceNumber = pReferenceNumber;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertMobiCredTransactionByte - " + Ex.Message);
            }
        }
		public String PostToUrl(String pUrl, String pParameter)
		{
			try
			{
				System.Net.HttpWebRequest xmlServerHttpRequest;

				xmlServerHttpRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(pUrl);
				xmlServerHttpRequest.ContentType = "application/x-www-form-urlencoded";
				xmlServerHttpRequest.Method = "POST";
				ASCIIEncoding encoding = new ASCIIEncoding();
				Byte[] data = encoding.GetBytes(pParameter);
				xmlServerHttpRequest.ContentLength = data.Length;
				xmlServerHttpRequest.AllowAutoRedirect = false;
				xmlServerHttpRequest.UserAgent = "Virtual Card Services";

				System.IO.Stream HTTPStream = xmlServerHttpRequest.GetRequestStream();
				HTTPStream.Write(data, 0, data.Length);
				HTTPStream.Close();


				String responseText = "";
				HttpWebResponse HTTPResponse = (System.Net.HttpWebResponse)xmlServerHttpRequest.GetResponse();
				if (HTTPResponse.StatusCode == HttpStatusCode.OK || HTTPResponse.StatusCode == HttpStatusCode.MovedPermanently || HTTPResponse.StatusCode == HttpStatusCode.Found)
				{
					System.IO.Stream str = HTTPResponse.GetResponseStream();
					System.IO.StreamReader sR = new System.IO.StreamReader(str, System.Text.Encoding.UTF8);
					responseText = sR.ReadToEnd();
				}

				return responseText;
			}
			catch (Exception Ex)
			{
				throw new Exception("PostToUrl-" + Ex.Message);
			}
		}	
		public VCSResponse MobiCredPurchasePreAuthorise(String pUserID, String pOtp, String pMCReference, MobiCredInformation pMobiCredInformation)
		{
			try
			{
				VCSResponse response = new VCSResponse();
				MobiCredInformation mobiInfo = pMobiCredInformation;
				String parameter = "rqDataMode=VAR/XML&rqAuthentication=User:" + mobiInfo.userName + "|" + mobiInfo.password + "|GSMUS||%26login_company_obj%3D-1%26login_company_branch_obj%3D-1%26process_date%3D" + mobiInfo.processDate + "&rqservice=ilDataService:purPreAuth&cMerchantID=" + mobiInfo.merchantID + "&cMerchantKey=" + mobiInfo.merchantKey + "&cMerchantRequestID=" + mobiInfo.requestID.ToString() + "&cMCReference=" + pMCReference + "&iOTP=" + pOtp;


				String returnData = PostToUrl(mobiInfo.url, parameter);
				XmlDocument mobiCredData = new XmlDocument();
				mobiCredData.LoadXml(returnData);

				String rqAuthentication = "";
				String pcMCReference = "";
				String pcMerchantRequestID = "";
				String piResponseCode = "";
				String pcReason = "";
				String pcStatus = "";

				if (mobiCredData.GetElementsByTagName("rqAuthentication").Count > 0)
					if (mobiCredData.GetElementsByTagName("rqAuthentication")[0].ChildNodes.Count > 0)
						rqAuthentication = mobiCredData.GetElementsByTagName("rqAuthentication")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcMCReference").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcMCReference")[0].ChildNodes.Count > 0)
						pcMCReference = mobiCredData.GetElementsByTagName("pcMCReference")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcMerchantRequestID").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcMerchantRequestID")[0].ChildNodes.Count > 0)
						pcMerchantRequestID = mobiCredData.GetElementsByTagName("pcMerchantRequestID")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("piResponseCode").Count > 0)
				{
					if (mobiCredData.GetElementsByTagName("piResponseCode")[0].ChildNodes.Count > 0)
						piResponseCode = mobiCredData.GetElementsByTagName("piResponseCode")[0].ChildNodes[0].Value;
				}
				else
				{
					if (mobiCredData.GetElementsByTagName("rqErrorMessage").Count > 0)
						if (mobiCredData.GetElementsByTagName("rqErrorMessage")[0].ChildNodes.Count > 0)
						{
							String errorMessage = mobiCredData.GetElementsByTagName("rqErrorMessage")[0].ChildNodes[0].Value;
							piResponseCode = "999";
							pcReason = errorMessage;
						}
				}

				if (mobiCredData.GetElementsByTagName("pcReason").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcReason")[0].ChildNodes.Count > 0)
						pcReason = mobiCredData.GetElementsByTagName("pcReason")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcStatus").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcStatus")[0].ChildNodes.Count > 0)
						pcStatus = mobiCredData.GetElementsByTagName("pcStatus")[0].ChildNodes[0].Value;

				tblMobiCredTransactionType tranType = GetMobiCredTransactionType("Purchase Pre Authorise");
				tblMobiCredResponse mobiCredResponse = GetMobiCredResponse(piResponseCode.PadLeft(3, '0'));
				tblMobiCredTransaction mobiCredTran = GetMobiCredTransaction(pUserID, pMCReference);
				String referenceNumber = "";
				if (RecordFound)
					referenceNumber = mobiCredTran.ReferenceNumber;

				InsertMobiCredTransaction(Convert.ToByte(tranType.TransactionTypeID), rqAuthentication, pcMCReference, mobiInfo.requestID, Convert.ToByte(mobiCredResponse.ResponseCodeID), pUserID, referenceNumber);

				if (pcStatus == "Approved")
				{
					response.VCSResponseCode = "0000";
					response.VCSResponseMessage = pcReason;
				}
				else
				{
					response.VCSResponseCode = piResponseCode;
					response.VCSResponseMessage = pcReason;
				}
				return response;
			}
			catch (Exception Ex)
			{
				throw new Exception("MobiCredPurchasePreAuthorise-" + Ex.Message);
			}
		}
		public VCSResponse MobiCredResendOTP(String pUserID, String pMCReference, MobiCredInformation pMobiCredInformation)
		{
			try
			{
				VCSResponse response = new VCSResponse();
				MobiCredInformation mobiInfo = pMobiCredInformation;
				String parameter = "rqDataMode=VAR/XML&rqAuthentication=User:" + mobiInfo.userName + "|" + mobiInfo.password + "|GSMUS||%26login_company_obj%3D-1%26login_company_branch_obj%3D-1%26process_date%3D" + mobiInfo.processDate + "&rqservice=ilDataService:purOTP&cMerchantID=" + mobiInfo.merchantID + "&cMerchantKey=" + mobiInfo.merchantKey + "&cMerchantRequestID=" + mobiInfo.requestID.ToString() + "&cMCReference=" + pMCReference;

				String returnData = PostToUrl(mobiInfo.url, parameter);
				XmlDocument mobiCredData = new XmlDocument();
				mobiCredData.LoadXml(returnData);

				String rqAuthentication = "";
				String pcMCReference = "";
				String pcMerchantRequestID = "";
				String piResponseCode = "";
				String pcReason = "";
				String pcStatus = "";
				String errorMessage = "";
				if (mobiCredData.GetElementsByTagName("rqAuthentication").Count > 0)
					if (mobiCredData.GetElementsByTagName("rqAuthentication")[0].ChildNodes.Count > 0)
						rqAuthentication = mobiCredData.GetElementsByTagName("rqAuthentication")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcMCReference").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcMCReference")[0].ChildNodes.Count > 0)
						pcMCReference = mobiCredData.GetElementsByTagName("pcMCReference")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcMerchantRequestID").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcMerchantRequestID")[0].ChildNodes.Count > 0)
						pcMerchantRequestID = mobiCredData.GetElementsByTagName("pcMerchantRequestID")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("piResponseCode").Count > 0)
				{
					if (mobiCredData.GetElementsByTagName("piResponseCode")[0].ChildNodes.Count > 0)
						piResponseCode = mobiCredData.GetElementsByTagName("piResponseCode")[0].ChildNodes[0].Value;
				}
				else
				{
					if (mobiCredData.GetElementsByTagName("rqErrorMessage").Count > 0)
						if (mobiCredData.GetElementsByTagName("rqErrorMessage")[0].ChildNodes.Count > 0)
						{
							errorMessage = mobiCredData.GetElementsByTagName("rqErrorMessage")[0].ChildNodes[0].Value;
							piResponseCode = "999";
							pcReason = errorMessage;
						}
				}
				if (mobiCredData.GetElementsByTagName("pcReason").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcReason")[0].ChildNodes.Count > 0)
						pcReason = mobiCredData.GetElementsByTagName("pcReason")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcStatus").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcStatus")[0].ChildNodes.Count > 0)
						pcStatus = mobiCredData.GetElementsByTagName("pcStatus")[0].ChildNodes[0].Value;

				tblMobiCredTransactionType tranType = GetMobiCredTransactionType("Resend OTP");
				tblMobiCredResponse mobiCredResponse = GetMobiCredResponse(piResponseCode.PadLeft(3, '0'));
				tblMobiCredTransaction mobiCredTran = GetMobiCredTransaction(pUserID, pMCReference);
				String referenceNumber = "";
				if (RecordFound)
					referenceNumber = mobiCredTran.ReferenceNumber;

				InsertMobiCredTransaction(Convert.ToByte(tranType.TransactionTypeID), rqAuthentication, pcMCReference, mobiInfo.requestID, Convert.ToByte(mobiCredResponse.ResponseCodeID), pUserID, referenceNumber);

				if (pcStatus == "Pending")
				{
					response.VCSResponseCode = "0000";
					response.VCSResponseMessage = pcReason;
					response.ReturnValue = pcMCReference;
				}
				else
				{
					response.VCSResponseCode = piResponseCode;
					response.VCSResponseMessage = pcReason;
				}
				return response;

			}
			catch (Exception Ex)
			{
				throw new Exception("MobiCredResendOTP-" + Ex.Message);
			}
			
		}
		internal Boolean WriteLog(String pSystem, String pApplication, String pMessage, eLogType pLogType)
		{
			try
			{
				BackOfficeWcf.iBackOfficeClient backOFficeClient = new BackOfficeWcf.iBackOfficeClient();
				BackOfficeWcf.eLogType logType = new BackOfficeWcf.eLogType();
				switch (pLogType)
				{
					case eLogType.Debug: logType = BackOfficeWcf.eLogType.Debug; break;
					case eLogType.Error: logType = BackOfficeWcf.eLogType.Error; break;
					case eLogType.Information: logType = BackOfficeWcf.eLogType.Information; break;
					case eLogType.Warning: logType = BackOfficeWcf.eLogType.Warning; break;
				}
				return backOFficeClient.InsertLog(pSystem, pApplication, pMessage, logType);
			}
			catch (Exception Ex)
			{
				throw new Exception("WriteLog-" + Ex.Message);
			}
		}
		public VCSResponse MobiCredRefund(String pUserID, String pMCReference, Decimal pAmount, String pReason, MobiCredInformation pMobiCredInformation)
		{
			try
			{
				VCSResponse response = new VCSResponse();
				MobiCredInformation mobiInfo = pMobiCredInformation;
				String parameter = "rqDataMode=VAR/XML&rqAuthentication=User:" + mobiInfo.userName + "|" + mobiInfo.password + "|GSMUS||%26login_company_obj%3D-1%26login_company_branch_obj%3D-1%26process_date%3D" + mobiInfo.processDate + "&rqservice=ilDataService:purRefund&cMerchantID=" + mobiInfo.merchantID + "&cMerchantKey=" + mobiInfo.merchantKey + "&cMerchantRequestID=" + mobiInfo.requestID.ToString() + "&cMCReference=" + pMCReference + "&dAmount=" + pAmount.ToString() + "&cMerchantReason=" + pReason; 

				String returnData = PostToUrl(mobiInfo.url, parameter);
				XmlDocument mobiCredData = new XmlDocument();
				mobiCredData.LoadXml(returnData);

				String rqAuthentication = "";
				String pcMCReference = "";
				String pcMerchantRequestID = "";
				String piResponseCode = "";
				String pcReason = "";
				String pcStatus = "";
				String errorMessage = "";
				if (mobiCredData.GetElementsByTagName("rqAuthentication").Count > 0)
					if (mobiCredData.GetElementsByTagName("rqAuthentication")[0].ChildNodes.Count > 0)
						rqAuthentication = mobiCredData.GetElementsByTagName("rqAuthentication")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcMCReference").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcMCReference")[0].ChildNodes.Count > 0)
						pcMCReference = mobiCredData.GetElementsByTagName("pcMCReference")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcMerchantRequestID").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcMerchantRequestID")[0].ChildNodes.Count > 0)
						pcMerchantRequestID = mobiCredData.GetElementsByTagName("pcMerchantRequestID")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("piResponseCode").Count > 0)
				{
					if (mobiCredData.GetElementsByTagName("piResponseCode")[0].ChildNodes.Count > 0)
						piResponseCode = mobiCredData.GetElementsByTagName("piResponseCode")[0].ChildNodes[0].Value;
				}
				else
				{
					if (mobiCredData.GetElementsByTagName("rqErrorMessage").Count > 0)
						if (mobiCredData.GetElementsByTagName("rqErrorMessage")[0].ChildNodes.Count > 0)
						{
							errorMessage = mobiCredData.GetElementsByTagName("rqErrorMessage")[0].ChildNodes[0].Value;
							piResponseCode = "999";
							pcReason = errorMessage;
						}
				}
				if (mobiCredData.GetElementsByTagName("pcReason").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcReason")[0].ChildNodes.Count > 0)
						pcReason = mobiCredData.GetElementsByTagName("pcReason")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcStatus").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcStatus")[0].ChildNodes.Count > 0)
						pcStatus = mobiCredData.GetElementsByTagName("pcStatus")[0].ChildNodes[0].Value;

				tblMobiCredTransactionType tranType = GetMobiCredTransactionType("Purchase Refund");
				tblMobiCredResponse mobiCredResponse = GetMobiCredResponse(piResponseCode.PadLeft(3, '0'));
				if (!recordFound)
				{
					WriteLog("Virtual Online", "MobiCred", "Response (" + piResponseCode + ") not recognised.", eLogType.Error);

				}
				else
				{
					tblMobiCredTransaction mobiCredTran = GetMobiCredTransaction(pUserID, pMCReference);
					String referenceNumber = "";
					if (RecordFound)
						referenceNumber = mobiCredTran.ReferenceNumber;

					InsertMobiCredTransaction(Convert.ToByte(tranType.TransactionTypeID), rqAuthentication, pcMCReference, mobiInfo.requestID, Convert.ToByte(mobiCredResponse.ResponseCodeID), pUserID, referenceNumber);
				}

				if (pcStatus == "Pending")
				{
					response.VCSResponseCode = "0000";
					response.VCSResponseMessage = pcReason;
					response.ReturnValue = pcMCReference;
				}
				else
				{
					response.VCSResponseCode = piResponseCode;
					response.VCSResponseMessage = pcReason;
				}
				return response;
			}
			catch (Exception Ex)
			{
				throw new Exception("MobiCredRefund-" + Ex.Message);
			}
		}
		public VCSResponse MobiCredApprove(String pUserID, String pMCReference, MobiCredInformation pMobiCredInformation)
		{
			try
			{
				VCSResponse response = new VCSResponse();
				MobiCredInformation mobiInfo = pMobiCredInformation;
				String parameter = "rqDataMode=VAR/XML&rqAuthentication=User:" + mobiInfo.userName + "|" + mobiInfo.password + "|GSMUS||%26login_company_obj%3D-1%26login_company_branch_obj%3D-1%26process_date%3D" + mobiInfo.processDate + "&rqservice=ilDataService:purApprove&cMerchantID=" + mobiInfo.merchantID + "&cMerchantKey=" + mobiInfo.merchantKey + "&cMerchantRequestID=" + mobiInfo.requestID.ToString() + "&cMCReference=" + pMCReference;

				String returnData = PostToUrl(mobiInfo.url, parameter);
				XmlDocument mobiCredData = new XmlDocument();
				mobiCredData.LoadXml(returnData);

				String rqAuthentication = "";
				String pcMCReference = "";
				String pcMerchantRequestID = "";
				String piResponseCode = "";
				String pcReason = "";
				String pcStatus = "";
				String errorMessage = "";
				if (mobiCredData.GetElementsByTagName("rqAuthentication").Count > 0)
					if (mobiCredData.GetElementsByTagName("rqAuthentication")[0].ChildNodes.Count > 0)
						rqAuthentication = mobiCredData.GetElementsByTagName("rqAuthentication")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcMCReference").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcMCReference")[0].ChildNodes.Count > 0)
						pcMCReference = mobiCredData.GetElementsByTagName("pcMCReference")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcMerchantRequestID").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcMerchantRequestID")[0].ChildNodes.Count > 0)
						pcMerchantRequestID = mobiCredData.GetElementsByTagName("pcMerchantRequestID")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("piResponseCode").Count > 0)
				{
					if (mobiCredData.GetElementsByTagName("piResponseCode")[0].ChildNodes.Count > 0)
						piResponseCode = mobiCredData.GetElementsByTagName("piResponseCode")[0].ChildNodes[0].Value;
				}
				else
				{
					if (mobiCredData.GetElementsByTagName("rqErrorMessage").Count > 0)
						if (mobiCredData.GetElementsByTagName("rqErrorMessage")[0].ChildNodes.Count > 0)
						{
							errorMessage = mobiCredData.GetElementsByTagName("rqErrorMessage")[0].ChildNodes[0].Value;
							piResponseCode = "999";
							pcReason = errorMessage;
						}
				}
				if (mobiCredData.GetElementsByTagName("pcReason").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcReason")[0].ChildNodes.Count > 0)
						pcReason = mobiCredData.GetElementsByTagName("pcReason")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcStatus").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcStatus")[0].ChildNodes.Count > 0)
						pcStatus = mobiCredData.GetElementsByTagName("pcStatus")[0].ChildNodes[0].Value;

				tblMobiCredTransactionType tranType = GetMobiCredTransactionType("Resend OTP");
				tblMobiCredResponse mobiCredResponse = GetMobiCredResponse(piResponseCode.PadLeft(3, '0'));
				tblMobiCredTransaction mobiCredTran = GetMobiCredTransaction(pUserID, pMCReference);
				String referenceNumber = "";
				if (RecordFound)
					referenceNumber = mobiCredTran.ReferenceNumber;

				InsertMobiCredTransaction(Convert.ToByte(tranType.TransactionTypeID), rqAuthentication, pcMCReference, mobiInfo.requestID, Convert.ToByte(mobiCredResponse.ResponseCodeID), pUserID, referenceNumber);

				if (pcStatus == "Approved")
				{
					response.VCSResponseCode = "0000";
					response.VCSResponseMessage = pcReason;
					response.ReturnValue = pcMCReference;
				}
				else
				{
					response.VCSResponseCode = piResponseCode;
					response.VCSResponseMessage = pcReason;
				}
				return response;

			}
			catch (Exception Ex)
			{
				throw new Exception("MobiCredResendOTP-" + Ex.Message);
			}

		}
		public VCSResponse MobiCredQuery(String pUserID, String pMCReference, MobiCredInformation pMobiCredInformation)
		{
			try
			{
				VCSResponse response = new VCSResponse();
				MobiCredInformation mobiInfo = pMobiCredInformation;
				String parameter = "rqDataMode=VAR/XML&rqAuthentication=User:" + mobiInfo.userName + "|" + mobiInfo.password + "|GSMUS||%26login_company_obj%3D-1%26login_company_branch_obj%3D-1%26process_date%3D" + mobiInfo.processDate + "&rqservice=ilDataService:purQuery&cMerchantID=" + mobiInfo.merchantID + "&cMerchantKey=" + mobiInfo.merchantKey + "&cMCReference=" + pMCReference;

				String returnData = PostToUrl(mobiInfo.url, parameter);
				XmlDocument mobiCredData = new XmlDocument();
				mobiCredData.LoadXml(returnData);

				String rqAuthentication = "";
				String pcMCReference = "";
				String piResponseCode = "";
				String pcReason = "";
				String pcStatus = "";
				String errorMessage = "";
				if (mobiCredData.GetElementsByTagName("rqAuthentication").Count > 0)
					if (mobiCredData.GetElementsByTagName("rqAuthentication")[0].ChildNodes.Count > 0)
						rqAuthentication = mobiCredData.GetElementsByTagName("rqAuthentication")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcMCReference").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcMCReference")[0].ChildNodes.Count > 0)
						pcMCReference = mobiCredData.GetElementsByTagName("pcMCReference")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("piResponseCode").Count > 0)
				{
					if (mobiCredData.GetElementsByTagName("piResponseCode")[0].ChildNodes.Count > 0)
						piResponseCode = mobiCredData.GetElementsByTagName("piResponseCode")[0].ChildNodes[0].Value;
				}
				else
				{
					if (mobiCredData.GetElementsByTagName("rqErrorMessage").Count > 0)
						if (mobiCredData.GetElementsByTagName("rqErrorMessage")[0].ChildNodes.Count > 0)
						{
							errorMessage = mobiCredData.GetElementsByTagName("rqErrorMessage")[0].ChildNodes[0].Value;
							piResponseCode = "999";
							pcReason = errorMessage;
						}
				}
				if (mobiCredData.GetElementsByTagName("pcReason").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcReason")[0].ChildNodes.Count > 0)
						pcReason = mobiCredData.GetElementsByTagName("pcReason")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcStatus").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcStatus")[0].ChildNodes.Count > 0)
						pcStatus = mobiCredData.GetElementsByTagName("pcStatus")[0].ChildNodes[0].Value;

				String pcPurchaseState = "";

				if (mobiCredData.GetElementsByTagName("pcPurchaseState").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcPurchaseState")[0].ChildNodes.Count > 0)
						pcPurchaseState = mobiCredData.GetElementsByTagName("pcPurchaseState")[0].ChildNodes[0].Value;

				String pdTranAmt = "";
				if (mobiCredData.GetElementsByTagName("pdTranAmt").Count > 0)
					if (mobiCredData.GetElementsByTagName("pdTranAmt")[0].ChildNodes.Count > 0)
						pdTranAmt = mobiCredData.GetElementsByTagName("pdTranAmt")[0].ChildNodes[0].Value;

				String pdPurchaseBal = "";
				if (mobiCredData.GetElementsByTagName("pdPurchaseBal").Count > 0)
					if (mobiCredData.GetElementsByTagName("pdPurchaseBal")[0].ChildNodes.Count > 0)
						pdPurchaseBal = mobiCredData.GetElementsByTagName("pdPurchaseBal")[0].ChildNodes[0].Value;

				pcReason = "PurchaseState=" + pcPurchaseState + "|TransactionAmount=" + pdTranAmt + "|Balance=" + pdPurchaseBal;
				tblMobiCredTransactionType tranType = GetMobiCredTransactionType("Purchase Query");
				tblMobiCredResponse mobiCredResponse = GetMobiCredResponse(piResponseCode.PadLeft(3, '0'));
				if (!recordFound)
				{
					WriteLog("Virtual Online", "MobiCred", "Response (" + piResponseCode + ") not recognised.", eLogType.Error);

				}
				else
				{
					tblMobiCredTransaction mobiCredTran = GetMobiCredTransaction(pUserID, pMCReference);
					String referenceNumber = "";
					if (RecordFound)
						referenceNumber = mobiCredTran.ReferenceNumber;

					InsertMobiCredTransaction(Convert.ToByte(tranType.TransactionTypeID), rqAuthentication, pcMCReference, mobiInfo.requestID, Convert.ToByte(mobiCredResponse.ResponseCodeID), pUserID, referenceNumber);
				}

				if (pcStatus == "Success")
				{
					response.VCSResponseCode = "0000";
					response.VCSResponseMessage = pcReason;
					response.ReturnValue = pcMCReference;
				}
				else
				{
					response.VCSResponseCode = piResponseCode;
					response.VCSResponseMessage = pcReason;
				}
				return response;
			}
			catch (Exception Ex)
			{
				throw new Exception("MobiCredQuery-" + Ex.Message);
			}
		}
        public tblMobiCredTransaction GetMobiCredTransactionByVCSRefNo(String pUserID, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                tblMobiCredTransactionCrud crud = new tblMobiCredTransactionCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                tblMobiCredTransaction search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMobiCredTransactionByVCSRefNo - " + Ex.Message);
            }
        }
		public tblMobiCredTransaction GetMobiCredTransaction(String pUserID, String pMCReference)
        {
            try
            {
                recordFound = false;
                tblMobiCredTransactionCrud crud = new tblMobiCredTransactionCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("MCReference", General.Operator.Equals, pMCReference);

                tblMobiCredTransaction search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetMobiCredTransaction - " + Ex.Message);
            }
        }
		public Double MobiCredInstallment(Double pAmount, Double pInterestRate)
		{
			try
			{
				Double installment = 0;
				pInterestRate = pInterestRate / 100;
				installment = Microsoft.VisualBasic.Financial.Pmt((pInterestRate / 12), 12, pAmount) + 1;
				installment = installment * -1;
				return installment;
			}
			catch (Exception Ex)
			{
				throw new Exception("MobiCredInstallment - " + Ex.Message);
			}
		}
		public Double MobiCredTotalCredit(Double pInstallment)
		{
			try
			{
				return pInstallment * 12; 
			}
			catch (Exception Ex)
			{
				throw new Exception("MobiCredTotalCredit -" + Ex.Message);
			}
		}
		public VCSResponse MobiCredPurchaseCreate(String pUserID, String pReferenceNumber, String pClientEmailAddress, String pClientPassword, Decimal pAmount, MobiCredInformation pMobiCredInformation)
		{
			try
			{
				VCSResponse response = new VCSResponse();
				//MobiCredInformation mobiInfo = GetMobiCredInformation(pUserID);
				MobiCredInformation mobiInfo = pMobiCredInformation;

				String parameter = "rqDataMode=VAR/XML&rqAuthentication=User:" + mobiInfo.userName + "|" + mobiInfo.password + "|GSMUS||%26login_company_obj%3D-1%26login_company_branch_obj%3D-1%26process_date%3D" + mobiInfo.processDate + "&rqservice=ilDataService:purCreate&cMerchantID=" + mobiInfo.merchantID + "&cMerchantKey=" + mobiInfo.merchantKey + "&cMerchantRequestID=" + mobiInfo.requestID.ToString() + "&cCustUsername=" + pClientEmailAddress + "&cCustPasswd=" + pClientPassword + "&lAutoApprove=TRUE&cOrderNo=" + pReferenceNumber + "&dAmount=" + pAmount.ToString().Replace(",", ".") + "&cCustomField=";

				String returnData= PostToUrl(mobiInfo.url, parameter);
				XmlDocument mobiCredData = new XmlDocument();
				mobiCredData.LoadXml(returnData);

				String rqAuthentication = "";
				String pcMCReference = "";
				String pcMerchantRequestID = "";
				String piResponseCode = "";
				String pcReason = "";
				String pcStatus = "";
				String errorMessage = "";
				if (mobiCredData.GetElementsByTagName("rqAuthentication").Count > 0)
					if (mobiCredData.GetElementsByTagName("rqAuthentication")[0].ChildNodes.Count > 0)
						rqAuthentication = mobiCredData.GetElementsByTagName("rqAuthentication")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcMCReference").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcMCReference")[0].ChildNodes.Count > 0)
						pcMCReference = mobiCredData.GetElementsByTagName("pcMCReference")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcMerchantRequestID").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcMerchantRequestID")[0].ChildNodes.Count > 0)
						pcMerchantRequestID = mobiCredData.GetElementsByTagName("pcMerchantRequestID")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("piResponseCode").Count > 0)
				{
					if (mobiCredData.GetElementsByTagName("piResponseCode")[0].ChildNodes.Count > 0)
						piResponseCode = mobiCredData.GetElementsByTagName("piResponseCode")[0].ChildNodes[0].Value;
				}
				else
				{
					if (mobiCredData.GetElementsByTagName("rqErrorMessage").Count > 0)
						if (mobiCredData.GetElementsByTagName("rqErrorMessage")[0].ChildNodes.Count > 0)
						{
							errorMessage = mobiCredData.GetElementsByTagName("rqErrorMessage")[0].ChildNodes[0].Value;
							piResponseCode = "999";
							pcReason = errorMessage;
						}
				}
				if (mobiCredData.GetElementsByTagName("pcReason").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcReason")[0].ChildNodes.Count > 0)
						pcReason = mobiCredData.GetElementsByTagName("pcReason")[0].ChildNodes[0].Value;

				if (mobiCredData.GetElementsByTagName("pcStatus").Count > 0)
					if (mobiCredData.GetElementsByTagName("pcStatus")[0].ChildNodes.Count > 0)
						pcStatus = mobiCredData.GetElementsByTagName("pcStatus")[0].ChildNodes[0].Value;

				tblMobiCredTransactionType tranType = GetMobiCredTransactionType("Purchase Create");
				tblMobiCredResponse mobiCredResponse = GetMobiCredResponse(piResponseCode.PadLeft(3, '0'));
				InsertMobiCredTransaction(Convert.ToByte(tranType.TransactionTypeID), rqAuthentication, pcMCReference, mobiInfo.requestID, Convert.ToByte(mobiCredResponse.ResponseCodeID), pUserID, pReferenceNumber);

				if (pcStatus == "Pending")
				{
					response.VCSResponseCode = "0000";
					response.VCSResponseMessage = pcReason;
					response.ReturnValue = pcMCReference;
				}
				else
				{
					response.VCSResponseCode = piResponseCode;
					response.VCSResponseMessage = pcReason;
				}
				return response;

			}
			catch (Exception Ex)
			{
				throw new Exception("MobiCredPurchaseCreate-" + Ex.Message);
			}
		}
    }
}
