﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;

namespace DPO.VirtualVendor.BL
{
public partial class VirtualVendorBL
    {
        public Boolean LogWCFIncomingTransaction(String pService, String pUserID, String pReferenceNumber, String pCallerIP, String pParameters, String pDetail)
        {
            try
            {
                StoredProcedure.vcs_LogWebserviceCalls(pService, pUserID, pReferenceNumber, pCallerIP, pParameters, pDetail);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("LogWCFIncomingTransaction - " + Ex.Message);
            }
        }
        public Boolean DeleteRecurTmpl(String pUserID, String pReferenceNumber)
        {
            try
            {
                RecurTmpl tmpl = GetRecurTmpl(pUserID, pReferenceNumber);
                RecurTmplCrud crud = new RecurTmplCrud();
                crud.Delete(tmpl);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteRecurTmpl - " + Ex.Message);
            }
        }
		public Boolean UpdateRecurTmpl(String pUserID, String pReferenceNumber, String pRecurType, String pCardType, String pCardNumber, String pCvc, String pCardHolderName, String pEmailAddress, String @pExpiryMM, String pExpityYY, String pAmountInd, String pVaBalanceInd, decimal pAmount, String pPercentage, String pDescOfGoods, DateTime pPresented, String pTransactionType, String pStartDate, String pEndDate, String pOccurCount, String pFrequency, String pSuspended, DateTime pSuspendedDate, String pDeleted, String pApoAccountNumber, String pApoMcmCardNumber, String pApoMblCardNumber, String pUserName, String pModifiedUserName, String pMpars, String pCustomerID, String pSpreedlyID)
		{
			try
			{
				StoredProcedure.vcs_UpdateRecurTmpl(pUserID, pReferenceNumber, pRecurType, pCardType, pCardNumber, pCvc, pCardHolderName, pEmailAddress, pExpiryMM, pExpityYY, pAmountInd, pVaBalanceInd, pAmount, pPercentage, pDescOfGoods, pPresented, pTransactionType, pStartDate, pEndDate, pOccurCount, pFrequency, pSuspended, pSuspendedDate, pDeleted, pApoAccountNumber, pApoMcmCardNumber, pApoMblCardNumber, pUserName, DateTime.Now, pModifiedUserName, pMpars, pCustomerID, pSpreedlyID);
				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("UpdateRecurTmpl-" + Ex.Message);
			}
		}
        public Boolean UpdateRecurTmpl(String pUserID, String pReferenceNumber, String pSuspended)
        {
            try
            {
                if (pSuspended.ToUpper() != "N" && pSuspended.ToUpper() != "Y")
                    throw new Exception ("Suspended field should be Y or N");

                RecurTmpl recur = GetRecurTmpl(pUserID, pReferenceNumber);
                if (recur.UserId != null)
                {
                    recur.Suspended = pSuspended;
                    if (pSuspended == "Y")
                        recur.SuspendedDate = DateTime.Now;
                    else
                        recur.SuspendedDate = null;

					RecurTmplCrud crud = new RecurTmplCrud();
					crud.Update(recur);
                }

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateRecurTmpl - " + Ex.Message);
            }
        }
		public Boolean UpdateRecurTmpl(String pUserID, String pReferenceNumber, String pCardNumber, String pCvc, String pCardholderName, String pExpiryMM, String pExpiryYY)
		{
			try
			{
				RecurTmpl recur = GetRecurTmpl(pUserID, pReferenceNumber);
				if (pCardNumber.Trim() == "") pCardNumber = recur.CardNumber;
				if (pCvc.Trim() == "") pCvc = recur.Cvc;
				if (pCardholderName.Trim() == "") pCardholderName = recur.CardholderName;
				if (pExpiryMM.Trim() == "") pExpiryMM = recur.ExpiryMm;
				if (pExpiryYY.Trim() == "") pExpiryYY = recur.ExpiryYy;
				DateTime suspendedDate = Object.ReferenceEquals(recur.SuspendedDate, null) ? DateTime.Now : Convert.ToDateTime(recur.SuspendedDate);
				StoredProcedure.vcs_UpdateRecurTmpl(pUserID, pReferenceNumber, recur.RecurType, recur.CardType, pCardNumber, pCvc, pCardholderName, recur.EmailAddress, pExpiryMM, pExpiryYY, recur.AmountInd, recur.VaBalanceInd, Convert.ToDecimal(recur.Amount), recur.Percentage, recur.DescrOfGoods, Convert.ToDateTime(recur.Presented), recur.TransactionType, recur.StartDate, recur.EndDate, recur.OccurCount, recur.Frequency, recur.Suspended, suspendedDate, recur.Deleted, recur.ApoAccountNumber, recur.ApoMcmCardNumber, recur.ApoMblCardNumber, recur.UserName, DateTime.Now, pUserID, recur.m_pars, recur.CustomerID, recur.SpreedlyID);

				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("UpdateRecurtmpl-" + Ex.Message); 
			}
		}
        public Boolean UpdateRecurTmpl(String pUserID, String pReferenceNumber, String pRecurType, String pCardType, String pCardNumber, String pCvc, String pCardHolderName, String pEmailAddress, String pExpiryMM, String pExpiryYY, String pAmountInd, String pVaBalanceInd, Decimal pAmount, String pPercentage, String pDescOfGoods, DateTime pPresented, String pTransactionType, String pStartDate, String pEndDate, String pOccurCount, String pFrequency, String pSuspended, DateTime pSuspendedDate, String pDeleted, String pApoAccountNumber, String pApoMcmCardNumber, String pApoMblCardNumber, String pUserName, DateTime pModifiedDate, String pModifiedUserName, String pMpars, String pCustomerID, String pSpreedlyID)
        {
            try
            {
                StoredProcedure.vcs_UpdateRecurTmpl(pUserID, pReferenceNumber, pRecurType, pCardType, pCardNumber, pCvc, pCardHolderName, pEmailAddress, pExpiryMM, pExpiryYY, pAmountInd, pVaBalanceInd, pAmount, pPercentage, pDescOfGoods, pPresented, pTransactionType, pStartDate, pEndDate, pOccurCount, pFrequency, pSuspended, pSuspendedDate, pDeleted, pApoAccountNumber, pApoMcmCardNumber, pApoMblCardNumber, pUserName, pModifiedDate, pModifiedUserName, pMpars, pCustomerID, pSpreedlyID);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateRecurTmpl-" + Ex.Message);
            }
        }
        public Boolean SaveRecurTmpl(String pUserID, String pReferenceNumber, String pRecurType, String pCardType, String pCardNumber, String pCvc, String pCardHolderName, String pEmailAddress, String pExpiryMM, String pExpiryYY, String pAmountInd, String pVaBalanceInd, Decimal pAmount, String pPercentage, String pDescOfGoods, DateTime pPresented, String pTransactionType, String pStartDate, String pEndDate, String pOccurCount, String pFrequency, String pSuspended, DateTime pSuspendedDate, String pDeleted, String pApoAccountNumber, String pApoMcmCardNumber, String pApoMblCardNumber, String pUserName, DateTime pModifiedDate, String pModifiedUserName, String pMpars, String pCustomerID, String pSpreedlyID)
        {
            try
            {

                StoredProcedure.vcs_StoreRecurTmpl(pUserID, pReferenceNumber, pRecurType, pCardType, pCardNumber, pCvc, pCardHolderName, pEmailAddress, pExpiryMM, pExpiryYY, pAmountInd, pVaBalanceInd, pAmount, pPercentage, pDescOfGoods, pPresented, pTransactionType, pStartDate, pEndDate, pOccurCount, pFrequency, pSuspended, pSuspendedDate, pDeleted, pApoAccountNumber, pApoMcmCardNumber, pApoMblCardNumber, pUserName, pModifiedDate, pModifiedUserName, pMpars, pCustomerID, pSpreedlyID);
                return true;

            }
            catch (Exception Ex)
            {
                throw new Exception("SaveRecurTmpl - " + Ex.Message);
            }

        }
        public RecurTmpl GetRecurTmpl(String pUserID, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                RecurTmplCrud crud = new RecurTmplCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                RecurTmpl search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetRecurTmpl - " + Ex.Message);
            }
        }
        public List<RecurTmpl> GetRecurTmpl(String pUserID)
        {
            try
            {
                recordFound = false;
                RecurTmplCrud crud = new RecurTmplCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);

                List<RecurTmpl> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetRecurTmpl - " + Ex.Message);
            }
        }
        public RecurTmpl GetRecurTmplByCustomerID(String pCustomerID)
        {
            try
            {
                recordFound = false;
                RecurTmplCrud crud = new RecurTmplCrud();
                crud.Where("CustomerID", General.Operator.Equals, pCustomerID);

                RecurTmpl search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetRecurTmplByCustomerID - " + Ex.Message);
            }
        }
       public Validation ValidateRecurring(String pUserID, String pReferenceNumber, DatabaseInteraction pDatabaseInteraction, String pDescriptionOfGoods, String pOccurCount, String pAmount, String pOccurFrequency, String pNextOccurDate, String pMobileNumber, String pCardHolderEmail, String pExpiryMM, String pExpiryYY, String pCCV, String pCardNumber)
        {
            try
            {
                Validation validate = new Validation();

                if (pUserID == null) pUserID = "";
                if (pUserID == "")
                {
                    validate.VCSResponseCode = "7014";
                    validate.VCSResponseMessage = "Terminal ID was not passed.";
                    return validate;
                }

                if (pReferenceNumber == null) pReferenceNumber = "";
                if (pReferenceNumber == "")
                {
                    validate.VCSResponseCode = "7015";
                    validate.VCSResponseMessage = "Reference number may not be empty.";
                    return validate;
                }
                Merchants merchant = GetMerchants(pUserID);
                if (merchant == null)
                {
                    validate.VCSResponseCode = "7012";
                    validate.VCSResponseMessage = "Terminal ID (" + pUserID + ") was not found.";
                    return validate;
                }

                if (pDatabaseInteraction == DatabaseInteraction.Insert)
                {
                    RecurTmpl tmpl = GetRecurTmpl(pUserID, pReferenceNumber);
                    if (tmpl.UserId != null)
                    {
                        validate.VCSResponseCode = "7001";
                        validate.VCSResponseMessage = "Recurring transaction already exists for TerminalId " + pUserID + " and reference number " + pReferenceNumber;
                        return validate;
                    }

                    if (pReferenceNumber.Length > 15)
                    {
                        validate.VCSResponseCode = "7002";
                        validate.VCSResponseMessage = "Reference number may not exceed 15 characters.";
                        return validate;
                    }
                }

                if (pDatabaseInteraction == DatabaseInteraction.Update)
                {
                    RecurTmpl tmpl = GetRecurTmpl(pUserID, pReferenceNumber);
                    if (tmpl.UserId == null)
                    {
                        validate.VCSResponseCode = "7013";
                        validate.VCSResponseMessage = "Recurring transaction was not found for TerminalId " + pUserID + " and reference number " + pReferenceNumber;
                        return validate;
                    }
                }

                Validation valCardNo = ValidateCardNumber(pUserID, pCardNumber);
                if (valCardNo.VCSResponseCode != "0000")
                {
                    validate.VCSResponseCode = valCardNo.VCSResponseCode;
                    validate.VCSResponseMessage = valCardNo.VCSResponseMessage;
                }


                if (pDescriptionOfGoods == null) pDescriptionOfGoods = "";
                if (pDescriptionOfGoods.Trim() == "")
                {
                    validate.VCSResponseCode = "7003";
                    validate.VCSResponseMessage = "Description of goods may not be empty.";
                    return validate;
                }

                if (pOccurCount == null) pOccurCount = "";
                if (pOccurCount != "")
                {
                    if (pOccurCount == "0")
                    {
                        validate.VCSResponseCode = "7004";
                        validate.VCSResponseMessage = "Occurrence count may not be zero.";
                        return validate;
                    }

                    Int32 iOccurCount = 0;
                    if (pOccurCount != "U")
                    {
                        if (!Int32.TryParse(pOccurCount, out iOccurCount))
                        {
                            validate.VCSResponseCode = "7005";
                            validate.VCSResponseMessage = "Occurrence count not numeric.";
                            return validate;
                        }
                    }
                }

                Validation validateAmount = new Validation();
                validateAmount = ValidateAmount(pAmount);
                if (validateAmount.VCSResponseCode != "0000")
                {
                    validate.VCSResponseCode = validateAmount.VCSResponseCode;
                    validate.VCSResponseMessage = validateAmount.VCSResponseMessage;
                    return validate;
                }

                if (pOccurFrequency == null) pOccurFrequency = "";
                if (pOccurFrequency != "")
                {
                    String validFreq = "ODWMQ6Y";
                    if (!validFreq.ToUpper().Contains(pOccurFrequency))
                    {
                        validate.VCSResponseCode = "7006";
                        validate.VCSResponseMessage = "Invalid occurrence frequency.";
                        return validate;
                    }

                    if (pNextOccurDate == "" && pOccurFrequency.ToUpper() == "O")
                    {
                        pNextOccurDate = DateTime.Now.ToString("yyyy/MM/dd");
                    }

                    if (pNextOccurDate == "")
                    {
                        validate.VCSResponseCode = "7008";
                        validate.VCSResponseMessage = "Next occur date cannot be empty.";
                        return validate;
                    }

                    if (pNextOccurDate == null) pNextOccurDate = "";
                    if (pNextOccurDate != "")
                    {
                        if (!pNextOccurDate.Contains("/"))
                        {
                            validate.VCSResponseMessage = "Next occur date is not valid. The format should be ccyy/mm/dd";
                            validate.VCSResponseCode = "7009";
                            return validate;
                        }

                        if (pNextOccurDate.IndexOf("/") != 4)
                        {
                            validate.VCSResponseMessage = "Next occur date is not valid. The year should be the first part of the date.";
                            validate.VCSResponseCode = "7010";
                            return validate;
                        }
                        DateTime occurDateCheck = DateTime.Now;
                        if (!DateTime.TryParse(pNextOccurDate, out occurDateCheck))
                        {
                            validate.VCSResponseMessage = "Next occur date is not valid.";
                            validate.VCSResponseCode = "7011";
                            return validate;
                        }
                    }

                }

                if (pMobileNumber == null) pMobileNumber = "";
                if (pMobileNumber != "")
                {
                    Validation valPhone = ValidatePhoneNumber(pMobileNumber);
                    if (valPhone.VCSResponseCode != "0000")
                    {
                        validate.VCSResponseCode = valPhone.VCSResponseCode;
                        validate.VCSResponseMessage = valPhone.VCSResponseMessage;
                        return validate;
                    }
                }

                if (pCardHolderEmail == null) pCardHolderEmail = "";
                if (pCardHolderEmail != "")
                {
                    Validation valEmail = ValidateEmailAddress(pCardHolderEmail);
                    if (valEmail.VCSResponseCode != "0000")
                    {
                        validate.VCSResponseCode = valEmail.VCSResponseCode;
                        validate.VCSResponseMessage = valEmail.VCSResponseMessage;
                        return validate;
                    }
                }

                if (pExpiryMM == null) pExpiryMM = "";
                if (pExpiryMM != "")
                {
                    Validation valCardExpiryMM = ValidateExpiryMonth(pExpiryMM);
                    if (valCardExpiryMM.VCSResponseCode != "0000")
                    {
                        validate.VCSResponseCode = valCardExpiryMM.VCSResponseCode;
                        validate.VCSResponseMessage = valCardExpiryMM.VCSResponseMessage;
                        return validate;
                    }
                }

                if (pExpiryYY == null) pExpiryYY = "";
                if (pExpiryYY != "")
                {
                    Validation valCardExpiryYear = ValidateExpiryYear(pExpiryYY);
                    if (valCardExpiryYear.VCSResponseCode != "0000")
                    {
                        validate.VCSResponseCode = valCardExpiryYear.VCSResponseCode;
                        validate.VCSResponseMessage = valCardExpiryYear.VCSResponseMessage;
                        return validate;
                    }
                }


                if (pCCV == null) pCCV = "";
                if (pCCV != "")
                {
                    Validation ccvVal = ValidateCCV(pCCV);
                    if (ccvVal.VCSResponseCode != "0000")
                    {
                        validate.VCSResponseCode = ccvVal.VCSResponseCode;
                        validate.VCSResponseMessage = ccvVal.VCSResponseMessage;
                        return validate;
                    }
                }

                validate.VCSResponseCode = "0000";
                validate.VCSResponseMessage = "Validated";
                return validate;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateRecurring-" + Ex.Message);
            }
        }
    }
}
