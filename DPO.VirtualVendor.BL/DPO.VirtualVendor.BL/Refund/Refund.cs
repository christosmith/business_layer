﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;

namespace DPO.VirtualVendor.BL
{
public partial class VirtualVendorBL
    {
        public Refunds GetRefunds(String pUserID, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                RefundsCrud crud = new RefundsCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                Refunds search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetRefund - " + Ex.Message);
            }
        }
        public Boolean UpdateRefund(String pUserID, String pReferenceNumber, String pUserName)
        {
            try
            {
				RefundsCrud crud = new RefundsCrud();
				crud.Where("UserID", General.Operator.Equals, pUserID);
				crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);
				Refunds refund = crud.ReadSingle();
				refund.UserName = pUserName;
				crud.Update(refund);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateRefund-" + Ex.Message);
            }
        }
        private String URLEncode(String strE)
        {
            Int32 x = 0;
            String strNew = "";
            String strC = "";
            
            for (x = 0; x< strE.Length; x++)
            {
                strC = strE.Substring(x, 1);
                if (strC == " ")
                    strNew += "+";
                else
                    strNew += strC;
            }

            strNew = strNew.Replace("&", "%26amp%3B");


            return strNew;
        }
        public String DoRefund(String pUserID, String pReferenceNumber, String pDescription, String pAmount, String pCardholderEmail, String pCellNumber, String pMessage, String pccXmlAuthUrl)
        {
            try
            {
                String xmlMessage = "xmlmessage=<?xml version='1.0' encoding='utf-8' ?>\n";
                xmlMessage += " <RefundRequest>\n";
                xmlMessage += "     <UserId>" + pUserID + "</UserId>\n";
                xmlMessage += "     <Reference>" + pReferenceNumber + "</Reference>\n";
                xmlMessage += "     <Description>" + pDescription + "</Description>\n";
                xmlMessage += "     <Amount>" + pAmount + "</Amount>\n";
                xmlMessage += "     <CardholderEmail>" + pCardholderEmail + "</CardholderEmail>\n";
                xmlMessage += "     <CellNumber>" + pCellNumber + "</CellNumber>\n";
                xmlMessage += "     <Message>" + pMessage + "</Message>\n";
                xmlMessage += " </RefundRequest>\n";
                //xmlMessage = System.Web.HttpContext.Current.Server.UrlEncode(xmlMessage);
                xmlMessage = URLEncode(xmlMessage);
                String refundURL = pccXmlAuthUrl;
                System.Net.HttpWebRequest xmlServerHttpRequest;
                xmlServerHttpRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(refundURL);
                xmlServerHttpRequest.ContentType = "application/x-www-form-urlencoded";
                xmlServerHttpRequest.Method = "POST";
                ASCIIEncoding encoding = new ASCIIEncoding();
                Byte[] data = encoding.GetBytes(xmlMessage);
                xmlServerHttpRequest.ContentLength = data.Length;
                xmlServerHttpRequest.AllowAutoRedirect = false;
                xmlServerHttpRequest.UserAgent = "Virtual Card Services";

                System.IO.Stream HTTPStream = xmlServerHttpRequest.GetRequestStream();
                HTTPStream.Write(data, 0, data.Length);
                HTTPStream.Close();


                System.Net.HttpWebResponse HTTPResponse = (System.Net.HttpWebResponse)xmlServerHttpRequest.GetResponse();
                String sServerStatus = HTTPResponse.StatusDescription;
                String sResponseText = "";
                if (HTTPResponse.StatusCode == System.Net.HttpStatusCode.OK || HTTPResponse.StatusCode == System.Net.HttpStatusCode.MovedPermanently || HTTPResponse.StatusCode == System.Net.HttpStatusCode.Found)
                {
                    System.IO.Stream str = HTTPResponse.GetResponseStream();
                    //System.IO.StreamReader sR = new System.IO.StreamReader(str, System.Text.Encoding.UTF8);
                    System.IO.StreamReader sR = new System.IO.StreamReader(str, System.Text.Encoding.UTF8);
                    System.Xml.XmlDocument returnDoc = new System.Xml.XmlDocument();
                    returnDoc.Load(sR);
                    String response = returnDoc.GetElementsByTagName("Response")[0].ChildNodes[0].Value;
                    sResponseText = response;
                }

                //Send mail to support if the refund was not successfull

                return sResponseText;
            }
            catch (Exception Ex)
            {
                throw new Exception("DoRefund-" + Ex.Message);
            }
        }
		public Boolean InsertRefund(String pUserID, String pReferenceNumber, String pReason, String pType, String pResponseCode, String pAuthCode, String pCaptureDate, String pRetrievalReference, String pSystemTrace)
        {
            try
            {
                RefundsCrud crud = new RefundsCrud();
                Refunds workRec = new Refunds();
                workRec.ReferenceNumber = pReferenceNumber;
                workRec.Reason = pReason;
                workRec.Type = pType;
                workRec.ResponseCode = pResponseCode;
                workRec.AuthCode = pAuthCode;
                workRec.CaptureDate = pCaptureDate;
                workRec.RetrievalReference = pRetrievalReference;
                crud.Insert(workRec);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertRefund - " + Ex.Message);
            }
        }
    }
}
