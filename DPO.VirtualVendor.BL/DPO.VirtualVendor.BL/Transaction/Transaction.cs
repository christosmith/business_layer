﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;

namespace DPO.VirtualVendor.BL
{
public partial class VirtualVendorBL
    {
        public List<TransLog> GetTransLog(String pUserID, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                TransLogCrud crud = new TransLogCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                List<TransLog> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTransLog - " + Ex.Message);
            }
        }
        public String MaskCardNumber(String pCardNumber)
        {
            try
            {
                Double cardNumeric = 0;
                if (!Double.TryParse(pCardNumber, out cardNumeric))
                    return pCardNumber;

                if (pCardNumber.Length != 16)
                    return pCardNumber;

                String maskedCard = pCardNumber.Substring(0, 6) + "******" + pCardNumber.Substring(11, 4);

                return maskedCard;
              
            }
            catch (Exception Ex)
            {
                throw new Exception("MaskCardNumber - " + Ex.Message);
            }
        }
        public List<TransactionsDue> GetTransactionDue(String pUserID)
        {
            try
            {
                recordFound = false;
                TransactionsDueCrud crud = new TransactionsDueCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);

                List<TransactionsDue> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTransactionDue - " + Ex.Message);
            }
        }
        public TransactionsDue GetTransactionDue(String pUserID, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                TransactionsDueCrud crud = new TransactionsDueCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                TransactionsDue search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTransactionDue - " + Ex.Message);
            }
        }
        public List<spTransactionView> GetTransactionViewByCardholderName(String pUserName, String pUserID, String pCardholderName)
        {
            try
            {
                List<spTransactionView> transactions = StoredProcedure.spTransactionView(pUserName, DateTime.Now, DateTime.Now, pUserID, null, pCardholderName);
                return transactions;
                
            }
            catch (Exception Ex)
            {
                String errorMessage = "GetTransactionViewByCardholderName(String, String, String) - " + Ex.Message;
                throw new Exception(errorMessage);
            }
        }
        public List<spTransactionView> GetTransactionView(String pUserName, String pUserID, String pCardNumber)
        {
            try
            {
                return StoredProcedure.spTransactionView(pUserName, DateTime.Now, DateTime.Now, pUserID, pCardNumber, null);
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTransactionView - " + Ex.Message);
            }
        }
        public List<spTransactionView> GetTransactionView(String pUserName, String pUserID, DateTime pStartDate, DateTime pEndDate)
        {
            try
            {
                return StoredProcedure.spTransactionView(pUserName, pStartDate, pEndDate, pUserID, null, null);
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTransactionView - " + Ex.Message);
            }
        }
        public List<spTransactionView> GetTransactionView(String pUserName, DateTime pStartDate, DateTime pEndDate)
        {
            try
            {
                return StoredProcedure.spTransactionView(pUserName, pStartDate, pEndDate, null, null, null);
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTransactionView - " + Ex.Message);
            }
        }
        public Boolean InsertTransactionDueErr(String pUserId, String pReferenceNumber, String pTranType, String pCardNumber, String pCvc, String pCardholderName, String pCardholderEmail, String pExpiryMm, String pExpiryYy, Decimal pAmount, String pBudgetPeriod, String pDescrOfGoods, DateTime pPresented, String pResponse, Int32 pRecurrenceId, String pRecurrenceRef, String pApoAccountNumber, String pApoMcmCardNumber, String pApoMblCardNumber, String pAuthCode, String pUserName, String pOriginalRef)
        {
            try
            {
                TransactionsDueErrCrud crud = new TransactionsDueErrCrud();
                TransactionsDueErr workRec = new TransactionsDueErr();
                workRec.ReferenceNumber = pReferenceNumber;
                workRec.TranType = pTranType;
                workRec.CardNumber = pCardNumber;
                workRec.Cvc = pCvc;
                workRec.CardholderName = pCardholderName;
                workRec.CardholderEmail = pCardholderEmail;
                workRec.ExpiryMm = pExpiryMm;
                workRec.ExpiryYy = pExpiryYy;
                workRec.Amount = pAmount;
                workRec.BudgetPeriod = pBudgetPeriod;
                workRec.DescrOfGoods = pDescrOfGoods;
                workRec.Presented = pPresented;
                workRec.Response = pResponse;
                workRec.RecurrenceId = pRecurrenceId;
                workRec.RecurrenceRef = pRecurrenceRef;
                workRec.ApoAccountNumber = pApoAccountNumber;
                workRec.ApoMcmCardNumber = pApoMcmCardNumber;
                workRec.ApoMblCardNumber = pApoMblCardNumber;
                workRec.AuthCode = pAuthCode;
                workRec.UserName = pUserName;
                crud.Insert(workRec);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertTransactionDueErr - " + Ex.Message);
            }
        }
        public TransactionsDueErr GetTransactionDueErr(String pUserID, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                TransactionsDueErrCrud crud = new TransactionsDueErrCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                TransactionsDueErr search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTransactionDueErr - " + Ex.Message);
            }
        }
        public Boolean DeleteTransactionDueErr(String pUserID, String pReferenceNumber)
        {
            try
            {
				TransactionsDueErr err = GetTransactionDueErr(pUserID, pReferenceNumber);
				TransactionsDueErrCrud crud = new TransactionsDueErrCrud();
				crud.Delete(err);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteTransactionDueErr - " + Ex.Message);
            }
        }
        public Transactions GetTransactions(String pUserID, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                TransactionsCrud crud = new TransactionsCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                Transactions search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTransaction - " + Ex.Message);
            }
        }
        public Boolean DeleteTransactionDue(String pUserID, String pReferenceNumber)
        {
            try
            {
				TransactionsDue due = GetTransactionDue(pUserID, pReferenceNumber);
				TransactionsDueCrud crud = new TransactionsDueCrud();
				crud.Delete(due);
                return true ;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteTransactionDue(String, String) - " + Ex.Message);
            }
        }
        public Boolean DeleteTransaction(String pUserID, String pReferenceNumber)
        {
            try
            {
                TransactionsCrud crud = new TransactionsCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                List<Transactions> search = crud.ReadMulti().ToList();
                foreach (Transactions del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteTransaction - " + Ex.Message);
            }
        }
        public List<TransLogError> GetTransLogError(String pUserID, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                TransLogErrorCrud crud = new TransLogErrorCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                List<TransLogError> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetTransLogError - " + Ex.Message);
            }
        }
        public Int32 SaveXmlTranData(String pUserID, String pReferenceNumber, String pXmlDocument, String pSystemTraceAuditNumber, String pRetrievalReferenceNumber, String pAuthNumber)
        {
            try
            {
                
                //PCIDSS - mask the card number in stored data
                if (pXmlDocument.Contains("<DE035>"))
                {
                    Int32 pos = pXmlDocument.IndexOf("<DE035>");
                    String varXml1 = pXmlDocument.Substring(0, pos + 12);
                    String varXml2 = pXmlDocument.Substring(pos+19);
                    pXmlDocument = varXml1 + "******" + varXml2;
                }

                //PCIDSS - remove the CVC in stored data
                if (pXmlDocument.Contains("! C000026 "))
                {
                    Int32 pos = pXmlDocument.IndexOf("! C000026 ");
                    String varXml1 = pXmlDocument.Substring(0, pos + 12);
                    String varXml2 = pXmlDocument.Substring(pos + 19);
                    pXmlDocument = varXml1 + "******" + varXml2;
                }

                XmlTranData tranData = new XmlTranData();
                tranData.AuthNumber = pAuthNumber;
                tranData.ReferenceNo = pReferenceNumber;
                tranData.RetrievalReferenceNumber = pRetrievalReferenceNumber;
                tranData.SystemTraceAuditNumber = pSystemTraceAuditNumber;
                tranData.TransactionDate = System.DateTime.Now;
                tranData.UserId = pUserID;
                tranData.XmlDocument = pXmlDocument;
				XmlTranDataCrud crud = new XmlTranDataCrud();
				long ID = 0;
				crud.Insert(tranData, out ID);
				return (Int32)ID;

            }
            catch (Exception Ex)
            {
                throw new Exception("SaveXmlTranData-" + Ex.Message);
            }
        }
		public Boolean StoreTransaction(String pUserId, String pRefNo, String pCardNumber, String pCardType, String pCvc, String pCardholderName, String pExpiryMm, String pExpiryYy, Decimal pAmount, String pBudgetPeriod, String pDescrOfGoods, String pResponse, DateTime pPresented, DateTime pAuthorised, String pCardholderEmail, String pCardholderIpAddr, DateTime pDelaySettlementDate, String pResponseCode, String pAuthCode, String pCaptureDate, String pRetrievalReferenceNumber, String pNumericCurrencyCode, String pSystemTraceAuditNumber, String pSettleSystemTraceAuditNumber, String pAcquirer, String pAuthMethod, String pSettleMethod, String pAcquiringInstitutionId, String pForwardingInstitutionId, String pTransmissionDateTime, String pSwitchSettleDate, String pTransactionGUID, String pEgateAvr, String pEgateReference, String pEgateTransId, String pEgateTrackId, String pEgatePaymentId, String pEgateCvv2Response, Decimal pOccurAmount, String pOccurNextDate, String pOccurCount, String pOccurFrequency, String pOccurEmail, String pOccurUpdate, Int32 pRecurrenceID, String pRecurrenceRef, String pm_pars, String pMerchVariable1, String pMerchVariable2, String pMerchVariable3, String pMerchVariable4, String pMerchVariable5, String pMerchVariable6, String pMerchVariable7, String pMerchVariable8, String pMerchVariable9, String pMerchVariable10)
		{
			try
			{
				StoredProcedure.vcs_StoreTransaction(pUserId, pRefNo, pCardNumber, pCardType, pCvc, pCardholderName, pExpiryMm, pExpiryYy, pAmount, pBudgetPeriod, pDescrOfGoods, pResponse, pPresented, pAuthorised, pCardholderEmail, pCardholderIpAddr, pDelaySettlementDate, pResponseCode, pAuthCode, pCaptureDate, pRetrievalReferenceNumber, pNumericCurrencyCode, pSystemTraceAuditNumber, pSettleSystemTraceAuditNumber, pAcquirer, pAuthMethod, pSettleMethod, pAcquiringInstitutionId, pForwardingInstitutionId, pTransmissionDateTime, pSwitchSettleDate, pTransactionGUID, pEgateAvr, pEgateReference, pEgateTransId, pEgateTrackId, pEgatePaymentId, pEgateCvv2Response, pOccurAmount, pOccurNextDate, pOccurCount, pOccurFrequency, pOccurEmail, pOccurUpdate, pRecurrenceID, pRecurrenceRef, pm_pars, pMerchVariable1, pMerchVariable2, pMerchVariable3, pMerchVariable4, pMerchVariable5, pMerchVariable6, pMerchVariable7, pMerchVariable8, pMerchVariable9, pMerchVariable10);
				return true;
			}
            catch (Exception Ex)
            {
                throw new Exception("StoreTransaction - " + Ex.Message);
            }
        }
		public Boolean SettleTransaction(String pUserID, String pReferenceNumber)
		{
			try
			{
				Transactions tran = GetTransactions(pUserID, pReferenceNumber);
				if (!RecordFound)
					throw new Exception("Transaction record was not found.");

				tran.Settled = DateTime.Now;
				tran.BatchSubmissionRef = DateTime.Now.ToString("yyyyMMdd");
				TransactionsCrud crud = new TransactionsCrud();
				crud.Update(tran);

				return true;
			}
			catch (Exception Ex)
			{
				throw new Exception("SettleTransaction-" + Ex.Message);
			}
		}
    }
}
