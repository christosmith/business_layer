﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;

namespace DPO.VirtualVendor.BL
{


public partial class VirtualVendorBL
    {
        public Authentication Authenticate(String UserID, String Password)
        {
            Authentication auth = new Authentication();
            try
            {
                vtUsers user = GetVTUser(UserID);

                if (UserID == null || UserID == "")
                {
                    auth.VCSResponseCode = "9007";
                    auth.VCSResponseMessage = "UserID was not passed.";
                    return auth;
                }

                if (Password == null || Password == "")
                {
                    auth.VCSResponseCode = "9008";
                    auth.VCSResponseMessage = "Password was not passed.";
                    return auth;
                }

                if (user == null)
                {
                    auth.VCSResponseCode = "9002";
                    auth.VCSResponseMessage = "UserID (" + UserID + ") was not found.";
                    return auth;
                }

                //user = vvoBL.GetVTUserByUserIDAndPassword(UserID, Password);
                if (!ValidateUserAndPassword(UserID, Password))
                {
                    auth.VCSResponseCode = "9001";
                    auth.VCSResponseMessage = "Invalid Password.";
                    return auth;
                }

                if (user.SecLevel != "0" && user.SecLevel != "1")
                {
                    auth.VCSResponseCode = "9003";
                    auth.VCSResponseMessage = "Only users with security level one may use this functionality.";
                    return auth;
                }

                if (user.LockedOut == "Y")
                {
                    DateTime dateLockedOut = Convert.ToDateTime(user.DateLockedOut);
                    DateTime dateUnlocked = Convert.ToDateTime("1900-01-01");

                    if (user.UnlockedByVcsDate != "")
                        dateUnlocked = Convert.ToDateTime(user.UnlockedByVcsDate);

                    if (dateUnlocked.Year == 1900)
                    {
                        auth.VCSResponseMessage = "The account was locked out from the VCS system.";
                        auth.VCSResponseCode = "9004";
                        return auth;
                    }
                    else
                    {
                        if (dateLockedOut > dateUnlocked)
                        {
                            auth.VCSResponseMessage = "The account was locked out from the VCS system.";
                            auth.VCSResponseCode = "9004";
                            return auth;
                        }
                    }

                }

                if (user.ExpiredPassword != null && user.ExpiredPassword != "")
                {
                    auth.VCSResponseMessage = "The password has expired.";
                    auth.VCSResponseCode = "9005";
                    return auth;
                }

                return auth;
            }
            catch (Exception Ex)
            {
                String errorMessage = "Authenticate - " + Ex.Message;
                auth.VCSResponseCode = "9999";
                auth.VCSResponseMessage = errorMessage;
                return auth;
            }
        }
		public Authentication Authenticate(String UserID, String Password, String TerminalID, String pSecurityLevel)
        {
            Authentication auth = new Authentication();
            try
            {
                if (UserID == null || UserID == "")
                {
                    auth.VCSResponseCode = "9007";
                    auth.VCSResponseMessage = "UserID was not passed.";
                    return auth;
                }

                if (Password == null || Password == "")
                {
                    auth.VCSResponseCode = "9008";
                    auth.VCSResponseMessage = "Password was not passed.";
                    return auth;
                }

                vtUsers user = GetVTUser(UserID);
                if (user == null)
                {
                    auth.VCSResponseCode = "9002";
                    auth.VCSResponseMessage = "UserID (" + UserID + ") was not found.";
                    return auth;
                }

                if (!ValidateUserAndPassword(UserID, Password))
                {
                    auth.VCSResponseCode = "9001";
                    auth.VCSResponseMessage = "Invalid Password.";
                    return auth;
                }

                if (Convert.ToInt32(user.SecLevel) > Convert.ToInt32(pSecurityLevel))
                {
                    auth.VCSResponseCode = "9003";
                    auth.VCSResponseMessage = "Only users with security level " + pSecurityLevel + " and higher may use this functionality.";
                    return auth;
                }

                if (user.LockedOut == "Y")
                {
                    DateTime dateLockedOut = Convert.ToDateTime(user.DateLockedOut);
                    DateTime dateUnlocked = Convert.ToDateTime("1900-01-01");

                    if (user.UnlockedByVcsDate != "")
                        dateUnlocked = Convert.ToDateTime(user.UnlockedByVcsDate);

                    if (dateUnlocked.Year == 1900)
                    {
                        auth.VCSResponseMessage = "The account was locked out from the VCS system.";
                        auth.VCSResponseCode = "9004";
                        return auth;
                    }
                    else
                    {
                        if (dateLockedOut > dateUnlocked)
                        {
                            auth.VCSResponseMessage = "The account was locked out from the VCS system.";
                            auth.VCSResponseCode = "9004";
                            return auth;
                        }
                    }

                }

                if (user.ExpiredPwHash != null)
                {
                    auth.VCSResponseMessage = "The password has expired.";
                    auth.VCSResponseCode = "9005";
                    return auth;
                }

                if (user.SecLevel != "0")
                {
                    if (TerminalID == null || TerminalID == "")
                    {
                        auth.VCSResponseMessage = "TerminalID was not passed.";
                        auth.VCSResponseCode = "9007";
                        return auth;
                    }
                    vtUsersLink link = GetVTUserLink(TerminalID, UserID);

                    if (link == null)
                    {
                        auth.VCSResponseCode = "9006";
                        auth.VCSResponseMessage = "Access denied.";
                        return auth;
                    }
                }
                return auth;
            }
            catch (Exception Ex)
            {
                String errorMessage = "Authenticate - " + Ex.Message;
                auth.VCSResponseCode = "9999";
                auth.VCSResponseMessage = errorMessage;
                return auth;
            }
        }
		public  Authentication Authenticate(String UserID, String Password, String TerminalID)
        {
            Authentication auth = new Authentication();
            try
            {
                if (UserID == null || UserID == "")
                {
                    auth.VCSResponseCode = "9007";
                    auth.VCSResponseMessage = "UserID was not passed.";
                    return auth;
                }

                if (Password == null || Password == "")
                {
                    auth.VCSResponseCode = "9008";
                    auth.VCSResponseMessage = "Password was not passed.";
                    return auth;
                }

                vtUsers user = GetVTUser(UserID);
                if (user == null)
                {
                    auth.VCSResponseCode = "9002";
                    auth.VCSResponseMessage = "UserID (" + UserID + ") was not found.";
                    return auth;
                }

                if (!ValidateUserAndPassword(UserID, Password))
                {
                    auth.VCSResponseCode = "9001";
                    auth.VCSResponseMessage = "Invalid Password.";
                    return auth;
                }


                if (user.SecLevel != "0" && user.SecLevel != "1")
                {
                    auth.VCSResponseCode = "9003";
                    auth.VCSResponseMessage = "Only users with security level one may use this functionality.";
                    return auth;
                }

                if (user.LockedOut == "Y")
                {
                    DateTime dateLockedOut = Convert.ToDateTime(user.DateLockedOut);
                    DateTime dateUnlocked = Convert.ToDateTime("1900-01-01");

                    if (user.UnlockedByVcsDate != "")
                        dateUnlocked = Convert.ToDateTime(user.UnlockedByVcsDate);

                    if (dateUnlocked.Year == 1900)
                    {
                        auth.VCSResponseMessage = "The account was locked out from the VCS system.";
                        auth.VCSResponseCode = "9004";
                        return auth;
                    }
                    else
                    {
                        if (dateLockedOut > dateUnlocked)
                        {
                            auth.VCSResponseMessage = "The account was locked out from the VCS system.";
                            auth.VCSResponseCode = "9004";
                            return auth;
                        }
                    }

                }

                if (user.ExpiredPwHash != null )
                {
                    auth.VCSResponseMessage = "The password has expired.";
                    auth.VCSResponseCode = "9005";
                    return auth;
                }

                if (user.SecLevel != "0")
                {
                    vtUsersLink link = GetVTUserLink(TerminalID, UserID);

                    if (link == null)
                    {
                        auth.VCSResponseCode = "9006";
                        auth.VCSResponseMessage = "Access denied.";
                        return auth;
                    }
                }
                return auth;
            }
            catch (Exception Ex)
            {
                String errorMessage = "Authenticate - " + Ex.Message;
                auth.VCSResponseCode = "9999";
                auth.VCSResponseMessage = errorMessage;
                return auth;
            }
        }
    }
}
