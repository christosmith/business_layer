﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;

namespace DPO.VirtualVendor.BL
{
public partial class VirtualVendorBL
    {
        public Boolean ValidateUserAndPassword(String pUserName, String pPassword)
        {
            try
            {
                Int32 validated = Convert.ToInt32(StoredProcedure.spValidatePassword(pUserName, pPassword)[0].Validated);
                if (validated == 1)
                    return true;

                return false;   
               
            }
            catch (Exception Ex)
            {
                String errorMessage = "ValidateUserAndPassword(String, String) - " + Ex.Message;
                throw new Exception(errorMessage);
            }
        }
        public vtUsers GetVTUserByUserIDAndPassword(String pUserName, String pPassword)
        {
            try
            {
                recordFound = false;
                vtUsersCrud crud = new vtUsersCrud();
                crud.Where("UserName", General.Operator.Equals, pUserName);
                crud.And("Password", General.Operator.Equals, pPassword);

                vtUsers search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetVTUserByUserIDAndPassword - " + Ex.Message);
            }
        }
        public vtUsersLink GetVTUserLink(String pUserID, String pUserName)
        {
            try
            {
                recordFound = false;
                vtUsersLinkCrud crud = new vtUsersLinkCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("UserName", General.Operator.Equals, pUserName);

                vtUsersLink search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetVTUserLink - " + Ex.Message);
            }
        }
        public List<vtUsers> GetVTUser()
        {
            try
            {
                recordFound = false;
                vtUsersCrud crud = new vtUsersCrud();
                List<vtUsers> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetVTUser - " + Ex.Message);
            }
        }
        public Int32 GetVTUser(String pUserName, String pPassword)
        {
            try
            {
				vtUsersCrud crud = new vtUsersCrud();
				crud.Where("UserName", General.Operator.Equals, pUserName);
				crud.And("Password", General.Operator.Equals, pPassword);
				vtUsers user = crud.ReadSingle();
                return Convert.ToInt32(user.AutoId);
            }
            catch (Exception Ex)
            {
				throw new Exception("Unable to get virtual terminal user. Error: " + Ex.Message);
            }
        }
        public vtUsers GetVTUser(String pUserName)
        {
            try
            {
                recordFound = false;
                vtUsersCrud crud = new vtUsersCrud();
                crud.Where("UserName", General.Operator.Equals, pUserName);

                vtUsers search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetVTUser - " + Ex.Message);
            }
        }
        public String GetVtUserName(String pUserName)
        {
			try
			{
				vtUsersCrud crud = new vtUsersCrud();
				crud.Where("UserName", General.Operator.Equals, pUserName);
				List<vtUsers> users = crud.ReadMulti().ToList();
				if (users.Count == 0)
					return "";
				else
					return users[0].UserName;
			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
		public Int32 GetVtUserSecurityLevel(String pUserName)
		{
			try
			{
				vtUsers users = GetVTUser(pUserName);
				return Convert.ToInt32(users.SecLevel);

			}
			catch (Exception Ex)
			{
				throw Ex;
			}
		}
    }
}
