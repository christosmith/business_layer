﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;

namespace DPO.VirtualVendor.BL
{

    public partial class VirtualVendorBL
	{
        public Validation ValidateMD5Hash()
        {
            try
            {
                Validation result = new Validation();
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateMD5Hash - " + Ex.Message);
            }
        }
        public String GetCardType(String pCardNumber)
        {
            try
            {
                String cardNo2 = pCardNumber.Substring(0,2);
                String cardNo4 = pCardNumber.Substring(0,4);
                String cardType = "";
                if (pCardNumber.Substring(0, 1) == "4") cardType = "Visa";
                if (pCardNumber.Substring(0, 1) == "5") cardType = "MasterCard";
                if (pCardNumber.Substring(0, 1) == "3") cardType = "Jcb";
                if (pCardNumber.Substring(0, 1) == "6") cardType = "Virtual Account";

                switch (cardNo2)
                {
                    case "30":
                    case "36":
                    case "38":
                        cardType = "Diners";
                        break;
                    case "34":
                    case "37":
                        cardType = "Amex";
                        break;

                }

                switch (cardNo4)
                {
                    case "2131":
                    case "1800":
                        cardType ="Jcb";
                        break;
                    case "6011":
                        cardType = "Discover";
                        break;
                    case "9710":
                        cardType = "Virtual Account";
                        break;

                }
                return cardType;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetCardType(string) - " + Ex.Message);
            }
        }
        public Validation ValidateBudgetPeriod(String pBudgetPeriod)
        {
            Validation result = new Validation();
            Int32 noCheck = 0;
            if (!Int32.TryParse(pBudgetPeriod, out noCheck))
            {
                result.VCSResponseCode = "4600";
                result.VCSResponseMessage = "Budget period should be a number.";
                return result;
            }

            Int32 budgetPeriod = Convert.ToInt32(pBudgetPeriod);
            Boolean found = false;

            for (Int32 bpCheck = 3; bpCheck < 13; bpCheck++)
                if (budgetPeriod == bpCheck)
                    found = true;

            for (Int32 bpCheck = 18; bpCheck < 61; bpCheck += 6)
                if (budgetPeriod == bpCheck)
                    found = true;

            if (!found)
            {
                result.VCSResponseCode = "4601";
                result.VCSResponseMessage = "Invalid budget period.";
                return result;
            }

            result.VCSResponseCode = "0000";
            result.VCSResponseMessage = "Validated";

            return result;
        }
        public Validation ValidatePhoneNumber(String pPhoneNumber)
        {
            String newNumber = "";
            try
            {
                Validation result = new Validation();

                String allowed = "0123456789";
                for (Int32 noLoop = 0; noLoop < pPhoneNumber.Length; noLoop++)
                {
                    String currentChar = pPhoneNumber.Substring(noLoop, 1);
                    if (currentChar.ToLower() == "o") currentChar = "0";
                    if (allowed.Contains(currentChar))
                        newNumber += currentChar;
                }

                String leftTwo = newNumber.Substring(0, 2);
                if (leftTwo == "08" || leftTwo == "07" || leftTwo == "06" && leftTwo.Length == 10)
                    newNumber = "27" + newNumber.Substring(2);


                Double iCellPhoneNumber = 0;
                if (!Double.TryParse(newNumber, out iCellPhoneNumber))
                {
                    result.VCSResponseCode = "4301";
                    result.VCSResponseMessage = "Cellphone number not numeric.";
                    return result;
                }

                if (newNumber.Contains(".") || newNumber.Contains(","))
                {
                    result.VCSResponseCode = "4301";
                    result.VCSResponseMessage = "Cellphone number not numeric.";
                    return result;
                }

                if (newNumber.Length < 10)
                {
                    result.VCSResponseCode = "4302";
                    result.VCSResponseMessage = "Invalid cellphone number length.";
                    return result;
                }

                if (newNumber.Length > 11)
                {
                    result.VCSResponseCode = "4302";
                    result.VCSResponseMessage = "Invalid cellphone number length.";
                    return result;
                }

                String leftThree = pPhoneNumber.Substring(0, 3);
                if (leftTwo != "06" && leftTwo != "07" && leftTwo != "08")
                    if (leftThree != "276" && leftThree != "277" && leftThree != "278")
                    {
                        result.VCSResponseCode = "4303";
                        result.VCSResponseMessage = "Invalid cellphone number.";
                        return result;
                    }

                if (pPhoneNumber.Length == 11)
                {
                    if (leftThree != "276" && leftThree != "277" && leftThree != "278")
                    {
                        String ndc = "0710|0711|0712|0713|0714|0715|0716|0717|0718|0719|";
                        if (!ndc.Contains(pPhoneNumber.Substring(0, 4) + "|"))
                        {
                            result.VCSResponseCode = "4303";
                            result.VCSResponseMessage = "Invalid cellphone number.";
                            return result;
                        }
                    }
                }
                result.VCSReturnValue = newNumber;

                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidatePhoneNumber(String) - " + Ex.Message);
            }
        }
        public Validation ValidateMod10(String pCardNumber)
        {
            try
            {
                Validation result = new Validation();
                Int32 cdvCalc = 0;
                Int32 weight = 1;
                String cdv = "";
                Int32 cdvModulus = 0;

                if (pCardNumber.Substring(pCardNumber.Length - 2) == "37")
                    return result;

                for (Int32 count = pCardNumber.Length -1; count >= 0; count--)
                {
                    cdvCalc = Convert.ToInt32(pCardNumber.Substring(count, 1)) * weight;
                    cdv += cdvCalc.ToString();
                    if (weight == 2)
                        weight = 1;
                    else
                        weight = 2;
                }

                cdvCalc = 0;
                for (Int32 count = 0; count < cdv.Length; count++)
                    cdvCalc += Convert.ToInt32(cdv.Substring(count, 1));

                cdvModulus = cdvCalc % 10;

                if (cdvModulus != 0)
                {
                    result.VCSResponseCode = "1250";
                    result.VCSResponseMessage = "Your card number has been entered incorrectly.";
                    return result;
                }

                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateMod10 - " + Ex.Message);
            }
        }
        private Boolean isDate(String pDate)
        {
            DateTime Dt;

            if (DateTime.TryParse(pDate, out Dt))
            {
                if (pDate.Contains('-'))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        private Boolean IsFutureDate(DateTime refDate)
        {
            DateTime today = DateTime.Today;
            return (refDate.Date != today) && (refDate > today);
        }
        public Validation ValidateDate(String pDate, String pOccurCount)
        {
            try
            {
                Validation result = new Validation();
                if (pDate != string.Empty || pDate != null)
                {
                    //Check Date Lenth
                    if (pDate.Length == 10)
                    {
                        //Check if Date is valid Date
                        if (isDate(pDate))
                        {
                            DateTime Dt;
                            DateTime.TryParse(pDate, out Dt);

                            //Check if date is future date
                            if (IsFutureDate(Dt))
                            {
                                result.VCSResponseCode = "00";
                                result.VCSResponseMessage = "Validated";
                                return result;
                            }
                            else
                            {
                                if (pOccurCount.ToUpper() == "O")
                                {
                                    if ((Convert.ToDateTime(pDate) - DateTime.Now).Days < 0)
                                    {
                                        result.VCSResponseCode = "01";
                                        result.VCSResponseMessage = "Date must be a today or a date in the future not in the past";
                                        return result;
                                    }
                                    else
                                    {
                                        result.VCSResponseCode = "00";
                                        result.VCSResponseMessage = "Validated";
                                        return result;
                                    }
                                }
                                else
                                {
                                    result.VCSResponseCode = "01";
                                    result.VCSResponseMessage = "Date must be a date in the future not in the past";
                                    return result;
                                }
                            }
                        }
                        else
                        {
                            result.VCSResponseCode = "02";
                            result.VCSResponseMessage = "Date Supplied is not a valid Date format: ccyy/mm/dd is the correct format";
                            return result;
                        }
                    }
                    else
                    {
                        result.VCSResponseCode = "03";
                        result.VCSResponseMessage = "Date Length must be 10 characters and in format ccyy/mm/dd";
                        return result;
                    }
                }
                else
                {
                    result.VCSResponseCode = "04";
                    result.VCSResponseMessage = "Date cannot be empty and must be in format ccyy/mm/dd";
                    return result;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateDate-" + Ex.Message);
            }
        }
        public Validation ValidateDate(String pDate)
        {
            try
            {
                Validation result = new Validation();
                if (pDate != string.Empty || pDate != null)
                {
                    //Check Date Lenth
                    if (pDate.Length == 10)
                    {
                        //Check if Date is valid Date
                        if (isDate(pDate))
                        {
                            DateTime Dt;
                            DateTime.TryParse(pDate, out Dt);

                            //Check if date is future date
                            if (IsFutureDate(Dt))
                            {
                                result.VCSResponseCode = "00";
                                result.VCSResponseMessage = "Validated";
                                return result;
                            }
                            else
                            {

                                result.VCSResponseCode = "01";
                                result.VCSResponseMessage = "Date must be a date in the future not in the past";
                                return result;

                            }
                        }
                        else
                        {
                            result.VCSResponseCode = "02";
                            result.VCSResponseMessage = "Date Supplied is not a valid Date format: ccyy/mm/dd is the correct format";
                            return result;
                        }
                    }
                    else
                    {
                        result.VCSResponseCode = "03";
                        result.VCSResponseMessage = "Date Length must be 10 characters and in format ccyy/mm/dd";
                        return result;
                    }
                }
                else
                {
                    result.VCSResponseCode = "04";
                    result.VCSResponseMessage = "Date cannot be empty and must be in format ccyy/mm/dd";
                    return result;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateDate-" + Ex.Message);
            }
        }
        public Validation ValidateFrequency(String pFrequency)
        {
            Validation result = new Validation();
            try
            {
                if (pFrequency != string.Empty)
                {
                    switch (pFrequency)
                    {
                        case "D":
                            result.VCSResponseCode = "00"; result.VCSResponseMessage = "Validated"; break;
                        case "W":
                            result.VCSResponseCode = "00"; result.VCSResponseMessage = "Validated";break;
                        case "M":
                            result.VCSResponseCode = "00"; result.VCSResponseMessage = "Validated";break;
                        case "Y":
                            result.VCSResponseCode = "00"; result.VCSResponseMessage = "Validated";break;
                        case "Q":
                            result.VCSResponseCode = "00"; result.VCSResponseMessage = "Validated";break;
                        case "6":
                            result.VCSResponseCode = "00"; result.VCSResponseMessage = "Validated";break;
                        case "O":
                            result.VCSResponseCode = "00"; result.VCSResponseMessage = "Validated";break;
                        default:
                            result.VCSResponseCode = "01"; result.VCSResponseMessage = "Invalid Frequency, D=Daily, W=Weekly, M=Monthly, Y=Yearly, Q=Quarterly, 6=Bi-Annually, O=On-Demand"; break;
                    }
                }
                else
                {
                    result.VCSResponseCode = "02"; result.VCSResponseMessage = "Frequency Cannot Be Empty";
                }

                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateFrequency-" + Ex.Message);
            }
        }
        public Validation ValidateBankAccountHolder(String pBankAccountHolder)
        {
            Validation result = new Validation();
            try
            {
                if (!String.IsNullOrEmpty(pBankAccountHolder))
                {
                    if (pBankAccountHolder.Length > 3)
                    {
                        result.VCSResponseCode = "0000";
                        result.VCSResponseMessage = "Validated";
                    }
                    else
                    {
                        result.VCSResponseCode = "0001";
                        result.VCSResponseMessage = "Error: Account Holder must be at least 3 characters";
                    }
                }
                else
                {
                    result.VCSResponseCode = "0002";
                    result.VCSResponseMessage = "Error: Account Holder Cannot Be Empty";
                }

                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValuidateBankAccountHolder-" + Ex.Message);
            }
        }
        public Validation ValidateReferenceNumber(String pReferenceNumber, Boolean pForRecurring)
        {
            Validation result = new Validation();

            if (!pForRecurring)
                return ValidateReferenceNumber(pReferenceNumber);
            
            try
            {
                char[] myChars = pReferenceNumber.ToCharArray();

                if (pReferenceNumber != string.Empty)
                {
                    if (pReferenceNumber.Length <= 15)
                    {
                        foreach (char myChr in myChars)
                        {
                            if (!char.IsLetterOrDigit(myChr))
                            {
                                result.VCSResponseCode = "01";
                                result.VCSResponseMessage = "Only alphanumeric characters are allowed";
                                return result;
                            }
                        }
                        result.VCSResponseCode = "00";
                        result.VCSResponseMessage = "Validated";
                        return result;
                    }
                    else
                    {
                        result.VCSResponseMessage = "Error: Reference Number too long, only 15 characters are allowed";
                        result.VCSResponseCode = "02";
                        return result;
                    }
                }
                else
                {
                    result.VCSResponseCode = "03";
                    result.VCSResponseMessage = "Reference Number Cannot Be Empty";
                    return result;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateReferenceNumber-" + Ex.Message);
            }

        }
        public Validation ValidateReferenceNumber(String pReferenceNumber)
        {
            Validation result = new Validation();

            try
            {
                char[] myChars = pReferenceNumber.ToCharArray();

                if (pReferenceNumber != string.Empty)
                {
                    if (pReferenceNumber.Length <= 25)
                    {
                        foreach (char myChr in myChars)
                        {
                            if (!char.IsLetterOrDigit(myChr))
                            {
                                result.VCSResponseCode = "01";
                                result.VCSResponseMessage = "Only alphanumeric characters are allowed";
                                return result;
                            }
                        }
                        result.VCSResponseCode = "00";
                        result.VCSResponseMessage = "Validated";
                        return result;
                    }
                    else
                    {
                        result.VCSResponseMessage = "Error: Reference Number too long, only 25 characters are allowed";
                        result.VCSResponseCode = "02";
                        return result;
                    }
                }
                else
                {
                    result.VCSResponseCode = "03";
                    result.VCSResponseMessage = "Reference Number Cannot Be Empty";
                    return result;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateReferenceNumber-" + Ex.Message);
            }

        }

        public Validation ValidateCardNumber(String pUserID, String pCardNumber)
        {
            try
            {
                Validation result = new Validation();

                if (pCardNumber == "")
                {
                    result.VCSResponseCode = "4207";
                    result.VCSResponseMessage = "Card number cannot be empty.";
                    return result;
                }

                System.Text.RegularExpressions.Regex regEx = new System.Text.RegularExpressions.Regex("^[0-9]+$");
                System.Text.RegularExpressions.Match match = regEx.Match(pCardNumber);
                if (!match.Success)
                {
                    result.VCSResponseCode = "4201";
                    result.VCSResponseMessage = "Only numeric digits allowed in card number.";
                    return result;
                }

                List<Bins> userBin = GetBins(pUserID);

                if (userBin.Count == 0)
                {
                    result.VCSResponseCode = "4205";
                    result.VCSResponseMessage = "The merchants BIN table has not been setup correctly.";
                    return result;
                }

                Int32 binPrefixLength = 9;
                while (binPrefixLength > 0)
                {
                    foreach (Bins thisBin in userBin)
                    {
                        String prefix = thisBin.prefix;
                        Int32 prefixLength = Convert.ToInt32(thisBin.prefixlength);
                        String cardType = thisBin.cardtype;
                        String mod10Required = thisBin.mod10required;
                        String pinRequired = thisBin.pinrequired;
                        String cardUsage = thisBin.cardusage;
                        String cardNumberLength = thisBin.cardnumberlength;

                        if (pinRequired == "1")
                        {
                            result.VCSResponseCode = "4202";
                            result.VCSResponseMessage = "Your card requires a PIN and therefore cannot be used.";
                            return result;
                        }
                        if (pCardNumber.Length < 13)
                        {
                            result.VCSResponseCode = "4203";
                            result.VCSResponseMessage = "The card number you entered has too few digits.";
                            return result;
                        }
                        if (pCardNumber.Length > 19)
                        {
                            result.VCSResponseCode = "4204";
                            result.VCSResponseMessage = "The card number you entered has too many digits.";
                            return result;
                        }

                        if (pCardNumber.Substring(0, binPrefixLength) == prefix.Substring(0, prefixLength))
                        {
                            if (mod10Required == "0")
                            {
                                Validation modVal = ValidateMod10(pCardNumber);
                                if (modVal.VCSResponseCode != "0000")
                                {
                                    result.VCSResponseCode = modVal.VCSResponseCode;
                                    result.VCSResponseMessage = modVal.VCSResponseMessage;
                                    return result;
                                }
                            }
                            return result;
                        }
                    }
                    binPrefixLength--;
                }
                result.VCSResponseCode = "4206";
                result.VCSResponseMessage = "Cards beginning with " + pCardNumber.Substring(0, 6) + " are not accepted by this merchant.";
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateCardNumber(string, string) - " + Ex.Message);
            }
        }
        public Validation ValidateEmailAddress(String pEmailAddress)
        {
            try
            {
                Validation result = new Validation();
                result.VCSResponseCode = "0000";
                result.VCSResponseMessage = "Validated";
                if (!pEmailAddress.Contains("@") || !pEmailAddress.Contains(".") || pEmailAddress.Length < 5)
                {
                    result.VCSResponseCode = "4400";
                    result.VCSResponseMessage = "Invalid e-mail address.";
                    return result;
                }

                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateEmailAddress(string) - " + Ex.Message);
            }
        }
        public Validation ValidateExpiryYear(String pExpiryYear)
        {
            try
            {
                Validation result = new Validation();

                Int32 iExpiryYear = 0;
                if (!Int32.TryParse(pExpiryYear, out iExpiryYear))
                {
                    result.VCSResponseCode = "4104";
                    result.VCSResponseMessage = "Expiry year should be a number.";
                    return result;
                }

                if (iExpiryYear > 3912)
                {
                    result.VCSResponseCode = "4105";
                    result.VCSResponseMessage = "Card Expiry date too far ahead";
                    return result;
                }
                return result;

            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateExpiryYear(string) - " + Ex.Message);
            }
        }
        public Validation ValidateExpiryMonth(String pExpiryMonth)
        {
            try
            {
                Validation result = new Validation();
                if (pExpiryMonth.Length != 2)
                {
                    result.VCSResponseCode = "4101";
                    result.VCSResponseMessage = "Expiry month should be two characters.";
                    return result;
                }

                Int32 expiryMonth = 0;
                if (!Int32.TryParse(pExpiryMonth, out expiryMonth))
                {
                    result.VCSResponseCode = "4102";
                    result.VCSResponseMessage = "Expiry month should be a number.";
                    return result;
                }

                if (expiryMonth < 1 || expiryMonth > 12)
                {
                    result.VCSResponseCode = "4103";
                    result.VCSResponseMessage = "Expiry month should be in the range 01-12.";
                    return result;
                }

                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateExpiryMonth(String) - " + Ex.Message);
            }

        }
        public Validation ValidateAmount(String pAmount)
        {
            Validation result = new Validation();
            if (pAmount.Trim() == "")
            {
                result.VCSResponseCode = "4001";
                result.VCSResponseMessage = "No amount present.";
                return result;
            }
            pAmount = StripComma(pAmount);

            if (pAmount.Length > 10)
            {
                result.VCSResponseCode = "4002";
                result.VCSResponseMessage = "Amount exceeds 10 digits.";
                return result;
            }

            Double amountDec = 0;
            if (!Double.TryParse(pAmount, out amountDec))
            {
                result.VCSResponseCode = "4004";
                result.VCSResponseMessage = "Amount is not numeric.";
                return result;
            }

            /*
            if (amountDec == 0)
            {
                result.VCSResponseCode = "4003";
                result.VCSResponseMessage = "Amount is zero.";
                return result;
            }
            */
            if (amountDec < 0)
            {
                result.VCSResponseCode = "4005";
                result.VCSResponseMessage = "Amount may not be negative.";
                return result;
            }

            if (amountDec > 9999999.99)
            {
                result.VCSResponseCode = "4006";
                result.VCSResponseMessage = "Amount is to large.";
                return result;
            }

            return result;
        }
        public Validation ValidateCCV(String pCcv)
        {
            try
            {
                Validation result = new Validation();
                if (pCcv.Length < 3)
                {
                    result.VCSResponseCode = "4500";
                    result.VCSResponseMessage = "CVV too short - must be three or four numeric digits";
                    return result;
                }

                Int32 ccv = 0;
                if (!Int32.TryParse(pCcv, out ccv))
                {
                    result.VCSResponseCode = "4501";
                    result.VCSResponseMessage = "CVV is not numeric";
                    return result;
                }

                result.VCSReturnValue = "0000";
                result.VCSResponseMessage = "Validated";
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateCCV(string) - " + Ex.Message);
            }
        }
        public Validation ValidateBankAccountType(String pAccountType)
        {
            Validation result = new Validation();
            try
            {
                if (pAccountType != string.Empty)
                {
                    switch (pAccountType)
                    {
                        case "1":
                        case "2":
                        case "3":
                        case "4":
                        case "5":
                        case "6":
                            result.VCSResponseCode = "0000"; result.VCSResponseMessage = "Validated"; break;
                        default:
                            result.VCSResponseCode = "1000"; result.VCSResponseMessage = "Invalid Account Type, 1=Current,2=Savings,3=Transmission,4=Bond,6=Subscription Share"; break;
                    }
                }
                else
                {
                    result.VCSResponseCode = "1001";
                    result.VCSResponseMessage = "Bank Account Type Cannot Be Empty";
                }

                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateBankAccountType() - " + Ex.Message);
            }
        }
        public Validation ValidateRecurringType(String pRecurringType)
        {
            Validation result = new Validation();
            try
            {
                if (pRecurringType != string.Empty)
                {
                    switch (pRecurringType)
                    {
                        case "R":
                            result.VCSResponseMessage = "Validated"; result.VCSResponseCode = "0000"; break;
                        default:
                            result.VCSResponseMessage = "Invalid RecurType, R=Recurring"; result.VCSResponseCode = "1000"; break;
                    }
                }
                else
                {
                    result.VCSResponseCode = "1000"; result.VCSResponseMessage = "RecurType cannot be empty";
                }

                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("VaidateRecurringType-" + Ex.Message);
            }
        }
        public Validation ValidateSignCode(String pSignCode)
        {
            Validation result = new Validation();
            try
            {
                if (pSignCode != string.Empty)
                {
                    if (pSignCode == "D")
                    {
                        result.VCSResponseMessage = "Validated"; result.VCSResponseCode = "0000";
                    }
                    else if (pSignCode == "C")
                    {
                        result.VCSResponseMessage = "Validated"; result.VCSResponseCode = "0000";
                    }
                    else
                    {
                        result.VCSResponseMessage = "Error: Invalid SignCode, D=Debit, C=Credit"; result.VCSResponseCode = "1000";
                    }
                }
                else
                {
                    result.VCSResponseCode = "1001"; result.VCSResponseMessage = "Error SignCode Cannot Be Empty";
                }

                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateSignCode-" + Ex.Message);
            }
        }
        public Validation ValidateOccuranceCount(String pOccuranceCount)
        {
            Validation result = new Validation();
            try
            {
                if (pOccuranceCount == null) pOccuranceCount = "";
                if (pOccuranceCount != "")
                {
                    if (pOccuranceCount == "0")
                    {
                        result.VCSResponseCode = "7004";
                        result.VCSResponseMessage = "Occurrence count may not be zero. Number of occurences 1 – 99 or U for an unlimited number of occurrences";
                        return result;
                    }

                    Int32 iOccurCount = 0;
                    if (pOccuranceCount != "U")
                    {
                        if (!Int32.TryParse(pOccuranceCount, out iOccurCount))
                        {
                            result.VCSResponseCode = "7005";
                            result.VCSResponseMessage = "Occurrence count not numeric. Number of occurences 1 – 99 or U for an unlimited number of occurrences";
                            return result;
                        }
                    }
                }

                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateOccuranceCount-" + Ex.Message);
            }
        }
        public Validation ValidateBankBranchCode(String pBankBranchCode)
        {
            Validation result = new Validation();
            try
            {
                if (!String.IsNullOrEmpty(pBankBranchCode))
                {
                    if (pBankBranchCode.Length == 6)
                    {
                        Int32 code = 0;
                        if (Int32.TryParse(pBankBranchCode, out code))
                        {
                            string BC = pBankBranchCode.Substring(0, 3);
                            if (BC != "000")
                            {
                                result.VCSResponseCode = "0000";
                                result.VCSResponseMessage = "Validated";
                            }
                            else
                            {
                                result.VCSResponseCode = "0001";
                                result.VCSResponseMessage = "Error: Bank Branch Code cannot be Zero(s)";
                            }
                        }
                        else
                        {
                            result.VCSResponseCode = "0002";
                            result.VCSResponseMessage = "Error: Bank Branch Code not Numeric";
                        }
                    }
                    else
                    {
                        result.VCSResponseCode = "0003";
                        result.VCSResponseMessage = "Error: Bank Branch Code not 6 digits";
                    }
                }
                else
                {
                    result.VCSResponseCode = "0004";
                    result.VCSResponseMessage = "Error: No Branch Code Supplied";
                }
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateBankBranchCode-" + Ex.Message);
            }
        }
        public Validation ValidateBankAccountNumber(String pBankAccountNumber)
        {
            Validation result = new Validation();
            try
            {
                if (!String.IsNullOrEmpty(pBankAccountNumber))
                {
                    if (pBankAccountNumber.Length >= 9)
                    {
                        Int64 number = 0;
                        if (Int64.TryParse(pBankAccountNumber, out number))
                        {
                            //string BC = pBankAccountNumber.Substring(0, 3);
                            //if (BC != "000")
                            //{
                                result.VCSResponseCode = "0000";
                                result.VCSResponseMessage = "Validated";
                            //}
                            //else
                            //{
                            //    result.VCSResponseCode = "0001";
                            //    result.VCSResponseMessage = "Error: Account Number cannot be Zero(s)";
                            //}
                        }
                        else
                        {
                            result.VCSResponseCode = "0002";
                            result.VCSResponseMessage = "Error: Account Number not Numeric";
                        }
                    }
                    else
                    {
                        result.VCSResponseCode = "0003";
                        result.VCSResponseMessage =  "Error: Account Number must be at least 9 digits";
                    }
                }
                else
                {
                    result.VCSResponseCode = "0004";
                    result.VCSResponseMessage = "Error: No Account Number Supplied";
                }

                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception("ValidateBankAccountNumber-" + Ex.Message);
            }
        }
    }


}
