﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DPO.VirtualVendor.DAL;

namespace DPO.VirtualVendor.BL
{

public partial class VirtualVendorBL
    {
        public vVirtualDebitWsAuth GetvVirtualDebitWsAuth(String pUserName, String pPassword, String pUserID)
        {
            try
            {
                recordFound = false;
                vVirtualDebitWsAuthCrud crud = new vVirtualDebitWsAuthCrud();
                crud.Where("UserName", General.Operator.Equals, pUserName);
                crud.And("Password", General.Operator.Equals, pPassword);
                crud.And("UserID", General.Operator.Equals, pUserID);

                vVirtualDebitWsAuth search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetvVirtualDebitWsAuth - " + Ex.Message);
            }
        }
        public vtUsersAcbQ GetvtUsersAcbQ(String pUserName, String pPassword)
        {
            try
            {
                recordFound = false;
                vtUsersAcbQCrud crud = new vtUsersAcbQCrud();
                crud.Where("UserName", General.Operator.Equals, pUserName);
                crud.And("Password", General.Operator.Equals, pPassword);

                vtUsersAcbQ search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetvtUsersAcbQ - " + Ex.Message);
            }
        }
		public VCSResponse AuthenticateVirtualDebitUser(String pUserName, String pPassword, String pUserID)
        {
            VCSResponse response = new VCSResponse();
            try
            {
                if (pUserName == null || pUserName == "")
                {
                    response.VCSResponseCode = "0093";
                    response.VCSResponseMessage = "User name was not passed.";
                    return response;
                }

                if (pPassword == null || pPassword == "")
                {
                    response.VCSResponseCode = "0093";
                    response.VCSResponseMessage = "Password was not passed.";
                    return response;
                }
                vtUsersAcbQ user = GetvtUsersAcbQ(pUserName, pPassword);
                String securityLevel = "";
                if (RecordFound)
                {
                    if (user.SecLevel == "0")
                        securityLevel = "Admin";
                    else
                        securityLevel = "User";
                }
                else
                {
                    securityLevel = "NotFound";
                }

                switch (securityLevel)
                {
                    case "Admin":
                        response.VCSResponseCode = "0000";
                        response.VCSResponseMessage = "Success";
                        break;
                    case "User":
                        vVirtualDebitWsAuth auth = GetvVirtualDebitWsAuth(pUserName.Trim(), pPassword.Trim(), pUserID.Trim());
                        if (RecordFound)
                        {
                            response.VCSResponseMessage = "0000";
                            response.VCSResponseMessage = "Success";
                        }
                        else
                        {
                            response.VCSResponseCode = "0094";
                            response.VCSResponseMessage = "User was not found.";
                        }
                        break;
                    case "NotFound":
                        response.VCSResponseCode = "0094";
                        response.VCSResponseMessage  = "User was not found.";
                        break;
                    default:
                        if (securityLevel.Contains("Error"))
                        {
                            response.VCSResponseMessage = securityLevel;
                            response.VCSResponseCode = "0099";
                        }
                        else
                        {
                            response.VCSResponseCode = "0094";
                            response.VCSResponseMessage = "Invalid Login Detected";
                        }
                        break;
                }
                return response;
            }
            catch (Exception Ex)
            {
                throw new Exception("AuthenticateVirtualDebitUser - " + Ex.Message);
            }
        }
        public Boolean VirtualDebitLogServiceCall(String pService, String pUserID, String pReferenceNumber, String pCallerIP, String pParameters, String pDetail)
        {
            try
            {
                StoredProcedure.vcs_LogWebserviceCalls(pService, pUserID, pReferenceNumber, pCallerIP, pParameters, pDetail);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("VirtualDebitLogServiceCall - " + Ex.Message);
            }
        }
        public System.Data.DataTable GetVirtualDebitProcessedTransactions (String pUserID, String pReferenceNumber, String pFromDate, String pToDate)
        {
			try
			{
				AcbTransCrud crud = new AcbTransCrud();
				crud.Where("UserID", General.Operator.Equals, pUserID);
				crud.And("Reference", General.Operator.Like, pReferenceNumber);
				crud.And("Presented", General.Operator.Between, pFromDate, pToDate);
				crud.OrderBy(x => (DateTime)x.Presented, CrudBase<AcbTrans>.SortOrder.Descending);
				List<AcbTrans> trans = crud.ReadMulti().ToList();
				System.Data.DataTable tblTran = new System.Data.DataTable(typeof(AcbTrans).Name);
				System.Reflection.PropertyInfo[] Props = typeof(AcbTrans).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
				foreach (System.Reflection.PropertyInfo prop in Props)
				{
					var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
					tblTran.Columns.Add(prop.Name, type);
				}

				foreach (AcbTrans item in trans)
				{
					var values = new object[Props.Length];
					for (int i = 0; i < Props.Length; i++)
					{
						//inserting property values to datatable rows
						values[i] = Props[i].GetValue(item, null);
					}
					tblTran.Rows.Add(values);
				}

				return tblTran;

			}
			catch (Exception Ex)
			{
				throw new Exception("GetVirtualDebitProcessedTransactions-" + Ex.Message);
			}
		}
        public System.Data.DataTable VirtualRecurGetCCPaymentsByTerminal(String pUserID)
        {
            try
            {
                System.Data.DataTable tblResult = new System.Data.DataTable();
                tblResult.Columns.Add("ID");
                tblResult.Columns.Add("UserID");
                tblResult.Columns.Add("ReferenceNumber");
                tblResult.Columns.Add("CardNo");
                tblResult.Columns.Add("CardholderName");
                tblResult.Columns.Add("EmailAddress");
                tblResult.Columns.Add("ExpiryMM");
                tblResult.Columns.Add("ExpiryYY");
                tblResult.Columns.Add("Amount");
                tblResult.Columns.Add("DescOfGoods");
                tblResult.Columns.Add("Presented");
                tblResult.Columns.Add("StartDate");
                tblResult.Columns.Add("EndDate");
                tblResult.Columns.Add("OccurCount");
                tblResult.Columns.Add("Frequency");
                tblResult.Columns.Add("Suspended");
                tblResult.Columns.Add("SuspendedDate");
                tblResult.Columns.Add("Deleted");
                tblResult.Columns.Add("M1");
                tblResult.Columns.Add("M2");
                List<vcs_VirtualRecurWsGetCCByUserID> spResult = StoredProcedure.vcs_VirtualRecurWsGetCCByUserID(pUserID);
                foreach (vcs_VirtualRecurWsGetCCByUserID rec in spResult)
                {
                    tblResult.Rows.Add(rec.ID, rec.UserId, rec.ReferenceNumber, rec.CardNo, rec.CardholderName, rec.EmailAddress, rec.ExpiryMm, rec.ExpiryYy, rec.Amount, rec.DescrOfGoods, rec.Presented, rec.StartDate, rec.EndDate, rec.OccurCount, rec.Frequency, rec.Suspended, rec.SuspendedDate, rec.Deleted, rec.MerchVariable1, rec.MerchVariable2);
                }

                return tblResult;
            }
            catch (Exception Ex)
            {
                throw new Exception("VirtualRecurGetCCPaymentsByTerminal-" + Ex.Message);
            }
        }
        public Boolean VirtualRecurInsertCCPayment(String pUserID, String pReferenceNumber, String pCardNumber, String pCvc, String pCardholderName, String pExpiryMM, String pExpiryYY, Decimal pAmount, String pDescriptionOfGoods, String pStartDate, String pEndDate, String pOccurCount, String pOccurFrequency, String pOccurEmail, String pMerchantVariable1, String pMerchantVariable2, String pMerchantVariable3, String pMerchantVariable4, String pMerchantVariable5, String pMerchantVariable6, String pMerchantVariable7, String pMerchantVariable8, String pMerchantVariable9, String pMerchantVariable10)
        {
            try
            {
                
                StoredProcedure.vcs_VirtualRecurWsInsertCC(pUserID, pReferenceNumber, pCardNumber, pCvc, pCardholderName, pExpiryMM, pExpiryYY, pAmount, pDescriptionOfGoods, pStartDate, pEndDate, pOccurCount, pOccurFrequency, pOccurEmail, pMerchantVariable1, pMerchantVariable2,pMerchantVariable3, pMerchantVariable4, pMerchantVariable5, pMerchantVariable6, pMerchantVariable7, pMerchantVariable8, pMerchantVariable9, pMerchantVariable10);
                
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("VirtualRecurInsertCCPayment-" + Ex.Message);
            }
        }
        public Boolean VirtualRecurUpdateCCPayment(String pUserID, String pReferenceNumber, String pCardholderName, Decimal pAmount, String pDescriptionOfGoods, String pStartDate, String pEndDate, String pOccurCount, String pOccurFrequency, String pOccurEmail, String pMerchantVariable1, String pMerchantVariable2, String pMerchantVariable3, String pMerchantVariable4, String pMerchantVariable5, String pMerchantVariable6, String pMerchantVariable7, String pMerchantVariable8, String pMerchantVariable9, String pMerchantVariable10, String pUserName)
        {
            try
            {
                StoredProcedure.vcs_VirtualRecurWsUpdateCC(pUserID, pReferenceNumber, pCardholderName, pAmount, pDescriptionOfGoods, pStartDate, pEndDate, pOccurCount, pOccurFrequency, pOccurEmail, pMerchantVariable1, pMerchantVariable2, pMerchantVariable3, pMerchantVariable4, pMerchantVariable5, pMerchantVariable6, pMerchantVariable7, pMerchantVariable8, pMerchantVariable9, pMerchantVariable10);
                RecurTmpl tmpl = GetRecurTmpl(pUserID, pReferenceNumber);
                tmpl.ModifiedUserName = pUserName;
                tmpl.ModifiedDate = DateTime.Now;
				RecurTmplCrud crud = new RecurTmplCrud();
				crud.Update(tmpl);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("VirtualRecurUpdateCCPayment-" + Ex.Message);
            }
        }
        public Boolean VirtualRecurUpdateCCNumber(String pUserID, String pReferenceNumber, String pCardNumber, String pCvc, String pCardholderName, String pExpiryMM, String pExpiryYY)
        {
            try
            {
                StoredProcedure.vcs_VirtualRecurWsUpdateCCNumber(pUserID, pReferenceNumber, pCardNumber, pCvc, pCardholderName, pExpiryMM, pExpiryYY);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("VirtualRecurUpdateCCNumber-" + Ex.Message);
            }
        }
        public AcbTmpl GetAcbTmpl(String pUserID, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                AcbTmplCrud crud = new AcbTmplCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                AcbTmpl search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetAcbTmpl - " + Ex.Message);
            }
        }
        public Boolean UpdateVirtualDebitOrder(String pUserID, String pReferenceNumber, String pAccountHolder, String pBankName, String pAccountNumber, String pBranchCode, 
            String pAccountType, Decimal pAmount, String pSignCode, String pTranDesc, String pEmailAddress, String pStartDate, String pEndDate, String pOccurCount, 
            String pRecurType, String pFrequency, String pModifiedUserName, String pIBan, String pApoAccountNumber)
        {
            try
            {
                AcbTmpl acb = GetAcbTmpl(pUserID, pReferenceNumber);
                acb.ReferenceNumber = pReferenceNumber;
                acb.AccountHolder = pAccountHolder;
                acb.BankName = pBankName;
                acb.AccountNumber = pAccountNumber;
                acb.BranchCode = pBranchCode;
                acb.AccountType = pAccountType;
                acb.Amount = pAmount;
                acb.SignCode = pSignCode;
                acb.TranDescr = pTranDesc;
                acb.EmailAddress = pEmailAddress;
                acb.StartDate = pStartDate;
                acb.EndDate = pEndDate;
                acb.OccurCount = pOccurCount;
                acb.RecurType = pRecurType;
                acb.Frequency = pFrequency;
                acb.ModifiedUserName = pModifiedUserName;
                acb.IBAN = pIBan;
                acb.ApoAccountNumber = pApoAccountNumber;
                acb.ModifiedDate = DateTime.Now;
                AcbTmplCrud crud = new AcbTmplCrud();
                crud.Update(acb);

                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateVirtualDebitOrder-" + Ex.Message);
            }
        }
        public Boolean UpdateVirtualDebitOrder(String pReferenceNumber, String pAccountHolder, String pBankName, String pAccountNumber, String pBranchCode, String pAccountType, Decimal pAmount, String pSignCode, String pTranDesc, String pEmailAddress, String pStartDate, String pEndDate, String pOccurCount, String pRecurType, String pFrequency, String pModifiedUserName, String pIBan)
        {
            try
            {
                StoredProcedure.vcs_VirtualDebitWsUpdateDebitOrder(pReferenceNumber, pAccountHolder, pBankName, pAccountNumber, pBranchCode, pAccountType, pAmount, pSignCode, pTranDesc, pEmailAddress, pStartDate, pEndDate, pOccurCount, pRecurType, pFrequency, pModifiedUserName, pIBan);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UpdateVirtualDebitOrder-" + Ex.Message);
            }
        }
        public Int32 InsertAcbTmpl(String pUserName, String pUserID, String pReferenceNumber, String pRecurType, String pAccountHolder, String pBankName, String pAccountNumber, String pBranchCode, String pAccountType, Decimal pAmount, String pSignCode, String pTranDescr, String pEmailAddress, String pStartDate, String pEndDate, String pOccurCount, String pFrequency, String pIBAN)
        {
            try
            {
                AcbTmplCrud crud = new AcbTmplCrud();
                AcbTmpl workRec = new AcbTmpl();
                workRec.UserID = pUserID;
                workRec.ReferenceNumber = pReferenceNumber;
                workRec.RecurType = pRecurType;
                workRec.AccountHolder = pAccountHolder;
                workRec.BankName = pBankName;
                workRec.AccountNumber = pAccountNumber;
                workRec.BranchCode = pBranchCode;
                workRec.AccountType = pAccountType;
                workRec.Amount = pAmount;
                workRec.SignCode = pSignCode;
                workRec.TranDescr = pTranDescr;
                workRec.EmailAddress = pEmailAddress;
                workRec.StartDate = pStartDate;
                workRec.EndDate = pEndDate;
                workRec.OccurCount = pOccurCount;
                workRec.Frequency = pFrequency;
                workRec.IBAN = pIBAN;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertAcbTmpl - " + Ex.Message);
            }
        }
        public Int32 InsertAcbTmpl(String pUserName, String pUserID, String pReferenceNumber, String pRecurType, String pAccountHolder, String pBankName, String pAccountNumber, String pBranchCode, String pAccountType, Decimal pAmount, String pSignCode, String pTranDescr, String pEmailAddress, String pStartDate, String pEndDate, String pOccurCount, String pFrequency, String pIBAN, String pApoAccountNumber)
        {
            try
            {
                AcbTmplCrud crud = new AcbTmplCrud();
                AcbTmpl workRec = new AcbTmpl();
                workRec.UserID = pUserID;
                workRec.ReferenceNumber = pReferenceNumber;
                workRec.RecurType = pRecurType;
                workRec.AccountHolder = pAccountHolder;
                workRec.BankName = pBankName;
                workRec.AccountNumber = pAccountNumber;
                workRec.BranchCode = pBranchCode;
                workRec.AccountType = pAccountType;
                workRec.Amount = pAmount;
                workRec.SignCode = pSignCode;
                workRec.TranDescr = pTranDescr;
                workRec.EmailAddress = pEmailAddress;
                workRec.StartDate = pStartDate;
                workRec.EndDate = pEndDate;
                workRec.OccurCount = pOccurCount;
                workRec.Frequency = pFrequency;
                workRec.IBAN = pIBAN;
                workRec.ApoAccountNumber = pApoAccountNumber;
                workRec.Presented = DateTime.Now;
                workRec.Suspended = "N";
                workRec.Deleted = "N";
                workRec.UserName = pUserName;
                long ID = 0;
                crud.Insert(workRec, out ID);
                return (Int32)ID;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertAcbTmpl - " + Ex.Message);
            }
        }
        public Boolean SuspendAcbTmpl(String pUserName, String pUserID, String pReferenceNumber)
        {
            try
            {
                AcbTmpl tmpl = GetAcbTmpl(pUserID, pReferenceNumber);
                tmpl.Suspended = "Y";
                tmpl.SuspendedDate = DateTime.Now;
                tmpl.ModifiedDate = DateTime.Now;
                tmpl.ModifiedUserName = pUserName;
				AcbTmplCrud crud = new AcbTmplCrud();
				crud.Update(tmpl);
				
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("SuspendAcbTmpl-" + Ex.Message);
            }
        }
        public Boolean UnSuspendAcbTmpl(String pUserName, String pUserID, String pReferenceNumber)
        {
            try
            {
                AcbTmpl tmpl = GetAcbTmpl(pUserID, pReferenceNumber);
                tmpl.Suspended = "N";
                tmpl.ModifiedDate = DateTime.Now;
                tmpl.ModifiedUserName = pUserName;
				tmpl.SuspendedDate = null;
				AcbTmplCrud crud = new AcbTmplCrud();
				crud.Update(tmpl);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UnSuspendAcbTmpl-" + Ex.Message);
            }
        }
        public Boolean DeleteAcbTmpl(String pUserName, String pUserID, String pReferenceNumber)
        {
            try
            {
                AcbTmpl tmpl = GetAcbTmpl(pUserID, pReferenceNumber);
                AcbTmplCrud crud = new AcbTmplCrud();
                crud.Delete(tmpl);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteAcbTmpl - " + Ex.Message);
            }
        }
        public Boolean UndeleteAcbTmpl(String pUserName, String pUserID, String pReferenceNumber)
        {
            try
            {
                AcbTmpl tmpl = GetAcbTmpl(pUserID, pReferenceNumber);
                tmpl.Deleted = "N";
                tmpl.ModifiedDate = DateTime.Now;
                tmpl.ModifiedUserName = pUserName;
				tmpl.DeletedUserName = null;
				tmpl.DeletedDate = null;
				AcbTmplCrud crud = new AcbTmplCrud();
				crud.Update(tmpl);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("UndeleteAcbTmpl-" + Ex.Message);
            }
        }
        public List<AcbBanks> GetAcbBanks()
        {
            try
            {
                recordFound = false;
                AcbBanksCrud crud = new AcbBanksCrud();
                List<AcbBanks> search = crud.ReadMulti().ToList();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetAcbBank - " + Ex.Message);
            }
        }
        public System.Data.DataTable GetAcbBankDT()
        {
            try
            {
                List<AcbBanks> result = GetAcbBanks();
                System.Data.DataTable tblBank = new System.Data.DataTable("Banks");
                tblBank.Columns.Add("Description");
                tblBank.Columns.Add("ShortCode");
                tblBank.Columns.Add("DefaultBranchCode");
                foreach (AcbBanks bank in result)
                    tblBank.Rows.Add(bank.Description, bank.ShortCode, bank.DefaultBranchCode);

                return tblBank;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetAcbBankDT-" + Ex.Message);
            }
        }
        public Boolean InsertAcbTrans(String pUserID, String pUserName, String pReferenceNumber, String pAccountHolder, String pBankName, String pBranchCode, String pAccountNumber, String pIBAN, String pAccountType, decimal pAmount, String pSignCode, String pTranDesc, String pApoAccountNumber)
        {
            try
            {
                AcbTransCrud crud = new AcbTransCrud();
                AcbTrans workRec = new AcbTrans();
                workRec.UserName = pUserName;
                workRec.ReferenceNumber = pReferenceNumber;
                workRec.AccountHolder = pAccountHolder;
                workRec.BankName = pBankName;
                workRec.BranchCode = pBranchCode;
                workRec.AccountNumber = pAccountNumber;
                workRec.IBAN = pIBAN;
                workRec.AccountType = pAccountType;
                workRec.Amount = pAmount;
                workRec.SignCode = pSignCode;
                workRec.ApoAccountNumber = pApoAccountNumber;
                workRec.TranDescr = pTranDesc;
                workRec.Presented = DateTime.Now;
                workRec.UserId = pUserID;
                crud.Insert(workRec);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertAcbTrans - " + Ex.Message);
            }
        }
        public Boolean InsertAcbTrans(String pUserID, String pUserName, String pReferenceNumber, String pAccountHolder, String pBankName, String pBranchCode, String pAccountNumber, String pIBAN, String pAccountType, decimal pAmount, String pSignCode, String pTranDesc)
        {
            try
            {
                AcbTransCrud crud = new AcbTransCrud();
                AcbTrans workRec = new AcbTrans();
                workRec.UserName = pUserName;
                workRec.ReferenceNumber = pReferenceNumber;
                workRec.AccountHolder = pAccountHolder;
                workRec.BankName = pBankName;
                workRec.BranchCode = pBranchCode;
                workRec.AccountNumber = pAccountNumber;
                workRec.IBAN = pIBAN;
                workRec.AccountType = pAccountType;
                workRec.Amount = pAmount;
                workRec.SignCode = pSignCode;
                crud.Insert(workRec);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("InsertAcbTrans - " + Ex.Message);
            }
        }
        public AcbTrans GetAcbTrans(String pUserID, String pReferenceNumber)
        {
            try
            {
                recordFound = false;
                AcbTransCrud crud = new AcbTransCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                AcbTrans search = crud.ReadSingle();
                recordFound = crud.RecordFound;
                return search;
            }
            catch (Exception Ex)
            {
                throw new Exception("GetAcbTran - " + Ex.Message);
            }
        }
        public Boolean DeleteAcbTran(String pUserID, String pReferenceNumber)
        {
            try
            {
                AcbTransCrud crud = new AcbTransCrud();
                crud.Where("UserID", General.Operator.Equals, pUserID);
                crud.And("ReferenceNumber", General.Operator.Equals, pReferenceNumber);

                List<AcbTrans> search = crud.ReadMulti().ToList();
                foreach (AcbTrans del in search)
                    crud.Delete(del);
                return true;
            }
            catch (Exception Ex)
            {
                throw new Exception("DeleteAcbTran - " + Ex.Message);
            }
        }
        public System.Data.DataTable GetDebitOrdersByTerminalID(String pUserID, DateTime pDateFrom, DateTime pDateTo)
        {
            try
            {
                List<vcs_VirtualDebitWsGetDebitordersByTerminal>  debitOrders = StoredProcedure.vcs_VirtualDebitWsGetDebitordersByTerminal(pUserID, pDateFrom, pDateTo);
                System.Data.DataTable tblDebitOrder = new System.Data.DataTable("DebitOrder");
                tblDebitOrder.Columns.Add("ReferenceNumber");
                tblDebitOrder.Columns.Add("RecurType");
                tblDebitOrder.Columns.Add("AccountHolder");
                tblDebitOrder.Columns.Add("BankName");
                tblDebitOrder.Columns.Add("AccountNumber");
                tblDebitOrder.Columns.Add("BranchCode");
                tblDebitOrder.Columns.Add("AccountType");
                tblDebitOrder.Columns.Add("AmountInd");
                tblDebitOrder.Columns.Add("Amount");
                tblDebitOrder.Columns.Add("SignCode");
                tblDebitOrder.Columns.Add("TranDescr");
                tblDebitOrder.Columns.Add("EmailAddress");
                tblDebitOrder.Columns.Add("Presented");
                tblDebitOrder.Columns.Add("StartDate");
                tblDebitOrder.Columns.Add("EndDate");
                tblDebitOrder.Columns.Add("OccurCount");
                tblDebitOrder.Columns.Add("Frequency");
                tblDebitOrder.Columns.Add("Suspended");
                tblDebitOrder.Columns.Add("Deleted");
                tblDebitOrder.Columns.Add("ApoAccountNumber");

                foreach (vcs_VirtualDebitWsGetDebitordersByTerminal order in debitOrders)
                    tblDebitOrder.Rows.Add(order.ReferenceNumber, order.RecurType, order.AccountHolder, order.BankName, order.AccountNumber, order.BranchCode, order.AccountType, order.AmountInd, order.Amount, order.SignCode, order.TranDescr, order.EmailAddress, order.Presented, order.StartDate, order.EndDate, order.OccurCount, order.Frequency, order.Suspended, order.Deleted, order.ApoAccountNumber);

                return tblDebitOrder;

            }
            catch (Exception Ex)
            {
                throw new Exception("GetDebitOrdersByTerminalID-" + Ex.Message);
            }

        }
    }
}
