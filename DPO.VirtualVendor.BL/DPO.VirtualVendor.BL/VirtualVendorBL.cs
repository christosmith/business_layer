﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DPO.VirtualVendor.BL
{

	public partial class VirtualVendorBL
	{
		private Boolean recordFound = false;

        public String ErrorMessage
        {
            get
            {
                return "";
            }
        }
		public Boolean RecordFound
		{
			get
			{
				return recordFound;
			}
		}
		public enum AuthTranType
		{
			Refund = 1,
			SettleOriginal = 2,
			RefundOriginal = 3,
			Settle220 = 4,
			Auth100 = 5,
			Auth200 = 6

		}
	}

	public enum eLogType
	{
		Debug = 1,
		Warning = 2,
		Information = 3,
		Error = 4
	}

	public enum AuthTranType
	{
		Refund = 1,
		SettleOriginal = 2,
		RefundOriginal = 3,
		Settle220 = 4,
		Auth100 = 5,
		Auth200 = 6

	}

	public class Validation
	{
		public String VCSResponseCode = "0000";
		public String VCSResponseMessage = "Validated";
		public String VCSReturnValue = "";
	}

	public class Authentication
	{
		public String VCSResponseCode = "0000";
		public String VCSResponseMessage = "Authenticated";
	}

	public enum DatabaseInteraction
	{
		Insert = 1,
		Update = 2,
		Delete = 3,
		Select = 4
	}

	public class VCSResponse
	{
		public String VCSResponseCode = "0000";
		public String VCSResponseMessage = "Success";
		public String ReturnValue = "";
	}


	public class MobiCredInformation
	{
		public String userName;
		public String password;
		public String merchantID;
		public String merchantKey;
		public String merchantRequestID;
		public Int32 requestID;
		public String url;
		public String processDate;
	}
	
	public class mPars
	{
		internal String mParName = "";
		internal String mParValue = "";
	}
	public class ccFormParameters
	{
		public String TerminalID = "";
		public String ReferenceNumber = "";
		public String DescriptionOfGoods = "";
		public String RecurReference = "";
		public Decimal Amount = 0;
		public String Currency = "";
		public String OccurCount = "";
		public String OccurFrequency = "";
		public String CellNumber = "";
		public String CellMessage = "";
		public String CancelUrl = "";
		public String OccurEmailAddress = "";
		public String DelayedSettlement = "";
		public Decimal OccurAmount = 0;
		public String PaymentMethod = "";
		public String CardType = "";
		public String CardholderName = "";
		public String CardholderEmail = "";
		public String NextOccurDate = "";
		public String BasketEntry = "";
		public String VTInvoice = "";
		public String CCPay = "";
		public String CCReservation = "";
		public String OSCommerce = "";
		public String OSApprovedURL = "";
		public String OSDeclinedURL = "";
		public String ApprovedURL = "";
		public String DeclinedURL = "";
		public String Hash = "";
		public String BudgetAllowed = "";
		public String Recap = "";
		public String RecapLine1 = "";
		public String RecapLine2 = "";
		public String RecapLine3 = "";
		public String TCUrl = "";
		public String TCConfirm = "";
		public DateTime LastAcceptanceDate = Convert.ToDateTime("1900-01-01");
		public String MysteryDiscount = "";
		public String MerchantMD5String = "";
		public String OccurUpdate = "";
		public String URLsProvided = "";
		public String m_1 = "";
		public String m_2 = "";
		public String m_3 = "";
		public String m_4 = "";
		public String m_5 = "";
		public String m_6 = "";
		public String m_7 = "";
		public String m_8 = "";
		public String m_9 = "";
		public String m_10 = "";
		public String UserID = "";
		public String MobileEnvironment = "";
		public String CardExpiryMM = "";
		public String CardExpiryYY = "";
		public String CCV = "";
		public String HashValue = "";
		public String CustomerID = "";
		public String MerchantToken = "";
		public String MerchMd5String = "";
	}
}
